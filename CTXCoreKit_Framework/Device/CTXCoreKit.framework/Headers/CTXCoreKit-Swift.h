// Generated by Apple Swift version 2.3 (swiftlang-800.10.12 clang-800.0.38)
#pragma clang diagnostic push

#if defined(__has_include) && __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wauto-import"
#include <objc/NSObject.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if defined(__has_include) && __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus) || __cplusplus < 201103L
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif

#if defined(__has_attribute) && __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if defined(__has_attribute) && __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if defined(__has_attribute) && __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if defined(__has_attribute) && __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name) enum _name : _type _name; enum SWIFT_ENUM_EXTRA _name : _type
# if defined(__has_feature) && __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) SWIFT_ENUM(_type, _name)
# endif
#endif
#if defined(__has_feature) && __has_feature(modules)
@import Foundation;
@import CoreData;
#endif

#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
@class NSObject;
@class NSCoder;

SWIFT_CLASS("_TtC10CTXCoreKit15CTXNetworkError")
@interface CTXNetworkError : NSError
+ (NSString * _Nonnull)OriginalErrorKey;
+ (NSString * _Nonnull)DataKey;
+ (NSString * _Nonnull)Domain;
- (nonnull instancetype)initWithUserInfo:(NSDictionary * _Nullable)dict;
- (nonnull instancetype)initWithCode:(NSInteger)code userInfo:(NSDictionary * _Nullable)dict;
- (nonnull instancetype)initWithCode:(NSInteger)code message:(NSString * _Nullable)message userInfo:(NSDictionary * _Nullable)dict OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_CLASS("_TtC10CTXCoreKit25CTXNetworkValidationError")
@interface CTXNetworkValidationError : CTXNetworkError
+ (NSString * _Nonnull)ValidationDatakey;
+ (NSString * _Nonnull)Domain;
- (nonnull instancetype)initWithCode:(NSInteger)code message:(NSString * _Nullable)message userInfo:(NSDictionary * _Nullable)dict OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_CLASS("_TtC10CTXCoreKit30CTXNetworkStateValidationError")
@interface CTXNetworkStateValidationError : CTXNetworkValidationError
+ (NSString * _Nonnull)Domain;
- (nonnull instancetype)initWithCode:(NSInteger)code message:(NSString * _Nullable)message userInfo:(NSDictionary * _Nullable)dict OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
@end



@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
@end

@class NSPredicate;
@class NSManagedObjectContext;
@class NSFetchRequest;

@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSFetchRequest * _Nonnull)ctx_requestFirstWithPredicate:(NSPredicate * _Nonnull)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestFirstByAttribute:(NSString * _Nonnull)attribute withValue:(NSObject * _Nonnull)searchValue inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSInteger)ctx_defaultBatchSize;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
@end

@class NSEntityDescription;

@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSString * _Nonnull)ctx_entityName;
+ (NSEntityDescription * _Nonnull)ctx_entityDescriptionInContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSInteger)ctx_numberOfEntitiesWithContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSInteger)ctx_numberOfEntitiesWithPredicate:(NSPredicate * _Nullable)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSInteger)ctx_countOfEntitiesWithContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSInteger)ctx_countOfEntitiesWithPredicate:(NSPredicate * _Nullable)searchFilter inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSFetchRequest * _Nonnull)ctx_createFetchRequestInContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_createFetchRequestWithRelationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_createFetchRequestNotFaultedInContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_createFetchRequestNotFaultedWithRelationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (nullable instancetype)ctx_createEntityInContext:(NSManagedObjectContext * _Nonnull)context;
- (BOOL)ctx_deleteEntityInContext:(NSManagedObjectContext * _Nonnull)context;
+ (BOOL)ctx_deleteAllMatchingPredicate:(NSPredicate * _Nullable)predicate inContext:(NSManagedObjectContext * _Nonnull)context;
+ (BOOL)ctx_truncateAllInContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSArray<NSDictionary<NSString *, id> *> * _Nullable)ctx_executeDictionaryFetchRequest:(NSFetchRequest * _Nonnull)request inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSDictionary<NSString *, id> * _Nullable)ctx_executeDictionaryFetchRequestAndReturnFirstObject:(NSFetchRequest * _Nonnull)request inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate * _Nullable)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate * _Nullable)searchTerm relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate * _Nullable)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate * _Nullable)searchTerm relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withLimit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllSortedBy:(NSString * _Nonnull)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate * _Nullable)searchTerm limit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithLimit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithPredicate:(NSPredicate * _Nonnull)searchTerm limit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value withLimit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithLimit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithPredicate:(NSPredicate * _Nonnull)searchTerm limit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value withLimit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithLimit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithPredicate:(NSPredicate * _Nonnull)searchTerm limit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value withLimit:(NSInteger)limit offset:(NSInteger)offset inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithLimit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithPredicate:(NSPredicate * _Nonnull)searchTerm limit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value withLimit:(NSInteger)limit offset:(NSInteger)offset relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
@end


@interface NSManagedObject (SWIFT_EXTENSION(CTXCoreKit))
+ (NSFetchRequest * _Nonnull)ctx_requestAllInContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithPredicate:(NSPredicate * _Nullable)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithRelationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWithPredicate:(NSPredicate * _Nonnull)searchTerm relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedInContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithPredicate:(NSPredicate * _Nonnull)searchTerm inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithRelationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)ctx_requestAllNotFaultedWithPredicate:(NSPredicate * _Nonnull)searchTerm relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
+ (NSFetchRequest * _Nonnull)SPL_requestAllNotFaultedWhere:(NSString * _Nonnull)property isEqualTo:(NSObject * _Nonnull)value relationshipPrefetching:(NSArray<NSString *> * _Nonnull)relationships inContext:(NSManagedObjectContext * _Nonnull)context;
@end

#pragma clang diagnostic pop
