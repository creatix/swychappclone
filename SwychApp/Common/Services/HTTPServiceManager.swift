//
//  HTTPServiceManager.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import CTXCoreKit

//@objc(SCMHTTPServiceManager)
public class HTTPServiceManager: NSObject, ServiceManagerProtocol {
    
    static public let sharedInstance = HTTPServiceManager()
    
    lazy public var completionDispatchQueue: DispatchQueue? = { return DispatchQueue(label: "\(Config.bundleIdentifier).service-manager.completion-queue", attributes: []) }()
    
    lazy internal var _networkLayer: HTTPNetworkLayer = { return try! HTTPNetworkLayer(baseURL: Config.Contacts.serverBaseURL) }()
    
    override private init() {
        super.init()
        
        self._networkLayer.DEBUG = true
        /*
        #if DEBUG
            self._networkLayer.DEBUG = true
        #else
            self._networkLayer.DEBUG = false
        #endif
        */
        
        self._networkLayer.bodyParametersEncoding = HTTPNetworkLayer.RequestParameterEncoding.json
        self._networkLayer.expectedResponseType = HTTPNetworkLayer.ResponseSerializerType.json
        self._networkLayer.headers = [
            "Accept": "application/json",
            "x-api-key": Config.Contacts.apiKey,
            "x-api-secret": Config.Contacts.apiSecret,
        ]
        self._networkLayer.responseContentValidate = { (request, response, data, stateValidation) in
            switch response?.statusCode ?? 0 {
            case 204: return (state: HTTPNetworkLayer.ResponseContentValidationState.success, validation: nil)
            default:
                guard let _: Dictionary<String, AnyObject> = data as? [String: AnyObject] else {
                    //  Send Error for incorrect Data
                    return (state: HTTPNetworkLayer.ResponseContentValidationState.invalid, validation: HTTPNetworkLayer.ResponseValidation(code: NetworkError.Code.response.rawValue, message: "Received content is in invalid format.", data: nil))
                }
                //  TODO: check if exists
                return (state: HTTPNetworkLayer.ResponseContentValidationState.success, validation: nil)
            }
        }
        self._networkLayer.responsePreprocess = { (request, response, data) in
            guard let value: Dictionary<String, AnyObject> = data as? Dictionary<String, AnyObject>,
                let parsedData: Dictionary<String, AnyObject> = value["data"] as? Dictionary<String, AnyObject>
                else { return data }
            return parsedData
        }
    }
    
    public func updateAuthorization(bearer token: String) {
        self._networkLayer.headers! += ["Authorization": "Bearer \(token)"]
    }
    
    internal func cancel() {
        self._networkLayer.sessionManager.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
            dataTasks.forEach( { $0.cancel() } )
            uploadTasks.forEach( { $0.cancel() } )
            downloadTasks.forEach( { $0.cancel() } )
        }
    }
    
}
