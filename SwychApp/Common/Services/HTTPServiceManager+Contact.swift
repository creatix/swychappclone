//
//  HTTPServiceManager+Contact.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import CTXCoreKit

import ObjectMapper

extension HTTPServiceManager {
    
    public func searchContacts(with term: String,
                               _ queueType: ServiceManager.CompletionDispatchQueueType? = ServiceManager.CompletionDispatchQueueType.own,
                               success: ((_ info: SearchResultServiceModel?) -> Void)?,
                               failure: ((_ error: NSError?) -> Void)?) {
        
        try! self._networkLayer.request(HTTPNetworkLayer.RequestMethod.get,
                                        relativeURL: "search/device",
                                        urlParameters: ["str": term],
                                        success: { [weak self] (data, validationData) in
                                            guard let data = data as? [String: Any] else {
                                                self?.completionCallback(queueType, completion: { () in
                                                    if let failure = failure {
                                                        failure(nil)
                                                    }
                                                })
                                                return
                                            }
                                            
                                            if let success = success {
                                                self?.completionCallback(queueType, completion: { () in
                                                    success(Mapper<SearchResultServiceModel>().map(JSON: data))
                                                })
                                            }
            },
                                        failure: { [weak self] (error, data, validationData) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(error as NSError?)
                                                })
                                            }
            },
                                        cancel: { [weak self] (response) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(nil)
                                                })
                                            }
            })
        
    }
    
}
