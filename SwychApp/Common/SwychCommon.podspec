Pod::Spec.new do |spec|

    spec.name                       = 'SwychCommon'
    spec.version                    = '1.0.0'
    spec.homepage                   = 'http://goswych.com'
    spec.license                    = {
        :type => 'Copyright',
        :text => 'Copyright (C) 2014-2016 Creatix Inc. All Rights Reserved.'
    }
    spec.authors                    = {
        'Default' => 'ios@thinkcreatix.com',
        'Milan Horvatovic' => 'milan.horvatovic@thinkcreatix.com',
        'Juraj Homola' => 'juraj.homola@thinkcreatix.com'
    }
    spec.summary                    = 'Common parts of Swych platform.'
    spec.source                     = {
        :path => './',
    }

    spec.module_name = 'SwychCommon'
    spec.platform = :ios, "8.0"
    spec.ios.deployment_target = "8.0"
    spec.requires_arc = true

    #spec.source_files = '**/*.{h,m,mm,swift}'

    spec.default_subspec = 'Core', 'Common'

    spec.subspec 'Core' do |cs|
        cs.source_files = 'Config.swift'
    end

    spec.subspec 'Common' do |cs|
        cs.source_files = '**/*.{h,m,mm,swift}'
        cs.exclude_files = 'Config.swift'

        cs.ios.dependency 'SwychCommon/Core'
        #   added CTX
        #spec.ios.dependency 'CTXCoreKit/Datastorage/CoreData'
        #spec.ios.dependency 'CTXCoreKit/Network/HTTP'
        spec.ios.dependency 'CTXCoreKit'
        #   added Mapping
        spec.ios.dependency 'ObjectMapper'
    end

    spec.ios.framework = 'Foundation', 'Intents'

end
