//
//  Config.swift
//  Swych
//
//  Created by Milan Horvatovic on 08/11/16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation

@objc(SCMConfig)
public class Config: NSObject {
    
    public static let classesPrefix: String = "SCM"
    
    public static let keychainGroupIdentifier: String = "com.creatix.enterprise.swych"
    public static let appGroupIdentifier: String = "group.com.creatix.enterprise.swych"
    
    public static let bundleIdentifier: String = Bundle.main.bundleIdentifier ?? "com.creatix.enterprise.swych"
    
    override private init() {
        super.init()
    }
    
    @objc(SCMConfigContacts)
    public class Contacts: NSObject {
        
        //DEV server
//        public static let serverBaseURL: String = "https://goswych-server.herokuapp.com/v1"
//        public static let apiKey: String = "7d783H1uDh7l69Qcmcz1jdI0Gh497GyZ"
//        public static let apiSecret: String = "SV6D[1GmKG8vGIK"
        
        
        //staging AWS server
        //        public static let serverBaseURL: String = "https://swychcontactserver-test.us-east-1.elasticbeanstalk.com"
        //        public static let apiKey: String = "0e6FIn3196usQO9597H9ZMfDoCu8HccX"
        //        public static let apiSecret: String = "lyrj!42%]J^`27O"
        
        //PROD AWS server
                public static let serverBaseURL: String = "https://contacts.swychbot.com/v1"
                public static let apiKey: String = "0e6FIn3196usQO9597H9ZMfDoCu8HccX"
                public static let apiSecret: String = "lyrj!42%]J^`27O"

        
        override private init() {
            super.init()
        }
        
    }
    
}
