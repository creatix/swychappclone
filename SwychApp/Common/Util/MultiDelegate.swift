//
//  MultiDelegate.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation

@objc(SCMMultiDelegate)
public final class MultiDelegate: NSObject {
    
    private let weakStorage = NSHashTable<AnyObject>.weakObjects()
    
    public func addDelegate<T: AnyObject>(delegate: T) {
        weakStorage.add(delegate)
    }
    
    public func removeDelegate<T: AnyObject>(delegate: T) {
        weakStorage.remove(delegate)
    }
    
    public func invoke<T: AnyObject>(closure:(T) -> ()) {
        for delegate in self.weakStorage.allObjects {
            guard let delegate = delegate as? T else { continue }
            closure(delegate)
        }
    }
    
}
