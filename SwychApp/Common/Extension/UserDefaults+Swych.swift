//
//  UserDefaults+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 08/11/16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation

public extension UserDefaults {
    
    /// Class var for easy access to standard UserDefaults instance.
    public class var swych_standard: UserDefaults {
        get { return UserDefaults.standard }
    }
    
    /// Class var for easy access to UserDefaults in AppGroup sandbox. It doesn't allowed to override that variable, so it has only public access control level.
    ///
    /// If AppGroup identifier isn't valid throw fatalError to inform developer about broken state.
    public class var swych_groupStandard: UserDefaults {
        get {
            guard let defaults = UserDefaults(suiteName: Config.appGroupIdentifier) else { fatalError("Can't create UserDefaults for AppGroup!") }
            return defaults
        }
    }
    
}
