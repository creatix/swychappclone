//
//  PersonNameComponents+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 18.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Intents

@available(iOS 9.0, *)
extension PersonNameComponents {
    
    public var swych_nameComponents: [String]? {
        get {
            var nameParts: [String] = [String]()
            
            if let namePrefix: String = self.namePrefix {
                nameParts.append(namePrefix)
            }
            if let givenName: String = self.givenName {
                nameParts.append(givenName)
            }
            if let middleName: String = self.middleName {
                nameParts.append(middleName)
            }
            if let familyName: String = self.familyName {
                nameParts.append(familyName)
            }
            if let nameSuffix: String = self.nameSuffix {
                nameParts.append(nameSuffix)
            }
            if let nickname: String = self.nickname {
                nameParts.append(nickname)
            }
            
            return nameParts.count > 0 ? nameParts : nil
        }
    }
    
}
