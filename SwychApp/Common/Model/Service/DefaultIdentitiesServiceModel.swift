//
//  DefaulIdentitiesServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import UIKit

import ObjectMapper

@objc(SCMDefaultIdentitiesServiceModel)
public class DefaultIdentitiesServiceModel: BaseServiceModel {
    
    internal(set) public var phone: PhoneServiceModel?
    internal(set) public var email: EmailServiceModel?
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)

        self.phone <- map["phones"]
        self.email <- map["emails"]
    }

}
