//
//  SearchResultServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import ObjectMapper

@objc(SCMSearchResultServiceModel)
public final class SearchResultServiceModel: BaseServiceModel {
    
    internal(set) public var users: [UserServiceModel]?
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)
        
        self.users <- map["data"]
    }
    
}
