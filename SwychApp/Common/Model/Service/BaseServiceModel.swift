//
//  BaseServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import ObjectMapper

@objc(SCMBaseServiceModel)
public class BaseServiceModel: NSObject, Mappable {
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    
    //  MARK: - Mapping: ObjectMapper
    public func mapping(map: Map) {
        
    }
    
}
