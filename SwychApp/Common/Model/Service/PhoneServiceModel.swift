//
//  PhoneServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import UIKit

import ObjectMapper

@objc(SCMPhoneServiceModel)
public class PhoneServiceModel: IdentityServiceModel {
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)
        //{
        //    "+971502411122": "Work"
        //}
        //self.value = map.JSON.keys.first
        //self.label = map.JSON.values.first as? String
    }
    
}
