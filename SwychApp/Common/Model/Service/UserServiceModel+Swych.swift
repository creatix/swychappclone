//
//  UserServiceModel+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 21.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Intents

//  MARK: - Intents
@available(iOSApplicationExtension 10.0, *)
extension UserServiceModel {
    
    @available(iOS 10.0, *)
    public var swych_inperson: INPerson? {
        get {
            var phonesArray: [String] = [String]()
            var emailsArray: [String] = [String]()
            if let defaults = self.defaults {
                if let object: IdentityServiceModel = defaults.phone, let value: String = object.value {
                    phonesArray.append(value)
                }
                if let object: IdentityServiceModel = defaults.email, let value: String = object.value {
                    emailsArray.append(value)
                }
            }
            if let values: [String] = self.phones?.flatMap({ $0.value }) {
                phonesArray.append(contentsOf: values)
            }
            if let values: [String] = self.emails?.flatMap({ $0.value }) {
                emailsArray.append(contentsOf: values)
            }
            
            guard phonesArray.count > 0 || emailsArray.count > 0 else { return nil }
            
            var personHandleOpt: INPersonHandle? = nil
            
            let phone: String? = phonesArray.first
            var phonesSet: Set<String> = Set<String>(phonesArray)
            if let value = phone {
                phonesSet.remove(value)
                personHandleOpt = INPersonHandle(value: value, type: INPersonHandleType.phoneNumber)
            }
            
            let email: String? = emailsArray.first
            var emailsSet: Set<String> = Set<String>(emailsArray)
            if let value = email {
                emailsSet.remove(value)
                if personHandleOpt == nil {
                    personHandleOpt = INPersonHandle(value: value, type: INPersonHandleType.emailAddress)
                }
            }
            
            guard var personHandle: INPersonHandle = personHandleOpt else { return nil }
            
            var nameComponents: PersonNameComponents = PersonNameComponents()
            var displayName: String = ""
            if let value = self.firstName {
                nameComponents.givenName = value
                displayName += " \(value)"
            }
            if let value = self.lastName {
                nameComponents.familyName = value
                displayName += " \(value)"
            }
            if let value = self.nickname {
                nameComponents.nickname = value
            }
            
            let aliases = phonesSet.flatMap({ INPersonHandle(value: $0, type: INPersonHandleType.phoneNumber) }) + emailsSet.flatMap({ INPersonHandle(value: $0, type: INPersonHandleType.emailAddress) })
            let identifier = UUID().uuidString

            return INPerson(handle: personHandle.value, nameComponents: nameComponents, displayName: displayName, image: nil, contactIdentifier: identifier)
            //return INPerson(personHandle: personHandle, nameComponents: nameComponents, displayName: displayName, image: nil, contactIdentifier: identifier, customIdentifier: identifier, aliases: aliases, suggestionType: INPersonSuggestionType.socialProfile)
        }
    }
    
}
