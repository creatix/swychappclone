//
//  UserServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import UIKit

import ObjectMapper

@objc(SCMUserServiceModel)
public class UserServiceModel: BaseServiceModel {

    internal(set) public var firstName: String?
    internal(set) public var lastName: String?
    internal(set) public var nickname: String?
    
    internal(set) public var defaults: DefaultIdentitiesServiceModel?
    
    internal(set) public var phones: [PhoneServiceModel]?
    internal(set) public var emails: [EmailServiceModel]?
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)
        
        self.firstName <- map["first_name"]
        self.lastName <- map["last_name"]
        self.nickname <- map["nick_name"]
        
        self.defaults <- map["defaults"]
        
        self.phones <- map["phones"]
        self.emails <- map["emails"]
    }
    
}
