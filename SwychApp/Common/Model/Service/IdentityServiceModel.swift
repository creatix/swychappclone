//
//  IdentityServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import UIKit

import ObjectMapper

@objc(SCMIdentityServiceModel)
public class IdentityServiceModel: BaseServiceModel {

    internal(set) public var value: String?
    internal(set) public var label: String?
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)
        
        self.value = map.JSON.keys.first
        self.label = map.JSON.values.first as? String
    }
    
}
