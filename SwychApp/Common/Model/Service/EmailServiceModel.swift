//
//  EmailServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.11.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import UIKit

import ObjectMapper

@objc(SCMEmailServiceModel)
public class EmailServiceModel: IdentityServiceModel {
    
    //  MARK: - Mapping: ObjectMapper
    override public func mapping(map: Map) {
        super.mapping(map: map)
        //{
        //    "jabc@thinkcreatix.com": "Work"
        //}
        //self.value = map.JSON.keys.first
        //self.label = map.JSON.values.first as? String
    }
    
}
