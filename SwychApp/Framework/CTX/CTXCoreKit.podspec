Pod::Spec.new do |spec|

    spec.name                       = 'CTXCoreKit'
    spec.version                    = '0.0.1'
    spec.homepage                   = 'http://www.thinkcreatix.com'
    spec.license                    = {
        :type => 'Copyright',
        :text => 'Copyright (C) 2014-2016 Creatix Inc. All Rights Reserved.'
    }
    spec.authors                    = {
        'Default' => 'ios@thinkcreatix.com',
        'Milan Horvatovic' => 'milan.horvatovic@thinkcreatix.com',
        'Juraj Homola' => 'juraj.homola@thinkcreatix.com'
    }
    spec.summary                    = 'Core Kit of extended functionality for iOS.'
    spec.source                     = {
        :path => './',
    }

    spec.module_name = 'CTXCoreKit'
    spec.platform = :ios, "8.0"
    spec.ios.deployment_target = "8.0"
    spec.requires_arc = true

    spec.vendored_frameworks = 'CTXCoreKit.framework'
    spec.source_files = 'CTXCoreKit.framework/Headers/*.h'
    spec.public_header_files = 'CTXCoreKit.framework/Headers/*.h'

    spec.ios.framework = 'Foundation'
    spec.ios.dependency 'KeychainAccess'
    spec.ios.dependency 'AlamofireDomain'

end
