//
//  ContactPhoneModel+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import libPhoneNumber_iOS
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l < r
    case (nil, _?): return true
    default: return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l > r
    default: return rhs < lhs
    }
}

extension ContactPhoneModel {
    
    var isValid: Bool {
        get { return self.numberFormatted?.characters.count > 0 }
    }
    
}
