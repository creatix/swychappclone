//
//  ContactEmailModel+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

extension ContactEmailModel {
    
    var isValid: Bool {
        get {
            guard let value: String = self.addressValid, value.characters.count > 0 else { return false }
            return true
        }
    }
    
}
