//
//  ContactModel+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 14.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import CryptoSwift
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l < r
    case (nil, _?): return true
    default: return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l > r
    default: return rhs < lhs
    }
}


extension ContactModel {
    
    var isValid: Bool {
        get { return self.name?.isValid ?? false && (self.emails?.reduce(true, { $1.isValid }) ?? false || self.phones?.reduce(true, { $1.isValid }) ?? false) }
    }
    
    var hashIdentifier: String {
        get {
            let identifier: String = self.identifier as? String ?? ""
            let name: String = "\(self.name?.firstName ?? ""):\(self.name?.middleName ?? ""):\(self.name?.lastName ?? "")"
            let emails: String = (self.emails != nil && self.emails!.count > 0) ? self.emails!.sorted( by: { $0.addressValid > $1.addressValid && $0.originalLabel > $1.originalLabel } ).map( { "\($0.originalLabel ?? ""):\($0.addressValid ?? "")" as String } ).joined(separator: "-") : ""
            let phones: String = (self.phones != nil && self.phones!.count > 0) ? self.phones!.sorted( by: { $0.numberFormatted > $1.numberFormatted && $0.originalLabel > $1.originalLabel } ).map( { "\($0.originalLabel ?? ""):\($0.numberFormatted ?? "")" as String } ).joined(separator: "-") : ""
            return "\(identifier)|\(name)|\(emails)|\(phones)".sha256()
        }
    }
    
}
