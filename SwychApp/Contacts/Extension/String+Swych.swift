//
//  String+Swych.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmailAddress() -> Bool {
        guard self.characters.count > 0 else { return false }
        
        return NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }

    func replaceCharacters(_ characters: String, toSeparator: String) -> String {
        let characterSet = CharacterSet(charactersIn: characters)
        let components = self.components(separatedBy: characterSet)
        let result = components.joined(separator: toSeparator)
        return result
    }
    
    func wipeCharacters(_ characters: String) -> String {
        return self.replaceCharacters(characters, toSeparator: "")
    }
    
    func normalizePhoneNumber() -> String {
        let characterSet = CharacterSet.decimalDigits.inverted
        let components = self.components(separatedBy: characterSet)
        let result = components.joined(separator: "")
        return result
    }

}

extension String {
    
    func stringByTrimmingLeadingAndTrailingContactLabel() -> String {
        let leadingAndTrailingWhitespacePattern = "(\\_\\$\\!\\<)|(\\>\\!\\$\\_)"
        
        do {
            let regex = try NSRegularExpression(pattern: leadingAndTrailingWhitespacePattern, options: .caseInsensitive)
            return regex.stringByReplacingMatches(in: self, options: .withTransparentBounds, range:NSMakeRange(0, self.characters.count), withTemplate:"")
        }
        catch {
            return self
        }
    }
    
}
