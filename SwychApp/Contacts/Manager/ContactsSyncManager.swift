//
//  ContactSyncManager.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import CoreData

import CTXCoreKit

import ObjectMapper

import SwychCommon

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l < r
    case (nil, _?): return true
    default: return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?): return l > r
    default: return rhs < lhs
    }
}

@objc(SCNContactsSyncDelegate)
public protocol ContactsSyncDelegate: NSObjectProtocol {
    
    func contactsSyncDidStart(_ contactsSync: ContactsSyncManager)
    func contactsSync(_ contactsSync: ContactsSyncManager, didFail state: ContactsSyncManager.State, error: Error?)
    func contactsSync(_ contactsSync: ContactsSyncManager, didFinish state: ContactsSyncManager.State)
}

@objc(SCNContactSyncManager)
public final class ContactsSyncManager: NSObject {
 
    @objc(GSContactsSyncState)
    public enum State: Int {
        
        case done
        case starting
        case started
        case importing
        case imported
        case checking
        case uploading
        case uploaded
        case updatingConfig
        case canceled
        
    }
    
    internal typealias ImportSuccessClosure = (_ active: Int, _ imported: Int, _ deleted: Int) -> ()
    internal typealias ImportFailureClosure = (_ error: NSError?) -> ()
    
    internal typealias UploadLocalContactsClosureDone = (_ success: Bool) -> ()
    
    fileprivate struct DefaultsKey {
        
        fileprivate struct Contact {
            
            fileprivate static let versionPrevious: String = "version-previous"
            fileprivate static let version: String = "version"
            fileprivate static let versionNext: String = "version-next"
            fileprivate static let syncTimestamp: String = "sync-timestamp"
            
        }
        
    }
    
    fileprivate static let contactDefaultsSuiteName: String = "\(Bundle.main.bundleIdentifier ?? "").application.contacts.sync"
    fileprivate static let contactDefaults: UserDefaults = UserDefaults.swych_groupStandard
    
    fileprivate static let batchSizeUpdate: Int = 100
    fileprivate static let versionThreshold: Int = 5
    
    public static let sharedInstance: ContactsSyncManager = ContactsSyncManager()
    
    private var _multicastDelegate: MultiDelegate
    
    public var enabledDEBUG: Bool = false
    
    fileprivate let _contactsManager: ContactsManager = ContactsManager()
    
    lazy fileprivate var _dispatchQueue: DispatchQueue = { return DispatchQueue(label: "\(Config.bundleIdentifier).contacts-manager.sync-queue", attributes: []) }()
    lazy fileprivate var _dispatchQueueConcurrent: DispatchQueue = { return DispatchQueue(label: "\(Config.bundleIdentifier).contacts-manager.sync-queue.concurrent", attributes: [DispatchQueue.Attributes.concurrent]) }()
    
    public var enableSending: Bool = false
    
    fileprivate(set) var state: State = .done
    fileprivate var _resync: Bool = false
    
    fileprivate var _timer: Timer?
    
    fileprivate var _minSynchronizationDelay: TimeInterval = 1 * 60 * 60
    var synchronizationDelay: TimeInterval {
        didSet {
            if synchronizationDelay == 0 || synchronizationDelay == TimeInterval.infinity {
                synchronizationDelay = _minSynchronizationDelay
            }
        }
    }
    
    fileprivate var _minSynchronizationInterval: TimeInterval = 1 * 60 * 60
    var synchronizationInterval: TimeInterval {
        didSet {
            if synchronizationInterval == 0 || synchronizationInterval == TimeInterval.infinity {
                synchronizationInterval = _minSynchronizationInterval
            }
        }
    }
    
    public var accessToken: String? {
        didSet { HTTPServiceManager.sharedInstance.updateAuthorization(accessToken ?? "") }
    }
    
    override fileprivate init() {
        self.synchronizationDelay = self._minSynchronizationDelay
        self.synchronizationInterval = self._minSynchronizationInterval
        self._multicastDelegate = MultiDelegate()
        
        super.init()
        
        self._contactsManager.externalChangesDispatchQueue = self._dispatchQueue
        self._contactsManager.externalChanges = { [weak self] in
            guard self?.state == .done else { self?._resync = true; return }
            
            self?.start()
        }
    }
    
    //  MARK: - Public
    public func start() {
        if self.state == .canceled { self.state = .done }
        
        if self.enabledDEBUG == true { print("START: \(Date().timeIntervalSince1970)") }
        
        self._start()
    }
    
    public func stop() {
        HTTPServiceManager.sharedInstance.cancel()
        self._destroyProcessTimer()
        self.state = .canceled
    }
    
    public func reset() {
        HTTPServiceManager.sharedInstance.cancel()
        self._destroyProcessTimer()
        type(of: self).removeContactObject(from: type(of: self).contactDefaults)
        CoreDataSQLiteManager.sharedInstance.removeSQLite()
        self.accessToken = nil
        self.state = .done
    }
    
    //  MARK: - Delegate
    public func add(delegate: ContactsSyncDelegate) {
        self._multicastDelegate.addDelegate(delegate: delegate)
    }
    
    public func remove(delegate: ContactsSyncDelegate) {
        self._multicastDelegate.removeDelegate(delegate: delegate)
    }
    
    //  MARK: - Private
    private func _start() {
        guard self.accessToken?.characters.count > 0 else { self._reportFailure(with: nil, at: .starting); return }
        
        guard self.state == .done else { self._resync = true; return }
        
        if self.enabledDEBUG == true { print("STARTED: \(Date().timeIntervalSince1970)") }
        
        self.state = .started
        
        self._multicastDelegate.invoke(closure: { [weak self] in ($0 as! ContactsSyncDelegate).contactsSyncDidStart(self!) })
        
        self._destroyProcessTimer()
        
        self._resync = false
        
        var version: Int = type(of: self).contactVersionNext() + 1
        
        self._importLocalContacts(version: version,
                                  //success(activeHashes.count, processedHashes.count, deletedHashes.count)
                                  success: { [weak self] (active, imported, deleted) in
                                    guard let selfStrong = self else { fatalError("Self instance was released!") }
                                    
                                    guard self?.state != .canceled else { return }
                                    
                                    guard self?.enableSending == true else { self?._finishSuccess(at: .imported); return }
                                    
                                    if imported > 0 || deleted > 0 {
                                        type(of: selfStrong).updateContactVersionNext(version)
                                    }
                                    else {
                                        let currentVersion: Int = type(of: selfStrong).contactVersion()
                                        version = currentVersion == 0 ? version : currentVersion
                                    }
                                    
                                    if imported > 0 || deleted > 0 {
                                        if self?.enabledDEBUG == true { print("UPDATE - UPDATING: \(Date().timeIntervalSince1970)") }
                                        self?._uploadLocalContacts(fullset: false, version: version)
                                        return
                                    }
                                    
                                    self?._checkUpdateContacts(version: version)
            },
                                  failure: { [weak self] (error) in
                                    guard self?.state != .canceled else { return }
                                    self?._finishFailure(with: error, at: .importing)
            }
        )
    }
    
    private func _importLocalContacts(version: Int, success: @escaping ImportSuccessClosure, failure: @escaping ImportFailureClosure) {
        self.state = .importing
        
        if self.enabledDEBUG == true { print("IMPORT - START: \(Date().timeIntervalSince1970)") }
        self._contactsManager.loadContacts({ [weak self] (contacts, error) in
            guard error == nil else { failure(error); return }
            
            guard self?.state != .canceled else { return }
            
            autoreleasepool {
                if self?.enabledDEBUG == true { print("IMPORT - LOADED: \(Date().timeIntervalSince1970)") }
                if self?.enabledDEBUG == true { print("IMPORT - TOTAL COUNT[\(contacts?.count ?? 0)]: \(Date().timeIntervalSince1970)") }
                
                guard let filtered: [ContactModel] = contacts?.filter({ $0.isValid }), filtered.count > 0 else { success(0, 0, 0); return }
                if self?.enabledDEBUG == true { print("IMPORT - FILTERED[\(filtered.count)]: \(Date().timeIntervalSince1970)") }
                
                guard let context: NSManagedObjectContext = CoreDataSQLiteManager.sharedInstance.createContext() else { failure(nil); return }
                
                let request: NSFetchRequest = CDContact.ctx_requestAllNotFaulted(in: context)
                request.propertiesToFetch = ["hashIdentifier"]
                let loadedHashes: Set<String> = Set<String>(CDContact.ctx_execute(dictionaryFetchRequest: request as! NSFetchRequest<NSFetchRequestResult>, in: context)?.flatMap({ $0["hashIdentifier"] as? String }) ?? [String]())
                var processedHashes: Set<String> = Set<String>()
                var activeHashes: Set<String> = Set<String>()
                
                for contact in filtered {
                    let hashIdentifier = contact.hashIdentifier
                    
                    activeHashes.insert(hashIdentifier)
                    
                    guard loadedHashes.contains(hashIdentifier) == false && processedHashes.contains(hashIdentifier) == false else { continue }
                    
                    guard let contactCD: CDContact = CDContact.ctx_createEntity(in: context) else { continue }
                    
                    processedHashes.insert(hashIdentifier)
                    
                    contactCD.hashIdentifier = hashIdentifier
                    contactCD.state = CDContact.State.imported
                    contactCD.firstName = contact.name?.firstName ?? ""
                    contactCD.middleName = contact.name?.middleName ?? ""
                    contactCD.lastName = contact.name?.lastName ?? ""
                    contactCD.nickname = contact.name?.nickname ?? ""
                    if let emails = contact.emails {
                        let minIndex = emails.flatMap( { $0.order } ).min() ?? 0
                        for email in emails {
                            guard let address = email.addressValid, address.characters.count > 0 else { continue }
                            guard let emailCD: CDContactIdentity = CDContactIdentity.ctx_createEntity(in: context) else { continue }
                            emailCD.type = CDContactIdentity.IdentityType.email
                            emailCD.label = email.originalLabel ?? email.localizedLabel
                            emailCD.value = email.addressValid
                            emailCD.contact = contactCD
                            emailCD.order = email.order
                            emailCD.primary = email.order == minIndex ? true : false
                        }
                    }
                    if let phones = contact.phones {
                        let minIndex = phones.flatMap( { $0.order } ).min() ?? 0
                        for phone in phones {
                            guard let number = phone.numberFormatted, number.characters.count > 0 else { continue }
                            guard let phoneCD: CDContactIdentity = CDContactIdentity.ctx_createEntity(in: context) else { continue }
                            phoneCD.type = CDContactIdentity.IdentityType.phone
                            phoneCD.label = phone.originalLabel ?? phone.localizedLabel
                            phoneCD.value = phone.numberFormatted
                            phoneCD.contact = contactCD
                            phoneCD.order = phone.order
                            phoneCD.primary = phone.order == minIndex ? true : false
                        }
                    }
                    contactCD.version = version
                }
                
                guard self?.state != .canceled else { return }
                
                if self?.enabledDEBUG == true { print("IMPORT - CREATED: \(Date().timeIntervalSince1970)") }
                CoreDataSQLiteManager.sharedInstance.saveContext(context, reset: true)
                if self?.enabledDEBUG == true { print("IMPORT - SAVED: \(Date().timeIntervalSince1970)") }
                
                let deletedHashes: Set<String> = loadedHashes.subtracting(activeHashes)
                if deletedHashes.count > 0 {
                    if let contacts: [CDContact] = CDContact.ctx_findAllNotFaulted(predicate: CDContact.fetchRequestForToFlagOnDelete(with: deletedHashes, in: context).predicate, in: context) {
                        for contact in contacts {
                            guard contact.isSynchronized == true else { context.delete(contact); continue }
                            contact.state = CDContact.State.onDelete
                        }
                        guard self?.state != .canceled else { return }
                        CoreDataSQLiteManager.sharedInstance.saveContext(context, reset: true)
                        if self?.enabledDEBUG == true { print("IMPORT - TO DELETE: \(Date().timeIntervalSince1970)") }
                    }
                }
                
                if self?.enabledDEBUG == true { print("IMPORT - DONE: \(Date().timeIntervalSince1970)") }
                
                success(activeHashes.count, processedHashes.count, deletedHashes.count)
            }
        }, dispatchQueue: self._dispatchQueue)
    }
    
    private func _checkUpdateContacts(version: Int) {
        self.state = .checking
        
        HTTPServiceManager.sharedInstance.contactSyncInformation(
            success: { [weak self] (info) in
                guard self?.state != .canceled else { return }
                
                guard let selfStrong = self else { fatalError("Self instance was released!") }
                
                guard let contactsVersion: Int = info?.contactsVersion else {
                    if self?.enabledDEBUG == true { print("UPDATE - UPDATING FULLSET: \(NSDate().timeIntervalSince1970)") }
                    self?._uploadLocalContacts(fullset: true, version: version); return
                }
                let diff: Int = abs(info!.contactsVersion! - version)
                
                if diff >= type(of: selfStrong).versionThreshold {
                    if self?.enabledDEBUG == true { print("UPDATE - UPDATING FULLSET: \(NSDate().timeIntervalSince1970)") }
                    self?._uploadLocalContacts(fullset: true, version: version)
                }
                else if diff > 0 && diff < type(of: selfStrong).versionThreshold {
                    if self?.enabledDEBUG == true { print("UPDATE - UPDATING CHANGES: \(NSDate().timeIntervalSince1970)") }
                    self?._uploadLocalContacts(fullset: false, version: version)
                }
                else {
                    if self?.enabledDEBUG == true { print("UPDATE - NOTHING TO UPDATE: \(NSDate().timeIntervalSince1970)") }
                    self?._updateContactsSyncTimeout()
                }
            },
            failure: { [weak self] (error) in
                guard self?.state != .canceled else { return }
                if self?.enabledDEBUG == true { print("LOAD SYNC INFO - FAIL: \(NSDate().timeIntervalSince1970) \n\tERROR: \(error?.description ?? "")") }
                self?._finishFailure(with: error, at: .imported)
        })
    }
    
    fileprivate func _uploadLocalContacts(fullset: Bool = false, version: Int) {
        self.state = .uploading
        
        self._dispatchQueue.async {
            if self.enabledDEBUG == true { print("SEND - START: \(Date().timeIntervalSince1970)") }
            guard let context: NSManagedObjectContext = CoreDataSQLiteManager.sharedInstance.createContext() else { self._finishUploadLocalContacts(success: false, version: version); return }
            
            let predicate: NSPredicate? = fullset == true ? nil : CDContact.fetchRequestToSend(in: context).predicate
            let countToSend = CDContact.ctx_countOfEntities(predicate: predicate, in: context)
            if self.enabledDEBUG == true { print("SEND - COUNT TO SEND[\(countToSend)]: \(Date().timeIntervalSince1970)") }
            guard countToSend > 0 else { self._finishUploadLocalContacts(success: true, version: version); return }
            
            let semaphore = DispatchSemaphore(value: 1)
            var responses: [Int: Bool] = [Int: Bool]()
            let batches = Int(ceil(Double(countToSend) / Double(type(of: self).batchSizeUpdate)))
            
            let dispatchGroup = DispatchGroup()
            DispatchQueue.concurrentPerform(iterations: batches, execute: { [weak self] (iteration) in
                autoreleasepool {
                    guard let selfStrong = self else { fatalError("Self instance was released!") }
                    
                    dispatchGroup.enter()
                    
                    guard let context: NSManagedObjectContext = CoreDataSQLiteManager.sharedInstance.createContext() else {
                        semaphore.wait()
                        responses[iteration] = false
                        semaphore.signal()
                        dispatchGroup.leave()
                        return
                    }
                    
                    guard let contacts: [CDContact] = CDContact.ctx_findAll(
                        predicate: predicate,
                        sortedBy: "hashIdentifier",
                        ascending: true,
                        limit: type(of: selfStrong).batchSizeUpdate,
                        offset: iteration * type(of: selfStrong).batchSizeUpdate,
                        in: context) else {
                            dispatchGroup.leave()
                            return
                    }
                    
                    if self?.enabledDEBUG == true { print("SEND[\(iteration)] - FETCH: \(NSDate().timeIntervalSince1970)") }
                    guard self?.state != .canceled else { dispatchGroup.leave(); return }
                    
                    let hashIdentifiers: [String] = contacts.flatMap({ $0.hashIdentifier })
                    HTTPServiceManager.sharedInstance.sendContacts(contacts,
                        success: { [weak self] (success) in
                            defer { dispatchGroup.leave() }
                            guard self?.state != .canceled else { return }
                            if self?.enabledDEBUG == true { print("SEND[\(iteration)] - SENT: \(NSDate().timeIntervalSince1970)") }
                            
                            semaphore.wait()
                            responses[iteration] = success
                            semaphore.signal()
                            guard success == true else { return }
                            
                            guard let context: NSManagedObjectContext = CoreDataSQLiteManager.sharedInstance.createContext() else { return }
                            
                            if let contacts: [CDContact] = CDContact.ctx_findAll(predicate: NSPredicate(format: "hashIdentifier IN %@", hashIdentifiers), in: context) {
                                guard let selfStrong = self else { fatalError("Self instance was released!") }
                                for contact in contacts {
                                    guard contact.state != CDContact.State.onDelete else { contact.ctx_deleteEntity(in: context); continue }
                                    contact.state = CDContact.State.synced
                                    contact.version = version
                                    contact.isSynchronized = true
                                }
                                guard self?.state != .canceled else { return }
                                CoreDataSQLiteManager.sharedInstance.saveContext(context)
                                if self?.enabledDEBUG == true { print("SEND[\(iteration)] - FLAGING: \(NSDate().timeIntervalSince1970)") }
                            }
                        },
                        failure: { [weak self] (error) in
                            defer { dispatchGroup.leave() }
                            guard self?.state != .canceled else { return }
                            if self?.enabledDEBUG == true { print("SEND[\(iteration)] - FAIL: \(NSDate().timeIntervalSince1970) \n\tERROR:\(error?.description ?? "")") }
                            
                            self?._reportFailure(with: error, at: .uploading)
                            
                            semaphore.wait()
                            responses[iteration] = false
                            semaphore.signal()
                    })
                }
            })
            
            dispatchGroup.notify(queue: self._dispatchQueue, execute: { [weak self] in
                if self?.enabledDEBUG == true { print("SEND - DONE: \(Date().timeIntervalSince1970)") }
                guard self?.state != .canceled else { return }
                self?.state = .uploaded
                self?._finishUploadLocalContacts(success: responses.values.contains(false) == true ? false : true, version: version)
            })
        }
    }
 
    private func _finishUploadLocalContacts(success: Bool, version: Int) {
        guard self.state != .canceled else { return }
        
        guard success == true else { self._finishFailure(with: nil, at: .uploading); return }
        
        self._updateContact(version: version)
    }
    
    private func _updateContact(version: Int) {
        self.state = .updatingConfig
        
        HTTPServiceManager.sharedInstance.updateContactVersion(version,
                                                               success: { [weak self] (success) in
                                                                guard let selfStrong = self else { fatalError("Self instance was released!") }
                                                                guard self?.state != .canceled else { return }
                                                                
                                                                if self?.enabledDEBUG == true { print("UDPATE VERSION: \(NSDate().timeIntervalSince1970)") }
                                                                
                                                                guard let context: NSManagedObjectContext = CoreDataSQLiteManager.sharedInstance.createContext() else { self?._finishFailure(with: nil, at: .updatingConfig); return }
                                                                
                                                                if let contacts: [CDContact] = CDContact.ctx_findAll(predicate: CDContact.fetchRequestForOlder(then: version, in: context).predicate, in: context) {
                                                                    for contact in contacts {
                                                                        contact.version = version
                                                                    }
                                                                    
                                                                    guard self?.state != .canceled else { return }
                                                                    CoreDataSQLiteManager.sharedInstance.saveContext(context)
                                                                }
                                                                type(of: selfStrong).updateSyncTimestamp(Date())
                                                                type(of: selfStrong).updateContactVersion(version)
                                                                type(of: selfStrong).checkContactVersionNext(version)
                                                                self?._finishSuccess(at: .updatingConfig)
            },
                                                               failure: { [weak self] (error) in
                                                                guard self?.state != .canceled else { return }
                                                                if self?.enabledDEBUG == true { print("UPDATE VERSION - FAIL: \(NSDate().timeIntervalSince1970) \n\tERROR: \(error?.description ?? "")") }
                                                                self?._finishFailure(with: error, at: .updatingConfig)
        })
        
    }
    
    private func _updateContactsSyncTimeout() {
        self.state = .updatingConfig
        
        HTTPServiceManager.sharedInstance.refreshContactSyncTimeout(
            success: { [weak self] (date) in
                guard self?.state != .canceled else { return }
                if self?.enabledDEBUG == true { print("UDPATE SYNC TIMEOUT: \(NSDate().timeIntervalSince1970)") }
                self?._finishSuccess(at: .updatingConfig)
            },
            failure: { [weak self] (error) in
                guard self?.state != .canceled else { return }
                if self?.enabledDEBUG == true { print("UPDATE SYNC TIMEOUT - FAIL: \(NSDate().timeIntervalSince1970) \n\tERROR: \(error?.description ?? "")") }
                self?._finishFailure(with: error, at: .updatingConfig)
        })
    }
    
    private func _finishSuccess(at state: State) {
        self.state = .done
        self._recreateProcessTimer()
        if self._resync == true { self._start() }
        self._reportSuccess(at: state)
    }
    
    private func _finishFailure(with error: Error?, at state: State) {
        self.state = .done
        self._recreateProcessTimer()
        if self._resync == true { self._start() }
        self._reportFailure(with: error, at: state)
    }
    
    //  MARK: - Report
    private func _reportSuccess(at state: State) {
        if self.enabledDEBUG == true { print("SYNC DONE: \(NSDate().timeIntervalSince1970) FROM: \(state.rawValue)") }
        self._multicastDelegate.invoke(closure: { [weak self] in ($0 as! ContactsSyncDelegate).contactsSync(self!, didFinish: state) })
    }
    
    private func _reportFailure(with error: Error?, at state: State) {
        self._multicastDelegate.invoke(closure: { [weak self] in ($0 as! ContactsSyncDelegate).contactsSync(self!, didFail: state, error: error) })
    }
    
    //  MARK: - Timer
    fileprivate func _recreateProcessTimer() {
        self._destroyProcessTimer()
        
        self._timer = Timer(timeInterval: self.synchronizationInterval, target: self, selector: #selector(type(of: self)._fireProcessTimer), userInfo: nil, repeats: false)
    }
    
    fileprivate func _destroyProcessTimer() {
        guard self._timer != nil else { return }
        
        self._timer?.invalidate()
        self._timer = nil
    }
    
    @objc fileprivate func _fireProcessTimer() {
        self._start()
    }
    
}

fileprivate extension ContactsSyncManager {
    
    private class func _contactObject() -> [String: Any] { return self._contactObject(from: self.contactDefaults) }
    
    private class func _contactObject(from userDefaults: UserDefaults) -> [String: Any] {
        var object: [String: Any]? = userDefaults.object(forKey: self.contactDefaultsSuiteName) as? [String: Any]
        if object == nil {
            object = [String: Any]()
            userDefaults.set(object, forKey: self.contactDefaultsSuiteName)
            userDefaults.synchronize()
        }
        return object!
    }
    
    private class func _update(contactObject object: [String: Any], in userDefaults: UserDefaults) {
        userDefaults.set(object, forKey: self.contactDefaultsSuiteName)
        userDefaults.synchronize()
    }
    
    fileprivate class func removeContactObject(from userDefaults: UserDefaults) {
        userDefaults.removeObject(forKey: self.contactDefaultsSuiteName)
        userDefaults.synchronize()
    }
    
    fileprivate class func contactVersion() -> Int { return (self._contactObject()[self.DefaultsKey.Contact.version] as? Int) ?? 0 }
    
    fileprivate class func updateContactVersion(_ version: Int) {
        let defaults = self.contactDefaults
        var object = self._contactObject(from: defaults)
        //guard version < object[self.DefaultsKey.Contact.version] as? Int else { return }
        object[self.DefaultsKey.Contact.version] = version
        self._update(contactObject: object, in: defaults)
    }
    
    fileprivate class func contactVersionNext() -> Int {
        guard let value: Int = self._contactObject()[self.DefaultsKey.Contact.versionNext] as? Int else { return Int(Date().timeIntervalSince1970) }
        return value
    }
    
    fileprivate class func updateContactVersionNext(_ version: Int) {
        let defaults = self.contactDefaults
        var object = self._contactObject(from: defaults)
        //guard version > object[self.DefaultsKey.Contact.versionNext] as? Int else { return }
        object[self.DefaultsKey.Contact.versionNext] = version
        self._update(contactObject: object, in: defaults)
    }
    
    fileprivate class func checkContactVersionNext(_ version: Int) {
        let defaults = self.contactDefaults
        var object = self._contactObject(from: defaults)
        if object[self.DefaultsKey.Contact.versionNext] == nil {
            object[self.DefaultsKey.Contact.versionNext] = version
            self._update(contactObject: object, in: defaults)
        }
    }
    
    fileprivate class func syncTimestamp() -> Date? { return self.contactDefaults.object(forKey: self.DefaultsKey.Contact.syncTimestamp) as? Date }
    
    fileprivate class func updateSyncTimestamp(_ timestamp: Date) {
        let defaults = self.contactDefaults
        var object = self._contactObject(from: defaults)
        object[self.DefaultsKey.Contact.syncTimestamp] = timestamp
        self._update(contactObject: object, in: defaults)
    }
    
}
