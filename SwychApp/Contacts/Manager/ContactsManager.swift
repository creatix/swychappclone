//
//  ContactsManager.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import Contacts
import AddressBook

@objc(SCNContactsManager)
public final class ContactsManager: NSObject {
    
    public typealias LoadContactsClosure = (_ contacts: [ContactModel]?, _ error: NSError?) -> ()
    public typealias RequestAccessClosure = (_ granted: Bool, _ error: NSError?) -> ()
    
    fileprivate var _access: ContactAccessRoutine?
    fileprivate var _reader: ContactReadRoutine?
    fileprivate let _thread: ContactThread = ContactThread()
    fileprivate var _changes: ContactObserverRoutine?
    
    var externalChanges: (() -> ())?
    var externalChangesDispatchQueue: DispatchQueue?
    
    class var authorizationStatus: ContactsAuthorizationStatus {
        get { return ContactAccessRoutine.authorizationStatus }
    }
    
    class var isAuthorizatedAccess: Bool {
        get { return ContactAccessRoutine.isAuthorizatedAccess }
    }
    
    override public init() {
        super.init()
        
        self._thread.start()
        self._thread.dispatchAsync { [weak self] in
            let contactWrapper = ContactWrapper()
            self?._access = ContactAccessRoutine(contactWrapper: contactWrapper)
            self?._reader = ContactReadRoutine(contactWrapper: contactWrapper)
            self?._changes = ContactObserverRoutine(contactWrapper: contactWrapper)
            
            self?._changes?.delegate = self
        }
    }
    
    //  MARK: - Public
    //  MARK: Access
    open func requestAccess(_ closure: @escaping RequestAccessClosure) {
        self.requestAccess(closure, dispatchQueue: DispatchQueue.main)
    }
    
    open func requestAccess(_ closure: @escaping RequestAccessClosure, dispatchQueue: DispatchQueue) {
        self._thread.dispatchAsync { [weak self] in
            self?._access?.requestAccess({ (granted, error) in
                dispatchQueue.async(execute: { 
                    closure(granted, error)
                })
            })
        }
    }
    
    //  MARK: Load
    open func loadContacts(_ closure: @escaping LoadContactsClosure) {
        self.loadContacts(closure, dispatchQueue: DispatchQueue.main)
    }
    
    open func loadContacts(_ closure: @escaping LoadContactsClosure, dispatchQueue: DispatchQueue) {
        self._thread.dispatchAsync { [weak self] in
            self?._access?.requestAccess({ [weak self] (granted, error) in
                guard granted == true else {
                    dispatchQueue.async(execute: { 
                        closure(nil, error)
                    })
                    return
                }
                
                self?._thread.dispatchAsync({ [weak self] in
                    guard let contacts: [ContactModel] = self?._reader?.allContacts() else {
                        dispatchQueue.async(execute: {
                            closure(nil, nil)
                        })
                        return
                    }
                    dispatchQueue.async(execute: {
                        closure(contacts, nil)
                    })
                })
            })
        }
    }
    
}

extension ContactsManager: GSContactObserverChangesDelegate {
    
    func contactsDidChanged() {
        guard let closure = self.externalChanges else { return }
        
        (self.externalChangesDispatchQueue ?? DispatchQueue.main).async(execute: closure)
    }
    
}
