Pod::Spec.new do |spec|

    spec.name                       = 'SwychContacts'
    spec.version                    = '1.0.0'
    spec.homepage                   = 'http://goswych.com'
    spec.license                    = {
        :type => 'Copyright',
        :text => 'Copyright (C) 2014-2016 Creatix Inc. All Rights Reserved.'
    }
    spec.authors                    = {
        'Default' => 'ios@thinkcreatix.com',
        'Milan Horvatovic' => 'milan.horvatovic@thinkcreatix.com',
        'Juraj Homola' => 'juraj.homola@thinkcreatix.com'
    }
    spec.summary                    = 'Common parts of Swych platform.'
    spec.source                     = {
        :path => './',
    }

    spec.module_name = 'SwychContacts'
    spec.platform = :ios, "8.0"
    spec.ios.deployment_target = "8.0"
    spec.requires_arc = true

    spec.source_files = '**/*.{h,m,mm,swift}'

    spec.resource_bundles = {
        'ContactModels' => ['Model/CoreData/**/*.xcdatamodeld']
    }

    spec.ios.framework = 'Foundation'

    #   added local dependencies
    spec.ios.dependency 'SwychCommon/Core'
    #   added CTX
    #spec.ios.dependency 'CTXCoreKit/Datastorage/CoreData'
    #spec.ios.dependency 'CTXCoreKit/Network/HTTP'
    spec.ios.dependency 'CTXCoreKit'
    #   added Mapping
    spec.ios.dependency 'ObjectMapper'
    # added phone numbers
    spec.ios.dependency 'libPhoneNumber-iOS'
    # added Crypto for Swift
    spec.ios.dependency 'CryptoSwift'

end
