//
//  SyncInformationServiceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import ObjectMapper

@objc(SCNSyncInformationServiceModel)
class SyncInformationServiceModel: BaseServiceModel {
    
    var userID: String?
    var deviceID: String?
    var contactsVersion: Int?
    
    //  MARK: - Mapping: ObjectMapper
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        self.userID <- map["user_id"]
        self.deviceID <- map["device_id"]
        self.contactsVersion <- (map["contacts_version"], IntTransform())
    }
    
}
