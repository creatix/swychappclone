//
//  IntTransform.swift
//  Swych
//
//  Created by Milan Horvatovic on 07.12.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import ObjectMapper

open class IntTransform: TransformType {
    public typealias Object = Int
    public typealias JSON = String
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Object? {
        if let int = value as? Int {
            return int
        }
        else if let string = value as? String {
            return Int(string)
        }
        return nil
    }
    
    open func transformToJSON(_ value: Object?) -> JSON? {
        guard let value = value else { return nil }
        return "\(value)"
    }
    
}
