//
//  SCNCDContact+CoreDataProperties.swift
//  Swych
//
//  Created by Milan Horvatovic on 07.12.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation
import CoreData


extension SCNCDContact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDContact> { return NSFetchRequest<CDContact>(entityName: "Contact") }
    
    @nonobjc public class func fetchRequestForOlder(then version: Int, in context: NSManagedObjectContext) -> NSFetchRequest<CDContact> {
        let request: NSFetchRequest<CDContact> = self.ctx_createFetchRequest(in: context)
        request.predicate = NSPredicate(format: "versionValue <= %i", version)
        return request
    }
    
    @nonobjc public class func fetchRequestForToFlagOnDelete(with identifiers: Set<String>, in context: NSManagedObjectContext) -> NSFetchRequest<CDContact> {
        let request: NSFetchRequest<CDContact> = self.ctx_createFetchRequest(in: context)
        //request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "stateValue != %i", CDContact.State.onDelete.rawValue), NSPredicate(format: "hashIdentifier IN %@", except)])
        request.predicate = NSPredicate(format: "hashIdentifier IN %@", identifiers)
        return request
    }
    
    @nonobjc public class func fetchRequestToSend(in context: NSManagedObjectContext) -> NSFetchRequest<CDContact> {
        let request: NSFetchRequest<CDContact> = self.ctx_createFetchRequest(in: context)
        request.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [
            NSPredicate(format: "stateValue == %@", NSNumber(value: CDContact.State.imported.rawValue as Int)),
            NSPredicate(format: "stateValue == %@", NSNumber(value: CDContact.State.onDelete.rawValue as Int))]
        )
        return request
    }

    @NSManaged public var birthday: NSDate?
    @NSManaged public var firstName: String?
    @NSManaged public var hashIdentifier: String?
    @NSManaged public var identifier: String?
    @NSManaged public var lastName: String?
    @NSManaged public var middleName: String?
    @NSManaged public var nickname: String?
    @NSManaged internal var stateValue: Int16
    @NSManaged internal var versionValue: Int64
    @NSManaged public var isSynchronized: Bool
    @NSManaged public var identities: NSSet?

}

// MARK: Generated accessors for identities
extension SCNCDContact {

    @objc(addIdentitiesObject:)
    @NSManaged public func addToIdentities(_ value: SCNCDContactIdentity)

    @objc(removeIdentitiesObject:)
    @NSManaged public func removeFromIdentities(_ value: SCNCDContactIdentity)

    @objc(addIdentities:)
    @NSManaged public func addToIdentities(_ values: NSSet)

    @objc(removeIdentities:)
    @NSManaged public func removeFromIdentities(_ values: NSSet)

}
