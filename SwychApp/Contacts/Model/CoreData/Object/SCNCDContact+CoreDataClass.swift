//
//  SCNCDContact+CoreDataClass.swift
//  Swych
//
//  Created by Milan Horvatovic on 07.12.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation
import CoreData

import CTXCoreKit
import ObjectMapper

public typealias CDContact = SCNCDContact

@objc(SCNCDContact)
public class SCNCDContact: NSManagedObject, Mappable {
    
    @objc(SCNCDContactState)
    public enum State: Int {
        
        case unknown
        case importing
        case imported
        case synced
        case onDelete
        
    }
    
    override public init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    required public init?(map: Map) {
        let context = CoreDataSQLiteManager.sharedInstance.mainObjectContext!
        super.init(entity: type(of: self).ctx_entityDescription(in: context), insertInto: context)
    }
    
    open func mapping(map: Map) {
        self.firstName <- map["first_name"]
        self.lastName <- map["last_name"]
        self.nickname <- map["nick_name"]
        
        self.version <- map["last_version"]
        self.hashIdentifier <- map["cuid"]
        
        self.state <- (map["delete"], TransformOf<State, Bool>(fromJSON: { _ in State.unknown }, toJSON: { $0 == State.onDelete ? true : false }))
        
        if let identities = self.identities as? Set<CDContactIdentity> {
            var emails = identities.filter( { $0.type == CDContactIdentity.IdentityType.email } )
            emails <- map["contact_data.email"]
            var phones = identities.filter( { $0.type == CDContactIdentity.IdentityType.phone } )
            phones <- map["contact_data.phone"]
        }
    }
    
}

public extension SCNCDContact {
    
    public var state: State {
        get { return State(rawValue: Int(self.stateValue))! }
        set { self.stateValue = Int16(newValue.rawValue) }
    }
    
    public var version: Int {
        get { return Int(self.versionValue) }
        set { self.versionValue = Int64(newValue) }
    }
    
}
