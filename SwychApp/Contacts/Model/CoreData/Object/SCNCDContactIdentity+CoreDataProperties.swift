//
//  SCNCDContactIdentity+CoreDataProperties.swift
//  Swych
//
//  Created by Milan Horvatovic on 07.12.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation
import CoreData


extension SCNCDContactIdentity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SCNCDContactIdentity> { return NSFetchRequest<SCNCDContactIdentity>(entityName: "ContactIdentity") }

    @NSManaged public var label: String?
    @NSManaged internal var orderValue: Int16
    @NSManaged public var primary: Bool
    @NSManaged internal var typeValue: Int16
    @NSManaged public var value: String?
    @NSManaged public var contact: SCNCDContact?

}
