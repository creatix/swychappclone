//
//  SCNCDContactIdentity+CoreDataClass.swift
//  Swych
//
//  Created by Milan Horvatovic on 07.12.16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

import Foundation
import CoreData

import CTXCoreKit
import ObjectMapper

public typealias CDContactIdentity = SCNCDContactIdentity

@objc(SCNCDContactIdentity)
public class SCNCDContactIdentity: NSManagedObject, Mappable {
    
    @objc(SCNCDContactIdentityType)
    public enum IdentityType: Int {
        
        case unknown
        case phone
        case email
        
    }
    
    override public init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    required public init?(map: Map) {
        let context = CoreDataSQLiteManager.sharedInstance.mainObjectContext!
        super.init(entity: type(of: self).ctx_entityDescription(in: context), insertInto: context)
    }
    
    open func mapping(map: Map) {
        self.label <- map["label"]
        self.value <- map["value"]
        //self.primary <- (map["default"], TransformOf<NSNumber, Bool>(fromJSON: { _ in return nil }, toJSON: { $0!.boolValue } ))
        self.primary <- map["default"]
    }
    
}

public extension SCNCDContactIdentity {
    
    public var order: Int {
        get { return Int(self.orderValue) }
        set { self.orderValue = Int16(newValue) }
    }
    
    public var type: IdentityType {
        get { return IdentityType(rawValue: Int(self.typeValue))! }
        set { self.typeValue = Int16(newValue.rawValue) }
    }
    
}
