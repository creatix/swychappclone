//
//  SwychContacts.h
//  Swych
//
//  Created by Milan Horvatovic on 08/11/16.
//  Copyright © 2016 Creatic, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SwychCommon.
FOUNDATION_EXPORT double SwychCommonVersionNumber;

//! Project version string for SwychCommon.
FOUNDATION_EXPORT const unsigned char SwychCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwychCommon/PublicHeader.h>
