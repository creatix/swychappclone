//
//  Config.swift
//  Swych
//
//  Created by Milan Horvatovic on 11.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import SwychCommon

@objc(SCNConfig)
public class Config: NSObject {
    
    public static let classesPrefix: String = "SCN"
    
    public static let serverBaseURL: String = SwychCommon.Config.Contacts.serverBaseURL
    public static let apiKey: String = SwychCommon.Config.Contacts.apiKey
    public static let apiSecret: String = SwychCommon.Config.Contacts.apiSecret
    
    public static let bundleIdentifier: String = Bundle.main.bundleIdentifier ?? SwychCommon.Config.bundleIdentifier
    
    public static let defaultMobileRegion = "US"
    
    @objc(SCNConfigDateFormatter)
    public class DateFormatter: NSObject {
        
        public static let api: Foundation.DateFormatter = {
            let dateFormatter = Foundation.DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            return dateFormatter
        }()
        
        override private init() {
            super.init()
        }
        
    }
    
    override private init() {
        super.init()
    }
    
}
