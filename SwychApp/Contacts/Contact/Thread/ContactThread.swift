//
//  ContactThread.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactThread)
internal class ContactThread: Thread {

    typealias ThreadClosure = () -> ()
    
    //  MARK: - Public
    func dispatchAsync(_ closure: @escaping ThreadClosure) {
        self.perform(#selector(ContactThread._performClosure(_:)), on: self, with: ClosureWrapper(closure), waitUntilDone: false)
    }
    
    func dispatchSync(_ closure: @escaping ThreadClosure) {
        self.perform(#selector(ContactThread._performClosure(_:)), on: self, with: ClosureWrapper(closure), waitUntilDone: true)
    }
    
    //  MARK: - Internal
    override internal func main() {
        while self.isCancelled == false {
            autoreleasepool(invoking: { 
                RunLoop.current.run(mode: RunLoopMode.defaultRunLoopMode, before: Date.distantFuture)
            })
        }
    }
    
    //  MARK: - Private
    @objc fileprivate func _performClosure(_ closure: ClosureWrapper) {
        closure.closure()
    }
    
}
