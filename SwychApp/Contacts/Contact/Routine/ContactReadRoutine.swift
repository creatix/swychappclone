//
//  ContactReadRoutine.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import AddressBook
import Contacts

@objc(SCNContactReadRoutine)
internal class ContactReadRoutine: ContactBaseRoutine {

    fileprivate let _dataExtractor: ContactDataExtractor = ContactDataExtractor()
    
    func allContacts() -> [ContactModel]? {
        
        if #available(iOS 9, *) {
            guard let contactStore = self.wrapper.contactStore else {
                return nil
            }
            
            let keysToFetch = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                CNContactNicknameKey,
                CNContactEmailAddressesKey,
                CNContactPhoneNumbersKey,
                CNContactJobTitleKey,
                CNContactImageDataAvailableKey,
                CNContactThumbnailImageDataKey
            ] as [Any]
            
            // Get all the containers
            var allContainers: [CNContainer] = []
            do {
                allContainers = try contactStore.containers(matching: nil)
            }
            catch let error {
                print("ERROR fetching containers: \(error)")
                return nil
            }
            
            var results: [CNContact] = []
            // Iterate all containers and append their contacts to our results array
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                do {
                    let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                    results.append(contentsOf: containerResults)
                }
                catch let error {
                    print("ERROR fetching results for container: \(error)")
                    return nil
                }
            }
            
            return results.flatMap({ (record) -> ContactModel? in
                if let contact = self._dataExtractor.contact(record) {
                    return contact
                }
                return nil
            })
        }
        else {
            guard let addressBook = self.wrapper.addressBook else {
                return nil
            }
            
            guard let contactList: [ABRecord] = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as [ABRecord] else {
                return nil
            }
            
            var contacts: [ContactModel] = [ContactModel]()
            for record: ABRecord in contactList {
                if let contact = self._dataExtractor.contact(record) {
                    contacts.append(contact)
                }
            }
            return contacts
        }
    }
    
    /*
    func contact(identifier: String) -> GSContactModel? {
        return nil
    }
     */
    
}
