//
//  ContactObserverRoutine.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import AddressBook
import Contacts

internal protocol GSContactObserverChangesDelegate {
    
    func contactsDidChanged()
    
}

@objc(SCNContactObserverRoutine)
internal class ContactObserverRoutine: ContactBaseRoutine {
    
    lazy fileprivate var _changeCallback: ABExternalChangeCallback = { (addressBook, info, pointer) in
        guard let pointer = pointer else { return }
        let instance = Unmanaged<ContactObserverRoutine>.fromOpaque(pointer).takeUnretainedValue()
        guard let delegate = instance.delegate else { return }
        
        delegate.contactsDidChanged()
    }
    lazy fileprivate var _changeClosure: (Notification) -> Void = { [weak self] (notification) in
        guard let delegate = self?.delegate else { return }
        
        delegate.contactsDidChanged()
    }
    fileprivate var _observer: NSObjectProtocol?
    
    var delegate: GSContactObserverChangesDelegate?
    
    override init(contactWrapper: ContactWrapper) {
        super.init(contactWrapper: contactWrapper)
        
        if #available(iOS 9, *) {
            guard let _ = self.wrapper.contactStore else {
                return
            }
            
            self._observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.CNContactStoreDidChange, object: nil, queue: nil, using: self._changeClosure)
        }
        else {
            guard let addressBook = self.wrapper.addressBook else {
                return
            }
            
            ABAddressBookRegisterExternalChangeCallback(addressBook, self._changeCallback, UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()))
        }
    }
    
    deinit {
        if #available(iOS 9, *) {
            guard let _ = self.wrapper.contactStore else {
                return
            }
            
            guard let observer = self._observer else {
                return
            }
            
            NotificationCenter.default.removeObserver(observer)
        }
        else {
            guard let addressBook = self.wrapper.addressBook else {
                return
            }
            
            ABAddressBookUnregisterExternalChangeCallback(addressBook, self._changeCallback, UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()))
        }
    }
    
}
