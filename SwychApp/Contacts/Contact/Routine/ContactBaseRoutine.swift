//
//  ContactBaseRoutine.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactBaseRoutine)
internal class ContactBaseRoutine: NSObject {

    let wrapper: ContactWrapper
    
    init(contactWrapper: ContactWrapper) {
        self.wrapper = contactWrapper
    }
    
}
