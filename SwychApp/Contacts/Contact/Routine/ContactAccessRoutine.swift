//
//  ContactAccessRoutine.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import AddressBook
import Contacts

@objc(SCNContactsAuthorizationStatus)
public enum ContactsAuthorizationStatus: Int {
    
    case unknown
    case granted
    case denied
    
    @available(iOS 8, *)
    internal init(authorizationStatus: ABAuthorizationStatus) {
        switch authorizationStatus {
        case .authorized: self = .granted
        case .denied: self = .denied
        case .restricted: self = .denied
        case .notDetermined: self = .unknown
        }
    }
    
    @available(iOS 9, *)
    internal init(authorizationStatus: CNAuthorizationStatus) {
        switch authorizationStatus {
        case .authorized: self = .granted
        case .denied: self = .denied
        case .restricted: self = .denied
        case .notDetermined: self = .unknown
        }
    }
    
}

@objc(SCNContactAccessRoutine)
internal class ContactAccessRoutine: ContactBaseRoutine {

    typealias RequestClosure = (_ granted: Bool, _ error: NSError?) -> ()
    
    class var authorizationStatus: ContactsAuthorizationStatus {
        get {
            return self._authorizationStatus()
        }
    }
    
    class var isAuthorizatedAccess: Bool {
        get {
            return self.authorizationStatus == .granted ? true : false
        }
    }
    
    func requestAccess(_ closure: RequestClosure?) {
        if #available(iOS 9, *) {
            guard let contactStore = self.wrapper.contactStore else {
                closure?(false, nil)
                return
            }
            
            contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) in
                closure?(granted, error as NSError?)
            })
        }
        else {
            guard let addressBook = self.wrapper.addressBook else {
                closure?(false, nil)
                return
            }
            
            ABAddressBookRequestAccessWithCompletion(addressBook, { (granted, error) in
                if let error = error {
                    closure?(granted, error as? NSError)
                }
                closure?(granted, nil)
            })
        }
    }
    
}

private extension ContactAccessRoutine {
    
    class func _authorizationStatus() -> ContactsAuthorizationStatus {
        if #available(iOS 9, *) {
            return ContactsAuthorizationStatus(authorizationStatus: CNContactStore.authorizationStatus(for: CNEntityType.contacts))
        }
        else {
            return ContactsAuthorizationStatus(authorizationStatus: ABAddressBookGetAuthorizationStatus())
        }
    }
    
}
