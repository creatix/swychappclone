//
//  ContactModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactModel)
open class ContactModel: NSObject {

    open var identifier: AnyObject?
    
    open var name: ContactNameModel?
    
    open var job: ContactJobModel?
    
    open var phones: [ContactPhoneModel]?
    open var emails: [ContactEmailModel]?
    
    open var source: ContactSourceModel?
    
}
