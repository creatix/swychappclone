//
//  ContactPhoneModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import libPhoneNumber_iOS

@objc(SCNContactPhoneModel)
open class ContactPhoneModel: NSObject {

    open var number: String? {
        didSet {
            guard let value: String = number, value.characters.count > 0 else { return }
            self.numberNormalized = value.normalizePhoneNumber()
            
            guard let normalizedValue: String = self.numberNormalized, normalizedValue.characters.count > 0 else { return }
            do {
                let phoneNumber = try NBPhoneNumberUtil.sharedInstance().parse(value, defaultRegion: Config.defaultMobileRegion)
                do {
                    let phone = try NBPhoneNumberUtil.sharedInstance().format(phoneNumber, numberFormat: NBEPhoneNumberFormat.E164)
                    self.numberFormatted = phone
                }
                catch let error {
                    #if DEBUG
                        print("ERROR: Phone number formatting: \(error)")
                    #endif
                }
            }
            catch let error {
                #if DEBUG
                    print("ERROR Phone number parsing: \(error)")
                #endif
            }
        }
    }
    open fileprivate(set) var numberNormalized: String?
    open fileprivate(set) var numberFormatted: String?
    open var originalLabel: String?
    open var localizedLabel: String?
    open var order: Int = -1

}
