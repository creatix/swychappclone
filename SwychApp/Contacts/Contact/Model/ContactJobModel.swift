//
//  ContactJobModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactJobModel)
open class ContactJobModel: NSObject {

    open var company: String?
    open var jobTitle: String?
    
}
