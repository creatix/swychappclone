//
//  ContactSourceModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactSourceModel)
open class ContactSourceModel: NSObject {

    open var sourceType: String?
    open var sourceID: Int?
    
}
