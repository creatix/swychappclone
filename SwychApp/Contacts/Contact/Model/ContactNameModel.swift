//
//  ContactNameModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactNameModel)
open class ContactNameModel: NSObject {

    open var firstName: String?
    open var middleName: String?
    open var lastName: String?
    open var compositeName: String?
    
    open var nickname: String?
    
}
