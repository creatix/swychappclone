//
//  ContactEmailModel.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNContactEmailModel)
open class ContactEmailModel: NSObject {

    open var address: String? {
        didSet {
            guard let value: String = address, value.characters.count > 0 else { return }
            
            if value.isValidEmailAddress() == true {
                self.addressValid = value
            }
        }
    }
    open fileprivate(set) var addressValid: String?
    open var originalLabel: String?
    open var localizedLabel: String?
    open var order: Int = -1
    
}
