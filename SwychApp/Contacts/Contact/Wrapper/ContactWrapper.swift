//
//  ContactWrapper.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import AddressBook
import Contacts

@objc(SCNContactWrapper)
internal class ContactWrapper: NSObject {

    fileprivate var _wrapped: AnyObject?
    fileprivate(set) var error: NSError?
    
    @available(iOS 8, *)
    var addressBook: ABAddressBook? {
        get {
            guard let wrapped = _wrapped else {
                return nil
            }
            return wrapped as ABAddressBook
        }
        set {
            _wrapped = newValue
        }
    }
    
    @available(iOS 9, *)
    var contactStore: CNContactStore? {
        get {
            guard let wrapped = _wrapped else {
                return nil
            }
            return wrapped as? CNContactStore
        }
        set {
            _wrapped = newValue
        }
    }
    
    override init() {
        super.init()
        
        if #available(iOS 9, *) {
            self._wrapped = CNContactStore()
        }
        else {
            var error: Unmanaged<CFError>? = nil
            self._wrapped = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
        }
    }
    
}
