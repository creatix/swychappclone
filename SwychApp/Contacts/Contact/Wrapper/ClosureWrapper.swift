//
//  ClosureWrapper.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

@objc(SCNClosureWrapper)
internal class ClosureWrapper: NSObject {

    typealias Closure = () -> ()
    
    let closure: Closure
    
    init(_ closure: @escaping Closure) {
        self.closure = closure
    }
    
}
