//
//  ContactDataExtractor.swift
//  Swych
//
//  Created by Milan Horvatovic on 13.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import Foundation

import AddressBook
import Contacts

@objc(SCNContactDataExtractor)
internal class ContactDataExtractor: NSObject {
    
    typealias MutliValueMapClosure = (_ multiValueRef: ABMutableMultiValue, _ value: CFTypeRef, _ index: CFIndex) -> AnyObject?
    
    func contact(_ source: AnyObject) -> ContactModel? {
        var contact: ContactModel? = nil
        
        if #available(iOS 9, *) {
            guard let contactRef: CNContact = source as? CNContact else { return nil }
            
            contact = ContactModel()
            
            contact?.identifier = contactRef.identifier as AnyObject?
            
            contact?.name = self._name(contactRef)
            contact?.job = self._job(contactRef)
            
            contact?.phones = self._phones(contactRef)
            contact?.emails = self._email(contactRef)
        }
        else {
            guard let recordRef: ABRecord = source as ABRecord else { return nil }
            
            contact = ContactModel()
            
            contact?.identifier = "\(ABRecordGetRecordID(recordRef))" as AnyObject?
            
            contact?.name = self._name(recordRef)
            contact?.job = self._job(recordRef)
            
            contact?.phones = self._phones(recordRef)
            contact?.emails = self._emails(recordRef)
            
            contact?.source = self._source(recordRef)
        }
        
        return contact
    }
    
    //  MARK: - Contacts
    @available(iOS 9.0, *)
    private func _name(_ contactRef: CNContact) -> ContactNameModel {
        let name: ContactNameModel = ContactNameModel()
        
        name.firstName = contactRef.givenName
        name.middleName = contactRef.middleName
        name.lastName = contactRef.familyName
        
        name.nickname = contactRef.nickname
        
        return name
    }
    
    @available(iOS 9.0, *)
    private func _job(_ contactRef: CNContact) -> ContactJobModel {
        let job: ContactJobModel = ContactJobModel()
        
        job.company = contactRef.organizationName
        job.jobTitle = contactRef.jobTitle
        
        return job
    }
    
    @available(iOS 9.0, *)
    private func _phones(_ contactRef: CNContact) -> [ContactPhoneModel]? {
        
        return contactRef.phoneNumbers.enumerated().flatMap { (index, labeledValue) -> ContactPhoneModel? in
            guard let value = labeledValue.value as? CNPhoneNumber else { return nil }
            
            let phone: ContactPhoneModel = ContactPhoneModel()
            phone.number = value.stringValue
            if let labeledValue = labeledValue.label {
                phone.originalLabel = labeledValue.stringByTrimmingLeadingAndTrailingContactLabel()
                phone.localizedLabel = CNLabeledValue<NSString>.localizedString(forLabel: labeledValue).stringByTrimmingLeadingAndTrailingContactLabel()
            }
            phone.order = index
            //print("Phone: \(labeledValue.label) -> \(value.stringValue)")
            //print("\t\(phone.originalLabel) -> \(phone.number)")
            
            return phone
        }
        
    }
    
    @available(iOS 9.0, *)
    private func _email(_ contactRef: CNContact) -> [ContactEmailModel]? {
        
        return contactRef.emailAddresses.enumerated().flatMap { (index, labeledValue) -> ContactEmailModel? in
            guard let value = labeledValue.value as? String else { return nil }
            
            let email: ContactEmailModel = ContactEmailModel()
            email.address = value
            if let labeledValue = labeledValue.label {
                email.originalLabel = labeledValue.stringByTrimmingLeadingAndTrailingContactLabel()
                email.localizedLabel = CNLabeledValue<NSString>.localizedString(forLabel: labeledValue).stringByTrimmingLeadingAndTrailingContactLabel()
            }
            email.order = index
            //print("Email: \(labeledValue.label) -> \(value.stringValue)")
            //print("\t\(phone.originalLabel) -> \(phone.number)")
            return email
        }
        
    }
    
    //  MARK: - AddressBook
    private func _name(_ recordRef: ABRecord) -> ContactNameModel {
        let name: ContactNameModel = ContactNameModel()
        
        name.firstName = self._string(kABPersonFirstNameProperty, recordRef: recordRef)
        name.middleName = self._string(kABPersonMiddleNameProperty, recordRef: recordRef)
        name.lastName = self._string(kABPersonLastNameProperty, recordRef: recordRef)
        name.compositeName = self._compositeName(recordRef)
        
        name.nickname = self._string(kABPersonNicknameProperty, recordRef: recordRef)
        
        return name
    }
 
    private func _job(_ recordRef: ABRecord) -> ContactJobModel {
        let job: ContactJobModel = ContactJobModel()
        
        job.company = self._string(kABPersonOrganizationProperty, recordRef: recordRef)
        job.jobTitle = self._string(kABPersonJobTitleProperty, recordRef: recordRef)
        
        return job
    }
    
    private func _phones(_ recordRef: ABRecord) -> [ContactPhoneModel]? {
        return self._mapMultiValue(kABPersonPhoneProperty,
                                   recordRef: recordRef,
                                   map: { [weak self] (multiValueRef, value, index) -> AnyObject? in
                                    let phone: ContactPhoneModel = ContactPhoneModel()
                                    
                                    phone.number = value as? String
                                    phone.originalLabel = self?._originalLabel(multiValueRef, index: index)
                                    phone.localizedLabel = self?._localizedLabel(multiValueRef, index: index)
                                    //print("Phone: \(self?._originalLabel(multiValueRef, index: index)) -> \(self?._localizedLabel(multiValueRef, index: index))")
                                    phone.order = index
                                    
                                    return phone
                                    
            }) as? [ContactPhoneModel]
    }
    
    private func _emails(_ recordRef: ABRecord) -> [ContactEmailModel]? {
        return self._mapMultiValue(kABPersonEmailProperty,
                                   recordRef: recordRef,
                                   map: { [weak self] (multiValueRef, value, index) -> AnyObject? in
                                    let email: ContactEmailModel = ContactEmailModel()
                                    
                                    email.address = value as? String
                                    email.originalLabel = self?._originalLabel(multiValueRef, index: index)
                                    email.localizedLabel = self?._localizedLabel(multiValueRef, index: index)
                                    //print("Email: \(self?._originalLabel(multiValueRef, index: index)) -> \(self?._localizedLabel(multiValueRef, index: index))")
                                    email.order = index
                                    
                                    return email
                                    
        }) as? [ContactEmailModel]
    }
    
    private func _source(_ recordRef: ABRecord) -> ContactSourceModel? {
        guard let sourceRef = ABPersonCopySource(recordRef) else { return nil }
        
        let source: ContactSourceModel = ContactSourceModel()
        source.sourceType = self._string(kABSourceNameProperty, recordRef: sourceRef.takeUnretainedValue())
        source.sourceType = "\(ABRecordGetRecordID(sourceRef.takeUnretainedValue()))"
        return source
    }
    
    //  MARK: Parsing
    private func _string(_ property: ABPropertyID, recordRef: ABRecord) -> String? { return ABRecordCopyValue(recordRef, property)?.takeRetainedValue() as? String }
    
    private func _originalLabel(_ multiValue: ABMultiValue, index: CFIndex) -> String? { return self._fix(labelName: ABMultiValueCopyLabelAtIndex(multiValue, index)?.takeRetainedValue() as? String) }
    
    private func _localizedLabel(_ multiValue: ABMultiValue, index: CFIndex) -> String? {
        guard let rawLabel = ABMultiValueCopyLabelAtIndex(multiValue, index) else { return nil }
        
        return self._fix(labelName: ABAddressBookCopyLocalizedLabel(rawLabel.takeUnretainedValue())?.takeRetainedValue() as? String)
    }
    
    private func _mapMultiValue(_ property: ABPropertyID, recordRef: ABRecord, map: MutliValueMapClosure) -> [AnyObject]? {
        guard let multiValue: ABMultiValue = ABRecordCopyValue(recordRef, property).takeRetainedValue() as ABMultiValue, ABMultiValueGetCount(multiValue) > 0 else { return nil }
        
        var values: [AnyObject] = [AnyObject]()
        
        let count = ABMultiValueGetCount(multiValue)
        for index in 0...count {
            guard let value = ABMultiValueCopyValueAtIndex(multiValue, index) else { continue }
            guard let object = map(multiValue, value.takeUnretainedValue(), index) else { continue }
            
            values.append(object)
        }
        
        return values
    }
    
    private func _compositeName(_ recordRef: ABRecord) -> String? { return ABRecordCopyCompositeName(recordRef)?.takeRetainedValue() as? String }
    
    @inline(__always) private func _fix(labelName label: String?) -> String? {
        guard var label: String = label, label.characters.count > 0 else { return nil }
        if label.hasPrefix("_$!<") == true {
            label = label.substring(from: label.characters.index(label.startIndex, offsetBy: 4))
        }
        if label.hasSuffix(">!$_") == true {
            label = label.substring(to: label.characters.index(label.endIndex, offsetBy: -4))
        }
        return label
    }
    
}
