//
//  HTTPServiceManager+Contact.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import CTXCoreKit

import ObjectMapper

extension HTTPServiceManager {
    
    func contactSyncInformation(_ queueType: CTXServiceManager.CompletionDispatchQueueType? = ServiceManager.CompletionDispatchQueueType.own,
                                success: ((_ info: SyncInformationServiceModel?) -> Void)?,
                                failure: ((_ error: NSError?) -> Void)?) {
        
        try! self._networkLayer.request(HTTPNetworkLayer.RequestMethod.get,
                                        relativeURL: "sync",
                                        success: { [weak self] (data, validationData) in
                                            guard let data = data as? [String: Any] else {
                                                self?.completionCallback(queueType, completion: { () in
                                                    if let failure = failure {
                                                        failure(nil)
                                                    }
                                                })
                                                return
                                            }
                                            
                                            if let success = success {
                                                self?.completionCallback(queueType, completion: { () in
                                                    success(Mapper<SyncInformationServiceModel>().map(JSON: data))
                                                })
                                            }
            },
                                        failure: { [weak self] (error, data, validationData) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(error as NSError?)
                                                })
                                            }
            },
                                        cancel: { [weak self] (response) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(nil)
                                                })
                                            }
            })
        
    }
    
    func sendContacts(_ contacts: [CDContact],
                      queueType: ServiceManager.CompletionDispatchQueueType? = ServiceManager.CompletionDispatchQueueType.own,
                      success: ((_ success: Bool?) -> Void)?,
                      failure: ((_ error: NSError?) -> Void)?) {
        
        try! self._networkLayer.request(HTTPNetworkLayer.RequestMethod.post,
                                        relativeURL: "sync",
                                        bodyParameters: [
                                            "contacts": Mapper().toJSONArray(contacts)
            ],
                                        success: { [weak self] (data, validationData) in
                                            guard let object = data as? [String: Any], let data: Bool = object["success"] as? Bool else {
                                                self?.completionCallback(queueType, completion: { () in
                                                    if let failure = failure {
                                                        failure(nil)
                                                    }
                                                })
                                                return
                                            }
                                            
                                            if let success = success {
                                                self?.completionCallback(queueType, completion: { () in
                                                    success(data)
                                                })
                                            }
            },
                                        failure: { [weak self] (error, data, validationData) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(error as NSError?)
                                                })
                                            }
            },
                                        cancel: { [weak self] (response) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(nil)
                                                })
                                            }
            })
        
    }
    
    func updateContactVersion(_ version: Int,
        queueType: CTXServiceManager.CompletionDispatchQueueType? = ServiceManager.CompletionDispatchQueueType.own,
        success: ((_ success: Bool?) -> Void)?,
        failure: ((_ error: NSError?) -> Void)?) {
        
        try! self._networkLayer.request(HTTPNetworkLayer.RequestMethod.post,
                                        relativeURL: "sync/update_version",
                                        bodyParameters: [
                                            "contacts_version": version
            ],
                                        success: { [weak self] (data, validationData) in
                                            guard let object = data as? [String: Any], let data: Bool = object["success"] as? Bool else {
                                                self?.completionCallback(queueType, completion: { () in
                                                    if let failure = failure {
                                                        failure(nil)
                                                    }
                                                })
                                                return
                                            }
                                            
                                            if let success = success {
                                                self?.completionCallback(queueType, completion: { () in
                                                    success(data)
                                                })
                                            }
            },
                                        failure: { [weak self] (error, data, validationData) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(error as NSError?)
                                                })
                                            }
            },
                                        cancel: { [weak self] (response) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(nil)
                                                })
                                            }
            })
        
    }
    
    func refreshContactSyncTimeout(_ queueType: CTXServiceManager.CompletionDispatchQueueType? = ServiceManager.CompletionDispatchQueueType.own,
                                   success: ((_ date: Date?) -> Void)?,
                                   failure: ((_ error: NSError?) -> Void)?) {
        
        try! self._networkLayer.request(HTTPNetworkLayer.RequestMethod.get,
                                        relativeURL: "sync/refresh_timeout",
                                        success: { [weak self] (data, validationData) in
                                            guard let object = data as? [String: Any], let data: String = object["data"] as? String else {
                                                self?.completionCallback(queueType, completion: { () in
                                                    if let failure = failure {
                                                        failure(nil)
                                                    }
                                                })
                                                return
                                            }
                                            
                                            if let success = success {
                                                self?.completionCallback(queueType, completion: { () in
                                                    success(Config.DateFormatter.api.date(from: data))
                                                })
                                            }
            },
                                        failure: { [weak self] (error, data, validationData) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(error as NSError?)
                                                })
                                            }
            },
                                        cancel: { [weak self] (response) in
                                            if let failure = failure {
                                                self?.completionCallback(queueType, completion: {
                                                    failure(nil)
                                                })
                                            }
            })
    }
    
}
