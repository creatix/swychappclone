//
//  HTTPServiceManager.swift
//  Swych
//
//  Created by Milan Horvatovic on 20.07.16.
//  Copyright © 2016 Creatix, Inc. All rights reserved.
//

import CTXCoreKit

//@objc(SCNHTTPServiceManager)
internal class HTTPServiceManager: NSObject, ServiceManagerProtocol {
    
    static let sharedInstance = HTTPServiceManager()
    
    lazy internal var completionDispatchQueue: DispatchQueue? = { return DispatchQueue(label: "\(Config.bundleIdentifier).service-manager.completion-queue", attributes: []) }()
    
    //lazy internal var _networkLayer: HTTPNetworkLayer = { return try! HTTPNetworkLayer(baseURL: Config.serverBaseURL) }()
    lazy internal var _networkLayer: HTTPNetworkLayer = { return try! HTTPNetworkLayer(baseURL: Config.serverBaseURL, numberOfConnnections: 1) }()
    
    override private init() {
        super.init()
        
        self._networkLayer.DEBUG = false
        /*
        #if DEBUG
            self._networkLayer.DEBUG = true
        #else
            self._networkLayer.DEBUG = false
        #endif
        */
        
        self._networkLayer.bodyParametersEncoding = HTTPNetworkLayer.RequestParameterEncoding.json
        self._networkLayer.expectedResponseType = HTTPNetworkLayer.ResponseSerializerType.json
        self._networkLayer.headers = [
            "Accept": "application/json",
            "x-api-key": Config.apiKey,
            "x-api-secret": Config.apiSecret,
        ]
        self._networkLayer.responseContentValidate = { (request, response, data, stateValidation) in
            switch response?.statusCode ?? 0 {
            case 204: return (state: HTTPNetworkLayer.ResponseContentValidationState.success, validation: nil)
            //  Send Error invalid
            case 400..<600: return (state: HTTPNetworkLayer.ResponseContentValidationState.error, validation: HTTPNetworkLayer.ResponseValidation(code: NetworkError.Code.response.rawValue, message: "Received invalid state.", data: data))
            default:
                guard let _: Dictionary<String, AnyObject> = data as? [String: AnyObject] else {
                    //  Send Error for incorrect Data
                    return (state: HTTPNetworkLayer.ResponseContentValidationState.invalid, validation: HTTPNetworkLayer.ResponseValidation(code: NetworkError.Code.response.rawValue, message: "Received content is in invalid format.", data: nil))
                }
                //  TODO: check if exists
                return (state: HTTPNetworkLayer.ResponseContentValidationState.success, validation: nil)
            }
        }
        self._networkLayer.responsePreprocess = { (request, response, data) in
            guard let value: Dictionary<String, AnyObject> = data as? Dictionary<String, AnyObject>,
                let parsedData: Dictionary<String, AnyObject> = value["data"] as? Dictionary<String, AnyObject>
                else { return data }
            return parsedData
        }
    }
    
    internal func updateAuthorization(_ bearer: String) {
        self._networkLayer.headers! += ["Authorization": "Bearer \(bearer)"]
    }
    
    internal func cancel() {
        self._networkLayer.sessionManager.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
            dataTasks.forEach( { $0.cancel() } )
            uploadTasks.forEach( { $0.cancel() } )
            downloadTasks.forEach( { $0.cancel() } )
        }
    }
    
}
