//
//  ReferFriendViewController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "ManullyUpdateBalance.h"
#import "UserListView.h"
#import "TTTAttributedLabel.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface ReferFriendViewController : SwychBaseViewController<UpdateWithTextFieldPopUpDelegate,TTTAttributedLabelDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UserListViewDelegate>
@end
