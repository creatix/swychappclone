//
//  ReferFriendViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "UITextFieldExtra.h"
#import "ReferFriendViewController.h"
#import "UITextFieldExtra.h"
#import "WebViewController.h"
#import "Utils.h"
#import "LocalUserInfo.h"
#import "ModelUser.h"
#import "RequestReferFriend.h"
#import "ResponseReferFriend.h"
#import "ModelSystemConfiguration.h"

#import "ResponseGetAffiliateList.h"
#import "ResponseUpdateAffiliate.h"
#import "ModelAffiliate.h"
#import "RequestGetAffiliateList.h"
#import "RequestUpdateAffiliate.h"
#import "UIImageView+WebCache.h"

#import "TrackingEventAffiliateDesignated.h"


#define TEXT_COLOR_TTTLE    UICOLORFROMRGB(192, 31,109,1.0)
#define TEXT_COLOR_DETAIL    UICOLORFROMRGB(28,130,181,1.0)
#define EMAIL_SENDER 123
#define PHONE_SENDER 124


@interface ReferFriendViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labSubTitleTop;
@property (weak, nonatomic) IBOutlet UILabel *labSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *labPromoCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *labPromoCode;
@property (weak, nonatomic) IBOutlet UILabel *labDetail;
@property (weak, nonatomic) IBOutlet UIImageView *img_referFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnText;
@property (strong, nonatomic)  NSString *friendNumberOrMail;
@property (strong,nonatomic) ModelAffiliate* selectedAffiliate;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labDisClaimer;
@property ( nonatomic)  int iIndex;

@property (weak, nonatomic) IBOutlet UIImageView *img_ivator;
@property (weak, nonatomic) IBOutlet UIView *nameTextBgView;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *nameText;
@property (weak, nonatomic) IBOutlet UIView *messageBoxBgView;
@property (weak, nonatomic) IBOutlet UILabel *messageBoxLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblTopTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottomTitle;
@property (weak,nonatomic) IBOutlet UserListView* vUserList;
@property (weak,nonatomic) IBOutlet UIButton* useListBtn;
- (IBAction)buttonTapped:(id)sender;
@end

@implementation ReferFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _labDetail.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ReferDetail)];
    [_labDetail addGestureRecognizer:tapGesture];
    //
    _labPromoCode.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(referalLink)];
    [_labPromoCode addGestureRecognizer:tapGesture1];
    [self LoadText];
    [self DisClaimerSetup];
    [self GetAffiliateList];
    _vUserList.delegate = self;
    _vUserList.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];

}
-(void)touchOnView:(id)sender{
    if (self.vUserList.hidden == NO){
        self.vUserList.hidden = YES;
    }
}
-(void)LoadText{
    _labDetail.textColor = TEXT_COLOR_DETAIL;
    _labSubTitleTop.textColor = TEXT_COLOR_TTTLE;
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    _labPromoCode.text = localUser.referalCode;
    _labDetail.text = NSLocalizedString(@"RefDetailsLabel", nil);
    [_btnText setTitle:NSLocalizedString(@"RefTextInvite", nil)];
    [_btnEmail setTitle:NSLocalizedString(@"RefEmailInvite", nil)];
    _labSubTitleTop.text = [NSString stringWithFormat: NSLocalizedString(@"Refsubtitle", nil),localUser.referalSenderCredit, localUser.referalRecipientCredit ];
    _labSubTitle.text = [NSString stringWithFormat: NSLocalizedString(@"RefsubtitleContent", nil),localUser.referalRecipientCredit,localUser.referalSenderCredit ];//NSLocalizedString(@"RefsubtitleContent", nil);
    _labPromoCodeTitle.text = NSLocalizedString(@"RefPromoCodeTitle", nil);
    
    _messageBoxBgView.layer.cornerRadius = 10.0f;
    _messageBoxBgView.backgroundColor = [UIColor clearColor];
    _nameTextBgView.layer.borderWidth = 0.3;
    _nameTextBgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    // drop shadow
    [_nameTextBgView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_nameTextBgView.layer setShadowOpacity:0.8];
    [_nameTextBgView.layer setShadowRadius:3.0];
    [_nameTextBgView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}
-(void)DisClaimerSetup{
    self.labDisClaimer.delegate = (id<TTTAttributedLabelDelegate>)self;
    
    NSString *text = NSLocalizedString(@"ReferDisclaimer", nil);
    NSRange linkRange1 = [text rangeOfString:@"Privacy Statement" options:NSCaseInsensitiveSearch];
    NSRange linkRange2 = [text rangeOfString:@"Terms of Service" options:NSCaseInsensitiveSearch];
    
    [self.labDisClaimer setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange1];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange2];
        return mutableAttributedString;
    }];
    [self.labDisClaimer addLinkToURL:[NSURL URLWithString:@"privacy"] withRange:linkRange1];
    [self.labDisClaimer addLinkToURL:[NSURL URLWithString:@"terms"] withRange:linkRange2];
}
-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsReferFrindViewNewTitle", nil);
}
-(void)ReferDetail{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    [self showAlert:[NSString stringWithFormat: NSLocalizedString(@"RefDetailContent", nil),localUser.referalRecipientCredit,localUser.referalSenderCredit ] title:@""];
    
}

-(void)GetAffiliateList{
    RequestGetAffiliateList* req = [RequestGetAffiliateList new];
   SEND_REQUEST(req, handleAffiliateListResponse, handleAffiliateListResponse);
}
-(void)handleAffiliateListResponse:(ResponseBase*)response {
    [self dismissLoadingView];
    if(response.success){
        ResponseGetAffiliateList* resp = (ResponseGetAffiliateList*)response;
        if(resp.userAffiliate){
       _nameText.text = resp.userAffiliate.name;
            NSString *str = resp.affiliateMessage;
            
            str = [str stringByReplacingOccurrencesOfString:@"\\n"
                                                 withString:@"\n"];
        _messageBoxLabel.text = str;
        if(![resp.userAffiliate.avatarURL isEqualToString:@""])
        [_img_ivator sd_setImageWithURL:[NSURL URLWithString:resp.userAffiliate.avatarURL]];
        }
        else{
        UIImage *image = [UIImage imageNamed: @"icon_user.png"];
            _img_ivator.image = image;
        }
        
        [_vUserList reloadAffiliates:resp.affiliates];
    }
    else{
        [Utils showAlert:response.errorMessage delegate:nil];
    }
}
-(void)referalLink{
    
     LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
     // ModelSystemConfiguration* config = localUser.sysConfig;
     NSString* url = localUser.referalLink;
     if(url == nil || [url isEqualToString:@""]) return;
     WebViewController *ctrl = (WebViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"WebViewController"];
     ctrl.TitleOfView = @" ";
     ctrl.urlString = url;
     [self.navigationController pushViewController:ctrl animated:YES];
    
}
- (IBAction)buttonTapped:(id)sender{
    if(sender == _useListBtn){
        _vUserList.hidden = !_vUserList.hidden;
        return;
    }
    ManullyUpdateBalance *v = (ManullyUpdateBalance*)[Utils loadViewFromNib:@"ManullyUpdateBalance" viewTag:10];
    v.txtMessageHei.constant = 30.0f;
    v.txtMessage.placeholder = NSLocalizedString(@"RefPopMessagePlaceHolder", nil);
    v.labTitle.text = NSLocalizedString(@"SettingsReferFrindViewTitle", nil);
    v.txtMessage.hidden = YES;
    v.uDelegate = self;
    if(sender == _btnEmail){
        v.labWeHaveTexted.text = NSLocalizedString(@"RefPopEmail", nil);
        v.txtUpdateBalnce.keyboardType = UIKeyboardTypeDefault;
        v.txtUpdateBalnce.placeholder=NSLocalizedString(@"RefPopEmailPlaceHolder", nil);
        _iIndex = EMAIL_SENDER;
    }
    if(sender == _btnText){
        v.labWeHaveTexted.text = NSLocalizedString(@"RefPopPhone", nil);;
        v.txtUpdateBalnce.keyboardType = UIKeyboardTypeNumberPad;
        v.txtUpdateBalnce.placeholder=NSLocalizedString(@"RefPopTxtPlaceHolder", nil);
        _iIndex = PHONE_SENDER;
    }
    //[Utils showPopupView:(PopupViewBase*)v];
    
    self.friendNumberOrMail = v.labWeHaveTexted.text;
    if(_iIndex == EMAIL_SENDER){
        
        [self ShowEmail];
    }
    if(_iIndex == PHONE_SENDER){

        [self ShowSMS];
    }
}
-(void)UserListViewUserSelected:(ModelAffiliate*)Affiliate{
     [self showLoadingView:nil];
    _selectedAffiliate = Affiliate;
    RequestUpdateAffiliate* req = [RequestUpdateAffiliate new];
    req.affiliate = Affiliate;
    
       SEND_REQUEST(req, handleUpdateAffiliateResponse, handleUpdateAffiliateResponse);
}
-(void)handleUpdateAffiliateResponse:(ResponseBase*)response {
    [self dismissLoadingView];
    if(response.success){
        ResponseUpdateAffiliate* resp = (ResponseUpdateAffiliate*)response;
        _nameText.text = _selectedAffiliate.name;
        NSString *str = resp.afflicateMessage;
        
        str = [str stringByReplacingOccurrencesOfString:@"\\n"
                                             withString:@"\n"];
        _messageBoxLabel.text = str;
        if(![_selectedAffiliate.avatarURL isEqualToString:@""])
            [_img_ivator sd_setImageWithURL:[NSURL URLWithString:_selectedAffiliate.avatarURL]];
        else{
            UIImage *image = [UIImage imageNamed: @"icon_user.png"];
            _img_ivator.image = image;
        }
    
        
    }
    else{
        [Utils showAlert:response.errorMessage delegate:nil];
    }
}
-(void)UpdateContext:(NSString*)text message:(NSString*)textmessage view:(ManullyUpdateBalance*)view{
   // [self showLoadingView:nil];
    RequestReferFriend* req = [RequestReferFriend new];
    self.friendNumberOrMail = text;
    if(_iIndex == EMAIL_SENDER){
        req.email = text;
        [self ShowEmail];
    }
    if(_iIndex == PHONE_SENDER){
        req.phone = text;
        [self ShowSMS];
    }
  //  req.message = textmessage;
    
 //   SEND_REQUEST(req, handleReferFriendResponse, handleReferFriendResponse);

}
-(void)handleReferFriendResponse:(ResponseBase*)response {
    [self dismissLoadingView];
    if(response.success){
        ResponseReferFriend* resp = (ResponseReferFriend*)response;
        
        if(_iIndex == EMAIL_SENDER){
            [self ShowEmail];
        }
        if(_iIndex == PHONE_SENDER){
            [self ShowSMS];
        }
    }
    else{
    [Utils showAlert:response.errorMessage delegate:nil];
    }
}
-(void)ShowSMS{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = [NSString stringWithFormat: NSLocalizedString(@"RefSMSMessage", nil),localUser.referalRecipientCredit, localUser.referalLink];
        controller.recipients = nil;
        controller.messageComposeDelegate = self;
       [self presentViewController:controller animated:YES completion:nil];
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    TrackingEventAffiliateDesignated *evt = [[TrackingEventAffiliateDesignated alloc] init];
    [[APP analyticsHandler] sendEvent:evt];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)ShowEmail{
    if(![MFMailComposeViewController canSendMail]) {
        return;
    }
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    NSString *emailTitle = [NSString stringWithFormat: NSLocalizedString(@"RefEmailSub", nil),localUser.referalRecipientCredit];
    NSString *messageBody = [NSString stringWithFormat: NSLocalizedString(@"RefEmailMessage", nil),localUser.referalRecipientCredit,localUser.referalCode, localUser.referalLink];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setToRecipients:nil];
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:YES];

    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"Mail sent");
            TrackingEventAffiliateDesignated *evt = [[TrackingEventAffiliateDesignated alloc] init];
            [[APP analyticsHandler] sendEvent:evt];
        }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    WebViewController *ctrl = (WebViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"WebViewController"];
    ctrl.urlString =[NSString stringWithFormat: @"http:goswych.com/%@",url.absoluteString];
    NSString* title = url.absoluteString;
    NSString *firstCapChar = [[title substringToIndex:1] capitalizedString];
    NSString *cappedString = [title stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    ctrl.TitleOfView = cappedString;
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end
