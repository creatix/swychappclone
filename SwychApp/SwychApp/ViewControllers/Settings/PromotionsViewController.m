//
//  PromotionsViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PromotionsViewController.h"
#import "UILabelExt.h"



@interface PromotionsViewController ()
@property (weak, nonatomic) IBOutlet UILabelExt *labNoPromotions;

@end

@implementation PromotionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labNoPromotions.numberOfLines = 10;
    self.labNoPromotions.verticalAlignment = VerticalAlignment_Middle;
    self.labNoPromotions.text = NSLocalizedString(@"NoPromotions", nil);
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsPromotionViewTitle", nil);
}

@end
