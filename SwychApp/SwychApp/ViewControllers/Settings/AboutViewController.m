//
//  AboutViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AboutViewController.h"
#import "Utils.h"
#import "WebViewController.h"
@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary* bundleInfo = [[NSBundle mainBundle] infoDictionary];
    self.appNameLabel.text = [bundleInfo objectForKey:@"CFBundleName"];
    self.versionLabel.text = [Utils getAppVersion];
    self.copyrightLabel.text = @"© 2016";
    [self setup];
}
-(void)setup{
    self.labTerms.delegate = (id<TTTAttributedLabelDelegate>)self;
    
    NSString *text = NSLocalizedString(@"LabelTermsAbout", nil);
    NSRange linkRange1 = [text rangeOfString:@"privacy policy" options:NSCaseInsensitiveSearch];
    NSRange linkRange2 = [text rangeOfString:@"terms" options:NSCaseInsensitiveSearch];
    
    [self.labTerms setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange1];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange2];
        return mutableAttributedString;
    }];
    [self.labTerms addLinkToURL:[NSURL URLWithString:@"privacy"] withRange:linkRange1];
    [self.labTerms addLinkToURL:[NSURL URLWithString:@"terms"] withRange:linkRange2];
}
-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsAboutViewTitle", nil);
}
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    WebViewController *ctrl = (WebViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"WebViewController"];
    ctrl.urlString =[NSString stringWithFormat: @"http:goswych.com/%@",url.absoluteString];
    NSString* title = url.absoluteString;
    NSString *firstCapChar = [[title substringToIndex:1] capitalizedString];
    NSString *cappedString = [title stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    ctrl.TitleOfView = cappedString;
    [self.navigationController pushViewController:ctrl animated:YES];
}

@end
