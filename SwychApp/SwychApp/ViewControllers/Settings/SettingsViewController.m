//
//  SettingsViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SettingsViewController.h"
#import "ApplicationConfigurations.h"
#import "MenuItem.h"
#import "LocalUserInfo.h"
#import "TouchIdTableViewCell.h"
#import "AppDelegate.h"
#import "SwychBaseViewController.h"
#import "Utils.h"
#import "UserInfoUpdatePopupView.h"
#import "ChangePasswordPopupView.h"
#import "UIRoundImageView.h"
#import "AvatarImageHandler.h"
#import "CreatixContactsManager.h"

#import "RequestLogOut.h"
#import "ResponseLogOut.h"
#import "RequestUploadImageVideo.h"
#import "ResponseUploadImageVideo.h"

#define ActionSheetButton_ChoosePhoto   0
#define ActionSheetButton_TakePhoto     1

#define PromptContext_ContactPermissionBot  0x0101
#define PromptContext_SignOut               0x0102

@interface SettingsViewController ()
<UITableViewDelegate, UITableViewDataSource> {
    NSArray<MenuItem*>* _connectedAccountMenuItems;
    NSArray<MenuItem*>* _touchIdMenuItems;
}

@property (weak, nonatomic) IBOutlet UIButton *btnEditAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignout;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *btnLinkCode;
- (IBAction)buttonTapped:(id)sender;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar clearBackground];
    
    ApplicationConfigurations* appConfig = [ApplicationConfigurations instance];
 //   LocalUserInfo* userInfo = [appConfig getLocalStoredUserInfo];
    LocalUserInfo* userInfo = [[LocalStorageManager instance] getLocalStoredUserInfo];
    self.nameLabel.text = userInfo.getUserFullName;
    self.userImage.image = [[ApplicationConfigurations instance] getAvatarImage];
    
    _connectedAccountMenuItems = [appConfig getConnectedAccountsMenuItems];
    _touchIdMenuItems = [appConfig getTouchIdMenuItems];
    
    [self.touchIdTable reloadData];
    
   // self.ivBackground.backgroundColor = [UIColor blueColor];
    self.userImage.imageViewDelegate = (id<UIRoundCornerImageViewDelegate>)self;
  //  [_btnLinkCode setTitle:NSLocalizedString(@"linkpotcode",nil)];
    [self lincodeTextSetup];
}
-(void)lincodeTextSetup{
    self.btnLinkCode.delegate = (id<TTTAttributedLabelDelegate>)self;
    
    NSString *text = NSLocalizedString(@"linkpotcode",nil);
    
    NSRange linkRange1 = [text rangeOfString:NSLocalizedString(@"linkpotcode",nil) options:NSCaseInsensitiveSearch];
    [self.btnLinkCode AddTextAndColor:[UIColor darkGrayColor] text:text];
    [self.btnLinkCode addLinkToURL:[NSURL URLWithString:text] withRange:linkRange1];
}
-(void)touchOnAvatarView:(id)sender{
    [self startPhotoSelection];
}

-(void)startPhotoSelection{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Button_Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Button_ChoosePhoto", nil),NSLocalizedString(@"Button_TakePhoto", nil), nil];
    [sheet showInView:self.view];
}


-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsSettingViewTitle", nil);
}

- (IBAction)buttonTapped:(id)sender{
    if (sender == self.btnEditAccount){
        [self editAccount];
    }
    else if (sender == self.btnChangePassword){
        [self changePassword];
    }
    else if (sender == self.btnSignout){
        [self signOut];
    }

}

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}
-(void)editAccount{
    UserInfoUpdatePopupView *uip = (UserInfoUpdatePopupView *)[Utils loadViewFromNib:@"UserInfoUpdatePopupView" viewTag:0];
    uip.delegateUpdate = (id<UserInfoUpdateViewDelegate>)self;
    
    [uip show];
}

-(void)changePassword{
    ChangePasswordPopupView *cppv =(ChangePasswordPopupView *)[Utils loadViewFromNib:@"ChangePasswordPopupView" viewTag:0];
    [cppv show];
}

-(void)signOut{
    [Utils showAlert:NSLocalizedString(@"PromptLogOut", nil)
               title:nil delegate:(id<AlertViewDelegate>)self
         firstButton:NSLocalizedString(@"Button_NO", nil)
        secondButton:NSLocalizedString(@"Button_YES", nil)
     withContext:PromptContext_SignOut withContextObject:nil checkboxText:nil];
}

-(void)switchStatechanged:(id)sender{
    UISwitch *s = (UISwitch*)sender;
    BOOL enabled = s.on;
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    lui.touchIDEnabled = enabled;
    [[LocalStorageManager instance] saveLocalUserInfo:lui];
}

-(void)promptContactPermissionForBOT{
    [Utils showAlert:NSLocalizedString(@"Prompt_ContactPermissionForBOT", nil)
               title:nil delegate:(id<AlertViewDelegate>)self
         firstButton:NSLocalizedString(@"Button_NO", nil)
        secondButton:NSLocalizedString(@"Button_YES", nil)
     withContext:PromptContext_ContactPermissionBot withContextObject:nil checkboxText:nil];

}
-(void)displayBOTLinkingCode{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    NSString* potcode = localUser.botLinkingCode;
    
    [self.btnLinkCode setText:potcode afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        return mutableAttributedString;
    }];
}

#pragma
#pragma mark - AvatarImageHandlerDelegate
-(void)avatarImageHandler:(AvatarImageHandler*)handler didSelectedImage:(UIImage*)selectedImage{
    self.userImage.image = selectedImage;
    NSString *imgData = nil;
    NSData *data = UIImageJPEGRepresentation(selectedImage, 1.0);
    imgData = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    [self sendUploadImageRequest:imgData];
}

-(void)avatarImageHdnalerDidCancelled:(AvatarImageHandler*)handler{
    
}

#pragma
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[AvatarImageHandler instance] handleAvatarImage:chosenImage viewController:self delegate:(id<AvatarImageHandlerDelegate>)self];
}

#pragma
#pragma mark -UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == ActionSheetButton_ChoosePhoto){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
        [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (buttonIndex == ActionSheetButton_TakePhoto){
#if TARGET_IPHONE_SIMULATOR
        return;
#endif
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else
        [self presentViewController:picker animated:YES completion:nil];
    }
}

#pragma
#pragma mark - UIRoundCornerImageViewDelegate
-(void)roundCornerImageViewTapped:(UIRoundCornerImageView*)imageView{
    [self startPhotoSelection];
}

#pragma 
#pragma mark - UserInfoUpdateViewDelegate
-(void)userInfoUpdateView:(UserInfoUpdateView*)view infoUpdated:(ModelUser*)user{
    [self showAlert:NSLocalizedString(@"MSG_ProfileUpdateSuccess", nil)];
}

#pragma
#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _touchIdMenuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuItem* menuItem = [_touchIdMenuItems objectAtIndex:indexPath.row];
    TouchIdTableViewCell* cell = (TouchIdTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"touchIdCell"];
    cell.label.text = menuItem.text;
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    cell.enabledSwitch.on = lui.touchIDEnabled;
    [cell.enabledSwitch addTarget:self action:@selector(switchStatechanged:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return NSLocalizedString(@"LabelTouchID", nil);
}

#pragma mark - UITableViewDelegate - 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    ((UITableViewHeaderFooterView*)view).backgroundView.backgroundColor = [UIColor clearColor];
}


#pragma
#pragma mark - Server Communication
-(void)sendLogOutRequest{
    [self showLoadingView:nil];
    
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestLogOut *req = [[RequestLogOut alloc] init];
    req.token = localUser.sessionToken;
    
    SEND_REQUEST(req, handleLogOutResponse, handleLogOutResponse);
}

-(void)handleLogOutResponse:(ResponseBase*)resposne{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self dismissLoadingView];
    
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    localUser.sessionToken = nil;
    
    [[LocalStorageManager instance] saveLocalUserInfo:localUser];
    [APP setCallContactSyncResetNeeded:YES];
    [app switchToLoginView:NO preview:@""];
}

-(void)sendUploadImageRequest:(NSString*)imageData{
    [self showLoadingView:nil];
    RequestUploadImageVideo *req = [[RequestUploadImageVideo alloc] init];
    req.imageData = imageData;
    
    
    SEND_REQUEST(req, handleUploadImageResponse, handleUploadImageResponse);
    
}

-(void)handleUploadImageResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(!response.success){
        [self showAlert:response.errorMessage];
    }
    else{
        [self showAlert:NSLocalizedString(@"MSG_AvatarImageUpdated", nil)];
        [[LocalStorageManager instance] saveUserAvatarImage:self.userImage.image];
        [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
    }
}

#pragma
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context == PromptContext_SignOut){
        if (index == 2){//Yes button to log out current session
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            if(localUser.touchIDEnabled){
            [[LocalStorageManager instance] saveLocalUserInfo:localUser];
            AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [app switchToLoginView:NO preview:@""];
                return YES;
            }
            [self sendLogOutRequest];
        }
        LocalUserInfo* userInfo = [[LocalStorageManager instance] getLocalStoredUserInfo];
        self.nameLabel.text = userInfo.getUserFullName;
    }
    else if (alertView.context == PromptContext_ContactPermissionBot){
        if (index == 2){//Yes agree the permission
            [self displayBOTLinkingCode];
            [[CreatixContactsManager shareInstance] startSyncContacts];
            [CreatixContactsManager shareInstance].isFirstTimeSync = NO;
        }
        else{
            [alertView dismiss];
            [self showAlert:NSLocalizedString(@"MSG_DenyContactPermissionForBOT", nil)];
            return NO;
        }
    }
    return YES;
}
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    [self promptContactPermissionForBOT];
    
}
@end
