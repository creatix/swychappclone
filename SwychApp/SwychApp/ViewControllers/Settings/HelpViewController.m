//
//  HelpViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "HelpViewController.h"
#import "ApplicationConfigurations.h"
#import "HelpMenuItem.h"

#define CELL_TAG_Getting_Started        1
#define CELL_TAG_Account_Settings       2
#define CELL_TAG_Sending_And_Receiving  3
#define CELL_TAG_Redeeming              4
#define CELL_TAG_Syncing                5
#define CELL_TAG_Uploading              6
#define CELL_TAG_Swych_Points           7
#define CELL_TAG_Safety_And_Security    8
#define CELL_TAG_Getting_Contact_Us     9

@interface HelpViewController ()
    <UITableViewDataSource, UITableViewDelegate> {
    NSArray<HelpMenuItem*>* _helpMenuItems;
}

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _helpMenuItems = (NSArray*)[[ApplicationConfigurations instance] getHelpMenuItems];
    
}


#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _helpMenuItems.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HelpCell"];
    HelpMenuItem* helpItem = [_helpMenuItems objectAtIndex:indexPath.row];
    cell.textLabel.text = helpItem.text;
    
    return cell;
}

@end
