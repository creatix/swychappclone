//
//  PaymentViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentViewController.h"
#import "ApplicationConfigurations.h"
#import "MenuItemWithPosition.h"
#import "PaymentTableViewCell.h"
#import "LocalUserInfo.h"


#define CELL_TAG_Apple_Pay      1
#define CELL_TAG_PayPal         2
#define CELL_TAG_PAY_WITH_CARD  3

@interface PaymentViewController ()
<UITableViewDataSource, UITableViewDelegate> {
    NSArray<MenuItemWithPosition*>* _paymentItems;
}

@property(nonatomic,assign) NSInteger selectedMethod;

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _paymentItems = (NSArray*)[[ApplicationConfigurations instance] getPaymentMenuItems];
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    
    self.selectedMethod = lui.selectedPaymentMethod;
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsPaymentViewTitle", nil);
}

#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _paymentItems.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuItemWithPosition* helpItem = [_paymentItems objectAtIndex:indexPath.row];
    NSString* cellIdentifier = nil;
    
    UIImage *img = nil;
    if (helpItem.tag == self.selectedMethod){
        img = [UIImage imageNamed:@"icon_checkmark.png"];
    }
    if(helpItem.iconPosition == kIconLocation_Left) {
        cellIdentifier = @"PaymentCell-Left";
    }
    else if(helpItem.iconPosition == kIconLocation_Right) {
        cellIdentifier = @"PaymentCell-Right";
    }
    else {
        cellIdentifier = @"PaymentCell-Left";
    }
    
    PaymentTableViewCell* cell = (PaymentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(helpItem.iconPosition == kIconLocation_Right){
        cell.label.text = helpItem.text;
        cell.icon.image = nil;
    }
    else{
        cell.label.text = nil;
        cell.icon.image = [UIImage imageNamed:helpItem.iconImageName];
    }
    cell.iconCheckmark.image = img;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuItemWithPosition* helpItem = [_paymentItems objectAtIndex:indexPath.row];
    if (helpItem.tag != self.selectedMethod){
        self.selectedMethod = helpItem.tag;
        [tableView reloadData];
        LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
        lui.selectedPaymentMethod = self.selectedMethod;
        [[LocalStorageManager instance] saveLocalUserInfo:lui];
    }
}
@end