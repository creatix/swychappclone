//
//  ArchivedCardsViewController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"

@interface ArchivedCardsViewController : SwychBaseViewController

@property (nonatomic, weak) IBOutlet UITableView* table;


@end
