//
//  ArchivedCardsViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ArchivedCardsViewController.h"
#import "ArchivedCardTableViewCell.h"
#import "ApplicationConfigurations.h"
#import "LocalUserInfo.h"
#import "ModelGiftCard.h"
#import "SDWebImageDownloader.h"
#import "IssuedGiftcardDetailViewController.h"
#import "ModelTransaction.h"
#import "RequestGetArchivedGiftcards.h"
#import "ResponseGetArchivedGiftcards.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"
#import "LocationManager.h"
@interface ArchivedCardsViewController()
    <UITableViewDelegate, UITableViewDataSource> {
        NSArray<ModelTransaction*>* _giftCards;
        NSMutableDictionary<NSURL*, UIImage*>* _imageCache;
}
@end

@implementation ArchivedCardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _imageCache = [NSMutableDictionary dictionary];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    RequestGetArchivedGiftcards* req = [RequestGetArchivedGiftcards new];
    
    SEND_REQUEST(req, handleGetArchivedGiftcardsResponse, handleGetArchivedGiftcardsResponse);
}
-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"SettingsArchivedCardViewTitle", nil);
}


-(void)handleGetArchivedGiftcardsResponse:(ResponseBase*)response {
    ResponseGetArchivedGiftcards* resp = (ResponseGetArchivedGiftcards*)response;
    
    _giftCards = resp.giftCards;
    [self.table reloadData];
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailPopupView:tran locations:((ResponseGetNearbyLocations*)response).locations];
}
-(void)showGiftcardDetailPopupView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.ArchivedStatus = ARCHIVED;
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}
#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _giftCards.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArchivedCardTableViewCell* cell = (ArchivedCardTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ArchivedCell"];
    ModelTransaction* giftCard = [_giftCards objectAtIndex:indexPath.row];
    cell.cardNameLabel.text = giftCard.giftCard.retailer;
    cell.amountLabel.text = [NSString stringWithFormat:@"$%.02f", giftCard.amount];
    cell.tag = indexPath.row;
    
    NSArray<NSString*>* imageUrls = giftCard.giftCard.giftCardImageURL;
    NSURL* imageUrl = [NSURL URLWithString:[imageUrls objectAtIndex:0]];
    
    UIImage* image = [_imageCache objectForKey:imageUrl];
    cell.cardImage.image = image; // replace with please wait.
    if(image == nil) {
        NSInteger index = indexPath.row;
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:imageUrl
                                                              options:0
                                                             progress:nil
                                                            completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                                if(error != nil) {
                                                                    NSLog(@"Error: %@", error);
                                                                }
                                                                else if(finished) {
                                                                    if(index == cell.tag) {
                                                                        cell.cardImage.image = image;
                                                                        [_imageCache setObject:image forKey:imageUrl];
                                                                    }
                                                                }
                                                            }];
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ModelTransaction* giftCard = [_giftCards objectAtIndex:indexPath.row];
    [self sendGetNearbyLocationRequest:giftCard];
}
@end
