//
//  AboutViewController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "TTTAttributedLabel.h"
@interface AboutViewController : SwychBaseViewController<TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *copyrightLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labTerms;

@end
