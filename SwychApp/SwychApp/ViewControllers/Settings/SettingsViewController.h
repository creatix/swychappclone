//
//  SettingsViewController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-15.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "TTTAttributedLabel.h"
@class UIRoundImageView;

@interface SettingsViewController : SwychBaseViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UIRoundImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITableView *touchIdTable;

@end
