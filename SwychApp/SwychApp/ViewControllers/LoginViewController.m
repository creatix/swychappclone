//
//  LoginViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "UIRoundCornerButton.h"
#import "LocalUserInfo.h"
#import "AppDelegate.h"
#import "RegistrationData.h"
#import "RegistrationMobileNumberViewController.h"
#import "SocialNetworkIntegration.h"
#import "ViewOTPVerification.h"
#import "UICheckbox.h"
#import "NSString+Extra.h"

#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"
#import "RequestResendOTP.h"
#import "ResponseResendOTP.h"
#import "RequestActivation.h"
#import "ResponseActivation.h"
#import "RequestVerifyOTP.h"
#import "ResponseVerifyOTP.h"
#import "TouchIDController.h"
#import "ForgotPasswordPopupView.h"
#import "RequestOTPForgetPassword.h"
#define Context_LoginOption_Phone       0x01
#define Context_LoginOption_Facebook    0x02
#define Context_LoginOption_Google      0x03
#define OPTFORGOTPASSWORD 1234
@interface LoginViewController ()

- (IBAction)buttonTapped:(id)sender;

-(void)setupButtons;
-(void)sendAuthenticationRequest;
-(void)handleAuthenticationResponse:(ResponseBase*)response;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self setupButtons];
 
    if(_localUser!=nil)
    _txtPhone.text = _localUser.phoneNumber;
    if(self.viewType == LoginViewType_Phone){
    [self EnableTouchId];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    LocalUserInfo *localData1 = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if(localData1!=nil)
    self.chkTouchID.checked=localData1.touchIDEnabled;
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(sendGetBotSentGiftListsRequest) name:BotNewTransactionNotification object:nil];
    [center addObserver:self
               selector:@selector(sendGetBotLinkRequest) name:BotNewLinkNotification object:nil];
}
- (void)appDidBecomeActive:(NSNotification *)notification {
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if(app.bakcgroudFlag==100){
    [self EnableTouchId];
        app.bakcgroudFlag =-1;
    }
}
-(void)EnableTouchId{
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localData.sessionToken != nil && localData.touchIDEnabled == YES){
        self.chkTouchID.checked = YES;
        [[TouchIDController instance] authicate:NSLocalizedString(@"touchidrequire",nil) withEnterPasswordEnabled:YES
                                 withCompletion:^(TouchIDControllerResponse response){
                                     [self touchIDAuthicationHandler:response];
                                 }];
    }


}
-(void)sendAuthenticationRequest:(LocalUserInfo*)localUser{
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = localUser.swychId;
    req.token = localUser.sessionToken;
    req.phoneNumber = localUser.phoneNumber;
    
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}
-(void)touchIDAuthicationHandler:(TouchIDControllerResponse)response{
    if(response == TouchIDControllerResponseSuccess){
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    [self sendAuthenticationRequest:localData];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.promptOTP){
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupButtons{
    self.btnLoginWithFacebook.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.btnLoginWithPhone.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.btnLoginWithGoogle.imageView.contentMode = UIViewContentModeScaleAspectFit;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)ignoreToSetInfo:(id)object{
    if (object == self.btnLoginWithGoogle ||
        object == self.btnLoginWithPhone ||
        object == self.btnLoginWithFacebook ||
        object == self.vMiddleView){
        return YES;
    }
    return [super ignoreToSetInfo:object];
}


-(IBAction)buttonTapped:(id)sender{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (sender == self.btnLoginWithFacebook){
        [app showLoginView:LoginViewType_Facebook];
    }
    else if (sender == self.btnLoginWithPhone){
        [app showLoginView:LoginViewType_Phone];
    }
    else if (sender == self.btnLoginWithGoogle){
         [app showLoginView:LoginViewType_Google];
    }
    else if(sender == self.btnForgotPassword){
        [self presentPop];
       
    }
    else if (sender == self.btnLogin){
        if (self.viewType == LoginViewType_Phone){
            [self sendAuthenticationRequest];
        }
        else if (self.viewType == LoginViewType_Google){
            if (self.txtPhone.text == nil || [[self.txtPhone.text trim] length] == 0){
                [self showAlert:NSLocalizedString(@"EmptyPhone", nil)];
                return;
            }
            [[SocialNetworkIntegration instance] doGoogleLoginOnViewController:self
                                                                      delegate:(id<SocialNetworkIntegrationDeleage>)self];
        }
        else if (self.viewType == LoginViewType_Facebook){
            if (self.txtPhone.text == nil || [[self.txtPhone.text trim] length] == 0){
                [self showAlert:NSLocalizedString(@"EmptyPhone", nil)];
                return;
            }
            [[SocialNetworkIntegration instance] doFacebookLoginOnViewController:self];
        }
    }
}
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (index == 2){//Yes button to log out current session
        [self showMobileNumberView:nil];
    }
    return YES;
}
-(void)UpdateContext:(NSString*)text message:(NSString*)textmessage view:(ManullyUpdateBalance*)view{
    RequestOTPForgetPassword *req = [[RequestOTPForgetPassword alloc] init];
    req.phone = text;
    req.otpSentType = OTPSENTTYPE_FORGOTPASSOWRD;
    if(![text isEqualToString:@""])
        req.context = [text longLongValue];
    SEND_REQUEST(req, handlePhoneResendOTPResponse, handlePhoneResendOTPResponse);
    
}
-(BOOL)handlePhoneResendOTPResponse:(ResponseBase *)response{
    if(!response.success){
        if(response.errorCode == ERR_VerifyPhonenumber_UserNotExist){
            [Utils showAlert:response.errorMessage
                       title:nil delegate:(id<AlertViewDelegate>)self
                 firstButton:NSLocalizedString(@"Button_NO", nil)
                secondButton:NSLocalizedString(@"Button_YES", nil)];
            return NO;
        }
        [self showAlert:response.errorMessage];
        return NO;
    }
     PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate =(id<OTPVerificationDelegate>) self;
        [vVerification setMobilePhoneNumber:[NSString stringWithFormat:@"%lu" ,response.context]];
        vVerification.context = OPTFORGOTPASSWORD;
        phoneunmber = [NSString stringWithFormat:@"%lu" ,response.context];
        [Utils showPopupView:(PopupViewBase*)v];
    }
    return YES;
    
}
-(void)presentPop{
    ManullyUpdateBalance *v = (ManullyUpdateBalance*)[Utils loadViewFromNib:@"ManullyUpdateBalance" viewTag:10];
    v.txtMessageHei.constant = 30.0f;
    v.txtMessage.placeholder = NSLocalizedString(@"RefPopMessagePlaceHolder", nil);
    v.labTitle.text = NSLocalizedString(@"ForgortPassowrd", nil);
    v.uDelegate = self;

    v.labWeHaveTexted.text = NSLocalizedString(@"RefPopPhone", nil);
    v.txtMessage.hidden = YES;
    v.txtUpdateBalnce.keyboardType = UIKeyboardTypeNumberPad;
    v.txtUpdateBalnce.placeholder=NSLocalizedString(@"RefPopTxtPlaceHolder", nil);

    
    [Utils showPopupView:(PopupViewBase*)v];
}

-(void)showMobileNumberView:(RegistrationData*)regData{
    RegistrationMobileNumberViewController *ctrl = (RegistrationMobileNumberViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"MobileNumberViewController"];
    ctrl.regData  = regData;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)switchToHomeView{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    [app switchToHomeView:localUser];
}

-(void)showVerificationViewWithUser:(ModelUser*)user{
    PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate =(id<OTPVerificationDelegate>) self;
        vVerification.user = user;
        [vVerification setMobilePhoneNumber:user.phoneNumber];
        [Utils showPopupView:(PopupViewBase*)v];
    }
}

#pragma 
#pragma makr = OTPVerificationDelegate
-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView{
    if(verificationView.user)
    [self sendResendOTPRequest:verificationView.user];
    else{
        [self sendResendOTPRequestWithPhone:verificationView.getMobilePhoneNumber];
    }
}
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP{
    [self sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
}



#pragma 
#pragma mark - SocialNetworkIntegrationDelegate
-(void)facebookLogin:(BOOL)success facebookToken:(NSString*)token facebookUserId:(NSString*)userId error:(NSError*)error{
    if (success){
        [self sendAuthenticationRequestWithFBtoken:token fbUserId:userId];
    }
}

-(void)googleSignIn:(BOOL)success googleToken:(NSString *)token googleUserId:(NSString*)userId userInfo:(ModelUser*)user error:(NSError*)error{
    if (success){
        [self sendAuthenticationRequestWithGoogletoken:token email:user.email];
    }
}

#pragma
#pragma mark Server communication handling
-(void)sendAuthenticationRequestWithGoogletoken:(NSString*)googleToken email:(NSString*)email{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = nil;
    req.phoneNumber = [self.txtPhone.text trim];
    req.googleToken = googleToken;
    req.email = email;
    req.context = Context_LoginOption_Google;
    
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}


-(void)sendAuthenticationRequestWithFBtoken:(NSString*)fbToken fbUserId:(NSString*)fbUserId{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = nil;
    req.phoneNumber = [self.txtPhone.text trim];
    req.fbToken = fbToken;
    req.facebookId = fbUserId;
    req.context = Context_LoginOption_Facebook;
    
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}


-(void)sendAuthenticationRequest{
    if (self.txtPhone.text == nil || [[self.txtPhone.text trim] length] == 0 ||
        self.txtPassword.text == nil || [[self.txtPassword.text trim] length] == 0){
        [self showAlert:NSLocalizedString(@"EmptyPasswordOrPhone", nil)];
        return;
    }
    [self showLoadingView:nil];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = nil;
    req.context = Context_LoginOption_Phone;
    NSRange range = [self.txtPhone.text rangeOfString:@"@"];
    if (range.location == NSNotFound){
        req.phoneNumber = self.txtPhone.text;
    }
    else{
        req.email = self.txtPhone.text;
    }
    req.password = self.txtPassword.text;
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}

-(void)handleAuthenticationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![self handleServerResponse:response]){
        ResponseAuthentication *authResp = (ResponseAuthentication*)response;
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        
        localUser.sessionToken = authResp.token;
        [localUser copyFromUser:authResp.user];
        localUser.sysConfig = authResp.systemConfigurations;
        switch (response.context) {
            case Context_LoginOption_Phone:
                localUser.previousLoginOption = LoginOption_PhoneNumber;
                break;
            case Context_LoginOption_Facebook:
                localUser.previousLoginOption = LoginOption_Facebook;
                break;
            case Context_LoginOption_Google:
                localUser.previousLoginOption = LoginOption_Google;
                break;
            default:
                break;
        }
        localUser.touchIDEnabled = self.chkTouchID.checked;
        localUser.avatarImageURL =  authResp.user.avatar;
        if(localUser.avatarImageURL != nil){
            [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
                if (!error){
                    [[LocalStorageManager instance] saveUserAvatarImage:image];
                    [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
                }
            }];
        }
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        SEL selector = @selector(switchToHomeView);
        NSMethodSignature *sig = [self methodSignatureForSelector:selector];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
        [invocation setSelector:selector];
        [invocation setTarget:self];
        [app requestPushNotificaitonToken:invocation];
        
    }
}

-(BOOL)handleServerResponse:(ResponseBase *)response{
    if ([response isKindOfClass:[ResponseAuthentication class]]){
        if(response.errorCode == ERR_Authentication_OTPVerificationNeeded){
            [self showVerificationViewWithUser:((ResponseAuthentication*)response).user];
            return YES;
        }
    }
    return [super handleServerResponse:response];
}


-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    [self showLoadingView:nil];
    RequestVerifyOTP *req = [[RequestVerifyOTP alloc] init];
    req.contextObject = v;
    req.swychId = user.swychId;
    req.otp = [v getOTPCode];
    req.context = v.context;
    req.phoneNumber = phoneunmber;
    if(v.context == OPTFORGOTPASSWORD){
        req.otpSentType = OTPSENTTYPE_FORGOTPASSOWRD;
    }
    else{
        req.otpSentType = OTPSENTTYPE_REGISTRATION;
    }
    SEND_REQUEST(req, handleOTPVerificationResponse, handleOTPVerificationResponse);
}

-(void)handleOTPVerificationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
 

    if (![self handleServerResponse:response]){
        ViewOTPVerification *v = (ViewOTPVerification*)response.contextObject;
        ResponseVerifyOTP *resp = (ResponseVerifyOTP*)response;
        ModelUser *user = resp.user;
        
        
        [v dismiss];
        if(v.context == OPTFORGOTPASSWORD){
            
            ForgotPasswordPopupView *cppv =(ForgotPasswordPopupView *)[Utils loadViewFromNib:@"ForgotPasswordPopupView" viewTag:0];
            cppv.phoneNumber = phoneunmber;
            cppv.otp = [v getOTPCode];
            cppv.uDelegate = self;
            [cppv show];
        }
        else
        [self sendAuthenticationRequest];
    }
}

-(void)sendResendOTPRequest:(ModelUser*)user{
    [self showLoadingView:nil];
    RequestResendOTP *req = [[RequestResendOTP alloc] init];
    req.swychId = user.swychId;
    req.otpSentType = OTPSENTTYPE_REGISTRATION;
    SEND_REQUEST(req, handleResendOTPResponse, handleResendOTPResponse);
}
-(void)sendResendOTPRequestWithPhone:(NSString*)phone{
    [self showLoadingView:nil];
    RequestOTPForgetPassword *req = [[RequestOTPForgetPassword alloc] init];
    req.phone = phone;
    req.otpSentType = OTPSENTTYPE_FORGOTPASSOWRD;
    SEND_REQUEST(req, handleResendOTPResponse, handleResendOTPResponse);
}

-(void)handleResendOTPResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        [self showAlert:response.errorMessage];
    }
}

-(void)sendActivationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    [self showLoadingView:nil];
    RequestActivation *req = [[RequestActivation alloc] init];
    req.swychId = user.swychId;
    if (v != nil){ //Do two steps registration activation
        req.oneTimePIN = [v getOTPCode];
        req.contextObject = v;
    }
    
    SEND_REQUEST(req, handleActivationResponse, handleActivationResponse);
}

-(void)handleActivationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        if (response.contextObject != nil && [response.contextObject isKindOfClass:[ViewOTPVerification class]]){
            [(ViewOTPVerification *)response.contextObject dismiss];
        }
        [self sendAuthenticationRequest];
    }
}
#pragma mark ForgotPasswordPopupViewDelegate
-(void)AfterUpdateNewPassword:(NSString*)password phone:(NSString*)phonenumber{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = nil;
    req.context = Context_LoginOption_Phone;
    req.phoneNumber = phonenumber;
    req.password = password;
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
}
@end
