//
//  ActivityViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ActivityViewController.h"
#import "RequestGetHistory.h"
#import "ResponseGetHistory.h"
#import "GiftCardMallViewController.h"
#import "TransactionDetailViewController.h"
#define NumberOfTransactionsToLoad 20
#define LODEMOREREQ 234
#define ViewName        @"Activity"
#import "Utils.h"
@implementation ActivityViewController
@synthesize vHistoryGiftcardSelection,arr_Transactions;
-(void)viewDidLoad{
    [super viewDidLoad];
    vHistoryGiftcardSelection.selectionViewDelegate = self;
    vHistoryGiftcardSelection.backgroundColor = [UIColor clearColor];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setNavBarInfo];
    if(!arr_Transactions){
        arr_Transactions = [[NSMutableArray alloc] initWithCapacity: NumberOfTransactionsToLoad];
    }
    [arr_Transactions removeAllObjects];
    [self sendHistoryRequest:NO];
}
-(BOOL)hideTabbar{
    return NO;
}

-(NSString*)getViewName{
    return ViewName;
}
-(void)sendHistoryRequest:(BOOL)loadMore{
    RequestGetHistory *req = [[RequestGetHistory alloc] init];
    req.numberOfTransactions = NumberOfTransactionsToLoad;
    if(loadMore){
       ModelTransaction* lastObj = [arr_Transactions lastObject];
       NSString* endtime = [Utils getTimeString: lastObj.transactionDate];
        req.endDate = endtime;
        req.context = LODEMOREREQ;
    }
 //   [self showLoadingView:nil];
    SEND_REQUEST(req, handleGetAvailableGiftcardByHistoryResponse, handleGetAvailableGiftcardByHistoryResponse);
}
-(void)handleGetMoreTransactions:(NSArray *)transactions{
    NSSortDescriptor *sortPreTranDate = [[NSSortDescriptor alloc] initWithKey:@"transactionDate" ascending:NO];
    NSArray *sortedTransactions = [transactions sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortPreTranDate, nil]];
    ModelTransaction *itemLastTran = [arr_Transactions lastObject];
    int startIdx = (int)[arr_Transactions count];
    for(int i = (int)[sortedTransactions count] - 1; i >= 0; i --){
        ModelTransaction *item = (ModelTransaction *)[transactions objectAtIndex:i];
        if ([item.transactionDate compare:itemLastTran.transactionDate] == NSOrderedSame){
            startIdx = i;
            break;
        }

    }
    startIdx = 0;
    for(int i = startIdx + 1; i < [sortedTransactions count]; i ++){
        if (i - startIdx - 1 < NumberOfTransactionsToLoad){
            [arr_Transactions addObject:[transactions objectAtIndex:i]];
        }
        else {
            break;
        }
    }
}
-(void)handleGetAvailableGiftcardByHistoryResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetHistory class]]){
        [vHistoryGiftcardSelection RemoveIndicatorFooter];
        return;
    }
    @synchronized(arr_Transactions){
    ResponseGetHistory *resp = (ResponseGetHistory*)response;
        if(resp.context == LODEMOREREQ){
            [self handleGetMoreTransactions:resp.transactions];
        }
        else{
            arr_Transactions = [resp.transactions mutableCopy];
            // [NSArray arrayWithArray:arr_Transactions];
        }
    vHistoryGiftcardSelection.transactions = [NSArray arrayWithArray:arr_Transactions];;
    }
    [vHistoryGiftcardSelection refreshData];
    return;


}
#pragma mark – HistoryGiftcardSelectionViewDelegate
-(void)HistorySelectionView:(HistorySelectionView*)selectionView loadMore:(ModelTransaction*)lastObject{
    [self sendHistoryRequest:YES];
}
-(void)HistorySelectionView:(HistorySelectionView*)selectionView giftcardSelected:(ModelTransaction*)giftcard{
    TransactionDetailViewController *ctrl = (TransactionDetailViewController*)[Utils loadViewControllerFromStoryboard:@"History" controllerId:@"TransactionDetailViewController"];
    [TransactionDetailViewController setiIndex:(int)giftcard.transactionType];
    ctrl.transaction = giftcard;
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end
