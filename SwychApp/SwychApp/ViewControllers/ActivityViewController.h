//
//  ActivityViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "HistorySelectionView.h"
#import "ModelTransaction.h"
@interface ActivityViewController : SwychBaseViewController<HistoryGiftcardSelectionViewDelegate>

@property (weak,nonatomic) IBOutlet HistorySelectionView *vHistoryGiftcardSelection;
@property (strong,nonatomic)  NSMutableArray* arr_Transactions;
@end
