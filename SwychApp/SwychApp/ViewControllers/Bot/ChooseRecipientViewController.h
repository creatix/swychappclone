//
//  ChooseRecipientViewController.h
//  SwychApp
//
//  Created by kndev3 on 10/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
@protocol ChooseRecipientViewControllerViewDelegate;
@class ModelBotTransaction;
@class ModelContact;
@class AContactItem;
@interface ChooseRecipientViewController : SwychBaseViewController<UITextFieldDelegate>
{
    NSArray *contactItemArray;
    NSString *_filterCriteria;
    NSInteger _selectedContactIndex;
}
@property (weak,nonatomic) id<ChooseRecipientViewControllerViewDelegate> selectionViewDelegate;
+(ModelContact*)GetBotTransactionFromPhoneOrEmail:(ModelBotTransaction*)transaction;
@end
@protocol ChooseRecipientViewControllerViewDelegate <NSObject>

@optional
-(void)ChooseRecipientViewController:(ChooseRecipientViewController*)selectionView ContactSelected:(AContactItem*)contact;

@end
