//
//  ChooseRecipientViewController.m
//  SwychApp
//
//  Created by kndev3 on 10/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ChooseRecipientViewController.h"
#import "UILabel+Extra.h"
#import "NSArray+Extra.h"
#import "UILabelExt.h"
#import "ChooseRecipientTableViewCell.h"
#import "ContactBookManager.h"
#import "ModelContact.h"
#import "NSString+Extra.h"
#import "ModelBotTransaction.h"
@interface ChooseRecipientViewController ()
@property (weak, nonatomic) IBOutlet UIView *vTop;
@property (weak, nonatomic) IBOutlet UIView *vSecond;
@property (weak, nonatomic) IBOutlet UILabelExt *lblTo;
@property (weak, nonatomic) IBOutlet UITextField *txtRecipientName;
@property (weak, nonatomic) IBOutlet UITableView *tvContactList;
@end

@implementation ChooseRecipientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tvContactList registerNib:[UINib nibWithNibName:@"ChooseRecipientTableViewCell" bundle:nil] forCellReuseIdentifier:@"ChooseRecipientTableViewCell"];
    self.txtRecipientName.delegate = self;
    _filterCriteria = [self GetFirstNameFormBotTransaction];
    self.txtRecipientName.text = _filterCriteria;
     _filterCriteria = [[_filterCriteria lowercaseString] trim];
    [self refreshContactData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma
#pragma mark UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([[str trim] length] > 0 || ([_filterCriteria length] > 0)){
        _filterCriteria = [[str lowercaseString] trim];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self performSelectorOnMainThread:@selector(refreshContactData) withObject:nil waitUntilDone:NO];
        });
    }
    _selectedContactIndex = -1;
    return YES;
}
-(NSString*)GetFirstNameFormBotTransaction{
    NSArray *arrayFiltered = nil;
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    NSString* email = self.botTransaction.recipientEmail;
    NSString* phone = self.botTransaction.recipientPhoneNumber;
    if (![email isEqualToString:@""]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasEmailAddresStartWith:email];
        }];
        arrayFiltered = [array filteredArrayUsingPredicate:pred];
    }
    else if(![phone isEqualToString:@""]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasPhoneNumberContainsWith:phone];
        }];
        arrayFiltered = [array filteredArrayUsingPredicate:pred];
    }
    else{
        arrayFiltered = nil;
    }
    if(arrayFiltered){
        if(arrayFiltered.count>0){
            ModelContact* item = arrayFiltered[0];
            return item.firstName;
        }
    }
    
    return @"";

}
+(ModelContact*)GetBotTransactionFromPhoneOrEmail:(ModelBotTransaction*)transaction{
    NSArray *arrayFiltered = nil;
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    NSString* email = transaction.recipientEmail;
    NSString* phone = transaction.recipientPhoneNumber;
    if (![email isEqualToString:@""]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasEmailAddresStartWith:email];
        }];
        arrayFiltered = [array filteredArrayUsingPredicate:pred];
    }
    else if(![phone isEqualToString:@""]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasPhoneNumberContainsWith:phone];
        }];
        arrayFiltered = [array filteredArrayUsingPredicate:pred];
    }
    else{
        arrayFiltered = nil;
    }
    if(arrayFiltered){
        if(arrayFiltered.count>0){
            ModelContact* item = arrayFiltered[0];
            return item;
        }
    }
    
    return nil;
    
}
-(void)refreshContactData{
    contactItemArray = [self filterContact];
    [self.tvContactList reloadData];
    
}

-(NSArray *)filterContact{
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    NSArray * arrayFiltered = nil;
    if (_filterCriteria == nil || [_filterCriteria length] == 0){
        return  [ContactBookManager getContactItemListByModelList:array];;
    }
    
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [contact hasNameStartWith:_filterCriteria];
    }];
    arrayFiltered =  [array filteredArrayUsingPredicate:pred];
    if(arrayFiltered){
       return  [ContactBookManager getContactItemListByModelList:arrayFiltered];
    }
    return nil;
}
#pragma mask - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(contactItemArray)
        return contactItemArray.count;
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"ChooseRecipientTableViewCell";
    ChooseRecipientTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil){
        cell = [[ChooseRecipientTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    AContactItem *contactItem = [contactItemArray objectAtIndex:indexPath.row];
    cell.contactItem = contactItem;
//    cell.name = contactItem.parentModal.fullName;
//    cell.emailOrPhone = contactItem.Content_Item;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    AContactItem *card = [contactItemArray objectAtIndex:indexPath.row];
    [self ContactSelected:card];
}
-(void)ContactSelected:(AContactItem*) item{
        if(self.selectionViewDelegate != nil && [self.selectionViewDelegate respondsToSelector:@selector(ChooseRecipientViewController:ContactSelected:)]){
            [self.selectionViewDelegate ChooseRecipientViewController:self ContactSelected:item];
            [self.navigationController popViewControllerAnimated:YES];
        }
}

#pragma


@end
