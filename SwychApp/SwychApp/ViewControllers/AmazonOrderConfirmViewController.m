//
//  AmazonOrderConfirmViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AmazonOrderConfirmViewController.h"
#import "UIRoundCornerButton.h"
#import "UILabelExt.h"
#import "AmazonPayPayment.h"
#import "ModelAmazonPaymentInstrument.h"
#import "NSString+Extra.h"
#import "ResponseAmazonGetOrderReferenceDetail.h"
#import "RequestAmazonGetOrderReferenceDetail.h"


@import PayWithAmazon;
@import LoginWithAmazon;

@interface AmazonOrderConfirmViewController ()

@property(nonatomic,weak) IBOutlet UIRoundCornerButton *btnPlaceOrder;

@property(nonatomic,weak) IBOutlet UIView *vPayment;
@property(nonatomic,weak) IBOutlet UILabel *labPayment;
@property(nonatomic,weak) IBOutlet UIButton *btnChangePaymentMethod;

@property(nonatomic,weak) IBOutlet UIView *vBilling;
@property (weak, nonatomic) IBOutlet UILabel *labBillingAddress;
@property (weak, nonatomic) IBOutlet UIView *vBillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *labBillingName;
@property (weak, nonatomic) IBOutlet UILabel *labBillingAddress1;
@property (weak, nonatomic) IBOutlet UILabel *labBillingAddress2;


@property(nonatomic,weak) IBOutlet UIView *vOrder;
@property (weak, nonatomic) IBOutlet UILabel *labYourOrder;
@property (weak, nonatomic) IBOutlet UIView *vOrderInfo;
@property (weak, nonatomic) IBOutlet UIView *vBrand;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardBrand;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardAmount;
@property (weak, nonatomic) IBOutlet UIView *vTax;
@property (weak, nonatomic) IBOutlet UILabel *labTaxValue;
@property (weak, nonatomic) IBOutlet UILabel *labTax;
@property (weak, nonatomic) IBOutlet UIView *vDiscount;
@property (weak, nonatomic) IBOutlet UILabel *labDiscountAmount;
@property (weak, nonatomic) IBOutlet UILabel *labDiscount;
@property (weak, nonatomic) IBOutlet UIView *vTotal;
@property (weak, nonatomic) IBOutlet UILabel *labTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *labTotal;

@property (weak, nonatomic) IBOutlet UIView *vLine;


@property(nonatomic,strong) UIButton *btnCancel;

-(IBAction)buttonTapped:(id)sender;
@end

@implementation AmazonOrderConfirmViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    [self setupViews];
}


-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"PaymentOption_AmazonReviewOrder", nil);
}

-(NSString *)getBackButtonText{
    return NSLocalizedString(@"Button_Cancel_low", nil);
}

-(UIColor*)getBackButtonTextColor{
    return UICOLORFROMRGB(28,102,174,1.0);
}

-(void)setupViews{
    self.vPayment.backgroundColor = UICOLOR_CLEAR;
    self.labPayment.backgroundColor = UICOLOR_CLEAR;
    self.labPayment.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    
    self.vBilling.backgroundColor = UICOLOR_CLEAR;
    self.labBillingAddress.backgroundColor = UICOLOR_CLEAR;
    self.labBillingAddress.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vBillingAddress.backgroundColor = UICOLOR_CLEAR;
    self.labBillingAddress1.backgroundColor = UICOLOR_CLEAR;
    self.labBillingAddress1.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labBillingAddress2.backgroundColor = UICOLOR_CLEAR;
    self.labBillingAddress2.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labBillingName.backgroundColor = UICOLOR_CLEAR;
    self.labBillingName.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    
    self.vOrder.backgroundColor = UICOLOR_CLEAR;
    self.labYourOrder.backgroundColor = UICOLOR_CLEAR;
    self.labYourOrder.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vOrderInfo.backgroundColor = UICOLOR_CLEAR;
    self.vBrand.backgroundColor = UICOLOR_CLEAR;
    self.labGiftcardBrand.backgroundColor = UICOLOR_CLEAR;
    self.labGiftcardBrand.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labGiftcardAmount.backgroundColor = UICOLOR_CLEAR;
    self.labGiftcardAmount.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vTax.backgroundColor = UICOLOR_CLEAR;
    self.labTax.backgroundColor = UICOLOR_CLEAR;
    self.labTax.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labTaxValue.backgroundColor = UICOLOR_CLEAR;
    self.labTaxValue.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vDiscount.backgroundColor = UICOLOR_CLEAR;
    self.labDiscount.backgroundColor = UICOLOR_CLEAR;
    self.labDiscount.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labDiscountAmount.backgroundColor = UICOLOR_CLEAR;
    self.labDiscountAmount.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vTotal.backgroundColor = UICOLOR_CLEAR;
    self.labTotal.backgroundColor = UICOLOR_CLEAR;
    self.labTotal.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.labTotalAmount.backgroundColor = UICOLOR_CLEAR;
    self.labTotalAmount.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    
    self.labPayment.text = NSLocalizedString(@"PaymentOption_AmazonDefaultCard", nil);
    self.labDiscount.text = NSLocalizedString(@"PaymentOption_AmazonDiscount", nil);
    
    //setup elements' value
    self.labGiftcardBrand.text = self.amazonPaymentHandler.purchases.firstObject.lineItem;
    self.labGiftcardAmount.text =[NSString stringWithFormat:@"%.2f",[self.amazonPaymentHandler.totalOrgAmount doubleValue]];
    double discount = [self.amazonPaymentHandler.totalOrgAmount doubleValue] - [self.amazonPaymentHandler.totalAmount doubleValue];
    self.labDiscountAmount.text = [NSString stringWithFormat:@"%.2f",discount];
    self.labTotalAmount.text = [NSString stringWithFormat:@"%.2f",[self.amazonPaymentHandler.totalAmount doubleValue]];
    
    [self.btnChangePaymentMethod setTitleColor:UICOLORFROMRGB(192, 31, 109, 1.0)];
    [self.btnPlaceOrder setTitle:NSLocalizedString(@"PaymentOption_AmazonPlaceOrder", nil)];
    
    [self setPaymentInstrumentInfo];
}

-(void)setNavBarInfo{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    NSString *backButtonText = [self getBackButtonText];
    if (backButtonText != nil){
        UIImage *imgBack = nil; [UIImage imageNamed:@"navbar_back.png"];
        self.btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50.0f, 40.0f)];
        [self.btnCancel addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnCancel setTitle:backButtonText forState:UIControlStateNormal];
        [self.btnCancel setTitle:backButtonText forState:UIControlStateHighlighted];
        [self.btnCancel setBackgroundImage:imgBack forState:UIControlStateNormal];
        [self.btnCancel setBackgroundImage:imgBack forState:UIControlStateHighlighted];
        self.btnCancel.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        
        UIColor *clr = [self getBackButtonTextColor];
        if (clr != nil){
            [self.btnCancel setTitleColor:clr forState:UIControlStateNormal];
            [self.btnCancel setTitleColor:clr forState:UIControlStateHighlighted];
        }
        self.bbiLeft = [[UIBarButtonItem alloc] initWithCustomView:self.btnCancel];
        
        self.navigationItem.leftBarButtonItem  = self.bbiLeft;
        self.navigationController.navigationBarHidden = NO;
        
        NSString *title = [self getNavTitleViewText];
        if (title != nil){
            UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imgBack.size.width, imgBack.size.height)];
            v.backgroundColor = [UIColor clearColor];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:v];
            UILabelExt *labTitle = [[UILabelExt alloc] initWithFrame:CGRectMake(0, 20, 50, 44)];
            labTitle.font = [UIFont fontWithName:@"Helvetica Nene" size:16.0f];
            labTitle.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
            labTitle.text = title;
            labTitle.verticalAlignment = VerticalAlignment_Middle;
            self.navigationItem.titleView = labTitle;
        }
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setPaymentInstrumentInfo{
    if(self.paymentInstrument != nil){
        if([NSString isNullOrEmpty:self.paymentInstrument.accountNumberTail]){
            self.labPayment.text = self.paymentInstrument.name;
        }
        else{
            self.labPayment.text = [NSString stringWithFormat:@"%@ %@",self.paymentInstrument.name,self.paymentInstrument.accountNumberTail];
        }
    }
}

-(IBAction)buttonTapped:(id)sender{
    if (sender == self.btnCancel){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if(sender == self.btnChangePaymentMethod){
        [self changePaymentMethod];
    }
    else if(sender == self.btnPlaceOrder){
        [self.amazonPaymentHandler processPaymentWithAuthorization:^(BOOL success){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}

-(void)changePaymentMethod{
    PWAChangeOrderDetailsRequest *changePaymentMethodRequest = [[PWAChangeOrderDetailsRequest alloc]
                                                                initWithAmazonOrderReferenceID:self.amazonPaymentHandler.amazonOrderReferenceId];
    
    NSLog(@"Making Change Payment Method PWA API call");
    [PayWithAmazon changePaymentMethod:changePaymentMethodRequest
                     completionHandler:^(PWAChangeOrderDetailsResponse *response, NSError *error) {
                         if (!error) {
                             [self sendGetAmazonOrderReferenceDetailRequest:response.amazonOrderReferenceId];
                         } else {
                             [self showAlert:[error localizedDescription]];
                         }
                     }];
}


-(void)sendGetAmazonOrderReferenceDetailRequest:(NSString*)orderId{
    RequestAmazonGetOrderReferenceDetail *req = [[RequestAmazonGetOrderReferenceDetail alloc] init];
    req.amazonToken = self.amazonPaymentHandler.accessToken;
    req.amazonReferenceId = orderId;
    req.amazonQueryString = @"QAZWSX";
    
    SwychBaseViewController *ctrl = [APP getCurrentViewController];
    [ctrl showLoadingView:nil];
    SEND_REQUEST(req, handGetAmazonOrderDetailResponse, handGetAmazonOrderDetailResponse);
}
#pragma
#pragma mark - server conmunication handling
-(void)handGetAmazonOrderDetailResponse:(ResponseBase*)response{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponseAmazonGetOrderReferenceDetail *resp = (ResponseAmazonGetOrderReferenceDetail*)response;
    self.paymentInstrument = resp.paymentInstrument;
    [self setPaymentInstrumentInfo];
}

@end
