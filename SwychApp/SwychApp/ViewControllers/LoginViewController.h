//
//  LoginViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import <Google/SignIn.h>
#import "ManullyUpdateBalance.h"
#import "ForgotPasswordPopupView.h"
@class UIRoundCornerButton;
@class UICheckbox;


@interface LoginViewController : SwychBaseViewController<GIDSignInUIDelegate,UpdateWithTextFieldPopUpDelegate,ForgotPasswordPopupViewDelegate>{
    NSString* phoneunmber;
}

@property (nonatomic,assign) LoginViewType viewType;

@property (nonatomic,assign) BOOL promptOTP;

@property (weak, nonatomic) IBOutlet UIView *vTopView;
@property (weak, nonatomic) IBOutlet UIView *vMiddleView;
@property (weak, nonatomic) IBOutlet UIView *vSocialButtonsView;

@property (weak, nonatomic) IBOutlet UILabel *labWelcomeTo;
@property (weak, nonatomic) IBOutlet UIImageView *vSwychLogo;

@property (weak, nonatomic) IBOutlet UILabel *labLoginWith;
@property (weak, nonatomic) IBOutlet UIView *vEmailAndPassword;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIImageView *ivEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithGoogle;

@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UICheckbox *chkTouchID;
@property (strong, nonatomic) LocalUserInfo *localUser;

@end
