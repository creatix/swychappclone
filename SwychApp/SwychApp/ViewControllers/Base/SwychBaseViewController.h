//
//  SwychBaseViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertViewNormal.h"
#import "ReceiveGiftcardPopupView.h"
#import "ModelRegiftParameters.h"


@class ModelGiftCard;
@class AppDelegate;
@class ProgressView;
@class ResponseBase;
@class ModelTransaction;
@class NavTitleView;
@class ModelUser;
@class ViewOTPVerification;


@interface SwychBaseViewController : UIViewController<AlertViewDelegate,ReceiveGiftcardPopupViewDelegate,UIPopoverControllerDelegate>{
    NSMutableArray *_textFields;
    ProgressView *_progressView;
}

@property(nonatomic,strong) UIBarButtonItem *bbiLeft;
@property(nonatomic,strong) UIBarButtonItem *bbiRight;

@property(nonatomic,strong) IBOutlet UIImageView *ivBackground;
@property(nonatomic,strong) ReceiveGiftcardPopupView *giftcardReceivedPopupView;
@property(nonatomic,strong) ModelRegiftParameters *regiftParam;
@property (nonatomic,readonly) BOOL regiftMode;
@property(nonatomic,strong) NavTitleView *navTitleView;
@property (strong, nonatomic) UIPopoverController* popover;
@property (nonatomic,strong) ModelBotTransaction* botTransaction;
@property (nonatomic) BOOL processBot;
@property (nonatomic) BOOL processBotLinking;
-(BOOL)hideBackButton;
-(BOOL)showTopRightSearchButton;
-(void)navbarBackButtonTapped;

-(NSString*)getNavTitleViewText;

-(BOOL)addBackgroundViewIfItIsNull;

-(void)sendUserLoginEvent:(NSString*)userIdentifier loginMethod:(NSString*)method success:(BOOL)success;
-(void)sendAnalyticsEvent:(NSString*)eventName attributes:(NSDictionary*)attributes;

-(BOOL)hideTabbar;

-(NSString*)getViewName;

-(BOOL)ignoreToSetInfo:(id)object;
-(BOOL)setControlsPropertiesBaseOnConfigurationFile;

-(NSString *)getBackButtonText;
-(UIColor *)getBackButtonTextColor;

-(void)setNavBarInfo;
-(void)navbarLeftButtonTapped;
-(void)setUILabelTextColor:(UILabel*)label;
-(void)setUITextFieldTextColor:(UITextField*)textField;
-(void)setUITextFieldPlaceholderColor:(UITextField*)textFiled;
-(void)setUITextViewTextColor:(UITextView*)textView;

-(void)showAlert:(NSString*)message;
-(void)showAlert:(NSString*)message title:(NSString*)title;
-(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id)del firstButton:(NSString*)first secondButton:(NSString*)second;
-(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id)del firstButton:(NSString*)first secondButton:(NSString*)second context:(NSInteger)context contextObject:(NSObject*)obj;

-(void)showLoadingView:(NSString*)loadingText;
-(void)dismissLoadingView;

-(UITextField*)getNextInputTextField:(UITextField*)textField;

-(void)dismissSlidingMenu:(BOOL)annimation;

-(BOOL)handleServerResponse:(ResponseBase*)response;
-(BOOL)handleSessionExpiredResponse:(ResponseBase*)response;

-(void)showNewGiftcardReceivedPopupView:(NSArray<ModelTransaction*>*)transactions;

-(void)checkNewGift;
-(void)AddTopRightSearchButton;
-(void)AddTitle:(NSString*)title;
-(void)addNotificationObserverByName:(NSString *)notiName;
-(void)removeNotificationObserverByName:(NSString *)notiName;
-(void)postLocalNotification:(NSString*)notiName userInfo:(NSDictionary*)userInfo;
-(BOOL)didReceivedLocalNotification:(NSNotification*)noti;
-(void)sendGetBotSentGiftListsRequest;
-(void)sendGetBotLinkRequest;
-(void)showUserInfoUpdatePopupView;
-(void)showUserInfoUpdatePopupViewWithContext:(NSInteger)context contextObject:(NSObject*)contextObject redeem:(BOOL)redeem;
-(void)userInfoUpdated:(ModelUser*)user context:(NSInteger)context contextObject:(NSObject*)contextObject;
-(void)KeyboardDoneClicked;
-(void)PopoverFromIpad:(UIImagePickerController *)picker;
-(BOOL)ValidatePassword:(int)context;
-(void)EmailValidate:(void(^)(void))donextstep Email:(NSString*)email;
@property (copy)void (^NextStep)(void);


//OTP verificaiton
-(void)sendResendOTPRequest:(ViewOTPVerification*)v;
-(void)handleResendOTPResponse:(ResponseBase*)response;
-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v;
-(void)handleOTPVerificationResponse:(ResponseBase*)response;
-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView;
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP;

@end
