//
//  SwychBaseViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "SWRevealViewController.h"
#import "UITextField+Extra.h"
#import "AppDelegate.h"
#import "Analytics.h"
#import "PopupViewHandler.h"
#import "ProgressView.h"
#import "UIButton+Extra.h"
#import "ObjectConfigInfo.h"
#import "ResponseBase.h"
#import "ModelGiftCard.h"
#import "ReceivedGiftcardDetailViewController.h"
#import "ModelTransaction.h"
#import "NavTitleView.h"
#import "UILabelExt.h"
#import "UserInfoUpdatePopupView.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "PaymentConstants.h"
#import "RequestGetSuggestedGiftcards.h"
#import "ResponseGetSuggestedGiftcards.h"
#import "RequestGetUnacknowledgedGiftcards.h"
#import "ResponseGetUnacknowledgedGiftcards.h"
#import "RequestAcknowledgeGiftcard.h"
#import "ResponseAcknowledgeGiftcard.h"
#import "ManullyUpdateBalance.h"
#import "RequestValidatePassword.h"
#import "ResponseValidatePassword.h"
#import "RequestPreGift.h"
#import "ViewOTPVerification.h"
#import "RequestResendOTP.h"
#import "RequestVerifyOTP.h"
#import "ResponseVerifyOTP.h"
#import "RequestBotSentGiftLists.h"
#import "ResponseBotSentGiftLists.h"
#import "RequestBotLinkingLists.h"
#import "ResponseBotLinkingLists.h"
#import "ResponseBotLinkConfirm.h"
#import "RequestBotLinkConfirm.h"
#import "ModelBotLink.h"
#import "GiftDetailViewController.h"
#import "ModelContact.h"
#import "ChooseRecipientViewController.h"
#import "RequestBotCancelTransaction.h"


#define Context_ReceivedGiftcard    0x01
#define Context_SessionExpired      0x02
#define Context_BotLink             0x03
#define Context_BotTransaction             0x04
#define Context_BotCancelTransaction        0x05

#define IMG_OFFSET       7.0

extern NSString *Noti_TouchOnView;

@interface SwychBaseViewController()
@property (nonatomic,strong) UIButton *backButton;
@property (nonatomic,strong) ModelBotLink *botLinkModel;
-(void)setControlPropertiesBaseOnConfiguratinFile:(UIView *)view;
@end

@implementation SwychBaseViewController

-(BOOL)regiftMode{
    return self.regiftParam != nil && self.regiftParam.originalGiftcard != nil;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        self.hidesBottomBarWhenPushed = [self hideTabbar];
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
     [self appWillEnterForeGround];
    self.navigationController.tabBarController.tabBar.hidden = [self hideTabbar];
    if (self.ivBackground == nil && [self addBackgroundViewIfItIsNull]){
        self.ivBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                          self.view.bounds.size.width
                                                                          , self.view.bounds.size.height)];
        
        self.ivBackground.image = [UIImage imageNamed:@"background_registration.png"];
        
        self.ivBackground.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view insertSubview:self.ivBackground atIndex:0];

    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    [self setNavBarInfo];
    if ([self setControlsPropertiesBaseOnConfigurationFile]){
        [self setControlPropertiesBaseOnConfiguratinFile:self.view];
    }
    
    if ([_textFields count] > 0){
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"tag" ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return [obj1 intValue] > [obj2 intValue];
        }];
        [_textFields sortUsingDescriptors:@[descriptor]];
    }
    
    if ([self.navigationController.viewControllers count] == 1)
    {
        //SWRevealViewController *revealController = [self revealViewController];
        //[revealController panGestureRecognizer];
        //[revealController tapGestureRecognizer];
    }
    if ([self hideBackButton] && self.backButton != nil){
        self.backButton.hidden = YES;
    }
    else{
        self.backButton.hidden = NO;
    }

}
- (id)appWillEnterForeGround{ //Application will enter foreground.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allFunctions)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    return self;
}


-(void) allFunctions{ //call any functions that need to be run when application will enter foreground
    NSLog(@"calling all functions...application just came back from foreground");
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSString *viewName = [self getViewName];
    if (viewName != nil){
        [self sendAnalyticsEvent:ANA_EVENT_VIEW_Displayed attributes:@{ANA_ATT_VIEW_Displayed_ViewName:viewName}];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Do view manipulation here.
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.popupViewHandler relayoutSubviews:CGRectMake(0, 0, size.width, size.height)];
}

-(BOOL)hideTabbar{
    return YES;
}

-(void)setUILabelTextColor:(UILabel*)label{
}
-(void)setUITextFieldTextColor:(UITextField*)textField{
}
-(void)setUITextFieldPlaceholderColor:(UITextField*)textFiled{
}
-(void)setUITextViewTextColor:(UITextView*)textView{
}


-(NSString*)getTitleText{
    return nil;
}
-(void)touchOnView:(id)sender{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:Noti_TouchOnView object:nil];
}

-(void)sendUserLoginEvent:(NSString*)userIdentifier loginMethod:(NSString*)method success:(BOOL)success{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    [app.analyticsInstance logLoginWithMethod:method
//                                       sucess:[NSNumber numberWithBool:success]
//                                   attributes:userIdentifier == nil ? @{} : @{ANA_ATT_USER_Login_UserName:userIdentifier}];
}

-(void)sendAnalyticsEvent:(NSString*)eventName attributes:(NSDictionary*)attributes{
//    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    [app.analyticsInstance logCustomEvent:eventName attributes:attributes];
}


-(NSString*)getViewName{
    return NSStringFromClass([self class]);
}

-(NSString *)getBackButtonText{
    return @"";// NSLocalizedString(@"Button_Cancel", nil);
}
-(UIColor *)getBackButtonTextColor{
    return  UICOLORFROMRGB(192, 31, 109, 1.0);
}

-(BOOL)hideBackButton{
    return NO;
}

-(BOOL)showTopRightSearchButton{
    return YES;
}

-(void)setNavBarInfo{
    NSUInteger viewControllers =[self.navigationController.viewControllers count];
    if (viewControllers >  1){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
        
        NSString *backButtonText = [self getBackButtonText];
        if (viewControllers > 1 && backButtonText != nil){
            UIImage *imgBack = [UIImage imageNamed:@"navbar_back.png"];
            self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imgBack.size.width, imgBack.size.height)];
            [self.backButton addTarget:self action:@selector(navbarBackButtonTapped) forControlEvents:UIControlEventTouchUpInside];
            [self.backButton setTitle:backButtonText forState:UIControlStateNormal];
            [self.backButton setTitle:backButtonText forState:UIControlStateHighlighted];
            [self.backButton setBackgroundImage:imgBack forState:UIControlStateNormal];
            [self.backButton setBackgroundImage:imgBack forState:UIControlStateHighlighted];
            UIColor *clr = [self getBackButtonTextColor];
            if (clr != nil){
                [self.backButton setTitleColor:clr forState:UIControlStateNormal];
                [self.backButton setTitleColor:clr forState:UIControlStateHighlighted];
            }
            self.bbiLeft = [[UIBarButtonItem alloc] initWithCustomView:self.backButton];
            
            self.navigationItem.leftBarButtonItem  = self.bbiLeft;
            self.navigationController.navigationBarHidden = NO;
            
            NSString *title = [self getNavTitleViewText];
            if (title != nil){
                UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imgBack.size.width, imgBack.size.height)];
                v.backgroundColor = [UIColor clearColor];
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:v];
                UILabelExt *labTitle = [[UILabelExt alloc] initWithFrame:CGRectMake(0, 20, 50, 44)];
                labTitle.font = [UIFont fontWithName:@"Helvetica Nene" size:16.0f];
                labTitle.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
                labTitle.text = title;
                labTitle.verticalAlignment = VerticalAlignment_Middle;
                self.navigationItem.titleView = labTitle;
            }
        }
    }
    else{
        
        //SWRevealViewController *revealController = [self revealViewController];
        
        UIImage *imgTopLeftIcon = [UIImage imageNamed:@"navbar_top_left_icon.png"];
        CGRect rc = CGRectMake(0, 0, imgTopLeftIcon.size.width , imgTopLeftIcon.size.height);
        CGRect rc1 = CGRectMake(0, 0, imgTopLeftIcon.size.width+IMG_OFFSET , imgTopLeftIcon.size.height+IMG_OFFSET);
        UIButton *btn = [[UIButton alloc] initWithFrame:rc1];
        //[btn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [btn addTarget:self action:@selector(hamburgerMenuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        UIImage *img = [[ApplicationConfigurations instance] getAvatarImage];
        if(img){
            
            imgTopLeftIcon = [Utils resizeImage:img WithWidth:rc1.size.width withHeight:rc1.size.height withKeepAspectRatio:YES];
            btn.layer.cornerRadius = imgTopLeftIcon.size.width/2;
            btn.layer.masksToBounds = YES;
        }
        [btn setBackgroundImage:imgTopLeftIcon forState:UIControlStateNormal];
        [btn setBackgroundImage:imgTopLeftIcon forState:UIControlStateHighlighted];
        self.bbiLeft = [[UIBarButtonItem alloc] initWithCustomView:btn];
        
        self.navigationItem.leftBarButtonItem  = self.bbiLeft;
        
        
        self.navTitleView = (NavTitleView*)[Utils loadViewFromNib:@"NavTitleView" viewTag:1];
        self.navTitleView.constratintSwychLogoHeight.constant = imgTopLeftIcon.size.height-IMG_OFFSET;
        self.navigationItem.titleView = self.navTitleView;
        self.navTitleView.titleText = [self getNavTitleViewText];
        
        if ([self showTopRightSearchButton]){
            UIImage *imgTopRightIcon = [UIImage imageNamed:@"navbar_top_right_icon.png"];
            rc = CGRectMake(0, 0, imgTopRightIcon.size.width -IMG_OFFSET, imgTopRightIcon.size.height-IMG_OFFSET);
            btn = [[UIButton alloc] initWithFrame:rc];
            [btn addTarget:self action:@selector(executeSearch:) forControlEvents:UIControlEventTouchUpInside];
//            [btn setBackgroundImage:imgTopRightIcon forState:UIControlStateNormal];
//            [btn setBackgroundImage:imgTopRightIcon forState:UIControlStateHighlighted];
            self.bbiRight = [[UIBarButtonItem alloc] initWithCustomView:btn];
            
            self.navigationItem.rightBarButtonItem  = self.bbiRight;

        }
    }
}
-(void)AddTopRightSearchButton{
    CGRect rc = CGRectZero;
    UIButton *btn = [[UIButton alloc] initWithFrame:rc];
    UIImage *imgTopRightIcon = [UIImage imageNamed:@"navbar_top_right_icon.png"];
    rc = CGRectMake(0, 0, imgTopRightIcon.size.width , imgTopRightIcon.size.height);
    btn = [[UIButton alloc] initWithFrame:rc];
    [btn setBackgroundImage:imgTopRightIcon forState:UIControlStateNormal];
    [btn setBackgroundImage:imgTopRightIcon forState:UIControlStateHighlighted];
    self.bbiRight = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem  = self.bbiRight;

}
-(void)AddTitle:(NSString*)title{
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.text = title;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor grayColor];
    lblTitle.shadowColor = [UIColor whiteColor];
    lblTitle.shadowOffset = CGSizeMake(0, 1);
    lblTitle.font = [UIFont fontWithName:@"Helvetica Neue" size:18.0];
    [lblTitle sizeToFit];
    self.navigationItem.titleView = lblTitle;
}
-(void)hamburgerMenuButtonTapped:(id)sender{
    [APP showHideHamburgerMenu];
}

-(NSString*)getNavTitleViewText{
    return nil;
}
-(BOOL)addBackgroundViewIfItIsNull{
    return YES;
}

-(BOOL)ignoreToSetInfo:(id)object{
    return NO;
}

-(BOOL)setControlsPropertiesBaseOnConfigurationFile{
    return YES;
}
-(void)PopoverFromIpad:(UIImagePickerController *)picker{

        [picker setModalPresentationStyle:UIModalPresentationFullScreen];
        self.popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        [self.popover setDelegate:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.popover presentPopoverFromRect:CGRectMake(0, 0, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        });

}
-(void)setControlPropertiesBaseOnConfiguratinFile:(UIView *)view{
    if (![self ignoreToSetInfo:view]){
        UIView *superView = view.superview;
        BOOL setProperty = YES;
        if (superView != nil && [superView respondsToSelector:@selector(ignoreToSetInfo:)]){
            NSMethodSignature *sig = [[superView class] instanceMethodSignatureForSelector:@selector(ignoreToSetInfo:)];
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
            [invocation setSelector:@selector(ignoreToSetInfo:)];
            [invocation setTarget:superView];
            [invocation setArgument:&view atIndex:2];
            [invocation invoke];
            BOOL ignore = NO;
            [invocation getReturnValue:&ignore];
            setProperty = !ignore;
        }
        if (!setProperty){
            return;
        }
        if ([view isKindOfClass:[UITextField class]]){
            ObjectConfigInfo *config = [[ApplicationConfigurations instance] getConfiguration:[self getViewName] object:view];
            if (config != nil){
                UITextField *txt = (UITextField*)view;
                if (config.textColor != nil){
                    txt.textColor = config.textColor;
                }
                if (config.placeHolderColor != nil){
                    [txt setPlaceHolderColor:config.placeHolderColor];
                }
                if (config.backgroundColor != nil){
                    txt.backgroundColor = config.backgroundColor;
                }
                if (config.font != nil){
                    txt.font = config.font;
                }
            }
            
            if (_textFields == nil){
                _textFields = [[NSMutableArray alloc] initWithCapacity:1];
            }
            [_textFields addObject:view];
        }
        else if ( [view isKindOfClass:[UILabel class]]){
            ObjectConfigInfo *config = [[ApplicationConfigurations instance] getConfiguration:[self getViewName] object:view];
            if (config != nil){
                UILabel *lab = (UILabel*)view;
                if (lab.restorationIdentifier != nil){
                    DEBUGLOG(@"Seting color for label: %@",lab.restorationIdentifier);
                }
                if (config.textColor != nil){
                    lab.textColor = config.textColor;
                }
                if (config.backgroundColor != nil){
                    lab.backgroundColor = config.backgroundColor;
                }
                if (config.font != nil){
                    lab.font = config.font;
                }
            }
        }
        else if ( [view isKindOfClass:[UITextView class]]){
            ObjectConfigInfo *config = [[ApplicationConfigurations instance] getConfiguration:[self getViewName] object:view];
            if (config != nil){
                UITextView *tv = (UITextView*)view;
                if (config.textColor != nil){
                    tv.textColor = config.textColor;
                }
                if (config.backgroundColor != nil){
                    tv.backgroundColor = config.backgroundColor;
                }
                if (config.font != nil){
                    tv.font = config.font;
                }
            }
            
            
            [self setUITextViewTextColor:(UITextView*)view];
        }
        else if ( [view isKindOfClass:[UIButton class]]){
            ObjectConfigInfo *config = [[ApplicationConfigurations instance] getButtonConfiguration:[self getViewName]];
            if(config != nil){
                [(UIButton*)view setButtonInfo:config];
            }
        }
        else {
            for(UIView *v in view.subviews){
                [self setControlPropertiesBaseOnConfiguratinFile:v];
            }
        }
    }
}

-(void)showAlert:(NSString*)message{
    [self showAlert:message title:nil];
}

-(void)showAlert:(NSString*)message title:(NSString*)title{
    [self showAlert:message title:title delegate:self firstButton:nil secondButton:nil];
}

-(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id)del firstButton:(NSString*)first secondButton:(NSString*)second{
    [Utils showAlert:message title:title delegate:del firstButton:first secondButton:second];
}

-(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id)del firstButton:(NSString*)first secondButton:(NSString*)second context:(NSInteger)context contextObject:(NSObject*)obj{
    [Utils showAlert:message title:title delegate:del firstButton:first secondButton:second withContext:context withContextObject:obj checkboxText:nil];
}

-(void)showLoadingView:(NSString*)loadingText{
    if (_progressView == nil){
        _progressView = (ProgressView*)[Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_ProgressView];
    }
    if (loadingText != nil){
        _progressView.labLoading.text = loadingText;
    }
    else{
        _progressView.labLoading.text = NSLocalizedString(@"Progress_Message_Loading", nil);
    }
    [_progressView startAnimation];
    [Utils showPopupView:_progressView];
}
-(void)dismissLoadingView{
    if(_progressView){
        [_progressView dismiss];
    }
}

-(void)navbarBackButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)navbarLeftButtonTapped{
    
}
-(void)KeyboardDoneClicked{

}
-(UITextField*)getNextInputTextField:(UITextField*)textField{
    if (_textFields == nil || [_textFields count] == 0){
        return nil;
    }
    NSInteger idx = [_textFields indexOfObject:textField];
    if (idx >= 0 && idx < [_textFields count] - 1){
        return [_textFields objectAtIndex:idx+ 1];
    }
    return nil;
}


-(void)dismissSlidingMenu:(BOOL)annimation{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController RemoveOverLayView];
    [revealController setFrontViewPosition:FrontViewPositionLeft animated:annimation];
}


-(void)showNewGiftcardReceivedPopupView:(NSArray<ModelTransaction*>*)transactions{
    ReceiveGiftcardPopupView *v = (ReceiveGiftcardPopupView*)[Utils loadViewFromNib:@"ReceiveGiftcardPopupView" viewTag:1];
    v.transactions = transactions;
    v.receiveGiftcardPopupViewDelegate = self;
    v.context = Context_ReceivedGiftcard;
    [v show];
}

-(void)showNewGiftcardDetailView:(ModelTransaction*)transaction suggestedGiftcards:(NSArray<ModelGiftCard*>*)suggestedGiftcards selectedIndex:(NSUInteger)selectedIndex{
    ReceivedGiftcardDetailViewController *ctrl = (ReceivedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"ReceivedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.suggestedGiftcards = suggestedGiftcards;
    ctrl.selectedIndex = selectedIndex;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)checkNewGift{
    [self sendGetUnacknowledgedGiftcardsRequest];
}

-(void)addNotificationObserverByName:(NSString *)notiName{
    [Utils addNotificationObserver:self selector:@selector(didReceivedLocalNotification:) name:notiName object:nil];
}
-(void)removeNotificationObserverByName:(NSString *)notiName{
    [Utils removeNotificationObserver:self name:notiName object:nil];
}
-(void)postLocalNotification:(NSString*)notiName userInfo:(NSDictionary*)userInfo{
    [Utils postLocalNotification:notiName object:nil userInfo:userInfo];
}

- (void) executeSearch:(id)sender {
    
}

-(BOOL)didReceivedLocalNotification:(NSNotification*)noti{
    return YES;
}
-(void)EmailValidate:(void(^)(void))donextstep Email:(NSString*)email{
    self.NextStep = donextstep;
    [self ValidateOPTByEmail:email];
}
-(BOOL)ValidateOPTByEmail:(NSString*)email{
    PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate =(id<OTPVerificationDelegate>) self;
        vVerification.bTapDissMiss = NO;
        [vVerification setMobilePhoneNumber:email];
        vVerification.textCode = NSLocalizedString(@"JustEmailedCodeTo", nil);
        [Utils showPopupView:(PopupViewBase*)v];
    }
    return YES;
}
#pragma makr = OTPVerificationDelegate
-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView{
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    ModelUser *user = localData;
    [self sendResendOTPRequest:user];
}
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP{
    [self sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
}
-(void)sendResendOTPRequest:(ModelUser*)user{
    [self showLoadingView:nil];
    RequestResendOTP *req = [[RequestResendOTP alloc] init];
    req.swychId = user.swychId;
    req.otpSentType = OTPSENTTYPE_EMAILVALIDATION;
    
    SEND_REQUEST(req, handleResendOTPResponse, handleResendOTPResponse);
}


-(void)handleResendOTPResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        [self showAlert:response.errorMessage];
    }
}
-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    [self showLoadingView:nil];
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    user = localData;
    RequestVerifyOTP *req = [[RequestVerifyOTP alloc] init];
    req.contextObject = v;
    req.swychId = user.swychId;
    req.otp = [v getOTPCode];
    req.context = v.context;
    req.phoneNumber = [v getMobilePhoneNumber];
    req.otpSentType = OTPSENTTYPE_EMAILVALIDATION;
    SEND_REQUEST(req, handleOTPVerificationResponse, handleOTPVerificationResponse);
}

-(void)handleOTPVerificationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    
    if (![self handleServerResponse:response]){
        
        ViewOTPVerification *v = (ViewOTPVerification*)response.contextObject;
        ResponseVerifyOTP *resp = (ResponseVerifyOTP*)response;
        [v dismiss];
        if(self.NextStep!=nil)
            self.NextStep();

    }
}
#pragma mark - payment validate Password
-(BOOL)ValidatePassword:(int)context{
    ManullyUpdateBalance *v = (ManullyUpdateBalance*)[Utils loadViewFromNib:@"ManullyUpdateBalance" viewTag:10];
    v.labTitle.text = NSLocalizedString(@"ValidatePasswordTitle",nil);
    NSString* message = NSLocalizedString(@"enterpassword",nil);
    v.ContextID = context;
    v.labWeHaveTexted.text = message;
    v.txtUpdateBalnce.secureTextEntry = YES;
    [v.txtUpdateBalnce setKeyboardType:UIKeyboardTypeDefault];
    v.uDelegate = self;
    [Utils showPopupView:(PopupViewBase*)v];
    return YES;
    
}
-(void)UpdateContext:(NSString*)text message:(NSString*)textmessage view:(ManullyUpdateBalance*)view{
    [self showLoadingView:nil];
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestValidatePassword *req = [[RequestValidatePassword alloc] init];
    req.password = text;
    req.swychId = localData.swychId;
    req.context = view.ContextID;
    SEND_REQUEST(req, handleValidatePassowrdResponse, handleValidatePassowrdResponse)
}
-(void)handleValidatePassowrdResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    ResponseValidatePassword*resp = (ResponseValidatePassword*)response;
    if (resp.success){
        if(resp.context == PAY_METHOD_APPPLE_PAY){
            
            [self makePayment:PaymentMethod_ApplePay];
        }
        else if(resp.context == PAY_METHOD_PAYPAL){
            [self makePayment:PaymentMethod_PayPal];
            
        }
        else if(resp.context == PAY_METHOD_CREDITCARD){
            [self makePayment:PaymentMethod_DirectCreditCard];
        }
        else if(resp.context == PAY_METHOD_AMAZON){
            [self makePayment:PaymentMethod_AmazonPay];
        }
        else if(resp.context == PAY_METHOD_NONE){
            [self makePayment:PaymentMethod_None];
        }
    }
    else{
        [Utils showAlert:resp.errorMessage delegate:nil];
    }
}
-(void)makePayment:(PaymentMethod)method{

}
#pragma
#pragma mark - PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
//    if (confirmationView.context == Context_ReceivedGiftcard){
//        //Do save gift card work, then show detail
//        ReceiveGiftcardPopupView *v = (ReceiveGiftcardPopupView*)confirmationView;
//        [self showLoadingView:nil];
//        [self sendGetSuggestedGiftcardsRequest:v.giftcard];
//    }
}

#pragma 
#pragma mark - ReceiveGiftcardPopupViewDelegate
-(void)receiveGiftcardPopupView:(ReceiveGiftcardPopupView *)popupView giftcardAccepted:(ModelTransaction *)transaction{
    [self showLoadingView:nil];
    [self sendAcknowledgeGiftcardsRequest:[NSArray arrayWithObject:transaction.transactionId] withTransaction:transaction];
}

#pragma
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal *)alertView buttonTappedWithIndex:(NSInteger)index{
    if(alertView.context == Context_SessionExpired){
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [app switchToLoginView:NO preview:@""];
        return YES;
    }
    if(alertView.context == Context_BotLink){
        BOOL ifConfirm = (index==2?YES:NO);
        dispatch_async(dispatch_get_main_queue(), ^{
        [self sendBotLinkConfirmInformationRequest:ifConfirm];
            });
        return YES;
    }
    if(alertView.context == Context_BotTransaction){
        BOOL ifConfirm = (index==2?YES:NO);
        if(ifConfirm){
        // start to send card
            ModelContact* contact = [ChooseRecipientViewController GetBotTransactionFromPhoneOrEmail:self.botTransaction];
             GiftDetailViewController *ctrl = (GiftDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftDetailViewController"];
             ctrl.giftcard = self.botTransaction.giftCard;
             ctrl.contact = contact;
             ctrl.giftForFriend = YES;
             ctrl.botTransaction = self.botTransaction;
             ctrl.processBot = self.processBot;
             ctrl.regiftParam = self.regiftParam;
             [self.navigationController pushViewController:ctrl animated:YES];

        }
        else{
        // to cancel the transation,
            self.processBot = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendCancelBotSentGiftRequest:self.botTransaction.botTransactionId];
            });
            
        }
    }
    return YES;
}

#pragma 
#pragma mark - Update user info
-(void)showUserInfoUpdatePopupView{
    [self showUserInfoUpdatePopupViewWithContext:0 contextObject:nil redeem:NO];
}

-(void)showUserInfoUpdatePopupViewWithContext:(NSInteger)context contextObject:(NSObject*)contextObject  redeem:(BOOL)redeem{
    UserInfoUpdatePopupView *uip = (UserInfoUpdatePopupView *)[Utils loadViewFromNib:@"UserInfoUpdatePopupView" viewTag:0];
    uip.delegateUpdate = (id<UserInfoUpdateViewDelegate>)self;
    uip.contextObj = contextObject;
    uip.context = context;
    uip.selfGifting = !redeem;
    
    [uip show];
}

-(void)userInfoUpdateView:(UserInfoUpdateView*)view infoUpdated:(ModelUser*)user{
    if (user != nil){
        [self userInfoUpdated:user context:view.context contextObject:view.contextObj];
    }
}

-(void)userInfoUpdated:(ModelUser*)user  context:(NSInteger)context contextObject:(NSObject*)contextObject{
    
}
#pragma 
#pragma mark - Handle failed server response
-(BOOL)handleSessionExpiredResponse:(ResponseBase*)response{
    if (response.errorCode == ERR_SessionExpired){
        [APP setCallContactSyncResetNeeded:YES];
        [self showAlert:response.errorMessage title:nil delegate:self firstButton:nil secondButton:nil context:Context_SessionExpired contextObject:nil];
        return  YES;
    }
    return NO;
}

-(BOOL)handleServerResponse:(ResponseBase*)response{

    if (response.success){
        if(response.pullRequests.botSendGiftcard){
            if(!self.processBot){
//            [[NSNotificationCenter defaultCenter] postNotificationName:BotNewTransactionNotification object:nil];
//                [self sendGetBotSentGiftListsRequestWithOutNotification];
            }
        }
        if(response.pullRequests.botLinking){
            
            //[[NSNotificationCenter defaultCenter] postNotificationName:BotNewLinkNotification object:nil];
        }
        return NO;
    }
    else{
        if ([self handleSessionExpiredResponse:response]){
            return YES;
        }
        [self showAlert:response.errorMessage];
        return YES;
    }
}

-(void)sendGetBotSentGiftListsRequest{
    [APP swichToGiftView];
    [self sendGetBotSentGiftListsRequestWithOutNotification];
}
-(void)sendGetBotSentGiftListsRequestWithOutNotification{
    if(self.processBot) return;
    self.processBot = YES;
    
    RequestBotSentGiftLists *req = [[RequestBotSentGiftLists alloc] init];
    
    SEND_REQUEST(req, handleGetBotSentGiftListsResponse, handleGetBotSentGiftListsResponse)
}
-(void)handleGetBotSentGiftListsResponse:(ResponseBase*)response{
       [self dismissLoadingView];
    if(!response.success){
        return;
    }
 
    ResponseBotSentGiftLists *resp = (ResponseBotSentGiftLists*)response;
    NSArray* arr = resp.botTransactions;
    if(arr!=nil){
        NSUInteger viewControllers =[self.navigationController.viewControllers count];
        if(arr.count>0 && viewControllers == 1){
            ModelBotTransaction* item = (ModelBotTransaction*)arr[0];
            //[[LocalStorageManager instance] saveBotTransaction:item];
            self.botTransaction = item;
            UIView * v = [APP popupViewHandler].topView;
            if (v != nil && [v isKindOfClass:[AlertViewNormal class]]){
                AlertViewNormal *av = (AlertViewNormal*)v;
                if (av.context == Context_BotTransaction){
                    return;
                }
            }
            [[APP getCurrentViewController] showAlert:resp.promptMessage
                                                title:nil delegate:self
                                          firstButton:NSLocalizedString(@"Button_NO", nil)
                                         secondButton:NSLocalizedString(@"Button_YES", nil)
                                              context:Context_BotTransaction contextObject:nil];
        }
    }
}

-(void)sendCancelBotTransacitonRequest{
    if (self.botTransaction){
        RequestBotCancelTransaction *req = [[RequestBotCancelTransaction alloc] init];
        req.botTransactionId = self.botTransaction.botTransactionId;
        
        SEND_REQUEST(req, handleCancelBotTransactionResponse, handleCancelBotTransactionResponse);
    }
}
-(void)handleCancelBotTransactionResponse:(ResponseBase*)response
{
    if (response.success){
        DEBUGLOG(@"Bot transaciton %@ has been cancelled successfully.",self.botTransaction.botTransactionId);
    }
    else{
        DEBUGLOG(@"Failed to cancel bot transaciton %@.",self.botTransaction.botTransactionId);
    }
    self.botTransaction = nil;
}

-(void)sendGetBotLinkRequest{
    if(self.botLinkModel) return;
    RequestBotLinkingLists *req = [[RequestBotLinkingLists alloc] init];
    
    SEND_REQUEST(req, handleGetBotLinkResponse, handleGetBotLinkResponse)
}
-(void)handleGetBotLinkResponse:(ResponseBase*)response{
    ResponseBotLinkingLists *resp = (ResponseBotLinkingLists*)response;
    if(resp.botInfo == nil || [resp.botInfo count] == 0){
        [self sendGetBotSentGiftListsRequest];
        return;
    }
    NSArray* arr = resp.botInfo;
    if(arr!=nil){
        
        if(arr.count>0){
            ModelBotLink* link = (ModelBotLink*)arr[0];
            self.botLinkModel = link;
            if(!self.processBotLinking){
                self.processBotLinking = YES;
            [[APP getCurrentViewController] showAlert:link.message
                                                title:nil delegate:self
                                          firstButton:NSLocalizedString(@"Button_Reject", nil)
                                         secondButton:NSLocalizedString(@"Button_Accept", nil)
                                         context:Context_BotLink contextObject:nil];
            }
        }
    }
    else{
        [self sendGetBotSentGiftListsRequest];
    }
    
}
-(void)sendCancelBotSentGiftRequest:(NSString*)botTransactionID{
    RequestBotCancelTransaction *req = [[RequestBotCancelTransaction alloc] init];
    req.botTransactionId = botTransactionID;
    [self showLoadingView:nil];
    SEND_REQUEST(req, handleCancelBotSentGiftResponse, handleCancelBotSentGiftResponse)
}
-(void)handleCancelBotSentGiftResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(!response.success){
        return;
    }
}
-(void)sendBotLinkConfirmInformationRequest:(BOOL)ifAccept{
    RequestBotLinkConfirm *req = [[RequestBotLinkConfirm alloc] init];
    req.confirm = ifAccept;

    req.botAccountNumber = self.botLinkModel.botAccountNumber;
    req.linkingToken = self.botLinkModel.linkingToken;

    
    
    SEND_REQUEST(req, handleGetBotLinkConfirmResponse, handleGetBotLinkConfirmResponse)
}
-(void)handleGetBotLinkConfirmResponse:(ResponseBase*)response{
    self.processBotLinking = NO;
    [self dismissLoadingView];
    self.botLinkModel = nil;
    if ([self handleServerResponse:response]){
        return;
    }
    if(response.success){
    [self showAlert:response.errorMessage];
    }
}
-(void)sendGetUnacknowledgedGiftcardsRequest{
    RequestGetUnacknowledgedGiftcards *req = [[RequestGetUnacknowledgedGiftcards alloc] init];
    
    SEND_REQUEST(req, handleGetUnacknowledgedGiftcardsResponse, handleGetUnacknowledgedGiftcardsResponse)
}
-(void)handleGetUnacknowledgedGiftcardsResponse:(ResponseBase*)response{
    if(!response.success){
        return;
    }
    ResponseGetUnacknowledgedGiftcards *resp = (ResponseGetUnacknowledgedGiftcards*)response;
    if (resp.transactions.count > 0){
        [self showNewGiftcardReceivedPopupView:resp.transactions];
    }
}

-(void)sendGetSuggestedGiftcardsRequest:(ModelTransaction*)transaction{
    RequestGetSuggestedGiftcards *req = [[RequestGetSuggestedGiftcards alloc] init];
    req.contextObject = transaction;
    
    SEND_REQUEST(req, handleGetSuggestedGiftcardsResponse, handleGetSuggestedGiftcardsResponse);
}

-(void)handleGetSuggestedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    
    ModelTransaction *transaction = (ModelTransaction*)response.contextObject;
    NSMutableArray<ModelGiftCard*> *array = [[NSMutableArray alloc] initWithArray:((ResponseGetSuggestedGiftcards*)response).giftCards];
    [array addObject:[ModelGiftCard emptyGiftcard]];
    
    NSUInteger selectedIndex = -1;
    
    ModelGiftCard *card = transaction.giftCard;
    for(int i = 0; i < [array count]; i ++){
        ModelGiftCard *c = [array objectAtIndex:i];
        if (card.giftCardId == c.giftCardId){
            c.isSenderSelected = YES;
            selectedIndex = i;
        }
        else{
            c.isSenderSelected = NO;
        }
    }
    if (selectedIndex == -1){
        selectedIndex = [array count] / 2;
        card.isSenderSelected = YES;
        [array insertObject:card atIndex:selectedIndex];
    }
    
    [self showNewGiftcardDetailView:transaction suggestedGiftcards:array selectedIndex:selectedIndex];
}


-(void)sendAcknowledgeGiftcardsRequest:(NSArray*)transactionIds withTransaction:(ModelTransaction*)transaction{
    RequestAcknowledgeGiftcard *req = [[RequestAcknowledgeGiftcard alloc] init];
    req.transactionIds = transactionIds;
    req.contextObject = transaction;
    
    SEND_REQUEST(req, handleAcknowledgeGiftcardResponse, handleAcknowledgeGiftcardResponse);
}

-(void)handleAcknowledgeGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    
    [self sendGetSuggestedGiftcardsRequest:tran];
}
@end
