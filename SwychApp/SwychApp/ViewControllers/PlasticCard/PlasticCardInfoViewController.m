//
//  PlasticCardInfoViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PlasticCardInfoViewController.h"
#import "UIRoundCornerButton.h"
#import "UIImageView+WebCache.h"
#import "PurchaseForMyselfConfirmationView.h"
#import "ModelTransaction.h"
#import "IssuedGiftcardDetailViewController.h"
#import "LocationManager.h"
#import "ModelLocation.h"


#import "RequestUploadPlasticGiftcard.h"
#import "ResponseUploadPlasticGiftcard.h"
#import "RequestGetIssuedGiftcards.h"
#import "ResponseGetIssuedGiftcards.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"

#import "TrackingEventCardUploaded.h"

@interface PlasticCardInfoViewController (){
    BOOL _incompleteCard;
}

@property (weak, nonatomic) IBOutlet UIView *vCView;

@property (weak, nonatomic) IBOutlet UIView *vCTop;
@property (weak, nonatomic) IBOutlet UIView *vCCard;
@property (weak, nonatomic) IBOutlet UIImageView *ivCCard;
@property (weak, nonatomic) IBOutlet UIImageView *ivCBrand;
@property (weak, nonatomic) IBOutlet UITextField *txtCCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCPIN;
@property (weak, nonatomic) IBOutlet UITextField *txtCBalance;
@property (weak, nonatomic) IBOutlet UILabel *labCSeperator;

@property (weak, nonatomic) IBOutlet UIView *vCBottom;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCAddCard;


@property (weak, nonatomic) IBOutlet UIView *vICView;
@property (weak, nonatomic) IBOutlet UIView *vICTop;
@property (weak, nonatomic) IBOutlet UIImageView *ivICCardBackImage;

@property (weak, nonatomic) IBOutlet UIView *vICBottom;
@property (weak, nonatomic) IBOutlet UIView *vICCard;
@property (weak, nonatomic) IBOutlet UIImageView *ivICCard;
@property (weak, nonatomic) IBOutlet UIImageView *ivICBrand;
@property (weak, nonatomic) IBOutlet UITextField *txtICCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtICPIN;
@property (weak, nonatomic) IBOutlet UITextField *txtICBalance;
@property (weak, nonatomic) IBOutlet UILabel *labICSeperator;

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnICAddCard;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation PlasticCardInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.giftcardImage == nil){
        _incompleteCard = NO;
        self.vCView.hidden = NO;
        self.vICView.hidden = YES;
        self.txtCCardNumber.text = self.giftcard.giftCardNumber;
        [self.ivCBrand sd_setImageWithURL:[NSURL URLWithString:self.giftcard.logoUrl]];
        
        //[self.txtCPIN becomeFirstResponder];
        self.txtCCardNumber.delegate = self;
        self.txtCPIN.delegate = self;
        self.txtCBalance.delegate = self;
    }
    else{
        _incompleteCard = YES;
        self.vCView.hidden = YES;
        self.vICView.hidden = NO;
        
        self.txtICCardNumber.text = nil;
        self.ivICCardBackImage.image = self.giftcardImage;
        [self.ivICBrand sd_setImageWithURL:[NSURL URLWithString:self.giftcard.logoUrl]];
        //[self.txtICCardNumber becomeFirstResponder];
        self.txtICCardNumber.delegate = self;
        self.txtICPIN.delegate = self;
        self.txtICBalance.delegate = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addRightIcon:(UITextField*) txtcc{
    UIView *vwContainer = [[UIView alloc] init];
    [vwContainer setFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    [vwContainer setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *icon = [[UIImageView alloc] init];
    [icon setImage:[UIImage imageNamed:@"icon_checkbox_checked_2.png"]];
    [icon setFrame:CGRectMake(0.0f, 5.0f, 20.0f, 20.0f)];
    [icon setBackgroundColor:[UIColor clearColor]];
    
    [vwContainer addSubview:icon];
    
    [txtcc setRightView:vwContainer];
    [txtcc setRightViewMode:UITextFieldViewModeAlways];
}
-(void)removeRightIcon:(UITextField*) txtcc{
    [txtcc setRightView:nil];
    [txtcc setRightViewMode:UITextFieldViewModeAlways];
}
-(void)manageCompleteIcon:(UITextField *)textField{
    if (self.giftcardImage == nil){
        if(![_txtCCardNumber.text isEqualToString:@""]&&![_txtCBalance.text isEqualToString:@""]&&![_txtCPIN.text isEqualToString:@""]){
            [self addRightIcon:_txtCCardNumber];
        }
        else{
            [self removeRightIcon:_txtCCardNumber];
        }
    }
    else{
        if(![_txtICCardNumber.text isEqualToString:@""]&&![_txtICBalance.text isEqualToString:@""]&&![_txtICPIN.text isEqualToString:@""]){
            [self addRightIcon:_txtICCardNumber];
        }
        else{
            [self removeRightIcon:_txtICCardNumber];
        }
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self manageCompleteIcon:textField];
 
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;{
    [self manageCompleteIcon:textField];
    if (textField == _txtCBalance||textField == _txtICBalance){
            NSString *number=textField.text;
            
            number=[number stringByReplacingCharactersInRange:range withString:string];
            number=[number stringByReplacingOccurrencesOfString:@"." withString:@""];
            number =[number
                     stringByReplacingOccurrencesOfString:@"$ " withString:@""];
            if([number integerValue]>0){
                textField.text=[NSString stringWithFormat:@"$ %.2f",[number integerValue]*0.01f];
              //  textField.text=[NSString stringWithFormat:@"%.2f",[number integerValue]*0.01f];
            }
            else{
                textField.text=@"";
            }
        return NO;
    }
    if (textField == _txtCPIN||textField == _txtICPIN){
        NSString *number=textField.text;
        
        number=[number stringByReplacingCharactersInRange:range withString:string];
        number=[number stringByReplacingOccurrencesOfString:@"." withString:@""];
        number =[number
                 stringByReplacingOccurrencesOfString:@"PIN " withString:@""];
        if([number integerValue]>0){
            textField.text=[NSString stringWithFormat:@"PIN %d",(int)[number integerValue]];
            
        }
        else{
            textField.text=@"";
        }
        return NO;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnCAddCard || sender == self.btnICAddCard){
        [self showLoadingView:nil];
        [self sendUploadPlasticGiftcardRequest];
    }
}


-(void)showGiftcardDetailPopupView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    ctrl.backToRootViewController = YES;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

#pragma mark GiftcardWithBalanceAndWalletButtonViewToParentDelegate
-(void)AddPassToWallet:(GiftcardWithBalanceAndWalletButtonView*)giftcardView{
    giftcardView.pkCtl.delegate = self;
    PurchaseForMyselfConfirmationView *v = (PurchaseForMyselfConfirmationView *)giftcardView.superview;
    v.hidden = YES;
   // [self.navigationController pushViewController:giftcardView.pkCtl animated:YES];
    if(giftcardView.pkCtl)
        [self presentViewController:giftcardView.pkCtl
                                                            animated:YES
                                                          completion:nil];
    
}
#pragma
#pragma mark PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma 
#pragma mark - Server communication
-(void)sendUploadPlasticGiftcardRequest{
    RequestUploadPlasticGiftcard *req = [[RequestUploadPlasticGiftcard alloc] init];
    
    req.giftCardId = self.giftcard.giftCardId;
    if (_incompleteCard){
        req.giftCardNumber = self.txtICCardNumber.text;
        req.giftCardPIN = [self.txtICPIN.text stringByReplacingOccurrencesOfString:@"PIN " withString:@""];// self.txtICPIN.text;
        req.giftCardBalance = [[self.txtICBalance.text stringByReplacingOccurrencesOfString:@"$ " withString:@""] doubleValue];//[self.txtICBalance.text doubleValue];
    }
    else{
        req.giftCardNumber = self.txtCCardNumber.text;
        req.giftCardPIN = [self.txtCPIN.text stringByReplacingOccurrencesOfString:@"PIN " withString:@""];//self.txtCPIN.text;
        req.giftCardBalance = [[self.txtCBalance.text stringByReplacingOccurrencesOfString:@"$ " withString:@""] doubleValue];//[self.txtCBalance.text doubleValue];
    }
    
    SEND_REQUEST(req, handleUploadPlasticGiftcardResponse, handleUploadPlasticGiftcardResponse)
}

-(void)handleUploadPlasticGiftcardResponse:(ResponseBase*)resposne{
    [self dismissLoadingView];
    if ([self  handleServerResponse:resposne]){
        return;
    }
    TrackingEventCardUploaded *evt = [[TrackingEventCardUploaded alloc] init];
    [[APP analyticsHandler] sendEvent:evt];
    
    ResponseUploadPlasticGiftcard* resp = (ResponseUploadPlasticGiftcard*)resposne;
    
    [self sendGetIssuedGiftcardsRequest:resp.plasticGiftCardId];
    
}

-(void)sendGetIssuedGiftcardsRequest:(long long)plasticCardId{
    RequestGetIssuedGiftcards *req = [[RequestGetIssuedGiftcards alloc] init];
    req.contextObject = [NSNumber numberWithLongLong:plasticCardId];
    
    SEND_REQUEST(req, handleGetIssuedGiftcardsResponse, handleGetIssuedGiftcardsResponse)
}

-(void)handleGetIssuedGiftcardsResponse:(ResponseBase*)response{
    if ([self handleServerResponse:response]){
        return;
    }
    NSInteger plasticcardId = [(NSNumber*)response.contextObject integerValue];
    
    NSArray<ModelTransaction*> *issuedGiftCardTransactions =  ((ResponseGetIssuedGiftcards*)response).issuedGiftcardsTransaction;
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *tran, NSDictionary<NSString *,id> * _Nullable bindings) {
        return tran.giftCard.plasticGiftCardId == plasticcardId;
    }];
    NSArray<ModelTransaction*> *filteredTransactions = [issuedGiftCardTransactions filteredArrayUsingPredicate:pred];
    if (filteredTransactions.count > 0){
        ModelTransaction *tran = [filteredTransactions objectAtIndex:0];
        [self sendGetNearbyLocationRequest:tran];
    }
    else{
        [self showAlert:NSLocalizedString(@"Error_CannotRetrieveUploadedCardInfo", nil)];
    }
    
    
//    NSArray<ModelGiftCard*> *issuedGiftcards = ((ResponseGetIssuedGiftcards*)response).giftCards;
//    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *card, NSDictionary<NSString *,id> * _Nullable bindings) {
//        return card.plasticGiftCardId == plasticcardId;
//    }];
//    
//    NSArray<ModelGiftCard*> *filteredGiftcards = [issuedGiftcards filteredArrayUsingPredicate:pred];
//    if (filteredGiftcards.count > 0){
//        ModelTransaction *tran = [[ModelTransaction alloc] initWithGiftCard:[filteredGiftcards objectAtIndex:0]];
//        [self sendGetNearbyLocationRequest:tran];
//    }
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailPopupView:tran locations:((ResponseGetNearbyLocations*)response).locations];
}

@end
