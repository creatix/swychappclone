//
//  CardPictureViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CardPictureViewController.h"
#import "ModelGiftcard.h"
#import "UIRoundCornerButton.h"
#import "PlasticCardInfoViewController.h"
#import "PreviewView.h"
#import "Camera.h"
#import "UIImageView+WebCache.h"
#import "UILabelExt.h"


@interface CardPictureViewController () <BarcodeScannerProtocol,CameraProtocol> {
    Camera *_backCamera;
}

@property (weak, nonatomic) IBOutlet UIView *vTop;

@property (weak, nonatomic) IBOutlet UIView *vStripe;

@property (weak, nonatomic) IBOutlet UIView *vCardInfo;
@property (weak, nonatomic) IBOutlet UIImageView *ivGiftcardLogo;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardName;
@property (weak, nonatomic) IBOutlet UIImageView *ivArrow;
@property (weak, nonatomic) IBOutlet UILabelExt *labTakePhoto;

@property (weak, nonatomic) IBOutlet UIView *vPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *vPreviewInterestAreaPlaceHolder;


@property (weak, nonatomic) IBOutlet UIView *ivBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnFlash;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property (weak, nonatomic) IBOutlet PreviewView *vPreview;

- (IBAction)buttonTapped:(id)sender;
@end

@implementation CardPictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [Camera loadCameras:^(NSArray<Camera *> * cameras, NSError * error) {
        if(error != nil) {
            [self showAlert:error.localizedDescription];
        }
        else {
            for(Camera* camera in cameras) {
                if(camera.isBackCamera) {
                    _backCamera = camera;
                    _backCamera.barcodeProtocol = self;
                    _backCamera.cameraProtocol = self;
                    break;
                }
            }
            
            if(_backCamera == nil) {
                [self performSelectorOnMainThread:@selector(showErrorMessage:) withObject:NSLocalizedString(@"Error_FailedLoadCamera", nil) waitUntilDone:NO];
            }
            else{
                [self startBackCamera];
            }
        }
    }];
    [self.ivGiftcardLogo sd_setImageWithURL:[NSURL URLWithString:self.selectedGiftcard.logoUrl]];
    self.labGiftcardName.text = self.selectedGiftcard.retailer;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   // [self startBackCamera];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear: animated];
    
    [self stopCamera];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(void)setupUI{
    self.vStripe.backgroundColor = [UIColor clearColor];
    self.labTakePhoto.verticalAlignment = VerticalAlignment_Bottom;
}

-(CGRect)getCameraInterestArea{
    CGRect rc = self.vPreviewInterestAreaPlaceHolder.frame;
    return [self.vPlaceholder convertRect:rc toView:self.vTop];

}

-(void)startBackCamera{
    if(_backCamera != nil) {
        [_backCamera open:^(AVCaptureSession* session, NSError* err) {
            if(err != nil) {
                [self performSelectorOnMainThread:@selector(showErrorMessage:) withObject:NSLocalizedString(@"Error_FailedOpenCamera", nil) waitUntilDone:NO];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.vPreview setSession:session];
                    CGRect rc = [self getCameraInterestArea];
                    [self.vPreview setAreaOfInterest:rc];
                    [_backCamera start];
                    _backCamera.enableBarcodeScanning = YES;
                });
            }
        }];
    }
}

-(void)stopCamera{
    if (_backCamera != nil ){
        [_backCamera stop];
    }
}

- (void) captureImageFromCamera{
    if(_backCamera != nil) {
        [_backCamera captureImage:^(NSData* imageData, NSError* error) {
            if(error != nil) {
                //Show error message
                [self showErrorMessage:[error localizedDescription]];
            }
            else {
                
                //Crop here
                

                [self switchToPlasticCardInfoViewWithCardImage:[self.vPreview getInterestedAreaImage:imageData] withBarCode:nil];
            }
        }];
    }
}

- (void)showErrorMessage:(NSString*)error{
    [self showAlert:error];
}

- (IBAction)buttonTapped:(id)sender {
    if(sender == self.btnSkip){
        [self switchToPlasticCardInfoViewWithCardImage:nil withBarCode:nil];
    }
    else if (sender == self.btnPhoto){
        [self captureImageFromCamera];
    }
    else if (sender == self.btnFlash){
        [_backCamera turnTorchOnOff];
    }
}

-(void)switchToPlasticCardInfoViewWithCardImage:(UIImage *)cardImage withBarCode:(NSString*)barcode{
    PlasticCardInfoViewController *ctrl = (PlasticCardInfoViewController*)[Utils loadViewControllerFromStoryboard:@"PlasticCard" controllerId:@"PlasticCardInfoViewController"];
    ctrl.giftcard = self.selectedGiftcard;
    ctrl.giftcardImage = cardImage;
    ctrl.giftcard.giftCardNumber = barcode;
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [self.navigationController pushViewController:ctrl animated:YES];

}

#pragma
#pragma mark - BarcodeScannerProtocol -

- (CGRect) areaOfInterest {
    return  CGRectMake(.25, .25, .5, .5);
}

- (void) barcodeDetected:(NSString*)value withBounds:(CGRect)bounds {
    [self stopCamera];
    [self switchToPlasticCardInfoViewWithCardImage:nil withBarCode:value];
}

#pragma
#pragma mark - CameraProtocol-

@end
