//
//  CardPictureViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"

@class ModelGiftCard;

@interface CardPictureViewController : SwychBaseViewController

@property(nonatomic,strong) ModelGiftCard *selectedGiftcard;

@end
