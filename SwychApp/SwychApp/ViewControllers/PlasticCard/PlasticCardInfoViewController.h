//
//  PlasticCardInfoViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "GiftcardWithBalanceAndWalletButtonView.h"
@class ModelGiftCard;

@interface PlasticCardInfoViewController : SwychBaseViewController<PKAddPassesViewControllerDelegate,UITextFieldDelegate>

@property(nonatomic,strong) ModelGiftCard *giftcard;
@property(nonatomic,strong) UIImage *giftcardImage;

@end
