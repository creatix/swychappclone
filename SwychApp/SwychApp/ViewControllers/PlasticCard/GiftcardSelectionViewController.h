//
//  GiftcardSelectionViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "GiftcardSelectionView.h"


@interface GiftcardSelectionViewController : SwychBaseViewController <GiftcardSelectionViewDelegate>

@property (strong,nonatomic) NSArray<ModelGiftCard*> *giftcards;
@end
