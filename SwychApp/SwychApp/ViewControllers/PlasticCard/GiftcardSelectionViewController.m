//
//  GiftcardSelectionViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftcardSelectionViewController.h"
#import "CardPictureViewController.h"




@interface GiftcardSelectionViewController ()
@property (weak, nonatomic) IBOutlet GiftcardSelectionView *vGiftcardSelectionView;

@end

@implementation GiftcardSelectionViewController

@synthesize giftcards = _giftcards;

-(NSArray*)giftcards{
    return _giftcards;
}

-(void)setGiftcards:(NSArray *)giftcards{
    _giftcards = giftcards;
    if (self.vGiftcardSelectionView != nil){
        self.vGiftcardSelectionView.giftcards = giftcards;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.vGiftcardSelectionView.selectionViewDelegate = self;
    self.vGiftcardSelectionView.giftcards = self.giftcards;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)ignoreToSetInfo:(id)object{
    return  YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma 
#pragma mark - GiftcardSelectionViewDelegate
-(void)giftcardSelectionView:(GiftcardSelectionView*)selectionView giftcardSelected:(ModelGiftCard*)giftcard{
    CardPictureViewController *ctrl = (CardPictureViewController*)[Utils loadViewControllerFromStoryboard:@"PlasticCard" controllerId:@"CardPictureViewController"];
    ctrl.selectedGiftcard = giftcard;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}


@end
