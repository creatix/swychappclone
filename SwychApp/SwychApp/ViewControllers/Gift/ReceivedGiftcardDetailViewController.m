//
//  ReceivedGiftcardDetailViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ReceivedGiftcardDetailViewController.h"
#import "GiftAttachmentView.h"
#import "UIRoundCornerButton.h"
#import "GiftCardCarousel.h"
#import "ModelTransaction.h"
#import "ModelGiftCard.h"
#import "PurchaseForMyselfConfirmationView.h"
#import "ModelRedeemOptionGiftCard.h"
#import "ModelRedeemOptionCash.h"
#import "GiftViewController.h"
#import "ModelRegiftParameters.h"
#import "ContactBookManager.h"
#import "ModelTransaction.h"
#import "ModelUser.h"
#import "ModelGiftAttachments.h"
#import "AlertViewNormal.h"
#import "Utils.h"
#import "LocalUserInfo.h"
#import "LocalStorageManager.h"
#import "GiftCardMallViewController.h"
#import "IssuedGiftcardDetailViewController.h"
#import "LocationManager.h"
#import "ButtonWithTextAtBottom.h"
#import "UILabel+Extra.h"
#import "RegistrationProfileViewController.h"
#import "NSString+Extra.h"
#import "ModelGiftcardOrderInfo.h"
#import "GiftPaymentViewController.h"

#import "RequestRedeemGiftcard.h"
#import "ResponseRedeemGiftcard.h"
#import "RequestGetRedeemedGiftcards.h"
#import "ResponseGetRedeemedGiftcards.h"
#import "RequestGetGiftcardsByRetailer.h"
#import "ResponseGetGiftcardsByRetailer.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"
#import "RequestGetSavedGiftcards.h"
#import "ResponseGetSavedGiftcards.h"

#define Context_Popup_Swych_Prompt  0x00201
#define Context_Action_Redeem       0x00210
#define Context_Action_ReGift       0x00211
#define Context_Action_Cashout      0x00212
#define Context_Action_Swych        0x00213
#define Context_Action_Swych_PayExtraMoney  0x00214

@interface ReceivedGiftcardDetailViewController ()
@property (weak, nonatomic) IBOutlet UIView *vTop;
@property (weak, nonatomic) IBOutlet UIImageView *ivBackgroundTop;
@property (weak, nonatomic) IBOutlet GiftCardCarousel *carouselGiftcards;
@property (weak, nonatomic) IBOutlet UILabel *labAmount;
@property (weak, nonatomic) IBOutlet UILabel *labRedeemSwych;
@property (weak, nonatomic) IBOutlet UILabel *labWontTell;


@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (weak, nonatomic) IBOutlet UIImageView *ivBackgroundBottom;
@property (weak, nonatomic) IBOutlet UIView *vButtonsBottom;
@property (weak, nonatomic) IBOutlet GiftAttachmentView *vAttachmentView;

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSwych;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnCashOut;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnRedeem;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnReGift;

@property (weak, nonatomic) IBOutlet  UIRoundCornerButton* btnDecideLater;
@property (strong, nonatomic) ModelGiftCard *selectedGiftcardForSwyching;
@property (assign,nonatomic) double amountSwychedTo;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation ReceivedGiftcardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // [self AddTopRightSearchButton];
    [self setup];
    
    [Utils addNotificationObserver:self selector:@selector(userStatusActivatedNotificationHandler:) name:NOTI_UserStatusActivated object:nil];
    if ([NSString isNullOrEmpty:self.transaction.sentMessage.imgUrl]){
        self.vAttachmentView.videoButtonViewHeight = 0.0f;
    }
    else{
        self.vAttachmentView.imageUrl = self.transaction.sentMessage.imgUrl;
    }

}
-(BOOL)hideBackButton{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)userStatusActivatedNotificationHandler:(NSNotification*)noti{
    if (noti.userInfo != nil){
        NSArray *controllers = self.navigationController.viewControllers;
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
        for(UIViewController *vc in controllers){
            if ([vc isKindOfClass:[RegistrationProfileViewController class]]){
                break;
            }
            else{
                [array addObject:vc];
            }
        }
        self.navigationController.viewControllers = array;
        NSInteger context = [[noti.userInfo objectForKey:@"Context"] integerValue];
        switch (context) {
            case Context_Action_Swych:
                [self handleSwychGift];
                break;
            case Context_Action_Redeem:
                [self redeemGiftcard];
                break;
            case Context_Action_Cashout:
                [self cashoutGiftcard];
                break;
            case Context_Action_ReGift:
                [self regiftGiftcard];
                break;
            default:
                break;
        }
    }
}

-(void)setup{
    [self AddTitle: NSLocalizedString(@"CARDDETAILS",nil)];
    self.vTop.backgroundColor = UICOLOR_CLEAR;
    self.vBottom.backgroundColor = UICOLOR_CLEAR;
    
    self.btnSwych.hidden = YES;
    self.vButtonsBottom.hidden = NO;
    self.vButtonsBottom.backgroundColor = [UIColor clearColor];
    
    self.carouselGiftcards.transactions = [Utils getTransactionsFromGiftcards:self.suggestedGiftcards];
    self.carouselGiftcards.delegate =(id<GiftCardCarouselDelegate>)self;
    self.carouselGiftcards.currentItemIndex = self.selectedIndex;
    
    //self.labAmount.text = [NSString stringWithFormat:FORMAT_MONEY,self.transaction.amount];
    [self.labAmount AmountFormatWithDMark:self.transaction.amount fontsize:self.labAmount.font.pointSize  / 2.0 lengthOfRange:2 ifFromTheLeft:NO];
    
    
    NSString *senderName =[ContactBookManager getContactFirstNameByPhoneNumber:self.transaction.sender.phoneNumber];
    self.vAttachmentView.avatarImage = [ContactBookManager getContactHeadImgByPhoneNumber:self.transaction.sender.phoneNumber];
    self.vAttachmentView.senderName = senderName;
    self.vAttachmentView.message = self.transaction.sentMessage.message;
    self.vAttachmentView.transaction = self.transaction;
    
    //self.labRedeemSwych.text = NSLocalizedString(@"RedeemOrSwych", nil);
    [self SwychItText];
    self.labWontTell.text = [NSString stringWithFormat:NSLocalizedString(@"WeWontTellSender", nil),senderName];
    
    self.btnRedeem.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    self.btnCashOut.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    self.btnReGift.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    
    [self.btnDecideLater setTitle:NSLocalizedString(@"Button_DecideLater", nil)];
    self.btnDecideLater.backgroundColor = UICOLORFROMRGB(192,31,109,1.0);
}
-(void)SwychItText{
    NSString *str1 = NSLocalizedString(@"RedeemOrSwych1", nil);
    
    NSString *str2 = NSLocalizedString(@"RedeemOrSwych2", nil);
    
    NSString *text = [NSString stringWithFormat:@"%@%@",str1,str2];
    
    
    // Define general attributes for the entire text
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:UICOLORFROMRGB(76, 77, 77, 1.0),
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:14]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange range = [text rangeOfString:str2];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: UICOLORFROMRGB(192, 31, 109, 1.0),
                                    NSFontAttributeName:[UIFont fontWithName:@"Helvetica-bold" size:14.0]} range:range];
    
    self.labRedeemSwych.attributedText = attributedText;
}
-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(BOOL)needUpdateUserInfo:(NSInteger)context{
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (user.userStatus == UserStatus_PendingOTPVerified){
        RegistrationProfileViewController *vc = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
        vc.user = user;
        vc.context = context;
        vc.isActiveUserInTransaction = YES;
        [APP pushViewController:vc animated:YES];
        return YES;
    }
    else{
        return NO;
    }
}
-(void)userInfoUpdated:(ModelUser*)user  context:(NSInteger)context contextObject:(NSObject*)contextObject{
    switch (context) {
        case Context_Action_Swych:
            [self handleSwychGift];
            break;
        case Context_Action_Redeem:
            [self redeemGiftcard];
            break;
        case Context_Action_Cashout:
            [self cashoutGiftcard];
            break;
        case Context_Action_ReGift:
            [self regiftGiftcard];
            break;
        default:
            break;
    }
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnSwych){
        if([Utils CheckIfTarget:self.selectedGiftcardForSwyching]){
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:@"targetRedeemTerms" title:@"Disclaimer" type:YES ];
            cppv.uDelegate = self;
            cppv.tag = Context_Action_Swych;
            [cppv show];
            return;
        }
        else if(![NSString isNullOrEmpty:self.selectedGiftcardForSwyching.disclaimerURL]){
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:self.selectedGiftcardForSwyching.disclaimerURL title:@"Disclaimer" type:YES ];
            cppv.uDelegate = self;
            cppv.giftcard = self.selectedGiftcardForSwyching;
            [cppv show];
            return;
        }

        else
        [self swychGiftcard];
    }
    if(sender == self.btnDecideLater){
            [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(void)swychGiftcard{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localUser.notPromptWhenSwychGift){
        [self handleSwychGift];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"SwychOnce", nil)
                   title:NSLocalizedString(@"Swych_Prompt_Title", nil)
                delegate:self
             firstButton:NSLocalizedString(@"Button_Cancel", nil)
            secondButton:NSLocalizedString(@"Button_Continue", nil)
             withContext:Context_Popup_Swych_Prompt
       withContextObject:nil
            checkboxText:NSLocalizedString(@"DoNotShowItAgain", nil)];
    }
}

-(void)redeemGiftcard{
    if (![self needUpdateUserInfo:Context_Action_Redeem]){
        [self showLoadingView:nil];
        [self sendRedeemGiftcardRequest:nil];
    }
}

-(void)cashoutGiftcard{
       [self showAlert:NSLocalizedString(@"CashOutComingsoonContent", nil) title:NSLocalizedString(@"CashOutComingsoonTitle", nil)];
//    if (![self needUpdateUserInfo:Context_Action_Cashout]){
//    }
}

-(void)regiftGiftcard{
    if (![self needUpdateUserInfo:Context_Action_ReGift]){
        GiftViewController *ctrl = (GiftViewController*)[Utils loadViewControllerFromStoryboard:@"Main" controllerId:@"GiftViewController"];
        ModelRegiftParameters *regiftParam = [[ModelRegiftParameters alloc] init];
        regiftParam.originalTransaction = self.transaction;
        
        ctrl.regiftParam = regiftParam;
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

-(void)handleSwychGift{
    if (![self needUpdateUserInfo:Context_Action_Swych]){
        double allowedAmount = [self.selectedGiftcardForSwyching amountSwychTo:self.amountSwychedTo];
        if (fabs(allowedAmount - self.amountSwychedTo) >= 0.01){
            LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
            ModelGiftcardOrderInfo *order = [[ModelGiftcardOrderInfo alloc] init];
            order.forSwych = YES;
            order.swychToGiftCard = self.selectedGiftcardForSwyching;
            order.swychToAmount = self.amountSwychedTo;    
            order.swychFromAmount = self.transaction.amount;
            
            order.amount = allowedAmount;
            ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
            mpi.paymentType = PaymentOption_SwychBalance;
            mpi.amount = self.transaction.amount;
            mpi.paymentReferenceNumber =[NSString stringWithFormat:@"%@",self.transaction.giftCard.parentTransactionId];
            order.payments = [NSArray arrayWithObject:mpi];
            order.amountDue = allowedAmount - self.amountSwychedTo;
            order.giftcard = self.selectedGiftcardForSwyching;
            order.amount = allowedAmount;
            order.recipientIdenIsPhoneNumber = YES;
            order.recipientPhoneNumberOrEmail = user.phoneNumber;
            
            NSString *msg = nil;
            msg = [NSString stringWithFormat:NSLocalizedString(@"PromptSwychMerchantMinimumLimit", nil),allowedAmount];
            [Utils showAlert:msg
                       title:nil delegate:self
                 firstButton:NSLocalizedString(@"Button_NO", nil)
                secondButton:NSLocalizedString(@"Button_YES", nil)
                 withContext:Context_Action_Swych_PayExtraMoney
           withContextObject:order
                checkboxText:nil];
            return;
        }
        else{
            [self showLoadingView:nil];
            [self sendRedeemGiftcardRequest:self.selectedGiftcardForSwyching];
        }
    }
}

-(void)showPaymentView:(ModelGiftcardOrderInfo*)order credits:(NSArray<ModelTransaction*>*)credits{
    GiftPaymentViewController *ctrl = (GiftPaymentViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftPaymentViewController"];
    
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *tran, NSDictionary<NSString *,id> * _Nullable bindings) {
        return tran.transactionId != self.transaction.transactionId;
    }];
    NSArray *array = [credits filteredArrayUsingPredicate:pred];
    
    ctrl.orderInfo = order;
    ctrl.availableCredits = array;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)showGiftcardDetailView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    ctrl.backToRootViewController = YES;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)swithToGiftcardMallView:(NSArray<ModelGiftCard*>*)giftcards{
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *cardItem, NSDictionary<ModelGiftCard *,id> * _Nullable bindings) {
        if(cardItem.giftCardId == self.transaction.giftCard.giftCardId) return NO;
        if(cardItem.availability == OUTOFSTOCK) return NO;
        return [cardItem validateAmount:[cardItem amountSwychTo:self.transaction.amount*cardItem.conversionRate] showAlert:NO];
    }];
    giftcards = [giftcards filteredArrayUsingPredicate:pred];
    GiftCardMallViewController *ctrl = (GiftCardMallViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftCardMallViewController"];
    ctrl.giftcards = giftcards;
    ctrl.delegate = (id<GiftCardMallViewDelegate>)self;
    ctrl.amount = self.transaction.amount;
    [self.navigationController pushViewController:ctrl animated:YES];
}

#pragma 
#pragma mark - ButtonWithTextAtBottomDelegate
-(void)buttonWithTextAtBottonItemTapped:(ButtonWithTextAtBottom*)buttonItem{
    if (buttonItem == self.btnRedeem){
        if([Utils CheckIfAmazon:_transaction.giftCard]){
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:@"amazonRedeemTerms" title:@"Disclaimer" type:YES ];
            cppv.uDelegate = self;
            cppv.tag = Context_Action_Redeem;
            [cppv show];
        }
        else if([Utils CheckIfTarget:_transaction.giftCard]){
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:@"targetRedeemTerms" title:@"Disclaimer" type:YES ];
            cppv.uDelegate = self;
            cppv.tag = Context_Action_Redeem;
            [cppv show];
            return;
        }
        else if(![NSString isNullOrEmpty:_transaction.giftCard.disclaimerURL]){
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:_transaction.giftCard.disclaimerURL title:@"Disclaimer" type:YES ];
            cppv.uDelegate = self;
            cppv.tag = Context_Action_Redeem;
            [cppv show];
            return;
        }
        else{
            [self redeemGiftcard];
        }
    }
    else if (buttonItem == self.btnCashOut){
        [self cashoutGiftcard];
    }
    else if (buttonItem == self.btnReGift){
        [self regiftGiftcard];
    }
}
#pragma mark - MessageWithWebViewPopViewDelegate
-(void)NextStep:(BOOL)next obj:(id)obj tag:(int)itag{
    if(itag == Context_Action_Swych){
    [self swychGiftcard];
    }
    else
    [self redeemGiftcard];
}
#pragma 
#pragma mark - GiftCardMallViewDelegate
-(void)giftcardMallView:(GiftCardMallViewController*)giftcardMall giftcardSelected:(ModelGiftCard*)giftcard{
    NSInteger toBeSelected = -1;
    for(NSInteger idx = 0; idx < self.suggestedGiftcards.count; idx++){
        if([self.suggestedGiftcards objectAtIndex:idx].giftCardId == giftcard.giftCardId){
            toBeSelected = idx;
            break;
        }
    }
    if (toBeSelected == -1){
        toBeSelected = self.suggestedGiftcards.count / 2;
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:self.suggestedGiftcards];
        [array insertObject:giftcard atIndex:toBeSelected];
        self.suggestedGiftcards = array;
        self.carouselGiftcards.transactions = [Utils getTransactionsFromGiftcards:self.suggestedGiftcards];
        [self.carouselGiftcards reloadData];
    }
    self.carouselGiftcards.currentItemIndex = toBeSelected;
}

#pragma
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if(alertView.context == Context_Popup_Swych_Prompt && index == 2){
        [alertView dismiss];
        [self handleSwychGift];
        return NO;
    }
    else if(alertView.context == Context_Action_Swych_PayExtraMoney){
        if (index == 2){
            ModelGiftcardOrderInfo *order = (ModelGiftcardOrderInfo*)alertView.contextObject;
            [self sendGetSavedGiftcardsRequest:order];
        }
    }
    return YES;
}

-(void)alertView:(AlertViewNormal *)alertView checkboxValueChanged:(BOOL)checked{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    localUser.notPromptWhenSwychGift = checked;
    [[LocalStorageManager instance] saveLocalUserInfo:localUser];
    
}

#pragma
#pragma mark PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma 
#pragma mark - GiftCardCarouselDelegate
- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
         giftCardSelected:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    if (giftCard.isEmptyGiftcard){
        [self showLoadingView:nil];
        [self sendGetGiftcarsByRetailerRequest];
    }
    else{
        self.selectedGiftcardForSwyching = giftCard;
        self.amountSwychedTo = self.transaction.amount * giftCard.conversionRate;
    }
}

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
          giftCardFocused:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    if (giftCard.isSenderSelected){
        self.vButtonsBottom.hidden = NO;
        self.btnSwych.hidden = YES;
        self.labAmount.hidden = NO;
        self.btnDecideLater.backgroundColor = MAINCOLOR_PINK;
        //self.labAmount.text = [NSString stringWithFormat:FORMAT_MONEY,self.transaction.amount];
        [self.labAmount AmountFormatWithDMark:self.transaction.amount fontsize:self.labAmount.font.pointSize / 2.0 lengthOfRange:2 ifFromTheLeft:NO];
        self.selectedGiftcardForSwyching = nil;
        self.amountSwychedTo = 0.0;
    }
    else if (giftCard.isEmptyGiftcard){
        self.vButtonsBottom.hidden = YES;
        self.btnSwych.hidden = YES;
        self.btnDecideLater.backgroundColor = MAINCOLOR_PINK;
        self.labAmount.hidden = YES;
    }
    else{
        self.vButtonsBottom.hidden = YES;
        self.btnSwych.hidden = NO;
        self.btnDecideLater.backgroundColor = MAINCOLOR_GRAY;
        self.labAmount.hidden = NO;
        //self.labAmount.text = [NSString stringWithFormat:FORMAT_MONEY,self.transaction.amount * giftCard.conversionRate];
        [self.labAmount AmountFormatWithDMark:self.transaction.amount * giftCard.conversionRate fontsize:self.labAmount.font.pointSize  / 2.0 lengthOfRange:2 ifFromTheLeft:NO];
        self.selectedGiftcardForSwyching = giftCard;
        self.amountSwychedTo = self.transaction.amount * giftCard.conversionRate;
    }
}

#pragma 
#pragma mark - Server comminication
-(void)sendRedeemGiftcardRequest:(ModelGiftCard*)giftcard{
    RequestRedeemGiftcard *req = [[RequestRedeemGiftcard alloc] init];
    req.transactionId = self.transaction.transactionId;
    ModelRedeemOptionGiftCard *option = [[ModelRedeemOptionGiftCard alloc] init];
    if (giftcard == nil){
        NSInteger giftcardId = self.transaction.giftCard.giftCardId;
        option.giftCardId = [NSString stringWithFormat:@"%ld",giftcardId];
        option.amount = self.transaction.amount;
    }
    else{
        NSInteger giftcardId = giftcard.giftCardId;
        option.giftCardId = [NSString stringWithFormat:@"%ld",giftcardId];
        option.amount = self.amountSwychedTo;
    }
    req.giftCardOptions = [NSArray arrayWithObject:option];
    
    SEND_REQUEST(req, handleRedeemGiftcardResposne, handleRedeemGiftcardResposne);
}

-(void)handleRedeemGiftcardResposne:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response]){
        [self dismissLoadingView];
        return;
    }
    [self showLoadingView:nil];
    ResponseRedeemGiftcard *resp = (ResponseRedeemGiftcard*)response;
    [self sendGetRedeemedGiftcardsRequest:[NSString stringWithFormat:@"%@",resp.transactionIdFrom]];
}

-(void)sendGetRedeemedGiftcardsRequest:(NSString*)transactionId{
    [self showLoadingView:nil];
    RequestGetRedeemedGiftcards *req = [[RequestGetRedeemedGiftcards alloc] init];
    req.transactinId = transactionId;
    
    SEND_REQUEST(req, handleGetRedeemedGiftcardsResponse, handleGetRedeemedGiftcardsResponse)
}

-(void)handleGetRedeemedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response]){
        return;
    }

    ResponseGetRedeemedGiftcards *resp = (ResponseGetRedeemedGiftcards*)response;
    if ([resp.redeemedGiftcards count] > 0){
        ModelTransaction *tran = [resp.redeemedGiftcards objectAtIndex:0];
        
        [self sendGetNearbyLocationRequest:tran];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"NoRedeemedInfoReturned", nil) delegate:nil];
    }
}

-(void)sendGetGiftcarsByRetailerRequest{
    RequestGetGiftcardsByRetailer *req = [[RequestGetGiftcardsByRetailer alloc] init];
    req.purchaseType = 3;
    SEND_REQUEST(req, handleGetAvailableGiftcardByRetailerResponse, handleGetAvailableGiftcardByRetailerResponse);
}

-(void)handleGetAvailableGiftcardByRetailerResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetGiftcardsByRetailer class]]){
        return;
    }
    ResponseGetGiftcardsByRetailer *resp = (ResponseGetGiftcardsByRetailer*)response;
    [self swithToGiftcardMallView:resp.giftCards];
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailView:tran locations:((ResponseGetNearbyLocations*)response).locations];}

-(void)sendGetSavedGiftcardsRequest:(ModelGiftcardOrderInfo*)order{
    RequestGetSavedGiftcards *req = [[RequestGetSavedGiftcards alloc] init];
    req.contextObject = order;
    
    SEND_REQUEST(req, handleGetSavedGiftcardsResponse, handleGetSavedGiftcardsResponse);
}

-(void)handleGetSavedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetSavedGiftcards class]]){
        return;
    }
    ResponseGetSavedGiftcards *resp = (ResponseGetSavedGiftcards*)response;
    [self showPaymentView:(ModelGiftcardOrderInfo*)response.contextObject credits:resp.savedGiftcardsTransaction];
}

#pragma
@end
