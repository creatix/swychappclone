//
//  PaymentOptionsViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentOptionsViewController.h"
#import "PaymentOptionApplePayTableViewCell.h"
#import "PaymentOptionPaypalTableViewCell.h"
#import "PaymentOptionPayWithCardTableViewCell.h"
#import "ModelGiftCard.h"
#import "ModelGiftcardOrderInfo.h"
#import "PurchaseForMyselfConfirmationView.h"
#import "PurchaseConfirmationView.h"
#import "ModelContact.h"
#import "PurchaseConfirmationViewController.h"
#import "IssuedGiftcardDetailViewController.h"
#import "ModelTransaction.h"
#import "LocationManager.h"
#import "ModelLocation.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "ModelSystemConfiguration.h"

#import "RequestPurchaseGiftcard.h"
#import "ResponsePurchaseGiftcard.h"
#import "RequestSwychBoard.h"
#import "ResponseSwychBoard.h"
#import "RequestGetRedeemedGiftcards.h"
#import "ResponseGetRedeemedGiftcards.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"
#import "RequestValidatePassword.h"
#import "ResponseValidatePassword.h"
#import "RequestPreGift.h"
#import "ResponsePreGift.h"
#import "ModelBotTransaction.h"
#import "AnalyticsShareASale.h"
#import "TrackingEventGiftCardPurchasedForSelfGifting.h"
#import "TrackingEventGiftCardPurchasedForSendToFriend.h"

#import "ViewOTPVerification.h"
#import "RequestResendOTP.h"
#import "ResponseResendOTP.h"
#import "RequestVerifyOTP.h"
#import "ResponseVerifyOTP.h"
#import "RequestVoidPayment.h"
#import "ResponseVoidPayment.h"

#define Context_ApplePayNotConfigured   3
#define Context_OTPVerificationPayment  0x0101


@interface TempPaymentToken : NSObject
@property(nonatomic,strong) NSString *token;
@property(nonatomic,assign) PaymentMethod method;

-(id)initWithToken:(NSString*)token withPaymentMethod:(PaymentMethod)method;
@end

@implementation TempPaymentToken
-(id)initWithToken:(NSString*)token withPaymentMethod:(PaymentMethod)method{
    self = [super init];
    if(self){
        self.token = token;
        self.method = method;
    }
    return self;
}
@end


@interface PaymentOptionsViewController ()
@property (weak, nonatomic) IBOutlet UIView *vHeader;
@property (weak, nonatomic) IBOutlet UITableView *tvOptions;
@property (weak, nonatomic) IBOutlet UILabel *labHeaderDesc;
@property (weak, nonatomic) IBOutlet UILabel *labHeaderAmount;
@property (weak, nonatomic) IBOutlet UIWebView *labDisclaimer;
@property (nonatomic,assign) BOOL applePayAvaialbe;
@property (nonatomic,assign) BOOL canMakeApplePayPayment;
@end

@implementation PaymentOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self checkApplePayAvailibility];
    
    [self addNotificationObserverByName:UIApplicationDidBecomeActiveNotification];
    
    [self setup];
    [self AddDisclaimer];
}
-(void)AddDisclaimer{
    NSString *file_path = [[NSBundle mainBundle] pathForResource:@"paymentMethodComment" ofType:@"html"];
    NSURLRequest * req = [NSURLRequest requestWithURL: [NSURL fileURLWithPath: file_path]];
    [self.labDisclaimer setOpaque:NO];
    [self.labDisclaimer setBackgroundColor:[UIColor clearColor]];
    self.labDisclaimer.scrollView.scrollEnabled = NO;
    self.labDisclaimer.scrollView.backgroundColor = [UIColor clearColor];
    [self.labDisclaimer loadRequest: req];
}
-(BOOL) didReceivedLocalNotification:(NSNotification *)noti{
    if ([noti.name compare:UIApplicationDidBecomeActiveNotification] == NSOrderedSame){
        
        [self checkApplePayAvailibility];
        [self.tvOptions reloadData];
    }
    return YES;
}

-(void)checkApplePayAvailibility{
    self.applePayAvaialbe = NO;// [[PaymentHandler instance] isPaymentMethodAvailable:PaymentMethod_ApplePay];
    self.canMakeApplePayPayment = NO;//[[PaymentHandler instance] isPaymentMethodCanMakePayment:PaymentMethod_ApplePay];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL* url = [request URL];
    
    if ([[url scheme] caseInsensitiveCompare:@"http"] == NSOrderedSame || [[url scheme] caseInsensitiveCompare:@"https"] == NSOrderedSame){
        [[UIApplication sharedApplication] openURL:url];
        return NO;
        
    }
    else{
        return YES;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setup{
    self.tvOptions.backgroundColor = [UIColor clearColor];
    self.vHeader.backgroundColor = [UIColor clearColor];
    
    self.labHeaderDesc.text = [NSString stringWithFormat:NSLocalizedString(@"PaymentOptionsViewTitle",nil),self.orderInfo.giftcard.retailer,self.orderInfo.amount];
    self.labHeaderAmount.text = [NSString stringWithFormat:FORMAT_MONEY,self.orderInfo.amountDue];
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"PaymentOptionViewTitle", nil);
}

-(void)makePayment:(PaymentMethod)method{
    NSString *imageUrl = nil;
    if (self.orderInfo.giftcard.giftCardImageURL != nil && [self.orderInfo.giftcard.giftCardImageURL count] > 0){
        imageUrl = [self.orderInfo.giftcard.giftCardImageURL objectAtIndex:0];
    }
    NSString *paymentDesc = nil;
    switch (method) {
        case PaymentMethod_ApplePay:
            paymentDesc = PAYMENT_DESCRIPTION_APPLEPAY;
            break;
        case PaymentMethod_PayPal:
            paymentDesc = self.orderInfo.giftcard.retailer;
            break;
        case PaymentMethod_DirectCreditCard:
            paymentDesc = self.orderInfo.giftcard.retailer;
            break;
        default:
            paymentDesc = self.orderInfo.giftcard.retailer;
            break;
    }
    [[PaymentHandler instance] makePayment:method
                                    amount:self.orderInfo.amountDue
                               description:paymentDesc
                              withDelegate:self
                            viewController:self
                              productImage:imageUrl];
}


-(void)showGiftcardDetailPopupView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    ctrl.backToRootViewController = YES;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)showConfirmationPopupView:(NSString*)transactionId earnedPoints:(double)earnedPoints{
    PurchaseConfirmationViewController *ctrl =(PurchaseConfirmationViewController*) [Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"PurchaseConfirmationViewController"];
    
    ctrl.transactionId = transactionId;
    ctrl.orderAmount = self.orderInfo.amount;
    ctrl.earnedPoints = earnedPoints;
    ctrl.paymentAmount = self.orderInfo.amount;
    ctrl.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    ctrl.deliveryDate = self.orderInfo.dateDelivery == nil ? [NSDate date] : self.orderInfo.dateDelivery;
    ctrl.giftcard = self.orderInfo.giftcard;
    ctrl.contact = self.contact;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)ValidateOPTWithResponse:(ResponsePurchaseGiftcard*)response{
    PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate =(id<OTPVerificationDelegate>) self;
        vVerification.bTapDissMiss = NO;
        [vVerification setMobilePhoneNumber:response.email];
        vVerification.textCode = response.errorMessage == nil ? NSLocalizedString(@"JustEmailedCodeTo", nil) : response.errorMessage;
        vVerification.contextObject = response;
        vVerification.context = Context_OTPVerificationPayment;

        [Utils showPopupView:(PopupViewBase*)v];
    }
}

#pragma makr = OTPVerificationDelegate
-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView{
    if(verificationView.context == Context_OTPVerificationPayment){
        [self sendResendOTPRequest:verificationView];
    }
    else{
        [super OTPVerificationResendOTP:verificationView];
    }

}
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP{
    if(verificationView.context == Context_OTPVerificationPayment){
        [self sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
    }
    else{
        [super sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
    }
}
-(void)sendResendOTPRequest:(ViewOTPVerification*)v{
    if(v.context == Context_OTPVerificationPayment){
        [self showLoadingView:nil];
        ResponsePurchaseGiftcard *respPurchase = (ResponsePurchaseGiftcard*)v.contextObject;
        TempPaymentToken *tpt = (TempPaymentToken*)respPurchase.contextObject;
        
        LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
        RequestResendOTP *req = [[RequestResendOTP alloc] init];
        req.swychId = user.swychId;
        req.email = respPurchase.email;
        req.otpSentType =[Utils getOTPSentTypeWithPaymentMethod:tpt.method];//  OTPSENTTYPE_PAYPAL_EMAIL_VALIDATION;
        req.contextObject = v.contextObject;
        req.context = Context_OTPVerificationPayment;
        
        SEND_REQUEST(req, handleResendOTPResponse, handleResendOTPResponse);
    }
    else{
        [super sendResendOTPRequest:v];
    }
}

-(void)OTPVerificationCancelled:(ViewOTPVerification*)verificationView{
    if(verificationView.context == Context_OTPVerificationPayment){
        [self sendCancelPayPalPaymentRequest:verificationView];
    }
}

-(void)handleResendOTPResponse:(ResponseBase*)response{
    if(response.context == Context_OTPVerificationPayment){
        [self dismissLoadingView];
        
        [Utils showAlert:response.errorMessage delegate:nil];
    }
    else{
        [super handleResendOTPResponse:response];
    }
}

-(void)sendCancelPayPalPaymentRequest:(ViewOTPVerification*)v{
    RequestVoidPayment *req = [RequestVoidPayment new];
    ResponsePurchaseGiftcard *resp = (ResponsePurchaseGiftcard*)v.contextObject;
    [v dismiss];
    TempPaymentToken *tpt = (TempPaymentToken*)resp.contextObject;
    ModelPaymentInfo *pi = [[ModelPaymentInfo alloc] init];
    pi.paymentType = PaymentOption_PayPal;
    pi.paymentReferenceNumber = tpt.token;
    req.payments = [NSArray arrayWithObject:pi];
    [self showLoadingView:nil];
    SEND_REQUEST(req, handleCancelPayPalPaymentResponse, handleCancelPayPalPaymentResponse);
}

-(void)handleCancelPayPalPaymentResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    [self showAlert:response.errorMessage];
}

-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    if(v.context == Context_OTPVerificationPayment){
        [self showLoadingView:nil];
        ResponsePurchaseGiftcard *respPurchase = (ResponsePurchaseGiftcard*)v.contextObject;
        TempPaymentToken *tpt = (TempPaymentToken*)respPurchase.contextObject;
        LocalUserInfo *lu = [[LocalStorageManager instance] getLocalStoredUserInfo];
        RequestVerifyOTP *req = [[RequestVerifyOTP alloc] init];
        req.contextObject = v;
        req.swychId = lu.swychId;
        req.otp = [v getOTPCode];
        req.context = Context_OTPVerificationPayment;
        req.email = respPurchase.email;
        
        req.otpSentType =[Utils getOTPSentTypeWithPaymentMethod:tpt.method]; // OTPSENTTYPE_PAYPAL_EMAIL_VALIDATION;
        
        SEND_REQUEST(req, handleOTPVerificationResponse, handleOTPVerificationResponse);
    }
    else{
        [super sendOTPVerificationRequest:user verificationView:v];
    }
}

-(void)handleOTPVerificationResponse:(ResponseBase*)response{
    if(response.context == Context_OTPVerificationPayment){
        [self dismissLoadingView];
    
    
        if (response.success){
            
            ViewOTPVerification *v = (ViewOTPVerification*)response.contextObject;
            ResponsePurchaseGiftcard *resp = (ResponsePurchaseGiftcard*)v.contextObject;
            [v dismiss];
            TempPaymentToken *tpt = (TempPaymentToken*)resp.contextObject;
            [self sendPurchaseGiftcardRequest:tpt.token paymentMethod:tpt.method];
        }
        else{
            [Utils showAlert:response.errorMessage delegate:nil];
        }
    }
    else{
        [super handleOTPVerificationResponse:response];
    }
}



#pragma
#pragma mark PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma
#pragma mark - PaymentHanderDelegate
-(void)paymentCompleted:(BOOL)success paymentToken:(NSString *)token error:(NSError *)error method:(PaymentMethod)method{
    if (!success){
        if (method == PaymentMethod_ApplePay && error.code == ERR_CODE_PAYMENT_ApplePay_Not_Configured){
            NSString *str = NSLocalizedString(@"ApplePayNotConfigured", nil);
            [self showAlert:str title:nil delegate:self
                firstButton:NSLocalizedString(@"Button_NO", nil)
               secondButton:NSLocalizedString(@"Button_YES",nil)
                    context:Context_ApplePayNotConfigured contextObject:nil ];
            
        }
        else{
            [self showAlert:[error localizedDescription]];
        }
        return;
    }
    [self sendPurchaseGiftcardRequest:token paymentMethod:method];}


#pragma
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context ==Context_ApplePayNotConfigured && index == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=PASSBOOK"]];
    }
    return YES;
}



#pragma 
#pragma mark - PaymentOptionCellDelegate
-(void)paymentOptionCellButtonTapped:(PaymentOptionTableViewCell *)paymentOptionCell buttonTag:(NSInteger)tag{
    NSIndexPath *ip = [self.tvOptions indexPathForCell:paymentOptionCell];
    switch(ip.row){
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;
    }
}

#pragma 
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if(self.applePayAvaialbe){
        return localUser.sysConfig.directCreditCardEnabled ? 3 : 2;
    }
    else{
        return localUser.sysConfig.directCreditCardEnabled ? 2 : 1;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if (self.applePayAvaialbe){
        if (indexPath.row == 0){
            PaymentOptionApplePayTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"PaymentOptionCellApplePay"];
            if (c == nil){
                c = [[PaymentOptionApplePayTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaymentOptionCellApplePay"];
            }
            c.delegate = self;
            if(self.canMakeApplePayPayment){
                c.ivApplePayLogo.image = [UIImage imageNamed:@"apple_pay_white_logo.png"];
            }
            else{
                c.ivApplePayLogo.image = [UIImage imageNamed:@"apple_pay_setup_white.png"];
            }
            cell = c;
        }
        else if (indexPath.row == 1){
            PaymentOptionPaypalTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"PaymentOptionCellPaypal"];
            if (c == nil){
                c = [[PaymentOptionPaypalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaymentOptionCellPaypal"];
            }
            c.delegate = self;
            cell = c;
        }
        else if (indexPath.row == 2){
            PaymentOptionPayWithCardTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"PaymentOptionPayWithCardTableViewCell"];
            if (c == nil){
                c = [[PaymentOptionPayWithCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaymentOptionPayWithCardTableViewCell"];
            }
            c.labPayWithCard.text = NSLocalizedString(@"PaymentOption_DirectCreditCard", nil);
            c.delegate = self;
            cell = c;
        }
    }
    else{
        if (indexPath.row == 0){
            PaymentOptionPaypalTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"PaymentOptionCellPaypal"];
            if (c == nil){
                c = [[PaymentOptionPaypalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaymentOptionCellPaypal"];
            }
            c.delegate = self;
            cell = c;
        }
        else if (indexPath.row == 1){
            PaymentOptionPayWithCardTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"PaymentOptionPayWithCardTableViewCell"];
            if (c == nil){
                c = [[PaymentOptionPayWithCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaymentOptionPayWithCardTableViewCell"];
            }
            c.labPayWithCard.text = NSLocalizedString(@"PaymentOption_DirectCreditCard", nil);
            c.delegate = self;
            cell = c;
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[PaymentOptionApplePayTableViewCell class]]){
        if (self.canMakeApplePayPayment){
            [self sendPreGift:PaymentMethod_ApplePay context:PAY_METHOD_APPPLE_PAY];
        }
        else{
            PKPassLibrary *passLib = [[PKPassLibrary alloc] init];
            [passLib openPaymentSetup];
        }
    }
    else if ([cell isKindOfClass:[PaymentOptionPaypalTableViewCell class]]){
        [self sendPreGift:PaymentMethod_PayPal context:PAY_METHOD_PAYPAL];
    }
    else if ([cell isKindOfClass:[PaymentOptionPayWithCardTableViewCell class]]){
        [self sendPreGift:PaymentMethod_DirectCreditCard context:PAY_METHOD_CREDITCARD];
    }
}
/*
-(BOOL)ValidatePassword:(int)context{
 //   if(self.orderInfo.amountDue>25.00){
    ManullyUpdateBalance *v = (ManullyUpdateBalance*)[Utils loadViewFromNib:@"ManullyUpdateBalance" viewTag:10];
    v.labTitle.text = NSLocalizedString(@"ValidatePasswordTitle",nil);
    NSString* message = NSLocalizedString(@"enterpassword",nil);
    v.ContextID = context;
    v.labWeHaveTexted.text = message;
    v.txtUpdateBalnce.secureTextEntry = YES;
    [v.txtUpdateBalnce setKeyboardType:UIKeyboardTypeDefault];
    v.uDelegate = self;
    [Utils showPopupView:(PopupViewBase*)v];
    return YES;

}
-(void)UpdateContext:(NSString*)text message:(NSString*)textmessage view:(ManullyUpdateBalance*)view{
    [self showLoadingView:nil];
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestValidatePassword *req = [[RequestValidatePassword alloc] init];
    req.password = text;
    req.swychId = localData.swychId;
    req.context = view.ContextID;
    SEND_REQUEST(req, handleValidatePassowrdResponse, handleValidatePassowrdResponse)
}
-(void)handleValidatePassowrdResponse:(ResponseBase*)response{
    [self dismissLoadingView];

    ResponseValidatePassword*resp = (ResponseValidatePassword*)response;
    if (resp.success){
      //[self makePayment:PaymentMethod_DirectCreditCard];
        if(resp.context == PaymentMethod_ApplePay){
            
            [self makePayment:PaymentMethod_ApplePay];
        }
        else if(resp.context == PAY_METHOD_PAYPAL){
            [self makePayment:PaymentMethod_PayPal];
            
        }
        else if(resp.context == PAY_METHOD_CREDITCARD){
            [self makePayment:PaymentMethod_DirectCreditCard];
        }
    }
    else{
        [Utils showAlert:resp.errorMessage delegate:nil];
    }
}
 */
#pragma
#pragma  mark - Server communiation
-(BOOL)handleServerResponse:(ResponseBase*)response{
    if (!response.success && [response isKindOfClass:[ResponsePurchaseGiftcard class]] &&
        response.errorCode == ERR_Payment_BuyerEmailVerificationNeeded &&
        response.contextObject != nil){
        [self ValidateOPTWithResponse:(ResponsePurchaseGiftcard*)response];
        return YES;
    }
    return [super handleServerResponse:response];
}

-(void)sendPreGift:(PaymentMethod)method context:(int)context{
    RequestPreGift *req = [[RequestPreGift alloc] init];
    req.context = context;
    if (self.orderInfo.recipientIdenIsPhoneNumber){
        req.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    else{
        req.recipientEmail = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    
    req.giftCardId = self.orderInfo.giftcard.giftCardId;
    req.giftCardAmount = self.orderInfo.amount;
    NSArray<ModelPaymentInfo*> * payments =self.orderInfo.payments;
    
    if (method != PaymentMethod_None){
        ModelPaymentInfo *mpi = [payments objectAtIndex:0];
        switch (method) {
            case PaymentMethod_ApplePay:
                mpi.paymentType = PaymentOption_ApplePay;
                break;
            case PaymentMethod_PayPal:
                mpi.paymentType = PaymentOption_PayPal;
                break;
            case PaymentMethod_DirectCreditCard:
                mpi.paymentType = PaymentOption_DirectCreditCard;
                break;
            default:
                break;
        }
    }
    if(self.orderInfo.forSwych){
        NSMutableArray *array = [NSMutableArray arrayWithArray:payments];
//        [array addObject:self.orderInfo.payments.firstObject];
        req.payments = array;
        req.purchaseType = PurchaseType_SwychGiftCard;
    }
    else{
        req.payments = payments;
        if (self.orderInfo.giftForFriend){
            req.purchaseType = PurchaseType_ForFriend;
        }
        else{
            req.purchaseType = PurchaseType_SelfGifting;
        }
    }
    if (self.orderInfo.dateDelivery != nil){
        req.deliverDate = [Utils getTimeString:self.orderInfo.dateDelivery];
    }
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchasePreGiftcardResponse, handPurchasePreGiftcardResponse);
}
-(void)handPurchasePreGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePreGift *resp = (ResponsePreGift*)response;
    if(resp.emailVerificationNeeded){
        [self EmailValidate:^{

            switch (resp.context) {
                case PAY_METHOD_APPPLE_PAY:
                    [self sendPreGift:PaymentMethod_ApplePay context:PAY_METHOD_APPPLE_PAY];
                    break;
                case PAY_METHOD_PAYPAL:
                    [self sendPreGift:PaymentMethod_PayPal context:PAY_METHOD_PAYPAL];
                    break;
                case PAY_METHOD_CREDITCARD:
 
                    [self sendPreGift:PaymentMethod_DirectCreditCard context:PAY_METHOD_CREDITCARD];
                    break;
                default:
                    break;
            }
        }
        Email:resp.senderEmail];
        return;
    }
    if(resp.validatePassword){
        [self ValidatePassword:(int)resp.context];
        return;
    }
    if(resp.context == PAY_METHOD_APPPLE_PAY){

        [self makePayment:PaymentMethod_ApplePay];
    }
    else if(resp.context == PAY_METHOD_PAYPAL){
        [self makePayment:PaymentMethod_PayPal];
        
    }
    else if(resp.context == PAY_METHOD_CREDITCARD){
        [self makePayment:PaymentMethod_DirectCreditCard];
    }


    
}

-(void)sendPurchaseGiftcardRequest:(NSString*)paymentToken paymentMethod:(PaymentMethod)method{
    
    TempPaymentToken *tpt = [[TempPaymentToken alloc] initWithToken:paymentToken withPaymentMethod:method];
    
    RequestPurchaseGiftcard *req = [[RequestPurchaseGiftcard alloc] init];
    req.contextObject = tpt;
    
    if (self.orderInfo.recipientIdenIsPhoneNumber){
        req.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    else{
        req.recipientEmail = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    if(self.botTransaction){
        req.botTransactionId = self.botTransaction.botTransactionId;
    }
    req.giftCardId = self.orderInfo.giftcard.giftCardId;
    req.giftCardAmount = self.orderInfo.amount;
    NSArray<ModelPaymentInfo*> * payments =self.orderInfo.payments;
    
    if (method != PaymentMethod_None){
        ModelPaymentInfo *mpi = [payments objectAtIndex:0];
        mpi.paymentReferenceNumber = paymentToken;
        switch (method) {
            case PaymentMethod_ApplePay:
                mpi.paymentType = PaymentOption_ApplePay;
                break;
            case PaymentMethod_PayPal:
                mpi.paymentType = PaymentOption_PayPal;
                break;
            case PaymentMethod_DirectCreditCard:
                mpi.paymentType = PaymentOption_DirectCreditCard;
                break;
            default:
                break;
        }
    }
    req.payments = payments;
    req.message = self.orderInfo.message;
    NSData *data = UIImageJPEGRepresentation(self.orderInfo.imagePhoto, 1.0);
    req.image = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if (self.orderInfo.forSwych){
        req.purchaseType = PurchaseType_SwychGiftCard;
    }
    else{
        if (self.orderInfo.giftForFriend){
            req.purchaseType = PurchaseType_ForFriend;
        }
        else{
            req.purchaseType = PurchaseType_SelfGifting;
        }
    }
    if (self.orderInfo.dateDelivery != nil){
        req.deliverDate = [Utils getTimeString:self.orderInfo.dateDelivery];
    }    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchaseGiftcardResponse, handPurchaseGiftcardResponse);
    
}

-(void)handPurchaseGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePurchaseGiftcard *resp = (ResponsePurchaseGiftcard*)response;
    [APP UpdateBanlance:resp.user.swychPoint];
    
    TrackingEventBase *evt = nil;
    
    if(self.orderInfo.giftForFriend){
        [(AnalyticsShareASale*)[AnalyticsShareASale instance] trackTransaction:resp.transactionId amount:self.orderInfo.amount purchaseForFriend:YES];
        evt = [[TrackingEventGiftCardPurchasedForSendToFriend alloc] init];
        [self showConfirmationPopupView:resp.transactionId earnedPoints:resp.earnedPoints];
    }
    else{
        [(AnalyticsShareASale*)[AnalyticsShareASale instance] trackTransaction:resp.transactionId amount:self.orderInfo.amount purchaseForFriend:NO];
        evt = [[TrackingEventGiftCardPurchasedForSelfGifting alloc] init];
        [self sendGetRedeemedGiftcardsRequest:resp.transactionId];
    }
    [[APP analyticsHandler] sendEvent:evt];
    
}

-(void)sendGetRedeemedGiftcardsRequest:(NSString*)transactionId{
    [self showLoadingView:nil];
    RequestGetRedeemedGiftcards *req = [[RequestGetRedeemedGiftcards alloc] init];
    req.transactinId = transactionId;
    
    SEND_REQUEST(req, handleGetRedeemedGiftcardsResponse, handleGetRedeemedGiftcardsResponse)
}

-(void)handleGetRedeemedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response]){
        return;
    }

    ResponseGetRedeemedGiftcards *resp = (ResponseGetRedeemedGiftcards*)response;
    if ([resp.redeemedGiftcards count] > 0){
        ModelTransaction *tran = [resp.redeemedGiftcards objectAtIndex:0];
        
        [self sendGetNearbyLocationRequest:tran];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"NoRedeemedInfoReturned", nil) delegate:nil];
    }
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailPopupView:tran locations:((ResponseGetNearbyLocations*)response).locations];
}


@end
