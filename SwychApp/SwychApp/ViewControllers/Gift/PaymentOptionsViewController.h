//
//  PaymentOptionsViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "PaymentOptionTableViewCell.h"
#import "ModelGiftcardOrderInfo.h"
#import "PaymentHandler.h"
#import "ManullyUpdateBalance.h"
@class ModelContact;

@interface PaymentOptionsViewController : SwychBaseViewController<UITableViewDelegate,UITableViewDataSource,PaymentOptionCellDelegate,PaymentHandlerDelegate,UpdateWithTextFieldPopUpDelegate>


@property (nonatomic,strong) ModelGiftcardOrderInfo *orderInfo;
@property (strong,nonatomic) ModelContact *contact;
@end
