//
//  GiftViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwychBaseViewController.h"
#import "UIGiftSelection.h"
#import "GiftForFriendView.h"
#import "GiftForMySelfView.h"
#import "ContactDetailView.h"
#import "GiftcardSelectionView.h"
#import "MessageWithWebViewPopView.h"

@interface GiftViewController : SwychBaseViewController<UIGiftSelectionDelegate, GiftForMySelfViewDelegate,GiftForFriendViewDelegate,
            ContactDetailViewDelegate,GiftcardSelectionViewDelegate,MessageWithWebViewPopViewDelegate>

@property (weak, nonatomic) IBOutlet UIGiftSelection *vGiftSelection;
@property (weak, nonatomic) IBOutlet GiftForFriendView *vForFriend;
@property (weak, nonatomic) IBOutlet GiftForMySelfView *vForMySelf;
@property (nonatomic)  BOOL backToHome;

@end

