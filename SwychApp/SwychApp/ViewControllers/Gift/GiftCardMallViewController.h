//
//  GiftCardMallViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "UIGiftcardView.h"
#import "CustomizedActionSheet.h"
#import "GiftcardSelectionView.h"
#import "GiftCardCarousel.h"

@class ModelContact;
@class ModelGiftCard;
@protocol GiftCardMallViewDelegate;

@interface GiftCardMallViewController : SwychBaseViewController<GiftcardSelectionViewDelegate,GiftCardCarouselDelegate>{
    NSMutableArray *filteredGiftcards;
}

@property (strong,nonatomic) ModelContact *contact;


@property (strong,nonatomic) NSArray<ModelGiftCard*> *giftcards;
@property (strong,nonatomic) NSArray<ModelGiftCard*> *suggestedGiftcards;
@property (nonatomic,weak) id<GiftCardMallViewDelegate> delegate;
@property (nonatomic) double amount;
@end


@protocol GiftCardMallViewDelegate <NSObject>

-(void)giftcardMallView:(GiftCardMallViewController*)giftcardMall giftcardSelected:(ModelGiftCard*)giftcard;

@end