//
//  PurchaseConfirmationViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PurchaseConfirmationViewController.h"
#import "UIRoundCornerButton.h"
#import "PurchaseConfirmationView.h"
#import "ModelContact.h"



@interface PurchaseConfirmationViewController ()

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnDone;
@property (weak, nonatomic) IBOutlet PurchaseConfirmationView *purchaseConfirmationView;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation PurchaseConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.purchaseConfirmationView.isRegifting = self.isReGifting;
    
    self.purchaseConfirmationView.transactionId = self.transactionId;
    self.purchaseConfirmationView.orderAmount = self.orderAmount;
    self.purchaseConfirmationView.earnedPoints = self.earnedPoints;
    self.purchaseConfirmationView.paymentAmount = self.paymentAmount;
    self.purchaseConfirmationView.recipientPhoneNumber =  self.recipientPhoneNumber;
    self.purchaseConfirmationView.deliveryDate = self.deliveryDate;
    self.purchaseConfirmationView.giftcard = self.giftcard;
    self.purchaseConfirmationView.contact = self.contact;

    [self.purchaseConfirmationView updateView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)hideBackButton{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnDone){
        [self.navigationController popToRootViewControllerAnimated:YES];
      [Utils postLocalNotification:NOTI_BackToHome object:nil userInfo:[NSDictionary dictionaryWithObject:self forKey:@"home"]];
    }
}

@end
