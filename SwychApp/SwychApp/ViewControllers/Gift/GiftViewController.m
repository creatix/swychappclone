//
//  GiftViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftViewController.h"
#import "GiftCardMallViewController.h"
#import "ModelGiftCard.h"
#import "GiftDetailViewController.h"
#import "LocalUserInfo.h"
#import "ModelContact.h"
#import "MessageWithWebViewPopView.h"
#import "RequestGetGiftcardsByRetailer.h"
#import "ResponseGetGiftcardsByRetailer.h"
#import "RequestGetSuggestedGiftcards.h"
#import "ResponseGetSuggestedGiftcards.h"
#import "RequestGetAvailableAndSuggestedGiftcards.h"
#import "ResponseGetAvailableAndSuggestedGiftcards.h"
#include "NSString+Extra.h"

#import "CreatixContactsManager.h"

#define KEY_CONTACT     @"CONTACT"
#define KEY_ITEM        @"ITEM"
#define KEY_IS_PHONE    @"IS_PHONE"


#define Context_GetAvailableGiftcards_ForMySelf     1
#define Context_GetAvailableGiftcards_ForFriend     2

#define Context_GetSuggestedGiftcards_ForGiftcardMallView       10
#define Context_GetSuggestedGiftcards_ForGiftForMySelf          11

@interface GiftViewController ()

@property (weak,nonatomic) IBOutlet GiftcardSelectionView *vGiftcardSelection;

-(void)setupViewInfo;
@end

@implementation GiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(sendGetBotSentGiftListsRequest) name:BotNewTransactionNotification object:nil];
    [center addObserver:self
               selector:@selector(sendGetBotLinkRequest) name:BotNewLinkNotification object:nil];
    // Do any additional setup after loading the view, typically from a nib.
    self.vGiftcardSelection.selectionViewDelegate = self;
    if(self.regiftMode){
        self.vGiftSelection.hideForMyselfSelection = YES;
    }
    [self setupViewInfo];
    _backToHome = NO;
    [Utils addNotificationObserver:self selector:@selector(backToHome:) name:NOTI_BackToHome object:nil];
}
-(void)backToHome:(id)ctr{
    _backToHome = YES;

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.vForFriend refreshContactData];
    if(!self.vForMySelf.hidden){
    [self showLoadingView:nil];
    [self sendGetAvailableGiftcardByRetailerRequest:nil];
    }
    [self setNavBarInfo];
    if (![CreatixContactsManager shareInstance].isFirstTimeSync){
        [[CreatixContactsManager shareInstance] startSyncContacts];
    }
    
    [self sendGetBotLinkRequest];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(_backToHome){
        _backToHome = NO;
        [APP swichToCardsView];
        return;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.processBot = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)hideTabbar{
    return NO;
}

-(void)setupViewInfo{
    [self.vGiftSelection setTitleForFriend:NSLocalizedString(@"TitleForFriend", nil)
                            titleForMySelf:NSLocalizedString(@"TitleForMySelf", nil)];
    
    self.vForMySelf.backgroundColor = [UIColor clearColor];
    self.vForFriend.backgroundColor = [UIColor clearColor];
    
    self.vGiftSelection.selectionDelegate = self;
  //  LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
  //  if (user.userStatus != UserStatus_PendingOTPVerified){
        self.vGiftSelection.giftForFriendSelected = NO;
//    }
//    else
//    self.vGiftSelection.giftForFriendSelected = YES;
}

-(BOOL)ignoreToSetInfo:(id)object{
    if (object == self.vGiftSelection){
        return YES;
    }
    if (object == self.vForFriend || object == self.vForMySelf){
        return YES;
    }
    return [super ignoreToSetInfo:object];
}

-(void)switchToGiftDetailViewWithSelectedGiftcard:(ModelGiftCard*)selectedGiftcard{
    GiftDetailViewController *ctrl = (GiftDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftDetailViewController"];
    if(selectedGiftcard.availability == OUTOFSTOCK){
        [self showAlert:NSLocalizedString(@"OutOfStore", nil)];
        return;
    }
    if([Utils CheckIfAmazon:selectedGiftcard]){
        MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
        [cppv AddUrl:@"amazonRedeemTerms" title:@"Disclaimer" type:YES ];
        cppv.uDelegate = self;
        cppv.giftcard = selectedGiftcard;
        [cppv show];
        return;
    }
    if([Utils CheckIfTarget:selectedGiftcard]){
        MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
        [cppv AddUrl:@"targetRedeemTerms" title:@"Disclaimer" type:YES ];
        cppv.uDelegate = self;
        cppv.giftcard = selectedGiftcard;
        [cppv show];
        return;
    }
    if(![NSString isNullOrEmpty:selectedGiftcard.disclaimerURL]){
        MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
        [cppv AddUrl:selectedGiftcard.disclaimerURL title:@"Disclaimer" type:YES ];
        cppv.uDelegate = self;
        cppv.giftcard = selectedGiftcard;
        [cppv show];
        return;
    }
    ctrl.giftcard = selectedGiftcard;
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    ModelContact *contact = [[ModelContact alloc] init];
    contact.firstName = user.firstName;
    contact.lastName = user.lastName;
    contact.phoneNumber = user.phoneNumber;
    contact.profileImage = user.avatarImage;
    
    ctrl.giftForFriend = NO;
    ctrl.contact = contact;
    [self.navigationController pushViewController:ctrl animated:YES];

}

#pragma 
#pragma mark - UIGiftSelectionDelegate
-(void)giftSelectionChanged:(UIGiftSelection*)selectionView forFriend:(BOOL)forFriend{
    if (forFriend){
        self.vForFriend.hidden = NO;
        self.vForMySelf.hidden = YES;
    }
    else{
        [self showLoadingView:nil];
        [self sendGetAvailableGiftcardByRetailerRequest:nil];
    }
}
#pragma
#pragma mark - MessageWithWebViewPopViewDelegate
-(void)NextStep:(BOOL)next obj:(id)obj tag:(int)itag{
    ModelGiftCard* selectedGiftcard = (ModelGiftCard*)obj;
    GiftDetailViewController *ctrl = (GiftDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftDetailViewController"];
    ctrl.giftcard = selectedGiftcard;
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    ModelContact *contact = [[ModelContact alloc] init];
    contact.firstName = user.firstName;
    contact.lastName = user.lastName;
    contact.phoneNumber = user.phoneNumber;
    contact.profileImage = user.avatarImage;
    
    ctrl.giftForFriend = NO;
    ctrl.contact = contact;
    [self.navigationController pushViewController:ctrl animated:YES];
    
}
#pragma
#pragma mark - GiftForMySelfViewDelegate
-(void)giftForMySelfViewGiftcardSelected:(ModelGiftCard *)giftcard{
    if (giftcard != nil){
        [self switchToGiftDetailViewWithSelectedGiftcard:giftcard];
    }
}

#pragma
#pragma mark - GiftForFriendViewDelegate
-(void)giftForFriendView:(GiftForFriendView *)forFriendView itemSelected:(ModelContact *)contact{
    
}

#pragma 
#pragma mark - ContactDetailViewDelegate
-(void)contactDetailView:(ContactDetailView *)detailView contact:(ModelContact *)contact selected:(NSString *)selectedItem isPhoneNumber:(BOOL)isPhone{
    if(self.regiftMode){
        GiftDetailViewController *ctrl = (GiftDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftDetailViewController"];
        ctrl.regiftParam = self.regiftParam;
        ctrl.giftcard = self.regiftParam.originalGiftcard;
        ctrl.contact = contact;
        ctrl.giftForFriend = YES;
        ctrl.regiftParam.selectedContactItem = selectedItem;
        ctrl.regiftParam.selectedContactItemIsPhoneNumber = isPhone;
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }
    else{
        [self showLoadingView:nil];
        [self sendGetAvailableGiftcardByRetailerRequest:[NSDictionary dictionaryWithObjectsAndKeys: contact,KEY_CONTACT,
                                                         selectedItem, KEY_ITEM,
                                                         [NSNumber numberWithBool:isPhone],KEY_IS_PHONE,nil]];
    }
    
}

#pragma
#pragma mark Server communication handling
-(void)sendGetAvailableGiftcardByRetailerRequest:(NSDictionary*)dictObjects{
    RequestGetGiftcardsByRetailer *req = [[RequestGetGiftcardsByRetailer alloc] init];
    if (dictObjects != nil){
        req.contextObject = dictObjects;
        req.context = Context_GetAvailableGiftcards_ForFriend;
        req.purchaseType = 1;
    }
    else{
        req.context = Context_GetAvailableGiftcards_ForMySelf;
        req.purchaseType = 2;
    }
    SEND_REQUEST(req, handleGetAvailableGiftcardByRetailerResponse, handleGetAvailableGiftcardByRetailerResponse);
}

-(void)handleGetAvailableGiftcardByRetailerResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetGiftcardsByRetailer class]]){
        return;
    }
    ResponseGetGiftcardsByRetailer *resp = (ResponseGetGiftcardsByRetailer*)response;
    if (resp.context == Context_GetAvailableGiftcards_ForFriend){
        GiftCardMallViewController *ctrl = (GiftCardMallViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftCardMallViewController"];
        NSDictionary *dict = (NSDictionary*)response.contextObject;
        ModelContact *c = (ModelContact*)[dict objectForKey:KEY_CONTACT];
        NSString *phoneOrEmail = [dict objectForKey:KEY_ITEM];
        BOOL selectedIsPhoneNumber = [[dict objectForKey:KEY_IS_PHONE] boolValue];
        ModelContact *contact = [[ModelContact alloc] init];
        contact.firstName = c.firstName;
        contact.lastName = c.lastName;
        contact.profileImage = c.profileImage;
        if (selectedIsPhoneNumber){
            contact.phoneNumber = phoneOrEmail;
        }
        else{
            contact.email = phoneOrEmail;
        }
        ctrl.contact = contact;
        ctrl.giftcards = resp.giftCards;
        ctrl.regiftParam = self.regiftParam;
        
        [self sendGetSuggestedGiftcardsRequest:Context_GetSuggestedGiftcards_ForGiftcardMallView contextObject:ctrl];
    }
    else{
        self.vGiftcardSelection.giftcards = resp.giftCards;
        [self.vGiftcardSelection refreshData];
        
        [self sendGetSuggestedGiftcardsRequest:Context_GetSuggestedGiftcards_ForGiftForMySelf contextObject:nil];
    }
}

-(void)sendGetSuggestedGiftcardsRequest:(NSInteger)context contextObject:(NSObject*)obj{
    [self showLoadingView:nil];
    RequestGetSuggestedGiftcards *req = [[RequestGetSuggestedGiftcards alloc] init];
    if(context == Context_GetSuggestedGiftcards_ForGiftcardMallView){
        req.purchaseType = 1;
    }
    else{
        req.purchaseType = 2;
    }
    req.context = context;
    req.contextObject = obj;
    
    SEND_REQUEST(req, handlerGetSuggestedGiftcardsResponse, handlerGetSuggestedGiftcardsResponse);
}

-(void)handlerGetSuggestedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetSuggestedGiftcards class]]){
        return;
    }
    ResponseGetSuggestedGiftcards *resp = (ResponseGetSuggestedGiftcards*)response;
    if(resp.context == Context_GetSuggestedGiftcards_ForGiftForMySelf){
        self.vForMySelf.giftcards = [resp.giftCards sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            ModelGiftCard* gift1 = (ModelGiftCard*)obj1;
            ModelGiftCard* gift2 = (ModelGiftCard*)obj2;
            
            // sort the cards so that they are sorted by amount with the favorites at the top
            if(gift1.favourite) {
                if(!gift2.favourite) {
                    return NSOrderedAscending;
                }
            }
            else if(gift2.favourite) {
                if(!gift1.favourite) {
                    return NSOrderedDescending;
                }
            }
            
            if(gift1.selfGiftingDiscount > gift2.selfGiftingDiscount) {
                return NSOrderedAscending;
            }
            else if(gift1.selfGiftingDiscount < gift2.selfGiftingDiscount) {
                return NSOrderedDescending;
            }
            else {
                return NSOrderedSame;
            }
        }];
        self.vForFriend.hidden = YES;
        self.vForMySelf.hidden = NO;
    }
    else if (resp.context == Context_GetSuggestedGiftcards_ForGiftcardMallView){
        GiftCardMallViewController *ctrl = (GiftCardMallViewController*)resp.contextObject;
        ctrl.suggestedGiftcards = resp.giftCards;
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

#pragma
#pragma mark - GiftcardSelectionViewDelegate
-(void)giftcardSelectionView:(GiftcardSelectionView *)selectionView giftcardSelected:(ModelGiftCard *)giftcard{
    [self switchToGiftDetailViewWithSelectedGiftcard:giftcard];
}
@end
