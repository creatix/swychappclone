//
//  GiftDetailViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftDetailViewController.h"
#import "ModelGiftCard.h"
#import "SelectAmountView.h"
#import "UIRoundCornerButton.h"
#import "ButtonWithTextAtBottom.h"
#import "UIRoundCornerImageView.h"
#import "GiftPaymentViewController.h"
#import "UIPlaceholderTextView.h"
#import "ModelGiftcardOrderInfo.h"
#import "ModelContact.h"
#import "ModelTransaction.h"
#import "PurchaseConfirmationView.h"
#import "LocalUserInfo.h"
#import "ModelSystemConfiguration.h"
#import "PurchaseConfirmationViewController.h"
#import "RegistrationProfileViewController.h"
#import "RequestPrepurchase.h"
#import "ResponsePrepurchase.h"
#import "RequestGetSavedGiftcards.h"
#import "ResponseGetSavedGiftcards.h"
#import "RequestPurchaseGiftcard.h"
#import "ResponsePurchaseGiftcard.h"
#import "LocationManager.h"
#import "RequestPreGift.h"
#import "ResponsePreGift.h"
#import "ModelBotTransaction.h"
#import "RequestGetGiftcardsByRetailer.h"
#import "ResponseGetGiftcardsByRetailer.h"
#import "AContactItem.h"
#define Context_PurchaseGiftRequest_Regifting   0x0201

#define ActionSheetButton_ChoosePhoto   0
#define ActionSheetButton_TakePhoto     1


@interface GiftDetailViewController ()

@property (weak,nonatomic) IBOutlet CardInfoView *cardInfoView;
@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (weak, nonatomic) IBOutlet SelectAmountView *vSelectAmount;

@property (weak, nonatomic) IBOutlet UIView *vMessage;
@property (weak, nonatomic) IBOutlet UILabel *labMessageTitle;
@property (weak, nonatomic) IBOutlet UIPlaceholderTextView *textViewMessage;
@property (weak, nonatomic) IBOutlet UIView *vMessageButtons;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnPhoto;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnVideo;


@property (weak, nonatomic) IBOutlet UIView *vSchedule;
@property (weak, nonatomic) IBOutlet UILabel *labScheduleTitle;
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivScheduleBackground;
@property (weak, nonatomic) IBOutlet UILabel *labSchedult;
@property (weak, nonatomic) IBOutlet UILabel *labBotDeliverTitle;
@property (weak, nonatomic) IBOutlet UILabel *labBotDeliver;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vBotDeliverHeigth;
@property (weak, nonatomic) IBOutlet UIView *vBotDeliver;
@property (nonatomic,strong) NSDate *selectedDeliveryDate;


@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnNext;
@property (strong, nonatomic) UIImage *selectedImage;

@property (weak, nonatomic) IBOutlet UIImageView *ivQuote;

@property (nonatomic,strong) NSArray *quotesFileNameArray;
@property (nonatomic,strong) NSString *promoCode;
@property (nonatomic,assign)  double discount;
-(IBAction)buttonTappged:(id)sender;
@end

@implementation GiftDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[LocationManager instance] startUpdateLocation];
    // Do any additional setup after loading the view.
    self.quotesFileNameArray = [NSArray arrayWithObjects:@"quote_treat.png",
                                @"quote_awesome.png",
                                @"quote_deserve.png",
                                @"quote_happy.png",
                                @"quote_monday.png",
                                @"quote_tgif.png",
                                @"quote_thanks.png",
                                @"quote_when.png",
                                nil];
    [self setup];
    if (self.regiftMode){
        [self regiftSetup];
    }
    
    [Utils addNotificationObserver:self selector:@selector(userStatusActivatedNotificationHandler:) name:NOTI_UserStatusActivated object:nil];
    if(self.botTransaction){
            [self.vSelectAmount setCurrentSelectedAmountByValue:[self.botTransaction.amount doubleValue]];
        
    }
    [self setBotView];
}
-(void)setBotView{
    if(self.botTransaction){
        self.labBotDeliver.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.labBotDeliver.layer.borderWidth = 1.0;
        self.labBotDeliver.layer.cornerRadius = 3.0f;
        self.labBotDeliver.backgroundColor = [UIColor whiteColor];
        NSString* str = self.contact.phoneNumber?self.contact.phoneNumber:self.contact.email;
        self.labBotDeliver.text = [NSString stringWithFormat:@" %@%@ %@", self.contact.fullName,@":",str ];
        self.vBotDeliverHeigth.constant = 60.0f;
        [self AddRecognizer];
    }
    else{
        self.vBotDeliverHeigth.constant = 0.0f;
    }
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.vSelectAmount updateConstraints_After_gifted];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.giftForFriend){
        self.vMessage.hidden = NO;
        self.vSchedule.hidden = YES;//Temporary hide Schedule deliver section
        self.ivQuote.hidden = YES;
    }
    else{
        self.vMessage.hidden = YES;
        self.vSchedule.hidden = YES;
        self.ivQuote.hidden = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navbarBackButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)regiftSetup{
    if (self.regiftParam != nil){
        [self.vSelectAmount setCurrentSelectedAmountByValue:self.regiftParam.amount];
        self.vSelectAmount.userInteractionEnabled = NO;
    }
}

-(void)setup{
    _defaultMessage = [NSString stringWithFormat:@"%@, %@", _contact.firstName, NSLocalizedString(@"PlaceHolder_Message", nil)];
    if (self.botTransaction != nil && self.botTransaction.message != nil){
        self.textViewMessage.text = self.botTransaction.message;
    }
    else{
        self.textViewMessage.placeholder = _defaultMessage;
    }
    self.cardInfoView.giftcard = self.giftcard;
    self.cardInfoView.imgBackground = self.contact.profileImage;
    self.cardInfoView.footerType = CIVFT_RedeemTypeAtRight;
    self.cardInfoView.delegate = self;
    self.cardInfoView.vGiftcard.giftcardViewDelegate = (id<UIGiftcardViewDelegate>)self;
    self.cardInfoView.labFooterGiftcardName.hidden = YES;
    self.btnPhoto.itemDelegate =(id<ButtonWithTextAtBottomDelegate>) self;
    self.btnVideo.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    self.vSelectAmount.giftcard = self.giftcard;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnSchedultBackgroundImageView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self.ivScheduleBackground addGestureRecognizer:tap];
    
    int quoteIndex = 0 + arc4random() % (self.quotesFileNameArray.count - 0);
    if (quoteIndex >= self.quotesFileNameArray.count){
        quoteIndex =(int)( self.quotesFileNameArray.count - 1);
    }
    self.ivQuote.image = [UIImage imageNamed:[self.quotesFileNameArray objectAtIndex:quoteIndex]];
    if (!self.regiftMode){
        if(!self.botTransaction){
          [self.vSelectAmount SetDefaultButton];
        }
     }
}
-(void)AddRecognizer{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate = self;
    [self.labBotDeliver addGestureRecognizer:tap];
    self.labBotDeliver.userInteractionEnabled = YES;
}
-(void)handleTap:(id)sender{
    ChooseRecipientViewController *ctrl = (ChooseRecipientViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"ChooseRecipientViewController"];
    ctrl.botTransaction = self.botTransaction;
    ctrl.processBot = self.processBot;
    ctrl.selectionViewDelegate = self;
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)userStatusActivatedNotificationHandler:(NSNotification*)noti{
    NSArray *controllers = self.navigationController.viewControllers;
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    for(UIViewController *vc in controllers){
        if ([vc isKindOfClass:[RegistrationProfileViewController class]]){
            break;
        }
        else{
            [array addObject:vc];
        }
    }
    self.navigationController.viewControllers = array;
    [self goNext];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)ignoreToSetInfo:(id)object{
    if (object == self.vSelectAmount || object == self.vMessageButtons){
        return YES;
    }
    
    return [super ignoreToSetInfo:object];
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"GiftDetailViewTitle", nil);
}
-(void)KeyboardDoneClicked{
    if([self.giftcard validateAmount:self.vSelectAmount.selectedAmount showAlert:YES]){
        if(self.vSelectAmount.selectedAmount<10){
          [self showAlert:NSLocalizedString(@"Error_InvalidAmountSelection_MUSTOVER10",nil)];
        }
    }
}
-(IBAction)buttonTappged:(id)sender{
    if (sender == self.btnNext){
        if (self.vSelectAmount.selectedAmount <= 0){
            [self showAlert:NSLocalizedString(@"Error_EmptyAmount", nil)];
            return;
        }
        if (![self.giftcard validateAmount:self.vSelectAmount.selectedAmount showAlert:YES]){
            return;
        }
        LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (user.userStatus == UserStatus_PendingOTPVerified){
            RegistrationProfileViewController *vc = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
            vc.user = user;
            vc.isActiveUserInTransaction = YES;
            [APP pushViewController:vc animated:YES];
        }
        else{
            [self goNext];
        }
    }
}

-(void)goNext{
    if(self.vSelectAmount.selectedAmount<10){
        [self showAlert:NSLocalizedString(@"Error_InvalidAmountSelection_MUSTOVER10",nil)];
        return;
    }
    if (self.regiftMode){
        [self regift];
        return;
    }
    [self sendprepurchaseRequest];
   // [self sendGetSavedGiftcardsRequest];

}

-(void)userInfoUpdated:(ModelUser*)user  context:(NSInteger)context contextObject:(NSObject*)contextObject{
    if (user != nil && ![user needUpdateInfoToContinuePurchase]){
        [self goNext];
    }
    else{
        [self showUserInfoUpdatePopupView];
    }
}
-(void)regift{
   // [self sendPurchaseGiftcardRequestForRegifting];
    [self sendPreGift];
}


-(void)showPaymentView:(NSArray<ModelTransaction*>*)credits{
    GiftPaymentViewController *ctrl = (GiftPaymentViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftPaymentViewController"];
    ModelGiftcardOrderInfo *order = [[ModelGiftcardOrderInfo alloc] init];
    order.giftcard = self.giftcard;
    order.amount = self.vSelectAmount.selectedAmount;
    NSString* sendMessage = ![self.textViewMessage.text isEqualToString:@""]?self.textViewMessage.text:_defaultMessage;
    order.message = sendMessage;
    order.imagePhoto = self.selectedImage;
    order.dateDelivery = self.selectedDeliveryDate;
    if (self.contact.phoneNumber != nil){
        order.recipientPhoneNumberOrEmail = self.contact.phoneNumber;
        order.recipientIdenIsPhoneNumber = YES;
    }
    else{
        order.recipientPhoneNumberOrEmail = self.contact.email;
        order.recipientIdenIsPhoneNumber = NO;
    }
    order.giftForFriend = self.giftForFriend;
    ctrl.botTransaction = self.botTransaction;
    ctrl.processBot = self.processBot;
    ctrl.orderInfo = order;
    ctrl.availableCredits = credits;
    ctrl.contact = self.contact;
    ctrl.promoCode = self.promoCode;
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)touchOnSchedultBackgroundImageView:(id)sender{
    CustomizedInputView *input = [CustomizedInputView  createInstanceWithViewType:CustomizedInputView_Date objectWithInput:self.ivScheduleBackground];
    input.delegate = self;
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    NSDate *dtNow = [NSDate date];
    NSDate *dtMax =  nil;
    if (localUser.sysConfig != nil && localUser.sysConfig.maximumGiftCardSchedulingDays > 0){
        dtMax = [dtNow dateByAddingTimeInterval:60 * 60 * 24 * localUser.sysConfig.maximumGiftCardSchedulingDays];
    }
    [input setDatePickerMinDate:dtNow maxDate:dtMax];
    [input show];
}
#pragma  mark - GiftCardMallViewDelegate
-(void)giftcardMallView:(GiftCardMallViewController*)giftcardMall giftcardSelected:(ModelGiftCard*)giftcard{
    self.giftcard = giftcard;
    self.cardInfoView.giftcard = self.giftcard;
}
-(void)ChooseRecipientViewController:(ChooseRecipientViewController*)selectionView ContactSelected:(AContactItem*)contact{
    self.contact = contact.parentModal;
    NSString* str = contact.Content_Item;
    self.labBotDeliver.text = [NSString stringWithFormat:@" %@%@ %@", self.contact.fullName,@":",str ];
    self.botTransaction.recipientEmail = self.contact.email;
    self.botTransaction.recipientPhoneNumber = self.contact.phoneNumber;
}
#pragma  mark - UIGiftcardViewDelegate
-(void)giftcardViewTapped:(UIGiftcardView*)giftcardView{
    if(self.botTransaction){
        [self sendGetGiftcarsByRetailerRequest];
    }
}
-(void)sendGetGiftcarsByRetailerRequest{
    RequestGetGiftcardsByRetailer *req = [[RequestGetGiftcardsByRetailer alloc] init];
    req.purchaseType = 3;
    SEND_REQUEST(req, handleGetAvailableGiftcardByRetailerResponse, handleGetAvailableGiftcardByRetailerResponse);
}

-(void)handleGetAvailableGiftcardByRetailerResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetGiftcardsByRetailer class]]){
        return;
    }
    ResponseGetGiftcardsByRetailer *resp = (ResponseGetGiftcardsByRetailer*)response;
    [self swithToGiftcardMallView:resp.giftCards];
}
-(void)swithToGiftcardMallView:(NSArray<ModelGiftCard*>*)giftcards{
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *cardItem, NSDictionary<ModelGiftCard *,id> * _Nullable bindings) {
        if(cardItem.giftCardId == self.botTransaction.giftCard.giftCardId) return NO;
        if(cardItem.availability == OUTOFSTOCK) return NO;
        return [cardItem validateAmount:[cardItem amountSwychTo:[self.botTransaction.amount doubleValue]*cardItem.conversionRate] showAlert:NO];
    }];
    giftcards = [giftcards filteredArrayUsingPredicate:pred];
    GiftCardMallViewController *ctrl = (GiftCardMallViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftCardMallViewController"];
    ctrl.giftcards = giftcards;
    ctrl.delegate = (id<GiftCardMallViewDelegate>)self;
    ctrl.amount = [self.botTransaction.amount doubleValue];
    ctrl.botTransaction = self.botTransaction;
    ctrl.processBot = self.processBot;
    [self.navigationController pushViewController:ctrl animated:YES];
}
#pragma
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    self.selectedImage = chosenImage;
    
    self.btnPhoto.pictureImageViewImage = chosenImage;
    self.btnPhoto.showCloseButton = YES;

}

#pragma
#pragma mark -UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == ActionSheetButton_ChoosePhoto){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (buttonIndex == ActionSheetButton_TakePhoto){
#if TARGET_IPHONE_SIMULATOR
        return;
#endif
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

#pragma 
#pragma mark - ButtonWithTextAtBottomDelegate

-(void)buttonWithTextAtBottonItemTapped:(ButtonWithTextAtBottom*)buttonItem{
    if (buttonItem == self.btnVideo){
        
    }
    else if (buttonItem == self.btnPhoto){
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Button_Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Button_ChoosePhoto", nil),NSLocalizedString(@"Button_TakePhoto", nil), nil];
        [sheet showInView:self.view];

    }
}

-(void)buttonWithTextAtBottomCloseButtonTapped:(ButtonWithTextAtBottom*)buttonItem{
    if (buttonItem == self.btnPhoto){
        self.btnPhoto.pictureImageViewImage = nil;
        self.btnPhoto.showCloseButton = NO;
    }
}

#pragma 
#pragma mark - CardInfoViewDelegate
-(void)cardInfoView:(CardInfoView *)cardInfoView termsLinkTapped:(NSString *)url{
    
}
#pragma 
#pragma mark - CustomizedInputViewDelegate
-(void)customizedInputViewValueChanged:(CustomizedInputView*)inputView{
    NSDate *dateSelected = [inputView getDate];
    NSDate *dateNow = [NSDate date];
    if ([[NSCalendar currentCalendar] isDate:dateSelected inSameDayAsDate:dateNow]){
        self.selectedDeliveryDate = nil;
        self.labSchedult.text = NSLocalizedString(@"DeliveryDate_Now", nil);
    }
    else{
        self.selectedDeliveryDate = dateSelected;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"MMMM dd yyy";
        self.labSchedult.text = [formatter stringFromDate:dateSelected];
    }
}

-(void)customizedInputViewDoneButtonTapped:(CustomizedInputView*)inputView{
    //NSDate *date = [inputView getDate];
}

-(void)customizedInputviewDismissed:(CustomizedInputView *)inputView doneButtonTapped:(BOOL)done{
    
}

#pragma
#pragma mark Server communication handling
-(void)sendGetSavedGiftcardsRequest{
    RequestGetSavedGiftcards *req = [[RequestGetSavedGiftcards alloc] init];
    
    SEND_REQUEST(req, handleGetSavedGiftcardsResponse, handleGetSavedGiftcardsResponse);
}

-(void)handleGetSavedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetSavedGiftcards class]]){
        return;
    }
    ResponseGetSavedGiftcards *resp = (ResponseGetSavedGiftcards*)response;
    [self showPaymentView:resp.savedGiftcardsTransaction];
}
-(void)sendprepurchaseRequest{
    _promoCode=@"";
    _discount = 0;
    RequestPrepurchase *req = [[RequestPrepurchase alloc] init];
    req.gifcardId = self.giftcard.giftCardId;
    req.amount = self.vSelectAmount.selectedAmount;
    if(self.giftForFriend){
        req.purchaseType = 1;
    }
    else
        req.purchaseType=2;
    SEND_REQUEST(req, handleprepurchaseResponse, handleprepurchaseResponse);
}
-(void)handleprepurchaseResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponsePrepurchase class]]){
        return;
    }
    ResponsePrepurchase *resp = (ResponsePrepurchase*)response;
    _discount = resp.discount;
    _promoCode = resp.promoCode;
    [self sendGetSavedGiftcardsRequest];
}
#pragma 
#pragma mark - For regifting
-(void)sendPreGift{
    RequestPreGift *req = [[RequestPreGift alloc] init];
    if (self.regiftParam.selectedContactItemIsPhoneNumber){
        req.recipientPhoneNumber = self.regiftParam.selectedContactItem;
    }
    else{
        req.recipientEmail = self.regiftParam.selectedContactItem;
    }
    
    req.giftCardId = self.regiftParam.originalGiftcard.giftCardId;
    req.giftCardAmount = self.regiftParam.amount;
    ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
    mpi.paymentType = PaymentOption_SwychBalance;
    mpi.paymentReferenceNumber =[NSString stringWithFormat:@"%@",self.regiftParam.originalTransaction.transactionId];
    mpi.amount = self.regiftParam.amount;
    
    NSArray<ModelPaymentInfo*> * payments = [NSArray arrayWithObject:mpi];
    
    req.payments = payments;
    req.message = self.textViewMessage.text;
    
    if (self.selectedDeliveryDate != nil){
        req.deliverDate = [Utils getTimeString:self.selectedDeliveryDate];
    }
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchasePreGiftcardResponse, handPurchasePreGiftcardResponse);
}
-(void)handPurchasePreGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePreGift *resp = (ResponsePreGift*)response;
    if(resp.emailVerificationNeeded){
        [self EmailValidate:^{
            [self sendPreGift];
            
        } Email:resp.senderEmail];
        return;
    }
    if(resp.validatePassword){
        [self ValidatePassword:(int)resp.context];
        return;
    }
    [self sendPurchaseGiftcardRequestForRegifting];
    
}
-(void)makePayment:(PaymentMethod)method{
    [self sendPurchaseGiftcardRequestForRegifting];
}
-(void)sendPurchaseGiftcardRequestForRegifting{
    RequestPurchaseGiftcard *req = [[RequestPurchaseGiftcard alloc] init];
    if (self.regiftParam.selectedContactItemIsPhoneNumber){
        req.recipientPhoneNumber = self.regiftParam.selectedContactItem;
    }
    else{
        req.recipientEmail = self.regiftParam.selectedContactItem;
    }
    if(self.botTransaction){
        req.botTransactionId = self.botTransaction.botTransactionId;
    }
    req.giftCardId = self.regiftParam.originalGiftcard.giftCardId;
    req.giftCardAmount = self.regiftParam.amount;
    ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
    mpi.paymentType = PaymentOption_SwychBalance;
    mpi.paymentReferenceNumber =[NSString stringWithFormat:@"%@",self.regiftParam.originalTransaction.transactionId];
    mpi.amount = self.regiftParam.amount;
    
    NSArray<ModelPaymentInfo*> * payments = [NSArray arrayWithObject:mpi];
    
    req.payments = payments;
    req.message = self.textViewMessage.text;
    NSData *data = UIImageJPEGRepresentation(self.selectedImage, 1.0);
    req.image = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if (self.selectedDeliveryDate != nil){
        req.deliverDate = [Utils getTimeString:self.selectedDeliveryDate];
    }
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchaseGiftcardResponse, handPurchaseGiftcardResponse);
    
}

-(void)handPurchaseGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePurchaseGiftcard *resp = (ResponsePurchaseGiftcard*)response;
    [APP UpdateBanlance:resp.user.swychPoint];
    [self showConfirmationPopupView:resp.transactionId earnedPoints:resp.earnedPoints];
}

-(void)showConfirmationPopupView:(NSString*)transactionId earnedPoints:(double)earnedPoints{
    PurchaseConfirmationViewController *ctrl =(PurchaseConfirmationViewController*) [Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"PurchaseConfirmationViewController"];
    
    ctrl.transactionId = transactionId;
    ctrl.orderAmount = self.regiftParam.amount;
    ctrl.earnedPoints = earnedPoints;
    ctrl.paymentAmount = 0;
    ctrl.recipientPhoneNumber = self.regiftParam.selectedContactItem;
    ctrl.deliveryDate = self.selectedDeliveryDate == nil ? [NSDate date] : self.selectedDeliveryDate;
    ctrl.giftcard = self.regiftParam.originalGiftcard;
    ctrl.contact = self.contact;
    ctrl.isReGifting = YES;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}
#pragma
#pragma mark PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
