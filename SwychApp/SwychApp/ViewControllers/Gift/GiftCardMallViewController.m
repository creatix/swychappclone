//
//  GiftCardMallViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftCardMallViewController.h"
#import "ModelContact.h"
#import "GiftDetailViewController.h"

#define ITEM_WIDTH_COLLECTION_VIEW  160

#define ACTION_SHEET_CONTEXT_FILTER     1
#define ACTION_SHEET_CONTEXT_SORTING    2


@interface GiftCardMallViewController ()
@property (weak, nonatomic) IBOutlet UIView *vTopView;
@property (weak, nonatomic) IBOutlet GiftcardSelectionView *vGiftcardSelectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *labContactName;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *labSuggestedFor;
@property (weak, nonatomic) IBOutlet UIView *vCardList;

@property (weak, nonatomic) IBOutlet GiftCardCarousel *giftCardCarousel;
@end

@implementation GiftCardMallViewController

@synthesize giftcards = _giftcards;
@synthesize suggestedGiftcards = _suggestedGiftcards;
@synthesize delegate = _delegate;

-(void)setDelegate:(id<GiftCardMallViewDelegate>)delegate{
    _delegate = delegate;
    if (_delegate != nil){
        self.constraintTopViewHeight.constant = 0.0f;
    }
    
}

-(id<GiftCardMallViewDelegate>)delegate{
    return _delegate;
}

-(NSArray*)suggestedGiftcards{
    return _suggestedGiftcards;
}

-(void)setSuggestedGiftcards:(NSArray<ModelGiftCard *> *)suggestedGiftcards{
    _suggestedGiftcards = suggestedGiftcards;
    if (self.giftCardCarousel != nil){
        self.giftCardCarousel.giftCards = suggestedGiftcards;
    }
}

-(NSArray*)giftcards{
    return _giftcards;
}

-(void)setGiftcards:(NSArray *)giftcards{
    _giftcards = giftcards;
    if (self.vGiftcardSelectionView != nil){
        self.vGiftcardSelectionView.giftcards = giftcards;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.giftCardCarousel.backgroundColor = [UIColor clearColor];
    if(self.botTransaction){
        self.vGiftcardSelectionView.ifHiddenPrice = YES;
    }
    self.vGiftcardSelectionView.selectionViewDelegate = self;
    self.vGiftcardSelectionView.amount = _amount;
    [self setupInfos];
    
    self.vGiftcardSelectionView.giftcards = self.giftcards;
    [self.vGiftcardSelectionView refreshData];
    
    self.giftCardCarousel.giftCards = self.suggestedGiftcards;
    
    if (_delegate != nil){
        self.vTopView.hidden = YES;
        self.constraintTopViewHeight.constant = 64.0f;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.contact != nil && self.contact.profileImage != nil){
        self.ivProfileImage.image = self.contact.profileImage;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)ignoreToSetInfo:(id)object{
    if (object == self.vGiftcardSelectionView){
        return YES;
    }
    return [super ignoreToSetInfo:object];
}

-(void)switchToGiftcardDetailView:(ModelGiftCard*)selectedGiftCard{
    if (self.delegate != nil){
        [self.navigationController popViewControllerAnimated:YES];
        if([self.delegate respondsToSelector:@selector(giftcardMallView:giftcardSelected:)]){
            [self.delegate giftcardMallView:self giftcardSelected:selectedGiftCard];
        }
    }
    else{
        GiftDetailViewController *ctrl = (GiftDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"GiftDetailViewController"];
        ctrl.giftcard = selectedGiftCard;
        ctrl.contact = self.contact;
        ctrl.giftForFriend = YES;
        ctrl.regiftParam = self.regiftParam;
        if(ctrl.giftcard.availability == OUTOFSTOCK){
            [self showAlert:NSLocalizedString(@"OutOfStore", nil)];
            return;
        }
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)hideTabbar{
    return YES;
}

-(void)setupInfos{
    
    self.vGiftcardSelectionView.backgroundColor = [UIColor clearColor];
    self.vTopView.backgroundColor  = [UIColor clearColor];
    self.ivProfileImage.backgroundColor  = [UIColor clearColor];
    self.labContactName.backgroundColor  = [UIColor clearColor];
    self.labSuggestedFor.backgroundColor  = [UIColor clearColor];
    self.vCardList.backgroundColor  = [UIColor clearColor];

    self.labSuggestedFor.text = NSLocalizedString(@"SuggestedFor", nil);
    self.labContactName.text = self.contact == nil ? @"" : [self.contact.fullName uppercaseString];
    
}

#pragma
#pragma mark - GiftcardSelectionViewDelegate
-(void)giftcardSelectionView:(GiftcardSelectionView *)selectionView giftcardSelected:(ModelGiftCard *)giftcard{
    [self switchToGiftcardDetailView:giftcard];
    
}

#pragma 
#pragma mark - GiftCardCarouselDelegate
-(void)giftCardCarousel:(GiftCardCarousel *)giftCardCarousel giftCardSelected:(ModelGiftCard *)giftCard transaction:(ModelTransaction *)transacton{
    [self switchToGiftcardDetailView:giftCard];
}

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
          giftCardFocused:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    
}

@end
