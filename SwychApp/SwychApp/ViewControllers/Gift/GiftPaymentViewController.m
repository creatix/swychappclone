//
//  GiftPaymentViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftPaymentViewController.h"
#import "CardInfoView.h"
#import "ModelGiftcardOrderInfo.h"
#import "ModelGiftCard.h"
#import "PaymentOptionsViewController.h"
#import "UIRoundCornerButton.h"
#import "ModelPaymentInfo.h"
#import "ModelContact.h"
#import "PurchaseConfirmationView.h"
#import "PurchaseForMyselfConfirmationView.h"
#import "PurchaseConfirmationViewController.h"
#import "IssuedGiftcardDetailViewController.h"
#import "ModelTransaction.h"
#import "LocationManager.h"
#import "ModelLocation.h"
#import "ModelUser.h"
#import "RequestPreGift.h"
#import "ResponsePreGift.h"
#import "RequestVerifyPromotionCode.h"
#import "ResponseVerifyPromotionCode.h"
#import "RequestPurchaseGiftcard.h"
#import "ResponsePurchaseGiftcard.h"
#import "RequestSwychBoard.h"
#import "ResponseSwychBoard.h"
#import "RequestGetRedeemedGiftcards.h"
#import "ResponseGetRedeemedGiftcards.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"
#import "UIButton+Extra.h"
#import "ModelAmazonPaymentToken.h"
#import "NSObject+Extra.h"
#import "LocalUserInfo.h"
#import "ModelSystemConfiguration.h"
#import "AmazonPayPayment.h"
#import "ModelBotTransaction.h"
#import "AnalyticsHandler.h"
#import "AnalyticsShareASale.h"
#import "TrackingEventGiftCardPurchasedForSelfGifting.h"
#import "TrackingEventGiftCardPurchasedForSendToFriend.h"
#import "ModelSwychBillingInfo.h"
#import "NSObject+Extra.h"
#import "ModelApplePayData.h"
#import "ModelAddress.h"
#import "ModelSwychAddress.h"

#define BUTTON_COLOR_CHECKOUT   UICOLORFROMRGB(192,31,109,1.0)

#define Context_Purchase_ForFriend      1
#define Context_Purchase_ForMyself      2
#define Context_ApplePayNotConfigured   3

@interface GiftPaymentViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *ivViewBackground;
@property (weak, nonatomic) IBOutlet CardInfoView *vCardInfo;
@property (weak, nonatomic) IBOutlet UIView *vBottomView;
@property (weak, nonatomic) IBOutlet OrderCheckoutDetailView *vOrderDetailView;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePaymentMethod;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnApplePay;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnApplePay2;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCheckout;
@property (weak, nonatomic) IBOutlet UIButton *btnAmazonPay;
@property (weak, nonatomic) IBOutlet UIButton *btnAmazonPay2;
@property (weak, nonatomic) IBOutlet UIView *vAmazonAppleBTN;

@property (nonatomic) int iPreGiftMethod;
@property (nonatomic,strong) ModelPaymentProcessorData *processorData;

@property (nonatomic,strong) PKPassLibrary *passLib;

- (IBAction)buttonTapped:(id)sender;
@end

@implementation GiftPaymentViewController

@synthesize orderInfo = _orderInfo;

-(void)setOrderInfo:(ModelGiftcardOrderInfo *)orderInfo{
    _orderInfo = orderInfo;
}

-(ModelGiftcardOrderInfo*)orderInfo{
    return _orderInfo;
}
-(void)setPromoCode:(NSString *)promoCode{

    _promoCode = promoCode;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[LocationManager instance] startUpdateLocation];
    _vOrderDetailView.promoCode = _promoCode;
    [self setup];
    [self setupApplePayButton];
    
    [self.vOrderDetailView update];
    
    self.passLib = [[PKPassLibrary alloc] init];
    
    [self addNotificationObserverByName:UIApplicationDidBecomeActiveNotification];
}
-(void) allFunctions{
    [[LocationManager instance] startUpdateLocation];
    
}


-(BOOL)didReceivedLocalNotification:(NSNotification *)noti{
    if([noti.name compare:UIApplicationDidBecomeActiveNotification] == NSOrderedSame){
        [self setupApplePayButton];
    }
    return YES;
}

-(void)appWillEnterForeground:(NSNotification*)noti{
    [self setupApplePayButton];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if( _paymentsBackUp){
            NSMutableArray* array = [NSMutableArray arrayWithCapacity:_paymentsBackUp.count];
            for(int i=0;i<_paymentsBackUp.count;i++){
                [array addObject:_paymentsBackUp[i]];
            }
            self.orderInfo.payments = array;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.vOrderDetailView updateConstraints_After_gifted];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setup{
    self.vOrderDetailView.delegate = self;
    self.vOrderDetailView.backgroundColor = UICOLOR_CLEAR;
    
    if (self.orderInfo != nil){
        self.vCardInfo.giftcard = self.orderInfo.giftcard;
        if(self.orderInfo.payments){
        _paymentsBackUp = [NSMutableArray arrayWithCapacity:self.orderInfo.payments.count];
            for(int i=0;i<self.orderInfo.payments.count;i++){
                [_paymentsBackUp addObject:self.orderInfo.payments[i]];
            }
        }
        self.vCardInfo.footerType = CIVFT_AmountAtRight;
        self.vCardInfo.imgBackground = self.contact.profileImage;
        
        self.vCardInfo.amount = self.orderInfo.amount;
        
        [self.vOrderDetailView setPurchaseInfo:self.orderInfo.amount giftcard:self.orderInfo.giftcard credits:self.availableCredits];
        if(self.orderInfo.forSwych){
            [self.vOrderDetailView setDiscountAmount:self.orderInfo.swychToAmount description:NSLocalizedString(@"Desc_SwychCredit", nil)];
            self.vOrderDetailView.disableCredits = YES;
            self.vOrderDetailView.allowMultipleDiscount = YES;
        }
        else if (!self.orderInfo.giftForFriend){
            double discount = self.orderInfo.amount * self.orderInfo.giftcard.selfGiftingDiscount;
            [self.vOrderDetailView setDiscountAmount:discount description:NSLocalizedString(@"Desc_SelfGiftingDiscount", nil)];
        }
    }
    
    [self.btnChangePaymentMethod setTitleColor:UICOLORFROMRGB(76, 77, 77, 1.0)];
    [self.btnCheckout setTitle:NSLocalizedString(@"Button_Checkout", nil)];
    self.btnCheckout.backgroundColor = BUTTON_COLOR_CHECKOUT;
    self.vAmazonAppleBTN.backgroundColor = UICOLOR_CLEAR;
    
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    
    if (lui.sysConfig.amazonPayEnabled){
        self.btnApplePay.hidden = YES;
        self.btnApplePay2.hidden = NO;
        self.btnAmazonPay.hidden = NO;
        self.btnAmazonPay2.hidden = NO;
    }
    else{
        self.btnApplePay.hidden = NO;
        self.btnApplePay2.hidden = YES;
        self.btnAmazonPay.hidden = YES;
        self.btnAmazonPay2.hidden = YES;
    }
}

-(void)setupApplePayButton{
    self.btnCheckout.hidden = YES;
    self.vAmazonAppleBTN.hidden = NO;
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if(![[PaymentHandler instance] isPaymentMethodAvailable:PaymentMethod_ApplePay]){
        if (lui.sysConfig.amazonPayEnabled == NO){
            self.btnCheckout.hidden = NO;
            self.vAmazonAppleBTN.hidden = YES;
        }
        else{
            self.btnApplePay2.hidden = YES;
            self.btnApplePay.hidden = YES;
            self.btnAmazonPay.hidden = YES;
            self.btnAmazonPay2.hidden = NO;
        }
    }
    else{
        self.btnAmazonPay2.hidden = YES;
        self.btnAmazonPay.hidden = NO;
        if (![[PaymentHandler instance] isPaymentMethodCanMakePayment:PaymentMethod_ApplePay]){
            [self.btnApplePay setButtonBackgroundImage:[UIImage imageNamed:@"apple_pay_setup_black.png"]];
            [self.btnApplePay2 setButtonBackgroundImage:[UIImage imageNamed:@"apple_pay_setup_black.png"]];
        }
        else{
            [self.btnApplePay setButtonBackgroundImage:[UIImage imageNamed:@"apple_pay_black.png"]];
            [self.btnApplePay2 setButtonBackgroundImage:[UIImage imageNamed:@"apple_pay_black.png"]];
        }
    }
}


-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"PaymentViewTitle", nil);
}
-(BOOL)CheckLocation{
    return YES;
CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if(!loc){
        [Utils showAlert:NSLocalizedString(@"locationserviceerror", nil) delegate:nil];
    }
    return loc?YES:NO;
}
- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnApplePay || sender == self.btnApplePay2){
        if(![[PaymentHandler instance] isPaymentMethodCanMakePayment:PaymentMethod_ApplePay]){
            PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
            [passLib openPaymentSetup];
        }
        else{
            if([self CheckLocation]){
                _iPreGiftMethod = 0;
                [self sendPreGift:PaymentMethod_ApplePay context:PAY_METHOD_APPPLE_PAY];
            }
        }
    }
    else if (sender == self.btnAmazonPay || sender == self.btnAmazonPay2){
        if ([self CheckLocation]){
            _iPreGiftMethod = 0;
            [self sendPreGift:PaymentMethod_AmazonPay context:PAY_METHOD_AMAZON];
        }
//        [ [PaymentHandler instance] makePayment:PaymentMethod_AmazonPay
//                                         amount:self.vOrderDetailView.amountDue
//                                    description:self.orderInfo.giftcard.retailer
//                                   withDelegate:self
//                                 viewController:self];
    }
    else if (sender == self.btnCheckout){
        if([self CheckLocation])
        [self checkout];
    }
    else if (sender == self.btnChangePaymentMethod){
        if(![self CheckLocation]) return;
        PaymentOptionsViewController *ctrl = (PaymentOptionsViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"PaymentOptionsViewController"];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.vOrderDetailView.payments];
        if (self.orderInfo.forSwych){
            [array addObject:self.orderInfo.payments.firstObject];
        }
        self.orderInfo.payments = array;
        self.orderInfo.amountDue = self.vOrderDetailView.amountDue;
        ctrl.botTransaction = self.botTransaction;
        ctrl.processBot = self.processBot;
        ctrl.contact = self.contact;
        ctrl.orderInfo = self.orderInfo;
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

-(void)makePayment:(PaymentMethod)method{
     double amountDue = self.vOrderDetailView.amountDue;
    if (amountDue > 0.001){
        NSString *paymentDesc = nil;
        switch (method) {
            case PaymentMethod_ApplePay:
                paymentDesc = PAYMENT_DESCRIPTION_APPLEPAY;
                break;
            case PaymentMethod_PayPal:
                paymentDesc = self.orderInfo.giftcard.retailer;
                break;
            case PaymentMethod_DirectCreditCard:
                paymentDesc = self.orderInfo.giftcard.retailer;
                break;
            default:
                paymentDesc = self.orderInfo.giftcard.retailer;
                break;
        }
        [[PaymentHandler instance] makePayment:method
                                        amount:amountDue
                                   description:paymentDesc
                                  withDelegate:self
                                viewController:self
                                productImage:nil
                                 contextObject:self.processorData];
        
    }
    else{
    [self sendPurchaseGiftcardRequest:nil paymentMethod:PaymentMethod_None];
    }
}
-(void)checkout{
    double amountDue = self.vOrderDetailView.amountDue;
    if (amountDue > 0.001){
//        [[PaymentHandler instance] makePayment:PaymentMethod_PayPal
//                                        amount:amountDue
//                                   description:self.orderInfo.giftcard.retailer
//                                  withDelegate:self
//                                viewController:self];
        _iPreGiftMethod = 1;
        [self sendPreGift:PaymentMethod_PayPal context:PAY_METHOD_PAYPAL];
    }
    else{
        _iPreGiftMethod = 2;
        [self sendPreGift:PaymentMethod_None context:PAY_METHOD_NONE];
        //[self sendPurchaseGiftcardRequest:nil paymentMethod:PaymentMethod_None];
    }
}

-(void)showGiftcardDetailPopupView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    ctrl.backToRootViewController = YES;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)showConfirmationPopupView:(NSString*)transactionId earnedPoints:(double)earnedPoints{
    //PurchaseConfirmationView *v = (PurchaseConfirmationView*)[Utils loadViewFromNib:@"PurchaseConfirmationView" viewTag:1];
    PurchaseConfirmationViewController *ctrl =(PurchaseConfirmationViewController*) [Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"PurchaseConfirmationViewController"];
    
    ctrl.transactionId = transactionId;
    ctrl.orderAmount = self.orderInfo.amount;
    ctrl.earnedPoints = earnedPoints;
    ctrl.paymentAmount = self.vOrderDetailView.amountDue;
    ctrl.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    ctrl.deliveryDate = self.orderInfo.dateDelivery == nil ? [NSDate date] : self.orderInfo.dateDelivery;
    ctrl.giftcard = self.orderInfo.giftcard;
    ctrl.contact = self.contact;
    
    //[v show];
    [self.navigationController pushViewController:ctrl animated:YES];
}

#pragma
#pragma mark PopupConfirmationViewDelegate
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma 
#pragma mark - PaymentHanderDelegate
-(void)paymentCompleted:(BOOL)success paymentToken:(NSString *)token error:(NSError *)error method:(PaymentMethod)method{
    if (!success){
        if (method == PaymentMethod_ApplePay && error.code == ERR_CODE_PAYMENT_ApplePay_Not_Configured){
            NSString *str = NSLocalizedString(@"ApplePayNotConfigured", nil);
            [self showAlert:str title:nil delegate:self
                firstButton:NSLocalizedString(@"Button_NO", nil)
               secondButton:NSLocalizedString(@"Button_YES",nil)
             context:Context_ApplePayNotConfigured contextObject:nil ];
            
        }
        else if (method == PaymentMethod_AmazonPay){
            if (error.code != AMAZON_ERR_PAYMENT_USER_CANCELLED && error.code != AMAZON_ERR_AUTH_USER_CANCELLED){
                [self showAlert:[error localizedDescription]];
            }
        }
        else{
            [self showAlert:[error localizedDescription]];
        }
        return;
    }
    [self sendPurchaseGiftcardRequest:token paymentMethod:method];
}

#pragma 
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context ==Context_ApplePayNotConfigured && index == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=PASSBOOK"]];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma 
#pragma mark - OrderCheckoutDetailViewDelegate
-(void)orderCheckoutDetailView:(OrderCheckoutDetailView *)orderDetailView verifyPromotionCode:(NSString *)code giftcardId:(NSInteger)giftcardId purchaseAmount:(double)purchaseAmount callback:(void (^)(ResponseVerifyPromotionCode *))callback{
    [self sendVerifyPromotionCodeRequest:code withGiftcardId:giftcardId withPurchaseAmount:purchaseAmount callback:callback];
}

-(void)orderCheckoutDetailView:(OrderCheckoutDetailView *)orderDetailView paymentInfoChangedWithDueAmount:(double)amountDue{
    if (amountDue > 0.001){
        self.vAmazonAppleBTN.hidden = NO;
        self.btnCheckout.hidden = YES;
        self.btnChangePaymentMethod.hidden = NO;
    }
    else{
        self.vAmazonAppleBTN.hidden = YES;
        self.btnCheckout.hidden = NO;
        if(amountDue < 0.001)
        self.btnChangePaymentMethod.hidden = YES;
        else{
        self.btnChangePaymentMethod.hidden = NO;
        }
    }
}
#pragma
#pragma mark - Server commnication
-(void)sendPreGift:(PaymentMethod)method context:(int)context{
    RequestPreGift *req = [[RequestPreGift alloc] init];
    req.context = context;
    if (self.orderInfo.recipientIdenIsPhoneNumber){
        req.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    else{
        req.recipientEmail = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    
    req.giftCardId = self.orderInfo.giftcard.giftCardId;
    req.giftCardAmount = self.orderInfo.amount;
    NSArray<ModelPaymentInfo*> * payments =self.vOrderDetailView.payments;
    
    PaymentProcessor paymentProcessor = [[PaymentHandler instance] getPaymentProssor:method];
    
    if (method != PaymentMethod_None){
        ModelPaymentInfo *mpi = [payments objectAtIndex:0];
        switch (method) {
            case PaymentMethod_ApplePay:
                mpi.paymentType = PaymentOption_ApplePay;
                mpi.paymentProcessor = paymentProcessor;
                break;
            case PaymentMethod_PayPal:
                mpi.paymentType = PaymentOption_PayPal;
                mpi.paymentProcessor = paymentProcessor;
                break;
            case PaymentMethod_AmazonPay:
                mpi.paymentType = PaymentOption_AmazonCheckout;
                mpi.paymentProcessor = paymentProcessor;
                break;
            default:
                break;
        }
    }
    if(self.orderInfo.forSwych){
        NSMutableArray *array = [NSMutableArray arrayWithArray:payments];
        [array addObject:self.orderInfo.payments.firstObject];
        req.payments = array;
        req.purchaseType = PurchaseType_SwychGiftCard;
    }
    else{
        req.payments = payments;
        if (self.orderInfo.giftForFriend){
            req.purchaseType = PurchaseType_ForFriend;
        }
        else{
            req.purchaseType = PurchaseType_SelfGifting;
        }
    }
    req.message = self.orderInfo.message;
//    NSData *data = UIImageJPEGRepresentation(self.orderInfo.imagePhoto, 1.0);
//    req.image = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if (self.orderInfo.dateDelivery != nil){
        req.deliverDate = [Utils getTimeString:self.orderInfo.dateDelivery];
    }
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchasePreGiftcardResponse, handPurchasePreGiftcardResponse);
}
-(void)handPurchasePreGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePreGift *resp = (ResponsePreGift*)response;
    if(resp.emailVerificationNeeded){
        [self EmailValidate:^{
            
            switch (_iPreGiftMethod) {
                case 0:
                    if(resp.context == PAY_METHOD_APPPLE_PAY){
                        [self sendPreGift:PaymentMethod_ApplePay context:PAY_METHOD_APPPLE_PAY];
                    }
                    else{
                        [self sendPreGift:PaymentMethod_AmazonPay context:PAY_METHOD_AMAZON];
                    }
                    break;
                case 1:
                    [self sendPreGift:PaymentMethod_PayPal context:PAY_METHOD_PAYPAL];
                    break;
                case 2:
                    
                    [self sendPreGift:PaymentMethod_None context:PAY_METHOD_NONE];
                    break;
                default:
                    break;
            }
        } Email:resp.senderEmail];
        return;
    }
    if(resp.validatePassword){
        self.processorData = resp.paymentProcessorData;
        [self ValidatePassword:(int)resp.context];
        return;
    }
    double amountDue = self.vOrderDetailView.amountDue;
    if(resp.context == PAY_METHOD_APPPLE_PAY){
        
        if (amountDue > 0.0){
        [[PaymentHandler instance] makePayment:PaymentMethod_ApplePay
                                            amount:amountDue
                                       description:PAYMENT_DESCRIPTION_APPLEPAY
                                      withDelegate:self
                                    viewController:self
                                    productImage:nil
                                 contextObject:(NSObject*)resp.paymentProcessorData];
        }
    }
    else if(resp.context == PAY_METHOD_PAYPAL){
        [[PaymentHandler instance] makePayment:PaymentMethod_PayPal
                                        amount:amountDue
                                   description:self.orderInfo.giftcard.retailer
                                  withDelegate:self
                                viewController:self];
    
    }
    else if(resp.context == PAY_METHOD_NONE){
        [self sendPurchaseGiftcardRequest:nil paymentMethod:PaymentMethod_None];
    }
    else if (resp.context == PAY_METHOD_AMAZON){
        [ [PaymentHandler instance] makePayment:PaymentMethod_AmazonPay
                                         amount:self.vOrderDetailView.amountDue
                                    description:self.orderInfo.giftcard.retailer
                         originalPurchaseAmount:self.vOrderDetailView.amount
                                   withDelegate:self
                                 viewController:self];

    }

    
}

-(void)sendVerifyPromotionCodeRequest:(NSString*)promotionCode withGiftcardId:(NSInteger)giftCardId withPurchaseAmount:(double)purchaseAmount callback:(void (^)(ResponseVerifyPromotionCode *))callback{
    RequestVerifyPromotionCode *req = [[RequestVerifyPromotionCode alloc] init];
    req.promotionCode = promotionCode;
    req.giftCardId = giftCardId;
    req.purchaseAmount = purchaseAmount;
    req.contextObject = callback;
    if (self.orderInfo.giftForFriend){
        req.purchaseType =1;
    }
    else{
        req.purchaseType =2;
    }
    
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleVerifyPromotionCodeResponse, handleVerifyPromotionCodeResponse);
    
}

-(void)handleVerifyPromotionCodeResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response]){
        return;
    }
    if (response.success){
        PromotionCodeVerificationCallback cb = (PromotionCodeVerificationCallback)response.contextObject;
        cb((ResponseVerifyPromotionCode*)response);
    }
}

-(void)sendPurchaseGiftcardRequest:(NSString*)paymentToken paymentMethod:(PaymentMethod)method{
    RequestPurchaseGiftcard *req = [[RequestPurchaseGiftcard alloc] init];
    if (self.orderInfo.recipientIdenIsPhoneNumber){
        req.recipientPhoneNumber = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    else{
        req.recipientEmail = self.orderInfo.recipientPhoneNumberOrEmail;
    }
    if(self.botTransaction){
        req.botTransactionId = self.botTransaction.botTransactionId;
    }
    req.giftCardId = self.orderInfo.giftcard.giftCardId;
    req.giftCardAmount = self.orderInfo.amount;
    NSArray<ModelPaymentInfo*> * payments =self.vOrderDetailView.payments;
    
    if (method != PaymentMethod_None){
        ModelPaymentInfo *mpi = [payments objectAtIndex:0];
        
        PaymentProcessor paymentProcessor = [[PaymentHandler instance] getPaymentProssor:method];
        switch (method) {
            case PaymentMethod_ApplePay:
                mpi.paymentType = PaymentOption_ApplePay;
                mpi.paymentProcessor = paymentProcessor;
                break;
            case PaymentMethod_PayPal:
                mpi.paymentType = PaymentOption_PayPal;
                mpi.paymentProcessor = paymentProcessor;
                break;
            case PaymentMethod_DirectCreditCard:
                mpi.paymentType = PaymentOption_DirectCreditCard;
                mpi.paymentProcessor = paymentProcessor;
                break;
            case PaymentMethod_AmazonPay:
            {
                mpi.paymentType = PaymentOption_AmazonCheckout;
                ModelAmazonPaymentToken *token = [[ModelAmazonPaymentToken alloc] init];
                NSError *error = nil;
                [token parseJSONString:paymentToken error:&error dateTimeFormatter:nil];
                mpi.amazonAuthorizationId = token.amazonAuthorizationId;
                mpi.amazonOrderReferenceId = token.amazonOrderReferenceId;
                mpi.amazonToken = token.amazonAccessToken;
                mpi.paymentReferenceNumber = nil;
            }
                break;
            default:
                break;
        }
        if (method == PaymentOption_ApplePay){
            NSError *error = nil;
            ModelApplePayData *apData = [[ModelApplePayData alloc] init];
            [apData parseJSONString:paymentToken error:&error dateTimeFormatter:nil];
            mpi.paymentReferenceNumber = apData.token;
            if(apData.billingAddress != nil){
                mpi.billingInfo = [[ModelSwychBillingInfo alloc] init];
                mpi.billingInfo.firstName = apData.billingAddress.firstName;
                mpi.billingInfo.lastName = apData.billingAddress.lastName;
                mpi.billingInfo.billingAddress.line1 = apData.billingAddress.street1;
                mpi.billingInfo.billingAddress.line2 = apData.billingAddress.street2;
                mpi.billingInfo.billingAddress.city = apData.billingAddress.city;
                mpi.billingInfo.billingAddress.state = apData.billingAddress.state;
                mpi.billingInfo.billingAddress.country = apData.billingAddress.country;
            }
        }
        else{
            mpi.paymentReferenceNumber = paymentToken;
        }
    }
    if(self.orderInfo.forSwych){
        NSMutableArray *array = [NSMutableArray arrayWithArray:payments];
        [array addObject:self.orderInfo.payments.firstObject];
        req.payments = array;
        req.purchaseType = PurchaseType_SwychGiftCard;
    }
    else{
        req.payments = payments;
        if (self.orderInfo.giftForFriend){
            req.purchaseType = PurchaseType_ForFriend;
        }
        else{
            req.purchaseType = PurchaseType_SelfGifting;
        }
    }
    req.message = self.orderInfo.message;
    NSData *data = UIImageJPEGRepresentation(self.orderInfo.imagePhoto, 1.0);
    req.image = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if (self.orderInfo.dateDelivery != nil){
        req.deliverDate = [Utils getTimeString:self.orderInfo.dateDelivery];
    }
    req.context = self.orderInfo.giftForFriend ? Context_Purchase_ForFriend : Context_Purchase_ForMyself;
    
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    
    SEND_REQUEST(req, handPurchaseGiftcardResponse, handPurchaseGiftcardResponse);
    
}

-(void)handPurchaseGiftcardResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    ResponsePurchaseGiftcard *resp = (ResponsePurchaseGiftcard*)response;
    [APP UpdateBanlance:resp.user.swychPoint];
    
    TrackingEventBase *evt = nil;
    
    if(resp.context == Context_Purchase_ForFriend){
        [(AnalyticsShareASale*)[AnalyticsShareASale instance] trackTransaction:resp.transactionId amount:self.orderInfo.amount purchaseForFriend:YES];
        evt = [[TrackingEventGiftCardPurchasedForSendToFriend alloc] init];
        [self showConfirmationPopupView:resp.transactionId earnedPoints:resp.earnedPoints];
    }
    else{
        [(AnalyticsShareASale*)[AnalyticsShareASale instance] trackTransaction:resp.transactionId amount:self.orderInfo.amount purchaseForFriend:NO];
        evt = [[TrackingEventGiftCardPurchasedForSelfGifting alloc] init];
        [self sendGetRedeemedGiftcardsRequest:resp.transactionId];
    }
    [[APP analyticsHandler] sendEvent:evt];
}

-(void)sendGetRedeemedGiftcardsRequest:(NSString*)transactionId{
    [self showLoadingView:nil];
    RequestGetRedeemedGiftcards *req = [[RequestGetRedeemedGiftcards alloc] init];
    req.transactinId = transactionId ;
    
    SEND_REQUEST(req, handleGetRedeemedGiftcardsResponse, handleGetRedeemedGiftcardsResponse)
}

-(void)handleGetRedeemedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response]){
        return;
    }

    ResponseGetRedeemedGiftcards *resp = (ResponseGetRedeemedGiftcards*)response;
    if ([resp.redeemedGiftcards count] > 0){
        ModelTransaction *tran = [resp.redeemedGiftcards objectAtIndex:0];
    
        [self sendGetNearbyLocationRequest:tran];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"NoRedeemedInfoReturned", nil) delegate:nil];
    }
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailPopupView:tran locations:((ResponseGetNearbyLocations*)response).locations];
}

@end
