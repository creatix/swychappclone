//
//  GiftPaymentViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "OrderCheckoutDetailView.h"
#import "PaymentHandler.h"
#import "PopupConfirmationView.h"

@class ModelGiftcardOrderInfo;
@class ModelTransaction;
@class ModelGiftCard;
@class ModelContact;

@interface GiftPaymentViewController : SwychBaseViewController<OrderCheckoutDetailViewDelegate,
    PaymentHandlerDelegate,PopupConfirmationViewDelegate>

@property (nonatomic,strong) ModelGiftcardOrderInfo *orderInfo;
@property (nonatomic,strong) NSArray<ModelTransaction*> *availableCredits;
@property (strong,nonatomic) ModelContact *contact;
@property (nonatomic,strong) NSString *promoCode;
@property (nonatomic,strong) NSMutableArray<ModelPaymentInfo*> * paymentsBackUp;
@end
