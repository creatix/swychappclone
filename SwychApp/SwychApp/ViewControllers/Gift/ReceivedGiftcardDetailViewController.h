//
//  ReceivedGiftcardDetailViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "MessageWithWebViewPopView.h"
@class ModelTransaction;
@interface ReceivedGiftcardDetailViewController : SwychBaseViewController<MessageWithWebViewPopViewDelegate>

@property(nonatomic,strong) ModelTransaction *transaction;
@property(nonatomic,strong) NSArray<ModelGiftCard*> *suggestedGiftcards;
@property(nonatomic,assign) NSUInteger selectedIndex;

@end
