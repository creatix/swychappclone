//
//  GiftDetailViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "CustomizedInputView.h"
#import "CardInfoView.h"
#import "UIGiftcardView.h"
#import "GiftCardMallViewController.h"
#import "ChooseRecipientViewController.h"
@class ModelGiftCard;
@class ModelContact;


@interface GiftDetailViewController : SwychBaseViewController<CustomizedInputViewDelegate,UIGiftcardViewDelegate,CardInfoViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,GiftCardMallViewDelegate,UIGestureRecognizerDelegate,ChooseRecipientViewControllerViewDelegate>


@property (nonatomic,strong) ModelGiftCard *giftcard;

@property (strong,nonatomic) ModelContact *contact;
@property (assign,nonatomic) BOOL giftForFriend;
@property (strong,nonatomic) NSString *defaultMessage;
@end
