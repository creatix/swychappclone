//
//  AmazonOrderConfirmViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"

@class AmazonPayPayment;
@class ModelAmazonPaymentInstrument;

@interface AmazonOrderConfirmViewController : SwychBaseViewController

@property(nonatomic,strong) AmazonPayPayment *amazonPaymentHandler;
@property(nonatomic,strong) ModelAmazonPaymentInstrument *paymentInstrument;

@end
