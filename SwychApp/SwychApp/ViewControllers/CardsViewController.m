//
//  CardsViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CardsViewController.h"
#import "ApplicationConfigurations.h"
#import "LocalUserInfo.h"
#import "GiftCardCarousel.h"
#import "UIGiftcardView.h"
#import "UILabel+Extra.h"
#import "NSArray+Extra.h"
#import "GiftcardMallTableViewCell.h"
#import "ReceivedGiftcardDetailViewController.h"
#import "ModelTransaction.h"
#import "GiftcardSelectionViewController.h"
#import "IssuedGiftcardDetailViewController.h"
#import "UILabelExt.h"
#import "LocationManager.h"
#import "ModelLocation.h"
#import "SocialNetworkIntegration.h"

#import "RequestGetSavedGiftcards.h"
#import "ResponseGetSavedGiftcards.h"
#import "RequestGetIssuedGiftcards.h"
#import "ResponseGetIssuedGiftcards.h"
#import "RequestGetSuggestedGiftcards.h"
#import "ResponseGetSuggestedGiftcards.h"
#import "RequestGetGiftcardsByRetailer.h"
#import "ResponseGetGiftcardsByRetailer.h"
#import "RequestGetNearbyLocations.h"
#import "ResponseGetNearbyLocations.h"



#define ViewName        @"Cards"

#define TEXT_COLOR_SAVED    UICOLORFROMRGB(76,77,77,1.0)
#define TEXT_COLOR_TOTAL    UICOLORFROMRGB(28,130,181,1.0)

#define Context_GiftcardDetail  0x1001

#define TABLE_CELL_Indentifier  @"giftcardMallTableviewDetailCell"
#define ROW_HEIGHT_SAVED_GIFTCARDS_TABLEVIEW    40.0f

@interface CardsViewController ()<UITableViewDataSource,UITableViewDelegate>



@property (weak, nonatomic) IBOutlet UIView *vHeaderSaved;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTotalTop;

@property (weak, nonatomic) IBOutlet UILabelExt *labSaved;
@property (weak, nonatomic) IBOutlet UILabelExt *labTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnList;

@property (weak, nonatomic) IBOutlet UIView *vSaved;
@property (weak, nonatomic) IBOutlet GiftCardCarousel *carouselSaved;
@property (weak, nonatomic) IBOutlet UIGiftcardView *emptySavedGiftcard;
@property (weak, nonatomic) IBOutlet UITableView *tvSavedGiftcards;

@property (weak, nonatomic) IBOutlet UIView *vSeparator;
@property (weak, nonatomic) IBOutlet UIView *vHeaderIssued;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UILabel *labIssued;


@property (weak, nonatomic) IBOutlet GiftCardCarousel *carouselIssued;
@property (weak, nonatomic) IBOutlet UIGiftcardView *emptyIssuedGiftcard;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constIssuedCardouselHeight;
- (IBAction)buttonTapped:(id)sender;
@end

@implementation CardsViewController

@synthesize savedGiftcards = _savedGiftcards;
@synthesize issuedGiftcards = _issuedGiftcards;

-(void)setSavedGiftcards:(NSArray<ModelTransaction *> *)savedGiftcards{
    _savedGiftcards = savedGiftcards;
    if (savedGiftcards.count > 0){
        self.carouselSaved.transactions = savedGiftcards;
        self.carouselSaved.hidden = NO;
        self.tvSavedGiftcards.hidden = YES;
        self.btnList.hidden = NO;
        self.emptySavedGiftcard.hidden = YES;
        self.constraintTotalTop.constant = self.vHeaderSaved.bounds.size.height / 2.0;
        self.labTotal.text = [NSString stringWithFormat:NSLocalizedString(@"Total_Saved", nil),[self.savedGiftcards doubleTotal:@"amount"]];
    }
    else{
        self.carouselSaved.giftCards = nil;
        self.carouselSaved.transactions = nil;
        self.carouselSaved.hidden = YES;
        self.tvSavedGiftcards.hidden = YES;
        self.btnList.hidden = YES;
        self.emptySavedGiftcard.hidden = NO;
        self.constraintTotalTop.constant = self.vHeaderSaved.bounds.size.height;
    }
    [self.tvSavedGiftcards reloadData];
    
}
-(NSArray<ModelTransaction*>*)savedGiftcards{
    return _savedGiftcards;
}

-(void)setIssuedGiftcards:(NSArray<ModelTransaction *> *)issuedGiftcards{
    _issuedGiftcards = issuedGiftcards;
    if (issuedGiftcards.count > 0){
        self.carouselIssued.hidden = NO;
        self.emptyIssuedGiftcard.hidden = YES;
        self.carouselIssued.transactions = issuedGiftcards;
    }
    else{
        self.carouselIssued.hidden = YES;
        self.emptyIssuedGiftcard.hidden = NO;
        self.emptyIssuedGiftcard.giftcardViewDelegate = (id<UIGiftcardViewDelegate>)self;
    }
}
#pragma  mark - UIGiftcardViewDelegate
-(void)giftcardViewTapped:(UIGiftcardView*)giftcardView{
    if(giftcardView.viewType == GiftcardView_Empty_StoreCard){
        [self addPlasticCard];
    }
}
-(NSArray<ModelTransaction*>*)issuedGiftcards{
    return _issuedGiftcards;
}

-(NSString*)getNavTitleViewText{
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (user.swychPoint > 0){
        return [NSString stringWithFormat:NSLocalizedString(@"Points", nil),user.swychPoint];
    }
    else{
        return NSLocalizedString(@"EarnPoints", nil);
    }
}
-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self addNotificationObserverByName:NOTI_Giftcard_Archived];
    [self addNotificationObserverByName:NOTI_Giftcard_Unarchived];
    
    self.emptySavedGiftcard.viewType = GiftcardView_Empty_Discount;
    self.emptyIssuedGiftcard.viewType = GiftcardView_Empty_StoreCard;
    
    self.issuedGiftcards = nil;
    self.savedGiftcards = nil;
    
    self.tvSavedGiftcards.delegate = (id<UITableViewDelegate>)self;
    self.tvSavedGiftcards.dataSource = (id<UITableViewDataSource>)self;
    
    [self.tvSavedGiftcards registerNib:[UINib nibWithNibName:@"GiftcardMallTableViewCell" bundle:nil] forCellReuseIdentifier:TABLE_CELL_Indentifier];
    
    self.constIssuedCardouselHeight.constant = self.carouselSaved.frame.size.height;
    
    [[LocationManager instance] startUpdateLocation];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [self setNavBarInfo];
    [self sendGetSavedGiftcardsRequest];
    [self sendGetIssuedGiftcardsRequest];
    
    [self checkNewGift];
    UIImage *img = [UIImage imageNamed:@"icon_list.png"];
    [self.btnList setButtonImage:img];
    [APP UpdateAffiliateCode];
 }
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.processBot = NO;
}
-(void)setupUI{
    UIColor *clrClear = [UIColor clearColor];
    self.vHeaderSaved.backgroundColor = clrClear;
    self.vHeaderIssued.backgroundColor = clrClear;
    self.vSeparator.backgroundColor = clrClear;
    
    self.labTotal.textColor = TEXT_COLOR_TOTAL;
    self.labSaved.textColor = TEXT_COLOR_SAVED;
    self.labIssued.textColor = TEXT_COLOR_SAVED;
    
    self.labSaved.text = NSLocalizedString(@"LabelSaved", nil);
    self.labIssued.text = NSLocalizedString(@"LabelIssued", nil);
    
    self.labSaved.verticalAlignment = VerticalAlignment_Bottom;
    self.labTotal.verticalAlignment = VerticalAlignment_Top;
    
}

-(BOOL)didReceivedLocalNotification:(NSNotification *)noti{
    [self sendGetIssuedGiftcardsRequest];
    return YES;
}

-(BOOL)hideTabbar{
    return NO;
}

-(BOOL)showTopRightSearchButton{
    return YES;
}
-(NSString*)getViewName{
    return ViewName;
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnList){
        if (self.savedGiftcards == nil && self.savedGiftcards.count == 0){
            return;
        }
        self.carouselSaved.hidden = !self.carouselSaved.hidden;
        self.tvSavedGiftcards.hidden = !self.tvSavedGiftcards.hidden;
        UIImage *img = (self.carouselSaved.hidden == YES ? [UIImage imageNamed:@"icon_thumbnail.png"] : [UIImage imageNamed:@"icon_list.png"]);
        [self.btnList setButtonImage:img];
    }
    else if (sender == self.btnUpload){
        [self addPlasticCard];
     }
}

-(void)showGiftcardDetailPopupView:(ModelTransaction*)transaction locations:(NSArray<ModelLocation*>*)locations{
    IssuedGiftcardDetailViewController *ctrl = (IssuedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"IssuedGiftcardDetailViewController"];
    ctrl.transaction = transaction;
    ctrl.locations = locations;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)showSavedGiftcardDetailView:(ModelTransaction*)transaction suggestedGiftcards:(NSArray<ModelGiftCard*>*)suggestedGiftcards selectedIndex:(NSUInteger)selectedIndex{
    ReceivedGiftcardDetailViewController *ctrl = (ReceivedGiftcardDetailViewController*)[Utils loadViewControllerFromStoryboard:@"Gift" controllerId:@"ReceivedGiftcardDetailViewController"];
    if([transaction.parentTransactionId isEqualToString:@""]){
        transaction.parentTransactionId = transaction.transactionId;
        transaction.giftCard.parentTransactionId = transaction.transactionId;
    }
    ctrl.transaction = transaction;
    ctrl.suggestedGiftcards = suggestedGiftcards;
    ctrl.selectedIndex = selectedIndex;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)addPlasticCard{
    [self showLoadingView:nil];
    [self sendGetAvailableGiftcardByRetailerRequest];
}

-(void)showGiftcardSelectionView:(NSArray<ModelGiftCard*>*)giftcards{
    GiftcardSelectionViewController *ctrl = (GiftcardSelectionViewController*)[Utils loadViewControllerFromStoryboard:@"PlasticCard" controllerId:@"GiftcardSelectionViewController"];
    ctrl.giftcards = giftcards;
    
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)AddPassToWallet:(GiftcardWithBalanceAndWalletButtonView*)giftcardView{
    giftcardView.pkCtl.delegate = self;
    PurchaseForMyselfConfirmationView *v = (PurchaseForMyselfConfirmationView *)giftcardView.superview;
    v.hidden = YES;
    if(giftcardView.pkCtl)
        [self presentViewController:giftcardView.pkCtl
                           animated:YES
                         completion:nil];
    
}

- (void) executeSearch:(id)sender {
    UIAlertController* searchController = [UIAlertController alertControllerWithTitle:@"Search"
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [searchController addTextFieldWithConfigurationHandler:nil];
    [searchController addAction:[UIAlertAction actionWithTitle:@"Apply" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField* textField = searchController.textFields[0];
        NSString* const searchText = textField.text;
        
        if(searchText.length != 0) {
            NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                return [((ModelTransaction*)evaluatedObject).giftCard.retailer localizedCaseInsensitiveContainsString:searchText];
            }];
            
            self.carouselSaved.transactions = [self.savedGiftcards filteredArrayUsingPredicate:predicate];
            self.carouselIssued.transactions = [self.issuedGiftcards filteredArrayUsingPredicate:predicate];
        }
        else {
            self.carouselSaved.transactions = self.savedGiftcards;
            self.carouselIssued.transactions = self.issuedGiftcards;
        }
    }]];
    [searchController addAction:[UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.carouselSaved.transactions = self.savedGiftcards;
        self.carouselIssued.transactions = self.issuedGiftcards;
    }]];
    [self presentViewController:searchController animated:YES completion:nil];
}


#pragma
#pragma mark - Server commnication

-(void)sendGetSavedGiftcardsRequest{
    RequestGetSavedGiftcards *req = [[RequestGetSavedGiftcards alloc] init];
    
    SEND_REQUEST(req, handleGetSavedGiftcardsResponse, handleGetSavedGiftcardsResponse);
}

-(void)handleGetSavedGiftcardsResponse:(ResponseBase*)response{
    if ([self handleServerResponse:response]){
        return;
    }

    if (response.success){
        NSArray<ModelTransaction *> *transactions = ((ResponseGetSavedGiftcards*)response).savedGiftcardsTransaction;
        if (transactions == nil || transactions.count == 0){
            self.savedGiftcards = nil;
        }
        else{
            self.savedGiftcards = transactions;
        }
    }
    else{
        
    }
}

-(void)sendGetIssuedGiftcardsRequest{
    RequestGetIssuedGiftcards *req = [[RequestGetIssuedGiftcards alloc] init];
    
    SEND_REQUEST(req, handleGetIssuedGiftcardsResponse, handleGetIssuedGiftcardsResponse)
}

-(void)handleGetIssuedGiftcardsResponse:(ResponseBase*)response{
    if ([self handleServerResponse:response]){
        return;
    }

    if (response.success){
        self.issuedGiftcards = ((ResponseGetIssuedGiftcards*)response).issuedGiftcardsTransaction;
    }
    else{
        
    }
}

-(void)sendGetSuggestedGiftcardsRequest:(ModelTransaction*)transaction{
    RequestGetSuggestedGiftcards *req = [[RequestGetSuggestedGiftcards alloc] init];
    req.contextObject = transaction;
    req.purchaseType = 3;
    
    SEND_REQUEST(req, handleGetSuggestedGiftcardsResponse, handleGetSuggestedGiftcardsResponse);
}

-(void)handleGetSuggestedGiftcardsResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    ModelGiftCard *card = tran.giftCard;
    NSMutableArray<ModelGiftCard*> *arrayall = [[NSMutableArray alloc] initWithArray:((ResponseGetSuggestedGiftcards*)response).giftCards];
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *cardItem, NSDictionary<ModelGiftCard *,id> * _Nullable bindings) {
        return [cardItem validateAmount:[cardItem amountSwychTo:tran.amount] showAlert:NO];
    }];
    NSArray *array1 = [arrayall filteredArrayUsingPredicate:pred];
    arrayall = [NSMutableArray arrayWithArray:array1];
    [arrayall addObject:[ModelGiftCard emptyGiftcard]];
    
    NSUInteger selectedIndex = -1;
    
    for(int i = 0; i < [arrayall count]; i ++){
        ModelGiftCard *c = [arrayall objectAtIndex:i];
        if (card.giftCardId == c.giftCardId){
            c.isSenderSelected = YES;
            selectedIndex = i;
        }
        else{
            c.isSenderSelected = NO;
        }
    }
    if (selectedIndex == -1){
        selectedIndex = [array1 count] / 2;
        card.isSenderSelected = YES;
        [arrayall insertObject:card atIndex:selectedIndex];
    }
    
    [self showSavedGiftcardDetailView:tran suggestedGiftcards:arrayall selectedIndex:selectedIndex];
}

-(void)sendGetAvailableGiftcardByRetailerRequest{
    RequestGetGiftcardsByRetailer *req = [[RequestGetGiftcardsByRetailer alloc] init];

    SEND_REQUEST(req, handleGetAvailableGiftcardByRetailerResponse, handleGetAvailableGiftcardByRetailerResponse);
}

-(void)handleGetAvailableGiftcardByRetailerResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if ([self handleServerResponse:response] || ![response isKindOfClass:[ResponseGetGiftcardsByRetailer class]]){
        return;
    }
    ResponseGetGiftcardsByRetailer *resp = (ResponseGetGiftcardsByRetailer*)response;
    [self showGiftcardSelectionView:resp.giftCards];
}

-(void)sendGetNearbyLocationRequest:(ModelTransaction*)transaction{
    RequestGetNearbyLocations *req = [[RequestGetNearbyLocations alloc] init];
    req.contextObject = transaction;
    req.merchantName = transaction.giftCard.retailer;
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        req.latitude = loc.coordinate.latitude;
        req.longitude = loc.coordinate.longitude;
    }
    [self showLoadingView:nil];
    
    SEND_REQUEST(req, handleGetNearbyLocationResponse, handleGetNearbyLocationResponse);
}

-(void)handleGetNearbyLocationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![response isKindOfClass:[ResponseGetNearbyLocations class]]){
        return;
    }
    ModelTransaction *tran = (ModelTransaction*)response.contextObject;
    [self showGiftcardDetailPopupView:tran locations:((ResponseGetNearbyLocations*)response).locations];
}

#pragma mark - GiftCardCarouselDelegate -

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
         giftCardSelected:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    if (giftCardCarousel == self.carouselIssued){
        [self sendGetNearbyLocationRequest:transacton];
    }
    else if (giftCardCarousel == self.carouselSaved){
        [self showLoadingView:nil];
        [self sendGetSuggestedGiftcardsRequest:transacton];
    }
}

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
          giftCardFocused:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton {
    
}

#pragma 
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.savedGiftcards == nil ? 0 : self.savedGiftcards.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GiftcardMallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TABLE_CELL_Indentifier];
    if (cell == nil){
        cell = [[GiftcardMallTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TABLE_CELL_Indentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    ModelGiftCard *card = [self.savedGiftcards objectAtIndex:indexPath.row].giftCard;
    cell.giftcard = card;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ModelTransaction *tran = [self.savedGiftcards objectAtIndex:indexPath.row];
    [self showLoadingView:nil];
    [self sendGetSuggestedGiftcardsRequest:tran];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT_SAVED_GIFTCARDS_TABLEVIEW;
}



@end
