//
//  WebViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "SwychBaseViewController.h"

@interface WebViewController : SwychBaseViewController<WKScriptMessageHandler>

@property(nonatomic,strong) NSString *urlString;
@property(nonatomic,strong) NSString *TitleOfView;
@end
