//
//  StripeDirectCreditCardViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"

@protocol StripeDirectCreditCardPaymentDelegate;

@interface StripeDirectCreditCardViewController : SwychBaseViewController<UIWebViewDelegate>

@property(nonatomic,assign) double purchaseAmount;
@property(nonatomic,strong) NSString *purchaseDesc;
@property(nonatomic,strong) NSString *productImageUrl;

@property(nonatomic,strong) NSString *paymentURL;

@property(nonatomic,strong) id<StripeDirectCreditCardPaymentDelegate> delPayment;
@end


@protocol StripeDirectCreditCardPaymentDelegate <NSObject>

@optional
-(void)stripeDirectCreditCardPaymentCancelled:(StripeDirectCreditCardViewController*)viewController;
-(void)stripeDirectCreditCardPaymentSuccess:(StripeDirectCreditCardViewController*)viewController token:(NSString*)token;
@end
