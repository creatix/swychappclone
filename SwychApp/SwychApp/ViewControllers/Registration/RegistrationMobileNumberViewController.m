//
//  RegistrationMobileNumberViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RegistrationMobileNumberViewController.h"
#import "RegistrationProfileViewController.h"
#import "UIRoundCornerButton.h"
#import "PopupViewBase.h"
#import "ViewOTPVerification.h"
#import "DataFomatter.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "NSString+Extra.h"


#import "RequestRegistration.h"
#import "ResponseRegistration.h"
#import "RequestResendOTP.h"
#import "ResponseResendOTP.h"
#import "RequestActivation.h"
#import "RequestVerifyOTP.h"
#import "ResponseVerifyOTP.h"
#import "LocationManager.h"

#import "ResponseActivation.h"
#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"


#import "TrackingEventOTPCompleted.h"

@implementation RegistrationMobileNumberViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [[LocationManager instance] startUpdateLocation];
    [self setupTextInfo];
    
}
-(void) allFunctions{
    [[LocationManager instance] startUpdateLocation];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    if (self.regData != nil && self.regData.phoneNumber != nil){
        if ([self.regData.phoneNumber length] > 10){
            self.txtMobileNumber.text = [self.regData.phoneNumber substringFromIndex:[self.regData.phoneNumber length] - 10];
        }
        else{
            self.txtMobileNumber.text = self.regData.phoneNumber;
        }
    }
    if (self.regData.verifyTOPDirectly){
        [self showVerificationViewWithUser:self.regData];
    }
}
-(void)setupTextInfo{
    self.labWelcometo.text = NSLocalizedString(@"WelcomeTo", nil);
    
    self.labSMS.text = NSLocalizedString(@"Text_ReceiveSMS", nil);
    
    self.txtMobileNumber.placeholder = NSLocalizedString(@"Placeholder_MobileNumber", nil);
    
    [self.btnVerify setTitle:NSLocalizedString(@"Button_CONTINUE", nil)];
    
    self.labTerms.delegate = (id<TTTAttributedLabelDelegate>)self;
    
    NSString *text = NSLocalizedString(@"LabelTerms", nil);
    NSRange linkRange1 = [text rangeOfString:@"privacy policy" options:NSCaseInsensitiveSearch];
    NSRange linkRange2 = [text rangeOfString:@"terms" options:NSCaseInsensitiveSearch];

    [self.labTerms setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange1];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blueColor] range:linkRange2];
        return mutableAttributedString;
    }];
    [self.labTerms addLinkToURL:[NSURL URLWithString:@"privacy"] withRange:linkRange1];
    [self.labTerms addLinkToURL:[NSURL URLWithString:@"terms"] withRange:linkRange2];
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnVerify){
        if ([self.txtMobileNumber.text length] == 0){
            [self showAlert:NSLocalizedString(@"EmptyPhoneNumberOrPassword", nil)];
            return;
        }
        else {
            NSString *phone = [self.txtMobileNumber.text trim];
            if ([phone length] != 10){
                if ([self.txtMobileNumber.text length] == 11){
                    if ([[phone substringToIndex:1] isEqualToString:@"1"]){
                        [self sendRegistrationRequest:[phone substringFromIndex:1]];
                    }
                }
                [self showAlert:NSLocalizedString(@"InvalidPhoneNumber", nil)];
                return;
            }
            else{
                [self sendRegistrationRequest:phone];
            }
        }
    }
}

-(BOOL)alertView:(AlertViewNormal *)alertView buttonTappedWithIndex:(NSInteger)index{
    
    return YES;
}

-(void)showVerificationViewWithUser:(ModelUser*)user{
    PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate = self;
        vVerification.user = user;
        [vVerification setMobilePhoneNumber:[[DataFomatter instance] formattedPhoneNumber:self.txtMobileNumber.text countryCode:@"1"]];
        [Utils showPopupView:(PopupViewBase*)v];
    }
}

-(void)showVerificationView:(ResponseRegistration*)response{
    if (response.user == nil){
        [self showAlert:NSLocalizedString(@"InvalidServerResponse", nil)];
        return;
    }
    [self showVerificationViewWithUser:response.user];
}

-(NSString*)getEnteredPhoneNumber{
    if (self.txtMobileNumber.text == nil || [self.txtMobileNumber.text length] ==0){
        return nil;
    }
    else{
        return self.txtMobileNumber.text;
    }
}

-(void)showUserProfileView:(ModelUser*)user{
    RegistrationProfileViewController *ctrl = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
    ctrl.user = user;
    [self.navigationController pushViewController:ctrl animated:YES];
}


-(void)switchToLoginView:(ModelUser*)user{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app switchToLoginView:NO preview:@"register"];
}

#pragma 
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    WebViewController *ctrl = (WebViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"WebViewController"];
    ctrl.urlString =[NSString stringWithFormat: @"http:goswych.com/%@",url.absoluteString];
    NSString* title = url.absoluteString;
    NSString *firstCapChar = [[title substringToIndex:1] capitalizedString];
    NSString *cappedString = [title stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    ctrl.TitleOfView = cappedString;
    [self.navigationController pushViewController:ctrl animated:YES];
}



#pragma 
#pragma mark - OTPVerificationDelegate
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView  value:(NSString*)OTP{
    [self sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
}

-(void)OTPVerificationResendOTP:(ViewOTPVerification *)verificationView{
    [self sendOTPResendRequest:verificationView.user];
}

#pragma 
#pragma mark Server communication handling
-(BOOL)handleServerResponse:(ResponseBase *)response{
    if (!response.success && response.errorCode == ERR_Registration_UserExist){
        ResponseRegistration *resp = (ResponseRegistration*)response;
        if (resp.user.userStatus == UserStatus_Actived){
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            if (localUser == nil){
                localUser = [[LocalUserInfo alloc] initWithUser:resp.user];
            }
            else{
                [localUser copyFromUser:resp.user];
            }
            [[LocalStorageManager instance] saveLocalUserInfo:localUser];
            [self switchToLoginView:resp.user];
        }
        return YES;
    }
    return [super handleServerResponse:response];
}

-(void)sendRegistrationRequest:(NSString*)phoneNumber{
    [self showLoadingView:nil];
    RequestRegistration *req = [[RequestRegistration alloc] init];
    req.phoneNumber = phoneNumber;
    
    SEND_REQUEST(req, handleRegistrationResponse, handleRegistrationResponse);
}
-(void)handleRegistrationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        ResponseRegistration *resp = (ResponseRegistration*)response;
        switch (response.errorCode) {
            case ERR_Registration_OneStep:
                [self showUserProfileView:resp.user];
                break;
            case ERR_Registration_TwoSteps:
                [self showVerificationView:resp];
            default:
                break;
        }
    }
}

-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    [self showLoadingView:nil];
    RequestVerifyOTP *req = [[RequestVerifyOTP alloc] init];
    req.contextObject = v;
    req.swychId = user.swychId;
    req.otp = [v getOTPCode];
    
    
    SEND_REQUEST(req, handleOTPVerificationResponse, handleOTPVerificationResponse);
}

-(void)handleOTPVerificationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if (![self handleServerResponse:response]){
        ViewOTPVerification *v = (ViewOTPVerification*)response.contextObject;
        ResponseVerifyOTP *resp = (ResponseVerifyOTP*)response;
        [v dismiss];
        
        TrackingEventOTPCompleted *evt = [[TrackingEventOTPCompleted alloc] init];
        [[APP analyticsHandler] sendEvent:evt];
        
        if (resp.user.userStatus == UserStatus_PendingOTPVerified){
            [self showUserProfileView:resp.user];
        }
        else{
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            if (localUser == nil){
                localUser = [[LocalUserInfo alloc] initWithUser:resp.user];
            }
            else{
                [localUser copyFromUser:resp.user];
            }
            [[LocalStorageManager instance] saveLocalUserInfo:localUser];
            [self switchToLoginView:resp.user];
        }
    }
}

-(void)sendOTPResendRequest:(ModelUser*)user{
    [self showLoadingView:nil];
    RequestResendOTP *req = [[RequestResendOTP alloc] init];
    req.swychId = user.swychId;
    req.otpSentType = OTPSENTTYPE_REGISTRATION;
    SEND_REQUEST(req, handleOTPResendResponse, handleOTPResendResponse);
}

-(void)handleOTPResendResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        [self showAlert:response.errorMessage];
    }
}



-(void)sendAuthenticationRequest:(ModelUser*)user{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = user.swychId;
    req.phoneNumber = user.phoneNumber;
    req.password = self.regData.password;
    
    SEND_REQUEST(req, handlerAuthenticationResponse , handlerAuthenticationResponse);
    
}

-(void)handlerAuthenticationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![self handleServerResponse:response]){
        ResponseAuthentication *authResp = (ResponseAuthentication*)response;
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (localUser == nil){
            localUser = [[LocalUserInfo alloc] initWithUser:authResp.user];
        }
        else{
            [localUser copyFromUser:authResp.user];
        }
        localUser.sessionToken = authResp.token;
        localUser.avatarImageURL = authResp.user.avatar;
        localUser.sysConfig = authResp.systemConfigurations;
        if(localUser.avatarImageURL != nil){
            [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
                if (!error){
                    [[LocalStorageManager instance] saveUserAvatarImage:image];
                    [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
                }
            }];
        }
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        
        //Request push token
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        SEL selector = @selector(doneRegistrationSwitchToHomeView);
        NSMethodSignature *sig = [self methodSignatureForSelector:selector];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
        [invocation setSelector:selector];
        [invocation setTarget:self];
        [app requestPushNotificaitonToken:invocation];
    }
}

@end
