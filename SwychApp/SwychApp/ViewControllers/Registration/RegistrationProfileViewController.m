 //
//  RegistrationProfileViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RegistrationProfileViewController.h"
#import "UICheckbox.h"
#import "UIAddPhoto.h"
#import "UIRoundCornerButton.h"
#import "TTTAttributedLabel.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "AppDelegate.h"
#import "UITextFieldExtra.h"
#import "UIRoundImageView.h"
#import "AvatarImageHandler.h"
#import "SocialNetworkIntegration.h"
#import "ModelUser.h"
#import "NSString+Extra.h"
#import "EditAccountViewController.h"


#import "RequestUserUpdate.h"
#import "ResponseUserUpdate.h"
#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"
#import "RequestActivation.h"
#import "ResponseActivation.h"
#import "RequestUploadImageVideo.h"
#import "ResponseUploadImageVideo.h"

#import "TrackingEventRegistrationCompleted.h"

#define ActionSheetButton_ChoosePhoto   0
#define ActionSheetButton_TakePhoto     1

@interface RegistrationProfileViewController()

@property(nonatomic,assign) BOOL avatarImageChanged;
@property(nonatomic,assign) BOOL isRegistration;

-(void)setButtonInfo;
-(void)setTextInfo;
-(LocalUserInfo*)saveUserInformationAtLocal:(BOOL)loginOption;
-(IBAction)buttonTapped:(id)sender;

-(void)sendAuthenticationRequest:(BOOL)withPassword;
-(void)handleAuthenticationResponse:(ResponseBase*)resposne;
@end

@implementation RegistrationProfileViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.ivAvatar.imageViewDelegate = (id<UIRoundCornerImageViewDelegate>)self;
    [self setButtonInfo];
    [self setTextInfo];
    
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localUser.sessionToken != nil){
        self.isRegistration = NO;
    }
    else{
        self.isRegistration = YES;
    }
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"ProfileViewTitle", nil);
}

-(void)setTextInfo{
    self.txtFirstName.placeholder  = NSLocalizedString(@"PlaceHolder_FirstName", nil);
    self.txtLastName.placeholder  = NSLocalizedString(@"PlaceHolder_LastName", nil);
    self.txtEmail.placeholder  = NSLocalizedString(@"PlaceHolder_Email", nil);
    self.txtPassword.placeholder = NSLocalizedString(@"PlaceHolder_Password", nil);
    
    self.vCheckbox.text  = NSLocalizedString(@"EnableTouchID", nil);
    self.vCheckbox.checked = NO;
    self.vCheckbox.hidden = YES;
    self.labAddPhoto.text = NSLocalizedString(@"UploadPhoto", nil);
    
    [self.btnCreateAccount setTitle:NSLocalizedString(@"CreateAccount", nil)];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localUser.sessionToken != nil){
        [self.btnSkip setTitle:NSLocalizedString(@"Button_Cancel", nil)];
    }
    else{
        [self.btnSkip setTitle:NSLocalizedString(@"Skip", nil)];
    }
    self.btnSkip.backgroundColor = MAINCOLOR_GRAY;
    _txtFirstName.textColor = [UIColor darkGrayColor ];
    _txtLastName.textColor = [UIColor darkGrayColor ];
    _txtEmail.textColor = [UIColor darkGrayColor ];
    _txtPassword.textColor = [UIColor darkGrayColor ];

    
}

-(void)setButtonInfo{
    [self.btnCreateAccount setButtonTextColor:[UIColor whiteColor] backgroundColor:UICOLORFROMRGB(192, 31, 109, 1.0) cornerRadius:ROUND_CORNER_BUTTON_RADIUS];
    [self.btnSkip setButtonTextColor:[UIColor whiteColor] backgroundColor:UICOLORFROMRGB(192, 31, 109, 1.0) cornerRadius:ROUND_CORNER_BUTTON_RADIUS];
}


-(IBAction)buttonTapped:(id)sender{
    if (sender == self.btnSkip){
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (localUser.sessionToken != nil){
            [[APP getCurrentViewController].navigationController popViewControllerAnimated:YES];
        }
        else{
            [self skipCreateAccount];
        }
    }
    else if (sender == self.btnCreateAccount){
        [self createAccount];
    }
    else if (sender == self.btnGoogle){
        [[SocialNetworkIntegration instance] doGoogleLoginOnViewController:nil delegate:(id<SocialNetworkIntegrationDeleage>)self];
    }
    else if (sender == self.btnFacebook){
        [[SocialNetworkIntegration instance] doFacebookLoginOnViewController:self];
    }
}

-(void)skipCreateAccount{
    [self saveUserInformationAtLocal:NO];
    [self sendAuthenticationRequest:NO];
}

-(void)createAccount{
    if ([self.txtFirstName.text length] == 0 ||
        [self.txtLastName.text length] == 0 ||
        [self.txtEmail.text length] == 0){
        [self showAlert:NSLocalizedString(@"EmptyNameOrEmail", nil)];
        return;
    }
    [self sendActivationRequest:self.user];
}

-(LocalUserInfo*)saveUserInformationAtLocal:(BOOL)loginOption{
    LocalUserInfo *localUser = [[LocalUserInfo alloc] initWithUser:self.user];
    [[LocalStorageManager instance] saveLocalUserInfo:localUser];
    return localUser;
}

-(void)startPhotoSelection{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Button_Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Button_ChoosePhoto", nil),NSLocalizedString(@"Button_TakePhoto", nil), nil];
    [sheet showInView:self.view];
}


-(void)switchToHomeView{
    if (self.isRegistration){
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        AppDelegate *app  = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [app switchToHomeView:localUser];
    }
    else{
        if (self.isActiveUserInTransaction){
            [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:self.context] forKey:@"Context"]];
        }
        else{
            [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:nil];
            [[APP getCurrentViewController].navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(EditAccountViewController*)createEditAccountView{
    EditAccountViewController *ctrl = (EditAccountViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"EditAccountViewController"];
    ctrl.isActiveUserInTransaction = self.isActiveUserInTransaction;
    ctrl.context = self.context;
    return ctrl;
}

#pragma
#pragma mark - SocialNetworkIntegrationDelegate
-(void)facebookLogin:(BOOL)success facebookToken:(NSString*)token facebookUserId:(NSString*)userId error:(NSError*)error{
    if (success){
        EditAccountViewController *ctrl = [self createEditAccountView];
        ModelUser *u = [self.user duplicate];
        ctrl.user = u;
        ctrl.facebookId = userId;
        ctrl.facebookToken = token;
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

-(void)googleSignIn:(BOOL)success googleToken:(NSString *)token googleUserId:(NSString*)userId userInfo:(ModelUser*)user error:(NSError*)error{
    if (success){
        EditAccountViewController *ctrl = [self createEditAccountView];

        ModelUser * u = [self.user duplicate];
        u.firstName = user.firstName;
        u.lastName = user.lastName;
        u.email = user.email;
        
        ctrl.user = u;
        ctrl.googleId = userId;
        ctrl.googleToken = token;
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}


#pragma 
#pragma mark - AvatarImageHandlerDelegate
-(void)avatarImageHandler:(AvatarImageHandler*)handler didSelectedImage:(UIImage*)selectedImage{
    self.ivAvatar.image = selectedImage;
    self.avatarImageChanged = YES;
}

-(void)avatarImageHdnalerDidCancelled:(AvatarImageHandler*)handler{
    
}

#pragma 
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[AvatarImageHandler instance] handleAvatarImage:chosenImage viewController:self delegate:(id<AvatarImageHandlerDelegate>)self];
}

#pragma 
#pragma mark -UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == ActionSheetButton_ChoosePhoto){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (buttonIndex == ActionSheetButton_TakePhoto){
#if TARGET_IPHONE_SIMULATOR
        return;
#endif
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

#pragma 
#pragma mark - UIRoundCornerImageViewDelegate
-(void)roundCornerImageViewTapped:(UIRoundCornerImageView*)imageView{
    [self startPhotoSelection];
}

#pragma
#pragma mark Server communication handling
-(void)sendAuthenticationRequest:(BOOL)withPassword{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = self.user.swychId;
    req.phoneNumber = self.user.phoneNumber;
    if (withPassword){
        req.password = [self.txtPassword.text trim];
    }
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}

-(void)handleAuthenticationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![self handleServerResponse:response]){
        ResponseAuthentication *authResp = (ResponseAuthentication*)response;
        LocalUserInfo *localUser = [[LocalUserInfo alloc] initWithUser:authResp.user];
        localUser.sessionToken = authResp.token;
        localUser.avatarImageURL = authResp.user.avatar;
        localUser.sysConfig = authResp.systemConfigurations;
        if(localUser.avatarImageURL != nil){
            [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
                if (!error){
                    [[LocalStorageManager instance] saveUserAvatarImage:image];
                    [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
                }
            }];
        }
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        
        if (self.avatarImageChanged){
            NSString *imgData = nil;
            NSData *data = UIImageJPEGRepresentation(self.ivAvatar.image, 1.0);
            imgData = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [self sendUploadImageRequest:imgData];
        }
        else{
            [self switchToHomeView];
        }
    }
}

-(void)sendActivationRequest:(ModelUser*)user{
    [self showLoadingView:nil];
    RequestActivation *req = [[RequestActivation alloc] init];
    req.swychId = user.swychId;
    req.firstName = [self.txtFirstName.text trim];
    req.lastName = [self.txtLastName.text trim];
    req.email = [self.txtEmail.text trim];
    req.password = [self.txtPassword.text trim];
    
    SEND_REQUEST(req, handleActivationResponse, handleActivationResponse);
}

-(void)handleActivationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        ResponseActivation *activationResp = (ResponseActivation*)response;
        self.user = activationResp.user;
        [self saveUserInformationAtLocal:activationResp.user];
        [self sendAuthenticationRequest:YES];
        
        TrackingEventRegistrationCompleted *evt = [[TrackingEventRegistrationCompleted alloc] init];
        [[APP analyticsHandler] sendEvent:evt];
    }
}

-(void)sendUploadImageRequest:(NSString*)imageData{
    [self showLoadingView:nil];
    RequestUploadImageVideo *req = [[RequestUploadImageVideo alloc] init];
    req.imageData = imageData;
    
    
    SEND_REQUEST(req, handleUploadImageResponse, handleUploadImageResponse);

}

-(void)handleUploadImageResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(response.success){
        ResponseUploadImageVideo *resp = (ResponseUploadImageVideo*)response;
        
    }
    [self switchToHomeView];
}
@end
