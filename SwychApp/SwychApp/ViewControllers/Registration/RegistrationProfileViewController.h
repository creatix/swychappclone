//
//  RegistrationProfileViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"

@class UIAddPhoto;
@class TTTAttributedLabel;
@class UICheckbox;
@class UIRoundCornerButton;
@class ModelUser;
@class UIRoundImageView;
@class UITextFieldExtra;

@interface RegistrationProfileViewController : SwychBaseViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *vTopView;
@property (weak, nonatomic) IBOutlet UIView *vBottomView;

@property (weak, nonatomic) IBOutlet UIView *vUserInfo;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCreateAccount;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSkip;

@property (weak,nonatomic) IBOutlet UIRoundImageView *ivAvatar;
@property (weak,nonatomic) IBOutlet UILabel *labAddPhoto;

@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtLastName;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtEmail;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtPassword;

@property (weak, nonatomic) IBOutlet UICheckbox *vCheckbox;

@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;

@property (strong, nonatomic) ModelUser *user;

@property (assign,nonatomic) BOOL isActiveUserInTransaction;
@property (assign,nonatomic) NSInteger context;
@end
