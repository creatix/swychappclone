//
//  EditAccountViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "EditAccountViewController.h"
#import "ModelUser.h"
#import "UIRoundImageView.h"
#import "AvatarImageHandler.h"
#import "LocalUserInfo.h"
#import "AppDelegate.h"


#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"
#import "RequestActivation.h"
#import "ResponseActivation.h"
#import "RequestUploadImageVideo.h"
#import "ResponseUploadImageVideo.h"

#import "TrackingEventRegistrationCompleted.h"

#define ActionSheetButton_ChoosePhoto   0
#define ActionSheetButton_TakePhoto     1

@interface EditAccountViewController ()
@property(nonatomic,weak) IBOutlet UserInfoUpdateView *userInfoView;
@property(nonatomic,assign) BOOL avatarImageChanged;
@property(nonatomic,assign) BOOL isRegistration;

@end

@implementation EditAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.userInfoView.delegate = self;
    self.userInfoView.user = self.user;
    self.userInfoView.buttonText = NSLocalizedString(@"Button_CreateAccount", nil);
    self.ivAvatar.imageViewDelegate = (id<UIRoundCornerImageViewDelegate>)self;
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localUser.sessionToken != nil){
        self.isRegistration = NO;
    }
    else{
        self.isRegistration = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"EditAccountViewTitle", nil);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)startPhotoSelection{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Button_Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Button_ChoosePhoto", nil),NSLocalizedString(@"Button_TakePhoto", nil), nil];
    [sheet showInView:self.view];
}

-(void)doFacebookRegistration{
    [self sendActivationRequest];
}

-(void)doGoogleRegistration{
    [self sendActivationRequest];
}


#pragma 
#pragma mark - UserInfoUpdateViewDelegate
-(void)userInfoUpdateViewSaveButtonTapped:(UserInfoUpdateView *)view withUserInfo:(ModelUser *)user{
    self.user.firstName = user.firstName;
    self.user.lastName = user.lastName;
    self.user.email = user.email;
    
    if (self.facebookToken != nil){
        [self doFacebookRegistration];
    }
    else if (self.googleToken != nil){
        [self doGoogleRegistration];
    }
}

#pragma
#pragma mark - AvatarImageHandlerDelegate
-(void)avatarImageHandler:(AvatarImageHandler*)handler didSelectedImage:(UIImage*)selectedImage{
    self.ivAvatar.image = selectedImage;
    self.avatarImageChanged = YES;
}

-(void)avatarImageHdnalerDidCancelled:(AvatarImageHandler*)handler{
    
}

#pragma
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[AvatarImageHandler instance] handleAvatarImage:chosenImage viewController:self delegate:(id<AvatarImageHandlerDelegate>)self];
}

#pragma
#pragma mark -UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == ActionSheetButton_ChoosePhoto){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (buttonIndex == ActionSheetButton_TakePhoto){
#if TARGET_IPHONE_SIMULATOR
        return;
#endif
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self PopoverFromIpad:picker];
        }
        else{
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

#pragma
#pragma mark - UIRoundCornerImageViewDelegate
-(void)roundCornerImageViewTapped:(UIRoundCornerImageView*)imageView{
    [self startPhotoSelection];
}

#pragma
#pragma mark Server communication handling
-(void)sendAuthenticationRequest{
    [self showLoadingView:nil];
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = self.user.swychId;
    req.phoneNumber = self.user.phoneNumber;
    
    if (self.facebookToken != nil){
        req.facebookId = self.facebookId;
        req.fbToken = self.facebookToken;
    }
    else if (self.googleToken != nil){
        req.googleId = self.googleId;
        req.email = self.user.email;
        req.googleToken = self.googleToken;
    }
    
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);
    
}

-(void)handleAuthenticationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if (![self handleServerResponse:response]){
        ResponseAuthentication *authResp = (ResponseAuthentication*)response;
        LocalUserInfo *localUser = [[LocalUserInfo alloc] initWithUser:authResp.user];

        localUser.sessionToken = authResp.token;
        localUser.avatarImageURL = authResp.user.avatar;
        localUser.sysConfig = authResp.systemConfigurations;
        if(localUser.avatarImageURL != nil){
            [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
                if (!error){
                    [[LocalStorageManager instance] saveUserAvatarImage:image];
                    [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
                }
            }];
        }
        
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        if (self.avatarImageChanged){
            NSString *imgData = nil;
            NSData *data = UIImageJPEGRepresentation(self.ivAvatar.image, 1.0);
            imgData = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [self sendUploadImageRequest:imgData];
        }
        else{
            if (self.isRegistration){
                AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                [app switchToHomeView:localUser];
            }
            else{
                if (self.isActiveUserInTransaction){
                    [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:self.context] forKey:@"Context"]];
                }
                else{
                    [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:nil];
                    [[APP getCurrentViewController].navigationController popToRootViewControllerAnimated:YES];
                }
            }
        }
        
    }
}

-(void)sendActivationRequest{
    [self showLoadingView:nil];
    RequestActivation *req = [[RequestActivation alloc] init];
    req.swychId = self.user.swychId;
    req.firstName = self.user.firstName;
    req.lastName = self.user.lastName;
    req.email = self.user.email;
    
    if (self.facebookToken != nil){
        req.facebookId = self.facebookId;
        req.fbToken = self.facebookToken;
    }
    else if (self.googleToken != nil){
        req.googleId = self.googleId;
        req.email = self.user.email;
        req.googleToken = self.googleToken;
    }
    
    SEND_REQUEST(req, handleActivationResponse, handleActivationResponse);
}

-(void)handleActivationResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(![self handleServerResponse:response]){
        ResponseActivation *activationResp = (ResponseActivation*)response;
        self.user = activationResp.user;
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (localUser == nil){
            localUser = [[LocalUserInfo alloc] initWithUser:activationResp.user];
        }
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        
        [self sendAuthenticationRequest];
        TrackingEventRegistrationCompleted *evt = [[TrackingEventRegistrationCompleted alloc] init];
        [[APP analyticsHandler] sendEvent:evt];
    }
}

-(void)sendUploadImageRequest:(NSString*)imageData{
    [self showLoadingView:nil];
    RequestUploadImageVideo *req = [[RequestUploadImageVideo alloc] init];
    req.imageData = imageData;
    
    
    SEND_REQUEST(req, handleUploadImageResponse, handleUploadImageResponse);
    
}

-(void)handleUploadImageResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    if(response.success){
        ResponseUploadImageVideo *resp = (ResponseUploadImageVideo*)response;
        
    }
    if (self.isRegistration){
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        [app switchToHomeView:localUser];
    }
    else{
        if (self.isActiveUserInTransaction){
            [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:self.context] forKey:@"Context"]];
        }
        else{
            [Utils postLocalNotification:NOTI_UserStatusActivated object:nil userInfo:nil];
            [[APP getCurrentViewController].navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

@end
