//
//  EditAccountViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "UserInfoUpdateView.h"


@class ModelUser;
@class UIRoundImageView;

@interface EditAccountViewController : SwychBaseViewController<UserInfoUpdateViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) ModelUser *user;
@property(nonatomic,strong) NSString *facebookId;
@property(nonatomic,strong) NSString *facebookToken;

@property(nonatomic,strong) NSString *googleId;
@property(nonatomic,strong) NSString *googleToken;

@property (weak, nonatomic) IBOutlet UILabel *labChangePhoto;
@property (weak, nonatomic) IBOutlet UIRoundImageView *ivAvatar;

@property (assign,nonatomic) BOOL isActiveUserInTransaction;
@property (assign,nonatomic) NSInteger context;
@end
