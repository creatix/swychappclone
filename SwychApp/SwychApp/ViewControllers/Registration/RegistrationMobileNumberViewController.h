//
//  RegistrationMobileNumberViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "ViewOTPVerification.h"
#import "RegistrationData.h"
#import "TTTAttributedLabel.h"

@class UIRoundCornerButton;
@class UIButtonLeftImage;
@class ModelUser;

@interface RegistrationMobileNumberViewController : SwychBaseViewController<OTPVerificationDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labWelcometo;
@property (weak, nonatomic) IBOutlet UIImageView *ivSwych;
@property (weak, nonatomic) IBOutlet UILabel *labCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *labSMS;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnVerify;
@property (strong,nonatomic) RegistrationData *regData;

@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIImageView *ivPhone;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labTerms;


- (IBAction)buttonTapped:(id)sender;

@end
