//
//  StripeDirectCreditCardViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "StripeDirectCreditCardViewController.h"
#import "UILabelExt.h"


#define JS_PREFIX_FUNC                      @"js-frame:"
#define JS_METHOD_DisableCancelButton       @"ios_disable_cancel"
#define JS_METHOD_PaymentCancelled          @"ios_payment_cancelled"
#define JS_METHOD_PaymentSuccess            @"ios_payment_success"


@interface StripeDirectCreditCardViewController()

@property (weak, nonatomic) IBOutlet UIWebView *wvWebView;
@property(nonatomic,strong) UIButton *btnCancel;

@end

@implementation StripeDirectCreditCardViewController
@synthesize purchaseDesc,purchaseAmount,paymentURL,productImageUrl,delPayment;


-(void)viewDidLoad{
    [super viewDidLoad];
    NSString *fullURL = [[NSString stringWithFormat:paymentURL,self.purchaseAmount * 100.0,self.purchaseDesc,self.productImageUrl == nil ? @"" : self.productImageUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",fullURL);
    self.navigationController.navigationBarHidden = NO;
    [self.wvWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:fullURL]]];
    
}

-(NSString*)getNavTitleViewText{
    return NSLocalizedString(@"PaymentOption_DirectCreditCard", nil);
}

-(NSString *)getBackButtonText{
    return NSLocalizedString(@"Button_Cancel_low", nil);
}

-(UIColor*)getBackButtonTextColor{
    return UICOLORFROMRGB(28,102,174,1.0);
}

-(void)setNavBarInfo{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    NSString *backButtonText = [self getBackButtonText];
    if (backButtonText != nil){
        UIImage *imgBack = nil; [UIImage imageNamed:@"navbar_back.png"];
        self.btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50.0f, 40.0f)];
        [self.btnCancel addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnCancel setTitle:backButtonText forState:UIControlStateNormal];
        [self.btnCancel setTitle:backButtonText forState:UIControlStateHighlighted];
        [self.btnCancel setBackgroundImage:imgBack forState:UIControlStateNormal];
        [self.btnCancel setBackgroundImage:imgBack forState:UIControlStateHighlighted];
        self.btnCancel.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        
        UIColor *clr = [self getBackButtonTextColor];
        if (clr != nil){
            [self.btnCancel setTitleColor:clr forState:UIControlStateNormal];
            [self.btnCancel setTitleColor:clr forState:UIControlStateHighlighted];
        }
        self.bbiLeft = [[UIBarButtonItem alloc] initWithCustomView:self.btnCancel];
        
        self.navigationItem.leftBarButtonItem  = self.bbiLeft;
        self.navigationController.navigationBarHidden = NO;
        
        NSString *title = [self getNavTitleViewText];
        if (title != nil){
            UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imgBack.size.width, imgBack.size.height)];
            v.backgroundColor = [UIColor clearColor];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:v];
            UILabelExt *labTitle = [[UILabelExt alloc] initWithFrame:CGRectMake(0, 20, 50, 44)];
            labTitle.font = [UIFont fontWithName:@"Helvetica Nene" size:16.0f];
            labTitle.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
            labTitle.text = title;
            labTitle.verticalAlignment = VerticalAlignment_Middle;
            self.navigationItem.titleView = labTitle;
        }
    }
    
    
}

-(void)buttonTapped:(id)sender{
    if (sender == self.btnCancel){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)disableCancelButton{
    self.btnCancel.hidden = YES;
}

-(void)userCancelledPayment{
    [self dismissViewControllerAnimated:YES completion:nil];
    if(self.delPayment != nil && [self.delPayment respondsToSelector:@selector(stripeDirectCreditCardPaymentCancelled:)]){
        [self.delPayment stripeDirectCreditCardPaymentCancelled:self];
    }
}

-(void)paymentSuccess:(NSString*)paymentToken{
    if(self.delPayment != nil && [self.delPayment respondsToSelector:@selector(stripeDirectCreditCardPaymentSuccess:token:)]){
        [self.delPayment stripeDirectCreditCardPaymentSuccess:self token:paymentToken];
    }
}


#pragma 
#pragma mark -UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *absURLString = [[request URL] absoluteString];
    if ([absURLString hasPrefix:JS_PREFIX_FUNC]) {
        
        // Extract the selector name from the URL
        NSArray *components = [absURLString componentsSeparatedByString:@":"];
        if([components count] > 2){
            NSString *method = [components objectAtIndex:1];
            if ([method isEqualToString:JS_METHOD_DisableCancelButton]){
                [self disableCancelButton];
                return NO;
            }
            else if([method isEqualToString:JS_METHOD_PaymentCancelled]){
                [self userCancelledPayment];
                return NO;
            }
            else if([method isEqualToString:JS_METHOD_PaymentSuccess]){
                //Get payment token
                NSString *paymentToken = [components objectAtIndex:3];
                
                [self paymentSuccess:paymentToken];
                return NO;
            }
            return YES;
        }
        else{
            return YES;
        }
    }
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"WebView Error - %@",[error localizedDescription]);
}
@end
