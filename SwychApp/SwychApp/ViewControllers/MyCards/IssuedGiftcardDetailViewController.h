//
//  IssuedGiftcardDetailViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#define ARCHIVED 123
@class ModelTransaction;
@class ModelLocation;
#import "PurchaseForMyselfConfirmationView.h"
@interface IssuedGiftcardDetailViewController : SwychBaseViewController<PurchaseForMyselfConfirmatinViewDelegete>

@property(nonatomic,assign) BOOL backToRootViewController;

@property(nonatomic,strong) ModelTransaction *transaction;
@property(nonatomic,strong) NSArray<ModelLocation*> *locations;
@property (nonatomic) int ArchivedStatus;
@end
