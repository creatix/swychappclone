//
//  IssuedGiftcardDetailViewController.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "IssuedGiftcardDetailViewController.h"
#import "UIRoundCornerButton.h"
#import "ModelTransaction.h"
#import "ModelGiftCard.h"
#import "Utils.h"
#import "WebViewController.h"
#import "Utils.h"
@interface IssuedGiftcardDetailViewController ()

@property (weak, nonatomic) IBOutlet PurchaseForMyselfConfirmationView *purchaseConfirmationView;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnDone;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation IssuedGiftcardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.purchaseConfirmationView.confirmationViewDelegate = self;
    self.purchaseConfirmationView.transaction = self.transaction;
    self.purchaseConfirmationView.locations = self.locations;
    
    self.purchaseConfirmationView.isPopupMode = NO;
    [self.purchaseConfirmationView updateView];
    
    [Utils addNotificationObserver:self selector:@selector(giftCardArchived:) name:NOTI_Giftcard_Archived object:nil];
    [Utils addNotificationObserver:self selector:@selector(ungiftCardArchived:) name:NOTI_Giftcard_Unarchived object:nil];

   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.ArchivedStatus == ARCHIVED){
        _purchaseConfirmationView.isArchived = YES;
        [self AddTitle: NSLocalizedString(@"ArchivedTitle",nil)];
    }
    else{
        _purchaseConfirmationView.isArchived = NO;
    }
    [_purchaseConfirmationView UpdateViewStatus];
    //[self.purchaseConfirmationView.cvGiftcardView updateSubviews];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)giftCardArchived:(NSNotification*)noti{
    if (noti != nil){
        if (noti.userInfo != nil){
            id obj = [noti.userInfo objectForKey:@"Transaction"];
            if (obj != nil && [obj isKindOfClass:[ModelTransaction class]]){
                ModelTransaction *tran = (ModelTransaction*)obj;
                if (tran.transactionId == self.transaction.transactionId){
                 //   [self.navigationController popViewControllerAnimated:YES];
                    [self UpdateToUnArchivedView];
                }
            }
        }
    }
}
-(void)UpdateToUnArchivedView{
[self AddTitle: NSLocalizedString(@"ArchivedTitle",nil)];
     _purchaseConfirmationView.isArchived = YES;
    [_purchaseConfirmationView UpdateViewStatus];
}
-(void)ungiftCardArchived:(NSNotification*)noti{
    if (noti != nil){
        if (noti.userInfo != nil){
            id obj = [noti.userInfo objectForKey:@"Transaction"];
            if (obj != nil && [obj isKindOfClass:[ModelTransaction class]]){
                ModelTransaction *tran = (ModelTransaction*)obj;
                if (tran.transactionId == self.transaction.transactionId){
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnDone){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)navbarBackButtonTapped{
    if (self.backToRootViewController){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [super navbarBackButtonTapped];
    }
}
//
#pragma  PurchaseForMyselfConfirmatinViewDelegete

-(void)OpenTerms:(NSString*) termsUrl{
            WebViewController *ctrl = (WebViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"WebViewController"];
            ctrl.urlString =termsUrl;
            NSString* title = NSLocalizedString(@"TermsConditions",nil);
            NSString *firstCapChar = [[title substringToIndex:1] capitalizedString];
            NSString *cappedString = [title stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
            ctrl.TitleOfView = cappedString;
            [self.navigationController pushViewController:ctrl animated:YES];
}


@end
