//
//  CardsViewController.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwychBaseViewController.h"
#import "PurchaseForMyselfConfirmationView.h"
#import "GiftcardWithBalanceAndWalletButtonView.h"
@class ModelGiftCard;
@class ModelTransaction;

@interface CardsViewController : SwychBaseViewController<PopupConfirmationViewDelegate, PKAddPassesViewControllerDelegate>

@property(nonatomic,strong) NSArray<ModelTransaction*> *savedGiftcards;
@property(nonatomic,strong) NSArray<ModelTransaction*> *issuedGiftcards;

@end

