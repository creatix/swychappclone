//
//  TransactionDetailViewController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-06.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SwychBaseViewController.h"
#import "TransactionDetailCardInfoView.h"
#import "TransactionDetailCommentView.h"
@interface TransactionDetailViewController : SwychBaseViewController<HistoryTransactionDetailCommentViewDelegate,AlertViewDelegate>
@property (weak,nonatomic) IBOutlet TransactionDetailCardInfoView *vCardInfo;
@property (strong,nonatomic) IBOutlet TransactionDetailCommentView *vCardComment;
@property (strong,nonatomic) IBOutlet UILabel *lblDate;
@property (strong,nonatomic) IBOutlet UILabel *lblTime;
@property (strong,nonatomic)  ModelTransaction *transaction;

+(int)getiIndex;
+(void)setiIndex:(int)index;
@end
