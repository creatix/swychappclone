//
//  TransactionDetailViewController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-06.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionDetailViewController.h"
#import "TransactionDetailCommentSwychView.h"
#import "RequestRemind.h"
#import "RequestTakeBack.h"
#import "ResponseTakeBack.h"
#import "ResponseRemind.h"
#import "UIImageView+WebCache.h"
#import "ResponseBase.h"
#import "ResponseSayThanks.h"
#import "RequestSayThanks.h"
#import "Utils.h"
#define Context_Prompt_TakeBack   0x02
@interface TransactionDetailViewController ()

@end
static int iIndex = -1;

@implementation TransactionDetailViewController
+(int)getiIndex{
    return iIndex;
}
+(void)setiIndex:(int)index{
    iIndex = index;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _vCardComment.selectionViewDelegate = self;
    self.vCardInfo.transaction = self.transaction;
    [self.vCardComment setTransaction: self.transaction];
   
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
   [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    formatter.dateFormat = @"MMM dd, yyyy";
    NSString* sdate = [formatter stringFromDate:_transaction.transactionDate];
    self.lblDate.text = sdate;
    [formatter setDateFormat:@"hh:mm a"];
    self.lblTime.text = [formatter stringFromDate:self.transaction.transactionDate];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.processBot = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    
    if (alertView.context == Context_Prompt_TakeBack && index == 2){
        [alertView dismiss];
        [self sendTakeBackRequest];
    }
    return YES;
}
#pragma HistoryTransactionDetailCommentViewDelegate
-(void)TakeBack{
    [Utils showAlert:NSLocalizedString(@"PromptTakeBackSentCard", nil)
               title:nil
            delegate:self
         firstButton:NSLocalizedString(@"Button_NO", nil)
        secondButton:NSLocalizedString(@"Button_YES", nil)
         withContext:Context_Prompt_TakeBack
   withContextObject:nil checkboxText:nil];
}
-(void)Remind{
    [self sendRemindRequest];
}
-(void)SayThanks:(NSString*)text{
    RequestSayThanks *req = [[RequestSayThanks alloc] init];
    req.transactionId = _transaction.transactionId;
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    SEND_REQUEST(req, handSaythanksRequestResponse, handSaythanksRequestResponse);
}
#pragma mark - Server communication handling
-(void)sendRemindRequest{
    RequestRemind *req = [[RequestRemind alloc] init];
    req.transactionId = _transaction.transactionId;
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    SEND_REQUEST(req, handRemindRequestResponse, handRemindRequestResponse);
}
-(void)handRemindRequestResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    [self showAlert:response.errorMessage];

}
-(void)handSaythanksRequestResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    [self showAlert:response.errorMessage];
    
}
-(void)sendTakeBackRequest{
    RequestTakeBack *req = [[RequestTakeBack alloc] init];
    req.transactionId = _transaction.transactionId;
    [self showLoadingView:NSLocalizedString(@"Progress_Message_Sending_Request", nil)];
    SEND_REQUEST(req, handTakeBackResponse, handTakeBackResponse);
}
-(void)handTakeBackResponse:(ResponseBase*)response{
    [self dismissLoadingView];
    
    if ([self handleServerResponse:response]){
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
