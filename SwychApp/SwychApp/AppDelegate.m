//
//  AppDelegate.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "TabbarItem.h"
#import "Analytics.h"
#import "NSObject+Extra.h"
#import "KeyboardHandler.h"
#import "PopupViewHandler.h"
#import "LocalUserInfo.h"
#import "LoginViewController.h"
#import "ModelAppleNotification.h"
#import "NSObject+Extra.h"
#import "SocialNetworkIntegration.h"
#import "RegistrationMobileNumberViewController.h"
#import "HamburgerMenuView.h"
#import "ModelUser.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/SignIn.h>
#import <Google/Analytics.h>

#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"
#import "RequestUpdatePushToken.h"
#import "ResponseUpdatePushToken.h"
#import "CreatixContactsManager.h"
#import "ModelSystemConfiguration.h"
#import "ResponseUpdateAffiliate.h"
#import "RequestUpdateAffiliate.h"

#import "TrackingEventAppInstalled.h"
#import "AnalyticsShareASale.h"

#import <LoginWithAmazon/LoginWithAmazon.h>

#define  Context_StartPurchaseGiftCard  0x01
#define  Context_ReceivedGiftCard       0x02
#define  Context_ReceivedBotGift       0x03
#define SWYCH_AFFILIATE             @"swychaffiliate"
#define SWYCH_BOTLINK               @"botlink"
#define SCHEME_SWYCH_BOTGIFT        @"swychapp"

@interface AppDelegate ()
@property(nonatomic,strong) NSInvocation *invocationPushToken;
@property(nonatomic,strong) UITabBarController *tabbarController;
@property(nonatomic,strong) HamburgerMenuView *hamburgerMenuView;
@property(nonatomic,readonly) BOOL isHamburgerMenuShowing;

-(void)doRouting;
-(void)sendAuthenticationRequest:(LocalUserInfo*)localUser;
-(BOOL)isHamburgerMenuShowing;
@end

@implementation AppDelegate

@synthesize invocationPushToken = _invocationPushToken;
static NSString* s_affiliateCode;
-(BOOL)isHamburgerMenuShowing{
    return self.hamburgerMenuView == nil ? NO : self.hamburgerMenuView.frame.origin.x >= 0;
}

//  !!! Required method of UIApplicationDelegate !!!
- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //  Please don't forget those lines to load external Bundle!
    for (NSURL *url in [[NSBundle bundleWithIdentifier:@"org.cocoapods.SwychContacts"] URLsForResourcesWithExtension:@"bundle" subdirectory:nil]) {
        [[NSBundle bundleWithURL:url] load];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [CreatixContactsManager shareInstance].isFirstTimeSync = true;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = [SocialNetworkIntegration instance];
    [GIDSignIn sharedInstance].uiDelegate = [SocialNetworkIntegration instance];
    
    
    UIColor *barTintColor = UICOLORFROMRGB(228, 225, 225, 1.0);
    UIColor *tintColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    [[UITabBar appearance] setTintColor:tintColor];
    [[UITabBar appearance] setBarTintColor:barTintColor];
    
    [self setApplicationBadgeNumber:0];
    
    [self doRouting];
    
    self.analyticsHandler = [AnalyticsHandler getInstance];
    [self.analyticsHandler initialize];

    BOOL appInstalledFlag = [[LocalStorageManager instance] getAppInstalledFlag];
    
    if(appInstalledFlag != YES){
        [[LocalStorageManager instance] setAppInstalledFlag];
        [self.analyticsHandler sendEvent:[[TrackingEventAppInstalled alloc] init]];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    _bakcgroudFlag = 100;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Utils postLocalNotification:NOTI_AppWillEnterForeground object:nil userInfo:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    [FBSDKAppEvents logEvent:@"Application Active"];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if (url != nil){
        NSString *path = [url path];
        NSString *query = [url query];
        NSString *scheme = [url scheme];
        if (scheme != nil && [scheme caseInsensitiveCompare:SCHEME_SWYCH_BOTGIFT] == NSOrderedSame){
            //TODO: Show prompt, let user choose whether start the gift purcahse.
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            if(localUser&&localUser.sessionToken){
                SwychBaseViewController *ctrl = [self getCurrentViewController];
                if (ctrl != nil){
                    ctrl.processBot = NO;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:BotNewTransactionNotification object:nil];
            }
            return YES;
        }
        if (path != nil && query != nil){
            if ([path caseInsensitiveCompare:SWYCH_AFFILIATE] == NSOrderedSame){
                [self updateAffiliateCode:query];
                return YES;
            }
            if ([path caseInsensitiveCompare:SWYCH_BOTLINK] == NSOrderedSame){
                LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
                if(localUser&&localUser.sessionToken){
                [[NSNotificationCenter defaultCenter] postNotificationName:BotNewLinkNotification object:nil];
                }

                return YES;
            }
        }
    }
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    if (handled){
        return  handled;
    }
    
    handled = [[GIDSignIn sharedInstance] handleURL:url
                                      sourceApplication:sourceApplication
                                             annotation:annotation];
    if (handled){
        return handled;
    }
    
    handled = [AMZNAuthorizationManager handleOpenURL:url sourceApplication:sourceApplication];
    
    return handled;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSLog(@"Remote noti token - %@", deviceToken);
    NSString *token  = [deviceToken description];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [self sendUpdatePushTokenRequest:token];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Error in registration. Error: %@", error);
    if (_invocationPushToken != nil){
        [_invocationPushToken invoke];
    }
    _invocationPushToken = nil;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    ModelAppleNotification *noti = [[ModelAppleNotification alloc] init];
    NSError *error = nil;
    [noti parseJSONDiction:userInfo error:&error dateTimeFormatter:nil];
    
    if (noti.send == NotificationType_AskSenderStartPurchase){
        [Utils postLocalNotification:NOTI_NeedPurchaseGift object:nil userInfo:[NSDictionary dictionaryWithObject:noti forKey:@"Noti"]];
        NSString *msg = [NSString stringWithFormat:@"Do you want to start purchase %@ %@ giftcard for %@",
                         [NSString stringWithFormat:FORMAT_MONEY_INTEGER_ONLY,(double)noti.gift.amount], noti.gift.gname,noti.name];
        [Utils showAlert:msg title:nil delegate:(id<AlertViewDelegate>)self firstButton:NSLocalizedString(@"Button_NO", nil)
            secondButton:NSLocalizedString(@"Button_YES", nil)
             withContext:Context_StartPurchaseGiftCard
       withContextObject:noti checkboxText:nil];
    }
    else if (noti.send == NotificationType_ReceivedNewGift){
        [Utils postLocalNotification:NOTI_ReceivedNewGift object:nil userInfo:[NSDictionary dictionaryWithObject:noti forKey:@"Noti"]];
        NSString *msg = [NSString stringWithFormat:@"%@ has sent you a %@ %@ giftcard.",
                         noti.name,
                         [NSString stringWithFormat:FORMAT_MONEY_INTEGER_ONLY,(double)noti.gift.amount],
                         noti.gift.gname == nil ? @"" : noti.gift.gname];
        [Utils showAlert:msg title:nil delegate:(id<AlertViewDelegate>)self firstButton:NSLocalizedString(@"Button_OK", nil)
            secondButton:nil
             withContext:Context_ReceivedGiftCard
       withContextObject:noti checkboxText:nil];
    }
    else if(noti.send == NotificationType_BotLinkConfirm){
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if(localUser&&localUser.sessionToken){
            [[NSNotificationCenter defaultCenter] postNotificationName:BotNewLinkNotification object:nil];
        }
    
    }
    else if (noti.send == NotificationType_BotGiftPurchase){
        //TODO: we should show notification message here if user already logged in Swych app. user can choose "yes"/"no" button on the popup.
        //      if user choose "YES", then we start the gift purchase flow. otherwise, dismiss the popup.
        SwychBaseViewController *ctrl = [self getCurrentViewController];
        LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (lui.sessionToken){
            [Utils showAlert:noti.aps.alert title:nil delegate:self];
        }
    }
}


-(void)updateAffiliateCode:(NSString*)affiliateCode{
    if(affiliateCode && ![affiliateCode isEqualToString:@""]){
        s_affiliateCode = affiliateCode;
        
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if(localUser&&localUser.sessionToken){
        //already login user
            [self UpdateAffiliateCode];
        }

        
    }
    else{
        s_affiliateCode = nil;
    }
}
-(void)UpdateAffiliateCode{
    if(s_affiliateCode){
    ModelAffiliate * Affiliate = [ModelAffiliate new];
    Affiliate.affiliateCode = s_affiliateCode;
    RequestUpdateAffiliate* req = [RequestUpdateAffiliate new];
    req.affiliate = Affiliate;
    
    SEND_REQUEST(req, handleUpdateAffiliateResponse, handleUpdateAffiliateResponse);
    }
}
-(void)handleUpdateAffiliateResponse:(ResponseBase*)response {

        ResponseUpdateAffiliate* resp = (ResponseUpdateAffiliate*)response;
        s_affiliateCode = nil;

}
-(void)swichToGiftView{
    self.tabbarController.selectedIndex = 1;
}
-(void)swichToGiftView:(ModelAppleNotification*)noti{
    self.tabbarController.selectedIndex = 1;
}
-(void)swichToCardsView{
    self.tabbarController.selectedIndex = 0;
}
#pragma 
#pragma mark - AlertViewDelegate

-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context == Context_StartPurchaseGiftCard){
        if (index == 2){
            [alertView dismiss];
            ModelAppleNotification *noti = (ModelAppleNotification *)alertView.contextObject;
            [self swichToGiftView:noti];
            return YES;
        }
    }
    else if (alertView.context == Context_ReceivedGiftCard){
        [alertView dismiss];
        SwychBaseViewController *ctrl = [self getCurrentViewController];
        [ctrl checkNewGift];
        return YES;
    }
    else if (alertView.context == Context_ReceivedBotGift){
        [alertView dismiss];
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if(localUser&&localUser.sessionToken){
            [[NSNotificationCenter defaultCenter] postNotificationName:BotNewTransactionNotification object:nil];
        }
        return YES;
    }
    return YES;
}

#pragma

-(void)requestPushNotificaitonToken:(NSInvocation*)invocation{
    self.invocationPushToken = invocation;
    
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

-(void)doRouting{
    LocalUserInfo *localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localData == nil){
        DEBUGLOG(@"%@",@"No user informatio stored at local.")
        UIViewController *nav = [Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"navController"];
        if ([nav isKindOfClass:[UINavigationController class]]){
            //MobileNumberViewController
            RegistrationMobileNumberViewController *ctrl = (RegistrationMobileNumberViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"MobileNumberViewController"];
            UINavigationController *navCtrl = (UINavigationController*)nav;
            
            navCtrl.viewControllers = [NSArray arrayWithObject:ctrl];
            self.window = [self createNewWindow];
            self.window.rootViewController = nav;
            [self.window makeKeyAndVisible];
            
            [self setupOtherInfos];
        }
    }
    else{
        if (localData.sessionToken != nil || localData.userStatus == UserStatus_PendingOTPVerified){
            if(localData.touchIDEnabled){
             [self switchToLoginView:NO preview:@""];
            }
            else
            [self sendAuthenticationRequest:localData];
        }
        else{
            [self switchToLoginView:NO preview:@""];
        }
    }
    
}

-(SwychBaseViewController*)getCurrentViewController{
    UIViewController *vc = self.window.rootViewController;
    if ([vc isKindOfClass:[SwychBaseViewController class]]){
        return (SwychBaseViewController*)vc;
    }
    else if ([vc isKindOfClass:[UINavigationController class]]){
        return ((UINavigationController*)vc).topViewController;
    }
    else if ([vc isKindOfClass:[UITabBarController class]]){
        UITabBarController *tabVC = (UITabBarController*)vc;
        if ([tabVC.selectedViewController isKindOfClass:[UINavigationController class]]){
            UINavigationController *navCtrl = (UINavigationController *)tabVC.selectedViewController;
            return  (SwychBaseViewController*)navCtrl.topViewController;
        }
        else{
            return nil;
        }
    }
    return nil;
}
-(void)showProfileView{
    UIViewController *nav = [Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"navController"];
    if ([nav isKindOfClass:[UINavigationController class]]){
        UINavigationController *navCtrl = (UINavigationController*)nav;
        UIViewController *ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
        navCtrl.viewControllers = [NSArray arrayWithObject:ctrl];
        
        self.window = [self createNewWindow];
        self.window.rootViewController = ctrl;
        [self.window makeKeyAndVisible];
    }

}
-(void)switchToHomeView:(LocalUserInfo*)localData{
    if (localData == nil){
        localData = [[LocalStorageManager instance] getLocalStoredUserInfo];
    }
    DEBUGLOG(@"Local stored user: %@",[localData getJSONStringWithDateTimeFormatter:nil]);
    UIViewController *ctrl = [Utils loadViewControllerFromStoryboard:@"Main"  controllerId:@"TabBarController"];
    if([ctrl isKindOfClass:[UITabBarController class]]){
        UITabBarController *ctrlTab = (UITabBarController *)ctrl;
        self.tabbarController = ctrlTab;
        
        [self setTabbarInfo:ctrlTab];
        
//        SlidingMenuViewController *ctrlMenu = (SlidingMenuViewController*)[Utils loadViewControllerFromStoryboard:@"SlidingMenu" controllerId:@"SlidingMenuViewController"];
//        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:ctrlMenu frontViewController:ctrlTab];
//        mainRevealController.rearViewRevealWidth = [[ApplicationConfigurations instance] getSlidingMenuViewWidth];
//        mainRevealController.delegate = self;
        
        
        self.window = [self createNewWindow];
        
        self.window.rootViewController = ctrlTab;// mainRevealController;
        [self.window makeKeyAndVisible];
        [self swichToGiftView:nil];
        
    }
    [self setupOtherInfos];
}
-(void)switchToLoginView:(BOOL)promptOTP preview:(NSString*)viewname{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    LoginViewController *ctrl = nil;
    if (localUser.regType == RegistrationType_Facebook){
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_Facebook"];
        ctrl.viewType = LoginViewType_Facebook;
    }
    else if (localUser.regType == RegistrationType_Google){
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_Google"];
        ctrl.viewType = LoginViewType_Google;
    }
    else{
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_PhoneNumber"];
        ctrl.viewType = LoginViewType_Phone;
    }
    if([viewname isEqualToString:@"register"]){
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        ctrl.localUser =  localUser;
    }
    else{
        ctrl.localUser = nil;
    }
    ctrl.promptOTP = promptOTP;
    UIViewController *nav = [Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"navController"];
    if ([nav isKindOfClass:[UINavigationController class]]){
        UINavigationController *navCtrl = (UINavigationController*)nav;
        navCtrl.viewControllers = [NSArray arrayWithObject:ctrl];
        
        self.window = [self createNewWindow];
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
    }
    [self setupOtherInfos];
}

-(void)showLoginView:(LoginViewType)viewType{
    LoginViewController *ctrl = nil;
    if (viewType == LoginViewType_Phone){
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_PhoneNumber"];
        ctrl.viewType = LoginViewType_Phone;
    }
    else if (viewType == LoginViewType_Facebook){
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_Facebook"];
        ctrl.viewType = LoginViewType_Facebook;
    }
    else if (viewType == LoginViewType_Google){
        ctrl = (LoginViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"LoginViewController_Google"];
        ctrl.viewType = LoginViewType_Google;
    }
    UIViewController *nav = [Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"navController"];
    if ([nav isKindOfClass:[UINavigationController class]]){
        UINavigationController *navCtrl = (UINavigationController*)nav;
        navCtrl.viewControllers = [NSArray arrayWithObject:ctrl];
        
        self.window = [self createNewWindow];
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
    }
    [self setupOtherInfos];
}

-(void)showHideHamburgerMenu{
    if ([self isHamburgerMenuShowing]){
        [self hideHamburgerMenu];
    }
    else{
        [self showHamburgerMenu];
    }
}

-(void)showHamburgerMenu{
    if (self.hamburgerMenuView != nil){
        [self.hamburgerMenuView removeFromSuperview];
        self.hamburgerMenuView  = nil;
    }
    self.hamburgerMenuView = (HamburgerMenuView*)[Utils loadViewFromNib:@"HamburgerMenuView" viewTag:1];
    self.hamburgerMenuView.frame = CGRectMake(self.window.bounds.size.width * (-1), 0, self.window.bounds.size.width, self.window.bounds.size.height);
    [self.window addSubview:self.hamburgerMenuView];
    self.hamburgerMenuView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.hamburgerMenuView updateInfo];
    
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.hamburgerMenuView.frame = CGRectMake(0, 0, self.window.bounds.size.width, self.window.bounds.size.height);
    [self.window bringSubviewToFront:self.hamburgerMenuView];
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView commitAnimations];
}
-(void)hideHamburgerMenu{
    if (self.hamburgerMenuView == nil){
        return;
    }
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.hamburgerMenuView.frame = CGRectMake(self.window.bounds.size.width * (-1), 0, self.window.bounds.size.width, self.window.bounds.size.height);
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(menuDidDisappeared)];
    [UIView commitAnimations];
}

-(void)menuDidDisappeared{
}

-(void)pushViewController:(UIViewController*)ctrl animated:(BOOL)animated{
    SwychBaseViewController *currentVC = [self getCurrentViewController];
    if (currentVC != nil){
        [currentVC.navigationController pushViewController:ctrl animated:animated];
    }
}

-(UIWindow*)createNewWindow{
    UIWindow *w = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    w.backgroundColor=[UIColor whiteColor];
    
    BOOL appInstalledFlag = [[LocalStorageManager instance] getAppInstalledFlag];
    
    if(appInstalledFlag != YES){
        [[LocalStorageManager instance] setAppInstalledFlag];
        [(AnalyticsShareASale*)[AnalyticsShareASale instance] trackInstallWithView:w];
    }
    
    return  w;
}

-(void)setTabbarInfo:(UITabBarController*)ctrl{
    UIColor *clr = [[ApplicationConfigurations instance] getTabbarBackgroundColor];
    if (clr != nil){
        ctrl.tabBar.backgroundColor = clr;
    }
    
    NSArray *array = [[ApplicationConfigurations instance] getTabbarItemInfo];
    if (array != nil && [array count] == [ctrl.tabBar.items count]){
        for(int i = 0; i < [array count]; i++){
            UITabBarItem *item = [ctrl.tabBar.items objectAtIndex:i];
            TabbarItem *item2 = [array objectAtIndex:i];
            
            item.title = item2.text;
            UIImage *img = [UIImage imageNamed:item2.iconImageName];
            if (img != nil){
                item.image = img;
                item.selectedImage = img;
            }
        }
    }
}

-(void)showPopupView:(PopupViewBase*)popup{
    [self.popupViewHandler pushView:popup];
}
-(void)dismissPopupView{
    [self.popupViewHandler popView];
}

-(void)setupOtherInfos{
    if(_keyboardHandler != nil){
        [_keyboardHandler removeNotifications];
    }
    _keyboardHandler = [[KeyboardHandler alloc] initWithWindow:self.window];
    self.popupViewHandler = [[PopupViewHandler alloc] init];
}

#ifdef __IPHONE_8_0

- (BOOL)checkNotificationType:(UIUserNotificationType)type
{
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    
    return (currentSettings.types & type);
}

#endif

- (void)setApplicationBadgeNumber:(NSInteger)badgeNumber
{
    UIApplication *application = [UIApplication sharedApplication];
    
#ifdef __IPHONE_8_0
    // compile with Xcode 6 or higher (iOS SDK >= 8.0)
    
    if(SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        application.applicationIconBadgeNumber = badgeNumber;
    }
    else
    {
        if ([self checkNotificationType:UIUserNotificationTypeBadge])
        {
            NSLog(@"badge number changed to %d", badgeNumber);
            application.applicationIconBadgeNumber = badgeNumber;
        }
        else
            NSLog(@"access denied for UIUserNotificationTypeBadge");
    }
    
#else
    // compile with Xcode 5 (iOS SDK < 8.0)
    application.applicationIconBadgeNumber = badgeNumber;
    
#endif
}


#pragma
#pragma mark - Server communications
-(void)sendUpdatePushTokenRequest:(NSString*)pushToken{
    RequestUpdatePushToken *req = [[RequestUpdatePushToken alloc] init];
    req.pushToken = pushToken;
    SEND_REQUEST(req, handleUpdatePushTokenResponse, handleUpdatePushTokenResponse)
}

-(void)handleUpdatePushTokenResponse:(ResponseBase*)response{
    if (_invocationPushToken != nil){
        [_invocationPushToken invoke];
    }
    _invocationPushToken = nil;
}

-(void)sendAuthenticationRequest:(LocalUserInfo*)localUser{
    RequestAuthentication *req = [[RequestAuthentication alloc] init];
    req.swychId = localUser.swychId;
    req.token = localUser.sessionToken;
    req.phoneNumber = localUser.phoneNumber;
    
    SEND_REQUEST(req, handleAuthenticationResponse , handleAuthenticationResponse);

}

-(void)handleAuthenticationResponse:(ResponseBase*)response{
    if (response.success && response.errorCode != ERR_Authentication_OTPVerificationNeeded){
        ResponseAuthentication *authResp = (ResponseAuthentication*)response;
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        [localUser copyFromUser:authResp.user];
        localUser.sysConfig = authResp.systemConfigurations;
        localUser.avatarImageURL = authResp.user.avatar;
        if(localUser.avatarImageURL != nil){
            [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
                if (!error){
                    [[LocalStorageManager instance] saveUserAvatarImage:image];
                    [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
                }
            }];
        }
        [[LocalStorageManager instance] saveLocalUserInfo:localUser];
        
        SEL selector = @selector(switchToHomeView:);
        NSMethodSignature *sig = [self methodSignatureForSelector:selector];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
        [invocation setSelector:selector];
        [invocation setTarget:self];
        LocalUserInfo *userInfo = nil;
        [invocation setArgument:&userInfo atIndex:2];
        [self requestPushNotificaitonToken:invocation];
    }
    else{
        [self switchToLoginView:response.errorCode == ERR_Authentication_OTPVerificationNeeded preview:@""];
    }
}
-(void)UpdateBanlance:(double)swychPoint{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];

    localUser.swychPoint = swychPoint;
    if(localUser.avatarImageURL != nil){
        [Utils downloadImageByURL:localUser.avatarImageURL callback:^(UIImage *image, NSError *error) {
            if (!error){
                [[LocalStorageManager instance] saveUserAvatarImage:image];
                [Utils postLocalNotification:NOTI_AvatarImageAvailable object:nil userInfo:nil];
            }
        }];
    }
    [[LocalStorageManager instance] saveLocalUserInfo:localUser];
}
#pragma
#pragma mark - SWRevealViewControllerDelegate Protocol

- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController{
    return YES;
}

#pragma 
#pragma mark - Test method
-(void)test{
    SwychBaseViewController *ctrl = [self getCurrentViewController];
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (lui.sessionToken){
        [[NSNotificationCenter defaultCenter] postNotificationName:BotNewLinkNotification object:nil];
    }
}
@end
