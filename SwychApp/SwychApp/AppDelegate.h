//
//  AppDelegate.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AnalyticsHandler.h"

@class KeyboardHandler;
@class PopupViewHandler;
@class PopupViewBase;
@class CustomizedActionSheet;
@class SwychBaseViewController;
@class LocalUserInfo;

@interface AppDelegate : UIResponder <UIApplicationDelegate,SWRevealViewControllerDelegate>{
    KeyboardHandler *_keyboardHandler;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AnalyticsHandler *analyticsHandler;
@property (strong,nonatomic) PopupViewHandler *popupViewHandler;
@property (nonatomic) int bakcgroudFlag;;

@property (nonatomic,assign) BOOL isProcessingBotTransaction;
@property (nonatomic,assign) BOOL callContactSyncResetNeeded;

-(void)showPopupView:(PopupViewBase*)popup;
-(void)dismissPopupView;

-(void)switchToHomeView:(LocalUserInfo*)localData;
-(void)switchToLoginView:(BOOL)promptOTP preview:(NSString*)viewname;
-(void)showLoginView:(LoginViewType)viewType;

-(void)requestPushNotificaitonToken:(NSInvocation*)invocation;

-(SwychBaseViewController*)getCurrentViewController;

-(void)showProfileView;
-(void)swichToCardsView;
-(void)swichToGiftView;
-(void)showHideHamburgerMenu;
-(void)pushViewController:(UIViewController*)ctrl animated:(BOOL)animated;
-(void)UpdateBanlance:(double)swychPoint;
-(void)UpdateAffiliateCode;

-(void)test;
@end

