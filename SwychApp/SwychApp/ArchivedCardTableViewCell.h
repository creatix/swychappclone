//
//  ArchivedCardTableViewCell.h
//  SwychApp
//
//  Created by Donald Hancock on 3/24/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchivedCardTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView* cardImage;

@property (nonatomic, weak) IBOutlet UILabel* cardNameLabel;

@property (nonatomic, weak) IBOutlet UILabel* amountLabel;

@end
