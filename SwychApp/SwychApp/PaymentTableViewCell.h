//
//  PaymentTableViewCell.h
//  SwychApp
//
//  Created by Donald Hancock on 3/25/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* label;
@property (nonatomic, weak) IBOutlet UIImageView* icon;
@property (nonatomic, weak) IBOutlet UIImageView* iconCheckmark;
@end
