//
//  MenuItemWithPosition.h
//  SwychApp
//
//  Created by Donald Hancock on 3/25/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "MenuItem.h"

typedef enum {
    kIconLocation_None = 0,
    kIconLocation_Left = 1,
    kIconLocation_Right = 2
} IconPosition;

@interface MenuItemWithPosition : MenuItem

@property (nonatomic, assign) IconPosition iconPosition;

@end
