//
//  UINavigationBar+Help.m
//  SwychApp
//
//  Created by Donald Hancock on 3/18/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UINavigationBar+Help.h"

@implementation UINavigationBar (Help)

- (void) clearBackground {
    [self setBackgroundImage:[UIImage new]
    forBarMetrics:UIBarMetricsDefault];
    self.shadowImage = [UIImage new];
    self.translucent = YES;
}

@end
