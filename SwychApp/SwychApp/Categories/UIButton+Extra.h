//
//  UIButton+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ObjectConfigInfo;

@interface UIButton (Extra)

-(void)setTitle:(NSString*)title;
-(void)setTitleColor:(UIColor *)color;
-(void)setButtonInfo:(ObjectConfigInfo*)config;
-(void)setButtonImage:(UIImage*)image;
-(void)setButtonBackgroundImage:(UIImage *)image;
-(void)underlineText;
-(BOOL)isAnimating;
-(void)startAnimation;
-(void)stopAnimation;

@end
