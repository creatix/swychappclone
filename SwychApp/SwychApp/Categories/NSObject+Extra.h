//
//  NSObject+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extra)
+(id)AllocEx;

-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName;
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName;

-(NSData*)getJSONDataWithDateTimeFormatter:(NSDateFormatter*)dateFormatter;
-(NSString*)getJSONStringWithDateTimeFormatter:(NSDateFormatter*)dateFormatter;

-(void)parseJSONData:(NSData*)jsonData error:(NSError**)error dateTimeFormatter:(NSDateFormatter*)dateFormatter;
-(void)parseJSONString:(NSString*)jsonString error:(NSError**)error  dateTimeFormatter:(NSDateFormatter*)dateFormatter;
-(void)parseJSONDiction:(NSDictionary*)dict error:(NSError**)error  dateTimeFormatter:(NSDateFormatter*)dateFormatter;

-(BOOL)isJSONIgnoredProperty:(NSString*)propName;
-(Class)getJSONArrayElementDataType:(NSString*)propertyName;

-(void)propertyValueWasSet:(NSString*)properyName;
@end
