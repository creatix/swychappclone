//
//  NSString+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extra)


+(BOOL)isNullOrEmpty:(NSString*)string;


-(NSString*)leftTrim;
-(NSString*)rightTrim;
-(NSString*)trim;
-(BOOL)allNumber;
-(NSData *)sha256Data;
-(NSString*)sha256String;
-(NSData*)HMACSHA256Data:(NSString*)secretKey;
-(NSString*)HMACSHA256String:(NSString*)secretKey;
-(BOOL)startWith:(NSString *)source;
@end
