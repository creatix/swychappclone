//
//  NSArray+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extra)

-(double)doubleTotal:(NSString*)property;
-(NSInteger)integerTotal:(NSString*)property;
@end
