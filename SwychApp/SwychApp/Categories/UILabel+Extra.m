//
//  UILabel+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UILabel+Extra.h"

@implementation UILabel (Extra)

//-(CGSize)intrinsicContentSize{
//    if (self.hidden){
//        return CGSizeZero;
//    }
//    return [super intrinsicContentSize];
//}
//-(CGSize)intrinsicContentSize{
//    if (self.hidden){
//        return CGSizeZero;
//    }
//    return [super intrinsicContentSize];
//}
-(void)AmountFormatWithDMark:(double)amount fontsize:(double)fontsize lengthOfRange:(int)range ifFromTheLeft:(BOOL)ifFromTheLeft{
    NSString* strAmount = [NSString stringWithFormat:@"$%0.02f", amount];
    int len  = (int)strAmount.length;
    if(len>2){
        len = len -range;
    }
    if(ifFromTheLeft){
        len = 0;
    }
    
    UIFont *fnt = [UIFont fontWithName:@"Helvetica" size:fontsize];
    
    CGFloat offset = self.font.capHeight - fnt.capHeight;
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strAmount];
    [string setAttributes:@{NSFontAttributeName : [fnt fontWithSize:fontsize]
                            , NSBaselineOffsetAttributeName :@(offset)} range:NSMakeRange(len,range)];
    self.attributedText = string;
}
@end
