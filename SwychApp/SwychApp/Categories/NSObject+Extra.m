//
//  NSObject+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "NSObject+Extra.h"
#import <objc/runtime.h>
#import "ResponseBase.h"

@implementation NSObject (Extra)
+(id)AllocEx{
    NSString *parentClassName = NSStringFromClass([self class]);
    NSString *subClassName = [[ApplicationConfigurations instance] getSubClassName:parentClassName];
    
    Class cls;
    if ([parentClassName compare:subClassName] != NSOrderedSame){
        cls =  NSClassFromString(subClassName);
    }
    else{
        cls = NSClassFromString(NSStringFromClass([self class]));
    }
    id instance = [cls alloc];
    
    return instance;
}

#pragma 
#pragma mark Name mapping betewen properties and JSON data
-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    return jsonKeyName;
}
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    return propertyName;
}

#pragma
#pragma mark JSON serialization

-(BOOL)isJSONIgnoredProperty:(NSString*)propName{
    return NO;
}

-(NSData*)getJSONDataWithDateTimeFormatter:(NSDateFormatter*)dateFormatter{
    if (![self isKindOfClass:[NSObject class]]){
        return nil;
    }
    NSError *error = nil;
    NSDictionary *dict = [self getJSONDictionaryDataWithDateTimeFormatter:dateFormatter];
    if (dict == nil  || [dict count] == 0){
        return nil;
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    if (error != nil){
        NSLog(@"Error when generating JSON data. code: %li, description: %@", error.code, error.localizedDescription);
        return nil;
    }
    return data;
}

-(NSString*)getJSONStringWithDateTimeFormatter:(NSDateFormatter*)dateFormatter{
    NSData *data = [self getJSONDataWithDateTimeFormatter:dateFormatter];
    return data == nil ? nil : [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

-(NSArray*)getPropertyNames{
    Class cls = [self class];
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    while (cls != [NSObject class]) {
        [self getProperiesFromClass:cls array:array];
        cls = [cls superclass];
    }
    return array;
}

-(NSDictionary *)getJSONDictionaryDataWithDateTimeFormatter:(NSDateFormatter*)dateFormatter{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:1];
    NSArray *array = [self getPropertyNames];
    for (NSString *name in array){
        if ([self isJSONIgnoredProperty:name]){
            continue;
        }
        objc_property_t prop = class_getProperty([self class], [name UTF8String]);
        char *typeEncoding = property_copyAttributeValue(prop, "T");
        if (typeEncoding == NULL){
            continue;
        }
        id value = [self valueForKey:name];
        NSString *jsonKey = [self getPropertyNameInJSONKeys:name];
        switch (typeEncoding[0]) {
            case '@':
            {
                
                if (value != nil){
                    Class cls = nil;
                    if (strlen(typeEncoding) >= 3){
                        char *className = strndup(typeEncoding + 2, strlen(typeEncoding) - 3);
                        cls = NSClassFromString([NSString stringWithUTF8String:className]);
                    }
                    if ([cls isSubclassOfClass:[NSString class]]){//NSString
                        [dict setObject:value forKey:jsonKey];
                    }
                    else if ([cls isSubclassOfClass:[NSNumber class]]){//NSNumber
                        [dict setObject:value forKey:jsonKey];
                    }
                    else if ([cls isSubclassOfClass:[NSArray class]]){//NSArray
                        NSArray *array = (NSArray*)value;
                        [dict setObject:[self arrayToJSONArray:array dateTimeFormatter:dateFormatter] forKey:jsonKey];
                    }
                    else if ([cls isSubclassOfClass:[NSDate class]]){
                        NSString *dateString = nil;
                        if (dateFormatter != nil){
                            dateString = [dateFormatter stringFromDate:value];
                        }
                        else{
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:DATE_TIME_FORMATTER];
                            dateString = [formatter stringFromDate:value];
                        }
                        [dict setObject:dateString forKey:jsonKey];
                    }
                    else if ([cls isSubclassOfClass:[NSDictionary class]]){//NSDictionary
                        [dict setObject:value forKey:jsonKey];
                    }
                    else{//other object
                        [dict setObject:[value getJSONDictionaryDataWithDateTimeFormatter:dateFormatter] forKey:jsonKey];
                    }
                }
            }
                break;
            case 'i': //int
            case 's': //short
            case 'l': //long
            case 'q': //long long
            case 'I': //unsigned int
            case 'S': //unsigned short
            case 'L': //unsigned long
            case 'Q': //unsigned long long
            case 'f': //float
            case 'd': //double
            {
                
                NSNumber * dvalue = (NSNumber*)value;
                CFNumberType numberType = CFNumberGetType((CFNumberRef)dvalue);
                if(numberType == kCFNumberDoubleType|| numberType == kCFNumberFloat64Type|| numberType == kCFNumberFloat32Type){
                double sd = [dvalue doubleValue];
                NSString* str = [NSString stringWithFormat:@"%0.02f",sd];
                [dict setObject:str forKey:jsonKey];
                }
                else{
                [dict setObject:value forKey:jsonKey];
                }
                break;
            }
            case 'B': //BOOL
            case 'c': //char
            case 'C': //unsigned char
            {
                [dict setObject:value forKey:jsonKey];
            }
                break;
            default:
                break;
        }
        free(typeEncoding);
    }
    return dict;
}

-(NSArray*)arrayToJSONArray:(NSArray*)orgArray dateTimeFormatter:(NSDateFormatter*)dateFormatter{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:[orgArray count]];
    
    for(int i = 0; i < [orgArray count]; i ++){
        id val = [orgArray objectAtIndex:i];
        if ([val isKindOfClass:[NSObject class]] && ![self isRootLevelObject:val]){
            [array addObject:[val getJSONDictionaryDataWithDateTimeFormatter:dateFormatter]];
        }
        else{
            [array addObject:val];
        }
    }
    return  array;
}

-(BOOL)isRootLevelObject:(id)object{
    if ([object isKindOfClass:[NSString class]]){
        return YES;
    }
    if ([object isKindOfClass:[NSValue class]]){
        return YES;
    }
    return NO;
}

-(void)getProperiesFromClass:(Class)cls array:(NSMutableArray*)array{
    uint propertiesCount;
    objc_property_t *classPropertiesArray = class_copyPropertyList(cls, &propertiesCount);
    for(int i = 0; i < propertiesCount; i++){
        objc_property_t property = classPropertiesArray[i];
        NSString *propertyName = [self getPropertyName:property];
        [array addObject:propertyName];
    }
    free(classPropertiesArray);
}

const char * property_getTypeString( objc_property_t property )
{
    const char * attrs = property_getAttributes( property );
    if ( attrs == NULL )
        return ( NULL );
    
    static char buffer[256];
    const char * e = strchr( attrs, ',' );
    if ( e == NULL )
        return ( NULL );
    
    int len = (int)(e - attrs);
    memcpy( buffer, attrs, len );
    buffer[len] = '\0';
    
    return ( buffer );
}

-(NSString*)getPropertyName:(objc_property_t)property{
    const char* name = property_getName(property);
    return [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
}

-(NSString*)getPropertyTypeName:(objc_property_t)property{
    const char* name = property_getTypeString(property);
    return [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
}

#pragma
#pragma mark JSON deserialization
-(void)parseJSONData:(NSData*)jsonData error:(NSError**)error  dateTimeFormatter:(NSDateFormatter*)dateFormatter{
    NSString *s = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self parseJSONString:s error:error dateTimeFormatter:dateFormatter];
}

-(void)parseJSONString:(NSString*)jsonString  error:(NSError**)error  dateTimeFormatter:(NSDateFormatter*)dateFormatter{
    if (jsonString == nil || ![self isKindOfClass:[NSObject class]]){
        *error = [[NSError alloc] initWithDomain:ERR_DOMAIN code:ERR_CODE_INVALID_JSON_DATA
                                        userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"InvalidJSONData", nil)}];
        return;
    }
    NSError *err = nil;
    NSDictionary *dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
    
    if (err){
        *error = [[NSError alloc] initWithDomain:ERR_DOMAIN code:ERR_CODE_JSON_DATA_PARSING
                                        userInfo:@{NSLocalizedDescriptionKey:[err localizedDescription]}];
    }
    else if (dict == nil && [dict count] == 0){
        *error = [[NSError alloc] initWithDomain:ERR_DOMAIN code:ERR_CODE_EMPTY_JSON_DATA
                                        userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"EmptyJSONData", nil)}];
    }
    else{
        if([dict isKindOfClass:[NSDictionary class]]){
            [self parseJSONDiction:dict error:error dateTimeFormatter:dateFormatter];
        }
        else if([dict isKindOfClass:[NSArray class]]){
            NSArray *array = (NSArray*)dict;
            [self parseJSONDiction:array.firstObject error:error dateTimeFormatter:dateFormatter];
        }
    }
}

-(void)parseJSONDiction:(NSDictionary*)dict error:(NSError**)error  dateTimeFormatter:(NSDateFormatter*)dateFormatter{
    if ([dict objectForKey:@"statusCode"] != nil){
        NSInteger statusCode = [[dict objectForKey:@"statusCode"] integerValue];
        if (statusCode == 404){
            if ([self isKindOfClass:[ResponseBase class]]){
                NSString *errMessage = [dict objectForKey:@"message"];
                ((ResponseBase*)self).errorCode = -1;
                ((ResponseBase*)self).errorMessage = errMessage;
                return;
            }
        }
    }
    NSArray *array = [self getPropertyNames];
    for (NSString *name in array){
        if ([self isJSONIgnoredProperty:name]){
            continue;
        }
        NSString *propertyKey = [self getJSONKeyNameInProperties:name];
        id value = [dict objectForKey:propertyKey];
        if (value == nil || [value isEqual:[NSNull null]]){
            continue;
        }
        objc_property_t prop = class_getProperty([self class], [name UTF8String]);
        char *typeEncoding = property_copyAttributeValue(prop, "T");
        if (typeEncoding == NULL){
            continue;
        }
        @try {
            switch (typeEncoding[0]) {
                case '@':
                {
                    
                    if (value != nil){
                        Class cls = nil;
                        if (strlen(typeEncoding) >= 3){
                            char *className = strndup(typeEncoding + 2, strlen(typeEncoding) - 3);
                            cls = NSClassFromString([NSString stringWithUTF8String:className]);
                        }
                        if ([cls isSubclassOfClass:[NSString class]]){//NSString
                            if ([value isKindOfClass:[NSString class]]){
                                [self setValue:value forKey:name];
                            }
                            else{
                                [self setValue:[value stringValue] forKey:name];
                            }
                        }
                        else if ([cls isSubclassOfClass:[NSNumber class]]){//NSNumber
                            NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
                            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                            [self setValue:[formatter numberFromString:value]
                                    forKey:name];
                        }
                        else if ([cls isSubclassOfClass:[NSArray class]]){//NSArray
                            //Some extra work need to do here about elements in the array
                            Class cls = [self getJSONArrayElementDataType:propertyKey];
                            if (cls != nil && [value isKindOfClass:[NSArray class]]){
                                NSMutableArray *array = [[NSMutableArray alloc] init];
                                
                                for(NSObject *element in value){
                                    if ([element isKindOfClass:[NSDictionary class]]){
                                        NSObject *obj = [[cls alloc] init];
                                        NSError *error = nil;
                                        [obj parseJSONDiction:(NSDictionary*)element error:&error dateTimeFormatter:nil];
                                        [array addObject:obj];
                                    }
                                    else{
                                        [array addObject:element];
                                    }
                                }
                                [self setValue:array forKey:name];
                            }
                        }
                        else if ([cls isSubclassOfClass:[NSDate class]]){
                            NSDate *date = nil;
                            if (dateFormatter != nil){
                                date = [dateFormatter dateFromString:value];
                            }
                            else{
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                                NSString *sentence1 = @"am";
                                NSString *sentence2 = @"pm";
                                if ([[value lowercaseString] rangeOfString:sentence1].location == NSNotFound&&[[value lowercaseString] rangeOfString:sentence2].location == NSNotFound) {
                                [formatter setDateFormat:DATE_TIME_FORMATTER_WITHOUTAM];
                                }
                                else{
                                [formatter setDateFormat:DATE_TIME_FORMATTER];
                                }
                                date = [formatter dateFromString:value];
                            }
                            [self setValue:date forKey:name];
                        }
                        else{//other object
                            if ([value isKindOfClass:[NSDictionary class]]){
                                NSDictionary *dict = (NSDictionary*)value;
                                if ([dict count] > 0){
                                    NSObject *obj = [[cls alloc] init];
                                    [obj parseJSONDiction:value error:error dateTimeFormatter:dateFormatter];
                                    [self setValue:obj forKey:name];
                                }
                                else{
                                    [self setValue:nil forKey:name];
                                }
                            }
                            else{
                                [self setValue:nil forKey:name];
                            }
                        }
                    }
                }
                    break;
                case 'i': //int
                case 's': //short
                case 'l': //long
                case 'q': //long long
                case 'I': //unsigned int
                case 'S': //unsigned short
                case 'L': //unsigned long
                case 'Q': //unsigned long long
                case 'f': //float
                case 'd': //double
                case 'B': //BOOL
                {
                    NSString *val = nil;
                    if (![value isKindOfClass:[NSString class]]){
                        val = [value stringValue];
                    }
                    else{
                        val = value;
                    }
                    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
                    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    NSNumber *num = [formatter numberFromString:val];
                    [self setValue:num
                            forKey:name];
                    
                }
                    break;
                case 'c': //char
                case 'C': //unsigned char
                {
                    if ([value isKindOfClass:[NSString class]]){
                        [self setValue:[NSNumber numberWithChar:[value characterAtIndex:0]]
                                forKey:name];
                    }
                    else{
                        NSString *val =nil;
                        if (![value isKindOfClass:[NSString class]]){
                            val = [value stringValue];
                        }
                        else{
                            val = value;
                        }
                        NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
                        [self setValue:[formatter numberFromString:val]
                                forKey:name];

                    }
                }
                    break;
                default:
                    break;
            }
        } @catch (NSException *exception) {
            NSLog(@"Excepiton: %@",exception.name);
        } @finally {
            
        }
        [self propertyValueWasSet:name];
        free(typeEncoding);
    }
}

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    return nil;
}

-(void)propertyValueWasSet:(NSString*)properyName{
    
}
@end
