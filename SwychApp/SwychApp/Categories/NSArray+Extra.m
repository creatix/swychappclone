//
//  NSArray+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "NSArray+Extra.h"

@implementation NSArray (Extra)
-(NSInteger)integerTotal:(NSString*)property{
    return [[self numberTotal:property] integerValue];
}
-(double)doubleTotal:(NSString*)property{
    return [[self numberTotal:property] doubleValue];
}

-(NSNumber*)numberTotal:(NSString*)property{
    if(property == nil){
        @throw [NSException exceptionWithName:@"Invalid parameter" reason:nil userInfo:nil];
    }
    return [self valueForKeyPath:[NSString stringWithFormat:@"@sum.%@",property]];
}
@end
