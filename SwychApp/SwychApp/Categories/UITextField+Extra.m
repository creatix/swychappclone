//
//  UITextField+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UITextField+Extra.h"

@implementation UITextField (Extra)

-(void)setPlaceHolderColor:(UIColor*)color{
    float systemVersion=[[[UIDevice currentDevice] systemVersion] floatValue];
    if(systemVersion>=7.0f)
    {
        [self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    }
    
}

@end
