//
//  UIButton+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIButton+Extra.h"
#import "ObjectConfigInfo.h"


#define BUTTON_ANIMATION_KEY    @"UPDATING"

@implementation UIButton (Extra)

-(void)setButtonInfo:(ObjectConfigInfo*)config{
    if (config == nil){
        return;
    }
    if (config.font != nil){
        self.titleLabel.font = config.font;
    }
    if (config.backgroundColor != nil){
        [self setBackgroundColor:config.backgroundColor];
    }
    if (config.textColor != nil){
        [self setTitleColor:config.textColor forState:UIControlStateNormal];
        [self setTitleColor:config.textColor forState:UIControlStateHighlighted];
    }
    if (config.fixedHeight > 0){
        self.frame = CGRectMake(self.frame.origin.x,
                                self.frame.origin.y,
                                self.bounds.size.width,
                                config.fixedHeight);
    }
}

-(void)setTitle:(NSString*)title{
    [self setTitle:title forState:UIControlStateHighlighted];
    [self setTitle:title forState:UIControlStateNormal];
}

-(void)setTitleColor:(UIColor *)color{
    [self setTitleColor:color forState:UIControlStateNormal];
    [self setTitleColor:color forState:UIControlStateHighlighted];
}

-(void)setButtonImage:(UIImage*)image{
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:image forState:UIControlStateHighlighted];
}

-(void)setButtonBackgroundImage:(UIImage *)image{
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateHighlighted];
}

-(void)underlineText{
    NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:self.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:linkAttributes];
    [self.titleLabel setAttributedText:attributedString];
}

- (void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event
{
    [super sendAction:action to:target forEvent:event];
    
}

-(BOOL)isAnimating{
    return [self.layer animationForKey:BUTTON_ANIMATION_KEY] != nil;
}

-(void) startAnimation
{
    const float PI = 2 * 3.14159265358979323846264338327950288;
    
    CAKeyframeAnimation *anm = [CAKeyframeAnimation animationWithKeyPath: @"transform.rotation.z"];
    anm.duration = 1.0f;// 0.5f;
    anm.cumulative = YES;
    anm.repeatCount = 0xffff;
    
    /*anm.values = [NSArray arrayWithObjects:
                  [NSNumber numberWithFloat: PI],
                  [NSNumber numberWithFloat: PI * .875f],
                  [NSNumber numberWithFloat: PI * .75f],
                  [NSNumber numberWithFloat: PI * .625f],
                  [NSNumber numberWithFloat: PI * .5f],
                  [NSNumber numberWithFloat: PI * .375f],
                  [NSNumber numberWithFloat: PI * .25f],
                  [NSNumber numberWithFloat: PI * .125f],
                  [NSNumber numberWithFloat: 0], nil];*/
    anm.values = [NSArray arrayWithObjects:
                  [NSNumber numberWithFloat: 0],
                  [NSNumber numberWithFloat: PI * .125f],
                  [NSNumber numberWithFloat: PI * .25f],
                  [NSNumber numberWithFloat: PI * .375f],
                  [NSNumber numberWithFloat: PI * .5f],
                  [NSNumber numberWithFloat: PI * .625f],
                  [NSNumber numberWithFloat: PI * .75f],
                  [NSNumber numberWithFloat: PI * .875f],
                  [NSNumber numberWithFloat: PI],
                  nil];

    anm.keyTimes = [NSArray arrayWithObjects:
                    [NSNumber numberWithFloat: 0],
                    [NSNumber numberWithFloat: .125],
                    [NSNumber numberWithFloat: .25],
                    [NSNumber numberWithFloat: .375],
                    [NSNumber numberWithFloat: .5],
                    [NSNumber numberWithFloat: .625],
                    [NSNumber numberWithFloat: .75],
                    [NSNumber numberWithFloat: .875],
                    [NSNumber numberWithFloat: 1], nil];
    
    [self.layer addAnimation: anm forKey: BUTTON_ANIMATION_KEY];
}

-(void) stopAnimation
{
    [self.layer removeAnimationForKey: BUTTON_ANIMATION_KEY];
}

@end
