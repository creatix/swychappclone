//
//  UINavigationBar+Help.h
//  SwychApp
//
//  Created by Donald Hancock on 3/18/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Help)

- (void) clearBackground;

@end
