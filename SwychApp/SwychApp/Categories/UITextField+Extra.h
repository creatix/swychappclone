//
//  UITextField+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UITextField (Extra)



-(void)setPlaceHolderColor:(UIColor*)color;
@end
