//
//  NSString+Extra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "NSString+Extra.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCrypto.h>
#import "NSData+Base64.h"

@implementation NSString (Extra)

-(NSString*)leftTrim{
    NSRange range = [self rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
    return [self stringByReplacingCharactersInRange:range withString:@""];
}
-(NSString*)rightTrim{
    NSRange range = [self rangeOfString:@"\\s*$" options:NSRegularExpressionSearch];
    return [self stringByReplacingCharactersInRange:range withString:@""];
}
-(NSString*)trim{
    return [[self leftTrim] rightTrim];
}


-(BOOL)allNumber{
    if ([self length] == 0){
        return YES;
    }
    NSRange range = [self rangeOfString:@"^[0-9]" options:NSRegularExpressionSearch];
    if (range.location == NSNotFound){
        return NO;
    }
    return YES;
}

+(BOOL)isNullOrEmpty:(NSString*)string{
    return string == nil || [string length] == 0;
}


- (NSData *)sha256Data
{
    const char *str = [self UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG) strlen(str), result);
    return [NSData dataWithBytes:(const void *)result
                          length:sizeof(unsigned char)*CC_SHA256_DIGEST_LENGTH];
}


-(NSString*)sha256String{
    NSData *data = [self sha256Data];
    return [data base64Encoding];
}

-(NSData*)HMACSHA256Data:(NSString*)secretKey{
    const char *cKey  = [secretKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [self cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMACData = [NSData dataWithBytes:cHMAC length:sizeof(cHMAC)];
    
    return HMACData;
}
-(NSString*)HMACSHA256String:(NSString*)secretKey{
    NSData *data = [self HMACSHA256Data:secretKey];
    return [data base64Encoding];
}

-(BOOL)startWith:(NSString *)source{
    NSRange range = [self rangeOfString:source];
    if(range.location != NSNotFound){
        return range.location == 0;
    }
    return NO;
}
@end
