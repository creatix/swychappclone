//
//  UIImage+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extra)
-(UIImage*)scaletoSizeKeepAspect:(CGSize)size;
@end
