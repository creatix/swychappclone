//
//  UIView+Helper.m
//  SwychApp
//
//  Created by Donald Hancock on 3/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIView+Helper.h"

@implementation UIView (Helper)

- (void) setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = (cornerRadius != 0);
}

- (CGFloat) cornerRadius {
    return self.layer.cornerRadius;
}

- (void) setBorderColor:(UIColor *)borderColor {
    self.layer.borderWidth = 2;
    self.layer.borderColor = borderColor.CGColor;
}

- (UIColor*) borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

@end
