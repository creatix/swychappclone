//
//  UIView+Helper.h
//  SwychApp
//
//  Created by Donald Hancock on 3/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Helper)

IBInspectable
@property (nonatomic, assign) CGFloat cornerRadius;

IBInspectable
@property (nonatomic, copy) UIColor* borderColor;

@end
