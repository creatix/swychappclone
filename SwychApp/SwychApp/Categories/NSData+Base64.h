//
//  NSData+Base64.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Base64)

- (NSString*)base64Encoding;

@end
