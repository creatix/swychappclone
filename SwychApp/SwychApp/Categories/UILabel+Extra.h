//
//  UILabel+Extra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extra)
-(void)AmountFormatWithDMark:(double)amount fontsize:(double)fontsize lengthOfRange:(int)range ifFromTheLeft:(BOOL)ifFromTheLeft;
@end
