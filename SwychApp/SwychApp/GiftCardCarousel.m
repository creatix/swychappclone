//
//  GiftCardCarousel.m
//  SwychApp
//
//  Created by Donald Hancock on 3/12/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftCardCarousel.h"
#import "iCarousel.h"
#import "ModelGiftCard.h"
#import "UIGiftcardView.h"
#import "Utils.h"
#import "ModelTransaction.h"


#define AspectRatio_Default 300.0/190.0

@interface GiftCardCarousel () <iCarouselDataSource, iCarouselDelegate> {
    iCarousel* _carousel;
    NSMutableDictionary* _cachedImageDictionary;
    
}

@end

@implementation GiftCardCarousel

@synthesize currentItemIndex = _currentItemIndex;
@synthesize transactions = _transactions;
@synthesize giftCards = _giftCards;

-(void)setGiftCards:(NSArray<ModelGiftCard *> *)giftCards{
    _giftCards = giftCards;
    [_cachedImageDictionary removeAllObjects];
    [_carousel reloadData];
    [_carousel scrollToItemAtIndex:_giftCards.count/2 animated:YES];
}

-(NSArray<ModelGiftCard*>*)giftCards{
    return _giftCards;
}

-(void)setTransactions:(NSArray<ModelTransaction *> *)transactions{
    if(transactions == nil || transactions.count == 0){
        _transactions = nil;
        self.giftCards = nil;
    }
    else{
        _transactions = transactions;
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
        for(ModelTransaction *tran in transactions){
            if (tran.giftCard.amount != tran.amount){
                tran.giftCard.amount = tran.amount;
            }
            [array addObject:tran.giftCard];
        }
        self.giftCards = [NSArray arrayWithArray:array];
    }
}

-(NSArray<ModelTransaction*>*)transactions{
    return _transactions;
}

-(void)setCurrentItemIndex:(NSInteger)currentItemIndex{
    _currentItemIndex = currentItemIndex;
    [_carousel scrollToItemAtIndex:currentItemIndex animated:YES];
}
-(NSInteger)currentItemIndex{
    return _currentItemIndex;
}

- (instancetype) initWithFrame:(CGRect)frame {
    if((self = [super initWithFrame:frame]) != nil) {
        [self setupCarousel:frame];
    }
    return self;
}

-(void)reloadData{
    [_carousel reloadData];
}
- (void) awakeFromNib {
    [super awakeFromNib];
    
    [self setupCarousel:self.frame];
}

- (void) setupCarousel:(CGRect)frame {
    _cachedImageDictionary = [NSMutableDictionary dictionary];
    
    frame.origin = (CGPoint){0,0};
    
    _carousel = [[iCarousel alloc] initWithFrame:frame];
    _carousel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _carousel.dataSource = self;
    _carousel.delegate = self;
    _carousel.type = iCarouselTypeCoverFlow;
    _carousel.zOffset = -200;
    _carousel.angle = 0.0;//-0.26090;
    _carousel.highlightedColor = [UIColor clearColor];
    _carousel.translatesAutoresizingMaskIntoConstraints = YES;
    _carousel.centerItemWhenSelected = YES;

    _carousel.backgroundColor = [UIColor clearColor];
    _carousel.disableTapGesture = YES;
    [self addSubview:_carousel];
    
    self.backgroundColor = [UIColor clearColor];
    
    
}

#pragma mark - iCarouselDataSource -

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return _giftCards != nil ? _giftCards.count : 0;
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view {
    UIGiftcardView *gcv = nil;
    if(view == nil || ![view isKindOfClass:[UIGiftcardView class]]) {
        CGRect rc = CGRectZero;
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getCarouselView:ItemAtIndex:)]){
            gcv = (UIGiftcardView*)[self.delegate getCarouselView:self ItemAtIndex:index];
        }
        else{
            CGFloat height = carousel.frame.size.height - 10;
            CGFloat width = (height - 20.0) * (300.0 / 190.0);
            rc = CGRectMake(0, 0, width, height);
        
            gcv = [[UIGiftcardView alloc] initWithFrame:rc];
        }
        gcv.giftcardViewDelegate = (id<UIGiftcardViewDelegate>)self;
        
        // add drop shadow
        gcv.layer.shadowRadius = 4;
        gcv.layer.shadowOpacity = 1;
        gcv.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
        gcv.layer.shadowOffset = CGSizeMake(5, 5);
    }
    else{
        gcv = (UIGiftcardView*)view;
    }
    //gcv.viewType = GiftcardView_Normal_bigger_bottom;
    gcv.tag = index;
    
    ModelGiftCard* giftCard = [_giftCards objectAtIndex:index];
    gcv.giftcard = giftCard;
    return gcv;
}

#pragma 
#pragma  mark - UIGiftcardViewDelegate
-(void)giftcardViewTapped:(UIGiftcardView*)giftcardView{
    ModelTransaction *tran = (_transactions == nil ? nil : [self.transactions objectAtIndex:_carousel.currentItemIndex]);
    ModelGiftCard* giftCard = giftcardView.giftcard;
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(giftCardCarousel:giftCardSelected:transaction:)]) {
        [self.delegate giftCardCarousel:self
                       giftCardSelected:giftCard
                            transaction:tran];
    }
}

#pragma mark - iCarouselDelegate -

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    if (_giftCards == nil || _transactions == nil || index >= _giftCards.count || index >= _transactions.count){
        return;
    }
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(giftCardCarousel:giftCardSelected:transaction:)]) {
        ModelGiftCard* giftCard = [_giftCards objectAtIndex:index];
        ModelTransaction *tran = (_transactions == nil ? nil : [_transactions objectAtIndex:index]);
        
        [self.delegate giftCardCarousel:self
                       giftCardSelected:giftCard transaction:tran];
    }
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel {
    if (_giftCards == nil || _transactions == nil || carousel.currentItemIndex >= _giftCards.count || carousel.currentItemIndex >= _transactions.count){
        return;
    }
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(giftCardCarousel:giftCardFocused:transaction:)]) {
        ModelGiftCard* giftCard = [_giftCards objectAtIndex:carousel.currentItemIndex];
        ModelTransaction *tran = (_transactions == nil ? nil : [_transactions objectAtIndex:carousel.currentItemIndex]);
        
        [self.delegate giftCardCarousel:self
                        giftCardFocused:giftCard transaction:tran];
    }
}

@end
