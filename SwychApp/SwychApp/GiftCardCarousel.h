//
//  GiftCardCarousel.h
//  SwychApp
//
//  Created by Donald Hancock on 3/12/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelGiftCard;
@class ModelTransaction;

@protocol GiftCardCarouselDelegate;

IB_DESIGNABLE
@interface GiftCardCarousel : UIView

@property (nonatomic,copy) NSArray<ModelGiftCard *> *giftCards;
@property (nonatomic,strong) NSArray<ModelTransaction*> *transactions;

@property (nonatomic, weak) IBOutlet NSObject<GiftCardCarouselDelegate>* delegate;
@property (nonatomic, assign) NSInteger currentItemIndex;

-(void)reloadData;
@end

@protocol GiftCardCarouselDelegate <NSObject>

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
         giftCardSelected:(ModelGiftCard*)giftCard transaction:(ModelTransaction*)transacton;

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
         giftCardFocused:(ModelGiftCard*)giftCard  transaction:(ModelTransaction*)transacton;

@optional
-(UIView*)getCarouselView:(GiftCardCarousel*)carousel ItemAtIndex:(NSInteger)index;
@end