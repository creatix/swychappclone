//
//  RequestVerifyOTP.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestVerifyOTP.h"

#define HTTP_VERB       @"/api/v1/otp/verify"

@implementation RequestVerifyOTP
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}


@end
