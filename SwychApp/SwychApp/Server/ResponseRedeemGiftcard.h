//
//  ResponseRedeemGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseRedeemGiftcard : ResponseBase

@property(nonatomic,strong) NSString* transactionIdFrom;
@property(nonatomic,strong) NSArray<NSString*> *transactionIdRedeemed;
@end
