//
//  RequestManuallyUpdateBalance.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-24.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestManuallyUpdateBalance.h"
#define HTTP_VERB       @"/api/v1/giftcards/balance"
@implementation RequestManuallyUpdateBalance
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
