//
//  ResponseGetNearbyLocations.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelLocation;

@interface ResponseGetNearbyLocations : ResponseBase

@property(nonatomic,strong) NSArray<ModelLocation*> *locations;
@end
