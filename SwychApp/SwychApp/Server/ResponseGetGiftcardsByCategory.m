//
//  ResponseGetGiftcardsByCategory.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetGiftcardsByCategory.h"
#import "ModelGiftCard.h"
#import "NSObject+Extra.h"
#import "ModelFilterOption.h"

@implementation ResponseGetGiftcardsByCategory

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftCards"]){
        return [ModelGiftCard class];
    }
    else if ([propertyName isEqualToString:@"filterOptions"]){
        return [ModelFilterOption class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
