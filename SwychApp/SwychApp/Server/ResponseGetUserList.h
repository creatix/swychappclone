//
//  ResponseGetUserList.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelUser;

@interface ResponseGetUserList : ResponseBase

@property (nonatomic,strong) NSArray<ModelUser*> *users;
@end
