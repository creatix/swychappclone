//
//  RequestLogOut.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestLogOut.h"
#import "ResponseLogOut.h"


#define HTTP_VERB       @"/api/v1/authentication"

@implementation RequestLogOut
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_DELETE;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
