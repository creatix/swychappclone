//
//  RequestAuthentication.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestAuthentication : RequestBase

@property(nonatomic,strong) NSString *phoneNumber;
@property(nonatomic,strong) NSString *facebookId;
@property(nonatomic,strong) NSString *fbToken;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *googleId;
@property(nonatomic,strong) NSString *googleToken;
@property(nonatomic,strong) NSString *twitterId;
@property(nonatomic,strong) NSString *twitterToken;

-(NSString*)phoneNumberWithoutPossibleAreaCode;
@end
