//
//  ResponseBotSentGiftLists.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelBotTransaction.h"
@interface ResponseBotSentGiftLists : ResponseBase
@property(nonatomic,strong) NSArray<ModelBotTransaction*> *botTransactions;
@property(nonatomic,strong) NSString*  promptMessage;
@end
