//
//  RequestGetHistory.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetHistory : RequestBase
@property (nonatomic,assign) NSInteger filter;
@property (nonatomic,strong) NSString *startDate;
@property (nonatomic,strong) NSString *endDate;
@property (nonatomic,assign) NSInteger numberOfTransactions;

@end
