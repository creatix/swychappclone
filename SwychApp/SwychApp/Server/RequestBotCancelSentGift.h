//
//  RequestBotCancelSentGift.h
//  SwychApp
//
//  Created by kndev3 on 10/24/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestBotCancelSentGift : RequestBase
@property (nonatomic,strong) NSString *botTransactionId;
@end
