//
//  ResponseGetArchivedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetArchivedGiftcards.h"
#import "NSObject+Extra.h"
#import "ModelGiftCard.h"

@implementation ResponseGetArchivedGiftcards

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftCards"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
