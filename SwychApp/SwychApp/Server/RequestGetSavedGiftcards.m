//
//  RequestGetSavedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetSavedGiftcards.h"

#define HTTP_VERB       @"/api/v1/giftcards/saved"

@implementation RequestGetSavedGiftcards

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
