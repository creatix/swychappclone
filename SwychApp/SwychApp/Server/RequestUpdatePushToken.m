//
//  RequestUpdatePushToken.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUpdatePushToken.h"


#define HTTP_VERB       @"/api/v1/pushtoken"

@implementation RequestUpdatePushToken

@synthesize pushToken = _pushToken;
@synthesize prodToken = _prodToken;

-(BOOL)prodToken{
    return _prodToken;
}

-(void)setPushToken:(NSString *)pushToken{
    _pushToken = pushToken;
#ifdef DEBUG
    _prodToken = NO;
#else
    _prodToken = YES;
#endif
}

-(NSString*)pushToken{
    return _pushToken;
}
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
