//
//  RequestValidatePassword.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-08-17.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestValidatePassword.h"
#define HTTP_VERB       @"/api/v1/users/validatePassword"
@implementation RequestValidatePassword
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}
@end
