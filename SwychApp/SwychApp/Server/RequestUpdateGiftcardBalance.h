//
//  RequestUpdateGiftcardBalance.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestUpdateGiftcardBalance : RequestBase

@property (nonatomic,strong) NSString *giftcardNumber;

@end
