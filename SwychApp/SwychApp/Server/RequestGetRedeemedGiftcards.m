//
//  RequestGetRedeemedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetRedeemedGiftcards.h"

#define HTTP_VERB_ALL       @"/api/v1/giftcards/redeemed"
#define HTTP_VERB_SINGLE    @"/api/v1/giftcards/redeemed/%@"


@implementation RequestGetRedeemedGiftcards


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    if (self.transactinId > 0){
        return [NSString stringWithFormat:HTTP_VERB_SINGLE, self.transactinId];
    }
    else{
        return HTTP_VERB_ALL;
    }
}



@end
