//
//  ResponseGetSavedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelGiftCard;
@class ModelTransaction;

@interface ResponseGetSavedGiftcards : ResponseBase

@property (nonatomic,strong) NSArray<ModelTransaction *> *savedGiftcardsTransaction;
@property (nonatomic,readonly) double totalAmount;

@end
