//
//  RequestRedeemGiftcard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestRedeemGiftcard.h"

#define HTTP_VERB       @"/api/v1/redeem"

@implementation RequestRedeemGiftcard

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}



@end
