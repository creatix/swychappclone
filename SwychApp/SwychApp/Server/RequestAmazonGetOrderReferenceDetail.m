//
//  RequestAmazonGetOrderReferenceDetail.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestAmazonGetOrderReferenceDetail.h"


#define HTTP_VERB       @"/api/v1/amazon/getorderReferenceDetail"


@implementation RequestAmazonGetOrderReferenceDetail

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
