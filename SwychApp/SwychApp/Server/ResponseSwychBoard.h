//
//  ResponseSwychBoard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelSwychBoard;

@interface ResponseSwychBoard : ResponseBase

@property (nonatomic,strong) ModelSwychBoard *swychBoard;

@end
