//
//  RequestBase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@class ResponseBase;

@interface RequestBase : CommunicationModelBase

@property(nonatomic,strong) NSString *clientId;
@property(nonatomic,strong) NSString *apiKey;
@property(nonatomic,strong) NSString *token;
@property(nonatomic,strong) NSString *swychId;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *deviceType;
@property(nonatomic,strong) NSString *deviceUniqueId;
@property(nonatomic,strong) NSString *appVersion;
@property(nonatomic,strong) NSString *language;
@property(nonatomic,strong) NSString *ext;
@property(nonatomic,strong) NSString *deviceInfo;

-(ResponseBase*)getResponse;
-(NSString*)getVerb;
-(NSString*)getHTTPMethod;
-(NSDictionary*)getExtraHeaders;
@end
