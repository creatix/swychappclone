//
//  RequestVerifyPromotionCode.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestVerifyPromotionCode : RequestBase

@property (nonatomic,assign) NSInteger giftCardId;
@property (nonatomic,assign) double purchaseAmount;
@property (nonatomic,strong) NSString *promotionCode;
@property (nonatomic,assign) int purchaseType;
@end
