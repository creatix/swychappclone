//
//  RequestSwychBoard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestSwychBoard.h"


#define HTTP_VERB       @"/api/v1/swychboard/%@"

@implementation RequestSwychBoard

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.swychId];
}

@end
