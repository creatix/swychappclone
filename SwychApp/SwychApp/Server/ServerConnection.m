//
//  ServerConnection.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ServerConnection.h"
#import "RequestBase.h"
#import "ResponseBase.h"
#import "NSObject+Extra.h"
#import "LocationManager.h"
typedef void(^communicationCompletion)(ResponseBase*);


@interface ServerConnectionWorker : NSObject<NSURLConnectionDelegate>{
    NSURLConnection *_connection;
    ResponseBase *_response;
    NSMutableData *_responseData;
    NSTimer *_timer;
    communicationCompletion _successCompletion;
    communicationCompletion _failureCompletion;
}

-(id)initWithResponse:(ResponseBase*)resp success:(communicationCompletion)success failure:(communicationCompletion)failure;
-(void)sendRequest:(NSURLRequest*)req;
@end

@implementation ServerConnectionWorker

#pragma mark - NSURLConnectionDelegate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount]> 0) {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        return;
    }
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust: challenge.protectionSpace.serverTrust] forAuthenticationChallenge: challenge];
        return;
    }
    else  {
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger code = [httpResponse statusCode];
    if (code != 200) {
        _response.success = NO;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_responseData appendData: data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @synchronized(self){
        if(!_timer){
            return;
        }
        [_timer invalidate];
        _timer =nil;
    }
    NSError *error = nil;
#if DEBUG
    NSLog(@"Response - %@ - %@",NSStringFromClass([_response class]),[[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding]);
#endif

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_TIME_FORMATTER];
    [_response parseJSONData:_responseData error:&error dateTimeFormatter:formatter];
    if (!error){
        if (!_response.success) {
            _failureCompletion(_response);
        }
        else {
            _successCompletion(_response);
        }
    }
    else{
        _response.success = false;
        _response.errorCode = error.code;
        _response.errorMessage = [error localizedDescription];
        _failureCompletion(_response);
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    @synchronized(self){
        if(!_timer){
            return;
        }
        [_timer invalidate];
        _timer = nil;
    }
    _response.success = NO;
    switch (error.code) {
        case NSURLErrorNotConnectedToInternet:
            _response.errorMessage = NSLocalizedString(@"NoInternetConnection", nil);
            break;
        default:
            _response.errorMessage = NSLocalizedString(@"FailedConnectToServer", nil);
            break;
    }
    
    _failureCompletion(_response);
}

-(void) cancelConnection
{
    @synchronized(self){
        if(!_timer){
            return;
        }
        _timer = nil;
        [_connection cancel];
    }
    _response.success = NO;
    _response.errorMessage = NSLocalizedString(@"RequestTimeout", @"Request Timeout");
    _failureCompletion(_response);
}

-(id)initWithResponse:(ResponseBase*)resp success:(communicationCompletion)success failure:(communicationCompletion)failure{
    if (self = [super init]){
        _response = resp;
        _successCompletion = success;
        _failureCompletion = failure;
        _responseData = [[NSMutableData alloc] init];
    }
    return self;
}
-(void)sendRequest:(NSURLRequest*)req{
    _connection = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    _timer = [NSTimer scheduledTimerWithTimeInterval:COMM_TIMEOUT
                                          target:self
                                            selector:@selector(cancelConnection)
                                            userInfo:nil
                                             repeats:NO];
}


@end

@implementation ServerConnection

+(void)sendRequest:(RequestBase*)request success:(void(^)(ResponseBase*))success failure:(void(^)(ResponseBase*))failure{
    NSString *sURL = [NSString stringWithFormat:@"%@%@",[[ApplicationConfigurations instance] getServerURL],[request getVerb]];
    if (sURL == nil){
        ResponseBase *resp = [request getResponse];
        resp.errorMessage = NSLocalizedString(@"InvalidServerURL", @"Invalid server URL.");
        failure(resp);
    }
    NSURL *url = [NSURL URLWithString:sURL];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    NSString *httpMethod = [request getHTTPMethod];
    [req setHTTPMethod:httpMethod];
    [req setValue:@"Application/JSON" forHTTPHeaderField:@"Accept"];
    [req setValue:@"Application/JSON;charset=UTF-8" forHTTPHeaderField:@"content-Type"];
    CLLocation *loc = [[LocationManager instance] getCurrentLocation];
    if (loc != nil){
        NSString* lat = [NSString stringWithFormat:@"%f" ,loc.coordinate.latitude ];
        NSString* lon = [NSString stringWithFormat:@"%f" ,loc.coordinate.longitude ];
        [req setValue:lat forHTTPHeaderField:@"latitude"];
        [req setValue:lon forHTTPHeaderField:@"longitude"];
    }
    NSDictionary *dictHeaders = [request getExtraHeaders];
    if ([dictHeaders count] > 0){
        for(NSString *key in dictHeaders.allKeys){
            [req setValue:[dictHeaders objectForKey:key] forHTTPHeaderField:key];
        }
    }
    NSData *data = nil;
    if (![httpMethod isEqualToString:HTTP_METHOD_GET]){
        data = [request getJSONDataWithDateTimeFormatter:nil];
        [req setHTTPBody:data];
    }
#if DEBUG
    NSLog(@"Request [%@] - %@ ",httpMethod,sURL);
    NSLog(@"%@ - %@",NSStringFromClass([request class]), data == nil ? @"" : [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
#endif
    ResponseBase *resp = [request getResponse];
    resp.context = request.context;
    resp.contextObject = request.contextObject;
    ServerConnectionWorker *worker = [[ServerConnectionWorker alloc] initWithResponse:resp success:success failure:failure];
    [worker sendRequest:req];
}
@end
