//
//  RequestPreGift.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-08-23.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestPreGift.h"
#define HTTP_VERB       @"/api/v1/giftcards/pregift"
@implementation RequestPreGift
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
