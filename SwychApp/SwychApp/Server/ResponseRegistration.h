//
//  ResponseRegistration.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelUser;

@interface ResponseRegistration : ResponseBase

@property(nonatomic,strong) ModelUser *user;
@property(nonatomic,strong) NSString *token;
@end

