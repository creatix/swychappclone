//
//  RequestGetGiftcardsByCategory.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetGiftcardsByCategory.h"

#define HTTP_VERB       @"/api/v1/giftcards/category"

@implementation RequestGetGiftcardsByCategory

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
