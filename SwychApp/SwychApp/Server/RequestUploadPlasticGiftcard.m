//
//  RequestUploadPlasticGiftcard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUploadPlasticGiftcard.h"

#define HTTP_VERB       @"/api/v1/giftcards/plastic"

@implementation RequestUploadPlasticGiftcard

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}



@end
