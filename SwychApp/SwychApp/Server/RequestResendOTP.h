//
//  RequestResendOTP.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestResendOTP : RequestBase
@property(nonatomic,strong) NSString *otpSentType;
@property(nonatomic,strong) NSString *email;

@end
