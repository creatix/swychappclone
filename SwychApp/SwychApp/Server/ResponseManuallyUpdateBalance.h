//
//  ResponseManuallyUpdateBalance.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-24.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseManuallyUpdateBalance : ResponseBase
@property(nonatomic,assign) double balance;
@end
