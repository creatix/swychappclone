//
//  RequestChangePassword.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestChangePassword.h"

#define HTTP_VERB       @"/api/v1/users/changePassword"

@implementation RequestChangePassword

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
