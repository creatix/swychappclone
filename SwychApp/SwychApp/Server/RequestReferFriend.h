//
//  RequestReferFriend.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestReferFriend : RequestBase
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *message;
@end
