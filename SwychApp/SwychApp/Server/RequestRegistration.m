//
//  RequestRegistration.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestRegistration.h"
#import "ResponseRegistration.h"

#define HTTP_VERB   @"/api/v1/users"

@implementation RequestRegistration

@synthesize phoneNumber = _phoneNumber;

-(id)init{
    self = [super init];
    if (self){
        self.mcc = [Utils getPhoneMCC];
    }
    return self;
}
-(void)setPhoneNumber:(NSString *)phoneNumber{
    _phoneNumber =  [Utils phoneNumberWithAreaCode:phoneNumber];
}

-(NSString*)phoneNumber{
    return [Utils phoneNumberWithAreaCode:_phoneNumber];
}

-(NSString*)phoneNumberWithoutPossibleAreaCode{
    if (_phoneNumber!= nil && _phoneNumber.length == 11){
        if ([[_phoneNumber substringToIndex:0] isEqualToString:@"1"]){
            return [_phoneNumber substringFromIndex:1];
        }
    }
    return _phoneNumber;
}

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(ResponseBase*)getResponse{
    return [[ResponseRegistration alloc] init];
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}

@end
