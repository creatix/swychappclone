//
//  RequestContactSyncAccessToken.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestContactSyncAccessToken.h"

#define HTTP_VERB       @"/api/v1/bot/contactSync/token"
@implementation RequestContactSyncAccessToken
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
