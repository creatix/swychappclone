//
//  RequestBase.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"
#import "Utils.h"
#import "LocalUserInfo.h"

@implementation RequestBase

@synthesize apiKey,clientId,token,swychId,password,deviceType,deviceUniqueId,appVersion,language,ext;

-(id)init{
    if(self = [super init]){
        self.clientId = [[ApplicationConfigurations instance] getConnectionClientID];
        self.apiKey = [[ApplicationConfigurations instance] getConnectionApiKey];
        self.swychId = [[LocalStorageManager instance] getStoredSwychId];
        self.deviceType = [Utils getDeviceType];
        self.deviceUniqueId = [Utils getDeviceUniqueId];
        self.language = [Utils getCurrentLanguage];
        self.appVersion = [Utils getAppVersion];
        self.deviceInfo = [Utils getDeviceInfo];
        
    }
    return self;
}

-(ResponseBase*)getResponse{
    NSString *clsNameRequestFull = NSStringFromClass([self class]);
    NSRange range = [clsNameRequestFull rangeOfString:@"Request"];
    if (range.location == NSNotFound){
        return nil;
    }
    NSString *clsNameSub = [clsNameRequestFull substringFromIndex:7];
    NSString *clsNameRespFull = [NSString stringWithFormat:@"Response%@",clsNameSub];
    id resp = [[NSClassFromString(clsNameRespFull) alloc] init];
    return resp;
}

-(NSString*)getVerb{
    assert(nil);
    return nil;
}

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSDictionary*)getExtraHeaders{
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if(!user) return nil;
    return  [NSDictionary dictionaryWithObject:user.sessionToken forKey:@"Authorization"];
}

@end
