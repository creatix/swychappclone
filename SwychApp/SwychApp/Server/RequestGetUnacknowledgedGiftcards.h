//
//  RequestGetUnacknowledgedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetUnacknowledgedGiftcards : RequestBase

@end
