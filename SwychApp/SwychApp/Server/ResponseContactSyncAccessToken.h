//
//  ResponseContactSyncAccessToken.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseContactSyncAccessToken : ResponseBase

@property(nonatomic,strong) NSString *contactSyncToken;

@end
