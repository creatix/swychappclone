//
//  ResponseGetAvailableAndSuggestedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetAvailableAndSuggestedGiftcards.h"
#import "ModelGiftCard.h"
#import "NSObject+Extra.h"

@implementation ResponseGetAvailableAndSuggestedGiftcards


-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"availableGiftCards"] || [propertyName isEqualToString:@"suggestedGiftCards"]){
        return [ModelGiftCard class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
