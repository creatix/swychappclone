//
//  RequestRedeemGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"


@class ModelRedeemOptionCash;
@class ModelRedeemOptionGiftCard;


@interface RequestRedeemGiftcard : RequestBase

@property(nonatomic,strong) NSString* transactionId;
@property(nonatomic,assign) double swychBalance;
@property(nonatomic,strong) NSArray<ModelRedeemOptionCash*> *cashOptions;
@property(nonatomic,strong) NSArray<ModelRedeemOptionGiftCard*> *giftCardOptions;

@end
