//
//  RequestActivation.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestActivation : RequestBase

@property(nonatomic,strong) NSString* oneTimePIN;
@property(nonatomic,strong) NSString* firstName;
@property(nonatomic,strong) NSString* lastName;
@property(nonatomic,strong) NSString* email;
@property(nonatomic,strong) NSString *facebookId;
@property(nonatomic,strong) NSString *fbToken;
@property(nonatomic,strong) NSString *googleId;
@property(nonatomic,strong) NSString *googleToken;
@property(nonatomic,strong) NSString *twitterId;
@property(nonatomic,strong) NSString *twitterToken;

@end
