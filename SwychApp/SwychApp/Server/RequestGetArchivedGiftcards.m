//
//  RequestGetArchivedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetArchivedGiftcards.h"

#define HTTP_VERB       @"/api/v1/giftcards/archived"


@implementation RequestGetArchivedGiftcards


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
