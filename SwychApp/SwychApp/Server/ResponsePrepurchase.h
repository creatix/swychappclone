//
//  ResponsePrepurchase.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-22.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponsePrepurchase : ResponseBase
@property(nonatomic,assign) double discount;
@property(nonatomic,strong) NSString* promoCode;
@end
