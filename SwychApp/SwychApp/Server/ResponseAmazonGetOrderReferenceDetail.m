//
//  ResponseAmazonGetOrderReferenceDetail.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseAmazonGetOrderReferenceDetail.h"
#import "ModelAmazonIdList.h"

@implementation ResponseAmazonGetOrderReferenceDetail


-(NSString*)amazonAuthorizationId{
    if (self.orderReferenceDetail == nil || self.orderReferenceDetail.getOrderReferenceDetailsResult == nil || self.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails == nil ){
        return nil;
    }
    
    ModelAmazonOrderReferenceDetails *details =self.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails;
    if (details.idList != nil){
        return details.idList.amazonAuthorizationId;
    }
    return nil;
}

-(NSString*)amazonOrderState{
    if (self.orderReferenceDetail == nil || self.orderReferenceDetail.getOrderReferenceDetailsResult == nil || self.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails == nil ){
        return nil;
    }
    
    ModelAmazonOrderReferenceDetails *details =self.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails;
    if (details.orderReferenceStatus != nil){
        return details.orderReferenceStatus.state;
    }
    return nil;
}
@end
