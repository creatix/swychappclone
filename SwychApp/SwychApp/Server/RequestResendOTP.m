//
//  RequestResendOTP.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestResendOTP.h"
#import "ResponseResendOTP.h"

#define HTTP_VERB       @"/api/v1/otp/%@"

@implementation RequestResendOTP

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.swychId];
}

-(ResponseBase*)getResponse{
    return [[ResponseResendOTP alloc] init];
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}

@end
