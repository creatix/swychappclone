//
//  RequestUserUpdate.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUserUpdate.h"
#import "ResponseUserUpdate.h"

#define HTTP_VERB       @"/api/v1/users/%@"

@implementation RequestUserUpdate

@synthesize firstName,lastName,email;

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_PUT;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.swychId];
}

-(ResponseBase*)getResponse{
    return [[ResponseUserUpdate alloc] init];
}

@end
