//
//  RequestArchiveGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestArchiveGiftcard : RequestBase

@property (nonatomic,strong) NSString* transactionId;
@property (nonatomic,assign) BOOL isPlasticGiftcard;

@end
