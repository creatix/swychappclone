//
//  RequestUploadImageVideo.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUploadImageVideo.h"

#define HTTP_VERB       @"/api/v1/imageVideo"

@implementation RequestUploadImageVideo

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}



@end
