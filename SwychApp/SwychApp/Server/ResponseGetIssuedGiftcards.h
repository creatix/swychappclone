//
//  ResponseGetIssuedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelGiftCard;
@class ModelTransaction;

@interface ResponseGetIssuedGiftcards : ResponseBase

@property (nonatomic,strong) NSArray<ModelGiftCard *> *giftCards;
@property (nonatomic,strong) NSArray<ModelTransaction *> *issuedGiftcardsTransaction;

@end
