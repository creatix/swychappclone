//
//  RequestBotSentGiftLists.m
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "ResponseBotSentGiftLists.h"
#import "RequestBotSentGiftLists.h"
#define HTTP_VERB   @"/api/v1/bot/botTransaction"
@implementation RequestBotSentGiftLists
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
-(ResponseBase*)getResponse{
    return [[ResponseBotSentGiftLists alloc] init];
}
@end
