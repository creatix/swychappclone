//
//  RequestGetFeaturedDeals.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetFeaturedDeals.h"

#define HTTP_VERB       @"/api/v1/giftcards/featured"

@implementation RequestGetFeaturedDeals

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}


@end
