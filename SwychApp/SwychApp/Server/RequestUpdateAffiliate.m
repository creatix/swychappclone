//
//  RequestUpdateAffiliate.m
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUpdateAffiliate.h"
#define HTTP_VERB       @"/api/v1/affiliate"
@implementation RequestUpdateAffiliate
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
