//
//  RequestBotCancelSentGift.m
//  SwychApp
//
//  Created by kndev3 on 10/24/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBotCancelSentGift.h"
#import "ResponseBotCancelSentGift.h"
#define HTTP_VERB   @"/api/v1/bot/botTransaction/cancel"
@implementation RequestBotCancelSentGift
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_DELETE;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
-(ResponseBase*)getResponse{
    return [[ResponseBotCancelSentGift alloc] init];
}
@end
