//
//  RequestVoidPayment.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestVoidPayment.h"

#define HTTP_VERB       @"/api/v1/payment/void"

@implementation RequestVoidPayment


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}




@end
