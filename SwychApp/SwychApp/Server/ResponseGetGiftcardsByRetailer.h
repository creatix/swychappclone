//
//  ResponseGetGiftcardsByRetailer.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelGiftCard;
@class ModelFilterOption;

@interface ResponseGetGiftcardsByRetailer : ResponseBase

@property (nonatomic,strong) NSArray<ModelGiftCard *> *giftCards;
@property (nonatomic,strong) NSArray<ModelFilterOption*> *filterOptions;

@end
