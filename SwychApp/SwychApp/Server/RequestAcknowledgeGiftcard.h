//
//  RequestAcknowledgeGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestAcknowledgeGiftcard : RequestBase

@property(nonatomic,strong) NSArray *transactionIds;
@end
