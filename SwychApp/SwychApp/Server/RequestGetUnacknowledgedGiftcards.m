//
//  RequestGetUnacknowledgedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetUnacknowledgedGiftcards.h"


#define HTTP_VERB       @"/api/v1/acknowledge"

@implementation RequestGetUnacknowledgedGiftcards

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
