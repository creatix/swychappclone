//
//  CommunicationConst.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#ifndef CommunicationConst_h
#define CommunicationConst_h


#define HTTP_METHOD_GET     @"GET"
#define HTTP_METHOD_POST    @"POST"
#define HTTP_METHOD_PUT     @"PUT"
#define HTTP_METHOD_DELETE  @"DELETE"

#define COMM_TIMEOUT        30

/*  Error Code from server */
#define ERR_Registration_OneStep                    10000001
#define ERR_Registration_TwoSteps                   10000002
#define ERR_Registration_Failed                     10000003
#define ERR_Registration_UserExist                  10000004


#define ERR_UserActivation_Success                  10200001
#define ERR_UserActivation_ExpiredOTP               10200002
#define ERR_UserActivation_CannotActiveUser         10200003
#define ERR_UserActivation_InvalidOTP               10200004
#define ERR_UserActivation_NeedUpdateProfile        10200005


#define ERR_Authentication_Success                  10100001
#define ERR_Authentication_CannotLogin              10100002
#define ERR_Authentication_ExceedAttempts           10100003
#define ERR_Authentication_DisabledUser             10100004
#define ERR_Authentication_UserNotExist             10100005
#define ERR_VerifyPhonenumber_UserNotExist          10400010
#define ERR_Authentication_PendingActivationUser    10100006
#define ERR_Authentication_SocialLoginOnly          10100007
#define ERR_Authentication_OTPVerificationNeeded    10100008


#define ERR_Payment_BuyerEmailVerificationNeeded    10400021

#define ERR_SessionExpired                          20000002
#endif /* CommunicationConst_h */
