//
//  ResponseUploadImageVideo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseUploadImageVideo : ResponseBase

@property(nonatomic,strong) NSString *urlImage;
@property(nonatomic,strong) NSString *urlVideo;

@end
