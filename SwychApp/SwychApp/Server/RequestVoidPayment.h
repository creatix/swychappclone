//
//  RequestVoidPayment.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@class ModelPaymentInfo;

@interface RequestVoidPayment : RequestBase

@property(nonatomic,strong) NSArray<ModelPaymentInfo*> *payments;

@end
