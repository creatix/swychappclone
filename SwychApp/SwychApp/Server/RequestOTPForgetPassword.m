//
//  RequestOTPForgetPassword.m
//  SwychApp
//
//  Created by kndev3 on 9/9/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestOTPForgetPassword.h"
#import "ResponseResendOTP.h"

#define HTTP_VERB       @"/api/v1/otp/phone/%@"
@implementation RequestOTPForgetPassword
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.phone];
}

-(ResponseBase*)getResponse{
    return [[ResponseResendOTP alloc] init];
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}
@end
