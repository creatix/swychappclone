//
//  ResponseGetAffiliateList.m
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetAffiliateList.h"
#import "NSObject+Extra.h"
@implementation ResponseGetAffiliateList
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"affiliates"]){
        return [ModelAffiliate class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}
@end
