//
//  RequestReferFriend.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestReferFriend.h"
#define HTTP_VERB       @"/api/v1/refer"
@implementation RequestReferFriend
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
