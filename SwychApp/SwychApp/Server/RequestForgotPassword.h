//
//  RequestForgotPassword.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-20.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestForgotPassword : RequestBase
@property(nonatomic,strong) NSString *newpassword;
@property(nonatomic,strong) NSString *phoneNumber;
@property(nonatomic,strong) NSString *otp;
@end
