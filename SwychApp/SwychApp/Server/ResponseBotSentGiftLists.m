//
//  ResponseBotSentGiftLists.m
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBotSentGiftLists.h"
#import "NSObject+Extra.h"
@implementation ResponseBotSentGiftLists
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"botTransactions"]){
        return [ModelBotTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}
@end
