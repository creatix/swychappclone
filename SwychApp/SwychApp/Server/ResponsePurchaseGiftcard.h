//
//  ResponsePurchaseGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"


@class ModelUser;

@interface ResponsePurchaseGiftcard : ResponseBase

@property(nonatomic,strong) NSString *transactionId;
@property(nonatomic,strong) ModelUser *user;
@property(nonatomic,assign) double earnedPoints;

@property(nonatomic,assign) BOOL emailVerificationNeeded;
@property(nonatomic,strong) NSString *email;

@end
