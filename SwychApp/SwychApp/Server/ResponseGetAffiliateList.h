//
//  ResponseGetAffiliateList.h
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelAffiliate.h"
@interface ResponseGetAffiliateList : ResponseBase
@property (nonatomic,strong) NSArray<ModelAffiliate *> *affiliates;
@property (nonatomic,strong) ModelAffiliate *userAffiliate;
@property (nonatomic,strong) NSString *affiliateMessage;
@end
