//
//  RequestGetNearbyLocations.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetNearbyLocations : RequestBase

@property(nonatomic,strong) NSString *merchantName;
@property(nonatomic,assign) double latitude;
@property(nonatomic,assign) double longitude;

@end
