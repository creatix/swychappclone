//
//  RequestGetSuggestedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetSuggestedGiftcards : RequestBase
@property (nonatomic,assign) int purchaseType;
@end
