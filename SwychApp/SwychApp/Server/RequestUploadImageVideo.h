//
//  RequestUploadImageVideo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestUploadImageVideo : RequestBase

@property(nonatomic,strong) NSString* transactionId;
@property(nonatomic,assign) long long plasticGiftcardId;
@property(nonatomic,strong) NSString *imageData;
@property(nonatomic,strong) NSString *videoData;

@end
