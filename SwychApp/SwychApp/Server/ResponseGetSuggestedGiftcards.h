//
//  ResponseGetSuggestedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelGiftCard.h"


@interface ResponseGetSuggestedGiftcards : ResponseBase

@property (nonatomic,strong) NSArray<ModelGiftCard *> *giftCards;

@end
