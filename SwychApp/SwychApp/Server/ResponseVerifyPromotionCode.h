//
//  ResponseVerifyPromotionCode.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseVerifyPromotionCode : ResponseBase

@property (nonatomic,assign) double discount;
@end
