//
//  CommunicationModelBase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommunicationModelBase : NSObject
@property(nonatomic,assign) NSInteger context;
@property(nonatomic,strong) NSObject *contextObject;

-(NSDictionary*)getPropertiesMapping;
-(BOOL)isJSONIgnoredProperty:(NSString *)propName;
@end
