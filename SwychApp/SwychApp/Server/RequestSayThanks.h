//
//  RequestSayThanks.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-05-18.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestSayThanks : RequestBase
@property (nonatomic,strong) NSString* transactionId;
@end
