//
//  RequestUpdateGiftcardBalance.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestUpdateGiftcardBalance.h"

#define HTTP_VERB       @"/api/v1/giftcards/balance/%@"

@implementation RequestUpdateGiftcardBalance

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.giftcardNumber];
}



@end
