//
//  CommunicationModelBase.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"
#import <objc/runtime.h>
#import "NSObject+Extra.h"

@implementation CommunicationModelBase

@synthesize context,contextObject;


-(NSDictionary*)getPropertiesMapping{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:1];
    uint propertiesCount;
    objc_property_t *classPropertiesArray = class_copyPropertyList([self class], &propertiesCount);
    for(int i = 0; i < propertiesCount; i++){
        const char* propertyName = property_getName(classPropertiesArray[i]);
        NSString *str = [NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding];
        [dict setObject:str forKey:str];
    }
    free(classPropertiesArray);
    return dict;
}

-(BOOL)isJSONIgnoredProperty:(NSString *)propName{
    if ([propName caseInsensitiveCompare:@"context"] == NSOrderedSame||
        [propName caseInsensitiveCompare:@"contextObject"] == NSOrderedSame){
        return  YES;
    }
    return [super isJSONIgnoredProperty:propName];
}
@end
