//
//  RequestPrepurchase.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-22.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestPrepurchase.h"
#define HTTP_VERB       @"/api/v1/giftcards/prepurchase"
@implementation RequestPrepurchase
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
