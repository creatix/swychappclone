//
//  ResponseBotLinkingLists.m
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBotLinkingLists.h"
#import "NSObject+Extra.h"
@implementation ResponseBotLinkingLists
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"botInfo"]){
        return [ModelBotLink class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}
@end
