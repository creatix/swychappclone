//
//  RequestSayThanks.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-05-18.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestSayThanks.h"
#define HTTP_VERB       @"/api/v1/saythanks"
@implementation RequestSayThanks
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
