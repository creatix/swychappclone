//
//  ResponseGetUnacknowledgedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelTransaction;

@interface ResponseGetUnacknowledgedGiftcards : ResponseBase


@property (nonatomic,strong) NSArray<ModelTransaction *> *transactions;

@end
