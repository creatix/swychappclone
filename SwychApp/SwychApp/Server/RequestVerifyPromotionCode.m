//
//  RequestVerifyPromotionCode.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestVerifyPromotionCode.h"


#define HTTP_VERB       @"/api/v1/promotion"

@implementation RequestVerifyPromotionCode

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
