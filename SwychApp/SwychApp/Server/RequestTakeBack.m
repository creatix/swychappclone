//
//  RequestTakeBack.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-05-16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestTakeBack.h"
#define HTTP_VERB       @"/api/v1/giftcards/takeback"
@implementation RequestTakeBack
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_DELETE;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
