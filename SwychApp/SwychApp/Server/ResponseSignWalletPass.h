//
//  ResponseSignWalletPass.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseSignWalletPass : ResponseBase

@property (nonatomic,strong) NSString *passData;

@end
