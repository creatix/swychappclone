//
//  RequestPreGift.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-08-23.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"
#import "ModelPaymentInfo.h"
#define PAY_METHOD_NONE   678
#define PAY_METHOD_APPPLE_PAY   0x0123
#define PAY_METHOD_PAYPAL       0x0124
#define PAY_METHOD_CREDITCARD   0x0125
#define PAY_METHOD_AMAZON       0x0126

@interface RequestPreGift : RequestBase
@property(nonatomic,strong) NSString *recipientPhoneNumber;
@property(nonatomic,assign) NSString *recipientSwychId;
@property(nonatomic,strong) NSString *recipientEmail;

@property(nonatomic,assign) NSInteger giftCardId;
@property (nonatomic,strong) NSString *utid;
@property(nonatomic,assign) double giftCardAmount;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSArray<ModelPaymentInfo*> *payments;

@property(nonatomic,strong) NSString *deliverDate;

//@property(nonatomic,strong ) NSString *image;
@property (nonatomic,strong) NSString *video;

@property(nonatomic,assign) NSInteger purchaseType; //1 - for friend, 2 - self gifting, 3 - swych gift card
@end
