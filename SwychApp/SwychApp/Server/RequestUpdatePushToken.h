//
//  RequestUpdatePushToken.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestUpdatePushToken : RequestBase

@property(nonatomic,strong) NSString *pushToken;
@property(nonatomic,readonly) BOOL prodToken;
@end
