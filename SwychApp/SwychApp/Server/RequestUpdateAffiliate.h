//
//  RequestUpdateAffiliate.h
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"
#import "ModelAffiliate.h"
@interface RequestUpdateAffiliate : RequestBase
@property (nonatomic,strong) ModelAffiliate *affiliate;
@end
