//
//  RequestGetNearbyLocations.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetNearbyLocations.h"

#define HTTP_VERB       @"/api/v1/location"

@implementation RequestGetNearbyLocations


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}



@end
