//
//  RequestGetGiftcardsByRetailer.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetGiftcardsByRetailer : RequestBase
@property (nonatomic,assign) int purchaseType;
@end
