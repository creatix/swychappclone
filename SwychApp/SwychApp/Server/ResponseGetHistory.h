//
//  ResponseGetHistory.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelTransaction.h"
@class ModelFilterOption;
@interface ResponseGetHistory : ResponseBase
@property (nonatomic,strong) NSArray<ModelTransaction *> *transactions;


@end
