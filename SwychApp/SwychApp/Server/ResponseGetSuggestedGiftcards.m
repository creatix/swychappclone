//
//  ResponseGetSuggestedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetSuggestedGiftcards.h"
#import "NSObject+Extra.h"
#import "ModelGiftCard.h"

@implementation ResponseGetSuggestedGiftcards

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftCards"]){
        return [ModelGiftCard class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
