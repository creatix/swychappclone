//
//  RequestAmazonGetOrderReferenceDetail.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestAmazonGetOrderReferenceDetail : RequestBase

@property(nonatomic,strong) NSString *amazonToken;
@property(nonatomic,strong) NSString *amazonReferenceId;
@property(nonatomic,strong) NSString *amazonQueryString;

@end
