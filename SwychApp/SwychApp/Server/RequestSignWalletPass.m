//
//  RequestSignWalletPass.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestSignWalletPass.h"

#define HTTP_VERB       @"/api/v1/wallet"

@implementation RequestSignWalletPass

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
