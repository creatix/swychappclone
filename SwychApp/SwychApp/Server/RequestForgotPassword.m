//
//  RequestForgotPassword.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-20.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestForgotPassword.h"
#define HTTP_VERB       @"/api/v1/users/forgotPassword"
@implementation RequestForgotPassword
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}
@end
