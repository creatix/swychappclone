//
//  ResponseUserUpdate.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
@class ModelUser;

@interface ResponseUserUpdate : ResponseBase
@property(nonatomic,strong) ModelUser *user;

@end
