//
//  ResponseGetAvailableAndSuggestedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"


@class ModelGiftCard;

@interface ResponseGetAvailableAndSuggestedGiftcards : ResponseBase

@property(nonatomic,strong) NSArray<ModelGiftCard*> *availableGiftCards;
@property(nonatomic,strong) NSArray<ModelGiftCard*> *suggestedGiftCards;

@end
