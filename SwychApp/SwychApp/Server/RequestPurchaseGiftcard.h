//
//  RequestPurchaseGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"
#import "ModelPaymentInfo.h"


@interface RequestPurchaseGiftcard : RequestBase

@property(nonatomic,strong) NSString *recipientPhoneNumber;
@property(nonatomic,assign) NSString *recipientSwychId;
@property(nonatomic,strong) NSString *recipientEmail;

@property(nonatomic,assign) NSInteger giftCardId;
@property (nonatomic,strong) NSString *utid;
@property(nonatomic,assign) double giftCardAmount;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSArray<ModelPaymentInfo*> *payments;

@property(nonatomic,strong) NSString *deliverDate;

@property(nonatomic,strong ) NSString *image;
@property (nonatomic,strong) NSString *video;

@property(nonatomic,assign) NSInteger purchaseType; //1 - for friend, 2 - self gifting, 3 - swych gift card

@property(nonatomic,strong) NSString *botTransactionId; //pass transactionId to Swych server when doing purchase started from BOT sendgift request.
@end
