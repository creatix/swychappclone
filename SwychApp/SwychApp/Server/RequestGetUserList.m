//
//  RequestGetUserList.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetUserList.h"
#import "ResponseGetUserList.h"
#import "ModelUser.h"


#define HTTP_VERB   @"/api/v1/users/%@"

@implementation RequestGetUserList
@synthesize users;

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.clientId];
}

-(ResponseBase*)getResponse{
    return [[ResponseGetUserList alloc] init];
}

@end
