//
//  RequestPrepurchase.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-07-22.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestPrepurchase : RequestBase
@property(nonatomic,assign) NSInteger gifcardId;
@property(nonatomic,assign) double amount;
@property (nonatomic,assign) int purchaseType;
@property (nonatomic,strong) NSString *utid;
@end
