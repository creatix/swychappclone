//
//  RequestBotLinkConfirm.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"
@interface RequestBotLinkConfirm : RequestBase
@property (nonatomic,strong) NSString *botAccountNumber;
@property (nonatomic,strong) NSString *linkingToken;
@property (nonatomic,assign) BOOL confirm;
@end
