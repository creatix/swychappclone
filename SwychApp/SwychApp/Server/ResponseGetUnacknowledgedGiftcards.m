//
//  ResponseGetUnacknowledgedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetUnacknowledgedGiftcards.h"
#import "NSObject+Extra.h"
#import "ModelTransaction.h"

@implementation ResponseGetUnacknowledgedGiftcards

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"transactions"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
