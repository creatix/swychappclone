//
//  ServerConnection.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

/*
 Use sendRequest method below:
 
 [ServerConnection sendRequest:req
 success:^(ResponseBase* success){
 
 }
 failure:^(ResponseBase* failure){
 
 }];

 
 */
#import <Foundation/Foundation.h>

@class RequestBase;
@class ResponseBase;

@interface ServerConnection : NSObject

+(void)sendRequest:(RequestBase*)request success:(void(^)(ResponseBase*))success failure:(void(^)(ResponseBase*))failure;
@end
