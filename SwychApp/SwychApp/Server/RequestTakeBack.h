//
//  RequestTakeBack.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-05-16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestTakeBack : RequestBase
@property (nonatomic,strong) NSString* transactionId;
@end
