//
//  ResponseBotLinkingLists.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelBotLink.h"
@interface ResponseBotLinkingLists : ResponseBase
@property(nonatomic,strong) NSArray<ModelBotLink*> *botInfo;
@end
