//
//  ResponseGetGiftcardsByRetailer.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetGiftcardsByRetailer.h"
#import "ModelGiftCard.h"
#import "NSObject+Extra.h"
#import "ModelFilterOption.h"

@implementation ResponseGetGiftcardsByRetailer


-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftCards"]){
        return [ModelGiftCard class];
    }
    if ([propertyName isEqualToString:@"filterOptions"]){
        return [ModelFilterOption class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}


-(void)propertyValueWasSet:(NSString*)properyName{
    if ([properyName isEqualToString:@"filterOptions"]){
        [CacheData instance].filterOptions = self.filterOptions;
    }
}

@end
