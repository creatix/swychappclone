//
//  ResponseGetNearbyLocations.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetNearbyLocations.h"
#import "ModelLocation.h"
#import "NSObject+Extra.h"

@implementation ResponseGetNearbyLocations

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"locations"]){
        return [ModelLocation class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
