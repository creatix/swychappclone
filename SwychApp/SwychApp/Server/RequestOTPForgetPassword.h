//
//  RequestOTPForgetPassword.h
//  SwychApp
//
//  Created by kndev3 on 9/9/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestOTPForgetPassword : RequestBase
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *otpSentType;
@end
