//
//  RequestAcknowledgeGiftcard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestAcknowledgeGiftcard.h"

#define HTTP_VERB       @"/api/v1/acknowledge"

@implementation RequestAcknowledgeGiftcard

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}



@end
