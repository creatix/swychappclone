//
//  ResponseAmazonGetOrderReferenceDetail.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelAmazonOrderReferenceDetail.h"
#import "ModelAmazonPaymentInstrument.h"


@interface ResponseAmazonGetOrderReferenceDetail : ResponseBase

@property(nonatomic,strong) ModelAmazonOrderReferenceDetail *orderReferenceDetail;
@property(nonatomic,strong) NSString *signature;
@property(nonatomic,strong) ModelAmazonPaymentInstrument *paymentInstrument;

@property(nonatomic,readonly) NSString *amazonAuthorizationId;
@property(nonatomic,readonly) NSString *amazonOrderState;
@property(nonatomic,assign) BOOL emailVerificationNeeded;
@property(nonatomic,strong) NSString *email;

@end
