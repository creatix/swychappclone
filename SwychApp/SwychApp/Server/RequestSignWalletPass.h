//
//  RequestSignWalletPass.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestSignWalletPass : RequestBase

@property(nonatomic,strong) NSString* transactionId;
@property(nonatomic,assign) NSInteger plasticGiftcardId;

@end
