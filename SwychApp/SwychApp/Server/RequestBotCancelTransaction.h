//
//  RequestBotCancelTransaction.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestBotCancelTransaction : RequestBase

@property(nonatomic,strong) NSString *botTransactionId;

@end
