//
//  ResponsePreGift.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-08-23.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelPaymentProcessorData;


@interface ResponsePreGift : ResponseBase
@property(nonatomic) BOOL validatePassword;
@property(nonatomic) BOOL emailVerificationNeeded;
@property(nonatomic,strong) NSString* senderEmail;

@property(nonatomic,strong) ModelPaymentProcessorData *paymentProcessorData;

@end
