//
//  RequestUserUpdate.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestUserUpdate : RequestBase

@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *phoneNumber;

@end
