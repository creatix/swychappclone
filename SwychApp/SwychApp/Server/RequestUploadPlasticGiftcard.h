//
//  RequestUploadPlasticGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestUploadPlasticGiftcard : RequestBase

@property(nonatomic,assign) NSInteger giftCardId;
@property(nonatomic,strong) NSString *giftCardNumber;
@property(nonatomic,strong) NSString *giftCardPIN;
@property(nonatomic,assign) double giftCardBalance;

@end
