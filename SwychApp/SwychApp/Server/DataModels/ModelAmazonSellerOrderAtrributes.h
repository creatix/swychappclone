//
//  ModelAmazonSellerOrderAtrributes.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonSellerOrderAtrributes : NSObject

@property(nonatomic,strong) NSString *customInformation;
@property(nonatomic,strong) NSString *orderItemCategories;
@property(nonatomic,strong) NSString *sellerOrderId;
@property(nonatomic,strong) NSString *storeName;

@end
