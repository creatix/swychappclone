//
//  ModelPaymentInfo.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPaymentInfo.h"

@implementation ModelPaymentInfo

-(id)init{
    self = [super init];
    if (self){
        self.paymentProcessor = PaymentProcessor_Unknow;
    }
    return self;
}
@end
