//
//  ModelSystemConfiguration.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelSystemConfiguration.h"
#import "ModelFilterOption.h"
#import "CommunicationModelBase.h"
#import <objc/runtime.h>
#import "NSObject+Extra.h"
#import "ModelPaymentProcessor.h"

@implementation ModelSystemConfiguration

-(id)init{
    self = [super init];
    if (self){
    }
    return self;
}
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"filterOptions"]){
        return [ModelFilterOption class];
    }
    
    return [super getJSONArrayElementDataType:propertyName];
}

-(void)propertyValueWasSet:(NSString*)properyName{
    if ([properyName isEqualToString:@"filterOptions"]){
        [CacheData instance].filterOptions = self.filterOptions;
    }
}
@end
