//
//  ModelBotLink.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelBotLink : CommunicationModelBase
@property(nonatomic,strong) NSString *botAccountNumber;
@property(nonatomic,strong) NSString *linkingToken;
@property(nonatomic,strong) NSString *message;
@end
