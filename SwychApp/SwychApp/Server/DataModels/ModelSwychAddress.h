//
//  ModelSwychAddress.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelSwychAddress : CommunicationModelBase

@property(nonatomic,strong) NSString *line1;
@property(nonatomic,strong) NSString *line2;
@property(nonatomic,strong) NSString *line3;
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *state;
@property(nonatomic,strong) NSString *country;
@property(nonatomic,strong) NSString *postalCode;

@end
