//
//  ModelSwychBoard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"


@class ModelUser;

@interface ModelSwychBoard : CommunicationModelBase

@property(nonatomic,assign) double swychBalance;
@property (nonatomic,assign) double swychPoints;
@property (nonatomic,strong) ModelUser *user;

@end
