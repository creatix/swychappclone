//
//  ModelAmazonOrderReferenceDetails.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelAmazonBuyer.h"
#import "ModelAmazonConstraints.h"
#import "ModelAmazonDestination.h"
#import "ModelAmazonOrderReferenceStatus.h"
#import "ModelAmazonSellerOrderAtrributes.h"
#import "ModelAmazonIdList.h"
#import "ModelAmazonOrderTotal.h"
#import "ModelAmazonAddress.h"

@interface ModelAmazonOrderReferenceDetails : NSObject
@property(nonatomic,strong) NSString *amazonOrderReferenceId;
@property(nonatomic,strong) ModelAmazonAddress *billingAddress;
@property(nonatomic,strong) ModelAmazonBuyer *buyer;
@property(nonatomic,strong) ModelAmazonConstraints *constraints;
@property(nonatomic,strong) NSString *creationTimestamp;
@property(nonatomic,strong) ModelAmazonDestination *destination;
@property(nonatomic,strong) NSString *expirationTimestamp;
@property(nonatomic,strong) ModelAmazonIdList *idList;
@property(nonatomic,strong) NSString *orderLanguage;
@property(nonatomic,strong) ModelAmazonOrderReferenceStatus *orderReferenceStatus;
@property(nonatomic,strong) ModelAmazonOrderTotal *orderTotal;
@property(nonatomic,strong) NSString *parentDetails;
@property(nonatomic,strong) NSString *platformId;
@property(nonatomic,assign) NSInteger releaseEnvironment;
@property(nonatomic,strong) NSString *sellerNote;
@property(nonatomic,strong) ModelAmazonSellerOrderAtrributes *sellerOrderAttributes;


@property(nonatomic,readonly) NSString *amazonAuthorizationId;
@end
