//
//  ModelSwychBillingInfo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@class ModelSwychAddress;

@interface ModelSwychBillingInfo : CommunicationModelBase

@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *middleName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) ModelSwychAddress *billingAddress;
@property(nonatomic,strong) ModelSwychAddress *shippingAddress;

@end
