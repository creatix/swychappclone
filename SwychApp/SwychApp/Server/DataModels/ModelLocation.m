//
//  ModelLocation.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelLocation.h"
#import "ModelStoreHour.h"
#import "NSObject+Extra.h"

@implementation ModelLocation

-(NSString*)hoursString{
    return @"Hours String";
}

-(NSString*)distanceString{
    return [NSString stringWithFormat:@"%.2f %@",self.distance,self.distanceUnit];
}

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"openHours"]){
        return [ModelStoreHour class];
    }
    
    return [super getJSONArrayElementDataType:propertyName];
}

@end
