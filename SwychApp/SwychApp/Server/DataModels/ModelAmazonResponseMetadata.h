//
//  ModelAmazonResponseMetadata.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonResponseMetadata : NSObject

@property(nonatomic,strong) NSString *requestId;
@end
