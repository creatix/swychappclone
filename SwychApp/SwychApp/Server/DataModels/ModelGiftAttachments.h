//
//  ModelGiftAttachments.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelGiftAttachments : CommunicationModelBase

@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *imgUrl;
@property(nonatomic,strong) NSString *videoUrl;

@end
