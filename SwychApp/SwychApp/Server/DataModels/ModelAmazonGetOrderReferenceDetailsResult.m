//
//  ModelAmazonGetOrderReferenceDetailsResult.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonGetOrderReferenceDetailsResult.h"

@implementation ModelAmazonGetOrderReferenceDetailsResult

-(NSString*)amazonAuthorizationId{
    return self.orderReferenceDetails == nil ? nil : self.orderReferenceDetails.amazonAuthorizationId;
}
@end
