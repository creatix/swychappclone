//
//  ModelSystemConfiguration.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@class ModelPaymentProcessor;
@class ModelApplePaySettings;

@interface ModelSystemConfiguration : CommunicationModelBase

@property(nonatomic,assign) NSInteger resourceVersion;
@property(nonatomic,assign) double passwrodValidationMinPurchaseAmount;
@property(nonatomic, assign) NSInteger maximumGiftCardSchedulingDays;
@property (nonatomic,strong) NSArray<ModelFilterOption*> *filterOptions;
@property (nonatomic,strong) NSString*refer_detail_url;
@property(nonatomic,assign) BOOL directCreditCardEnabled;
@property(nonatomic,assign) BOOL referFriendOnMenuEnabled;
@property(nonatomic,assign) BOOL amazonPayEnabled;

@property(nonatomic,strong) ModelPaymentProcessor *paymentProcessor;
@property(nonatomic,strong) ModelApplePaySettings *applePaySettings;
@end
