//
//  ModelUser.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelUser.h"

@implementation ModelUser

@synthesize phoneNumber = _phoneNumber;

-(void)setPhoneNumber:(NSString *)phoneNumber{
    _phoneNumber = [Utils phoneNumberWithAreaCode:phoneNumber];
}

-(NSString*)phoneNumber{
    return [Utils phoneNumberWithAreaCode:_phoneNumber];
}

-(NSString*)phoneNumberWithoutPossibleAreaCode{
    if (_phoneNumber!= nil && _phoneNumber.length == 11){
        if ([[_phoneNumber substringToIndex:0] isEqualToString:@"1"]){
            return [_phoneNumber substringFromIndex:1];
        }
    }
    return _phoneNumber;
}
-(BOOL)needUpdateInfoToContinuePurchase{
    return self.firstName.length == 0 ||
    self.lastName.length == 0 ||
    self.email.length == 0;
}

-(ModelUser*)duplicate{
    ModelUser *u = [[ModelUser alloc] init];
    
    u.swychId = self.swychId;
    u.firstName = self.firstName;
    u.lastName = self.lastName;
    u.email = self.email;
    u.phoneNumber = self.phoneNumber;
    u.userStatus = self.userStatus;
    u.createdTime = self.createdTime;
    u.password = self.password;
    u.encryptedPassword = self.encryptedPassword;
    u.swychBalance = self.swychBalance;
    u.swychPoint = self.swychPoint;
    u.swychPointValue = self.swychPointValue;
    u.regType = self.regType;
    u.referalCode = self.referalCode;
    u.referalSenderCredit = self.referalSenderCredit;
    u.referalLink = self.referalLink;
    u.referalRecipientCredit = self.referalRecipientCredit;
    u.botLinkingCode = self.botLinkingCode;
    return u;
}
@end
