//
//  ModelAmazonOrderReferenceDetail.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"
#import "ModelAmazonGetOrderReferenceDetailsResult.h"
#import "ModelAmazonResponseMetadata.h"

@interface ModelAmazonOrderReferenceDetail : CommunicationModelBase

@property(nonatomic,strong) ModelAmazonGetOrderReferenceDetailsResult *getOrderReferenceDetailsResult;
@property(nonatomic,strong) ModelAmazonResponseMetadata *responseMetadata;


@property(nonatomic,readonly) NSString *amazonAuthorizationId;

@end
