//
//  ModelAmazonPaymentInstrument.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonPaymentInstrument : NSObject

@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *accountNumberTail;

@end
