//
//  ModelAmazonOrderReferenceStatus.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonOrderReferenceStatus : NSObject

@property(nonatomic,strong) NSString *lastUpdateTimestamp;
@property(nonatomic,strong) NSString *reasonCode;
@property(nonatomic,strong) NSString *reasonDescription;
@property(nonatomic,strong) NSString *state;

@end
