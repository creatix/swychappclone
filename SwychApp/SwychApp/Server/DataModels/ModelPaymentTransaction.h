//
//  ModelPayment.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelPaymentTransaction : CommunicationModelBase
@property (nonatomic,assign) NSInteger paymentType;
@property (nonatomic,assign) double amount;
@property (nonatomic,assign) NSInteger paymentStatus;
@property (nonatomic,strong) NSString *paymentReferenceNumber;
@property (nonatomic,strong) NSString *paymentAccountNumber;
@end
