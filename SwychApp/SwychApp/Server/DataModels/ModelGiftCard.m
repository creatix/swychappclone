//
//  ModelGiftCard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelGiftCard.h"
#import "NSObject+Extra.h"
#import "ModelCategory.h"
#import "ModelFilterOption.h"
#import "ModelTransaction.h"
#import "NSString+Extra.h"


@implementation ModelGiftCard

@synthesize logoUrl = _logoUrl;
@synthesize retailer = _retailer;

-(NSString*)retailer{
    return _retailer;
}

-(void)setRetailer:(NSString *)retailer{
    _retailer = (retailer == nil ? nil : [retailer trim]);
}
-(void)setLogoUrl:(NSString *)logoUrl{
    _logoUrl = logoUrl;
}

-(NSString*)logoUrl{
    if (_logoUrl == nil){
        NSString* serverUrl = [[ApplicationConfigurations instance] getServerURL];
        NSString* logUrl = [NSString stringWithFormat:@"%@%@",serverUrl,@"/gift_card_images/brand_logo_%lld.png"];
        logUrl = [NSString stringWithFormat:logUrl,self.giftCardId];
        return logUrl;
//        return [NSString stringWithFormat:@"https://api.appcaps.com/swychapp_qa/gift_card_images/brand_logo_%lld.png",self.giftCardId];
    }
    return  _logoUrl;
}

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"categories"]){
        return [ModelCategory class];
    }
    else if ([propertyName isEqualToString:@"giftCardImageURL"] || [propertyName isEqualToString:@"availableValues"] ||
             [propertyName isEqualToString:@"tangoUTIDs"]){
        return [NSString class];
    }
    else if ([propertyName isEqualToString:@"filterOptions"]){
        return [ModelFilterOption class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"description"]){
        return @"giftcardDesc";
    }
    else if ([jsonKeyName isEqualToString:@"giftPIN"]){
        return @"giftCardPIN";
    }
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftcardDesc"]){
        return @"description";
    }
    else if ([propertyName isEqualToString:@"giftCardPIN"]){
        return @"giftPIN";
    }
    return [super getJSONKeyNameInProperties:propertyName];
}

-(BOOL)retailerNameLike:(NSString*)name{
    NSRange range = [[self.retailer lowercaseString] rangeOfString:[name lowercaseString]];
    
    return range.location != NSNotFound;
}
-(BOOL)categoriesNameLike:(NSString*)name{
    if(self.categories == nil|| self.categories.count == 0)
        return NO;
    for (ModelCategory* item in self.categories) {
         NSRange range = [[item.name lowercaseString] rangeOfString:[name lowercaseString]];
        if(range.location != NSNotFound) return YES;
        
    }
    return NO;
}

+(ModelGiftCard*)emptyGiftcard{
    ModelGiftCard *card = [[ModelGiftCard alloc] init];
    card.conversionRate = 1.0;
    card.isEmptyGiftcard = YES;
    return card;
}

-(ModelTransaction*)generateTempTransacion{
    ModelTransaction *tran = [[ModelTransaction alloc] init];
    tran.giftCard = self;
    tran.transactionId = self.transactionId;
    tran.parentTransactionId = self.parentTransactionId;
    tran.amount = self.amount;
    
    return tran;
}

-(NSString*)getGiftcardSerialNumberForWalletPass{
   // return [@(self.isPlasticCard ? self.plasticGiftCardId : self.transactionId) stringValue];
    return self.isPlasticCard ? [@(self.plasticGiftCardId) stringValue] : self.transactionId;
}

-(BOOL)hasFilterOptionById:(NSInteger)filterId{
    if(self.categories == nil || self.categories.count == 0){
        return NO;
    }
    else{
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelCategory *c, NSDictionary<NSString *,id> * _Nullable bindings) {
            return c.Id == filterId;
        }];
        NSArray *array = [self.categories filteredArrayUsingPredicate:pred];
        return [array count] > 0;
    }
}

-(BOOL)validateAmount:(double)amount showAlert:(BOOL)showAlert{
    if (self.availableValues != nil && self.availableValues.count > 0){
        BOOL valid = NO;
        for(NSString *str in self.availableValues){
            double val = [str doubleValue];
            if (fabs(val - amount) <= 0.01){
                valid = YES;
                break;
            }
        }
        if (valid){
            return YES;
        }
        else{
            NSString *str = nil;
            for(NSInteger i = 0; i < self.availableValues.count;i++){
                if (str == nil){
                    str = [self.availableValues objectAtIndex:i];
                }
                else{
                    str = [NSString stringWithFormat:@"%@,%@",str,[self.availableValues objectAtIndex:i]];
                }
            }
            if(self.minimumValue==0 && self.maximumValue == 0){
                if(showAlert){
                NSString *msg =[NSString stringWithFormat:NSLocalizedString(@"Error_InvalidAmountSelection", nil),str];
                [Utils showAlert:msg delegate:nil];
                }
                return NO;
            }
        }
    }
    BOOL valid = YES;
    if (self.minimumValue >= 0.0){
        if (amount < self.minimumValue){
            valid = NO;
        }
    }
    if (self.maximumValue >= 0.0){
        if (amount > self.maximumValue){
            valid = NO;
        }
    }
    if (valid){
        return YES;
    }
    if(showAlert){
    NSString *msg =[NSString stringWithFormat:NSLocalizedString(@"Error_InvalidAmountRange", nil),self.minimumValue,self.maximumValue];
    [Utils showAlert:msg delegate:nil];
    }
    return NO;
}

-(double)amountSwychTo:(double)originalAmount{
    double curstatusRange = -2;
    double curstatusValue = -2;
    if( self.maximumValue > 0){
        
        if (self.minimumValue >= 0.0){
            if (originalAmount < self.minimumValue){
                curstatusRange = self.minimumValue;
            }
        }
        if (self.maximumValue >= 0.0){
            if (originalAmount > self.maximumValue){
                curstatusRange = -1;
            }
        }
        if(curstatusRange == -2)
        curstatusRange = originalAmount;
    }
    if (self.availableValues != nil && self.availableValues.count > 0){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(NSString *valString, NSDictionary<NSString *,id> * _Nullable bindings) {
            double val = [valString doubleValue];
            return fabs(val - originalAmount) < 0.01;
        }];
        NSArray *array = [self.availableValues filteredArrayUsingPredicate:pred];
        if([array count] > 0){
            curstatusValue =  originalAmount;
        }
        else{
            NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(NSString *valString, NSDictionary<NSString *,id> * _Nullable bindings) {
                double val = [valString doubleValue];
                return val > originalAmount;
            }];
            NSArray *array = [self.availableValues filteredArrayUsingPredicate:pred];
            
            if ([array count] > 0){
                curstatusValue = [[array firstObject] doubleValue];
            }
            else{// if(self.minimumValue==0 && self.maximumValue == 0){
                curstatusValue = -1;
            }
        }
    }
    if(curstatusRange>0&&curstatusValue>0){
    return curstatusRange<=curstatusValue? curstatusRange:curstatusValue;
    }

    return curstatusRange<=curstatusValue? curstatusValue:curstatusRange;

   
}
@end
