//
//  ModelPaymentProcessor.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelPaymentProcessor : CommunicationModelBase

@property(nonatomic,assign) int paymentProcessorApplePay;
@property(nonatomic,assign) int paymentProcessorAndroidPay;
@property(nonatomic,assign) int paymentProcessorPaypal;

@end
