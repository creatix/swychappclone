//
//  ModelAmazonPhysicalDestination.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonPhysicalDestination : NSObject

@property(nonatomic,strong) NSString* addressLine1;
@property(nonatomic,strong) NSString* addressLine2;
@property(nonatomic,strong) NSString* addressLine3;
@property(nonatomic,strong) NSString* city;
@property(nonatomic,strong) NSString* countryCode;
@property(nonatomic,strong) NSString* county;
@property(nonatomic,strong) NSString* district;
@property(nonatomic,strong) NSString* name;
@property(nonatomic,strong) NSString* phone;
@property(nonatomic,strong) NSString* postalCode;
@property(nonatomic,strong) NSString* stateOrRegion;

@end
