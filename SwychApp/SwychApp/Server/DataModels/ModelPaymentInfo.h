//
//  ModelPaymentInfo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@class ModelSwychBillingInfo;

@interface ModelPaymentInfo : CommunicationModelBase

@property(nonatomic,assign) PaymentOption paymentType;
@property(nonatomic,assign) double amount;
@property(nonatomic,strong) NSString *paymentReferenceNumber;
@property(nonatomic,strong) NSString *paymentAccountNumber;
@property(nonatomic,assign) NSInteger paymentStatus;
@property(nonatomic,strong) NSDate *paymentDateTime;
@property(nonatomic,strong) NSString *amazonOrderReferenceId;
@property(nonatomic,strong) NSString *amazonAuthorizationId;
@property(nonatomic,assign) NSInteger paymentProcessor;
@property(nonatomic,strong) NSString *amazonToken;
@property(nonatomic,strong) ModelSwychBillingInfo *billingInfo;
@end
