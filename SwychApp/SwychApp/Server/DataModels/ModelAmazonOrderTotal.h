//
//  ModelAmazonOrderTotal.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonOrderTotal : NSObject

@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) NSString *currencyCode;
@end
