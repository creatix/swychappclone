//
//  ModelAmazonConstraints.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonConstraints.h"
#import "NSObject+Extra.h"
#import "ModelAmazonConstraint.h"



@implementation ModelAmazonConstraints
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"constraint"]){
        return [ModelAmazonConstraint class];
    }
    
    return [super getJSONArrayElementDataType:propertyName];
}
@end
