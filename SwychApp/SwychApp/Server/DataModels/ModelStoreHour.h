//
//  ModelStoreHour.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelStoreHour : CommunicationModelBase

@property(nonatomic,assign) NSInteger day;
@property(nonatomic,assign) NSString *period;
@end
