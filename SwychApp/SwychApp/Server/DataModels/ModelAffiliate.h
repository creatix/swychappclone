//
//  ModelAffiliate.h
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelAffiliate : CommunicationModelBase
@property(nonatomic,strong) NSString *affilateId;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *affiliateCode;
@property(nonatomic,strong) NSString *avatarURL;
@property(nonatomic,assign) BOOL isFriend;
@end
