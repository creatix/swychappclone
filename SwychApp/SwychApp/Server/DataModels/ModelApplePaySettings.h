//
//  ModelApplePaySettings.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelApplePaySettings : CommunicationModelBase

@property(nonatomic,strong) NSString *stripePublishableKeySandbox;
@property(nonatomic,strong) NSString *stripePublishableKeyLive;
@property(nonatomic,strong) NSString *merchantIdentifierLive;
@property(nonatomic,strong) NSString *merchantIdentifierSandbox;
@end
