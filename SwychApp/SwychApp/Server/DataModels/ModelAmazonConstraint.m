//
//  ModelAmazonConstraint.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonConstraint.h"
#import "NSObject+Extra.h"


@implementation ModelAmazonConstraint
//description

-(NSString *)getJSONKeyNameInProperties:(NSString *)propertyName{
    if ([propertyName caseInsensitiveCompare:@"constraintDesc"] == NSOrderedSame){
        return @"description";
    }
    return propertyName;
}

-(NSString *)getPropertyNameInJSONKeys:(NSString *)jsonKeyName{
    if ([jsonKeyName caseInsensitiveCompare:@"description"] == NSOrderedSame){
        return @"constraintDesc";
    }
    return jsonKeyName;

}
@end
