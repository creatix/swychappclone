//
//  ModelAmazonPaymentInstrument.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonPaymentInstrument.h"

@implementation ModelAmazonPaymentInstrument

-(id)init{
    self = [super init];
    if (self){
        self.name = NSLocalizedString(@"PaymentOption_AmazonDefaultCard",nil);
        self.accountNumberTail = nil;
    }
    return self;
}
@end
