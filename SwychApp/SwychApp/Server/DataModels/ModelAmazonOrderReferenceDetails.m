//
//  ModelAmazonOrderReferenceDetails.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonOrderReferenceDetails.h"




@implementation ModelAmazonOrderReferenceDetails


-(NSString*)amazonOrderReferenceId{
    return self.idList == nil ? nil : self.idList.amazonAuthorizationId;
}
@end
