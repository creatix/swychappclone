//
//  ModelCategory.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelCategory.h"
#import "NSObject+Extra.h"

@implementation ModelCategory

-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"id"]){
        return @"Id";
    }
    
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}

-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"Id"]){
        return @"id";
    }
    return [super getJSONKeyNameInProperties:propertyName];
}

@end
