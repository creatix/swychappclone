//
//  ModelAmazonGetOrderReferenceDetailsResult.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelAmazonOrderReferenceDetails.h"

@interface ModelAmazonGetOrderReferenceDetailsResult : NSObject

@property(nonatomic,strong) ModelAmazonOrderReferenceDetails *orderReferenceDetails;

@property(nonatomic,readonly) NSString *amazonAuthorizationId;
@end
