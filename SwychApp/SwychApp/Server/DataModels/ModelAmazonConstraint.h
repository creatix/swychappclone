//
//  ModelAmazonConstraint.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonConstraint : NSObject
@property(nonatomic,strong) NSString *constraintID;
@property(nonatomic,strong) NSString *constraintDesc;
@end
