//
//  ModelAmazonAddress.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelAmazonDestination.h"

@interface ModelAmazonAddress : NSObject
@property(nonatomic,strong) NSString *addressType;
@property(nonatomic,strong) ModelAmazonDestination *physicalAddress;

@end
