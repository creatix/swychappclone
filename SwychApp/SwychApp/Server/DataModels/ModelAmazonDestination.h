//
//  ModelAmazonDestination.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelAmazonPhysicalDestination.h"


@interface ModelAmazonDestination : NSObject

@property(nonatomic,strong) NSString *destinationType;
@property(nonatomic,strong) ModelAmazonPhysicalDestination *physicalDestination;

@end
