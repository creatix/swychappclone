//
//  ModelRedeemOptionGiftCard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelRedeemOptionGiftCard : NSObject

@property(nonatomic,assign) double amount;
@property(nonatomic,strong) NSString* giftCardId;

@end
