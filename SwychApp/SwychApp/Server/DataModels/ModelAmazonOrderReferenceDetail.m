//
//  ModelAmazonOrderDetail.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonOrderReferenceDetail.h"

@implementation ModelAmazonOrderReferenceDetail


-(NSString*)amazonAuthorizationId{
    return self.getOrderReferenceDetailsResult == nil ? nil : self.getOrderReferenceDetailsResult.amazonAuthorizationId;
}
@end
