//
//  ModelAmazonConstraints.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ModelAmazonConstraint;
@interface ModelAmazonConstraints : NSObject
@property(nonatomic,strong) NSArray<ModelAmazonConstraint*> *constraint;
@end
