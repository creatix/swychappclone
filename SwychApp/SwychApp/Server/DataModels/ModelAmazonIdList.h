//
//  ModelAmazonIdList.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonIdList : NSObject

@property(nonatomic,strong) NSArray<NSString*> *member;

@property(nonatomic,readonly) NSString *amazonAuthorizationId;

@end
