//
//  ModelPaymentProcessorData.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelPaymentProcessorData : NSObject

@property(nonatomic,strong) NSString *fingerPrint;
@property(nonatomic,assign) long sequenceNumber;
@property(nonatomic,assign) long timeStamp;
@property(nonatomic,strong) NSString *apiLoginID;
@property(nonatomic,assign) Boolean isProduction;
@property(nonatomic,strong) NSString *accessToken;

@end
