//
//  ModelBotTransaction.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"
#import "ModelGiftCard.h"
@interface ModelBotTransaction : CommunicationModelBase
@property(nonatomic,strong) NSString *botTransactionId;
@property(nonatomic,strong) NSString *botAccountNumber;
@property(nonatomic,strong) NSString *senderSwychId;
@property(nonatomic,strong) NSString *recipientSwychId;
@property(nonatomic,strong) NSString *recipientPhoneNumber;
@property(nonatomic,strong) NSString *recipientEmail;
@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) ModelGiftCard *giftCard;
@property(nonatomic,strong) NSString *message;
@end
