//
//  ModelAmazonBuyer.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonBuyer : NSObject

@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *phone;

@end
