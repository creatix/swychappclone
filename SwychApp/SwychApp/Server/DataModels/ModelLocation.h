//
//  ModelLocation.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@class ModelStoreHour;

@interface ModelLocation : CommunicationModelBase

@property(nonatomic,strong) NSString *merchantName;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *distanceUnit;
@property(nonatomic,assign) double latitude;
@property(nonatomic,assign) double longitude;
@property(nonatomic,assign) double distance;
@property(nonatomic,strong) NSArray<ModelStoreHour*> *openHours;

@property(nonatomic,readonly) NSString *hoursString;
@property(nonatomic,readonly) NSString *distanceString;
@end
