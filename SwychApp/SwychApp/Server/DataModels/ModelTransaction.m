//
//  ModalTransaction.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-01.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelTransaction.h"
#import "NSObject+Extra.h"
#import "ModelUser.h"



@implementation ModelTransaction

-(BOOL)isPlasticCard{
    return self.giftCard == nil ? NO : self.giftCard.plasticGiftCardId > 0;
}

-(long long)plasticCardId{
    return self.giftCard == nil ? 0 : self.giftCard.plasticGiftCardId;
}
-(id)initWithGiftCard:(ModelGiftCard*)giftCard{
    self = [super init];
    if(self){
        self.giftCard = giftCard;
    }
    return self;
}

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"payments"]||[propertyName isEqualToString:@"payments2"]){
        return [ModelPaymentInfo class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

-(NSString*)getPropertyNameInJSONKeys:(NSString *)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"sentMesage"]){
        return @"sentMessage";
    }
    else if([jsonKeyName isEqualToString:@"transactionId"]){
        return @"transactionId";
    }
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}

-(NSString*)getJSONKeyNameInProperties:(NSString *)propertyName{
    if ([propertyName isEqualToString:@"sentMessage"]){
        return @"sentMesage";
    }
    else if([propertyName isEqualToString:@"transactionId"]){
        return @"transactionId";
    }

    return [super getJSONKeyNameInProperties:propertyName];
}
@end
