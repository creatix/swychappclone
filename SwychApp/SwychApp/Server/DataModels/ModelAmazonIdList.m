//
//  ModelAmazonIdList.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAmazonIdList.h"
#import "NSObject+Extra.h"



@implementation ModelAmazonIdList

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"member"]){
        return [NSString class];
    }
    
    return [super getJSONArrayElementDataType:propertyName];
}



-(NSString*)amazonAuthorizationId{
    return (self.member == nil || [self.member count] == 0) ? nil : self.member.firstObject;
}
@end
