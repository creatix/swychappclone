//
//  ModelUser.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelUser : CommunicationModelBase

@property(nonatomic,strong) NSString *swychId;
@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *phoneNumber;
@property(nonatomic,assign) NSInteger userStatus;
@property(nonatomic,strong) NSDate *createdTime;
@property(nonatomic,strong) NSString *password;
@property(nonatomic,strong) NSString *encryptedPassword;
@property(nonatomic,assign) double swychBalance;
@property(nonatomic,assign) double swychPoint;
@property(nonatomic,assign) double swychPointValue;
@property(nonatomic,assign) NSInteger regType;
@property(nonatomic,strong) NSString *avatar;
@property(nonatomic,strong) NSString *referalCode;
@property(nonatomic,strong) NSString *referalLink;
@property(nonatomic,strong) NSString *referalSenderCredit;
@property(nonatomic,strong) NSString *referalRecipientCredit;
@property(nonatomic,strong) NSString *botLinkingCode;
-(BOOL)needUpdateInfoToContinuePurchase;
-(ModelUser*)duplicate;
@end
