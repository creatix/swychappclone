//
//  ModelPullRequests.h
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelPullRequests : CommunicationModelBase
@property(nonatomic,assign) BOOL botLinking;
@property(nonatomic,assign) BOOL botSendGiftcard;
@end
