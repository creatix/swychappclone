//
//  ModelFilterOption.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelFilterOption : CommunicationModelBase

@property (nonatomic,assign) long long optionId;
@property (nonatomic,strong) NSString *name;
@end
