//
//  ModelCategory.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommunicationModelBase.h"


@interface ModelCategory : CommunicationModelBase

@property (nonatomic,assign) long long Id;
@property (nonatomic,strong) NSString *name;
@end
