//
//  ModelGiftCard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommunicationModelBase.h"
#define DISABLE 0
#define ENAABLE 1
#define OUTOFSTOCK 2

@class ModelCategory;
@class ModelFilterOption;
@class ModelTransaction;

@interface ModelGiftCard : CommunicationModelBase

@property (nonatomic,assign) long long giftCardId;
@property (nonatomic,assign) long long giftCardUid;
@property (nonatomic,strong) NSString *giftCardNumber;
@property (nonatomic,strong) NSString *giftCardPIN;
@property (nonatomic,strong) NSString *retailer;
@property (nonatomic,strong) NSArray<ModelCategory*> *categories;
@property (nonatomic,strong) NSString *barcodeUrl;
@property (nonatomic,strong) NSString *giftCardUrl;
@property (nonatomic,assign) double amount;
@property (nonatomic,strong) NSString *giftcardDesc;
@property (nonatomic,strong) NSArray<NSString*> *giftCardImageURL;
@property (nonatomic,strong) NSArray<NSString*> *availableValues;
@property (nonatomic,assign) BOOL customizedValueAvailable;
@property (nonatomic,assign) double minimumValue;
@property (nonatomic,assign) double maximumValue;
@property (nonatomic,assign) BOOL favourite;
@property (nonatomic,assign) BOOL isRecommended;
@property (nonatomic,assign) NSInteger trending;
@property (nonatomic,strong) NSDate *created;
@property (nonatomic,strong) NSDate *updated;
@property (nonatomic,assign) NSInteger redeemType;
@property (nonatomic,strong) NSString *termsURL;
@property (nonatomic,strong) NSString* transactionId;
@property (nonatomic,strong) NSString* parentTransactionId;
@property (nonatomic,assign) double conversionRate;
@property (nonatomic,assign) BOOL isPlasticCard;
@property (nonatomic,assign) long long plasticGiftCardId;
@property (nonatomic,assign) double selfGiftingDiscount;
@property (nonatomic,assign) BOOL canAddToWallet;

@property (nonatomic,strong) NSString* logoUrl;

@property (nonatomic,assign) BOOL isEmptyGiftcard;
@property (nonatomic,assign) BOOL isSenderSelected;
@property (nonatomic,strong) NSString *displayName;
@property (nonatomic,assign) NSInteger availability;

@property (nonatomic,assign) NSInteger source;
@property (nonatomic,strong) NSArray<NSString*> *tangoUTIDs;
@property (nonatomic,strong) NSString *disclaimerURL;

+(ModelGiftCard*)emptyGiftcard;
-(NSString*)getGiftcardSerialNumberForWalletPass;

-(ModelTransaction*)generateTempTransacion;
-(BOOL)retailerNameLike:(NSString*)name;
-(BOOL)categoriesNameLike:(NSString*)name;
-(BOOL)hasFilterOptionById:(NSInteger)filterId;

-(BOOL)validateAmount:(double)amount showAlert:(BOOL)showAlert;
-(double)amountSwychTo:(double)originalAmount;
@end
