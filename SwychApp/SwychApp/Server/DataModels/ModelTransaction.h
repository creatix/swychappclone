//
//  ModalTransaction.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-01.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"
#import "ModelGiftCard.h"
#import "ModelPaymentInfo.h"


@class ModelUser;
@class ModelGiftAttachments;
@class ModelGiftCard;


@interface ModelTransaction : CommunicationModelBase
@property (nonatomic,strong) NSString* transactionId;
@property (nonatomic,strong) NSString* parentTransactionId;
@property (nonatomic,strong) NSDate *transactionDate;
@property (nonatomic,strong) NSString *redeemOrSaveDate;

@property (nonatomic,assign) NSInteger transactionType;

//@property (nonatomic,strong) NSString *transactionType;

@property (nonatomic,assign) double amount;
@property (nonatomic,strong) NSString *recipientPhoneNumber;
@property (nonatomic,strong) NSString *recipientSwychId;
@property (nonatomic,strong) NSString *recipientName;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,strong) NSString *transactionDescription;
@property (nonatomic,strong) ModelGiftCard *giftCard;
@property (nonatomic,strong) ModelUser *sender;

@property (nonatomic,strong) NSArray<id> *redeemOptionCash;
@property (nonatomic,strong) NSArray<id> *redeemOptionMerchant;
@property (nonatomic,strong) NSArray<id> *redeemOptionGiftCard;
@property (nonatomic,strong) NSArray<ModelPaymentInfo*> *payments;
@property (nonatomic,strong) NSArray<ModelPaymentInfo*> *payments2;// no fund
@property (nonatomic,strong) ModelGiftAttachments *sentMessage;

@property (nonatomic,readonly) BOOL isPlasticCard;
@property (nonatomic,readonly) long long plasticCardId;

-(id)initWithGiftCard:(ModelGiftCard*)giftCard;
@end
