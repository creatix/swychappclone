//
//  RequestActivation.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestActivation.h"
#import "ResponseActivation.h"


#define HTTP_VERB   @"/api/v1/users"

@implementation RequestActivation

@synthesize oneTimePIN,firstName,lastName,email;


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_PUT;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(ResponseBase*)getResponse{
    return [[ResponseActivation alloc] init];
}


-(NSDictionary*)getExtraHeaders{
    return nil;
}

@end
