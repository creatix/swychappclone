//
//  RequestAuthentication.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestAuthentication.h"
#import "ResponseAuthentication.h"

#define HTTP_VERB       @"/api/v1/authentication"

@implementation RequestAuthentication

@synthesize phoneNumber = _phoneNumber;

-(void)setPhoneNumber:(NSString *)phoneNumber{
    _phoneNumber = [Utils phoneNumberWithAreaCode:phoneNumber];
}

-(NSString*)phoneNumber{
    return [Utils phoneNumberWithAreaCode:_phoneNumber];
}

-(NSString*)phoneNumberWithoutPossibleAreaCode{
    if (_phoneNumber!= nil && _phoneNumber.length == 11){
        if ([[_phoneNumber substringToIndex:0] isEqualToString:@"1"]){
            return [_phoneNumber substringFromIndex:1];
        }
    }
    return _phoneNumber;
}

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

-(NSDictionary*)getExtraHeaders{
    return nil;
}
@end
