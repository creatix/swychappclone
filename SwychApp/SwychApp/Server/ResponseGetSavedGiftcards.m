//
//  ResponseGetSavedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetSavedGiftcards.h"
#import "ModelGiftCard.h"
#import "NSObject+Extra.h"
#import "ModelTransaction.h"


@implementation ResponseGetSavedGiftcards

-(double)totalAmount{
    if (self.savedGiftcardsTransaction == nil || self.savedGiftcardsTransaction.count == 0){
        return 0.0;
    }
    NSNumber *num = [self.savedGiftcardsTransaction valueForKeyPath:@"@sum.amount"];
    return [num doubleValue];
}

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"savedGiftcardsTransaction"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
