//
//  RequestBotLinkingLists.m
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBotLinkingLists.h"
#import "ResponseBotLinkingLists.h"
#define HTTP_VERB   @"/api/v1/bot/linkInfo"
@implementation RequestBotLinkingLists
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
-(ResponseBase*)getResponse{
    return [[ResponseBotLinkingLists alloc] init];
}
@end
