//
//  ResponseGetArchivedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"
#import "ModelTransaction.h"
@class  ModelGiftCard;

@interface ResponseGetArchivedGiftcards : ResponseBase

@property (nonatomic,strong) NSArray<ModelTransaction *> *giftCards;

@end
