//
//  RequestGetHistory.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetHistory.h"
#define HTTP_VERB       @"/api/v1/history"
@implementation RequestGetHistory
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
