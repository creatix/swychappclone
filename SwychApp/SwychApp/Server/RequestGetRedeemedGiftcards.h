//
//  RequestGetRedeemedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestGetRedeemedGiftcards : RequestBase

@property (nonatomic,strong) NSString* transactinId;

@end
