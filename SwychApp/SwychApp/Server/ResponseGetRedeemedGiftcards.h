//
//  ResponseGetRedeemedGiftcards.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelTransaction;

@interface ResponseGetRedeemedGiftcards : ResponseBase

@property (nonatomic,strong) NSArray<ModelTransaction *> *redeemedGiftcards;

@end
