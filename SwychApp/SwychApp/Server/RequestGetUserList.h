//
//  RequestGetUserList.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@class ModelUser;

@interface RequestGetUserList : RequestBase

@property(nonatomic,strong) NSArray<ModelUser*> *users;
@end
