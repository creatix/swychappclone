//
//  RequestManuallyUpdateBalance.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-24.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestManuallyUpdateBalance : RequestBase
@property (nonatomic,strong) NSString *giftcardNumber;
@property (nonatomic) double balance;
@end
