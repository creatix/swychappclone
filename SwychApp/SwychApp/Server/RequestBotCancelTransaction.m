//
//  RequestBotCancelTransaction.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBotCancelTransaction.h"

#define HTTP_VERB       @"/api/v1/bot/botTransaction/cancel"
@implementation RequestBotCancelTransaction
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_DELETE;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
