//
//  ResponseUpdateGiftcardBalance.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseUpdateGiftcardBalance : ResponseBase

@property(nonatomic,assign) double balance;

@end
