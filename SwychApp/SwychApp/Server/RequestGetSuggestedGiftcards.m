//
//  RequestGetSuggestedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetSuggestedGiftcards.h"

#define HTTP_VERB       @"/api/v1/giftcards/suggested?purchaseType=%d"

@implementation RequestGetSuggestedGiftcards

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.purchaseType];
}


@end
