//
//  RequestGetGiftcardsByRetailer.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetGiftcardsByRetailer.h"
#import "LocalUserInfo.h"

#define HTTP_VERB       @"/api/v1/giftcards/retailer?purchaseType=%d"

@implementation RequestGetGiftcardsByRetailer


-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return [NSString stringWithFormat:HTTP_VERB,self.purchaseType];
}


@end
