//
//  RequestArchiveGiftcard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestArchiveGiftcard.h"

#define HTTP_VERB       @"/api/v1/giftcards/archive"

@implementation RequestArchiveGiftcard

-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}

@end
