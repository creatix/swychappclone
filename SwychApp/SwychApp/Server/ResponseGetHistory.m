//
//  ResponseGetHistory.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetHistory.h"
#import "NSObject+Extra.h"
#import "ModelFilterOption.h"
@implementation ResponseGetHistory
-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"transactions"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
