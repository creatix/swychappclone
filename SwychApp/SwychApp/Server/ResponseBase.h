//
//  ResponseBase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"
#import "ModelPullRequests.h"
@interface ResponseBase : CommunicationModelBase

@property(nonatomic,assign) NSInteger errorCode;
@property(nonatomic,strong) NSString *errorMessage;
@property(nonatomic,assign) BOOL success;
@property(nonatomic,strong) ModelPullRequests* pullRequests;
@end
