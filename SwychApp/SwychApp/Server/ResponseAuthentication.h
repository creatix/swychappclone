//
//  ResponseAuthentication.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@class ModelUser;
@class ModelSystemConfiguration;

@interface ResponseAuthentication : ResponseBase

@property(nonatomic,strong) ModelUser *user;
@property(nonatomic,strong) NSString *token;
@property(nonatomic,strong) ModelSystemConfiguration *systemConfigurations;
@end
