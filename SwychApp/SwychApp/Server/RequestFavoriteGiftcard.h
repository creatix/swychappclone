//
//  RequestFavoriteGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestFavoriteGiftcard : RequestBase

@property(nonatomic,assign) NSInteger giftCardId;

@end
