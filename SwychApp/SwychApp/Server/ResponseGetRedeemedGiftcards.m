//
//  ResponseGetRedeemedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetRedeemedGiftcards.h"
#import "NSObject+Extra.h"
#import "ModelTransaction.h"

@implementation ResponseGetRedeemedGiftcards

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"redeemedGiftcards"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
