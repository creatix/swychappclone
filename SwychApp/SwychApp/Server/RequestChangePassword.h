//
//  RequestChangePassword.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestChangePassword : RequestBase

@property(nonatomic,strong) NSString *oldPassword;

@end
