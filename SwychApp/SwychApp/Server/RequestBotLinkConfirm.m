//
//  RequestBotLinkConfirm.m
//  SwychApp
//
//  Created by kndev3 on 10/7/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBotLinkConfirm.h"
#define HTTP_VERB       @"/api/v1/bot/linkConfirm"
@implementation RequestBotLinkConfirm
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_POST;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
