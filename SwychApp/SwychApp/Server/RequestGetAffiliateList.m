//
//  RequestGetAffiliateList.m
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestGetAffiliateList.h"
#define HTTP_VERB       @"/api/v1/affiliate"
@implementation RequestGetAffiliateList
-(NSString*)getHTTPMethod{
    return HTTP_METHOD_GET;
}

-(NSString*)getVerb{
    return HTTP_VERB;
}
@end
