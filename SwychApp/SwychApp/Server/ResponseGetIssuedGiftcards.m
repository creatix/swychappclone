//
//  ResponseGetIssuedGiftcards.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseGetIssuedGiftcards.h"
#import "ModelGiftCard.h"
#import "ModelTransaction.h"
#import "NSObject+Extra.h"

@implementation ResponseGetIssuedGiftcards

-(Class)getJSONArrayElementDataType:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"giftCards"]){
        return [ModelGiftCard class];
    }
    else if ([propertyName isEqualToString:@"issuedGiftcardsTransaction"]){
        return [ModelTransaction class];
    }
    return [super getJSONArrayElementDataType:propertyName];
}

@end
