//
//  RequestVerifyOTP.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RequestBase.h"

@interface RequestVerifyOTP : RequestBase

@property(nonatomic,strong) NSString *otp;
@property(nonatomic,strong) NSString *phoneNumber;
@property(nonatomic,strong) NSString *otpSentType;
@property(nonatomic,strong) NSString *email;

@end
