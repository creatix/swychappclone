//
//  ResponseUploadPlasticGiftcard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseUploadPlasticGiftcard : ResponseBase

@property(nonatomic,assign) long long plasticGiftCardId;
@end
