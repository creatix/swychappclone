//
//  ResponseUpdateAffiliate.h
//  SwychApp
//
//  Created by kndev3 on 9/14/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ResponseBase.h"

@interface ResponseUpdateAffiliate : ResponseBase
@property (nonatomic,strong) NSString *afflicateMessage;
@end
