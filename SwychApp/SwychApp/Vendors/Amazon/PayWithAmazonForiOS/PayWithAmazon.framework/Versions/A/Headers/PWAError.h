
#import <Foundation/Foundation.h>

/**
 * Enum values for errors from Pay With Amazon SDK
 **/
typedef NS_ENUM(NSInteger, PWAError)
{
    PWA_ERROR,
    NETWORK_UNAVAILABLE,
    SYSTEM_ERROR
};
