/**
 * Enum values for supported PWA regions
 */

typedef NS_ENUM(NSInteger, PWARegion)
{
    NONE = 0,
    NA = 1,
    EU = 2,
    FE = 3,
};
