#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>

@interface PWAProcessPaymentResponse : NSObject <PWAWorkflowResponse>

@property (copy, nonatomic, readonly) NSString *amazonOrderReferenceId;

@end
