#import <Foundation/Foundation.h>
#import <LoginWithAmazon/LoginWithAmazon.h>

@interface PWAPaymentScope : NSObject

+ (id<AMZNScope>)initiate;
+ (id<AMZNScope>)instrument;
+ (id<AMZNScope>)shippingAddress;
+ (id<AMZNScope>)billingAddress;

@end
