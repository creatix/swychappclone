//
//  UIRoundImageView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIRoundImageView.h"

@implementation UIRoundImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat radius = self.bounds.size.width / 2.0;
    self.cornerRadius = radius;
}

@end
