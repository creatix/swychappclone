//
//  UIGiftCardView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIGiftcardView.h"
#import "UIImageView+WebCache.h"
#import "ModelGiftCard.h"
#import "MessageWithWebViewPopView.h"
#import "ResponseBase.h"
#import "RequestFavoriteGiftcard.h"
#import "ResponseFavoriteGiftcard.h"
#import "TTTAttributedLabel.h"
#import "UILabelExt.h"
#import "NSString+Extra.h"

#import "Utils.h"
#define VIEW_FRONT      0
#define VIEW_BACK       1

#define Height_DiscountLable    20.0f

@interface UIGiftcardView(){
    NSInteger currentView;
    
}
@property(nonatomic,strong) NSArray *views;


//Font
@property(strong,nonatomic) IBOutlet UIView *vFront;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLabeName;
@property (weak, nonatomic) IBOutlet UIImageView *ivFrontGiftcard;
@property (weak, nonatomic) IBOutlet UILabel *labFrontGiftcardName;
@property (weak, nonatomic) IBOutlet UIImageView *ivFavorite;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labSelfGiftingDiscount;
//Back with logo
@property(strong,nonatomic) IBOutlet UIView *vBackWithLogo;
@property (weak, nonatomic) IBOutlet UILabel *labCalimTitle;
@property (weak, nonatomic) IBOutlet UILabel *labCalimCode;
@property (weak, nonatomic) IBOutlet UIImageView *vLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
//Back
@property(strong,nonatomic) IBOutlet UIView *vBack;
@property (weak, nonatomic) IBOutlet UILabel *labBackGiftCardName;
@property (weak, nonatomic) IBOutlet UILabelExt *labBackGiftCardNumber;
@property (weak, nonatomic) IBOutlet UIView *vBackSeparator;
@property (weak, nonatomic) IBOutlet UILabel *labBackPIN;
@property (weak, nonatomic) IBOutlet UIImageView *ivBackBarcode;

//Empty Swych card
@property (weak, nonatomic) IBOutlet UIView *vEmptyCard;
@property (weak, nonatomic) IBOutlet UIImageView *ivEmptyCardGiftcardImageView;
@property (weak, nonatomic) IBOutlet UIView *vTextDiscountView;
@property (weak, nonatomic) IBOutlet UILabel *labFirstPurchase;
@property (weak, nonatomic) IBOutlet UILabel *labDollarOff;

@property (weak, nonatomic) IBOutlet UIView *vTextCanStoreView;
@property (weak, nonatomic) IBOutlet UILabel *labCanStoreGiftcard;
@property (weak, nonatomic) IBOutlet UILabel *labDidYouKnow;
@property (weak, nonatomic) IBOutlet UILabel *labAddCardNow;

@property (weak, nonatomic) IBOutlet UILabel *labEmptyCardMore;

//Single Giftcard image
@property (weak, nonatomic) IBOutlet UIView *vSingleGiftcardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ivGiftcardOnSingleGiftcardView;


//Constraints for top views
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewEmptyCardHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewEmptyCardWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewSingleGiftcardHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewSingleGiftcardWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewBackHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewBackWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewFrontHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewFrontWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constFooterBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constDiscountFooterHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constFooterBottomHeight;

@end
@implementation UIGiftcardView

@synthesize isFavorite = _isFavorite;
@synthesize giftcard = _giftcard;
@synthesize giftcardImage = _giftcardImage;
@synthesize frontOnly = _frontOnly;
@synthesize frontViewAsCurrent = _frontViewAsCurrent;
@synthesize viewType = _viewType;

-(void)setupRoundCorner{
    self.views = [NSArray arrayWithObjects:_vFront,_vBack,_vEmptyCard, _vSingleGiftcardImageView,_vBackWithLogo, nil];
    for(UIView *v in self.views){
        v.layer.cornerRadius = 8.0f;
        v.layer.masksToBounds = NO;
        v.clipsToBounds = YES;
    }
}
-(GiftcardViewType)viewType{
    return _viewType;
}

-(void)setViewType:(GiftcardViewType)viewType{
    _viewType = viewType;
    switch (viewType) {
        case GiftcardView_Normal_bigger_bottom:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = YES;
            self.vFront.hidden = NO;
            self.vBack.hidden = NO;
            self.constDiscountFooterHeight.constant = 0;
            self.constFooterBottom.constant = 0;
            self.constFooterBottomHeight.constant = 40.0f;
            break;
        case GiftcardView_Normal:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = YES;
            self.vFront.hidden = NO;
            self.vBack.hidden = NO;
            self.constDiscountFooterHeight.constant = 0;
            self.constFooterBottom.constant = 0;

            break;
        case GiftcardView_Empty_Discount:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = NO;
            self.ivEmptyCardGiftcardImageView.image = [UIImage imageNamed:@"cards_swychable.png"];
            self.vFront.hidden = YES;
            self.vBack.hidden = YES;
            self.vTextDiscountView.hidden = YES;
            self.vTextCanStoreView.hidden = YES;
            self.labEmptyCardMore.hidden = YES;
            break;
        case GiftcardView_Empty_StoreCard:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = NO;
            self.ivEmptyCardGiftcardImageView.image = [UIImage imageNamed:@"cards_mycards.png"];
            self.vFront.hidden = YES;
            self.vBack.hidden = YES;
            self.vTextDiscountView.hidden = YES;
            self.vTextCanStoreView.hidden = YES;
            self.labEmptyCardMore.hidden = YES;
            break;
        case GiftcardView_Empty_More:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = NO;
            self.vFront.hidden = YES;
            self.vBack.hidden = YES;
            self.vTextDiscountView.hidden = YES;
            self.vTextCanStoreView.hidden = YES;
            self.labEmptyCardMore.hidden = NO;
            break;
        case GiftcardView_GiftcardImageOnly:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = NO;
            self.vEmptyCard.hidden = YES;
            self.vFront.hidden = YES;
            self.vBack.hidden = YES;
            self.vTextDiscountView.hidden = YES;
            self.vTextCanStoreView.hidden = YES;
            self.labEmptyCardMore.hidden = YES;
            break;
        case GiftcardView_NormalWithDiscount:
            self.vBackWithLogo.hidden = YES;
            self.vSingleGiftcardImageView.hidden = YES;
            self.vEmptyCard.hidden = YES;
            self.vFront.hidden = NO;
            self.vBack.hidden = NO;
            self.constDiscountFooterHeight.constant = Height_DiscountLable;
            self.constFooterBottom.constant = Height_DiscountLable;
            break;
        default:
            break;
    }
}
- (IBAction)buttonTapped:(id)sender{
    if(sender == _btnLeft){
        NSURL *amazUrl = [[NSURL alloc] initWithString: @"http://www.amazon.com"];
        [[UIApplication sharedApplication] openURL:amazUrl];//NSURL(string: "https://google.com")!)
    }
    else{
    MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
        [cppv AddUrl:@"amazonredeem"];
    [cppv show];
    }
}
-(BOOL)frontViewAsCurrent{
    return _frontViewAsCurrent;
}
-(void)setFrontViewAsCurrent:(BOOL)frontViewAsCurrent{
    _frontViewAsCurrent = frontViewAsCurrent;
    if (self.vFront != nil && self.vBack != nil){
        if (frontViewAsCurrent){
            currentView = VIEW_FRONT;
            [self bringSubviewToFront:self.vFront];
        }
        else{
            currentView = VIEW_BACK;
            [self bringSubviewToFront:self.vBack];
        }
    }
}

-(ModelGiftCard *)giftcard{
    return _giftcard;
}

-(void)setGiftcard:(ModelGiftCard *)giftcard{
    _giftcard = giftcard;
    BOOL containsString = [Utils CheckIfAmazon:_giftcard];//[[_giftcard.retailer lowercaseString] containsString:@"amazon"];
    if(containsString){
        _ifShowBackViewWithLogo = YES;
        _labCalimCode.text = _giftcard.giftCardNumber;
        [_vLogo sd_setImageWithURL:[NSURL URLWithString:_giftcard.logoUrl]];
        
    }
    if (_giftcard.isEmptyGiftcard){
        self.viewType = GiftcardView_Empty_More;
    }
    else{
        if (giftcard.giftCardImageURL != nil && [giftcard.giftCardImageURL count] > 0){
            [self.ivFrontGiftcard sd_setImageWithURL:[NSURL URLWithString:[giftcard.giftCardImageURL objectAtIndex:0]]];
            [self.ivGiftcardOnSingleGiftcardView sd_setImageWithURL:[NSURL URLWithString:[giftcard.giftCardImageURL objectAtIndex:0]]];
            if(self.viewType == GiftcardView_Empty_More){
                self.viewType = GiftcardView_Normal;
            }
        }
        
        self.labFrontGiftcardName.text = giftcard.retailer;
        self.labBackGiftCardName.text = giftcard.retailer;
        
        if (giftcard.favourite){
            self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_on.png"];
        }
        else{
            self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_off.png"];
        }
        if (giftcard.giftCardPIN != nil && ![giftcard.giftCardPIN isEqualToString:@""]){
            self.labBackPIN.text = [NSString stringWithFormat:NSLocalizedString(@"GiftcardPINNumber", nil),giftcard.giftCardPIN];
        }
        else{
            self.labBackPIN.text = nil;
        }
        if (giftcard.amount > 0.0){
            self.labFrontViewAmount.text = [NSString stringWithFormat:FORMAT_MONEY_INTEGER_ONLY,giftcard.amount];
            self.constraintLabFrontViewAmountWidth.constant = 50.0f;
        }
        else{
            self.labFrontViewAmount.text = nil;
            self.constraintLabFrontViewAmountWidth.constant = 0.0f;
        }
        if([self giftCardNumberIsLink]){
            self.labBackGiftCardNumber.delegate = (id<UILabelExtDelegate>)self;
            self.labBackGiftCardNumber.underlineText = YES;
            self.labBackGiftCardNumber.text = NSLocalizedString(@"ClickHereToView", nil);
            self.labBackPIN.text = giftcard.giftCardPIN;
        }
        else{
            self.labBackGiftCardNumber.delegate = nil;
            self.labBackGiftCardNumber.underlineText = NO;
            self.labBackGiftCardNumber.text = [NSString stringWithFormat:@"#%@",giftcard.giftCardNumber];
        }
        
        [self.ivBackBarcode sd_setImageWithURL:[NSURL URLWithString:giftcard.barcodeUrl]];
        
        if (self.viewType == GiftcardView_NormalWithDiscount && giftcard.selfGiftingDiscount > 0.0){
            self.labSelfGiftingDiscount.text = [NSString stringWithFormat:NSLocalizedString(@"SelfGiftingDiscount", nil),(int)(giftcard.selfGiftingDiscount * 100)];
        }
        else{
            self.constDiscountFooterHeight.constant = 0;
            self.constFooterBottom.constant = 0;
        }
    }
}

-(UIImage *)giftcardImage{
    return _giftcardImage;
}

-(void)setGiftcardImage:(UIImage *)giftcardImage{
    _giftcardImage = giftcardImage;
    self.ivFrontGiftcard.image = _giftcardImage;
}
-(BOOL)isFavorite{
    return _isFavorite;
}

-(void)setIsFavorite:(BOOL)isFavorite{
    _isFavorite = isFavorite;
    
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UIGiftCardView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.delegate = self;
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
    
    
    self.vBackSeparator.backgroundColor = [UIColor clearColor];
    self.labBackGiftCardName.adjustsFontSizeToFitWidth = YES;
    
    self.frontOnly = YES;
    self.frontViewAsCurrent = YES;
    
    self.isFavorite = NO;
    self.labFrontGiftcardName.text = self.giftcardName;
    self.labBackGiftCardName.text = self.giftcardName;
    
    self.viewType = GiftcardView_Normal;
    
    [self setupRoundCorner];
    
    [self setupViewsDimension];
    
    [Utils addNotificationObserver:self selector:@selector(giftcardFavoriteNotificationHandler:) name:NOTI_Giftcard_Favorite object:nil];
}

-(void)setupViewsDimension{
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    
    if (self.viewType == GiftcardView_Normal){
        w = (300.0/190.0)*(h - 20.0);
    }
    else if (self.viewType == GiftcardView_NormalWithDiscount){
        w = (300.0/190.0)*(h - 20.0 - 20.0);
    }
    else if (self.viewType == GiftcardView_Empty_More || self.viewType == GiftcardView_Empty_Discount || self.viewType == GiftcardView_Empty_StoreCard || self.viewType == GiftcardView_GiftcardImageOnly){
        w = (300.0/190.0) * h;
    }
    self.constViewBackWidth.constant = w;
    self.constViewFrontWidth.constant = w;
    self.constViewEmptyCardWidth.constant = w;
    self.constViewSingleGiftcardWidth.constant = w;
    
    self.constViewBackHeight.constant = h;
    self.constViewFrontHeight.constant = h;
    self.constViewEmptyCardHeight.constant = h;
    self.constViewSingleGiftcardHeight.constant = h;
        
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    [self setupViewsDimension];
}

-(void)updateViews{
    [self setupViewsDimension];
}

-(void)touchOnView:(id)sender{
    if (self.frontOnly){
        if (self.giftcardViewDelegate != nil && [self.giftcardViewDelegate respondsToSelector:@selector(giftcardViewTapped:)]){
            [self.giftcardViewDelegate giftcardViewTapped:self];
        }
    } 
    else{
        UIView *vFrom;
        UIView *vTo;
        if(_ifShowBackViewWithLogo){
            vFrom = currentView == VIEW_FRONT ? self.vFront: self.vBackWithLogo;
            vTo = currentView == VIEW_FRONT ? self.vBackWithLogo : self.vFront;
        }
        else{
            vFrom = currentView == VIEW_FRONT ? self.vFront: self.vBack;
            vTo = currentView == VIEW_FRONT ? self.vBack : self.vFront;
        }
        NSUInteger option = currentView == VIEW_FRONT ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
        option |= UIViewAnimationOptionShowHideTransitionViews;
        [UIView transitionFromView:vFrom toView:vTo duration:0.5 options:option | UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {
            if (finished){
                currentView = (currentView == VIEW_FRONT) ? VIEW_BACK : VIEW_FRONT;
            }
        }];
    }
}

-(void)favoriteIconTapped{
    [self sendFavortieGiftcardRequest];
}
-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(void)giftcardFavoriteNotificationHandler:(NSNotification*)noti{
    if (self.giftcard != nil && noti.userInfo != nil){
        ModelGiftCard *giftcard = [noti.userInfo objectForKey:@"Giftcard"];
        if (giftcard != nil && giftcard.giftCardId == self.giftcard.giftCardId){
            self.giftcard.favourite = giftcard.favourite;
            if (self.giftcard.favourite){
                self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_on.png"];
            }
            else{
                self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_off.png"];
            }
        }
    }
}

-(BOOL)giftCardNumberIsLink{
    if( [[self.giftcard.giftCardNumber lowercaseString] startWith:@"http://"] ||
       [[self.giftcard.giftCardNumber lowercaseString] startWith:@"https://"]){
        return YES;
    }
    return NO;
}
#pragma
#pragma mark - UILabelExtDelegate
-(void)labelExtTouched:(UILabelExt*)label{
    if (label == self.labBackGiftCardNumber && [self giftCardNumberIsLink]){
        [Utils openURLWithExternalBrowser:self.giftcard.giftCardNumber];
    }
}

#pragma 
#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (gestureRecognizer.view != self){
        return NO;
    }
    if (touch.view == self.ivFavorite){
        [self favoriteIconTapped];
        return NO;
    }
    if (touch.view == self.btnLeft||touch.view == self.btnRight || touch.view == self.labBackGiftCardNumber){
        return NO;
    }
    return YES;
}

#pragma
#pragma mark - Server communication handling
-(void)sendFavortieGiftcardRequest{
    RequestFavoriteGiftcard *req = [[RequestFavoriteGiftcard alloc] init];
    req.giftCardId = self.giftcard.giftCardId;
    [self.activityIndicator startAnimating];
    SEND_REQUEST(req, handleFavoriteGiftcardResponse, handleFavoriteGiftcardResponse);
}

-(void)handleFavoriteGiftcardResponse:(ResponseBase*)response{
    [self.activityIndicator stopAnimating];
    if (!response.success){
        [Utils showAlert:response.errorMessage delegate:nil];
        return;
    }
    self.giftcard.favourite = !self.giftcard.favourite;
    [Utils postLocalNotification:NOTI_Giftcard_Favorite object:nil userInfo:[NSDictionary dictionaryWithObject:self.giftcard forKey:@"Giftcard"]];
    
    if (self.giftcard.favourite){
        self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_on.png"];
    }
    else{
        self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_off.png"];
    }
}
@end
