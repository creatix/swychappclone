//
//  UIAddPhoto.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UIAddPhotoDelegate;
IB_DESIGNABLE
@interface UIAddPhoto : UIView

@property(nonatomic,weak) IBInspectable UIImage *selectedImage;
@property(nonatomic,weak) IBInspectable UIImage *plusImage;
@property(nonatomic,weak) IBInspectable NSString *text;

@property(weak,nonatomic) id<UIAddPhotoDelegate> addPhotoDelegate;

-(void)setAddPhotoText:(NSString*)text;
@end

@protocol UIAddPhotoDelegate <NSObject>

-(void)addPhotoTapped:(UIAddPhoto*)sender;

@end
