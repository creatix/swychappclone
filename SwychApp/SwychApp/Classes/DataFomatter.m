//
//  DataFomatter.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "DataFomatter.h"

@implementation DataFomatter

+(DataFomatter*)instance{
    static DataFomatter *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}

-(NSString*)formattedPhoneNumber:(NSString*)phoneNumber  countryCode:(NSString*)countryCode{
    if ([phoneNumber length] >= 10){
        NSString *c = nil;
        NSString *p1 = nil;
        NSString *p2 = nil;
        NSString *p3 = nil;
        
        if ([phoneNumber length] == 10){
            NSRange range = NSMakeRange(0, 3);
            p1 = [phoneNumber substringWithRange:range];
            range = NSMakeRange(3, 3);
            p2 = [phoneNumber substringWithRange:range];
            range = NSMakeRange(6, 4);
            p3 = [phoneNumber substringWithRange:range];
            if (countryCode != nil){
                c = countryCode;
            }
        }
        else{
            NSInteger len = [phoneNumber length];
            c = [phoneNumber substringToIndex:len - 11];
            NSRange range = NSMakeRange(len -10, 3);
            p1 = [phoneNumber substringWithRange:range];
            range = NSMakeRange(len - 7, 3);
            p2 = [phoneNumber substringWithRange:range];
            range = NSMakeRange(len - 4, 4);
            p3 = [phoneNumber substringWithRange:range];
            if (countryCode != nil){
                c = countryCode;
            }
        }
        return [NSString stringWithFormat:@"+%@(%@) %@ - %@",c,p1,p2,p3];
    }
    else{
        return phoneNumber;
    }
}
@end
