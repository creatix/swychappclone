//
//  PurchaseForMyselfConfirmationView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupConfirmationView.h"
#import "GiftcardWithBalanceAndWalletButtonView.h"
#import "GiftAttachmentView.h"

@class ModelTransaction;
@class ModelLocation;
@protocol PurchaseForMyselfConfirmatinViewDelegete;

@interface PurchaseForMyselfConfirmationView : PopupConfirmationView <GiftcardWithBalanceAndWalletButtonViewDelegate,
    UITableViewDelegate,UITableViewDataSource,GiftAttachmentViewDelegate>

@property (weak, nonatomic) IBOutlet GiftcardWithBalanceAndWalletButtonView *cvGiftcardView;
@property(nonatomic,assign) BOOL isPopupMode;
@property(nonatomic,assign) BOOL isArchived;
@property(nonatomic,strong) ModelTransaction *transaction;
@property(nonatomic,strong) NSArray<ModelLocation*> *locations;
@property(nonatomic,assign) id<PurchaseForMyselfConfirmatinViewDelegete> confirmationViewDelegate;
-(void)UpdateViewStatus;
@end


@protocol PurchaseForMyselfConfirmatinViewDelegete <NSObject>

-(void)giftCardArchived:(ModelTransaction*)transaction;
-(void)OpenTerms:(NSString*) termsUrl;
@end