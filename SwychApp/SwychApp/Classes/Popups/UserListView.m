//
//  UserListView.m
//  VisaBuxx
//
//  Created by kai xing on 2013-10-08.
//  Copyright (c) 2013 DeviceFidelity. All rights reserved.
//

#import "UserListView.h"

@implementation UserListView

@synthesize delegate,ivBackground,tvUserList;

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
//    self.hidden = YES;
    
    UIImage *img =[[UIImage imageNamed:@"HalfRoundRectangeBackground.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    ivBackground.image = img;
    
    [self.tvUserList setTableFooterView:[UIView new]];
    self.clipsToBounds = true;
    NSMutableArray * array  =  [[NSMutableArray alloc] init];
//    [[LocalStorageManager getInstance] getAllUsers:array ];
//    [array insertObject:LoadResText(@"NewUser", nil) atIndex:0];
 //   usersList = array;

}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



-(IBAction)cancelButtonClicked:(id)sender{
    self.hidden = YES;
    if (self.delegate != NULL && [self.delegate respondsToSelector:@selector(UserListViewCancelButtonClicked)]){
        [self.delegate UserListViewCancelButtonClicked];
    }
}

-(void)reloadAffiliates:(NSArray *)affiliates{
    tvUserList.delegate = self;
    tvUserList.dataSource = self;
    _affiliates = affiliates;
    [tvUserList reloadData];
    
}

#pragma - mark UITAbleViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //  return [usersList count] + 1;
    if(!_affiliates) return 0;
    return [_affiliates count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30.0f;
}

-(UITableViewCell*)tableView:(UITableView* )tableView cellForRowAtIndexPath:(NSIndexPath* )indexPath{
    static NSString *identifier = @"UserListViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    ModelAffiliate* u = [_affiliates objectAtIndex:indexPath.row];
    cell.textLabel.text = u.name;
    return cell;
}

-(void)tableView:(UITableView* )tableView didSelectRowAtIndexPath:(NSIndexPath* )indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ModelAffiliate* u = [_affiliates objectAtIndex:indexPath.row];
    if (delegate != nil && [delegate respondsToSelector:@selector(UserListViewUserSelected:)]){
        [delegate UserListViewUserSelected:u];
    }
}
@end
