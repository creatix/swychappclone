//
//  ReceiveGiftcardPopupView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"
#import "ModelGiftCard.h"

@class ModelTransaction;
@protocol ReceiveGiftcardPopupViewDelegate;

@interface ReceiveGiftcardPopupView : PopupConfirmationView

@property(nonatomic,strong) NSArray<ModelTransaction*> *transactions;
@property(nonatomic,weak) id<ReceiveGiftcardPopupViewDelegate> receiveGiftcardPopupViewDelegate;

@end


@protocol ReceiveGiftcardPopupViewDelegate <NSObject>

-(void)receiveGiftcardPopupView:(ReceiveGiftcardPopupView*)popupView giftcardAccepted:(ModelTransaction*)transaction;

@end