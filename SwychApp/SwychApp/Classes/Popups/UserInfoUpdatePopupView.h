//
//  UserInfoUpdatePopupView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"
#import "UserInfoUpdateView.h"

@interface UserInfoUpdatePopupView : PopupConfirmationView

@property(nonatomic,assign) id<UserInfoUpdateViewDelegate> delegateUpdate;
@property(nonatomic,assign) BOOL selfGifting;
@end


