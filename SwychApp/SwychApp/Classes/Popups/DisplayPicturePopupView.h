//
//  DisplayPicturePopupView.h
//  SwychApp
//
//  Created by Kai Xing on 2016-06-23.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"

@interface DisplayPicturePopupView : PopupConfirmationView

@property(nonatomic,strong) NSString *imageURL;
@end
