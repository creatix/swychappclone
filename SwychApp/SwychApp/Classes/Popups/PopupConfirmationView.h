//
//  PopupConfirmationView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PopupConfirmationView;

@protocol PopupConfirmationViewDelegate <NSObject>

@optional
-(void)popupConfirmationViewDissmissed:(PopupConfirmationView*)confirmationView;

@end

@interface PopupConfirmationView : UIView

@property(nonatomic,weak) id<PopupConfirmationViewDelegate> delegate;
@property(nonatomic,assign) NSInteger context;
@property(nonatomic,strong) NSObject *contextObj;

-(void)show;
-(void)dismiss;
-(void)viewWillShow;
-(void)setupWhenAwakeFromNib;
-(void)updateView;

- (IBAction)buttonTapped:(id)sender;

@end
