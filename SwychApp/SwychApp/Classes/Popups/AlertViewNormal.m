//
//  AlertViewNormal.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AlertViewNormal.h"
#import "TTTAttributedLabel.h"
#import "AppDelegate.h"
#import "PopupViewHandler.h"
#import "ObjectConfigInfo.h"
#import "UIButton+Extra.h"
#import "UICheckbox.h"

#define ViewName        @"AlertViewNormal"

@interface AlertViewNormal()

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labTitle;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labMessage;
@property (weak, nonatomic) IBOutlet UILabel *labHorzLine;
@property (weak, nonatomic) IBOutlet UILabel *labVertLine;
@property (weak, nonatomic) IBOutlet UIButton *btnSecond;
@property (weak, nonatomic) IBOutlet UIButton *btnFirst;
@property (weak, nonatomic) IBOutlet UIButton *btnSingle;
@property (weak, nonatomic) IBOutlet UIView *vBottomButtonView;
@property (weak, nonatomic) IBOutlet UICheckbox *checkbox;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTitleHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSpaceMessageToBottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCheckboxBottomSpace;


- (IBAction)btnTapped:(id)sender;

@end


@implementation AlertViewNormal

@synthesize checkboxText = _checkboxText;

-(NSString *)checkboxText{
    return _checkboxText;
}

-(void)setCheckboxText:(NSString *)checkboxText{
    _checkboxText = checkboxText;
    if (_checkboxText == nil || checkboxText.length == 0){
        self.constraintSpaceMessageToBottomView.constant = 20.0f;
        self.checkbox.hidden = YES;
        self.checkbox.text = nil;
    }
    else{
        self.constraintSpaceMessageToBottomView.constant = self.constraintCheckboxHeight.constant + self.constraintCheckboxBottomSpace.constant;
        self.checkbox.hidden = NO;
        self.checkbox.text = checkboxText;
    }
}

-(void)awakeFromNib{
    self.labMessage.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
    UIColor *clr = [[ApplicationConfigurations instance] getTextColor:ViewName];
    self.labTitle.textColor = clr;
    self.labMessage.textColor = clr;
    
    self.vBottomButtonView.backgroundColor = [UIColor clearColor];
    
    ObjectConfigInfo *config = [[ApplicationConfigurations instance] getButtonConfiguration:ViewName];
    if(config != nil){
        [self.btnFirst setButtonInfo:config];
        [self.btnSecond setButtonInfo:config];
        [self.btnSingle setButtonInfo:config];
    }
    self.checkboxText = nil;
    self.checkbox.checked = NO;
    
    self.checkbox.checkboxDelegate = (id<UICheckboxDelegate>)self;
    
}

-(void)setTitle:(NSString*)title message:(NSString*)message buttonText:(NSString*)first,...{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    NSString* eachObject;
    va_list argumentList;
    if (first){
        [array addObject:first];
        va_start(argumentList, first);
        while((eachObject = va_arg(argumentList, NSString*))){
            [array addObject:eachObject];
        }
        va_end(argumentList);
    }
    if ([array count] == 0){
        return;
    }
    _singledButton = [array count] == 1;
    if (_singledButton) {
        self.btnSingle.hidden = NO;
        [self.btnSingle setTitle:[array objectAtIndex:0] forState:UIControlStateNormal];
        [self.btnSingle setTitle:[array objectAtIndex:0] forState:UIControlStateHighlighted];
        
        self.btnFirst.hidden = YES;
        self.btnSecond.hidden = YES;
        self.labVertLine.hidden = YES;
    }
    else{
        self.btnSingle.hidden = YES;
        self.labVertLine.hidden = NO;
        
        self.btnFirst.hidden = NO;
        [self.btnFirst setTitle:[array objectAtIndex:0] forState:UIControlStateNormal];
        [self.btnFirst setTitle:[array objectAtIndex:0] forState:UIControlStateHighlighted];
        
        self.btnSecond.hidden = NO;
        [self.btnSecond setTitle:[array objectAtIndex:1] forState:UIControlStateNormal];
        [self.btnSecond setTitle:[array objectAtIndex:1] forState:UIControlStateHighlighted];
    }
    self.labTitle.text = title;
    if (title == nil || [title length] == 0){
        self.constraintTitleHeight.constant = 0;
    }
    self.labMessage.text = message;
}

-(void)layoutSubviews{
    [super layoutSubviews];
}

-(void)setCheckboxState:(BOOL)checked{
    self.checkbox.checked = checked;
}

- (IBAction)btnTapped:(id)sender {
    BOOL ret = YES;
    if (self.alertViewDelegate != nil && [self.alertViewDelegate respondsToSelector:@selector(alertView:buttonTappedWithIndex:)]){
        NSInteger idx = (sender == self.btnSingle || sender == self.btnFirst) ? 1 : 2;
        ret = [self.alertViewDelegate alertView:self buttonTappedWithIndex:idx];
    }
    if (ret){
        [self dismiss];
    }
}

#pragma 
#pragma mark - UICheckboxDelegate
-(void)checkbox:(UICheckbox*)sender stateChanged:(BOOL)state{
    if (self.alertViewDelegate != nil && [self.alertViewDelegate respondsToSelector:@selector(alertView:checkboxValueChanged:)]){
        [self.alertViewDelegate alertView:self checkboxValueChanged:state];
    }
}
@end
