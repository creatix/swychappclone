//
//  UserListView.h
//  Moneto
//
//  Created by knupro-idev1 on 2014-12-15.
//
//

#import <UIKit/UIKit.h>
#import "ModelAffiliate.h"
@protocol UserListViewDelegate <NSObject>
@optional
-(void)userListViewNewUserSelected;
-(void)UserListViewUserSelected:(ModelAffiliate*)Affiliate;
-(void)UserListViewCancelButtonClicked;
@end
@interface UserListView : UIView<UITableViewDataSource,UITableViewDelegate>{
   // NSArray *usersList;
}
@property (strong,atomic) NSArray<ModelAffiliate *> *affiliates;
@property (assign,atomic) id<UserListViewDelegate> delegate;
@property (strong,atomic) IBOutlet UIImageView *ivBackground;
@property (strong,atomic) IBOutlet UITableView *tvUserList;

-(void)reloadAffiliates:(NSArray *)affiliates;

-(IBAction)cancelButtonClicked:(id)sender;
@end
