//
//  PurchaseForMyselfConfirmationView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PurchaseForMyselfConfirmationView.h"
#import "UIRoundCornerButton.h"
#import "ModelGiftCard.h"
#import "AppDelegate.h"
#import "SwychBaseViewController.h"
#import "Utils.h"
#import "PassHandler.h"
#import "ModelTransaction.h"
#import "LocationGiftAttachmentCollectionViewCell.h"
#import "LocationTableViewCell.h"
#import "ModelGiftAttachments.h"
#import "ModelUser.h"
#import "ContactBookManager.h"
#import "ModelUser.h"
#import "RequestArchiveGiftcard.h"
#import "ResponseArchiveGiftcard.h"
#import "RequestSignWalletPass.h"
#import "ResponseSignWalletPass.h"
#import "NSString+Extra.h"
#import "LocalUserInfo.h"
#import "MessageWithWebViewPopView.h"
#define Context_Prompt_Archive  0x01
#define Context_Prompt_unArchive  0x02
#define IDEN_CELL_COLLECTIONVIEW    @"IdenCellCollecetionView"
#define IDEN_CELL_TABLEVIEW         @"IdenCellTableView"

@interface PurchaseForMyselfConfirmationView()


@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;



@property (weak, nonatomic) IBOutlet UIView *vLocationAttachment;
@property (weak, nonatomic) IBOutlet UICollectionView *cvContainer;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;


@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btnArchive;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;

@property (strong,nonatomic) UITableView *tvLocations;
@property (nonatomic,strong) GiftAttachmentView * cvAttachment;
@property (nonatomic)  int iCollectionItems;
@property (nonatomic)  BOOL bMoveCellToMessage;
@end

@implementation PurchaseForMyselfConfirmationView


-(void)setupWhenAwakeFromNib{
    self.cvGiftcardView.delegate = self;
    self.isPopupMode = YES;
}

-(void)viewWillShow{
    [self setupCollectinView];
    self.bMoveCellToMessage = YES;
    self.cvGiftcardView.giftCard = self.transaction.giftCard;
    
    self.cvContainer.backgroundColor = [UIColor clearColor];
     [self.cvContainer layoutIfNeeded];
    [self.cvContainer reloadData];
    [self.cvGiftcardView updateSubviews];
    if(!self.transaction.giftCard.termsURL||[self.transaction.giftCard.termsURL isEqualToString:@""]){
    self.btnTerms.hidden = YES;
    }
    BOOL containsString = [Utils CheckIfAmazon:self.transaction.giftCard];
    if(containsString) {
        self.btnTerms.hidden = NO;
    }
    _btnArchive.hidden = YES;
}


-(void)setupCollectinView{
    [self.cvContainer registerClass:[LocationGiftAttachmentCollectionViewCell class] forCellWithReuseIdentifier:IDEN_CELL_COLLECTIONVIEW];
    
    self.cvContainer.delegate =(id<UICollectionViewDelegate>) self;
    self.cvContainer.dataSource = (id<UICollectionViewDataSource>)self;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.cvContainer setPagingEnabled:YES];
    [self.cvContainer setCollectionViewLayout:flowLayout];
    [self UpdateViewStatus];
}
-(void)UpdateViewStatus{
    if(_isArchived){
        [_btnArchive setTitle:NSLocalizedString(@"unArchivedtext", nil) forState:UIControlStateNormal];
    }
    else{
        [_btnArchive setTitle:NSLocalizedString(@"Archivedtext", nil) forState:UIControlStateNormal];
    }
}
- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnTerms){
        BOOL containsString = [Utils CheckIfAmazon:self.transaction.giftCard];
        if(containsString) {
            MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
            [cppv AddUrl:@"amazonterms" title:NSLocalizedString(@"TermsConditions",nil)];
            [cppv show];
            return;
        }
        if(self.confirmationViewDelegate){
        [self.confirmationViewDelegate OpenTerms:self.transaction.giftCard.termsURL];
        }
        
    }
    else if(sender == self.btnArchive){
        NSInteger cont;
        NSString* protext;
        if(_isArchived){
            cont = Context_Prompt_unArchive;
            protext = NSLocalizedString(@"unArchiveCardPrompt", nil);
        }
        else{
            cont = Context_Prompt_Archive;
            protext = NSLocalizedString(@"ArchiveCardPrompt", nil);
        }
        [Utils showAlert:protext
                   title:nil
                delegate:(id<AlertViewDelegate>)self
             firstButton:NSLocalizedString(@"Button_NO", nil)
            secondButton:NSLocalizedString(@"Button_YES", nil)
         withContext:cont
       withContextObject:nil
            checkboxText:nil];
    }
    else if (sender == self.btnDone){
        if (self.isPopupMode){
            [self dismiss];
        }
    }
    else{
        [super buttonTapped:sender];
    }
}

-(void)archiveGiftcard{
    [self sendArchiveGiftcardRequest];
}
-(void)unarchiveGiftcard{
    [self sendUnArchiveGiftcardRequest];
}
#pragma 
#pragma mark -AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context == Context_Prompt_Archive){
        if(index == 2){
            [alertView dismiss];
            [self archiveGiftcard];
            return NO;
        }
    }
    if (alertView.context == Context_Prompt_unArchive){
        if(index == 2){
            [alertView dismiss];
            [self unarchiveGiftcard];
            return NO;
        }
    }
    return YES;
}

#pragma 
#pragma mark - GiftcardWithBalanceAndWalletButtonViewDelegate
-(void)giftcardWithBalanceAndWalletButtonViewFinishedAddPassToWallet:(GiftcardWithBalanceAndWalletButtonView *)giftcardView{
  
}

-(void)giftcardWithBalanceAndWalletButtonView:(GiftcardWithBalanceAndWalletButtonView *)giftcardView finishBalanceUpdate:(double)newBalance{
    
}

-(void)giftcardWithBalanceAndWalletButtonViewStartAddPassToWallet:(GiftcardWithBalanceAndWalletButtonView *)giftcardView{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SwychBaseViewController *root = [app getCurrentViewController];
    [root showLoadingView:nil];
    [self sendSignWalletPassRequest];
}

-(void)giftcardWithBalanceAndWalletButtonViewStartUpdateBalance:(GiftcardWithBalanceAndWalletButtonView *)giftcardView{
    
}


#pragma 
#pragma  mark - Server communication
-(void)sendArchiveGiftcardRequest{
    RequestArchiveGiftcard * req = [[RequestArchiveGiftcard alloc] init];
    if (self.transaction.giftCard.isPlasticCard){
        req.transactionId = [@(self.transaction.giftCard.plasticGiftCardId) stringValue];
    }
    else{
        req.transactionId = self.transaction.giftCard.transactionId;
    }
    req.isPlasticGiftcard = self.transaction.giftCard.isPlasticCard;
    
    SEND_REQUEST(req, handleArchiveGiftcardResponse, handleArchiveGiftcardResponse)
}
-(void)handleArchiveGiftcardResponse:(ResponseBase*)response{
    AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    if (![ctrl handleServerResponse:response]){
        if (self.isPopupMode){
            [self dismiss];
        }
        if(response.context == Context_Prompt_unArchive){
            [Utils postLocalNotification:NOTI_Giftcard_Unarchived object:nil userInfo:[NSDictionary dictionaryWithObject:self.transaction forKey:@"Transaction"]];
        }
        else{
        [Utils postLocalNotification:NOTI_Giftcard_Archived object:nil userInfo:[NSDictionary dictionaryWithObject:self.transaction forKey:@"Transaction"]];
        }
    }
}
-(void)sendUnArchiveGiftcardRequest{
    RequestArchiveGiftcard * req = [[RequestArchiveGiftcard alloc] init];
    if (self.transaction.giftCard.isPlasticCard){
        req.transactionId = [@(self.transaction.giftCard.plasticGiftCardId) stringValue];
    }
    else{
        req.transactionId = self.transaction.giftCard.transactionId;
    }
    req.isPlasticGiftcard = self.transaction.giftCard.isPlasticCard;
    req.context = Context_Prompt_unArchive;
    SEND_REQUEST(req, handleArchiveGiftcardResponse, handleArchiveGiftcardResponse)
}
-(void)sendSignWalletPassRequest{
    RequestSignWalletPass *req = [[RequestSignWalletPass alloc] init];
    if (self.transaction.giftCard.isPlasticCard){
        req.plasticGiftcardId = self.transaction.giftCard.plasticGiftCardId;
    }
    else{
        req.transactionId = self.transaction.giftCard.transactionId;
    }
    SEND_REQUEST(req, handleSignWalletPassResponse, handleSignWalletPassResponse);
}

-(void)handleSignWalletPassResponse:(ResponseBase*)response{
    AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    [ctrl dismissLoadingView];
    if (![ctrl handleServerResponse:response]){
        ResponseSignWalletPass *resp = (ResponseSignWalletPass*)response;
        NSData *passData = [[NSData alloc] initWithBase64EncodedString:resp.passData options:0];
        [[PassHandler instance] GeneratePassFromServer:passData delegate:(id<PassHandlerDelegate>)self];
    }
}

#pragma 
#pragma mark - PassHandlerDelegate
-(void)beforePresentAddPassController{
    //self.hidden = YES;
}
-(void)afterPresentAddPassController{
    
}
-(void)addPassViewWillDismissWithCancelButtonTapped:(BOOL)cancelButtonTapped addPass:(BOOL)success error:(NSError*)error{
    //self.hidden = NO;
    if (success){
        [self.cvGiftcardView updateAddToWalletButton];
    }
    else{
        if (error != nil){
            [Utils showAlert:[error localizedDescription] delegate:nil];
        }
    }
}


#pragma 
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.locations.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDEN_CELL_TABLEVIEW];
    
    cell.location = [self.locations objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSSet *visibleSections = [NSSet setWithArray:[[tableView indexPathsForVisibleRows] valueForKey:@"section"]];
    if (visibleSections) {
        if(!self.bMoveCellToMessage) return;

        [self.cvContainer performBatchUpdates:^{}
                                   completion:^(BOOL finished) {
                                       
                                       self.bMoveCellToMessage = NO;
                                       NSArray *visibleItems = [self.cvContainer indexPathsForVisibleItems];
                                       if(_iCollectionItems>1){
                                           NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
                                           NSIndexPath *nextItem = [NSIndexPath indexPathForItem:1 inSection:currentItem.section];
                                           
                                           [self.cvContainer scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
                                       }
                                   }];
    }}
#pragma mark -
#pragma mark UICollectionView methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    BOOL containsString = [Utils CheckIfAmazon:self.transaction.giftCard];
    if(containsString) {
        _iCollectionItems = 1;
        return 1;
    }
    if (self.transaction.giftCard.isPlasticCard){
        _iCollectionItems = 1;
        return 1;
    }
     LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    
    if([self.transaction.sender.swychId isEqualToString:user.swychId]) {
        _iCollectionItems = 1;
        return 1;
    }
    _iCollectionItems = 2;
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LocationGiftAttachmentCollectionViewCell *cell = (LocationGiftAttachmentCollectionViewCell *)[self.cvContainer dequeueReusableCellWithReuseIdentifier:IDEN_CELL_COLLECTIONVIEW forIndexPath:indexPath];
    
    if (indexPath.row == 0){
        BOOL containsString = [Utils CheckIfAmazon:self.transaction.giftCard];
        if(containsString) {
            cell.tvLocations.delegate = nil;
            cell.tvLocations.dataSource = nil;
            LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
            cell.showQuoteView = [self.transaction.sender.swychId isEqualToString:user.swychId];
            if(!self.transaction.sender) cell.showQuoteView = YES;
            cell.showLocationView = NO;
            self.cvAttachment = cell.cvAttachment;
            self.cvAttachment.message = self.transaction.sentMessage.message;
            self.cvAttachment.senderName = self.transaction.sender.firstName;
            self.cvAttachment.avatarImage = [ContactBookManager getContactHeadImgByPhoneNumber:self.transaction.sender.phoneNumber];
            self.cvAttachment.transaction = self.transaction;
            if (self.transaction.sentMessage == nil||[NSString isNullOrEmpty:self.transaction.sentMessage.imgUrl]){
                self.cvAttachment.videoButtonViewHeight = 0.0f;
            }
            else{
                self.cvAttachment.imageUrl = self.transaction.sentMessage.imgUrl;
            }
        }
        else{
            cell.showQuoteView = _transaction.giftCard.redeemType == 1?YES:NO;
        cell.showLocationView = YES;

        self.tvLocations = cell.tvLocations;
        
        self.tvLocations.delegate = self;
        self.tvLocations.dataSource = self;
        
        [self.tvLocations registerNib:[UINib nibWithNibName:@"LocationTableViewCell" bundle:nil]
               forCellReuseIdentifier:IDEN_CELL_TABLEVIEW];
        
        [self.tvLocations reloadData];
        }
        
    }
    else{
        cell.showLocationView = NO;
        cell.tvLocations.delegate = nil;
        cell.tvLocations.dataSource = nil;
        
        
        self.cvAttachment = cell.cvAttachment;
        self.cvAttachment.message = self.transaction.sentMessage.message;
        self.cvAttachment.senderName = self.transaction.sender.firstName;
        self.cvAttachment.avatarImage = [ContactBookManager getContactHeadImgByPhoneNumber:self.transaction.sender.phoneNumber];
        self.cvAttachment.transaction = self.transaction;
        if (self.transaction.sentMessage == nil||[NSString isNullOrEmpty:self.transaction.sentMessage.imgUrl]){
            self.cvAttachment.videoButtonViewHeight = 0.0f;
        }
        else{
            self.cvAttachment.imageUrl = self.transaction.sentMessage.imgUrl;
        }
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.cvContainer.frame.size;
}

#pragma 
#pragma mark = GiftAttachmentViewDelegate
-(void)giftAttachmentViewSayThanksButtonTapped:(GiftAttachmentView*)attachmentView{
    
}

@end
