//
//  DisplayPicturePopupView.m
//  SwychApp
//
//  Created by Kai Xing on 2016-06-23.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "DisplayPicturePopupView.h"
#import "UIRoundCornerButton.h"
#import "UIImageView+WebCache.h"

@interface DisplayPicturePopupView()
@property (weak, nonatomic) IBOutlet UIImageView *ivViewBackground;
@property (weak, nonatomic) IBOutlet UIView *vContent;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnOK;
@property (weak, nonatomic) IBOutlet UIImageView *ivPicture;

- (IBAction)buttonTapped:(id)sender;
@end

@implementation DisplayPicturePopupView

-(void)setupWhenAwakeFromNib{
    
    
}
-(void)viewWillShow{
    self.vContent.layer.cornerRadius = 10.0f;
    
    [self.btnOK setTitle:NSLocalizedString(@"Button_OK", nil)];
    
    [self.ivPicture sd_setImageWithURL:[NSURL URLWithString:self.imageURL]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate =(id<UIGestureRecognizerDelegate>) self;
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnOK){
        [self dismiss];
    }
}

-(void)touchOnView:(id)sender{
    [self dismiss];
}

@end
