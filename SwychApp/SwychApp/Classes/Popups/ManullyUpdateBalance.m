//
//  ManullyUpdateBalance.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-24.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ManullyUpdateBalance.h"
#import "ModelUser.h"


@interface ManullyUpdateBalance()
@property (weak, nonatomic) IBOutlet UIView *vMiddleView;
@property (weak, nonatomic) IBOutlet UIView *vBottomView;
@property (weak, nonatomic) IBOutlet UILabel *labVertLine;
@property (weak, nonatomic) IBOutlet UILabel *labHorzLine;


@end
@implementation ManullyUpdateBalance

-(void)awakeFromNib{
    UIColor *clrText = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vMiddleView.backgroundColor = [UIColor clearColor];
    self.vBottomView.backgroundColor = [UIColor clearColor];
    self.txtUpdateBalnce.delegate = self;
    self.txtUpdateBalnce.layer.cornerRadius = 10.0f;
    self.txtUpdateBalnce.backgroundColor = UICOLORFROMRGB(209, 209, 209, 10);
    self.txtUpdateBalnce.offsetX = 8.0f;
    self.txtUpdateBalnce.textColor = clrText;
    self.txtMessage.delegate = self;
    self.txtMessage.layer.cornerRadius = 10.0f;
    self.txtMessage.backgroundColor = UICOLORFROMRGB(209, 209, 209, 10);
    self.txtMessage.offsetX = 8.0f;
    self.txtMessage.textColor = clrText;
    self.labTitle.text = @"my title";
    self.labTitle.textColor = clrText;
    
    self.labWeHaveTexted.text = NSLocalizedString(@"JustTextedCodeTo", nil);
    self.labWeHaveTexted.textColor = clrText;
    
    self.labVertLine.textColor = UICOLORFROMRGB(163, 162, 162, 1.0);
    self.labHorzLine.textColor = self.labVertLine.textColor;
    
    [self.btnCancel setTitle:NSLocalizedString(@"Button_Cancel_low", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"Button_Cancel_low", nil) forState:UIControlStateHighlighted];
    self.btnCancel.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    [self.btnCancel setTitleColor:[UIColor whiteColor]];
    
    [self.btnSubmit setTitle:NSLocalizedString(@"Button_Submit", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"Button_Submit", nil) forState:UIControlStateHighlighted];
    self.btnSubmit.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    [self.btnSubmit setTitleColor:[UIColor whiteColor]];
    self.txtMessageHei.constant = 0;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        self.txtUpdateBalnce.layer.cornerRadius = 10.0f;
        self.txtUpdateBalnce.backgroundColor = UICOLORFROMRGB(209, 209, 209, 10);
    }
    return self;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;{
    if (self.uDelegate != nil){
        return YES;
    }
    if (textField == _txtUpdateBalnce){
        NSString *number=textField.text;
        
        number=[number stringByReplacingCharactersInRange:range withString:string];
        number=[number stringByReplacingOccurrencesOfString:@"." withString:@""];
        if([number integerValue]>0){
            textField.text=[NSString stringWithFormat:@"%.2f",[number integerValue]*0.01f];
        }
        else{
            textField.text=@"";
        }
        return NO;
    }
    
    return YES;
}

-(BOOL)allowTouchToDismiss{
    return NO;
}

-(NSString*)getOTPCode{
    return self.txtUpdateBalnce.text;
}

-(void)setMobilePhoneNumber:(NSString*)phoneNumber{

}

- (IBAction)buttonTapped:(id)sender {

    if(sender == self.btnSubmit&&[self.txtUpdateBalnce.text isEqualToString:@""])
        return;
    [self dismiss];
    [self buttonTpaHandle:sender];
}

-(void)buttonTpaHandle:(id)sender{
    if (self.vDelegate != nil){
        if (sender == self.btnSubmit&& [self.vDelegate respondsToSelector:@selector(ManullyUpdateBalance:)]){
            if(![_txtUpdateBalnce.text isEqualToString:@""])
                [self.vDelegate ManullyUpdateBalance:_txtUpdateBalnce.text];
            else
                return;
        }
    }
    if (self.uDelegate != nil){
        if (sender == self.btnSubmit){
            if(![self.txtUpdateBalnce.text isEqualToString:@""])
                [self.uDelegate UpdateContext:self.txtUpdateBalnce.text message:_txtMessage.text view:self];
            else
                return;
        }
    }
}
@end
