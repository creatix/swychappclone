//
//  ManullyUpdateBalance.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-24.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"
#import "UITextFieldExtra.h"
#import "UIRoundCornerButton.h"
@protocol ManullyUpdateBalanceDelegate;
@protocol UpdateWithTextFieldPopUpDelegate;
@interface ManullyUpdateBalance : PopupViewBase<UITextFieldDelegate>
@property (assign,nonatomic) id<ManullyUpdateBalanceDelegate> vDelegate;
@property (assign,nonatomic) id<UpdateWithTextFieldPopUpDelegate> uDelegate;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labWeHaveTexted;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtUpdateBalnce;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtMessage;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSubmit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtMessageHei;
@property ( nonatomic)  int ContextID;
-(IBAction)buttonTapped:(id)sender;
@end
@protocol ManullyUpdateBalanceDelegate <NSObject>
-(void)ManullyUpdateBalance:(NSString*)Balance;
@end
@protocol UpdateWithTextFieldPopUpDelegate <NSObject>

-(void)UpdateContext:(NSString*)text message:(NSString*)textmessage view:(ManullyUpdateBalance*)view;

@end
