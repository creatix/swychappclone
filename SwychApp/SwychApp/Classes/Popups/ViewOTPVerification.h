//
//  ViewOTPVerification.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewBase.h"
@class UITextFieldExtra;
@class ModelUser;

@protocol OTPVerificationDelegate;

@interface ViewOTPVerification : PopupViewBase<AlertViewDelegate>


@property (assign,nonatomic) id<OTPVerificationDelegate> verificationDelegate;
@property (nonatomic,strong) ModelUser *user;
@property ( nonatomic)  BOOL bTapDissMiss;
@property (nonatomic,strong) NSString *textCode;

-(IBAction)buttonTapped:(id)sender;
-(NSString*)getOTPCode;
-(void)setMobilePhoneNumber:(NSString*)phoneNumber;
-(NSString*)getMobilePhoneNumber;
@end


@protocol OTPVerificationDelegate <NSObject>

-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView;
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP;
-(void)OTPVerificationCancelled:(ViewOTPVerification*)verificationView;
@end
