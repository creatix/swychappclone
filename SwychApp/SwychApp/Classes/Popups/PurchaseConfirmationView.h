//
//  PurchaseConfirmationView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewBase.h"
#import "PopupConfirmationView.h"


@class UIRoundCornerImageView;
@class UIGiftcardView;
@class UIRoundCornerButton;
@class ModelGiftCard;
@class ModelContact;

@interface PurchaseConfirmationView : PopupConfirmationView

@property(nonatomic,strong) NSString *transactionId;
@property(nonatomic,assign) double orderAmount;
@property(nonatomic,assign) double paymentAmount;
@property(nonatomic,strong) NSString *recipientPhoneNumber;
@property(nonatomic,strong) NSDate *deliveryDate;
@property(nonatomic,strong) ModelGiftCard *giftcard;
@property(nonatomic,strong) ModelContact *contact;

@property(nonatomic,assign) double earnedPoints;

@property(nonatomic,assign) BOOL isRegifting;

@end

