//
//  ProgressView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ProgressView.h"

@implementation ProgressView
-(void)awakeFromNib{
    self.layer.cornerRadius = 10.0f;
    self.backgroundColor = [UIColor whiteColor];
}

-(void)startAnimation{
    [self.activityProgress startAnimating];
}

-(void)startAnimationWithText:(NSString*)text{
    self.labLoading.text = text;
    [self startAnimation];
}

-(void)stopAnimation{
    [self.activityProgress stopAnimating];
}

-(void)setLoadingText:(NSString*)text{
    self.labLoading.text = text;
}
-(NSString*)getLoadingText{
    return self.labLoading.text;
}
@end
