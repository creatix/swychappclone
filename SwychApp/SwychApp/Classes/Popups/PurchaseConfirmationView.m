//
//  PurchaseConfirmationView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PurchaseConfirmationView.h"
#import "ModelGiftCard.h"
#import "CreditPointsManager.h"
#import "UIGiftcardView.h"
#import "ModelContact.h"

#define COLOR_EARNPOINTS    UICOLORFROMRGB(28,130,181,1.0)
#define COLOR_NAME  UICOLORFROMRGB(192,31,109,1.0)
#define COLOR_ORDER_INFO    UICOLORFROMRGB(76,77,77,1.0)

@interface PurchaseConfirmationView()
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UILabel *labHeader;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfileImage;
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivRoundCheckmark;
@property (weak, nonatomic) IBOutlet UILabel *labFirstName;
@property (weak, nonatomic) IBOutlet UILabel *labLastName;
@property (weak, nonatomic) IBOutlet UIView *vOrderInfoBackground;
@property (weak, nonatomic) IBOutlet UILabel *labOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *labOrderNumberValue;
@property (weak, nonatomic) IBOutlet UILabel *labOrderTotal;
@property (weak, nonatomic) IBOutlet UILabel *labOrderTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *labOrderPayment;
@property (weak, nonatomic) IBOutlet UILabel *labOrderPaymentValue;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardTitle;
@property (weak, nonatomic) IBOutlet UILabel *labPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *labMobileDelivery;
@property (weak, nonatomic) IBOutlet UILabel *labDate;
@property (weak, nonatomic) IBOutlet UILabel *labDeliveryDate;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardAmount;
@property (weak, nonatomic) IBOutlet UILabel *labAmount;
@property (weak, nonatomic) IBOutlet UIGiftcardView *cvGiftcard;
@property (weak, nonatomic) IBOutlet UIView *vGiftcardDeliveryInfo;

@property (weak, nonatomic) IBOutlet UIView *vFooterRegift;

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnDone;

@end
@implementation PurchaseConfirmationView

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(void)setupWhenAwakeFromNib{    
    UIColor *clearColor =[UIColor clearColor];
    self.labHeader.backgroundColor = clearColor;
    self.vOrderInfoBackground.backgroundColor = UICOLORFROMRGB(240, 240, 240, 1.0);
    self.vGiftcardDeliveryInfo.backgroundColor = clearColor;
    
    self.labOrderNumber.text = NSLocalizedString(@"LabelOrderNumber", nil);
    self.labOrderTotal.text = NSLocalizedString(@"LabelOrderTotal", nil);
    self.labOrderPayment.text = NSLocalizedString(@"LabelOrderPayment", nil);
    self.labMobileDelivery.text = NSLocalizedString(@"LabelMobileDelivery", nil);
    self.labDeliveryDate.text = NSLocalizedString(@"LabelDeliveryDate", nil);
    self.labAmount.text =  NSLocalizedString(@"LabelAmount", nil);
    
    self.labHeader.textColor = COLOR_EARNPOINTS;
    
    self.labFirstName.textColor = COLOR_NAME;
    self.labLastName.textColor = COLOR_NAME;
    
    self.labOrderNumber.textColor = COLOR_ORDER_INFO;
    self.labOrderNumberValue.textColor = COLOR_ORDER_INFO;
    self.labOrderTotal.textColor = COLOR_ORDER_INFO;
    self.labOrderTotalValue.textColor = COLOR_ORDER_INFO;
    self.labOrderPayment.textColor = COLOR_ORDER_INFO;
    self.labOrderPaymentValue.textColor = COLOR_ORDER_INFO;
    
    self.labGiftcardTitle.textColor = COLOR_ORDER_INFO;
    self.labPhoneNumber.textColor = COLOR_ORDER_INFO;
    self.labMobileDelivery.textColor = COLOR_EARNPOINTS;
    self.labDate.textColor = COLOR_ORDER_INFO;
    self.labDeliveryDate.textColor = COLOR_EARNPOINTS;
    self.labGiftcardAmount.textColor = COLOR_ORDER_INFO;
    self.labAmount.textColor = COLOR_EARNPOINTS;
		
}

-(void)viewWillShow{
    self.labHeader.text = [NSString stringWithFormat:NSLocalizedString(@"HeaderText", nil),(int)self.earnedPoints];
    
    self.labFirstName.text = self.contact.firstName;
    self.labLastName.text = self.contact.lastName;
    self.ivProfileImage.image = self.contact.profileImage;
    
    self.labOrderNumberValue.text = self.transactionId;
    self.labOrderTotalValue.text = [NSString stringWithFormat:FORMAT_MONEY,self.orderAmount];
    self.labOrderPaymentValue.text = [NSString stringWithFormat:FORMAT_MONEY,self.paymentAmount];
    self.labPhoneNumber.text = [Utils getFormattedPhoneNumber:self.recipientPhoneNumber  withFormat:@"."];
    self.labDate.text = [Utils getTimeString:self.deliveryDate withFormat:@"MMM dd yyyy"];
    self.labGiftcardAmount.text = [NSString stringWithFormat:FORMAT_MONEY,self.orderAmount];
    
    self.labGiftcardTitle.text = self.giftcard.retailer;
    self.cvGiftcard.giftcardName = self.giftcard.retailer;
    self.cvGiftcard.giftcard = self.giftcard;
    
    if (self.isRegifting){
        self.labHeader.hidden = YES;
    }
    [self.cvGiftcard updateViews];
}



@end
