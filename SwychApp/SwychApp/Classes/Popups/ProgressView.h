//
//  ProgressView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewBase.h"

@interface ProgressView : PopupViewBase
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityProgress;
@property (weak, nonatomic) IBOutlet UILabel *labLoading;

-(void)startAnimation;
-(void)startAnimationWithText:(NSString*)text;
-(void)stopAnimation;

-(void)setLoadingText:(NSString*)text;
-(NSString*)getLoadingText;

@end
