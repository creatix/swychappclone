//
//  PopupConfirmationView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"

@implementation PopupConfirmationView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setupWhenAwakeFromNib];
}


-(void)setupWhenAwakeFromNib{
    
}
-(void)viewWillShow{
    
}

-(void)updateView{
    [self viewWillShow];
}

-(void)show{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    [self viewWillShow];
    
    self.frame = CGRectMake(0, win.frame.size.height, win.frame.size.width, win.frame.size.height);
    [win addSubview:self];
    [win bringSubviewToFront:self];
    
    
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.frame = CGRectMake(0, 0, win.frame.size.width, win.frame.size.height);
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDidStopSelector:@selector(removeView)];
    [UIView commitAnimations];
}

-(void)dismiss{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.frame = CGRectMake(0, win.frame.size.height, self.frame.size.width, self.frame.size.height);
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeView)];
    [UIView commitAnimations];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(popupConfirmationViewDissmissed:)]){
        [self.delegate popupConfirmationViewDissmissed:self];
    }
}

- (IBAction)buttonTapped:(id)sender {
    [self dismiss];
}

-(void)removeView{
    [self removeFromSuperview];
}


@end
