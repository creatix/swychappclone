//
//  UserInfoUpdatePopupView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UserInfoUpdatePopupView.h"
#import "UserInfoUpdateView.h"
#import "LocalUserInfo.h"

@interface UserInfoUpdatePopupView()
@property (weak, nonatomic) IBOutlet UserInfoUpdateView *vInfoUpdate;
@property (weak, nonatomic) IBOutlet UIImageView *ivViewBackground;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIView *vInfo;

@end

@implementation UserInfoUpdatePopupView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    
    
}

-(void)viewWillShow{
    [super viewWillShow];
    self.vInfoUpdate.delegate = (id<UserInfoUpdateViewDelegate>)self;
    self.vInfo.layer.cornerRadius = 8.0f;
    self.vInfoUpdate.context = self.context;
    self.vInfoUpdate.contextObj = self.contextObj;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate =(id<UIGestureRecognizerDelegate>) self;
    [self.ivViewBackground addGestureRecognizer:tap];
    self.ivViewBackground.userInteractionEnabled = YES;

    self.labTitle.backgroundColor = self.vInfoUpdate.backgroundColor;
    self.labTitle.text = NSLocalizedString(@"TitleUserInfoUpdateView", nil);
}

-(void)touchOnView:(id)sender{
    [self dismiss];
}

#pragma
#pragma mark -UserInfoUpdateViewDelegate
-(void)userInfoUpdateView:(UserInfoUpdateView*)view infoUpdated:(ModelUser*)user{
    if (user != nil){
        LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
        lui.firstName = user.firstName;
        lui.lastName = user.lastName;
        lui.email = user.email;
        lui.phoneNumber = user.phoneNumber;
        [[LocalStorageManager instance] saveLocalUserInfo:lui];
        
        [self dismiss];
        
        if (self.delegateUpdate != nil && [self.delegateUpdate respondsToSelector:@selector(userInfoUpdateView:infoUpdated:)]){
            [self.delegateUpdate userInfoUpdateView:view infoUpdated:user];
        }
    }
}
@end
