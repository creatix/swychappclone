//
//  ReceiveGiftcardPopupView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ReceiveGiftcardPopupView.h"
#import "UIRoundCornerButton.h"
#import "ModelTransaction.h"
#import "UIRoundCornerImageView.h"
#import "UIGiftcardView.h"
#import "ContactBookManager.h"
#import "ModelUser.h"
#import "Utils.h"
#import "ModelGiftAttachments.h"
#import "NSString+Extra.h"
#import "ModelGiftCard.h"
#import "DisplayPicturePopupView.h"

@interface ReceiveGiftcardPopupView(){
    ModelTransaction *currentTransaction;
}
@property (weak, nonatomic) IBOutlet UIImageView *ivViewBackground;
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;

@property (weak, nonatomic) IBOutlet UIView *vMessage;
@property (weak, nonatomic) IBOutlet UILabel *labSenderName;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayMessage;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;


@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (weak, nonatomic) IBOutlet UIView *vGiftcardPlaceHolder;
@property (weak, nonatomic) IBOutlet UIGiftcardView *cvGiftcardView;

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSaveGiftcard;
@property (weak, nonatomic) IBOutlet UILabel *labMessageBottom;
@property (weak, nonatomic) IBOutlet UIView *vVideoPictureButtons;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;
@property (weak, nonatomic) IBOutlet UIButton *btnDisplayPicture;

@end

@implementation ReceiveGiftcardPopupView

@synthesize transactions = _transactions;
-(void)setTransactions:(NSArray<ModelTransaction *> *)transactions{
    _transactions = transactions;
    currentTransaction = _transactions[0];
}
-(NSArray<ModelTransaction*>*)transactions{
    return _transactions;
}

-(void)setupWhenAwakeFromNib{
    self.vMessage.backgroundColor = UICOLOR_CLEAR;
    self.vBottom.backgroundColor = UICOLOR_CLEAR;
    self.vGiftcardPlaceHolder.backgroundColor = UICOLOR_CLEAR;
    
}
-(void)viewWillShow{
    [self.btnPlayMessage setTitle:NSLocalizedString(@"Button_PlayVideoMessage", nil)];
    [self.btnSaveGiftcard setTitle:NSLocalizedString(@"Button_Accept", nil)];
    NSString *sender = nil;
    if (currentTransaction.sender != nil){
        sender = [ContactBookManager getContactFirstNameByPhoneNumber:currentTransaction.sender.phoneNumber];
        if (sender == nil){
            sender = currentTransaction.sender.firstName;
        }
    }
    if (sender == nil || sender.length == 0){
        sender =[Utils getFormattedPhoneNumber:currentTransaction.sender.phoneNumber withFormat:@"."];
    }
    UIImage *img = [ContactBookManager getContactHeadImgByPhoneNumber:currentTransaction.sender.phoneNumber];
    if (img != nil){
        self.ivAvatar.image = img;
    }
    NSString *format =NSLocalizedString(@"Title_GiftFrom", nil);
    self.labTitle.text = [NSString stringWithFormat:format,currentTransaction.amount,sender];
    self.txtMessage.text = currentTransaction.sentMessage.message;
    self.labSenderName.text = [NSString stringWithFormat:@"- %@",sender];
    self.labMessageBottom.text = NSLocalizedString(@"Message_Bottom", nil);
    
    self.cvGiftcardView.viewType = GiftcardView_GiftcardImageOnly;
    self.cvGiftcardView.giftcard = currentTransaction.giftCard;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate =(id<UIGestureRecognizerDelegate>) self;
    [self.ivViewBackground addGestureRecognizer:tap];
    self.ivViewBackground.userInteractionEnabled = YES;
    
    if (currentTransaction != nil){
        self.btnDisplayPicture.hidden = [NSString isNullOrEmpty:currentTransaction.sentMessage.imgUrl];
        self.btnPlayVideo.hidden = [NSString isNullOrEmpty:currentTransaction.sentMessage.videoUrl];
    }
}

- (IBAction)buttonTapped:(id)sender{
    if (sender == self.btnSaveGiftcard){
        if (self.receiveGiftcardPopupViewDelegate != nil && [self.receiveGiftcardPopupViewDelegate respondsToSelector:@selector(receiveGiftcardPopupView:giftcardAccepted:)]){
            [self.receiveGiftcardPopupViewDelegate receiveGiftcardPopupView:self giftcardAccepted:currentTransaction];
        }
        [self dismiss];
        return;
    }
    else if(sender == self.btnPlayVideo){
        return;
    }
    else if (sender == self.btnDisplayPicture){
        DisplayPicturePopupView *v = (DisplayPicturePopupView*)[Utils loadViewFromNib:@"DisplayPicturePopupView" viewTag:1];
        v.imageURL = currentTransaction.sentMessage.imgUrl;
        
        [v show];
        return;
    }
    return [super buttonTapped:sender];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
        return YES;
}
-(void)touchOnView:(id)sender{
    [self dismiss];
}

@end
