//
//  ViewOTPVerification.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ViewOTPVerification.h"
#import "UITextFieldExtra.h"
#import "ModelUser.h"
#import "UIRoundCornerButton.h"


@interface ViewOTPVerification()
@property (weak, nonatomic) IBOutlet UIView *vMiddleView;
@property (weak, nonatomic) IBOutlet UIView *vBottomView;

@property (weak, nonatomic) IBOutlet UILabel *labVerCode;
@property (weak, nonatomic) IBOutlet UILabel *labWeHaveTexted;
@property (weak, nonatomic) IBOutlet UILabel *labPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextFieldExtra *txtVerificatinCode;
@property (weak, nonatomic) IBOutlet UILabel *labVertLine;
@property (weak, nonatomic) IBOutlet UILabel *labHorzLine;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnResend;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSubmit;
@end
@implementation ViewOTPVerification

-(void)awakeFromNib{
    UIColor *clrText = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.vMiddleView.backgroundColor = [UIColor clearColor];
    self.vBottomView.backgroundColor = [UIColor clearColor];
    
    self.txtVerificatinCode.layer.cornerRadius = 10.0f;
    self.txtVerificatinCode.backgroundColor = UICOLORFROMRGB(209, 209, 209, 10);
    self.txtVerificatinCode.offsetX = 8.0f;
    self.txtVerificatinCode.textColor = clrText;
    
    self.labVerCode.text = NSLocalizedString(@"VerificationCode", nil);
    self.labVerCode.textColor = clrText;
    
    self.labWeHaveTexted.text = NSLocalizedString(@"JustTextedCodeTo", nil);
    self.labWeHaveTexted.textColor = clrText;
    
    self.labPhoneNumber.textColor = clrText;
    
    self.labVertLine.textColor = UICOLORFROMRGB(163, 162, 162, 1.0);
    self.labHorzLine.textColor = self.labVertLine.textColor;
    
    [self.btnResend setTitle:NSLocalizedString(@"Button_Resend", nil) forState:UIControlStateNormal];
    [self.btnResend setTitle:NSLocalizedString(@"Button_Resend", nil) forState:UIControlStateHighlighted];
    self.btnResend.backgroundColor = UICOLORFROMRGB(209, 210, 210, 1.0);
    [self.btnResend setTitleColor:UICOLORFROMRGB(76, 77, 77, 1.0)];
    
    [self.btnSubmit setTitle:NSLocalizedString(@"Button_Submit", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"Button_Submit", nil) forState:UIControlStateHighlighted];
    self.btnSubmit.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    [self.btnSubmit setTitleColor:[UIColor whiteColor]];
    _bTapDissMiss = NO;
    self.btnClose.hidden = YES;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        self.txtVerificatinCode.layer.cornerRadius = 10.0f;
        self.txtVerificatinCode.backgroundColor = UICOLORFROMRGB(209, 209, 209, 10);
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setTextCode:(NSString*)text{
    _textCode = text;
    self.labWeHaveTexted.text = _textCode;
    self.labVerCode.text = NSLocalizedString(@"VerificationCodeViaEmail", nil);
    _btnClose.hidden = NO;
}
-(BOOL)allowTouchToDismiss{
    return _bTapDissMiss;
}

-(NSString*)getOTPCode{
    return self.txtVerificatinCode.text;
}

-(void)setMobilePhoneNumber:(NSString*)phoneNumber{
    self.labPhoneNumber.text = phoneNumber;
}
-(NSString*)getMobilePhoneNumber{
   return  self.labPhoneNumber.text;
}
- (IBAction)buttonTapped:(id)sender {
    if(sender == _btnClose){

        [Utils showAlert:NSLocalizedString(@"CancelPurchase_Confirm", nil)
                   title:nil delegate:(id<AlertViewDelegate>)self
             firstButton:NSLocalizedString(@"Button_Cancel", nil)
            secondButton:NSLocalizedString(@"Button_OK", nil)];
        return;
    }
    if (self.verificationDelegate != nil){
        if (sender == self.btnResend && [self.verificationDelegate respondsToSelector:@selector(OTPVerificationResendOTP:)]){
            [self.verificationDelegate OTPVerificationResendOTP:self];
        }
        else if (sender == self.btnSubmit && [self.verificationDelegate respondsToSelector:@selector(OTPVerificationResendOTP:)]){
            if ([self.txtVerificatinCode.text length] == 0){
                [Utils showAlert:NSLocalizedString(@"EmptyOTP", nil) delegate:nil];
                return;
            }
            [self.verificationDelegate OTPVerificationSubmitOTP:self value:self.txtVerificatinCode.text];
        }
    }
    [self dismiss];
}
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (index == 2){
        [alertView dismiss];
        [self dismiss];
        if(self.verificationDelegate != nil && [self.verificationDelegate respondsToSelector:@selector(OTPVerificationCancelled:)]){
            [self.verificationDelegate OTPVerificationCancelled:self];
        }
    }
    return YES;
}
@end
