//
//  AlertViewNormal.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewBase.h"

@class TTTAttributedLabel;
@protocol AlertViewDelegate;


@interface AlertViewNormal : PopupViewBase{
    BOOL _singledButton;
}
@property (assign,nonatomic) id<AlertViewDelegate> alertViewDelegate;
@property (nonatomic,strong) NSString *checkboxText;

-(void)setTitle:(NSString*)title message:(NSString*)message buttonText:(NSString*)first,...;
-(void)setCheckboxState:(BOOL)checked;

@end


@protocol AlertViewDelegate <NSObject>

@optional
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index;
-(void)alertView:(AlertViewNormal*)alertView checkboxValueChanged:(BOOL)checked;
@end
