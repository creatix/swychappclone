//
//  PopupViewBase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupViewBase : UIView
@property(nonatomic,assign) NSInteger context;
@property(nonatomic,strong) NSObject *contextObject;
@property(nonatomic,strong) NSObject *contextObject2;

+(void)dismissCurrentPopup;
-(void)dismiss;
-(BOOL)allowTouchToDismiss;

-(void)adjustControls;
@end
