//
//  LocalStorageManager.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LocalUserInfo;
@class ModelBotTransaction;
@interface LocalStorageManager : NSObject

+(LocalStorageManager*)instance;

-(void)saveLocalUserInfo:(LocalUserInfo*)userInfo;
-(LocalUserInfo*)getLocalStoredUserInfo;
-(void)clearStoredUserInfo;

-(NSString*)getStoredSwychId;

-(void)saveResourceVersion:(NSInteger)resVersion;
-(NSInteger)getCurrentResourceVersion;

-(void)saveUserAvatarImage:(UIImage*)image;
-(UIImage*)getUserAvatarImage;
-(void)saveBotTransaction:(ModelBotTransaction*)botTransaction;
-(ModelBotTransaction*)getBotTransaction;
-(void)setAppInstalledFlag;
-(BOOL)getAppInstalledFlag;

@end
