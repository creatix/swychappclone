//
//  GiftcardWithBalanceAndWalletButtonView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftcardWithBalanceAndWalletButtonView.h"
#import "UIGiftcardView.h"
#import "ModelGiftCard.h"
#import "UIButton+Extra.h"
#import "PassHandler.h"
#import "RequestUpdateGiftcardBalance.h"
#import "ResponseUpdateGiftcardBalance.h"
#import "ManullyUpdateBalance.h"
#import "RequestManuallyUpdateBalance.h"
#import "ResponseManuallyUpdateBalance.h"
#import "UILabel+Extra.h"
#define Context_Prompt_RemovePass   0x01

@interface GiftcardWithBalanceAndWalletButtonView ()
@property (weak, nonatomic) IBOutlet UIView *vBalance;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToWallet;
@property (weak, nonatomic) IBOutlet UIGiftcardView *vGiftcard;

//ViewBalance
@property (weak, nonatomic) IBOutlet UIButton *btnRefreshBalance;
@property (weak, nonatomic) IBOutlet UILabel *labAmount;

- (IBAction)buttonTapped:(id)sender;
@end

@implementation GiftcardWithBalanceAndWalletButtonView

@synthesize giftCard = _giftCard;

-(void)setGiftCard:(ModelGiftCard *)giftCard{
    _giftCard = giftCard;
    self.vGiftcard.giftcard = giftCard;
    [self.labAmount AmountFormatWithDMark:giftCard.amount fontsize:10.0f lengthOfRange:2 ifFromTheLeft:NO];
    //check if the pass in the wallet
    [self updateAddToWalletButton];
}

-(ModelGiftCard*)giftCard{
    return _giftCard;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"GiftcardWithBalanceAndWalletButtonView" class:[self class] owner:self options:nil];
    
    view.backgroundColor = [UIColor clearColor];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.vBalance.backgroundColor = [UIColor clearColor];
    self.vGiftcard.frontOnly = NO;
    self.vGiftcard.labFrontViewAmount.hidden = YES;
  
    [self addSubview:view];
    
    [self setup];
}

-(void)setup{
    self.vGiftcard.giftcard = self.giftCard;
    _btnAddToWallet.layer.cornerRadius = 10.f;
    
    [self updateAddToWalletButton];
    
}

-(void)updateSubviews{
    [self.vGiftcard updateViews];
}

-(void)setBalanceLabelText:(double)newBalance{
    [self.labAmount AmountFormatWithDMark:newBalance fontsize:10.0f lengthOfRange:2 ifFromTheLeft:NO];
}


-(void)startUpdateBalance{
    [self sendBalanceUpdateRequest];
}

-(void)updateAddToWalletButton{
    if ([[PassHandler instance] passExists:[self.giftCard getGiftcardSerialNumberForWalletPass]]){
        [self.btnAddToWallet setButtonImage:nil];
        [self.btnAddToWallet setTitle:NSLocalizedString(@"Button_RemoveFromWallet", nil)];
        self.btnAddToWallet.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
        self.btnAddToWallet.titleLabel.textColor = [UIColor whiteColor];
    }
    else{
        [self.btnAddToWallet setButtonImage:[UIImage imageNamed:@"add_to_apple_wallet.png"]];
        [self.btnAddToWallet setTitle:nil];
    }
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnAddToWallet){
        if([[PassHandler instance] passExists:[self.giftCard getGiftcardSerialNumberForWalletPass]]){
            [Utils showAlert:NSLocalizedString(@"PromptRemovePass", nil)
                       title:nil
                    delegate:self
                 firstButton:NSLocalizedString(@"Button_NO", nil)
                secondButton:NSLocalizedString(@"Button_YES", nil)
                 withContext:Context_Prompt_RemovePass
                              withContextObject:nil checkboxText:nil];
        }
        else{
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(giftcardWithBalanceAndWalletButtonViewStartAddPassToWallet:)]){
                [self.delegate giftcardWithBalanceAndWalletButtonViewStartAddPassToWallet:self];
            }
        }
    }
    else if (sender == self.btnRefreshBalance){
        if ([self.btnRefreshBalance isAnimating]){
            return;
        }
        [self.btnRefreshBalance startAnimation];
        [self startUpdateBalance];
    }
}
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (alertView.context == Context_Prompt_RemovePass && index == 2){
        [[PassHandler instance] RemovePass:[self.giftCard getGiftcardSerialNumberForWalletPass]];
        [self updateAddToWalletButton];
    }
    return YES;
}
#pragma 
#pragma mark - Handle server communication
-(void)sendBalanceUpdateRequest{
    RequestUpdateGiftcardBalance *req = [[RequestUpdateGiftcardBalance alloc] init];
    req.giftcardNumber = self.giftCard.giftCardNumber;
    
    SEND_REQUEST(req, handleBalanceUpdateResponse, handleBalanceUpdateResponse);
}

-(void)handleBalanceUpdateResponse:(ResponseBase*)response{
    if ([self.btnRefreshBalance isAnimating]){
        [self.btnRefreshBalance stopAnimation];
    }
    
    if (response.success){
        [self setBalanceLabelText:((ResponseUpdateGiftcardBalance*)response).balance];
    }
    else{
        if(response.errorCode == 11300027){
        ManullyUpdateBalance *v = (ManullyUpdateBalance*)[Utils loadViewFromNib:@"ManullyUpdateBalance" viewTag:10];
        v.labTitle.text = NSLocalizedString(@"UpdateBalance",nil);
            NSString* message = response.errorMessage;
        if(response.errorMessage == nil || [response.errorMessage isEqualToString:@""]){
            message = NSLocalizedString(@"Default_manually_update",nil);
        }
        v.labWeHaveTexted.text = message;
            v.vDelegate = self;
        [Utils showPopupView:(PopupViewBase*)v];
        }
        else{

        [Utils showAlert:response.errorMessage delegate:nil];
        }
    }
}
-(void)ManullyUpdateBalance:(NSString*)Balance{
    RequestManuallyUpdateBalance *req = [[RequestManuallyUpdateBalance alloc] init];
    req.giftcardNumber = self.giftCard.giftCardNumber;
    req.balance = [Balance doubleValue];
    SEND_REQUEST(req, handleManuallyBalanceUpdateResponse, handleManuallyBalanceUpdateResponse);

}
-(void)handleManuallyBalanceUpdateResponse:(ResponseBase*)response{

    
    if (response.success){
        [self setBalanceLabelText:((ResponseManuallyUpdateBalance*)response).balance];
    }
    else{
            [Utils showAlert:response.errorMessage delegate:nil];

    }
}
@end
