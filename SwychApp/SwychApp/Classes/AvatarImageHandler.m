//
//  AvatarImageHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AvatarImageHandler.h"


@interface AvatarImageHandler()

@property(nonatomic,weak) id<AvatarImageHandlerDelegate> del;
@property(nonatomic,weak) UIImage *sourceImage;
@property(nonatomic,weak) UIViewController *viewController;
@end

@implementation AvatarImageHandler
+(AvatarImageHandler*)instance{
    static AvatarImageHandler *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}


-(void)handleAvatarImage:(UIImage*)source viewController:(UIViewController*)viewController delegate:(id<AvatarImageHandlerDelegate>)del{
    self.del = del;
    self.sourceImage = source;
    self.viewController = viewController;
    
    RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:self.sourceImage];
    imageCropVC.delegate = self;
    [self.viewController presentViewController:imageCropVC animated:YES completion:nil];
}

#pragma 
#pragma  mark - RSKImageCropViewControllerDelegate
// Crop image has been canceled.
- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    if (self.del != nil && [self.del respondsToSelector:@selector(avatarImageHdnalerDidCancelled:)]){
        [self.del avatarImageHdnalerDidCancelled:self];
    }
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    if (self.del != nil && [self.del respondsToSelector:@selector(avatarImageHandler:didSelectedImage:)]){
        [self.del avatarImageHandler:self didSelectedImage:croppedImage];
    }
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    if (self.del != nil && [self.del respondsToSelector:@selector(avatarImageHandler:didSelectedImage:)]){
        [self.del avatarImageHandler:self didSelectedImage:croppedImage];
    }
}

// The original image will be cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                  willCropImage:(UIImage *)originalImage
{
    // Use when `applyMaskToCroppedImage` set to YES.
    //[SVProgressHUD show];
}
@end
