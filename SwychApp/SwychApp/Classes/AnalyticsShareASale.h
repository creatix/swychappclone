//
//  AnalyticsShareASale.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "Analytics.h"

@interface AnalyticsShareASale : Analytics

-(void)trackInstallWithView:(UIView*)view;
-(void)trackTransaction:(NSString*)tranNumber amount:(double)amount purchaseForFriend:(BOOL)forFriend;
@end
