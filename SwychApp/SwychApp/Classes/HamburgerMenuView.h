//
//  HamburgerMenuView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIRoundCornerImageView.h"
@class MenuItem;
@protocol HambergerMenuViewDelegate;

@interface HamburgerMenuView : UIView<UIRoundCornerImageViewDelegate>

@property(weak,nonatomic) id<HambergerMenuViewDelegate> menuDelegate;
-(void)updateInfo;

@end


@protocol HambergerMenuViewDelegate <NSObject>

@optional
-(void)hamburgerMenuItemTapped:(MenuItem*)menuItem;

@end