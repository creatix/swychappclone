//
//  CreditPointsManager.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ModelCreditPointsForPurchase;
@class LocalUserInfo;
@class ModelGiftCard;
@class ModelTransaction;

@interface CreditPointsManager : NSObject

+(CreditPointsManager*)instance;
+(double)getValueByPoints:(NSInteger)points;
+(NSInteger)getPointsByValue:(double)value;

-(ModelCreditPointsForPurchase*)getCreditPointsForPurchase:(double)purchaseAmount credits:(NSArray<ModelTransaction*>*)credits promotionDiscount:(double)discount;
@end
