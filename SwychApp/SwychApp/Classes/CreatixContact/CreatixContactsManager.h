//
//  CreatixContactsManager.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreatixContactsManager : NSObject

@property(nonatomic,assign) BOOL isFirstTimeSync;

+(CreatixContactsManager*)shareInstance;

-(void)startSyncContacts;
@end
