//
//  CreatixContactsManager.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CreatixContactsManager.h"

@import SwychContacts;


#import "RequestContactSyncAccessToken.h"
#import "ResponseContactSyncAccessToken.h"

@interface CreatixContactsManager (SCNContactsSyncDelegate) <SCNContactsSyncDelegate>

@end

@implementation CreatixContactsManager


+(CreatixContactsManager*)shareInstance{
    static CreatixContactsManager *g_instanceCreatixContactsManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instanceCreatixContactsManager = [[self alloc] init];
    });
    return g_instanceCreatixContactsManager;
}


-(void)startSyncContacts{
    [self sendContactSyncAccessTokenRequest];
}


#pragma
#pragma mark - Server commnication
-(void)sendContactSyncAccessTokenRequest{
    RequestContactSyncAccessToken *req = [[RequestContactSyncAccessToken alloc] init];
    SEND_REQUEST(req, handleContactSyncAccessTokenResponse, handleContactSyncAccessTokenResponse)
}
-(void)handleContactSyncAccessTokenResponse:(ResponseBase*)response{
    if (response.success){
        ResponseContactSyncAccessToken *resp = (ResponseContactSyncAccessToken*)response;
        [self startContactSync:resp.contactSyncToken];
    }
    else{
        NSLog(@"Failed to get contactSync Access token: %@",response.errorMessage);
    }
}

#pragma
#pragma mark Private methods

-(void)startContactSync:(NSString*)accessToken{
    [SCNContactSyncManager sharedInstance].enabledDEBUG = YES;
    [SCNContactSyncManager sharedInstance].enableSending = YES;
    
    
    if ([APP callContactSyncResetNeeded]){
        NSLog(@"calling contactSync.reset()");
        [[SCNContactSyncManager sharedInstance] reset];
        [APP setCallContactSyncResetNeeded:NO];
    }
    [SCNContactSyncManager sharedInstance].accessToken = accessToken;
    [[SCNContactSyncManager sharedInstance] addWithDelegate:self];

    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"calling contactSync.start()");
        [[SCNContactSyncManager sharedInstance] start];
    });
    DEBUGLOG(@"Contact Sync is started.");
}
@end

@implementation CreatixContactsManager (SCNContactsSyncDelegate)

- (void)contactsSyncDidStart:(SCNContactSyncManager *)contactsSync {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)contactsSyncDidFinish:(SCNContactSyncManager *)contactsSync {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)contactsSync:(SCNContactSyncManager *)contactsSync didFail:(enum GSContactsSyncState)state error:(NSError *)error {
    NSLog(@"%s: %ld \n\tERROR: %@", __PRETTY_FUNCTION__, (long)state, error);
}
@end
