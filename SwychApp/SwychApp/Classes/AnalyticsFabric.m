//
//  AnalyticsFabric.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticsFabric.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Crashlytics/Answers.h>
#import "TrackingEventBase.h"

@implementation AnalyticsFabric


+(Analytics*)instance{
    static Analytics *g_instanceFabric = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instanceFabric = [[self alloc] init];
        g_instanceFabric.ignoredEvents = nil;
    });
    return g_instanceFabric;
}

-(void)fireCrash{
    [[Crashlytics sharedInstance] crash];
}

-(void)register{
    [Fabric with:@[[Crashlytics class]]];
}

-(void)logSignUpWithMethod:(nullable NSString*)methodOrNil
                   success:(nullable NSNumber*)successOrNil
                attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logSignUpWithMethod:methodOrNil success:successOrNil customAttributes:attributesOrNil];
}

-(void)logLoginWithMethod:(nullable NSString*)methodOrNil
                   sucess:(nullable NSNumber*)successOrNil
               attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logLoginWithMethod:methodOrNil success:successOrNil customAttributes:attributesOrNil];
}

-(void)logShareWithMethod:(nullable NSString*)methodOrNil
              contentName:(nullable NSString*)contentNameOrNil
              contentType:(nullable NSString*)contentTypeOrNil
                contentId:(nullable NSString*)contentIdOrNil
               attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logShareWithMethod:methodOrNil contentName:contentNameOrNil contentType:contentTypeOrNil contentId:contentIdOrNil customAttributes:attributesOrNil];
}

-(void)logInviteWithMethod:(nullable NSString*)methodOrNil
                attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logInviteWithMethod:methodOrNil customAttributes:attributesOrNil];
}

-(void)logPurchaseWithPrice:(nullable NSDecimalNumber*)itemPriceOrNil
                   currency:(nullable NSString*)currencyOrNil
                    success:(nullable NSNumber*)purchaseSucceededOrNil
                   itemName:(nullable NSString*)itemNameOrNil
                   itemType:(nullable NSString*)itemTypeOrNil
                     itemId:(nullable NSString*)itemIdOrNil
                 attributes:(nullable NSDictionary*) attributesOrNil{
    [Answers logPurchaseWithPrice:itemPriceOrNil currency:currencyOrNil success:purchaseSucceededOrNil itemName:itemNameOrNil itemType:itemTypeOrNil itemId:itemIdOrNil customAttributes:attributesOrNil];
}

-(void)logGameLevelStart:(nullable NSString*)levelNameOrNil
              attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logLevelStart:levelNameOrNil customAttributes:attributesOrNil];
}

-(void)logGameLevelEnd:(nullable NSString*)levelNameOrNil
                 score:(nullable NSNumber*)scoreOrNil
               success:(nullable NSNumber*)levelCompletedSuccesfullyOrNil
            attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logLevelEnd:levelNameOrNil score:scoreOrNil success:levelCompletedSuccesfullyOrNil customAttributes:attributesOrNil];
}

-(void)logAddToCartWithPrice:(nullable NSDecimalNumber*)itemPriceOrNil
                    currency:(nullable NSString*)currencyOrNil
                    itemName:(nullable NSString*)itemNameOrNil
                    itemType:(nullable NSString*)itemTypeOrNil
                      itemId:(nullable NSString*)itemIdOrNil
                  attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logAddToCartWithPrice:itemPriceOrNil currency:currencyOrNil itemName:itemNameOrNil itemType:itemTypeOrNil itemId:itemIdOrNil customAttributes:attributesOrNil];
}

-(void)logStartCheckoutWithPrice:(nullable NSDecimalNumber*)totalPriceOrNil
                        currency:(nullable NSString*)currencyOrNil
                       itemCount:(nullable NSNumber*)itemCountOrNil
                      attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logStartCheckoutWithPrice:totalPriceOrNil currency:currencyOrNil itemCount:itemCountOrNil customAttributes:attributesOrNil];
}

-(void)LogRating:(nullable NSNumber*)ratingOrNil
     contentName:(nullable NSString*)contentNameOrNil
     contentType:(nullable NSString*)contentTypeOrNil
       contentId:(nullable NSString*)contentIdOrNil
      attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logRating:ratingOrNil contentName:contentNameOrNil contentType:contentTypeOrNil contentId:contentIdOrNil customAttributes:attributesOrNil];
}

-(void)logContentViewWithName:(nullable NSString*)contentNameOrNil
                  contentType:(nullable NSString*)contentTypeOrNil
                    contentId:(nullable NSString*)contentIdOrNil
                   attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logContentViewWithName:contentNameOrNil contentType:contentTypeOrNil contentId:contentIdOrNil customAttributes:attributesOrNil];
}

-(void)logSearchWithQuery:(nullable NSString*)queryOrNil
               attributes:(nullable NSDictionary*)attributesOrNil{
    [Answers logSearchWithQuery:queryOrNil customAttributes:attributesOrNil];
}

-(void)logCustomEvent:(nullable TrackingEventBase *)event{
    if ([self isIgnoredEvent:event]){
        return;
    }
    [Answers logCustomEventWithName:event.eventName customAttributes:event.customValues];
}


@end
