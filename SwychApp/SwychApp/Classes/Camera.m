 //
//  Camera.m
//  TestCamera
//
//  Created by Donald Hancock on 2/28/16.
//  Copyright © 2016 com.threeOtwo. All rights reserved.
//

#import "Camera.h"
#import <ImageIO/ImageIO.h>

NSString * const CAMERA_UTILITY_ERROR_DOMAIN = @"Error.Domain.CameraUtility";

typedef union  {
    UInt8 captureMask;
    struct {
        BOOL adjustingFocus : 1;
        BOOL adjustingWhiteBalance : 1;
        BOOL adjustingExposure : 1;
        UInt8 :4;
        BOOL isCapturePending : 1;
    } ;
} CaptureMask;

AVCaptureVideoOrientation convertOrientation(UIDeviceOrientation deviceOrientation) {
    return AVCaptureVideoOrientationPortrait;
}

static CGFloat clampZoom(CGFloat desiredZoom, AVCaptureDevice* _captureDevice) {
    if(_captureDevice == nil) return 1;
    else return MIN(_captureDevice.activeFormat.videoMaxZoomFactor, MAX(1, desiredZoom));
}

@interface Camera ()
    <AVCaptureMetadataOutputObjectsDelegate> {
    AVCaptureStillImageOutput*  _stillOutput;
    AVCaptureMetadataOutput*    _metaOutput;
    AVCaptureDeviceInput*       _captureDeviceInput;
    AVCaptureDevice*            _captureDevice;
    AVCaptureSession*           _session;
    CaptureMask                 _captureMask;
        
    void(^_captureImageCallback)(NSData*, NSError*);
        
        BOOL delegateCalled;
}

@end

@implementation Camera

NSString* const sProperties[] = {
    @"adjustingFocus",
    @"adjustingExposure",
    @"adjustingWhiteBalance"
};

const NSUInteger sPropCount = sizeof(sProperties)/sizeof(NSString*);

- (void) removeObservers {
    for(int counter = 0; counter < sPropCount; counter++) {
        @try {
            [_captureDevice removeObserver:self forKeyPath:sProperties[counter]];
        }
        @catch (NSException *exception) { }
    }
}

- (void) addObservers  {
    for(int counter = 0; counter < sPropCount; counter++) {
        @try {
            [_captureDevice addObserver:self
                             forKeyPath:sProperties[counter]
                                options:NSKeyValueObservingOptionNew
                                context:nil];
        }
        @catch (NSException *exception) { }
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary<NSString *,id> *)change
                        context:(void *)context {
    
    if([@"adjustingFocus" isEqualToString:keyPath]) {
        NSNumber* val = [change objectForKey:NSKeyValueChangeNewKey];
        _captureMask.adjustingFocus = val.boolValue;
    }
    else if([@"adjustingExposure" isEqualToString:keyPath]) {
        NSNumber* val = [change objectForKey:NSKeyValueChangeNewKey];
        _captureMask.adjustingExposure = val.boolValue;
    }
    else if([@"adjustingWhiteBalance" isEqualToString:keyPath]) {
        NSNumber* val = [change objectForKey:NSKeyValueChangeNewKey];
        _captureMask.adjustingWhiteBalance = val.boolValue;
    }
    
    if(_captureMask.captureMask == 0x80) {
        NSLog(@"---->%@", @"Recapture");
        [self doCaptureImage];
    }
}

- (instancetype) init:(AVCaptureDevice*)captureDevice {
    if((self = [super init]) != nil) {
        _captureDevice = captureDevice;
        _captureMask.captureMask = 0;
        _focusPoint = CGPointMake(.5, .5);
        _zoom = 1;
    }
    return self;
}

- (void) doCaptureImage {
    AVCaptureConnection* captureConnection = nil;
    for(AVCaptureConnection* connection in _stillOutput.connections) {
        for(AVCaptureInputPort* port in connection.inputPorts) {
            if([AVMediaTypeVideo isEqual:port.mediaType]) {
                captureConnection = connection;
                break;
            }
        }
        if(captureConnection != nil) {
            break;
        }
    }
    
    if(captureConnection != nil) {
        __block void(^callback)(CMSampleBufferRef, NSError*) = ^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            if(error != nil) {
                NSError* newErr = [NSError errorWithDomain:CAMERA_UTILITY_ERROR_DOMAIN
                                                      code:kCameraUtilityError_ImageCaptureFailed
                                                  userInfo:error.userInfo];
                _captureImageCallback(nil, newErr);
            }
            else {
                NSData* jpegData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];                
                _captureImageCallback(jpegData, nil);
            }
            _captureImageCallback = nil;
        };
        
        [_stillOutput captureStillImageAsynchronouslyFromConnection:captureConnection
                                                  completionHandler:callback];
    }
}

+ (NSArray<Camera*>*) loadCameras {
    NSArray<AVCaptureDevice*>* inputDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    NSMutableArray<Camera*>* outCameras = [NSMutableArray arrayWithCapacity:inputDevices.count];
    
    for(AVCaptureDevice* device in inputDevices) {
        Camera* camera = [[Camera alloc] init:device];
        [outCameras addObject:camera];
    }
    
    return outCameras;
}

- (void) setOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  
    AVCaptureVideoOrientation captureOrientation = convertOrientation(orientation);
    
    if(_session != nil) {
        for(AVCaptureOutput* output in _session.outputs) {
            for(AVCaptureConnection* con in output.connections) {
                if(con != nil && [con isVideoOrientationSupported])  {
                    [con setVideoOrientation: captureOrientation];
                }
            }
        }
    }
}

- (NSError*) doInLock:(void(^)()) callback {
    NSError* err = nil;
    if([_captureDevice lockForConfiguration:&err]) {
        callback();
        [_captureDevice unlockForConfiguration];
    }
    return err;
}

- (void) turnTorchOnOff{
    if (_captureDevice != nil){
        NSError* err = nil;
        if([_captureDevice lockForConfiguration:&err]) {
            if ([_captureDevice torchMode] == AVCaptureTorchModeOff){
                [_captureDevice setTorchMode:AVCaptureTorchModeOn];
                [_captureDevice setFlashMode:AVCaptureFlashModeOn];
            }
            else{
                [_captureDevice setTorchMode:AVCaptureTorchModeOff];
                [_captureDevice setFlashMode:AVCaptureFlashModeOff];
            }
        }
    }
}

- (void) configureDevice {
    [self doInLock:^() {
        _captureDevice.videoZoomFactor = _zoom;
        
        if([_captureDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
            [_captureDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        }
        
        if(_captureDevice.focusPointOfInterestSupported) {
            [_captureDevice setFocusPointOfInterest:_focusPoint];
        }
        
        if([_captureDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
            [_captureDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        }
        
        if(_captureDevice.exposurePointOfInterestSupported) {
            [_captureDevice setExposurePointOfInterest:CGPointMake(.5, .5)];
        }
        
        if(_captureDevice.isLowLightBoostSupported) {
            [_captureDevice setAutomaticallyEnablesLowLightBoostWhenAvailable:YES];
        }
        
        if([_captureDevice hasFlash]) {
            if([_captureDevice isFlashModeSupported:AVCaptureFlashModeAuto]) {
                [_captureDevice setFlashMode:AVCaptureFlashModeAuto];
            }
            else {
                [_captureDevice setFlashMode:AVCaptureFlashModeOff];
            }
        }
        
        AVCaptureWhiteBalanceMode whiteBalanceModes[] = {
            AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance,
            AVCaptureWhiteBalanceModeAutoWhiteBalance,
            AVCaptureWhiteBalanceModeLocked
        };
        
        for (int counter = 0; sizeof(whiteBalanceModes) / sizeof(AVCaptureWhiteBalanceMode); counter++) {
            AVCaptureWhiteBalanceMode whiteBalanceMode = whiteBalanceModes[counter];
            if([_captureDevice isWhiteBalanceModeSupported:whiteBalanceMode]){
                [_captureDevice setWhiteBalanceMode:whiteBalanceMode];
                break;
            }
        }
    }];
}

#pragma mark - Properties -

- (void) setEnableBarcodeScanning:(BOOL)enableBarcodeScanning {
    if(_enableBarcodeScanning != enableBarcodeScanning) {
        _enableBarcodeScanning = enableBarcodeScanning;

        if(self.enableBarcodeScanning) {
            [_session addOutput:_metaOutput];
            
            _metaOutput.metadataObjectTypes = [_metaOutput availableMetadataObjectTypes];
        }
        else {
            [_session removeOutput: _metaOutput];
        }
    }
}
- (void) setBarcodeProtocol:(NSObject<BarcodeScannerProtocol> *)barcodeProtocol {
    _barcodeProtocol = barcodeProtocol;
}

- (BOOL) isBackCamera {
    return _captureDevice.position == AVCaptureDevicePositionBack;
}

- (void) setZoom:(CGFloat)zoom {
    _zoom = clampZoom(zoom, _captureDevice);
    [self doInLock:^() {
        _captureDevice.videoZoomFactor = _zoom;
    }];
}

-(void) setFocusPoint:(CGPoint)focusPoint {
    focusPoint.x = MAX(0, MIN(1, focusPoint.x));
    focusPoint.y = MAX(0, MIN(1, focusPoint.y));
    
    _focusPoint = focusPoint;
    [self doInLock:^() {
        if(_captureDevice.focusPointOfInterestSupported) {
            [_captureDevice setFocusPointOfInterest:_focusPoint];
            [_captureDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        }
        
        if(_captureDevice.exposurePointOfInterestSupported) {
            [_captureDevice setExposurePointOfInterest:_focusPoint];
            [_captureDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        }

    }];
}

#pragma mark - Public functions -

+ (void) loadCameras:(void(^)(NSArray<Camera*>*, NSError*)) cameraCallback {
    void(^accessGrantedCallback)(BOOL) = ^(BOOL granted){
        if(!granted) {
            NSError* err = [NSError errorWithDomain:CAMERA_UTILITY_ERROR_DOMAIN code:kCameraUtilityError_AccessDenied userInfo:nil];
            cameraCallback(nil, err);
        }
        else {
            dispatch_async(dispatch_queue_create(NULL, NULL), ^{
                NSArray<Camera*>* cameras = [Camera loadCameras];
                
                cameraCallback(cameras, nil);
            });
        }
        
    };
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo
                             completionHandler:accessGrantedCallback];
}

- (void) open:(void(^)(AVCaptureSession*, NSError*)) openCallback {
    NSError* err;
    _captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_captureDevice
                                                                error:&err];
    
    if(_captureDeviceInput == nil) {
        err = [NSError errorWithDomain:CAMERA_UTILITY_ERROR_DOMAIN
                                  code:kCameraUtilityError_CanNotOpenDevice
                              userInfo:err.userInfo];
        openCallback(nil, err);
        return;
    }
    
    _stillOutput = [AVCaptureStillImageOutput new];
    [_stillOutput  setOutputSettings:@{ AVVideoCodecKey : AVVideoCodecJPEG }];
    
    _metaOutput = [AVCaptureMetadataOutput new];
    [_metaOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    _session = [AVCaptureSession new];
    [_session beginConfiguration];
    
    _session.sessionPreset = AVCaptureSessionPresetInputPriority;
    
    [_session addInput:_captureDeviceInput];
    [_session addOutput:_stillOutput];
    
    if(self.enableBarcodeScanning) {
        [_session addOutput:_metaOutput];
    
        _metaOutput.metadataObjectTypes = [_metaOutput availableMetadataObjectTypes];
    }
    
    _captureMask.captureMask = 0;
    _captureMask.adjustingExposure = _captureDevice.adjustingExposure;
    _captureMask.adjustingFocus = _captureDevice.adjustingFocus;
    _captureMask.adjustingWhiteBalance = _captureDevice.adjustingWhiteBalance;
    
    [_session commitConfiguration];
    
    openCallback(_session, nil);
}

- (void) start {
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      [self setOrientation];
                                                  }];
    [self configureDevice];
    [self setOrientation];
    [self addObservers];
    [_session startRunning];
}

- (void) stop {
    [_session stopRunning];
    
    _captureMask.captureMask = 0x00;
    
    [self removeObservers];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (void) captureImage:(void(^)(NSData*, NSError*)) captureImageCallback {
    _captureMask.isCapturePending = NO;
    _captureImageCallback = captureImageCallback;
    [self doCaptureImage];
}

- (void)stopCapturing:(id)data{
    
}
#pragma mark - AVCaptureMetadataOutputObjectsDelegate -

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection {
    // see http://useyourloaf.com/blog/reading-qr-codes/
    for (AVMetadataObject* metaObject in metadataObjects) {
        if([metaObject isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
            AVMetadataMachineReadableCodeObject* machineRedable = (AVMetadataMachineReadableCodeObject*)metaObject;

            NSArray<NSDictionary*>* corners = machineRedable.corners;
            CGPoint topLeft,
                    bottomRight;
            
            CGPointMakeWithDictionaryRepresentation((CFDictionaryRef)[corners objectAtIndex:0], &topLeft);
            CGPointMakeWithDictionaryRepresentation((CFDictionaryRef)[corners objectAtIndex:2], &bottomRight);

            CGRect rect = CGRectMake(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
        
            [self.barcodeProtocol barcodeDetected:machineRedable.stringValue
                                       withBounds:rect];
        }
    }
}

@end
