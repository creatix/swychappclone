//
//  AnalyticShareASaleSettings.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticSettings.h"

@interface AnalyticShareASaleSettings : AnalyticSettings

@property (nonatomic,strong) NSString *merchantID;
@property (nonatomic,strong) NSString *appID;
@property (nonatomic,strong) NSString *appKey;
@property (nonatomic,assign )BOOL isTestEvn;
@end
