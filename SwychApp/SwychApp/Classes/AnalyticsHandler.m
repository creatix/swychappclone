//
//  AnalyticsHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticsHandler.h"
#import "Analytics.h"

#import "AnalyticsAdjust.h"
#import "AnalyticsFabric.h"
#import "TrackingEventBase.h"
#import "AnalyticsShareASale.h"


@interface AnalyticsHandler()

@property (nonatomic,strong) NSArray<Analytics*> *anaInstances;

@end

@implementation AnalyticsHandler

+(AnalyticsHandler*)getInstance{
    static AnalyticsHandler *g_analyticsHandlerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_analyticsHandlerInstance = [[self alloc] init];
        g_analyticsHandlerInstance.anaInstances = [NSArray arrayWithObjects:[AnalyticsAdjust instance],
                                                   [AnalyticsFabric instance],
                                                   [AnalyticsShareASale instance],
                                                   nil];
    });
    return g_analyticsHandlerInstance;
}

-(void)initialize{
    for(Analytics *ana in self.anaInstances){
        [ana register];
    }
}


-(void)sendEvent:(TrackingEventBase*)event{
    for(Analytics *ana in self.anaInstances){
        [ana logCustomEvent:event];
    }
}

@end
