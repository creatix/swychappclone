//
//  ContactCollectionViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ContactCollectionViewCell.h"
#import "ModelContact.h"
#import "UIRoundImageView.h"
#import "TTTAttributedLabel.h"

@interface ContactCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIRoundImageView *ivProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *labFirstName;
@property (weak, nonatomic) IBOutlet UILabel *labLastName;
@property (weak, nonatomic) IBOutlet UILabel *labPhoneOrEmail;
@property (weak, nonatomic) TTTAttributedLabel *labFirstLatterOfFirstName;

@property (weak,nonatomic) UIImageView *ivSelectedMaskImageView;
@end

@implementation ContactCollectionViewCell
@synthesize contact = _contact;


-(void)addMaskView{
    if (self.ivSelectedMaskImageView != nil){
        return;
    }
    UIImageView *iv = [[UIImageView alloc] initWithFrame:self.ivProfileImage.bounds];
    iv.image = [UIImage imageNamed:@"profile_image_selected_mask.png"];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.ivSelectedMaskImageView = iv;
    [self.ivProfileImage addSubview:iv];
}

-(void)addFirstLetterLabel{
    if (self.labFirstLatterOfFirstName != nil){
        return;
    }
    TTTAttributedLabel *lab = [[TTTAttributedLabel alloc] initWithFrame:self.ivProfileImage.bounds];
    lab.textAlignment = NSTextAlignmentCenter;
    lab.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
    lab.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    lab.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    lab.textColor = [UIColor whiteColor];
    lab.backgroundColor = [UIColor clearColor];
    
    [self.ivProfileImage addSubview:lab];
    self.labFirstLatterOfFirstName = lab;
}

-(ModelContact*)contact{
    return _contact;
}

-(BOOL)selectedContact{
    return _selectedContact;
}

-(void)setSelectedContact:(BOOL)selectedContact{
    _selectedContact = selectedContact;
    [self addMaskView];
    if (self.ivSelectedMaskImageView != nil){
        if (_selectedContact){
            [self.ivProfileImage bringSubviewToFront:self.ivSelectedMaskImageView];
            self.ivSelectedMaskImageView.hidden = NO;
            [self.ivProfileImage bringSubviewToFront:self.ivSelectedMaskImageView];
        }
        else{
            self.ivSelectedMaskImageView.hidden = YES;
        }
    }
}

-(void)setContact:(ModelContact *)contact{
    _contact = contact;
    [self addFirstLetterLabel];
    if (contact.profileImage != nil){
        self.ivProfileImage.image = contact.profileImage;
        self.labFirstLatterOfFirstName.hidden = YES;
    }
    else{
        self.ivProfileImage.image = [UIImage imageNamed:@"profile_image_default.png"];
        self.labFirstLatterOfFirstName.hidden = NO;
        self.labFirstLatterOfFirstName.text = [self getTextContentForEmptyProfileImageContact:_contact];
    }
    self.labFirstName.text = contact.firstName;
    self.labLastName.text = contact.lastName;
    if (contact.phoneNumber != nil){
        self.labPhoneOrEmail.text = contact.phoneNumber;
    }
    else{
        self.labPhoneOrEmail.text = contact.email;
    }
}

-(NSString *)getTextContentForEmptyProfileImageContact:(ModelContact*)contact{
    if (contact.firstName == nil || [contact.firstName length] == 0){
        return  nil;
    }
    else if ([contact.firstName length] == 1){
        return contact.firstName;
    }
    else{
        return [[contact.firstName uppercaseString] substringToIndex:1];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
