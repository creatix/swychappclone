//
//  PreviewView.h
//  TestCamera
//
//  Created by Donald Hancock on 2/28/16.
//  Copyright © 2016 com.threeOtwo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

IB_DESIGNABLE
@interface PreviewView : UIView

@property (nonatomic, copy) IBInspectable  UIColor* searchAreaOverlayColor;

@property (nonatomic, copy) IBInspectable UIColor* captureAreaColor;


- (void) setAreaOfInterest:(CGRect) areaOfInterest;
- (void) setSession:(AVCaptureSession*)session;

-(UIImage *)getInterestedAreaImage:(NSData *)imageData;
@end
