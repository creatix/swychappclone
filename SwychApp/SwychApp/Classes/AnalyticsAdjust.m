//
//  AnalyticsAdjust.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticsAdjust.h"
#import "AnalyticAdjustSettings.h"
#import "TrackingEventBase.h"

#import "Adjust/Adjust.h"


@implementation AnalyticsAdjust

+(Analytics*)instance{
    static Analytics *g_instanceAdjust = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instanceAdjust = [[self alloc] init];
        g_instanceAdjust.ignoredEvents = nil;
    });
    return g_instanceAdjust;
}

-(void)register{
    AnalyticAdjustSettings *settings = (AnalyticAdjustSettings*)[self getAnalyticInstanceSettings];
    ADJConfig *adjustConfig = [ADJConfig configWithAppToken:settings.appToken environment:settings.isTestEvn ? ADJEnvironmentSandbox : ADJEnvironmentProduction];
    
    [Adjust appDidLaunch:adjustConfig];
    
}

-(void)logCustomEvent:(nullable TrackingEventBase *)event{
    if ([self isIgnoredEvent:event]){
        return;
    }
    ADJEvent *evt = [ADJEvent eventWithEventToken:event.eventCode];
    if(event.customValues != nil){
        for(NSString *key in event.customValues){
            NSString *val = [event.customValues objectForKey:key];
            [evt addPartnerParameter:key value:val];
        }
    }
    [Adjust trackEvent:evt];
    
}

@end
