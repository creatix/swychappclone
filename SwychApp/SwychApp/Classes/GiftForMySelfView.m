//
//  GiftForMySelfView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftForMySelfView.h"
#import "GiftCardCarousel.h"
#import "UIGiftcardView.h"

@implementation GiftForMySelfView

@synthesize giftcards = _giftcards;

-(void)setGiftcards:(NSArray<ModelGiftCard *> *)giftcards{
    _giftcards = giftcards;
    if (self.giftCardCarousel != nil){
        self.giftCardCarousel.giftCards = giftcards;
    }
}

-(NSArray<ModelGiftCard*>*)giftcards{
    return _giftcards;
}

-(void)awakeFromNib{
    if (self.giftCardCarousel.giftCards == nil){
        self.giftCardCarousel.giftCards = self.giftcards;
    }
    self.giftCardCarousel.delegate = self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma
#pragma mark - GiftcardCarouselDelegate
- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
         giftCardSelected:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    if (self.giftForMySelfDelegate != nil && [self.giftForMySelfDelegate respondsToSelector:@selector(giftForMySelfViewGiftcardSelected:)]){
        [self.giftForMySelfDelegate giftForMySelfViewGiftcardSelected:giftCard];
    }
}

- (void) giftCardCarousel:(GiftCardCarousel*) giftCardCarousel
          giftCardFocused:(ModelGiftCard*)giftCard transaction:(ModelTransaction *)transacton{
    
}

-(UIView*)getCarouselView:(GiftCardCarousel *)carousel ItemAtIndex:(NSInteger)index{
    CGFloat height = carousel.frame.size.height;
    CGFloat width = (height - 50.0) * (300.0 / 190.0);
    CGRect rc = CGRectMake(0, 0, width, height);
    
    UIGiftcardView *gcv = [[UIGiftcardView alloc] initWithFrame:rc];
    gcv.viewType = GiftcardView_NormalWithDiscount;
    
    return gcv;
}

@end
