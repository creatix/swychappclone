//
//  PopupViewHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewHandler.h"
#import "PopupViewBase.h"
#import "AppDelegate.h"

@implementation PopupViewHandler


-(id)init{
    if((self = [super init])){
        _app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        _window =  _app.window;
        _maskView = [[UIView alloc] initWithFrame:_app.window.bounds];
        _maskView.backgroundColor = [UIColor darkGrayColor];
        _maskView.alpha = 0.6f;
        _view = [[UIView alloc] initWithFrame:_app.window.bounds];
        _view.backgroundColor = [UIColor clearColor];
        
        [_view addSubview:_maskView];
        
        _popupViews = [[NSMutableArray alloc] initWithCapacity:1];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [tap setCancelsTouchesInView:NO];
        [_maskView addGestureRecognizer:tap];

    }
    return self;
}

-(void)pushView:(PopupViewBase*)view{
    if ([view isKindOfClass:[ProgressView class]]){
        UIView *v = [self topView];
        if (v != nil && [v isKindOfClass:[ProgressView class]]){
            ProgressView *vProgCurrent = (ProgressView *)v;
            ProgressView *vNew = (ProgressView*)view;
            [vProgCurrent setLoadingText:[vNew getLoadingText]];
            return;
        }
    }
    
    [_popupViews insertObject:view atIndex:0];
    [_view bringSubviewToFront:_maskView];
    [view adjustControls];
    [_view addSubview:view];

    
    NSLayoutConstraint *centreHorizontallyConstraint = [NSLayoutConstraint
                                                        constraintWithItem:view
                                                        attribute:NSLayoutAttributeCenterX
                                                        relatedBy:NSLayoutRelationEqual
                                                        toItem:_view
                                                        attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                        constant:0];
    centreHorizontallyConstraint.identifier = @"$horizontallyCenter$";
    NSLayoutConstraint *centreVerticallyConstraint = [NSLayoutConstraint
                                                        constraintWithItem:view
                                                        attribute:NSLayoutAttributeCenterY
                                                        relatedBy:NSLayoutRelationEqual
                                                        toItem:_view
                                                        attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                        constant:0];
    centreVerticallyConstraint.identifier = @"$VericallyCenter$";
    [_view addConstraint: centreVerticallyConstraint];
    [_view addConstraint:centreHorizontallyConstraint];
    
    
    if (_view.superview == nil){
        [_window addSubview:_view];
    }
}

-(void)popView{
    if ([_popupViews count] == 0){
        return;
    }
    UIView *v = [_popupViews objectAtIndex:0];
    [_popupViews removeObjectAtIndex:0];
    [v removeFromSuperview];
    
    if ([_popupViews count] ==0){
        [_view removeFromSuperview];
    }
    else{
        UIView *v2 = [_popupViews objectAtIndex:0];
        [_view bringSubviewToFront:v2];
    }
}

-(void)popView:(PopupViewBase*)view{
    UIView *v = [self topView];
    if (v != nil && [view class] == [v class]){
        [self popView];
    }
}


-(UIView*)topView{
    if (_popupViews == nil || [_popupViews count] == 0){
        return nil;
    }
    return [_popupViews objectAtIndex:0];
}

-(void)relayoutSubviews:(CGRect )rc{
    if (_popupViews == nil || [_popupViews count] == 0){
        return;
    }
    CGRect rcOrg = _view.frame;
    if (rcOrg.size.width == rc.size.width && rcOrg.size.height == rc.size.height){
        return;
    }
    _view.frame = rc;
    _maskView.frame = rc;
    [_view layoutSubviews];
}

-(void)touchOnView:(id)sender{
    UIView *v = [self topView];
    if (v != nil && [v isKindOfClass:[PopupViewBase class]]){
        PopupViewBase *popV = (PopupViewBase*)v;
        if (![popV allowTouchToDismiss]){
            return;
        }
    }
    [self popView];
}

@end
