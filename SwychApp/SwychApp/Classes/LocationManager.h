//
//  LocationManager.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject

+(LocationManager*)instance;

-(void)startUpdateLocation;
-(CLLocation*)getCurrentLocation;
-(void)showRouteWithLogitude:(double)latitude longitude:(double)longitude destnationName:(NSString*)name;
@end
