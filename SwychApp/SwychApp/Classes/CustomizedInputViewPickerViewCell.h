//
//  CustomizedInputViewPickerViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomizedInputViewPickerViewCell : UIView

@property (weak, nonatomic) IBOutlet UIImageView *ivLeft;
@property (weak, nonatomic) IBOutlet UIImageView *ivRight;
@property (weak, nonatomic) IBOutlet UILabel *labLeft;
@property (weak, nonatomic) IBOutlet UILabel *labRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftImageViewAspect;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRightImageViewAspect;

@end
