//
//  UILabelExt.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UILabelExtDelegate;
IB_DESIGNABLE
typedef enum{
    VerticalAlignment_Top = 0,
    VerticalAlignment_Middle,
    VerticalAlignment_Bottom,
} UILabelVerticalAlignment;


@interface UILabelExt : UILabel

@property(assign,nonatomic) IBInspectable UILabelVerticalAlignment verticalAlignment;
@property(weak,nonatomic) id<UILabelExtDelegate> delegate;
@property(assign,nonatomic) BOOL underlineText;

@end

@protocol UILabelExtDelegate <NSObject>

-(void)labelExtTouched:(UILabelExt*)label;

@end