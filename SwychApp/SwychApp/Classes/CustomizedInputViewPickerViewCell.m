//
//  CustomizedInputViewPickerViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CustomizedInputViewPickerViewCell.h"


@interface CustomizedInputViewPickerViewCell()
- (IBAction)buttonTapped:(id)sender;

@end

@implementation CustomizedInputViewPickerViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)buttonTapped:(id)sender {
    if (self.ivRight.image == nil){
        self.ivRight.image = [UIImage imageNamed:@"icon_checkmark.png"];
    }
    else{
        self.ivRight.image = nil;
    }
}
@end
