//
//  UIPlaceholderTextView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIPlaceholderTextView : UITextView

@property(nonatomic,strong) IBInspectable NSString *placeholder;
@property(nonatomic,strong) IBInspectable UIColor *placeholderColor;

@end
