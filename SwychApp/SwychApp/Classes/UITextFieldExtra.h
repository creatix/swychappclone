//
//  UITextFieldExtra.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UITextFieldExtra : UITextField{
    
}
@property (nonatomic,assign) CGFloat offsetX;
@property (nonatomic,assign) CGFloat offsetY;
@property(nonatomic,strong) IBInspectable UIColor *bottomBorderColor;
@end
