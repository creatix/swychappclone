//
//  AnalyticsShareASale.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticsShareASale.h"
#import "AnalyticShareASaleSettings.h"
#import "ios_tracking.h"
#import "SwychBaseViewController.h"

#import "TrackingEventAppInstalled.h"
#import "TrackingEventOTPCompleted.h"
#import "TrackingEventRegistrationCompleted.h"
#import "TrackingEventCardUploaded.h"
#import "TrackingEventAffiliateDesignated.h"

#import "TrackingEventGiftCardPurchasedForSelfGifting.h"
#import "TrackingEventGiftCardPurchasedForSendToFriend.h"

@interface AnalyticsShareASale()

@property (nonatomic,strong) SASIos_tracking *shrsl;
@end

@implementation AnalyticsShareASale

+(Analytics*)instance{
    static Analytics *g_instanceShare = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instanceShare = [[self alloc] init];
        g_instanceShare.ignoredEvents = nil;
    });
    return g_instanceShare;
}

-(void)register{
    [super register];
    self.ignoredEvents = [NSArray arrayWithObjects:NSStringFromClass([TrackingEventAppInstalled class]),
                          NSStringFromClass([TrackingEventOTPCompleted class]),
                          NSStringFromClass([TrackingEventRegistrationCompleted class]),
                          NSStringFromClass([TrackingEventCardUploaded class]),
                          NSStringFromClass([TrackingEventAffiliateDesignated class]),
                                                    nil];
    
    AnalyticShareASaleSettings *settings = (AnalyticShareASaleSettings*)[self getAnalyticInstanceSettings];
    self.shrsl = [[SASIos_tracking alloc] initWithMerchantID:settings.merchantID
                                                   withAppID:settings.appID
                                                  withAppKey:settings.appKey];
}

-(void)trackInstallWithView:(UIView*)view{
    [self.shrsl trackInstallWithView:view];
}

-(void)logCustomEvent:(nullable TrackingEventBase *)event{
    
}

-(void)trackTransaction:(NSString*)tranNumber amount:(double)amount  purchaseForFriend:(BOOL)forFriend{
    UIWindow *w = [APP window];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:forFriend ? @"YES" : @"NO" forKey:@"ForFriend"];
    [self.shrsl trackTransactionWithView:w withOrderNumber:tranNumber withTranstype:@"sale" withAmount:amount withAdditionalParams:dict];
}
@end
