//
//  GiftForMySelfView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GiftCardCarousel.h"

@protocol GiftForMySelfViewDelegate;
@class GiftCardCarousel;
@class ModelGiftCard;

@interface GiftForMySelfView : UIView<GiftCardCarouselDelegate>

@property (weak,nonatomic) IBOutlet id<GiftForMySelfViewDelegate> giftForMySelfDelegate;
@property (weak, nonatomic) IBOutlet GiftCardCarousel *giftCardCarousel;
@property (nonatomic,strong) NSArray<ModelGiftCard *> *giftcards;

@end

@protocol GiftForMySelfViewDelegate <NSObject>

-(void)giftForMySelfViewGiftcardSelected:(ModelGiftCard*)giftcard;
@end

