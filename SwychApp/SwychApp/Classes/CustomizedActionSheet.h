//
//  CustomizedActionSheet.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomizedActionSheetDelegate;

@class ModelCustomizedActionSheetItem;

@interface CustomizedActionSheet : UIView<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>{
    NSInteger selectedItemIndex;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSheetView;
@property (weak,nonatomic) id<CustomizedActionSheetDelegate> actionSheetDelegate;
@property (weak,nonatomic) id<UITableViewDataSource> actionSheetTableViewDataSource;
@property (weak,nonatomic) id<UITableViewDelegate> actionSheetTableViewDelegate;

@property (nonatomic,strong) NSArray *items;
@property (nonatomic,assign) NSInteger context;
@property (nonatomic,assign) CGFloat minimumHeigtOfSheetView;
@property (nonatomic,assign) CGFloat maximumHeigtOfSheetView;

@property (nonatomic,assign) NSInteger selectedItemId;

@property (nonatomic,readonly) ModelCustomizedActionSheetItem *selectedItem;


+(CustomizedActionSheet*)createInstanceWithItems:(NSArray*)itemsArray withButtonText:(NSString *)buttonText;

-(void)show;
-(void)dismiss;
-(CGFloat)getRowHeight;
@end


@protocol CustomizedActionSheetDelegate <NSObject>
@optional
-(void)customizedActiondSheetButtonTapped:(CustomizedActionSheet*)sheet;

@end

