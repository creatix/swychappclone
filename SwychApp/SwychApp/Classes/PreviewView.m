//
//  PreviewView.m
//  TestCamera
//
//  Created by Donald Hancock on 2/28/16.
//  Copyright © 2016 com.threeOtwo. All rights reserved.
//

#import "PreviewView.h"
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

#define OUTPUTS_KEY @"outputs"
#define RECT_OF_INTEREST_KEY @"rectOfInterest"
#define VIDEO_ORIENTATION_KEY @"videoOrientation"

@interface  PreviewLayerDelegate : NSObject

@property (nonatomic, assign) CGRect captureBounds;

@property (nonatomic, assign) CGRect searchArea;

@property (nonatomic, strong) UIColor* searchAreaOverlayColor;

@property (nonatomic, strong) UIColor* captureAreaColor;

@end

@implementation PreviewLayerDelegate

- (instancetype) init {
    if((self = [super init]) != nil) {
        _searchAreaOverlayColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        _captureAreaColor = [UIColor redColor];
    }
    return self;
}

- (void)drawLayer:(CALayer *)layer
        inContext:(CGContextRef)ctx {
    
    if(!CGRectIsEmpty(_searchArea) && _searchAreaOverlayColor != nil) {
        CGContextSaveGState(ctx);
        CGFloat red, blue, green, alpha;
        
        [_searchAreaOverlayColor getRed:&red green:&green blue:&blue alpha:&alpha];
        UIColor* color = [UIColor colorWithRed:red
                                         green:green
                                          blue:blue
                                         alpha:.5];

        CGContextSetFillColorWithColor( ctx, color.CGColor );
        CGContextFillRect( ctx, layer.bounds );
        
        CGFloat radius =  10;
        CGRect rect = _searchArea;// CGRectMake(0,0,200,200);
        CGRect rect2 = CGRectMake(rect.origin.x + 1,
                                  rect.origin.y + 1,
                                  rect.size.width - 2, rect.size.height - 2);
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius];
        UIBezierPath *path2 = [UIBezierPath bezierPathWithRoundedRect:rect2 cornerRadius:radius];
        path.lineWidth = 1.0;
        CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
        CGContextAddPath(ctx, path.CGPath);
        CGContextDrawPath(ctx, kCGPathStroke);
        
        CGContextSetFillColorWithColor( ctx, [UIColor clearColor].CGColor );
        CGContextSetBlendMode(ctx, kCGBlendModeClear);
        CGContextAddPath(ctx, path2.CGPath);
        CGContextFillPath(ctx);

        CGContextRestoreGState(ctx);
    }

    if(!CGRectIsEmpty(_captureBounds) && _captureAreaColor != nil){
        CGContextSetStrokeColorWithColor(ctx, _captureAreaColor.CGColor);
        CGContextSetLineWidth(ctx, 3);
        CGContextStrokeRect(ctx, _captureBounds);
    }
}

@end

@interface PreviewView () {
    AVCaptureVideoPreviewLayer* _capturePreviewBack;
    CALayer*                    _previewLayer;
    CGRect                      _areaOfInterest;
    PreviewLayerDelegate*       _previewDelegate;
    AVCaptureConnection*        _stillConnection;
    AVCaptureMetadataOutput*    _metaOutput;
}

@end

@implementation PreviewView

-(UIImage *)getInterestedAreaImage:(NSData *)imageData{
    UIImage *takenImage = [UIImage imageWithData:imageData];
    
    CGRect outputRect = [_capturePreviewBack metadataOutputRectOfInterestForRect:_areaOfInterest];
    CGImageRef takenCGImage = takenImage.CGImage;
    size_t width = CGImageGetWidth(takenCGImage);
    size_t height = CGImageGetHeight(takenCGImage);
    CGRect cropRect = CGRectMake(outputRect.origin.x * width, outputRect.origin.y * height, outputRect.size.width * width, outputRect.size.height * height);
    
    CGImageRef cropCGImage = CGImageCreateWithImageInRect(takenCGImage, cropRect);
    takenImage = [UIImage imageWithCGImage:cropCGImage scale:1 orientation:takenImage.imageOrientation];
    CGImageRelease(cropCGImage);

    return takenImage;
}

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary<NSString *,id> *)change
                        context:(void *)context {
    
   if([RECT_OF_INTEREST_KEY isEqualToString:keyPath]) {
        NSValue* val = [change objectForKey:NSKeyValueChangeNewKey];
        CGRect bounds = [val CGRectValue];
       
       _previewDelegate.searchArea = [_capturePreviewBack rectForMetadataOutputRectOfInterest:bounds];
       [_previewLayer setNeedsDisplay];
    }
    else if([VIDEO_ORIENTATION_KEY isEqualToString:keyPath]) {
        NSNumber* val = [change objectForKey:NSKeyValueChangeNewKey];
        AVCaptureVideoOrientation orientation = (AVCaptureVideoOrientation)[val intValue];
        
        if(orientation != _capturePreviewBack.connection.videoOrientation) {
            _capturePreviewBack.connection.videoOrientation = orientation;
        }
    }
    else if([OUTPUTS_KEY isEqualToString:keyPath]) {
        [self removeObserverHelper:_metaOutput forKeyPath:RECT_OF_INTEREST_KEY];
        [self removeObserverHelper:_stillConnection forKeyPath:VIDEO_ORIENTATION_KEY];
        
        AVCaptureConnection*        tempStillConnection = nil;
        AVCaptureMetadataOutput*    tempMetaOutput = nil;
        
        for(AVCaptureOutput* output in _capturePreviewBack.session.outputs) {
            if([output isKindOfClass:[AVCaptureMetadataOutput class]]) {
                tempMetaOutput = (AVCaptureMetadataOutput*)output;
            }
            else if([output isKindOfClass:[AVCaptureStillImageOutput class]]) {
                for (AVCaptureConnection* con in output.connections) {
                    if([con isVideoOrientationSupported]) {
                        tempStillConnection = con;
                    }
                }
            }
        }
        _stillConnection = tempStillConnection;
        _metaOutput = tempMetaOutput;
        
        [self addObserverHelper:_stillConnection forKeyPath:VIDEO_ORIENTATION_KEY];
        [self addObserverHelper:_metaOutput forKeyPath:RECT_OF_INTEREST_KEY];
        
        [self updateCaptureArea];
    }
}

- (void) awakeFromNib {
    CGRect bounds = self.layer.bounds;
    
    _capturePreviewBack = [AVCaptureVideoPreviewLayer layer];
    _capturePreviewBack.frame = bounds;
    _capturePreviewBack.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [self.layer addSublayer:_capturePreviewBack];
    
    _previewDelegate = [PreviewLayerDelegate new];
    
    _previewLayer = [CALayer layer];
    _previewLayer.frame = bounds;
    _previewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    _previewLayer.delegate = _previewDelegate;
    _previewLayer.needsDisplayOnBoundsChange = YES;
    _previewLayer.masksToBounds = YES;
    
    [self.layer addSublayer:_previewLayer];
    
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    for(CALayer* layer in self.layer.sublayers) {
        layer.frame = self.bounds;
    }
    [self updateCaptureArea];
}

- (void) dealloc {
    [self removeObserverHelper:_metaOutput forKeyPath:RECT_OF_INTEREST_KEY];
    [self removeObserverHelper:_stillConnection forKeyPath:VIDEO_ORIENTATION_KEY];
    [self removeObserverHelper:_capturePreviewBack.session forKeyPath:OUTPUTS_KEY];
}

#pragma mark - properties -

- (void) setSearchAreaOverlayColor:(UIColor *)searchAreaOverlayColor {
    _previewDelegate.searchAreaOverlayColor = [searchAreaOverlayColor copy];
    [_previewLayer setNeedsDisplay];
}

- (void) setCaptureAreaColor:(UIColor *)captureAreaColor {
    _previewDelegate.captureAreaColor = [captureAreaColor copy];
    [_previewLayer setNeedsDisplay];
}

- (void )setAreaOfInterest:(CGRect) areaOfInterest {
    _areaOfInterest = areaOfInterest;
    
    [self updateCaptureArea];
    [self setNeedsDisplay];
}

- (void) setSession:(AVCaptureSession*)session {
    [self removeObserverHelper: _capturePreviewBack.session forKeyPath:OUTPUTS_KEY];
    [_capturePreviewBack setSession:session];
    [self addObserverHelper:_capturePreviewBack.session forKeyPath:OUTPUTS_KEY];
}

- (void) removeObserverHelper:(NSObject*)obj forKeyPath:(NSString*) key {
    @try {
        [obj removeObserver:self forKeyPath:key];
    }
    @catch (NSException *exception) { }
}

- (void) addObserverHelper:(NSObject*)obj forKeyPath:(NSString*) key {
    [obj addObserver:self forKeyPath:key options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:nil];
}

- (void) updateCaptureArea {
    if(_metaOutput != nil) {
        if(!CGRectIsEmpty(_areaOfInterest)) {
            CGRect bounds = [_capturePreviewBack metadataOutputRectOfInterestForRect:_areaOfInterest];
            _metaOutput.rectOfInterest = bounds;
        }
        else {
            _metaOutput.rectOfInterest = CGRectMake(0, 0, 1, 1);
            _previewDelegate.searchArea = CGRectMake(0, 0, 0, 0);
        }
    }
    else {
        _previewDelegate.searchArea = CGRectMake(0, 0, 0, 0);
        [_previewLayer setNeedsDisplay];
    }
}

@end

#undef OUTPUTS_KEY
#undef RECT_OF_INTEREST_KEY
#undef VIDEO_ORIENTATION_KEY