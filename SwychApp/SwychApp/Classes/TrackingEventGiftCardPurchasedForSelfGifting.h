//
//  TrackingEventGiftCardPurchasedForSelfGifting.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventBase.h"

@interface TrackingEventGiftCardPurchasedForSelfGifting : TrackingEventBase

@end
