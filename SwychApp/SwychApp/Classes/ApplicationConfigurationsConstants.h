//
//  ApplicationConfigurationsConstants.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#ifndef ApplicationConfigurationsConstants_h
#define ApplicationConfigurationsConstants_h

#define KEY_PROD                  @"PROD"
#define KEY_QA                    @"QA"
#define KEY_UAT                   @"UAT"
#define KEY_DEV                   @"DEV"

#define KEY_CLASS_MAPPINGS          @"ClassMappings"
#define KEY_CLASS_MAPPINGS_CLASS    @"Class"
#define KEY_CLASS_MAPPINGS_XIB      @"XIB"

#define KEY_PAYMENT_PROCESSOR       @"PaymentProcessor"

#define KEY_GeneralSettings                 @"GeneralSettings"
#define KEY_IndividualViewControllers       @"IndividualViewControllers"

#define KEY_SERVER_URL                      @"ServerURL"

#define KEY_ClientID                        @"ConnectionClientID"
#define KEY_ApiKey                          @"ConnectionApiKey"

#define KEY_PaymentMethods                                  @"PaymentMethods"
#define KEY_Payment_ApplePay                                @"ApplePay"
#define KEY_Payment_ApplePay_MerchantIdentifier             @"MerchantIdentifier"
#define KEY_Payment_ApplePay_Stripe_PublishableKey          @"StripePublishableKey"
#define KEY_Payment_PayPal                                  @"PayPal"
#define KEY_Payment_PayPal_MerchantKey_PROD                 @"MerchantKey_PROD"
#define KEY_Payment_PayPal_MerchantKey_TEST                 @"MerchantKey_TEST"
#define KEY_Payment_IsProdKey                               @"IsProdKey"
#define KEY_Payment_DirectCreditCard                        @"DirectCreditCard"
#define KEY_Payment_AmazonPay                               @"AmazonPay"
#define KEY_Payment_AmazonPay_AWSAccessKeyId                @"AWSAccessKeyId"
#define KEY_Payment_BrainTree                               @"BrainTree"
#define KEY_Payment_BrainTree_ClientKeyId                   @"ClientId"

#define KEY_COLOR_Red                       @"Red"
#define KEY_COLOR_Green                     @"Green"
#define KEY_COLOR_Blue                      @"Blue"
#define KEY_COLOR_Alpha                     @"Alpha"

#define KEY_Width                       @"Width"
#define KEY_Height                      @"Height"

#define KEY_FONT_NAME                       @"FontName"
#define KEY_FONT_SIZE                       @"FontSize"

#define KEY_MENU_ITEM_Text                  @"Text"
#define KEY_MENU_ITME_Icon                  @"Icon"
#define KEY_MENU_ITME_Tag                   @"Tag"
#define KEY_MENU_ITEM_Order                 @"Order"
#define KEY_MENU_ITEM_DegugOnly             @"DebugOnly"

#define KEY_MENU_ITEM_WITH_POSITION         @"IconLocation"

#define KEY_CONTROLS                        @"Controls"
#define KEY_BUTTON                          @"Button"
#define KEY_LABEL                           @"Label"
#define KEY_TEXTFIELD                       @"TextField"
#define KEY_TEXTVIEW                        @"TextView"


#define KEY_TabbarSettings                  @"TabbarSettings"
#define KEY_TabbarItems                     @"Items"

#define KEY_NavbarSettings                  @"NavbarSettings"

#define KEY_BackgroundColor                 @"BackgroundColor"
#define KEY_Text_Color                      @"TextColor"
#define KEY_PlaceHolder_Color               @"PlaceHolderColor"

#define KEY_ViewController_SlidingMenu      @"SlidingMenuViewController"
#define KEY_VC_SlidingMenu_Items            @"MenuItems"
#define KEY_VC_SlidingMenu_Items_PendingUser    @"MenuItemsPendingUser"



/*  Analytics   */
#define KEY_ANALYTICS               @"Analytics"
#define KEY_ANALYTICS_Adjust_AppToken   @"AppToken"
#define KEY_AnaLYTICS_Adjust_TextEnv    @"TestEvn"

#define KEY_ANALYTICS_ShareASale_MerchantID     @"MerchantID"
#define KEY_ANALYTICS_ShareASale_AppID          @"AppID"
#define KEY_ANALYTICS_ShareASale_AppKey         @"AppKey"
#define KEY_ANALYTICS_ShareASale_TextEnv        @"TestEvn"

#endif /* ApplicationConfigurationsConstants_h */

