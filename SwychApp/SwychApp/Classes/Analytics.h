//
//  Analytics.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TrackingEventBase;


@interface Analytics : NSObject


@property (nonatomic,strong,nullable) NSArray *ignoredEvents;

+(nonnull Analytics *)instance;

+(nonnull NSDictionary*)prepareAnalyticsEvent:(nullable NSString*)eventName attributes:(nullable NSDictionary*)attributes;

-(void)fireCrash;

-(void)register;
-(void)unregister;

-(void)startSession;
-(void)endSession;

-(void)logSignUpWithMethod:(nullable NSString*)methodOrNil
                   success:(nullable NSNumber*)successOrNil
                attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logLoginWithMethod:(nullable NSString*)methodOrNil
                   sucess:(nullable NSNumber*)successOrNil
               attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logShareWithMethod:(nullable NSString*)methodOrNil
              contentName:(nullable NSString*)contentNameOrNil
              contentType:(nullable NSString*)contentTypeOrNil
                contentId:(nullable NSString*)contentIdOrNil
               attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logInviteWithMethod:(nullable NSString*)methodOrNil
                attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logPurchaseWithPrice:(nullable NSDecimalNumber*)itemPriceOrNil
                   currency:(nullable NSString*)currencyOrNil
                    success:(nullable NSNumber*)purchaseSucceededOrNil
                   itemName:(nullable NSString*)itemNameOrNil
                   itemType:(nullable NSString*)itemTypeOrNil
                     itemId:(nullable NSString*)itemIdOrNil
                 attributes:(nullable NSDictionary*) attributesOrNil;

-(void)logGameLevelStart:(nullable NSString*)levelNameOrNil
              attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logGameLevelEnd:(nullable NSString*)levelNameOrNil
                 score:(nullable NSNumber*)scoreOrNil
               success:(nullable NSNumber*)levelCompletedSuccesfullyOrNil
            attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logAddToCartWithPrice:(nullable NSDecimalNumber*)itemPriceOrNil
                    currency:(nullable NSString*)currencyOrNil
                    itemName:(nullable NSString*)itemNameOrNil
                    itemType:(nullable NSString*)itemTypeOrNil
                      itemId:(nullable NSString*)itemIdOrNil
                  attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logStartCheckoutWithPrice:(nullable NSDecimalNumber*)totalPriceOrNil
                        currency:(nullable NSString*)currencyOrNil
                       itemCount:(nullable NSNumber*)itemCountOrNil
                      attributes:(nullable NSDictionary*)attributesOrNil;

-(void)LogRating:(nullable NSNumber*)ratingOrNil
     contentName:(nullable NSString*)contentNameOrNil
     contentType:(nullable NSString*)contentTypeOrNil
       contentId:(nullable NSString*)contentIdOrNil
      attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logContentViewWithName:(nullable NSString*)contentNameOrNil
                  contentType:(nullable NSString*)contentTypeOrNil
                    contentId:(nullable NSString*)contentIdOrNil
                   attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logSearchWithQuery:(nullable NSString*)queryOrNil
               attributes:(nullable NSDictionary*)attributesOrNil;

-(void)logCustomEvent:(nullable TrackingEventBase *)event;

-(BOOL)isIgnoredEvent:(TrackingEventBase*)event;

-(AnalyticSettings*)getAnalyticInstanceSettings;
@end
