//
//  UIFeaturedDeal.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIFeaturedDeal.h"
#import "UIRoundCornerImageView.h"
#import "TTTAttributedLabel.h"


@implementation UIFeaturedDeal

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UIFeaturedDeal" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
}

-(void)touchOnView:(id)sender{
    if (self.featuredDealDelegate != nil && [self.featuredDealDelegate respondsToSelector:@selector(featuredDealItemTapped:)]){
        [self.featuredDealDelegate featuredDealItemTapped:self];
    }
}

@end
