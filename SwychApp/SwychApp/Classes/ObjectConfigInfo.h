//
//  ObjectConfigInfo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectConfigInfo : NSObject {
    BOOL _hasPropertySettings;
}
@property (readonly,nonatomic) BOOL hasPropertySettings;

@property (strong,nonatomic) UIFont     *font;
@property (assign,nonatomic) CGFloat    fixedHeight;
@property (strong,nonatomic) UIColor    *backgroundColor;
@property (strong,nonatomic) UIColor    *textColor;
@property (strong, nonatomic) UIColor   *placeHolderColor;

-(id)initWithDictionary:(NSDictionary*)dict;
-(void)setProperties:(NSDictionary*)dict;

@end
