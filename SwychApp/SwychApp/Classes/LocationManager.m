//
//  LocationManager.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LocationManager.h"
@import MapKit;

@interface LocationManager()
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) CLLocation *currentLocation;
@end

@implementation LocationManager


+(LocationManager*)instance{
    static LocationManager *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
        g_instance.currentLocation = nil;
    });
    return g_instance;
}

-(void)startUpdateLocation{
    // Create the location manager if this object does not
    // already have one.
    if (nil == self.locationManager){
        self.locationManager = [[CLLocationManager alloc] init];
    }
    self.locationManager.delegate = (id<CLLocationManagerDelegate>)self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 500; // meters
    
    [self.locationManager startUpdatingLocation];

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    //[self.locationManager requestAlwaysAuthorization];
    [self.locationManager requestWhenInUseAuthorization];
#endif

}
-(CLLocation*)getCurrentLocation{
    if (self.currentLocation == nil){
        return nil;
    }
    else{
        return self.currentLocation;
    }
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    [manager stopUpdatingLocation];
    self.currentLocation  = [locations lastObject];
    DEBUGLOG(@"latitude %+.6f, longitude %+.6f\n",
             self.currentLocation.coordinate.latitude,
             self.currentLocation.coordinate.longitude)
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    [manager stopUpdatingLocation];
    NSLog(@"LocationManager error: %@",[error localizedDescription]);
    self.currentLocation = nil;
    [Utils showAlert:NSLocalizedString(@"locationserviceerror", nil) delegate:nil];
}

-(void)showRouteWithLogitude:(double)latitude longitude:(double)longitude destnationName:(NSString*)name{
    CLLocationCoordinate2D start = self.locationManager.location.coordinate;
    CLLocationCoordinate2D end = CLLocationCoordinate2DMake(latitude, longitude);
    
    Class mapItemClass = [MKMapItem class];
    
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLLocationCoordinate2D coordinate_end = CLLocationCoordinate2DMake(end.latitude, end.longitude);
        MKPlacemark *placemark_end = [[MKPlacemark alloc] initWithCoordinate:coordinate_end
                                                           addressDictionary:nil];
        MKMapItem *mapItem_end = [[MKMapItem alloc] initWithPlacemark:placemark_end];
        [mapItem_end setName:name];
         CLLocationCoordinate2D coordinate_start = CLLocationCoordinate2DMake(start.latitude, start.longitude);
        MKPlacemark *placemark_srart = [[MKPlacemark alloc] initWithCoordinate:coordinate_start addressDictionary:nil];
        MKMapItem *mapItem_start = [[MKMapItem alloc] initWithPlacemark:placemark_srart];
        [mapItem_start setName:@"Start"];
        
        NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
        
        [MKMapItem openMapsWithItems:@[mapItem_start, mapItem_end]
                       launchOptions:launchOptions];
        
    }else{
       NSURL *url = [[NSURL alloc] initWithString: [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",
                                                      start.latitude, start.longitude, end.latitude, end.longitude]];
        
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
