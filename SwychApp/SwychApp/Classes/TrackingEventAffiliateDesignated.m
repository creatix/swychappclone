//
//  TrackingEventAffiliateDesignated.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventAffiliateDesignated.h"

@implementation TrackingEventAffiliateDesignated
-(id)init{
    self = [super init];
    if (self){
        self.eventName = ANA_EVENT_AffiliateDesignated;
        self.eventCode = @"q2p4ey";
    }
    return self;
}

@end
