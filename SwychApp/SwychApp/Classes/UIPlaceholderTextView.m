//
//  UIPlaceholderTextView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIPlaceholderTextView.h"

@interface UIPlaceholderTextView()

@property (nonatomic,strong) UILabel *labPlaceholder;

-(void)textChanged:(NSNotification*)notificaiton;
@end

@implementation UIPlaceholderTextView

@synthesize placeholderColor = _placeholderColor;
@synthesize placeholder = _placeholder;


-(void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    if(self.labPlaceholder != nil){
        self.labPlaceholder.text = placeholder;
    }
}
-(NSString*)placeholder{
    return _placeholder;
}

-(void)setPlaceholderColor:(UIColor *)placeholderColor{
    _placeholderColor = placeholderColor;
    if (self.labPlaceholder != nil){
        self.labPlaceholder.textColor = placeholderColor;
    }
}
-(UIColor *)placeholderColor{
    if(_placeholderColor == nil){
        _placeholderColor = [UIColor lightGrayColor];
    }
    return _placeholderColor;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self setup:8.0f];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self setup:0.0f];
    }
    return self;
}
-(void)setup:(CGFloat)topOffset{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:self];
    CGSize sz = CGSizeMake(4.0f, 5.5f);
    CGRect rc = CGRectMake(sz.width, sz.height, self.bounds.size.width - 2 * sz.width , 20.0f);
    self.labPlaceholder = [[UILabel alloc] initWithFrame:rc];
    self.labPlaceholder.lineBreakMode = NSLineBreakByCharWrapping;
    self.labPlaceholder.numberOfLines = 0;
    self.labPlaceholder.font = self.font;
    self.labPlaceholder.backgroundColor = [UIColor clearColor];
    self.labPlaceholder.text = self.placeholder;
    self.labPlaceholder.textColor = self.placeholderColor;
    
    [self addSubview:self.labPlaceholder];
}
  
-(void)setFont:(UIFont *)font{
    [super setFont:font];
    self.labPlaceholder.font = font;
}

-(void)setTextAlignment:(NSTextAlignment)textAlignment{
    [super setTextAlignment:textAlignment];
    self.labPlaceholder.textAlignment = textAlignment;
}

-(id)insertDictationResultPlaceholder{
    id placeHolder = [super insertDictationResultPlaceholder];
    self.labPlaceholder.hidden = YES;
    return placeHolder;
}

-(void)removeDictationResultPlaceholder:(id)placeholder willInsertResult:(BOOL)willInsertResult{
    [super removeDictationResultPlaceholder:placeholder willInsertResult:willInsertResult];
    self.labPlaceholder.hidden = NO;
    if ([self.text length] == 0){
        self.labPlaceholder.alpha = 1.0f;
    }
    else{
        self.labPlaceholder.alpha = 0.0f;
    }
}

-(void)setText:(NSString *)text{
    [super setText:text];
    if ([self.text length] == 0){
        self.labPlaceholder.alpha = 1.0f;
    }
    else{
        self.labPlaceholder.alpha = 0.0f;
    }
}

-(void)textChanged:(NSNotification*)notificaiton{
    if ([self.text length] == 0){
        self.labPlaceholder.alpha = 1.0f;
    }
    else{
        self.labPlaceholder.alpha = 0.0f;
    }
}
@end
