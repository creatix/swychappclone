//
//  TrackingEventRegistrationCompleted.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventRegistrationCompleted.h"

@implementation TrackingEventRegistrationCompleted
-(id)init{
    self = [super init];
    if (self){
        self.eventName = ANA_EVENT_RegistrationCompleted;
        self.eventCode = @"k8nvxh";
    }
    return self;
}

@end
