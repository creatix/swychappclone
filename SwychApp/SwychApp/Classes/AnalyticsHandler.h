//
//  AnalyticsHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TrackingEventBase;

@interface AnalyticsHandler : NSObject

+(AnalyticsHandler*)getInstance;

-(void)initialize;
-(void)sendEvent:(TrackingEventBase*)event;

@end
