//
//  TrackingEventBase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackingEventBase : NSObject

@property(nonatomic,strong) NSString *eventCode;
@property(nonatomic,strong) NSString *eventName;
@property(nonatomic,strong) NSDictionary *customValues;
@end
