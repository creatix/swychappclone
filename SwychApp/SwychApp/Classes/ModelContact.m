//
//  ModelContact.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelContact.h"
#import "NSString+Extra.h"
#import "AContactItem.h"

#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumberDefines.h"


@interface ModelContact()
-(NSString*)findPhoneNumber:(ABRecordRef)record;
-(NSString*)findEmail:(ABRecordRef)record;
-(UIImage*)findProfileImage:(ABRecordRef)record;
@end

@implementation ModelContact

@dynamic fullName;

-(NSString*)fullName{
    return [NSString stringWithFormat:@"%@ %@",self.firstName,self.lastName];
}

-(id)initWithABRecordRef:(ABRecordRef)recordRef{
    if (self = [super init]){
        if (recordRef != nil){
            self.firstName =(__bridge_transfer NSString*)ABRecordCopyValue(recordRef, kABPersonFirstNameProperty);
            self.lastName = (__bridge_transfer NSString*)ABRecordCopyValue(recordRef, kABPersonLastNameProperty);
            self.birthday = (__bridge_transfer NSDate*)ABRecordCopyValue(recordRef, kABPersonBirthdayProperty);
            self.phoneNumber = [self findPhoneNumber:recordRef];
            self.email = [self findEmail:recordRef];
            self.profileImage = [self findProfileImage:recordRef];
            self.MyRecordRef = (__bridge id)(recordRef);
            self.TypeOfInit = INIT_TYPE_ABR;
        }
    }
    return self;
}
-(id)initWithCNContact:(CNMutableContact*)cnContact{
    if (self = [super init]){
        if (cnContact != nil){
            self.firstName = cnContact.givenName;
            self.lastName = cnContact.familyName;
            self.birthday = cnContact.birthday.date;
            self.phoneNumber = [self findPhoneNumberInCNContact:cnContact];
            self.email = [self findEmailInCNContact:cnContact];
            self.profileImage = [self findProfileImageInCNContact:cnContact];
            self.MyCnContact = cnContact;
            self.TypeOfInit = INIT_TYPE_CNC;
        }
    }
    return self;
}

-(NSString*)getPurePhoneNumber:(NSString*)phoneNumber{
    return [[phoneNumber componentsSeparatedByCharactersInSet:
             [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
            componentsJoinedByString:@""];
}

-(NSString*)findPhoneNumberInCNContact:(CNContact*)contact{
    NSString *phone = nil;;
    
    if ([contact.phoneNumbers count] > 0){
        self.phoneNumbers = [[NSMutableArray alloc] initWithCapacity:[contact.phoneNumbers count]];
        self.phoneNumbersAndTitle = [[NSMutableArray alloc] initWithCapacity:[contact.phoneNumbers count]];
        for(CNLabeledValue *label in contact.phoneNumbers){
            AContactItem* item = [AContactItem new];
            if (phone == nil && [label.label isEqualToString:CNLabelPhoneNumberiPhone]){
                phone =[self getPurePhoneNumber:[label.value stringValue]];
            }
            else if (phone == nil && [label.label isEqualToString:CNLabelPhoneNumberMobile]){
                phone = [self getPurePhoneNumber: [label.value stringValue]];
            }
            NSString* strTitle;
            strTitle = [label.label stringByReplacingOccurrencesOfString:@"_$!<"
                                                     withString:@""];
            strTitle = [strTitle stringByReplacingOccurrencesOfString:@">!$_"
                                                     withString:@""];
            item.Title_Item = strTitle;
            item.Content_Item = [self getPurePhoneNumber:[label.value stringValue]];
            if([strTitle.lowercaseString isEqualToString:@"iphone"] || [strTitle.lowercaseString isEqualToString:@"mobile"]){
            [self.phoneNumbersAndTitle addObject:item];
            [self.phoneNumbers addObject:[self getPurePhoneNumber:[label.value stringValue]]];
            }
        }
    }
    else{
        if (self.phoneNumbersAndTitle != nil){
            [self.phoneNumbersAndTitle removeAllObjects];
        }
    }
    return phone;
}

-(NSString*)findPhoneNumber:(ABRecordRef)record{
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
    NSString *phone = nil;
    if (phoneNumbers != nil && ABMultiValueGetCount(phoneNumbers) > 0){
        self.phoneNumbersAndTitle = [[NSMutableArray alloc] init];
        self.phoneNumbers = [[NSMutableArray alloc] init];
        
        for (CFIndex idx = 0 ; idx < ABMultiValueGetCount(phoneNumbers); idx ++){
            AContactItem* item = [AContactItem new];
            NSString *label = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phoneNumbers, idx);
            if ([label isEqualToString:(NSString *)kABPersonPhoneIPhoneLabel]){
                phone =[self getPurePhoneNumber:(__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, idx)];
            }
            else if ([label isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
                phone = [self getPurePhoneNumber:(__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, idx)];
            }
            label = [label stringByReplacingOccurrencesOfString:@"_$!<"
                                                 withString:@""];
            label = [label stringByReplacingOccurrencesOfString:@">!$_"
                                                     withString:@""];
            item.Title_Item = label;
            item.Content_Item  = [self getPurePhoneNumber:(__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, idx)];
            if([label.lowercaseString isEqualToString:@"iphone"] || [label.lowercaseString isEqualToString:@"mobile"]){
            [self.phoneNumbersAndTitle addObject:item];
            [self.phoneNumbers addObject:[self getPurePhoneNumber:(__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, idx)]];
            }
        }
        if (phone == nil){
            phone = [self.phoneNumbers objectAtIndex:0];
        }
        CFRelease(phoneNumbers);
        
        return phone;
    }
    else{
        if (self.phoneNumbersAndTitle != nil){
            [self.phoneNumbersAndTitle removeAllObjects];
        }
    }
    return nil;
}
-(NSString*)findEmailInCNContact:(CNContact*)contact{
    if (contact.emailAddresses != nil && [contact.emailAddresses count] > 0){
        self.emails = [[NSMutableArray alloc] init];
        self.emailsAndTitle = [[NSMutableArray alloc] init];
        for(CNLabeledValue *label in contact.emailAddresses){
            AContactItem* item = [AContactItem new];
            NSString* strLabel = [label.label stringByReplacingOccurrencesOfString:@"_$!<"
                                                     withString:@""];
            strLabel = [strLabel stringByReplacingOccurrencesOfString:@">!$_"
                                                     withString:@""];
            item.Title_Item = strLabel;
            item.Content_Item = label.value;
            [self.emails addObject:label.value];
            [self.emailsAndTitle addObject:item];
        }
    }
    else{
        if (self.emails != nil){
            [self.emails removeAllObjects];
        }
        if (self.emailsAndTitle != nil){
            [self.emailsAndTitle removeAllObjects];
        }
    }
    return (self.emails == nil || [self.emails count] == 0) ? nil : [self.emails objectAtIndex:0];
}

-(NSString*)findEmail:(ABRecordRef)record{
    ABMultiValueRef emails = ABRecordCopyValue(record, kABPersonEmailProperty);
    if (emails != nil && ABMultiValueGetCount(emails) > 0){
        self.emails = [[NSMutableArray alloc] init];
        self.emailsAndTitle = [[NSMutableArray alloc] init];
        for (CFIndex idx = 0 ; idx < ABMultiValueGetCount(emails); idx ++){
            AContactItem* item = [AContactItem new];
            NSString *label = (__bridge_transfer NSString*)ABMultiValueCopyLabelAtIndex(emails, idx);
            NSString *email = (__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(emails, idx);
            [self.emails addObject:email];
            label = [label stringByReplacingOccurrencesOfString:@"_$!<"
                                                     withString:@""];
            label = [label stringByReplacingOccurrencesOfString:@">!$_"
                                                     withString:@""];
            item.Title_Item = label;
            item.Content_Item = email;
            [self.emailsAndTitle addObject:item];
            
        }
        CFRelease(emails);
    }
    else{
        if (self.emails != nil){
            [self.emails removeAllObjects];
        }
        if (self.emailsAndTitle != nil){
            [self.emailsAndTitle removeAllObjects];
        }
    }
    return (self.emails == nil || [self.emails count] == 0) ? nil : [self.emails objectAtIndex:0];
}
-(UIImage*)findProfileImageInCNContact:(CNContact*)contact{
    if ( contact.imageData != nil){
        UIImage *img = [UIImage imageWithData:contact.imageData];
        return img;
    }
    return nil;
}

-(UIImage*)findProfileImage:(ABRecordRef)record{
    CFDataRef imageData = ABPersonCopyImageData(record);
    if (imageData != nil){
        UIImage *img = [UIImage imageWithData:(__bridge NSData*)imageData];
        CFRelease(imageData);
        return img;
    }
    return nil;
}

-(BOOL)hasPhoneNumberStartWith:(NSString*)startWith{
    if (self.phoneNumbersAndTitle == nil || [self.phoneNumbersAndTitle count] == 0){
        return NO;
    }
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(AContactItem *phone, NSDictionary<NSString *,id> * _Nullable bindings) {
        if (phone.Content_Item == nil){
            return NO;
        }
        return [phone.Content_Item rangeOfString:startWith].location == 0;
    }];
    NSArray *array = [self.phoneNumbersAndTitle filteredArrayUsingPredicate:pred];
    
    return  [array count] > 0;
}
-(BOOL)hasPhoneNumberContainsWith:(NSString*)startWith{
    if (self.phoneNumbersAndTitle == nil || [self.phoneNumbersAndTitle count] == 0){
        return NO;
    }
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(AContactItem *phone, NSDictionary<NSString *,id> * _Nullable bindings) {
        if (phone.Content_Item == nil){
            return NO;
        }
        @try {
            NSString *input = startWith;
            NSString *contactNumber = phone.Content_Item;
            //convert both number to "+1##########" format
            NSError *error = nil;
            NBPhoneNumber *nbPhoneNumberInput = [[NBPhoneNumberUtil sharedInstance] parse:input defaultRegion:@"US" error:&error];
            NBPhoneNumber *nbPhoneNumberContactNumber = [[NBPhoneNumberUtil sharedInstance] parse:contactNumber defaultRegion:@"US" error:&error];
            if (nbPhoneNumberInput != nil && nbPhoneNumberContactNumber != nil){
                NSString *formattedInput = [[NBPhoneNumberUtil sharedInstance] format:nbPhoneNumberInput
                                                                         numberFormat:NBEPhoneNumberFormatE164
                                                                                error:&error];
                NSString *formattedSource = [[NBPhoneNumberUtil sharedInstance] format:nbPhoneNumberContactNumber
                                                                         numberFormat:NBEPhoneNumberFormatE164
                                                                                error:&error];
                if (formattedInput != nil && formattedSource != nil){
                    return [formattedInput containsString:formattedSource];
                }
                else{
                    return [startWith containsString:phone.Content_Item];
                }
            }
            else{
                return [startWith containsString:phone.Content_Item];
            }
        } @catch (NSException *exception) {
        } @finally {
        }
        return [startWith containsString:phone.Content_Item];
    }];
    NSArray *array = [self.phoneNumbersAndTitle filteredArrayUsingPredicate:pred];
    
    return  [array count] > 0;
}
-(BOOL)hasEmailAddresStartWith:(NSString*)startWith{
    if (self.emails == nil || [self.emails count] == 0){
        return NO;
    }
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(NSString *email, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [[email lowercaseString] rangeOfString:startWith].location == 0;
    }];
    NSArray *array = [self.emails filteredArrayUsingPredicate:pred];
    
    return  [array count] > 0;
}

-(BOOL)hasNameStartWith:(NSString*)name{
    return [[self.firstName lowercaseString] rangeOfString:name].location == 0 || [[self.lastName lowercaseString] rangeOfString:name].location == 0;
}

-(BOOL)hasNameContains:(NSString*)partialName{
    if ([partialName rangeOfString:@","].location != NSNotFound){// case of "LastName,FirstName"
        NSArray *array = [partialName componentsSeparatedByString:@","];
        NSString *lName = [[array objectAtIndex:0] trim];
        NSString *fName = [[array objectAtIndex:1] trim];
        
        NSString *lastName = self.lastName == nil ? @"" : [self.lastName lowercaseString];
        NSString *firstName = self.firstName == nil ? @"" : [self.firstName lowercaseString];
        if ([lName length] == 0){
            return [firstName rangeOfString:fName].location != NSNotFound;
        }
        else if ([fName length] == 0){
            return [lastName rangeOfString:lName].location != NSNotFound;
        }
        else{
            return [firstName rangeOfString:fName].location != NSNotFound && [lastName rangeOfString:lName].location != NSNotFound;
        }
    }
    else if ([partialName rangeOfString:@" "].location != NSNotFound){//case of "FirstName lastName"
        NSArray *array = [partialName componentsSeparatedByString:@" "];
        NSString *lName = [[array objectAtIndex:[array count] - 1] trim];
        NSString *fName = [[array objectAtIndex:0] trim];
        
        NSString *lastName = self.lastName == nil ? @"" : [self.lastName lowercaseString];
        NSString *firstName = self.firstName == nil ? @"" : [self.firstName lowercaseString];
        
        if ([lName length] == 0){
            return [firstName rangeOfString:fName].location != NSNotFound;
        }
        else if ([fName length] == 0){
            return [lastName rangeOfString:lName].location != NSNotFound;
        }
        else{
            return [firstName rangeOfString:fName].location != NSNotFound && [lastName rangeOfString:lName].location != NSNotFound;
        }
    }
    else{
        NSString *fullName = [[NSString stringWithFormat:@"%@%@",self.firstName == nil ? @"" : self.firstName,
                               self.lastName == nil ? @"" : self.lastName] lowercaseString];
        return [fullName rangeOfString:partialName].location != NSNotFound;
    }
}
-(BOOL)changeContactPhoneNumber:(NSString *) phoneSought forThis:(NSString *) newPhoneNumber{
    if(self.TypeOfInit == INIT_TYPE_ABR){
        return  [self changeContactPhoneNumberABR:phoneSought forThis:newPhoneNumber];
    }
    else{
        return [self changeContactPhoneNumberCN:phoneSought forThis:newPhoneNumber];
    }
    
}
-(BOOL)changeContactPhoneNumberCN:(NSString *) phoneSought forThis:(NSString *) newPhoneNumber{
    CNContactStore* store = [CNContactStore new];
    CNMutableContact *con = self.MyCnContact;
    NSMutableArray * curePhoness = [[con phoneNumbers] mutableCopy];
    for(CNLabeledValue *label in curePhoness){
        if([phoneSought isEqualToString:[self getPurePhoneNumber:[label.value stringValue]]]){
            CNLabeledValue * phone = [CNLabeledValue labeledValueWithLabel:label.label value:[CNPhoneNumber phoneNumberWithStringValue:newPhoneNumber]];
            [curePhoness addObject:phone];
            [curePhoness removeObject:label];
            break;
        }
    }
    con.phoneNumbers = curePhoness;
    CNSaveRequest *sr = [[CNSaveRequest alloc] init];
    [sr updateContact:con];
    [store executeSaveRequest:sr error:nil];
    [self findPhoneNumberInCNContact:con];
    return YES;
}
-(BOOL)changeContactPhoneNumberABR:(NSString *) phoneSought
                         forThis:(NSString *) newPhoneNumber{
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    //ABRecordRef contactSelected = NULL;
    CFStringRef mobileLabelNumber = NULL;
    CFErrorRef error = nil;
    
        
    ABRecordRef ref = (__bridge ABRecordRef)(self.MyRecordRef);
    
    ABMultiValueRef phones = (ABMultiValueRef)ABRecordCopyValue(ref, kABPersonPhoneProperty);
    NSString* mobilePhoneNumber=@"";
    
    BOOL ifFindTheNumber = NO;
    if (ABMultiValueGetCount(phones) > 0) {
        for (int i=0; i < ABMultiValueGetCount(phones); i++) {
            
            mobilePhoneNumber = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            mobilePhoneNumber = [self getPurePhoneNumber:mobilePhoneNumber];
            if([mobilePhoneNumber isEqualToString:phoneSought]){
                mobileLabelNumber = ABMultiValueCopyLabelAtIndex(phones, i);
                ifFindTheNumber = YES;
            }
        }
    }
    for(int i = 0;i<self.phoneNumbersAndTitle.count;i++){
        AContactItem* item = (AContactItem*)self.phoneNumbersAndTitle[i];
        if([item.Content_Item isEqualToString:phoneSought]){
            item.Content_Item = newPhoneNumber;
            NSLog(@"Phone number updated: %@", newPhoneNumber);
        }
    
    }
    if(!ifFindTheNumber)
    {
        CFRelease(phones);
        return NO;
    }
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABPersonPhoneProperty);
    bool didAddPhone = ABMultiValueAddValueAndLabel(phoneNumberMultiValue ,(__bridge CFTypeRef)newPhoneNumber,mobileLabelNumber, NULL);
    
    NSNumber *recordId = [NSNumber numberWithInteger:ABRecordGetRecordID(ref)];
    NSLog(@"record id is %@",recordId);
    if(didAddPhone){
        ABRecordSetValue(ABAddressBookGetPersonWithRecordID(addressBook, (int)recordId.integerValue),
                         kABPersonPhoneProperty,
                         phoneNumberMultiValue,
                         nil);
        
        bool bSuccess = ABAddressBookSave(addressBook, &error);
        CFRelease(phones);
        if (!bSuccess) {
            NSLog(@"Could not save to address book: %@", error);
        } else {
            return YES;
        }
        
    } else {
        CFRelease(phones);
        NSLog(@"Error editing phone number: %@", error);
        error = nil;
    }
    
    return NO;
}
-(BOOL)changeContactEmail:(NSString *) emailSought
                  forThis:(NSString *) newEmail{
    if(self.TypeOfInit == INIT_TYPE_ABR){
        return  [self changeContactEmailABR:emailSought forThis:newEmail];
    }
    else{
        return [self changeContactEmailCN:emailSought forThis:newEmail];
    }

}
-(BOOL)changeContactEmailCN:(NSString *) emailSought forThis:(NSString *) newEmail{
    CNContactStore* store = [CNContactStore new];
    CNMutableContact *con = self.MyCnContact;
    NSMutableArray * curemailAddresses = [[con emailAddresses] mutableCopy];
    for(CNLabeledValue *label in curemailAddresses){
        if([emailSought isEqualToString:label.value]){
            //        AContactItem* item = [AContactItem new];
            //        NSString* strLabel = [label.label stringByReplacingOccurrencesOfString:@"_$!<"
            //                                                                    withString:@""];
            //        strLabel = [strLabel stringByReplacingOccurrencesOfString:@">!$_"
            //                                                       withString:@""];
            //        item.Title_Item = strLabel;
            //        item.Content_Item = label.value;
            CNLabeledValue * email = [CNLabeledValue labeledValueWithLabel:label.label value:newEmail];
            [curemailAddresses addObject:email];
            [curemailAddresses removeObject:label];
            break;
        }
    }
    con.emailAddresses = curemailAddresses;
    CNSaveRequest *sr = [[CNSaveRequest alloc] init];
    [sr updateContact:con];
    [store executeSaveRequest:sr error:nil];
    [self findEmailInCNContact:con];
    return YES;
}
-(BOOL)changeContactEmailABR:(NSString *) emailSought
                        forThis:(NSString *) newEmail{
    
    CFStringRef mobileLabelNumber = NULL;
    CFErrorRef error = nil;
    
    ABRecordRef ref = (__bridge ABRecordRef)(self.MyRecordRef);
    ABMultiValueRef emails = ABRecordCopyValue(ref, kABPersonEmailProperty);
    NSString* mobileEmail=@"";
    
    BOOL ifFindTheNumber = NO;
    if (ABMultiValueGetCount(emails) > 0) {
        for (int i=0; i < ABMultiValueGetCount(emails); i++) {
            
            mobileEmail = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
            if([mobileEmail isEqualToString:emailSought]){
                mobileLabelNumber = ABMultiValueCopyLabelAtIndex(emails, i);
                ifFindTheNumber = YES;
            }
        }
    }
    for(int i = 0;i<self.emailsAndTitle.count;i++){
        AContactItem* item = (AContactItem*)self.emailsAndTitle[i];
        if([item.Content_Item isEqualToString:emailSought]){
            item.Content_Item = newEmail;
            NSLog(@"Email updated: %@", newEmail);
        }
        
    }
    if(!ifFindTheNumber)
    {
    CFRelease(emails);
        return NO;
    }
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABPersonEmailProperty);
    bool didAddPhone = ABMultiValueAddValueAndLabel(phoneNumberMultiValue ,(__bridge CFTypeRef)newEmail,mobileLabelNumber, NULL);
    
    NSNumber *recordId = [NSNumber numberWithInteger:ABRecordGetRecordID(ref)];
    NSLog(@"record id is %@",recordId);
    if(didAddPhone){
        ABAddressBookRef addressBook = ABAddressBookCreate();
        ABRecordSetValue(ABAddressBookGetPersonWithRecordID(addressBook, (int)recordId.integerValue),
                         kABPersonEmailProperty,
                         phoneNumberMultiValue,
                         nil);
        
        bool bSuccess = ABAddressBookSave(addressBook, &error);
        CFRelease(emails);
        CFRelease(addressBook);
        if (!bSuccess) {
            NSLog(@"Could not save to address book: %@", error);
        } else {
            return YES;
        }
        
    } else {
        CFRelease(emails);
        NSLog(@"Error editing phone number: %@", error);
        error = nil;
    }
    
    return NO;
}
-(BOOL)changeBirthday:(NSDate *) newBirthday{
    if(self.TypeOfInit == INIT_TYPE_ABR){
    return  [self changeBirthdayABR:newBirthday];
    }
    else{
    return [self changeBirthdayCN:newBirthday];
    }

}
-(BOOL)changeBirthdayABR:(NSDate *) newBirthday{
    ABRecordRef ref = (__bridge ABRecordRef)(self.MyRecordRef);
    NSNumber *recordId = [NSNumber numberWithInteger:ABRecordGetRecordID(ref)];
    NSLog(@"record id is %@",recordId);

    ABAddressBookRef addressBook = ABAddressBookCreate();
    ABRecordSetValue(ABAddressBookGetPersonWithRecordID(addressBook, (int)recordId.integerValue),
                     kABPersonBirthdayProperty,
                     (__bridge CFDateRef)newBirthday,
                     nil);
    self.birthday = newBirthday;
    CFRelease(addressBook);

    return YES;
}
-(BOOL)changeBirthdayCN:(NSDate *) newBirthday{
    CNContactStore* store = [CNContactStore new];
    CNMutableContact *con = self.MyCnContact;
    NSDateComponents * newdate = [[NSCalendar currentCalendar] components:NSCalendarUnitDay |NSCalendarUnitMonth| NSCalendarUnitYear fromDate:newBirthday];
    NSDateComponents *components = con.birthday;
    if(components!=nil){
    components.year = newdate.year;
    components.month = newdate.month;
    components.day = newdate.day;
  
    }
    else{
        components = newdate;
    }
      [con setBirthday:components];
    self.birthday = newBirthday;
    CNSaveRequest *sr = [[CNSaveRequest alloc] init];
    [sr updateContact:con];
    [store executeSaveRequest:sr error:nil];

    return YES;
}

-(BOOL)hasPhoneNumber:(NSString*)phoneNumber{
    if (self.phoneNumbers.count == 0){
        return NO;
    }
    for(NSString *phone in self.phoneNumbers){
        if(phone.length >= phoneNumber.length){
            NSInteger start = phone.length - phoneNumber.length;
            if([[phone substringFromIndex:start] isEqualToString:phoneNumber]){
                return YES;
            }
        }
        else if (phone.length == 10 && phone.length == (phoneNumber.length - 1)){
            if ([phone isEqualToString:[phoneNumber substringFromIndex:1]]){
                return YES;
            }
        }
    }
    return NO;
}
@end
