//
//  ContactBookManager.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ModelContact;
@interface ContactBookManager : NSObject

+(ContactBookManager*)instance;
-(NSArray*)getAllContacts;
+(UIImage*)getContactHeadImgByPhoneNumber:(NSString*)phone;
+(NSString*)getContactFirstNameByPhoneNumber:(NSString*)phone;
+(NSArray*)getContactItemListByModelList:(NSArray*)users;
@end
