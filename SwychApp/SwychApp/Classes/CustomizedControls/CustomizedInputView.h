//
//  CustomizedInputView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,InputViewType){
    CustomizedInputView_Date = 1,
    CustomizedInputView_DateTime,
    CustomizedInputView_TableView_SingleSeledtion,
    CustomizedInputView_TableView_MultipleSelection,
    CustomizedInputView_PickerView_Numbers,
    CustomizedInputView_PickerView_List
};

@protocol CustomizedInputViewDelegate;


@interface CustomizedInputView : UIView<UIGestureRecognizerDelegate,UITableViewDelegate,UIPickerViewDelegate>

@property(nonatomic,weak) id<CustomizedInputViewDelegate> delegate;
@property(nonatomic,assign) InputViewType viewType;
@property(nonatomic,strong) UIView *objectWithInput;

@property(nonatomic,assign) NSInteger context;
@property (nonatomic,strong) NSObject *contextObject;
@property(nonatomic,strong) UIColor *textColorOfContent;
@property(nonatomic,strong) UIFont *titleFont;
+(CustomizedInputView*)createInstanceWithViewType:(InputViewType)type objectWithInput:(UIView*)object;
+(CustomizedInputView*)createInstanceWithViewType:(InputViewType)type objectWithInput:(UIView*)object doneButton:(NSString*)doneText;

-(void)setTitle:(NSString*)title;
-(void)enableDoneButton:(BOOL)enable;

-(void)setDataPickerDataSource:(id<UIPickerViewDataSource>)pickerViewDataSource;
-(void)updateDataPickerView;

-(void)setTableViewDataSource:(id<UITableViewDataSource>)tableViewDataSource;
-(void)registerNibForCell:(NSString*)nibName indentifier:(NSString*)identifier;
-(void)updateTableView;

-(void)show;

-(NSDate*)getDate;
-(NSArray*)getDataPickerValue;
-(NSInteger)getDataPickerSelectedNumber;

-(void)setDatePickerMinDate:(NSDate*)min maxDate:(NSDate*)maxDate;

@end

@protocol CustomizedInputViewDelegate <NSObject>

@optional
-(void)customizedInputViewDidShow:(CustomizedInputView*)inputView;
-(void)customizedInputViewValueChanged:(CustomizedInputView*)inputView;
-(void)customizedInputViewDoneButtonTapped:(CustomizedInputView*)inputView;
-(void)customizedInputviewDismissed:(CustomizedInputView*)inputView doneButtonTapped:(BOOL)done;

//PickerView
-(void)customizedInputView:(CustomizedInputView*)inputView pickerView:(UIPickerView*) pickerView valueChangedForRow:(NSInteger)row forComponent:(NSInteger)component;
-(NSString*)customizedInputView:(CustomizedInputView*)inputView stringForPickerView:(UIPickerView*)pickerView forRow:(NSInteger)row forComponet:(NSInteger)component;
-(UIView*)customizedInputView:(CustomizedInputView*)inputView viewForPickerView:(UIPickerView*)pickerView forRow:(NSInteger)row forComponent:(NSInteger)component;

//TableView delegate methods
-(CGFloat)customizedInputView:(CustomizedInputView*)inputView heightOfRowInTableView:(UITableView*) tableView row:(NSInteger)row section:(NSInteger)section;
-(void)customizedInputView:(CustomizedInputView*)inputView tableView:(UITableView*)tableView rowDidSelected:(NSIndexPath*)indexPath cell:(UITableViewCell*)cell;

@end