//
//  TransactionDetailCardInfoImgView.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-07.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelTransaction.h"
@interface TransactionDetailCardInfoImgView : UIView
@property (weak,nonatomic) IBOutlet UIImageView *backGroundImg;
@property (weak,nonatomic) IBOutlet UIImageView *imgFavor;
@property (weak,nonatomic) IBOutlet UIImageView *imgBKBottom;
@property (weak,nonatomic) IBOutlet UILabel *lblName;
@property (strong,nonatomic)  ModelTransaction *transaction;
@end
