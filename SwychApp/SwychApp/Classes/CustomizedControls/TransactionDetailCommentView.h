//
//  TransactionDetailCommentView.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-07.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelTransaction.h"
#import "ModelPaymentInfo.h"
#import "UIRoundCornerButton.h"
#import "TransactionMessageCell.h"
#import "GiftAttachmentView.h"
@protocol HistoryTransactionDetailCommentViewDelegate;
@interface TransactionDetailCommentView : UIView<UITableViewDelegate,UITableViewDataSource,TransactionMessageCellDelegate,GiftAttachmentViewDelegate>{
    NSMutableArray<ModelPaymentInfo*> *payments;
    NSMutableArray<ModelPaymentInfo*> *payments2;
}
@property (strong,nonatomic)  ModelTransaction *transaction;
@property (nonatomic)  int viewIndex;
@property (strong,nonatomic) IBOutlet UITableView *tbContent;
@property (strong,nonatomic) IBOutlet UITableView *tbContent1;
@property (strong,nonatomic) IBOutlet UIRoundCornerButton *btnTakeBack;
@property (strong,nonatomic) IBOutlet UIRoundCornerButton *btnRemind;
@property (weak, nonatomic) IBOutlet GiftAttachmentView *vAttachmentView;
@property (strong,nonatomic) IBOutlet UITableView *tbContentSweych;
@property (strong,nonatomic) IBOutlet NSLayoutConstraint *SweychBonusViewHeigt;
@property (weak,nonatomic) id<HistoryTransactionDetailCommentViewDelegate> selectionViewDelegate;
- (IBAction)buttonTapped:(id)sender;
@end
@protocol HistoryTransactionDetailCommentViewDelegate <NSObject>

@optional
-(void)TakeBack;
-(void)Remind;
-(void)SayThanks:(NSString*)text;
@end