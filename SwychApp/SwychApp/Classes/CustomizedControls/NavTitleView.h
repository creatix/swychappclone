//
//  NavTitleView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavTitleView : UIView

@property(nonatomic,strong) NSString *titleText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constratintSwychLogoHeight;
@end
