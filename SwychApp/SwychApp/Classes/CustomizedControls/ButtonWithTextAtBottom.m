//
//  ButtonWithTextAtBottom.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ButtonWithTextAtBottom.h"
#import "UIImage+Extra.h"

#define DefaultTitleTextColor   UICOLORFROMRGB(192, 31, 109,1.0f)
#define DefaultTitleTextFont    [UIFont fontWithName:@"Helvetica Neue" size:14.0f]

@interface ButtonWithTextAtBottom()
@property (weak, nonatomic) IBOutlet UIView *vNormal;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UIImageView *ivImageView;

@property (weak, nonatomic) IBOutlet UIView *vSingleImage;
@property (weak, nonatomic) IBOutlet UIImageView *ivPicture;
@property (weak, nonatomic) IBOutlet UIImageView *ivCloseButton;


@property (nonatomic,strong) UITapGestureRecognizer *tapImageButton;
@property (nonatomic,strong) UITapGestureRecognizer *tapCloseImageButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNormalTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNormalLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNormalRight;

-(void)touchOnView:(id)sender;
@end

@implementation ButtonWithTextAtBottom

@synthesize text = _text;
@synthesize buttonImage = _buttonImage;
@synthesize textColor = _textColor;
@synthesize textFont = _textFont;
@synthesize showCloseButton = _showCloseButton;
@synthesize pictureImageViewImage = _pictureImageViewImage;
@synthesize normalImageMargin = _normalImageMargin;

-(void)setNormalImageMargin:(CGFloat)normalImageMargin{
    _normalImageMargin = normalImageMargin;
    self.constraintNormalTop.constant =  0;//_normalImageMargin;
    self.constraintNormalLeft.constant = _normalImageMargin;
    self.constraintNormalRight.constant = _normalImageMargin;
}
-(CGFloat)normalImageMargin{
    return _normalImageMargin;
}

-(void)setPictureImageViewImage:(UIImage *)pictureImageViewImage{
    _pictureImageViewImage = pictureImageViewImage;
    self.ivPicture.image = [pictureImageViewImage scaletoSizeKeepAspect:CGSizeMake(self.ivPicture.bounds.size.width, self.ivPicture.bounds.size.height)];
}

-(UIImage*)singleImageViewImage{
    return _pictureImageViewImage;
}

-(void)setShowCloseButton:(BOOL)showCloseButton{
    _showCloseButton = showCloseButton;
    if(showCloseButton){
        self.vNormal.hidden = YES;
        self.vSingleImage.hidden = NO;
    }
    else{
        self.vNormal.hidden = NO;
        self.vSingleImage.hidden = YES;
    }
}
-(BOOL)showCloseButton{
    return _showCloseButton;
}


-(UIColor *)textColor{
    if (_textColor == nil){
        _textColor = DefaultTitleTextColor;
    }
    return _textColor;
}
-(void)setTextColor:(UIColor *)textColor{
    _textColor = textColor;
    if (self.labName != nil){
        self.labName.textColor = textColor;
    }
}


-(UIFont*)textFont{
    if (_textFont == nil){
        _textFont = DefaultTitleTextFont;
    }
    return _textFont;
}
-(void)setTextFont:(UIFont *)textFont{
    _textFont = textFont;
    if (self.labName != nil){
        self.labName.font = textFont;
    }
}


-(NSString *)text{
    return _text;
}
-(void)setText:(NSString *)text{
    _text = text;
    if (self.labName != nil){
        self.labName.text = text;
    }
}

-(UIImage *)buttonImage{
    return _buttonImage;
}
-(void)setButtonImage:(UIImage *)buttonImage{
    _buttonImage = buttonImage;
    if (self.ivImageView != nil){
        self.ivImageView.image = buttonImage;
    }
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"ButtonWithTextAtBottom" class:[self class] owner:self options:nil];
    
    UIColor *clr = [UIColor clearColor];
    self.backgroundColor = clr;
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.labName.backgroundColor  = [UIColor clearColor];
    self.ivImageView.backgroundColor = [ UIColor clearColor];
    
    self.tapImageButton = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    self.tapImageButton.numberOfTapsRequired = 1;
    self.tapImageButton.numberOfTouchesRequired = 1;
    [self.tapImageButton setCancelsTouchesInView:NO];
    [self addGestureRecognizer:self.tapImageButton];
    
    self.tapCloseImageButton = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    self.tapCloseImageButton.numberOfTapsRequired = 1;
    self.tapCloseImageButton.numberOfTouchesRequired = 1;
    [self.tapCloseImageButton setCancelsTouchesInView:NO];
    [self.ivCloseButton addGestureRecognizer:self.tapCloseImageButton];

    self.showCloseButton = NO;
}
-(void)SetColorOfImg:(UIColor*)newcolor{
   _ivImageView.image = [Utils ChangeImageColor:_ivImageView.image color:newcolor];
}
-(void)touchOnView:(id)sender{
    if (sender == self.tapImageButton){
        if (self.itemDelegate != nil && [self.itemDelegate respondsToSelector:@selector(buttonWithTextAtBottonItemTapped:)]){
            [self.itemDelegate buttonWithTextAtBottonItemTapped:self];
        }
    }
    else if (sender == self.tapCloseImageButton){
        if (self.itemDelegate != nil && [self.itemDelegate respondsToSelector:@selector(buttonWithTextAtBottomCloseButtonTapped:)]){
            [self.itemDelegate buttonWithTextAtBottomCloseButtonTapped:self];
        }
    }
}

@end
