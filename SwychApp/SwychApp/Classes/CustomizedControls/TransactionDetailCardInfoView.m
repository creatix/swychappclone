//
//  TransactionDetailCardInfoView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-06.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "NSString+Extra.h"
#import "ContactBookManager.h"
#import "TransactionDetailCardInfoView.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Extra.h"
#import "Utils.h"
#import "TransactionDetailViewController.h"
#import "ContactBookManager.h"
#import "ModelUser.h"
@implementation TransactionDetailCardInfoView
@synthesize transaction = _transaction;
-(void)setTransaction:(ModelTransaction *)transaction{
    int iIndex = [TransactionDetailViewController getiIndex];
    _transaction = transaction;
    self.imgCard.giftcard = _transaction.giftCard;
    [self.Ammount AmountFormatWithDMark:_transaction.amount fontsize:10.0f lengthOfRange:2 ifFromTheLeft:NO];
    self.lblPhoneNumber.text = [Utils getFormattedPhoneNumber:_transaction.recipientPhoneNumber  withFormat:@"-"];
    self.lblOrderNumber.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Transaction_detail_order_no", nil), _transaction.transactionId];
    _transaction.recipientName = [_transaction.recipientName trim];
    
    
    if(iIndex == E_giftSentOut||iIndex == Paid){
        if(_transaction.recipientName==nil||[_transaction.recipientName isEqualToString:@""]){
            self.lblToConstraintHiegth.constant = 0.0f;
        }
        else{
            self.lblTo.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Transaction_detail_Send_To", nil),_transaction.recipientName];
        }
    }
    else{
         ModelUser* sender = _transaction.sender;
        if(sender==nil||sender.firstName == nil){
            self.lblToConstraintHiegth.constant = 0.0f;
        }
        else{
           if(sender.lastName ==nil ) sender.lastName = @"";
            if(sender!=nil){
            self.lblTo.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Transaction_detail_Send_From", nil),[NSString stringWithFormat:@"%@ %@",sender.firstName, sender.lastName]];
            }
        }
    }
    
    self.lblStatus.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Transaction_detail_Status",nil),[Utils getPaymetStatusName:(int)_transaction.transactionType]];
   UIImage* img = [ContactBookManager getContactHeadImgByPhoneNumber:_transaction.recipientPhoneNumber];
    
    if(img!=nil){
        img = [Utils resizeHeaderImg:img];
        [_backGround setAlpha:0.3f];
        [_backGround setImage: img];
    }
    
    if(iIndex == RedeemedAsSwychBalace)
    {
        self.lblTo.hidden = YES;
        self.lblPhoneNumber.hidden = YES;
    }
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}
-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"TransactionDetailCardInfoView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    

//    
//    [self.tvGiftcards registerNib:[UINib nibWithNibName:@"TransactionHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"transactionDetailcell"];
//    [self.cvGiftcards registerNib:[UINib nibWithNibName:@"TransactionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TransactionCollectionViewDetailCell"];
//    
//    
//    self.cvGiftcards.hidden = NO;
//    self.tvGiftcards.hidden = YES;
//    self.cvGiftcards.delegate = self;
//    self.cvGiftcards.dataSource = self;
//    self.tvGiftcards.delegate = self;
//    self.tvGiftcards.dataSource = self;
}
@end
