//
//  OrderCheckoutDetailView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "OrderCheckoutDetailView.h"
#import "SwychBaseViewController.h"
#import "AppDelegate.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "CreditPointsManager.h"
#import "ModelCreditPointsForPurchase.h"
#import "ModelGiftCard.h"
#import "UIButton+Extra.h"
#import "ModelPaymentInfo.h"
#import "ModelCreditPointsForPurchase.h"
#import "CustomizedInputViewListTableViewCell.h"
#import "Utils.h"
#import "UIImageView+WebCache.h"
#import "ModelTransaction.h"
#import "UILabelExt.h"

#import "ResponseVerifyPromotionCode.h"

#define CustomziedInputContext_Credit       1
#define CustomziedInputContext_Reward       2


#define ZERO_MONEY              [NSString stringWithFormat:FORMAT_MONEY,0.0]

#define TEXT_COLOR_ACTIVE       UICOLORFROMRGB(76, 77, 77, 1.0)
#define TEXT_COLOR_INACTIVE     UICOLORFROMRGB(162, 162, 162, 1.0)
#define TEXT_COLOR_SAVEUPTO     UICOLORFROMRGB(40, 177,192, 1.0)

#define DISCOUNT_VIEW_HEIGHT        40.0f;
#define CREDITS_VIEW_HEIGHT         70.0f;

@interface ModelGiftCard(Extra)
@property(nonatomic,readonly) NSString *giftcardTransactionIdString;
@end

@implementation ModelGiftCard(Extra)

-(NSString*)giftcardTransactionIdString{
    return [NSString stringWithFormat:@"%@",self.parentTransactionId];
}

@end
@interface OrderCheckoutDetailView(){
    ModelPaymentInfo *paymentPromotionCode;
    ModelPaymentInfo *paymentCredit;
    ModelPaymentInfo *paymentRewardPoints;
    ModelPaymentInfo *paymentDue;
    
    NSString *promotionCode;
    NSArray *creditGiftcards;
    
    NSMutableDictionary *dictSelectedCredits;
    NSMutableDictionary *dictSelectedCreditsTemp;
    
    double promotionDiscount;
    
    double creditUpTo;
    double creditSelected;
    
    NSInteger pointsEntered;
    
    NSMutableArray<NSMutableArray*> *pointsSelections;
}
@property (weak, nonatomic) IBOutlet UIView *vPromotionCode;
@property (weak, nonatomic) IBOutlet UIView *vCredit;
@property (weak, nonatomic) IBOutlet UIView *vRewardPoints;
@property (weak, nonatomic) IBOutlet UIView *vAmountDue;
@property (weak, nonatomic) IBOutlet UIView *vSubTotal;

@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;
@property (weak, nonatomic) IBOutlet UILabel *labPromoDiscount;
@property (weak, nonatomic) IBOutlet UIButton *btnPromoApply;

@property (weak, nonatomic) IBOutlet UILabel *labSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *labSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *labSubtotalLine;

@property (weak, nonatomic) IBOutlet UICheckbox *cbCredit;
@property (weak, nonatomic) IBOutlet UILabel *labCreditAvaliable;
@property (weak, nonatomic) IBOutlet UILabel *labCreditSaveUpTo;
@property (weak, nonatomic) IBOutlet UILabel *labCreditAmount;
@property (weak, nonatomic) IBOutlet UILabel *labCreditLine;

@property (weak, nonatomic) IBOutlet UICheckbox *cbReward;
@property (weak, nonatomic) IBOutlet UILabel *labRewardPoints;
@property (weak, nonatomic) IBOutlet UILabel *labRewardAmount;
@property (weak, nonatomic) IBOutlet UILabel *labRewardLine;

@property (weak, nonatomic) IBOutlet UILabel *labDueAmount;
@property (weak, nonatomic) IBOutlet UILabel *labDue;


@property (nonatomic,strong ) UITapGestureRecognizer *gestureCredit;
@property (nonatomic,strong ) UITapGestureRecognizer *gestureReward;


@property (nonatomic,assign) double    totalAmountPurchased;
@property (nonatomic,strong) ModelGiftCard *giftcard;
@property (nonatomic,strong) NSArray<ModelTransaction*> *availableCredits;
@property (nonatomic,strong) NSArray<ModelTransaction*> *availableCreditsForInput;

@property (nonatomic,readonly) double enteredPointsValue;
@property (nonatomic,readonly) double enteredCreditValue;

@property (weak, nonatomic) IBOutlet UIView *vDiscount;
@property (weak, nonatomic) IBOutlet UILabelExt *labDiscountDescription;
@property (weak, nonatomic) IBOutlet UILabelExt *labDiscountAmount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintDiscountViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPromoViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSubTotalViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCreditViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPointsViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAmountDueViewHeight;
@property (nonatomic)  BOOL DiscountViewHide;
@property (nonatomic)  BOOL CreditViewHide;
@property (nonatomic)  BOOL PointsViewHide;
@property (nonatomic,assign) double discountAmount;
@property (nonatomic,assign) double swychAmount;
@property (nonatomic,strong) NSString *discountDesc;

@property (nonatomic,assign) double orgDiscountAmount;
@property (nonatomic,strong) NSString *orgDiscountDesc;

-(IBAction)buttonTapped:(id)sender;

@end

@implementation OrderCheckoutDetailView
@synthesize disableCredits = _disableCredits;
-(void)updateConstraints{
    if(!self.availableCredits || self.availableCredits.count == 0){
             _CreditViewHide = YES;
    }
    else{
             _CreditViewHide = NO;
    }
    [super updateConstraints];
    CGFloat frameHeight = self.frame.size.height ;
    self.constraintDiscountViewHeight.constant = 0.1*frameHeight;
    self.constraintPromoViewHeight.constant = 0.16*frameHeight;
    self.constraintSubTotalViewHeight.constant = 0.16*frameHeight;
    self.constraintCreditViewHeight.constant = 0.21*frameHeight;
    self.constraintPointsViewHeight.constant = 0.21*frameHeight;
    self.constraintAmountDueViewHeight.constant = 0.16*frameHeight;
    if(_DiscountViewHide) self.constraintDiscountViewHeight.constant=0;
    if(_CreditViewHide) self.constraintCreditViewHeight.constant=0;
    if(_PointsViewHide) self.constraintPointsViewHeight.constant=0;
}
-(void)setDisableCredits:(BOOL)disableCredits{
    _disableCredits = disableCredits;
    if (disableCredits){
        //self.constraintCreditViewHeight.constant = 0.0f;
        _CreditViewHide = YES;
    }
    else{
        //self.constraintCreditViewHeight.constant = CREDITS_VIEW_HEIGHT;
        _CreditViewHide = NO;
    }
}
-(void)setPromoCode:(NSString *)promoCode{
    _promoCode = promoCode;
    _txtPromoCode.text = promoCode;
    [_txtPromoCode setKeyboardType:UIKeyboardTypeDefault];
    if(_promoCode==nil || [_promoCode isEqualToString:@""]){
    self.btnPromoApply.alpha = 0.0f;
    }
    else{
    self.btnPromoApply.alpha = 1.0f;
    }
}
-(BOOL)disableCredits{
    return _disableCredits;
}

-(double)amountDue{
    return self.totalAmountPurchased - promotionDiscount - self.enteredCreditValue - self.enteredPointsValue - self.discountAmount;
}

-(double)amount{
    return self.totalAmountPurchased;
}

-(double)totalDiscount{
    return promotionDiscount + self.enteredCreditValue + self.enteredPointsValue + self.discountAmount;
}

-(double)enteredCreditValue{
    return self.cbCredit.checked ? [self getTotalSelectedCredit] : 0.0;
}

-(double)enteredPointsValue{
    return self.cbReward.checked ? [CreditPointsManager getValueByPoints:pointsEntered] : 0.0;
}

-(NSArray<ModelPaymentInfo*>*)payments{
    NSMutableArray *array = nil;
    if (self.totalAmountPurchased > 0){
        array = [[NSMutableArray alloc] initWithCapacity:1];
        double amountDue = self.totalAmountPurchased - promotionDiscount - self.enteredCreditValue - self.enteredPointsValue - self.discountAmount;
        if(amountDue>-0.001&&amountDue<0.001) amountDue = 0;
        if (amountDue > 0.0){
            ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
            mpi.paymentType = PaymentOption_Unknow;
            mpi.amount = amountDue;
            [array addObject:mpi];
        }
        
        if (promotionDiscount > 0.0){
            ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
            mpi.paymentType = PaymentOption_PromotionCode;
            mpi.paymentReferenceNumber = self.txtPromoCode.text;
            mpi.amount = promotionDiscount;
            
            [array addObject:mpi];
        }
        if (self.enteredPointsValue > 0.0){
            ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
            mpi.paymentType = PaymentOption_RewardPoints;
            mpi.paymentReferenceNumber =[NSString stringWithFormat:@"%ld",pointsEntered];
            mpi.amount = self.enteredPointsValue;
            
            [array addObject:mpi];
        }
        
        if (self.enteredCreditValue > 0.0){
            ModelPaymentInfo *mpi = [[ModelPaymentInfo alloc] init];
            mpi.paymentType = PaymentOption_SwychBalance;
            mpi.amount = self.enteredCreditValue;
            mpi.paymentReferenceNumber = [self getSelectedCreditsGiftcardNumber];
            
            [array addObject:mpi];
        }
        
    }
    return array;
}

-(void)setPurchaseInfo:(double)purchaseAmount giftcard:(ModelGiftCard*)card credits:(NSArray<ModelTransaction*>*)availableCredits{
    self.totalAmountPurchased = purchaseAmount;
    self.giftcard = card;
    self.availableCredits = availableCredits;
    
    [self calculateCreditPoints];
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"OrderCheckoutDetailView" class:[self class] owner:self options:nil];
    
    view.backgroundColor = [UIColor clearColor];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    [self setup];
    [self SetOrderTabel];
}

-(void)setup{
    promotionCode = nil;
    creditGiftcards = nil;
    pointsEntered = -1;
    
    UIColor *clr = [UIColor clearColor];
    self.backgroundColor = clr;
    self.vPromotionCode.backgroundColor = UICOLORFROMRGB(232, 234, 234, 1.0);
    self.vCredit.backgroundColor = clr;
    self.vRewardPoints.backgroundColor = clr;
    self.vAmountDue.backgroundColor = clr;
    self.vSubTotal.backgroundColor = clr;
    [self.btnPromoApply setTitleColor:TEXT_COLOR_ACTIVE];
    //[self.btnPromoApply underlineText];
    
    self.txtPromoCode.backgroundColor = clr;
    self.labPromoDiscount.backgroundColor = clr;
    
    self.txtPromoCode.placeholder = NSLocalizedString(@"Placeholder_Promotion", nil);
    [self.btnPromoApply setTitle:NSLocalizedString(@"Button_Apply", nil)];
    self.labPromoDiscount.text =ZERO_MONEY;
    self.labPromoDiscount.textColor = TEXT_COLOR_INACTIVE;
    
    self.labSubtotal.text = NSLocalizedString(@"Label_Subtotal", nil);
    self.labSubtotalAmount.text = ZERO_MONEY;
    self.labSubtotal.textColor = self.labSubtotalAmount.textColor = TEXT_COLOR_ACTIVE;
    
    self.labCreditAvaliable.text = NSLocalizedString(@"Label_CreditAvailable", nil);
    self.labCreditAvaliable.textColor = TEXT_COLOR_INACTIVE;
    self.labCreditAmount.textColor = TEXT_COLOR_INACTIVE;
    self.labCreditAmount.text = ZERO_MONEY;
    self.labCreditAmount.hidden = YES;
    self.labCreditSaveUpTo.text =[NSString stringWithFormat: NSLocalizedString(@"Label_CreditSaveUpTo", nil),@"0.00"];
    self.labCreditSaveUpTo.textColor = TEXT_COLOR_INACTIVE;
    self.cbCredit.checked = NO;
    self.cbCredit.checkboxDelegate = self;
    
    self.labRewardPoints.textColor = TEXT_COLOR_INACTIVE;
    self.labRewardPoints.text =[NSString stringWithFormat:NSLocalizedString(@"Label_Reward", nil),@"0",@"0.00"];
    self.cbReward.checked = NO;
    self.labRewardAmount.textColor = TEXT_COLOR_INACTIVE;
    self.labRewardAmount.text = ZERO_MONEY;
    self.labRewardAmount.hidden = YES;
    self.cbReward.checkboxDelegate = self;
    
    self.labDue.textColor = TEXT_COLOR_ACTIVE;
    self.labDue.text = NSLocalizedString(@"Label_Due", nil);
    self.labDueAmount.textColor = TEXT_COLOR_ACTIVE;
    self.labDueAmount.text = ZERO_MONEY;
    
    self.btnPromoApply.alpha = 0.0f;
    
    
    self.gestureCredit = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapOnView:)];
    self.gestureCredit.numberOfTapsRequired = 1;
    self.gestureCredit.numberOfTouchesRequired = 1;
    [self.gestureCredit setCancelsTouchesInView:NO];
    [self.vCredit addGestureRecognizer:self.gestureCredit];
    
    self.vCredit.userInteractionEnabled = YES;
    self.vRewardPoints.userInteractionEnabled = YES;
    
    self.gestureReward = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapOnView:)];
    self.gestureReward.numberOfTapsRequired = 1;
    self.gestureReward.numberOfTouchesRequired = 1;
    [self.gestureReward setCancelsTouchesInView:NO];
    [self.vCredit addGestureRecognizer:self.gestureReward];
    [self.vRewardPoints addGestureRecognizer:self.gestureReward];
    
    dictSelectedCredits = [[NSMutableDictionary alloc] initWithCapacity:1];
    dictSelectedCreditsTemp = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    self.labDiscountAmount.verticalAlignment = VerticalAlignment_Middle;
    self.labDiscountDescription.verticalAlignment = VerticalAlignment_Middle;
    
    self.labDiscountDescription.textAlignment = NSTextAlignmentLeft;
    self.labDiscountAmount.textAlignment = NSTextAlignmentRight;
    
    self.labDiscountDescription.textColor = TEXT_COLOR_ACTIVE;
    self.labDiscountAmount.textColor = TEXT_COLOR_ACTIVE;
    
    self.vDiscount.backgroundColor = self.vPromotionCode.backgroundColor;
    self.labDiscountAmount.backgroundColor = UICOLOR_CLEAR;
    self.labDiscountDescription.backgroundColor = UICOLOR_CLEAR;
    
    [self setDiscountAmount:0.0f description:nil];

}
-(void)SetOrderTabel{

    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    int points = (int)user.swychPoint;
    if(points<1){
        _PointsViewHide = YES;
    }
}
-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(void)setDiscountAmount:(double)amount description:(NSString*)desc{
    self.discountDesc = desc;
    self.orgDiscountDesc = desc;
    self.discountAmount = amount;
    self.orgDiscountAmount = amount;
    
    if (amount <= 0.01 || desc == nil){
        //self.constraintDiscountViewHeight.constant = 0.0f;
        _DiscountViewHide = YES;
        self.labDiscountDescription.text = desc;
        self.labDiscountAmount.text = @"";//[NSString stringWithFormat:FORMAT_MONEY_MINUS,0.0f];
    }
    else{
     //   self.constraintDiscountViewHeight.constant = DISCOUNT_VIEW_HEIGHT;
        _DiscountViewHide = NO;
        self.labDiscountDescription.text = desc;
        self.swychAmount = amount;
        self.labDiscountAmount.text = [NSString stringWithFormat:FORMAT_MONEY_MINUS,amount];
        [self calculateCreditPoints];
    }

}
-(void)updateConstraints_After_gifted{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}
-(void)gestureTapOnView:(id)sender{
    if (self.delegate != nil){
        if (sender == self.gestureCredit){
            if ([self hasRemaining] || self.enteredCreditValue > 0.0){
                [self showCustomizedInputViewForCredit];
            }
        }
        else if (sender == self.gestureReward){
            if ([self hasRemaining] || self.enteredPointsValue > 0){
                [self showCustomizedInputViewforReward];
            }
        }
    }
}

-(void)showCustomizedInputViewforReward{
    double remainingBalance = _totalAmountPurchased - promotionDiscount - creditSelected - self.discountAmount;
    remainingBalance = remainingBalance+1e-9;
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    NSInteger points = user.swychPoint > remainingBalance * 100 ? (NSInteger)(remainingBalance * 100 ) : (NSInteger)user.swychPoint;
    pointsSelections = [[NSMutableArray alloc] initWithCapacity:1];
    while(YES){
        NSInteger val = points % 10;
        points /= 10;
        NSMutableArray *array1 = [[NSMutableArray alloc] initWithCapacity:1];
        NSInteger start = 0;
        if (points == 0){
            start = 0;
        }
        else{
            if (val == 0){
                val = 9;
            }
        }
        for(NSInteger idx = start; idx <= val; idx++){
            [array1 addObject:[NSNumber numberWithInteger:idx]];
        }
        [pointsSelections insertObject:array1 atIndex:0];
        if (points == 0){
            break;
        }
    }
    CustomizedInputView *input = [CustomizedInputView  createInstanceWithViewType:CustomizedInputView_PickerView_Numbers objectWithInput:self.vRewardPoints doneButton:NSLocalizedString(@"Button_Apply", nil)];
    input.textColorOfContent = [UIColor darkGrayColor];
    input.context = CustomziedInputContext_Reward;
    input.delegate = self;
    [input setDataPickerDataSource:self];
    [input updateDataPickerView];
    [input setTitle:[NSString stringWithFormat:NSLocalizedString(@"CustomizedInputViewTitle_RewardPoints", nil),0]];
    
    [input show];
}


-(void)showCustomizedInputViewForCredit{
    double remainingBalance = _totalAmountPurchased - promotionDiscount - self.enteredPointsValue - self.discountAmount;
    
//    self.availableCreditsForInput = [self.availableCredits filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//        ModelGiftCard *card = (ModelGiftCard*)evaluatedObject;
//        return card.amount <= remainingBalance;
//    }]];
    self.availableCreditsForInput = self.availableCredits;
    CustomizedInputView *input = [CustomizedInputView  createInstanceWithViewType:CustomizedInputView_TableView_MultipleSelection objectWithInput:self.vCredit doneButton:NSLocalizedString(@"Button_Apply", nil)];
    input.context = CustomziedInputContext_Credit;
    input.textColorOfContent = [UIColor darkGrayColor];
    input.delegate = self;
    [input setTableViewDataSource:self];
    [input registerNibForCell:@"CustomizedInputViewListTableViewCell" indentifier:@"CustomizedInputViewListTableViewCell"];
    [input updateTableView];
    [input setTitle:[NSString stringWithFormat:NSLocalizedString(@"CustomizedInputViewTitle_Credit", nil),0.0]];
    
    [input show];
}


-(BOOL)hasRemaining{
    double remaining = self.totalAmountPurchased - promotionDiscount -self.enteredCreditValue - self.enteredPointsValue - self.discountAmount;
    
    return remaining > 0.01;
}
-(void)calculateCreditPoints{
    double purchaseAmount = self.totalAmountPurchased -self.swychAmount- promotionDiscount;
    purchaseAmount = purchaseAmount+ 1e-9;
//    ModelCreditPointsForPurchase *mcpfp = [[CreditPointsManager instance] getCreditPointsForPurchase:purchaseAmount credits:self.availableCredits promotionDiscount:promotionDiscount+ self.swychAmount];
    ModelCreditPointsForPurchase *mcpfp = [[CreditPointsManager instance] getCreditPointsForPurchase:purchaseAmount credits:self.availableCredits promotionDiscount:0];
    creditUpTo = mcpfp.creditUpTo;
    creditSelected = 0.0;
    pointsEntered = mcpfp.pointsUpTo;
    
    
    
    [self updateCreditPointInfoOnView];
}

-(void)updateCreditPointInfoOnView{
    if (self.vCredit != nil){
        self.labCreditSaveUpTo.text = [NSString stringWithFormat: NSLocalizedString(@"Label_CreditSaveUpTo", nil),
                                       [NSString stringWithFormat:@"%.02f",creditUpTo]];
        self.labCreditAmount.text = [NSString stringWithFormat:@"-$%.02f",creditSelected];
    }
    if (self.vRewardPoints != nil){
        self.labRewardPoints.text =[NSString stringWithFormat:NSLocalizedString(@"Label_Reward", nil),
                                    [NSString stringWithFormat:@"%ld",pointsEntered],
                                    [NSString stringWithFormat:@"%.02f",[CreditPointsManager getValueByPoints:pointsEntered]]];
        self.labRewardAmount.text = [NSString stringWithFormat:@"-$%.02f",[CreditPointsManager getValueByPoints:pointsEntered]];
    }
    self.labCreditAmount.hidden = !self.cbCredit.checked;
    self.labRewardAmount.hidden = !self.cbReward.checked;
}

-(IBAction)buttonTapped:(id)sender{
    if (sender == self.btnPromoApply){
        [self promot];
    }
}
-(void)promot{
    if ([self.btnPromoApply.titleLabel.text isEqualToString:NSLocalizedString(@"Button_Apply", nil)]){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(orderCheckoutDetailView:verifyPromotionCode:giftcardId:purchaseAmount:callback:)]){
            [self.delegate orderCheckoutDetailView:self
                               verifyPromotionCode:self.txtPromoCode.text
                                        giftcardId:self.giftcard == nil ? 0 : self.giftcard.giftCardId
                                    purchaseAmount:self.totalAmountPurchased
                                          callback:^(ResponseVerifyPromotionCode *response) {
                                              [self verifyPromotionCodeCallback:response];
                                          }];
        }
    }
    else{
        promotionDiscount = 0;
        [self recalculateOrderDetail];
        [self.btnPromoApply setTitle:NSLocalizedString(@"Button_Apply", nil)];
        self.btnPromoApply.alpha = 0.0;
        self.txtPromoCode.text = nil;
        self.labPromoDiscount.text = [NSString stringWithFormat:FORMAT_MONEY,promotionDiscount];
        self.labPromoDiscount.textColor = TEXT_COLOR_ACTIVE;
        
        
        //Show Discount View if it is needed;
        if (self.orgDiscountAmount >= 0.01){
            [self setDiscountAmount:self.orgDiscountAmount description:self.discountDesc];
        }
        
        [self calculateCreditPoints];
        [self recalculateOrderDetail];
    }
}
-(void)recalculateOrderDetail{
    double subTotal = self.totalAmountPurchased - promotionDiscount - self.discountAmount;
    self.labSubtotalAmount.text = [NSString stringWithFormat:FORMAT_MONEY,subTotal];
    double amountDue = self.totalAmountPurchased - promotionDiscount - self.enteredCreditValue - self.enteredPointsValue  - self.discountAmount;
    if(amountDue<0.001&&amountDue>-0.001) amountDue= 0;
    self.labDueAmount.text = [NSString stringWithFormat:FORMAT_MONEY,amountDue];
}

-(void)verifyPromotionCodeCallback:(ResponseVerifyPromotionCode*)response{
    ResponseVerifyPromotionCode *resp = (ResponseVerifyPromotionCode*)response;
    
    if(self.allowMultipleDiscount){
        promotionDiscount = resp.discount;
    }
    else{
        self.discountAmount = 0.0;
        self.swychAmount = 0.0;
        _DiscountViewHide = YES;
        
        
        
        if (resp.discount >= self.orgDiscountAmount){
            promotionDiscount = resp.discount;
        }
        else{
            promotionDiscount = self.orgDiscountAmount;
        }
    }
    self.labPromoDiscount.text = [NSString stringWithFormat:FORMAT_MONEY_MINUS,promotionDiscount];
    self.labPromoDiscount.textColor = TEXT_COLOR_ACTIVE;
    [self recalculateOrderDetail];
    [self.btnPromoApply setTitle:NSLocalizedString(@"Button_Remove", nil)];
    
    
    [self updateConstraints];
    
    [self calculateCreditPoints];
    
}

-(void)update{
    [self recalculateOrderDetail];
}

-(UIImage *)getCheckmarkImageForCredit{
    return [UIImage imageNamed:@"icon_checkmark.png"];
}

-(NSString*)getSelectedCreditsGiftcardNumber{
    NSString *str = nil;
    for(NSString *key in [dictSelectedCreditsTemp allKeys]){
        if (str == nil){
            str = key;
        }
        else{
            str = [NSString stringWithFormat:@"%@,%@",str,key];
        }
    }
    return str;
}

-(double)getTotalSelectedCredit{
    if ([dictSelectedCreditsTemp count] == 0){
        return 0.0;
    }
    double ret = 0.0;
    for(NSString *key in [dictSelectedCreditsTemp allKeys]){
        ret += [[dictSelectedCreditsTemp objectForKey:key] doubleValue];
    }
    return ret;
}
#pragma
#pragma mark - CustomizedInputViewDelegate
-(void)customizedInputViewDidShow:(CustomizedInputView *)inputView{
    dictSelectedCreditsTemp = [[NSMutableDictionary alloc] initWithDictionary:dictSelectedCredits];
}

-(void)customizedInputViewValueChanged:(CustomizedInputView*)inputView{
}

-(void)customizedInputViewDoneButtonTapped:(CustomizedInputView*)inputView{
    if (inputView.context == CustomziedInputContext_Credit){
        dictSelectedCredits = [[NSMutableDictionary alloc] initWithDictionary:dictSelectedCreditsTemp];
        double total = [self getTotalSelectedCredit];
        if (total < 0.01){
            creditSelected = 0.0;
            self.cbCredit.checked = NO;
        }
        else{
            creditSelected = total;
            self.cbCredit.checked = YES;
            double remaining = self.totalAmountPurchased - promotionDiscount - creditSelected - self.discountAmount;
            remaining = remaining+1e-9;
            NSInteger maxPoints = (NSInteger)(remaining * 100);
            pointsEntered = maxPoints;
        }
        [self recalculateOrderDetail];
        [self updateCreditPointInfoOnView];
    }
    else if (inputView.context == CustomziedInputContext_Reward){
        NSInteger points = [inputView getDataPickerSelectedNumber];
        if (points < 0.01){
            pointsEntered = 0.0;
            self.cbReward.checked = NO;
        }
        else{
            double remaining = self.totalAmountPurchased - promotionDiscount - creditSelected - self.discountAmount;
            remaining = remaining+1e-9;
            NSInteger maxPoints = (NSInteger)(remaining * 100);
            if (points > maxPoints){
                [Utils showAlert:[NSString stringWithFormat:NSLocalizedString(@"ExceedMaximumPoints", nil),maxPoints] delegate:nil];
                return;
            }
            pointsEntered = points;
            self.cbReward.checked = YES;
        }
        [self recalculateOrderDetail];
        [self updateCreditPointInfoOnView];
    }
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(orderCheckoutDetailView:paymentInfoChangedWithDueAmount:)]){
//        double amountDue = self.totalAmountPurchased - promotionDiscount - self.enteredPointsValue - self.enteredCreditValue;
          double amountDue =  self.totalAmountPurchased - promotionDiscount - self.enteredPointsValue - self.enteredCreditValue - self.discountAmount;
        [self.delegate orderCheckoutDetailView:self paymentInfoChangedWithDueAmount:amountDue];
    }
}

-(void)customizedInputviewDismissed:(CustomizedInputView *)inputView doneButtonTapped:(BOOL)done{
    
}

-(void)customizedInputView:(CustomizedInputView *)inputView pickerView:(UIPickerView *)pickerView valueChangedForRow:(NSInteger)row forComponent:(NSInteger)component{
    [inputView setTitle:[NSString stringWithFormat:NSLocalizedString(@"CustomizedInputViewTitle_RewardPoints",nil),[inputView getDataPickerSelectedNumber]]];
}

-(NSString*)customizedInputView:(CustomizedInputView *)inputView stringForPickerView:(UIPickerView *)pickerView forRow:(NSInteger)row forComponet:(NSInteger)component{
    NSMutableArray* contentArr = [pointsSelections objectAtIndex:component];
    if(contentArr.count  == 0) return @"0";
    NSNumber *num = [[pointsSelections objectAtIndex:component] objectAtIndex:row];
    return [NSString stringWithFormat:@"%ld",[num integerValue]];
}

-(void)customizedInputView:(CustomizedInputView*)inputView tableView:(UITableView *)tableView rowDidSelected:(NSIndexPath *)indexPath cell:(UITableViewCell *)cell{
    CustomizedInputViewListTableViewCell *c = (CustomizedInputViewListTableViewCell*)cell;
    if(c.HigherThanentered>0) return;
    ModelTransaction *tran = [self.availableCredits objectAtIndex:indexPath.row];
    ModelGiftCard *card = tran.giftCard;
    double total = 0.0;
    if ([dictSelectedCreditsTemp objectForKey:card.giftcardTransactionIdString] == nil){
        total = [self getTotalSelectedCredit];
        double remaining = self.totalAmountPurchased - promotionDiscount - self.discountAmount;
        if (total + card.amount > remaining){
            NSString *err = [NSString stringWithFormat:NSLocalizedString(@"ExceedMaximumCredit", nil),remaining];
            [Utils showAlert:err delegate:self];
            return;
        }
    }
    if (c.ivRight.image == nil){
        [dictSelectedCreditsTemp setObject:[NSNumber numberWithDouble:card.amount] forKey:card.giftcardTransactionIdString];
        c.ivRight.image = [self getCheckmarkImageForCredit];
    }
    else{
        c.ivRight.image = nil;
        [dictSelectedCreditsTemp removeObjectForKey:card.giftcardTransactionIdString];
    }
    total = [self getTotalSelectedCredit];
    [inputView setTitle:[NSString stringWithFormat:NSLocalizedString(@"CustomizedInputViewTitle_Credit", nil),total]];
}

#pragma
#pragma mark - UITextFiledDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length] == 1 && [string length] == 0 && range.location == 0){
        self.btnPromoApply.alpha = 0.0f;
    }
    else{
        self.btnPromoApply.alpha = 1.0f;
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if([textField.text isEqualToString:@""])
    return;
    if(textField == self.txtPromoCode&&[self.btnPromoApply.titleLabel.text isEqualToString:NSLocalizedString(@"Button_Apply", nil)]){
        [self promot];
    }
}
#pragma 
#pragma mask - UICheckboxDelegate
-(BOOL)checkbox:(UICheckbox *)sender stateWillChange:(BOOL)state{
    if (state == NO){
        return YES;
    }
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if(sender == self.cbCredit){
        if (user.swychBalance > 0.0){
            if (state == YES){
                if ([self hasRemaining]){
                    [self showCustomizedInputViewForCredit];
                    if ([self getTotalSelectedCredit] < 0.01){
                        return NO;
                    }
                    else{
                        return YES;
                    }
                }
                else{
                    return NO;
                }
            }
            return YES;
        }
        return NO;
    }
    else if (sender == self.cbReward){
        if(user.swychPoint > 0.0){
            if (state == YES){
                if ([self hasRemaining]){
                    return YES;
                }
                else{
                    return NO;
                }
            }
            return YES;
        }
        return NO;
    }
    return YES;
}

-(void)checkbox:(UICheckbox *)sender stateChanged:(BOOL)state{
    if (sender == self.cbCredit){
        if (sender.checked){
            self.labCreditAvaliable.textColor = TEXT_COLOR_ACTIVE;
            self.labCreditAmount.textColor = TEXT_COLOR_ACTIVE;
            self.labCreditSaveUpTo.textColor = TEXT_COLOR_ACTIVE;
            self.labCreditAmount.hidden = NO;
        }
        else{
            self.labCreditAvaliable.textColor = TEXT_COLOR_INACTIVE;
            self.labCreditAmount.textColor = TEXT_COLOR_ACTIVE;
            self.labCreditSaveUpTo.textColor = TEXT_COLOR_INACTIVE;
            self.labCreditAmount.hidden = YES;
            [dictSelectedCreditsTemp removeAllObjects];
            [dictSelectedCredits removeAllObjects];
            creditSelected = 0.0;
        }
        
        [self recalculateOrderDetail];
        [self updateCreditPointInfoOnView];
    }
    else if (sender == self.cbReward){
        if (sender.checked){
            self.labRewardPoints.textColor = TEXT_COLOR_ACTIVE;
            self.labRewardAmount.textColor = TEXT_COLOR_ACTIVE;
            self.labRewardAmount.hidden = NO;
        }
        else{
            self.labRewardAmount.textColor = TEXT_COLOR_ACTIVE;
            self.labRewardPoints.textColor = TEXT_COLOR_INACTIVE;
            self.labRewardAmount.hidden = YES;
        }
        [self recalculateOrderDetail];
        
    }
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(orderCheckoutDetailView:paymentInfoChangedWithDueAmount:)]){
        double amountDue = self.totalAmountPurchased - promotionDiscount - self.enteredPointsValue - self.enteredCreditValue - self.discountAmount;
        [self.delegate orderCheckoutDetailView:self paymentInfoChangedWithDueAmount:amountDue];
    }
}
#pragma 
#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return [pointsSelections count];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[pointsSelections objectAtIndex:component] count];
}


#pragma 
#pragma  mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.availableCreditsForInput count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"CustomizedInputViewListTableViewCell";

    double remainingBalance = _totalAmountPurchased - promotionDiscount - self.enteredPointsValue - self.discountAmount;
    CustomizedInputViewListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil){
        cell = [[CustomizedInputViewListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ModelGiftCard *card = [self.availableCreditsForInput objectAtIndex:indexPath.row].giftCard;
    
    cell.labLeft.text = card.retailer;
    cell.labRight.text = [NSString stringWithFormat:FORMAT_MONEY,card.amount];
    cell.ivRight.image = [dictSelectedCreditsTemp objectForKey:card.giftcardTransactionIdString] != nil ? [self getCheckmarkImageForCredit] : nil;
    [cell.ivLeft sd_setImageWithURL:[NSURL URLWithString:card.giftCardImageURL[0]]];
    cell.HigherThanentered = card.amount - remainingBalance;
    if(cell.HigherThanentered>0){
        
        cell.labLeft.textColor = [UIColor lightGrayColor];
        cell.labRight.textColor = [UIColor lightGrayColor];
    }
    else{
        cell.labLeft.textColor = [UIColor darkGrayColor];
        cell.labRight.textColor = [UIColor darkGrayColor];
    }
    return cell;
}

@end
