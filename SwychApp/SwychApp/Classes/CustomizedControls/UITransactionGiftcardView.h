//
//  UITransactionGiftcardView.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-04.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "ModelTransaction.h"
@protocol UITransactionGiftcardViewDelegate;
#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface UITransactionGiftcardView : UIView
@property (nonatomic,assign) IBInspectable BOOL isFavorite;
@property (nonatomic,weak) IBInspectable UIImage *giftcardImage;
@property (nonatomic,weak) IBInspectable NSString *giftcardName;
@property (nonatomic,strong) ModelTransaction *giftcard;
@property (nonatomic,weak) IBInspectable id<UITransactionGiftcardViewDelegate> giftcardViewDelegate;
@end
@protocol UITransactionGiftcardViewDelegate <NSObject>

@optional
-(void)giftcardViewTapped:(UITransactionGiftcardView*)giftcardView;

@end