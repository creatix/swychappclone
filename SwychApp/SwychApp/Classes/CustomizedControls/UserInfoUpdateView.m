//
//  UserInfoUpdateView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UserInfoUpdateView.h"
#import "UIRoundCornerButton.h"
#import "UILabelExt.h"
#import "LocalUserInfo.h"
#import "AppDelegate.h"
#import "SwychBaseViewController.h"
#import "NSString+Extra.h"

#import "RequestUserUpdate.h"
#import "ResponseUserUpdate.h"


@interface UserInfoUpdateView()
@property (weak, nonatomic) IBOutlet UIView *vName;
@property (weak, nonatomic) IBOutlet UIView *vFirstName;
@property (weak, nonatomic) IBOutlet UILabelExt *labFirst;
@property (weak, nonatomic) IBOutlet UITextField *txtFirst;
@property (weak, nonatomic) IBOutlet UIView *vLastName;
@property (weak, nonatomic) IBOutlet UILabelExt *labLast;
@property (weak, nonatomic) IBOutlet UITextField *txtLast;

@property (weak, nonatomic) IBOutlet UIView *vEmail;
@property (weak, nonatomic) IBOutlet UILabelExt *labEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UIView *vPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabelExt *labMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;

@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSave;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopSpaceOfSaveButton;

@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;

- (IBAction)buttonTapped:(id)sender;



@end
@implementation UserInfoUpdateView
@synthesize buttonText = _buttonText;
@synthesize backgroundImage = _backgroundImage;
@synthesize user = _user;


-(void)setUser:(ModelUser *)user{
    _user = user;
    if (user != nil){
        self.txtFirst.text = user.firstName;
        self.txtLast.text = user.lastName;
        self.txtMobile.text = user.phoneNumber;
        self.txtEmail.text = user.email;
    }
}
-(ModelUser*)user{
    return _user;
}

-(void)setBackgroundImage:(UIImage *)backgroundImage{
    _backgroundImage = backgroundImage;
    self.ivBackground.image = backgroundImage;
}
-(UIImage *)backgroundImage{
    return _backgroundImage;
}

-(void)setButtonText:(NSString *)buttonText{
    _buttonText = buttonText;
    [self.btnSave setTitle:buttonText];
}
-(NSString*)buttonText{
    return _buttonText;
}


-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UserInfoUpdateView" class:[self class] owner:self options:nil];
    
    view.backgroundColor = [UIColor clearColor];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.labFirst.verticalAlignment = VerticalAlignment_Bottom;
    self.labLast.verticalAlignment = VerticalAlignment_Bottom;
    self.labEmail.verticalAlignment = VerticalAlignment_Bottom;
    self.labMobile.verticalAlignment = VerticalAlignment_Bottom;
    
    LocalUserInfo *ui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    
    self.txtFirst.text = ui.firstName;
    self.txtLast.text = ui.lastName;
    self.txtEmail.text = ui.email;
    self.txtMobile.text = ui.phoneNumber;
    _labFirst.text = NSLocalizedString(@"capFirstName", nil);
    _labLast.text = NSLocalizedString(@"capLastName", nil);
    [self addSubview:view];
    
}

- (IBAction)buttonTapped:(id)sender {
    if (sender == self.btnSave){
        if ([self validateData]){
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(userInfoUpdateViewSaveButtonTapped:withUserInfo:)]){
                self.user.firstName = [self.txtFirst.text trim];
                self.user.lastName = [self.txtLast.text trim];
                self.user.email = [self.txtEmail.text trim];
                self.user.phoneNumber = [self.txtMobile.text trim];
                
                [self.delegate userInfoUpdateViewSaveButtonTapped:self withUserInfo:self.user];
            }
            else{
                [self sendUpdateInfoRequest];
            }
        }
    }
}

-(BOOL)validateData{
    if ([self.txtLast.text trim].length > 0 &&
        [self.txtFirst.text trim].length > 0 &&
        [self.txtEmail.text trim].length > 0){
        return YES;
    }
    else{
        [Utils showAlert:NSLocalizedString(@"Error_InfoMissing", nil) delegate:nil];
        return NO;
    }
}

#pragma 
#pragma mark - Server communication
-(void)sendUpdateInfoRequest{
    RequestUserUpdate *req = [[RequestUserUpdate alloc] init];
    if ([self.txtFirst.text trim].length > 0){
        req.firstName = [self.txtFirst.text trim];
    }
    if ([self.txtLast.text trim].length > 0){
        req.lastName = [self.txtLast.text trim];
    }
    if ([self.txtEmail.text trim].length > 0){
        req.email = [self.txtEmail.text trim];
    }
//    if (self.txtMobile.text.length > 0){
//        req.phoneNumber = self.txtMobile.text;
//    }
    
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    [ctrl showLoadingView:nil];
    
    SEND_REQUEST(req, handleServerResponse, handleServerResponse)
    
}

-(void)handleServerResponse:(ResponseBase*)response{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    [ctrl dismissLoadingView];
    
    if ([ctrl handleServerResponse:response]){
        return;
    }
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(userInfoUpdateView:infoUpdated:)]){
        ResponseUserUpdate * resp = (ResponseUserUpdate*)response;
        [self.delegate userInfoUpdateView:self infoUpdated:resp.user];
    }
}
@end
