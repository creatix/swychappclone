//
//  HistorySelectionView.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGiftcardView.h"
#import "CustomizedActionSheet.h"
#import "UITransactionGiftcardView.h"
#import "SearchFilterBar.h"
@protocol HistoryGiftcardSelectionViewDelegate;
IB_DESIGNABLE
@interface HistorySelectionView : UIView<UISearchBarDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITransactionGiftcardViewDelegate,UITableViewDataSource,UITableViewDelegate,CustomizedActionSheetDelegate,SearchFilterBarDelegate>{
 //   NSMutableArray *filteredGiftcards;
    UIActivityIndicatorView * indicatorFooter;
    UIActivityIndicatorView * indicatorFooterForCollectionview;
    CGPoint start;
    CGPoint end;
}

@property (strong,nonatomic) NSArray *transactions;

@property (weak,nonatomic) id<HistoryGiftcardSelectionViewDelegate> selectionViewDelegate;

@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet SearchFilterBar *vHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *cvGiftcards;
@property (weak, nonatomic) IBOutlet UITableView *tvGiftcards;

-(void)refreshData;
-(void)setupInfos;
-(void)switchGiftcardListView;
-(void)showSortingSheet;
-(void)showFilterSheet;
-(void)doSearch:(NSString*)criteria;
-(void)giftcardSelected:(ModelTransaction*)card;
-(void)RemoveIndicatorFooter;
@end
@protocol HistoryGiftcardSelectionViewDelegate <NSObject>

@optional
-(void)HistorySelectionView:(HistorySelectionView*)selectionView giftcardSelected:(ModelTransaction*)giftcard;
-(void)HistorySelectionView:(HistorySelectionView*)selectionView loadMore:(ModelTransaction*)lastObject;
@end
