//
//  GiftAttachmentView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftAttachmentView.h"
#import "UIRoundCornerImageView.h"
#import "AppDelegate.h"
#import "SwychBaseViewController.h"
#import "ModelTransaction.h"
#import "ButtonWithTextAtBottom.h"
#import "DisplayPicturePopupView.h"

#import "ResponseSayThanks.h"
#import "RequestSayThanks.h"

@interface GiftAttachmentView()
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;
@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (weak, nonatomic) IBOutlet UILabel *labSenderName;
@property (weak, nonatomic) IBOutlet UIButton *btnSayThanks;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrainVideoButtonViewHeight;
@property (weak, nonatomic) IBOutlet UIView *vVideoPictureButtons;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *cvSayThanks;
@property (weak, nonatomic) IBOutlet ButtonWithTextAtBottom *btnDisplayPicture;

- (IBAction)buttonTapped:(id)sender;
@end

@implementation GiftAttachmentView

@synthesize avatarImage = _avatarImage;
@synthesize message = _message;
@synthesize senderName = _senderName;
@synthesize videoButtonViewHeight = _videoButtonViewHeight;

-(void)setVideoButtonViewHeight:(CGFloat)videoButtonViewHeight{
    _videoButtonViewHeight = videoButtonViewHeight;
    self.constrainVideoButtonViewHeight.constant = _videoButtonViewHeight;
    if(videoButtonViewHeight == 0){
        _btnDisplayPicture.hidden = YES;
    }
}

-(CGFloat)videoButtonViewHeight{
    return _videoButtonViewHeight;
}

-(UIImage*)avatarImage{
    return _avatarImage;
}
-(void)setAvatarImage:(UIImage *)avatarImage{
    _avatarImage = avatarImage;
    if(_avatarImage){
    self.ivAvatar.image = avatarImage;
    }
    else{
        self.ivAvatar.hidden = YES;
    }
}

-(NSString*)message{
    return _message;
}
-(void)setMessage:(NSString *)message{
    _message = message;
    self.txtMessage.text = message;
}

-(NSString*)senderName{
    return _senderName;
}
-(void)setSenderName:(NSString *)senderName{
    _senderName = senderName;
    if(_senderName!=nil&&![_senderName isEqualToString:@""])
    self.labSenderName.text = [NSString stringWithFormat:@"-%@",senderName];
    else{
    self.labSenderName.text = @"";
    }
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"GiftAttachmentView" class:[self class] owner:self options:nil];
    
    view.backgroundColor = [UIColor clearColor];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    [self setup];
}

-(void)setup{
    self.vBottom.backgroundColor = [UIColor clearColor];
    self.txtMessage.backgroundColor = [UIColor clearColor];
    
    self.cvSayThanks.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    self.btnDisplayPicture.itemDelegate = (id<ButtonWithTextAtBottomDelegate>)self;
    [self.cvSayThanks SetColorOfImg:UICOLORFROMRGB(192, 31, 109,1.0f)];
}

- (IBAction)buttonTapped:(id)sender{
    if (sender == self.btnPlayVideo){
        
    }
    else if (sender == self.btnDisplayPicture){
        DisplayPicturePopupView *v = (DisplayPicturePopupView*)[Utils loadViewFromNib:@"DisplayPicturePopupView" viewTag:1];
        v.imageURL = self.imageUrl;
        
        [v show];
        return;
    }
}
#pragma 
#pragma mark -ButtonWithTextAtBottomDelegate
-(void)buttonWithTextAtBottonItemTapped:(ButtonWithTextAtBottom*)buttonItem{
    if (buttonItem == self.cvSayThanks){
        if(self.transaction != nil){
            [self sendSayThanksRequest];
        }
        else{
            if(self.delegate!=nil&&[self.delegate respondsToSelector:@selector(giftAttachmentViewSayThanksButtonTapped:)]){
                [self.delegate giftAttachmentViewSayThanksButtonTapped:self];
            }
        }
    }
    if (buttonItem == self.btnDisplayPicture){
        DisplayPicturePopupView *v = (DisplayPicturePopupView*)[Utils loadViewFromNib:@"DisplayPicturePopupView" viewTag:1];
        v.imageURL = self.imageUrl;
        
        [v show];
        return;
    }
}


#pragma 
#pragma mark - Server communication
-(void)sendSayThanksRequest{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    [ctrl showLoadingView:nil];
    RequestSayThanks *req = [[RequestSayThanks alloc] init];
    req.transactionId = self.transaction.transactionId;
    SEND_REQUEST(req, handleSayThanksResponse, handleSayThanksResponse);
}

-(void)handleSayThanksResponse:(ResponseBase*)response{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SwychBaseViewController *ctrl = [app getCurrentViewController];
    [ctrl dismissLoadingView];
    
    if ([ctrl handleServerResponse:response]){
        return;
    }
    [ctrl showAlert:response.errorMessage];
}
@end
