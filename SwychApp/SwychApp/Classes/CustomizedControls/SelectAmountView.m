//
//  SelectAmountView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SelectAmountView.h"
#import "UIRoundCornerButton.h"
#import "NSString+Extra.h"
#import "ModelGiftCard.h"
#define DefaultTitleTextColor       UICOLORFROMRGB(76,77,77,1.0f)

#define DefaultTitleTextFont        [UIFont fontWithName:@"Helvetica Neue" size:14.0f]

#define DefaultButtonBackgroundColorNormal      UICOLORFROMRGB(255,255,255,1.0f)
#define DefaultButtonBackgroundcolorSelected    UICOLORFROMRGB(192, 31, 109,1.0f)

#define DefaultButtonTextColorNormal        UICOLORFROMRGB(76,77,77,1.0f)
#define DefaultButtonTextColorSelected      UICOLORFROMRGB(255,255,255,1.0f)

#define DefaultButtonBorderColorNormal      UICOLORFROMRGB(215,215,215,1.0f)
#define DefaultbuttonBorderColorSelected    UICOLORFROMRGB(192, 31, 109,1.0f)


@interface SelectAmountView()
@property (weak, nonatomic) IBOutlet UILabel *labSelectAmount;
@property (weak, nonatomic) IBOutlet UIView *vAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount1;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount2;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount3;
@property (weak, nonatomic) IBOutlet UIView *vAmount4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vAmount4Width;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount4;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount4;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount41;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmountWidth;// the last button
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount2_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount3_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount1x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount2x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount3x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount4x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAmount5x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *framewidth;
@property (nonatomic)  BOOL ifEnableTxtInput;
@property (nonatomic)  BOOL ifUpdatedConstraint;
@property (nonatomic)  int seperateWidth;
-(IBAction)buttonTapped:(id)sender;
-(void)selectAmountButton:(NSInteger)btnIndex;
@end


@implementation SelectAmountView
@synthesize titleTextColor = _titleTextColor;

@synthesize titleTextFont = _titleTextFont;

@synthesize buttonBackgroundColorNormal = _buttonBackgroundColorNormal;
@synthesize buttonBackgroundColorSelected = _buttonBackgroundColorSelected;

@synthesize buttonTextColorNormal = _buttonTextColorNormal;
@synthesize buttonTextcolorSelected = _buttonTextcolorSelected;

@synthesize buttonBorderColorNormal = _buttonBorderColorNormal;
@synthesize buttonBorderColorSelected = _buttonBorderColorSelected;
-(void)updateConstraints{
    [super updateConstraints];
    if(!_ifUpdatedConstraint) return;
   // if(_vAmount4Width.constant == 0){
    [self autoArrangeBoxWithConstrints:buttonConstrintsArray width:50];
  //  }

}
-(void)autoArrangeBoxWithConstrints:(NSArray*)conarr width:(CGFloat)width{
    CGFloat step = (self.vAmount.frame.size.width -(width*conarr.count))/(conarr.count+1);
    for(int  i=0;i<conarr.count;i++){
        NSLayoutConstraint *constranit = conarr[i];
        constranit.constant = step;//_seperateWidth;
    }
}
-(UIColor*)buttonBorderColorSelected{
    if(_buttonBorderColorSelected == nil){
        _buttonBorderColorSelected = DefaultbuttonBorderColorSelected;
    }
    return _buttonBorderColorSelected;
}
-(void)setButtonBorderColorSelected:(UIColor *)buttonBorderColorSelected{
    _buttonBorderColorSelected = buttonBorderColorSelected;
    if ([buttonArray count] > 0 && selectedAmountIndex != -1){
        UIRoundCornerButton *btn = [buttonArray objectAtIndex:selectedAmountIndex];
        btn.borderColor = buttonBorderColorSelected;
    }
}

-(UIColor*)buttonBorderColorNormal{
    if (_buttonBorderColorNormal == nil){
        _buttonBorderColorNormal = DefaultButtonBorderColorNormal;
    }
    return _buttonBorderColorNormal;
}
-(void)setButtonBorderColorNormal:(UIColor *)buttonBorderColorNormal{
    _buttonBorderColorNormal = buttonBorderColorNormal;
    if ([buttonArray count] > 0){
        for(UIRoundCornerButton *btn in buttonArray){
            btn.borderColor = buttonBorderColorNormal;
        }
    }
}

-(UIColor*)buttonTextcolorSelected{
    if(_buttonTextcolorSelected == nil){
        _buttonTextcolorSelected = DefaultButtonTextColorSelected;
    }
    return _buttonTextcolorSelected;
}
-(void)setButtonTextcolorSelected:(UIColor *)buttonTextcolorSelected{
    _buttonTextcolorSelected = buttonTextcolorSelected;
    if ([buttonArray count] > 0 && selectedAmountIndex != -1){
        UIRoundCornerButton *btn = [buttonArray objectAtIndex:selectedAmountIndex];
        [btn setTitleColor:buttonTextcolorSelected];
    }
}


-(UIColor*)buttonTextColorNormal{
    if (_buttonTextColorNormal == nil){
        _buttonTextColorNormal = DefaultButtonTextColorNormal;
    }
    return _buttonTextColorNormal;
}
-(void)setButtonTextColorNormal:(UIColor *)buttonTextColorNormal{
    _buttonTextColorNormal = buttonTextColorNormal;
    if ([buttonArray count] > 0){
        for(UIRoundCornerButton *btn in buttonArray){
            [btn setTitleColor:buttonTextColorNormal];
        }
    }
}

-(UIColor*)buttonBackgroundColorSelected{
    if (_buttonBackgroundColorSelected == nil){
        _buttonBackgroundColorSelected = DefaultButtonBackgroundcolorSelected;
    }
    return _buttonBackgroundColorSelected;
}
-(void)setButtonBackgroundColorSelected:(UIColor *)buttonBackgroundColorSelected{
    _buttonBackgroundColorSelected = buttonBackgroundColorSelected;
    if ([buttonArray count] > 0 && selectedAmountIndex != -1){
        UIRoundCornerButton *btn = [buttonArray objectAtIndex:selectedAmountIndex];
        btn.backgroundColor = buttonBackgroundColorSelected;
    }
}

-(UIColor *)buttonBackgroundColorNormal{
    if (_buttonBackgroundColorNormal == nil){
        _buttonBackgroundColorNormal = DefaultButtonBackgroundColorNormal;
    }
    return _buttonBackgroundColorNormal;
}
-(void)setButtonBackgroundColorNormal:(UIColor *)buttonBackgroundColorNormal{
    _buttonBackgroundColorNormal = buttonBackgroundColorNormal;
    if ([buttonArray count] > 0){
        for(UIRoundCornerButton *btn in buttonArray){
            btn.backgroundColor = buttonBackgroundColorNormal;
        }
    }
}

-(UIColor *)titleTextColor{
    if (_titleTextColor == nil){
        _titleTextColor = DefaultTitleTextColor;
    }
    return _titleTextColor;
}
-(void)setTitleTextColor:(UIColor *)titleTextColor{
    _titleTextColor = titleTextColor;
    if (self.labSelectAmount != nil){
        self.labSelectAmount.textColor = titleTextColor;
    }
}


-(UIFont*)titleTextFont{
    if (_titleTextFont == nil){
        _titleTextFont = DefaultTitleTextFont;
    }
    return _titleTextFont;
}
-(void)setTitleTextFont:(UIFont *)titleTextFont{
    _titleTextFont = titleTextFont;
    if (self.labSelectAmount != nil){
        self.labSelectAmount.font = titleTextFont;
    }
}



-(double)selectedAmount{
    if (selectedAmountIndex == -1){
        return 0.0;
    }
    if(buttonArray.count == 0) return 0.0;
    if(selectedAmountIndex<(buttonArray.count-1)){
        UIButton *btn = (UIButton*)[buttonArray objectAtIndex:selectedAmountIndex];
        return [[btn.titleLabel.text substringFromIndex:1] doubleValue];
    }
    else{
       return [self.txtAmount4.text length] == 0 ? 0 : [[self.txtAmount4.text substringFromIndex:1] doubleValue];
    }
    return 0.0;
//    if ([self.amountArray count] == 4){
//        if (selectedAmountIndex < 3){
//            return [[self.amountArray objectAtIndex:selectedAmountIndex] intValue];
//        }
//        else{
//            return [self.txtAmount4.text length] == 0 ? 0 : [[self.txtAmount4.text substringFromIndex:1] intValue];
//        }
//    }
//    else{
//        if (selectedAmountIndex < 3){
//            UIButton *btn = (UIButton*)[buttonArray objectAtIndex:selectedAmountIndex];
//            return [[btn.titleLabel.text substringFromIndex:1] intValue];
//        }
//        else{
//            return [self.txtAmount4.text length] == 0 ? 0 : [[self.txtAmount4.text substringFromIndex:1] intValue];
//        }
//    }
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"SelectAmountView" class:[self class] owner:self options:nil];
    
    UIColor *clr = [UIColor clearColor];
    self.backgroundColor = clr;
    self.labSelectAmount.backgroundColor = clr;
    self.labSelectAmount.textColor = self.titleTextColor;
    self.labSelectAmount.font = self.titleTextFont;
    
    self.vAmount.backgroundColor = clr;
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];

    selectedAmountIndex = -1;
    self.txtAmount4.delegate = self;

  
    self.minimumAmount = 0;
    self.maximumAmount = NSIntegerMax;
}
-(void)setGiftcard:(ModelGiftCard *)giftcard{
    _seperateWidth = 25;
    _giftcard = giftcard;
    int status = -1;
    //
        if (giftcard.availableValues != nil && giftcard.availableValues.count > 0){
            if(giftcard.availableValues.count>4){
            status = 1; // show other
            }
            else{
                if(giftcard.minimumValue==0&&giftcard.maximumValue == 0){
                status = 2;//hide other, show denamnation
                }
                else
                status = 3;//show other
            }
        }
        else{
            status = 4;// hide other, show default
        }
 
    switch (status) {
        case 1:
            _ifEnableTxtInput = YES;
            self.txtAmount4.enabled = YES;
            [self manageButtonArray];
            break;
        case 2:
            _ifEnableTxtInput = NO;
            self.txtAmount4.enabled = NO;
            [self manageButtonArray];
            break;
        case 3:
            _ifEnableTxtInput = YES;
            self.txtAmount4.enabled = YES;
            [self manageButtonArray];
            break;
        case 4:
            _ifEnableTxtInput = YES;
            [self manageButtonByDeaultArray];
            break;
        default:
            break;
    }
       return;



}
-(void)updateConstraints_After_gifted{
    _ifUpdatedConstraint = YES;
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
}
-(void)manageButtonArray{

    if(_giftcard.availableValues.count<=4){
       self.amountArray = [NSArray arrayWithArray:_giftcard.availableValues];
    }
    else{
                NSMutableArray *mutableArray = [NSMutableArray array];
                for(NSInteger i = 0; i < 4;i++){
        
                    [mutableArray addObject:[_giftcard.availableValues objectAtIndex:i]];
        
                }
        self.amountArray = [NSArray arrayWithArray:mutableArray];
    }
    
    for(int i=0;i<self.amountArray.count;i++){
    
    }
    switch (self.amountArray.count) {
        case 1:
            _seperateWidth=80;
            self.btnAmountWidth.constant = 0;
            self.btnAmount4x.constant =0;
            self.btnAmount3x.constant =0;
            self.btnAmount3_width.constant =0;
            self.btnAmount2x.constant =0;
            self.btnAmount2_width.constant =0;
            buttonConstrintsArray =  [NSArray arrayWithObjects:self.btnAmount1x,self.btnAmount5x, nil];
            buttonArray = [NSArray arrayWithObjects:self.btnAmount1,self.btnAmount4, nil];
            break;
        case 2:
            _seperateWidth=40;
            self.btnAmountWidth.constant = 0;
            self.btnAmount4x.constant =0;
            self.btnAmount3x.constant =0;
            self.btnAmount3_width.constant =0;
            buttonConstrintsArray =  [NSArray arrayWithObjects:self.btnAmount1x,self.btnAmount2x,self.btnAmount5x, nil];
            buttonArray = [NSArray arrayWithObjects:self.btnAmount1,self.btnAmount2,self.btnAmount4, nil];
            break;
        case 3:
            self.btnAmountWidth.constant = 0;
            self.btnAmount4x.constant =0;
            buttonConstrintsArray =  [NSArray arrayWithObjects:self.btnAmount1x,self.btnAmount2x,self.btnAmount3x,self.btnAmount5x, nil];
            buttonArray = [NSArray arrayWithObjects:self.btnAmount1,self.btnAmount2,self.btnAmount3,self.btnAmount4, nil];
            break;
            
        case 4:
            _seperateWidth=10;
            buttonConstrintsArray =  [NSArray arrayWithObjects:self.btnAmount1x,self.btnAmount2x,self.btnAmount3x,self.btnAmount4x,self.btnAmount5x, nil];
            buttonArray = [NSArray arrayWithObjects:self.btnAmount1,self.btnAmount2,self.btnAmount3,self.btnAmount41,self.btnAmount4, nil];            break;
            
        default:
            break;
    }
    for(UIRoundCornerButton *btn in buttonArray){
        btn.backgroundColor = self.buttonBackgroundColorNormal;
        btn.borderColor = self.buttonBorderColorNormal;
        [btn setTitleColor:self.buttonTextColorNormal];
        if (self.amountArray != nil &&btn.tag < [self.amountArray count]){
            [btn setTitle:[NSString stringWithFormat:@"$%@",[self.amountArray objectAtIndex:btn.tag]]];
        }
    }
    if(!_ifEnableTxtInput)
    _vAmount4Width.constant = 0;
}
-(void)manageButtonByDeaultArray{
    self.btnAmountWidth.constant = 0;
    self.btnAmount4x.constant =0;
    buttonConstrintsArray =  [NSArray arrayWithObjects:self.btnAmount1x,self.btnAmount2x,self.btnAmount3x,self.btnAmount5x, nil];
    buttonArray = [NSArray arrayWithObjects:self.btnAmount1,self.btnAmount2,self.btnAmount3,self.btnAmount4, nil];
    if (self.amountArray == nil){
        self.amountArray = [NSArray arrayWithObjects:@"10",@"20",@"50", nil];
    }
    for(UIRoundCornerButton *btn in buttonArray){
        btn.backgroundColor = self.buttonBackgroundColorNormal;
        btn.borderColor = self.buttonBorderColorNormal;
        [btn setTitleColor:self.buttonTextColorNormal];
        if (self.amountArray != nil &&btn.tag < [self.amountArray count]){
            [btn setTitle:[NSString stringWithFormat:@"$%@",[self.amountArray objectAtIndex:btn.tag]]];
        }
    }
}
-(void)setCurrentSelectedAmountByValue:(double)amount{
    selectedAmountIndex = -1;
    for(int i = 0; i < self.amountArray.count; i ++){
        if ([self.amountArray[i] doubleValue] == amount){
            selectedAmountIndex = i;
            break;
        }
    }
    if (selectedAmountIndex == -1){
        self.txtAmount4.text = [NSString stringWithFormat:@"$%.02f",amount];
        [self.btnAmount4 setTitle:nil];
        selectedAmountIndex = 4;
    }
    [self selectAmountButton:selectedAmountIndex];
}

-(IBAction)buttonTapped:(id)sender{

    UIRoundCornerButton *btn = (UIRoundCornerButton*)sender;
    if(sender == _btnAmount4){
        if(!_ifEnableTxtInput){
            
            return;
        }
    }
    [self selectAmountButton:btn.tag];
    if ([self.txtAmount4.text length] == 0 && selectedAmountIndex != [buttonArray count] - 1){
        [self.btnAmount4 setTitle:NSLocalizedString(@"AmountSelection_Other", nil)];
    }
}
-(void)SetDefaultButton{
    [self buttonTapped:_btnAmount1];
}
-(void)selectAmountButton:(NSInteger)btnIndex{
    if (selectedAmountIndex != -1){
        UIRoundCornerButton *btnCurrentSelected ;
        if(selectedAmountIndex == 4){
            btnCurrentSelected = [buttonArray lastObject];
        }
        else{
            btnCurrentSelected = [buttonArray objectAtIndex:selectedAmountIndex];
        }
        btnCurrentSelected.backgroundColor = self.buttonBackgroundColorNormal;
        [btnCurrentSelected setTitleColor:self.buttonTextColorNormal];
        btnCurrentSelected.borderColor = self.buttonBorderColorNormal;
        if(selectedAmountIndex == 4){
            self.txtAmount4.textColor = self.buttonTextColorNormal;
        }
    }
    selectedAmountIndex = btnIndex;
    UIRoundCornerButton *btnSelected;
    if(selectedAmountIndex == 4){
    btnSelected = [buttonArray lastObject];
    }
    else{
    btnSelected = [buttonArray objectAtIndex:selectedAmountIndex];
    }
    
    btnSelected.backgroundColor = self.buttonBackgroundColorSelected;
    [btnSelected setTitleColor:self.buttonTextcolorSelected];
    btnSelected.borderColor = self.buttonBorderColorSelected;
    if (btnSelected == self.btnAmount4){
      //  [self.txtAmount4 becomeFirstResponder];
        self.txtAmount4.textColor = self.buttonTextcolorSelected;
    }
}

#pragma 
#pragma mark - UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(!_ifEnableTxtInput) return;
    [self.btnAmount4 setTitle:nil];
    [self selectAmountButton:self.btnAmount4.tag];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (![string allNumber]){
        return NO;
    }
    if(range.location == 1 && [string length] == 0){
        self.txtAmount4.text = nil;
    }
    else{
        if (range.location == 0){
            textField.text = [NSString stringWithFormat:@"$%@",string];
            return NO;
        }
    }
    if ([textField.text length] > 1){
        NSString *str = [textField.text substringFromIndex:1];
        NSInteger amount= [str intValue];
        if (amount > self.maximumAmount){
            return NO;
        }
    }
    return  YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    return YES;
}


@end
