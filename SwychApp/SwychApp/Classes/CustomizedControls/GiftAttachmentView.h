//
//  GiftAttachmentView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GiftAttachmentViewDelegate;
@class ModelTransaction;

IB_DESIGNABLE
@interface GiftAttachmentView : UIView

@property(nonatomic,strong) IBInspectable UIImage *avatarImage;
@property(nonatomic,strong) IBInspectable NSString *message;
@property(nonatomic,strong) IBInspectable NSString *senderName;
@property(nonatomic,strong) IBInspectable NSString *imageUrl;
@property(nonatomic,strong) IBInspectable NSString *videoUrl;

@property(nonatomic,weak) id<GiftAttachmentViewDelegate> delegate;

@property(nonatomic,strong) ModelTransaction *transaction;
@property(nonatomic,assign) CGFloat videoButtonViewHeight;
@end


@protocol GiftAttachmentViewDelegate <NSObject>

@optional
-(void)giftAttachmentViewSayThanksButtonTapped:(GiftAttachmentView*)attachmentView;
-(void)giftAttachmentViewWillPlayVideoMessage:(GiftAttachmentView*)attachmentView;
-(void)giftAttachmentViewFinishedPlayVideoMessage:(GiftAttachmentView*)attachmentView;

@end