//
//  CardInfoView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"


typedef NS_ENUM(NSInteger, CardInfoViewFooterType){
    CIVFT_AmountAtRight = 1,
    CIVFT_TermOnlyAtRight,
    CIVFT_RedeemTypeAtRight
} ;

@class ModelGiftCard;
@class UIGiftcardView;
@protocol CardInfoViewDelegate;

IB_DESIGNABLE
@interface CardInfoView : UIView<TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UIGiftcardView *vGiftcard;
@property (nonatomic,strong) IBInspectable UIImage *imgBackground;
@property (nonatomic,assign) IBInspectable CardInfoViewFooterType footerType;

@property (nonatomic,strong) ModelGiftCard *giftcard;
@property (nonatomic,assign) double amount;

@property (nonatomic,weak) id<CardInfoViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labFooterGiftcardName;
-(void)updateView;

@end


@protocol CardInfoViewDelegate <NSObject>

@optional
-(void)cardInfoView:(CardInfoView*)cardInfoView termsLinkTapped:(NSString*)url;

@end
