//
//  UITransactionGiftcardView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-04.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UITransactionGiftcardView.h"
#import "ModelGiftCard.h"
#import "UIImageView+WebCache.h"
@interface UITransactionGiftcardView()
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLabeName;
@property (weak, nonatomic) IBOutlet UIImageView *ivGiftcard;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardName;
@property (weak, nonatomic) IBOutlet UIImageView *ivHeadImg;
@end
@implementation UITransactionGiftcardView
@synthesize isFavorite = _isFavorite;
@synthesize giftcard = _giftcard;
@synthesize giftcardImage = _giftcardImage;

-(ModelTransaction *)giftcard{
    return _giftcard;
}

-(void)setGiftcard:(ModelTransaction *)giftcard{
    _giftcard = giftcard;
    if (giftcard.giftCard.giftCardUrl != nil && [giftcard.giftCard.giftCardImageURL count] > 0){
        [self.ivGiftcard sd_setImageWithURL:[NSURL URLWithString:[giftcard.giftCard.giftCardImageURL objectAtIndex:0]]];
    }
    self.labGiftcardName.text = giftcard.giftCard.retailer;
}

-(UIImage *)giftcardImage{
    return _giftcardImage;
}

-(void)setGiftcardImage:(UIImage *)giftcardImage{
    _giftcardImage = giftcardImage;
    self.ivGiftcard.image = _giftcardImage;
}
-(BOOL)isFavorite{
    return _isFavorite;
}

-(void)setIsFavorite:(BOOL)isFavorite{
    _isFavorite = isFavorite;
    
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UIGiftCardView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
    
    //self.constraintHeightLabeName.constant = 0.0f;
    //self.ivFavorite.hidden = YES;
    
    self.layer.cornerRadius = 9.0f;
    self.layer.masksToBounds = YES;
    
}

-(void)touchOnView:(id)sender{
    if (self.giftcardViewDelegate != nil && [self.giftcardViewDelegate respondsToSelector:@selector(giftcardViewTapped:)]){
        [self.giftcardViewDelegate giftcardViewTapped:self];
    }
}


@end
