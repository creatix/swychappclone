//
//  TransactionDetailCommentSwychView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-07.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionDetailCommentSwychView.h"

@implementation TransactionDetailCommentSwychView
-(void)setTransaction:(ModelTransaction *)transaction{
}
-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"TransactionDetailCommentSwychView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


//-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *cellIndentifier = @"ContactListTableViewCell";
//    ContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
//    if (cell == nil){
//        cell = [[ContactListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
//    }
//    cell.backgroundColor = [UIColor clearColor];
//    cell.contact = self.contact;
//    cell.txtContent.delegate = cell;
//    
//    if (indexPath.section == 0){
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    else if (indexPath.section == 1){
//        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//        if ([self.contact.phoneNumbersAndTitle count] > 0){
//            AContactItem* item = [self.contact.phoneNumbersAndTitle objectAtIndex:indexPath.row];
//            cell.txtContent.text = item.Content_Item;
//            cell.labTitle.text = item.Title_Item;
//            cell.txtContent.keyboardType = UIKeyboardTypeNumberPad;
//            cell.ContentOfContact = item.Content_Item;
//            cell.icontactType = PHONE_CELL;
//        }
//        else{
//            //     cell.textLabel.text = [self.contact.emails objectAtIndex:indexPath.row];
//        }
//    }
//    else {
//        AContactItem* item = [self.contact.emailsAndTitle objectAtIndex:indexPath.row];
//        cell.txtContent.text = item.Content_Item;//[self.contact.emails objectAtIndex:indexPath.row];
//        cell.labTitle.text = item.Title_Item;
//        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//        cell.ContentOfContact = item.Content_Item;
//        cell.icontactType = EMAIL_CELL;
//    }
//    return cell;
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.0f;
}

@end
