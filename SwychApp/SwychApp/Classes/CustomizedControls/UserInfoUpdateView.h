//
//  UserInfoUpdateView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelUser;
@protocol UserInfoUpdateViewDelegate;

IB_DESIGNABLE
@interface UserInfoUpdateView : UIView


@property (nonatomic,assign) id<UserInfoUpdateViewDelegate> delegate;
@property (nonatomic,assign) NSInteger context;
@property (nonatomic,strong) NSObject *contextObj;

@property (nonatomic,strong) IBInspectable NSString *buttonText;
@property (nonatomic,strong) IBInspectable UIImage *backgroundImage;
@property (nonatomic,strong) ModelUser *user;
@end

@protocol UserInfoUpdateViewDelegate <NSObject>

@optional
-(void)userInfoUpdateView:(UserInfoUpdateView*)view infoUpdated:(ModelUser*)user;
-(void)userInfoUpdateViewSaveButtonTapped:(UserInfoUpdateView*)view withUserInfo:(ModelUser*)user;
@end