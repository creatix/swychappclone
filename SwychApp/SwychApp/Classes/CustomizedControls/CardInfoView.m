//
//  CardInfoView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CardInfoView.h"
#import "ModelGiftCard.h"
#import "UIImageView+WebCache.h"
#import "UIGiftcardView.h"
#import "MessageWithWebViewPopView.h"
#define REDEEM_TYPE_ONLINE_ONLY     1

#define FONT_NAME       @"Helvetica Neue"
#define FONT_SIZE       12

#define TEXT_COLOR      UICOLORFROMRGB(76, 77, 77, 1.0)

@interface CardInfoView()
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UIView *vFooter;


@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labFooterGiftcardRedeemType;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labFooterGiftcardAmount;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *labFooterTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRedeemTypeHeight;

@end

@implementation CardInfoView

@synthesize imgBackground = _imgBackground;
@synthesize giftcard = _giftcard;
@synthesize footerType = _footerType;
@synthesize amount = _amount;

-(void)setAmount:(double)amount{
    _amount = amount;
    UIColor *clr = TEXT_COLOR;
    NSString *amountStr = [NSString stringWithFormat:FORMAT_MONEY,amount];
    self.labFooterGiftcardAmount.text = STRING_FOR_TTTATTRIBUTEDLABEL(amountStr,FONT_NAME,FONT_SIZE,clr);
}

-(double)amount{
    return _amount;
}

-(void)setGiftcard:(ModelGiftCard *)giftcard{
    _giftcard = giftcard;
    if (self.vGiftcard != nil && [giftcard.giftCardImageURL count] > 0){
        self.vGiftcard.giftcard = giftcard;
    }
    UIColor *textColor = TEXT_COLOR;
    
    self.labFooterGiftcardName.text = STRING_FOR_TTTATTRIBUTEDLABEL(giftcard.retailer,
                                                                    FONT_NAME, FONT_SIZE, textColor);
    if (giftcard.redeemType == REDEEM_TYPE_ONLINE_ONLY){
        self.labFooterGiftcardRedeemType.text =STRING_FOR_TTTATTRIBUTEDLABEL(NSLocalizedString(@"RedeemType_OnlineOnly", nil),
                                                                             FONT_NAME, FONT_SIZE, textColor);
    }
    else{
        self.labFooterGiftcardRedeemType.text = nil;
        self.labFooterGiftcardRedeemType.hidden = YES;
    }
    
    if ([giftcard.termsURL length] > 0){
        NSString* link = NSLocalizedString(@"View_Terms", nil);
        self.labFooterTerms.text = STRING_FOR_TTTATTRIBUTEDLABEL(link,
                                                                 FONT_NAME, FONT_SIZE, textColor);;
        [self.labFooterTerms addLinkToURL:[NSURL URLWithString:giftcard.termsURL] withRange:NSMakeRange(0, [link length])];
        //self.labFooterTerms.linkAttributes = @{ (id)kCTForegroundColorAttributeName: TEXT_COLOR,
          //                                    (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };
    }
    else{
        self.labFooterTerms.text = nil;
    }
    BOOL containsString = [Utils CheckIfAmazon:_giftcard];//[[_giftcard.retailer lowercaseString] containsString:@"amazon"];
    if(containsString){
        self.labFooterGiftcardRedeemType.text =STRING_FOR_TTTATTRIBUTEDLABEL(NSLocalizedString(@"RedeemType_OnlineOnly", nil),
                                                                             FONT_NAME, FONT_SIZE, textColor);
        NSString* link = NSLocalizedString(@"View_Terms", nil);
        
        self.labFooterTerms.text = STRING_FOR_TTTATTRIBUTEDLABEL(link,
                                                                 FONT_NAME, FONT_SIZE, UICOLORFROMRGB(192,31,109,1.0));;
        [self.labFooterTerms addLinkToURL:[NSURL URLWithString:@"amazonterms"] withRange:NSMakeRange(0, [link length])];
    }
}
-(void)setImgBackground:(UIImage *)imgBackground{
    _imgBackground = imgBackground;
    if (self.ivBackground != nil && imgBackground != nil){
        self.ivBackground.image = imgBackground;
    }
}

-(UIImage *)imgBackground{
    return _imgBackground;
}


-(CardInfoViewFooterType)footerType{
    return _footerType;
}

-(void)setFooterType:(CardInfoViewFooterType)footerType{
    _footerType = footerType;
    switch (footerType) {
        case CIVFT_AmountAtRight:
            self.labFooterGiftcardAmount.hidden = NO;
            self.labFooterTerms.hidden = YES;
            self.labFooterGiftcardRedeemType.hidden = YES;
            break;
        case CIVFT_TermOnlyAtRight:
            self.labFooterGiftcardAmount.hidden = YES;
            self.labFooterTerms.hidden = NO;
            self.labFooterGiftcardRedeemType.hidden = YES;
            break;
        case CIVFT_RedeemTypeAtRight:
            self.labFooterGiftcardAmount.hidden = YES;
            self.labFooterTerms.hidden = NO;
            self.labFooterGiftcardRedeemType.hidden = NO;
            break;
            
        default:
            break;
    }
}

-(void)updateView{
    
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"CardInfoView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    self.labFooterGiftcardAmount.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
    self.labFooterGiftcardRedeemType.verticalAlignment = TTTAttributedLabelVerticalAlignmentBottom;
    self.labFooterTerms.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
    
    NSMutableDictionary *dictAttr = [NSMutableDictionary dictionary];
    [dictAttr setObject:(__bridge id)UICOLORFROMRGB(192,31,109,1.0).CGColor forKey:(NSString*)kCTForegroundColorAttributeName];
    self.labFooterTerms.linkAttributes = dictAttr;
    self.labFooterTerms. activeLinkAttributes = dictAttr;
    self.labFooterTerms.inactiveLinkAttributes = dictAttr;
    
    self.labFooterTerms.delegate = self;
    
    self.footerType = CIVFT_AmountAtRight;
    if (self.giftcard != nil && [self.giftcard.giftCardImageURL count] > 0){
        self.vGiftcard.giftcard = self.giftcard;
    }
    
    if (self.imgBackground == nil){
        self.ivBackground.image = [Utils createUIImageFromColor:[UIColor lightGrayColor]
                                            withSize:CGSizeMake(10.0, 10.0)
                                           withAlpha:1.0f];
    }
    else{
        self.ivBackground.image = self.imgBackground;
    }
}

#pragma 
#pragma mark - TTTAttributedLabelDelegate
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    if([url.absoluteString isEqualToString:@"amazonterms"]){
    MessageWithWebViewPopView *cppv =(MessageWithWebViewPopView *)[Utils loadViewFromNib:@"MessageWithWebViewPopView" viewTag:0];
        [cppv AddUrl:url.absoluteString title:NSLocalizedString(@"TermsConditions",nil)];
    [cppv show];
        return;
    }
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cardInfoView:termsLinkTapped:)]){
        [self.delegate cardInfoView:self termsLinkTapped:[url absoluteString]];
    }

}
@end
