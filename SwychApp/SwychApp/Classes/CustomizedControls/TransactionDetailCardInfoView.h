//
//  TransactionDetailCardInfoView.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-06.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelTransaction.h"
#import "TransactionDetailCardInfoImgView.h"
#import "UIGiftcardView.h"
@interface TransactionDetailCardInfoView : UIView
@property (weak,nonatomic) IBOutlet UIImageView *backGround;
//@property (weak,nonatomic) IBOutlet UIImageView *imgCard;
@property (weak,nonatomic) IBOutlet UILabel *lblOrderNumber;
@property (weak,nonatomic) IBOutlet UILabel *lblTo;
@property (weak,nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak,nonatomic) IBOutlet UILabel *Ammount;
@property (weak,nonatomic) IBOutlet UILabel *lblStatus;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *lblToConstraintHiegth;
@property (weak,nonatomic) IBOutlet UIGiftcardView *imgCard;
@property (strong,nonatomic)  ModelTransaction *transaction;
@end
