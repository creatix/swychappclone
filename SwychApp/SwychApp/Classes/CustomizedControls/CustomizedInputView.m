//
//  CustomizedInputView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CustomizedInputView.h"
#import "CustomizedInputViewListTableViewCell.h"

#define HeightSelectionView      180.0f

extern NSString *Noti_TouchOnView;


@interface CustomizedInputView()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSelectionView;
@property (weak, nonatomic) IBOutlet UIView *vSelection;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *dataPicker;
@property (weak, nonatomic) IBOutlet UITableView *tvList;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIImageView *topView;

-(IBAction)buttonTapped:(id)sender;
-(void)dismiss;
-(void)dismissInputView:(BOOL)doneButtonTapped;
@end

@implementation CustomizedInputView

@synthesize viewType = _viewType;

-(void)setViewType:(InputViewType)viewType{
    _topView.layer.shadowOffset = CGSizeMake(0, 0);
    _topView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    _topView.layer.shadowOpacity = 1;
    _viewType = viewType;
    switch (viewType) {
        case CustomizedInputView_Date:
        case CustomizedInputView_DateTime:
            if (self.datePicker != nil){
                if (viewType == CustomizedInputView_Date){
                    self.datePicker.datePickerMode = UIDatePickerModeDate;
                }
                else{
                    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
                }
                self.datePicker.hidden =NO;
            }
            if (self.dataPicker != nil){
                self.dataPicker.hidden = YES;
            }
            if (self.tvList != nil){
                self.tvList.hidden = YES;
            }
            break;
        case CustomizedInputView_TableView_SingleSeledtion:
        case CustomizedInputView_TableView_MultipleSelection:
            if (self.datePicker != nil){
                self.datePicker.hidden = YES;
            }
            if (self.dataPicker != nil){
                self.dataPicker.hidden = YES;
            }
            if (self.tvList != nil){
                self.tvList.hidden = NO;
            }
            break;
        case CustomizedInputView_PickerView_Numbers:
            self.datePicker.hidden = YES;
            self.tvList.hidden = YES;
            self.dataPicker.hidden = NO;
            break;
        default:
            break;
    }
    
}

-(InputViewType)viewType{
    return _viewType;
}
-(void)setTextColorOfContent:(UIColor*)colorfont{
    _labTitle.textColor = colorfont;
    [_btnDone setTitleColor: colorfont];
    [_btnDone.titleLabel setFont: [_btnDone.titleLabel.font fontWithSize: 12]];
}
+(CustomizedInputView*)createInstanceWithViewType:(InputViewType)type  objectWithInput:(UIView*)object{
    return [CustomizedInputView createInstanceWithViewType:type objectWithInput:object doneButton:nil];
}

+(CustomizedInputView*)createInstanceWithViewType:(InputViewType)type objectWithInput:(UIView*)object doneButton:(NSString*)doneText{
    UIView *v = [Utils loadViewFromNib:@"CustomizedInputView" viewTag:1];
    if ([v isKindOfClass:[CustomizedInputView class]]){
        CustomizedInputView* civ = (CustomizedInputView*)v;
        UIWindow *win = [[UIApplication sharedApplication] keyWindow];
        if (win == nil){
            return nil;
        }
        civ.backgroundColor = [UIColor clearColor];
        civ.vSelection.backgroundColor = [UIColor whiteColor];
        civ.viewType = type;
        civ.objectWithInput = object;
        civ.constraintHeightSelectionView.constant = HeightSelectionView;
        if (doneText != nil){
            [civ.btnDone setTitle:doneText];
        }
        civ.tvList.delegate = civ;
        civ.dataPicker.delegate = civ;
        return civ;
    }
    return nil;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self){
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [tap setCancelsTouchesInView:NO];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.btnDone addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setDatePickerMinDate:(NSDate*)minDate maxDate:(NSDate*)maxDate{
    if (self.viewType != CustomizedInputView_Date && self.viewType != CustomizedInputView_DateTime){
        return;
    }
    if(minDate != nil){
        self.datePicker.minimumDate = minDate;
    }
    if (maxDate != nil){
        self.datePicker.maximumDate = maxDate;
    }
}

-(void)touchOnView:(id)sender{
    [self dismiss];
}

-(void)setTitle:(NSString*)title{
    self.labTitle.text = title;
}

-(void)enableDoneButton:(BOOL)enable{
    self.btnDone.enabled = enable;
}

-(void)show{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    UIView *rootView = win.rootViewController.view;
    [win addSubview:self];
    [win bringSubviewToFront:self];

    self.frame = CGRectMake(0, win.frame.size.height, win.frame.size.width,win.frame.size.height);
    
    CGFloat topEdge = win.frame.size.height - self.vSelection.frame.size.height;
    
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.frame = CGRectMake(0, 0, win.frame.size.width, win.frame.size.height);
    if (self.objectWithInput != nil){
        CGPoint ptObject = self.objectWithInput.frame.origin;
        UIView *vSuper = self.objectWithInput.superview;
        CGPoint ptNew = [win convertPoint:ptObject fromView:vSuper];
        if (ptNew.y + self.objectWithInput.frame.size.height > topEdge){
            CGFloat offsetY = (ptNew.y + self.objectWithInput.frame.size.height) - topEdge + 5.0f;
            rootView.frame = CGRectMake(0, (-1) * offsetY, rootView.frame.size.width, rootView.frame.size.height);
        }
    }
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView commitAnimations];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputViewDidShow:)]){
        [self.delegate customizedInputViewDidShow:self];
    }
}

-(void)dismissInputView:(BOOL)doneButtonTapped{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    UIView *rootView = win.rootViewController.view;
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: (__bridge void * _Nullable)([NSNumber numberWithBool:doneButtonTapped])];
    
    self.frame = CGRectMake(0, win.frame.size.height, self.frame.size.width, self.frame.size.height);
    if (rootView.frame.origin.y < 0){
        rootView.frame = CGRectMake(0, 0, rootView.frame.size.width, rootView.frame.size.height);
    }
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];

}
-(void)dismiss{
    [self dismissInputView:NO];
}

-(void)animationDidStop:(NSString*)annimationId finished:(NSNumber*)finished context:(void*)context{
    BOOL doneButtonTapped = NO;
    if (context != nil){
        NSNumber *num = (__bridge NSNumber *)context;
        doneButtonTapped = [num boolValue];
    }
    [self removeFromSuperview];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputviewDismissed:doneButtonTapped:)]){
        [self.delegate customizedInputviewDismissed:self doneButtonTapped:doneButtonTapped];
    }
}

-(IBAction)buttonTapped:(id)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputViewDoneButtonTapped:)]){
        [self.delegate customizedInputViewDoneButtonTapped:self];
    }
    [self dismissInputView:YES];
}

-(NSDate*)getDate{
    return self.datePicker.date;
}

-(NSArray*)getDataPickerValue{
    if (self.viewType != CustomizedInputView_PickerView_Numbers){
        return nil;
    }
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    for(NSInteger idx = 0; idx < self.dataPicker.numberOfComponents; idx ++){
        NSInteger row = [self.dataPicker selectedRowInComponent:idx];
        NSString *str = [self.dataPicker.delegate pickerView:self.dataPicker titleForRow:row forComponent:idx];
        [array addObject:str];
    }
    return array;
}


-(NSInteger)getDataPickerSelectedNumber{
    NSArray *array = [self getDataPickerValue];
    if ([array count] > 0){
        NSString *str = [array objectAtIndex:0];
        for(NSInteger idx = 1; idx < [array count]; idx ++){
            str = [NSString stringWithFormat:@"%@%@",str,[array objectAtIndex:idx]];
        }
        return [str integerValue];
    }
    return 0;
}

-(void)datePickerValueChanged:(id)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputViewValueChanged:)]){
        [self.delegate customizedInputViewValueChanged:self];
    }
}

-(void)setDataPickerDataSource:(id<UIPickerViewDataSource>)pickerViewDataSource{
    self.dataPicker.dataSource = pickerViewDataSource;
}
-(void)updateDataPickerView{
    [self.dataPicker reloadAllComponents];
}

-(void)setTableViewDataSource:(id<UITableViewDataSource>)tableViewDataSource{
    self.tvList.dataSource = tableViewDataSource;
}

-(void)registerNibForCell:(NSString*)nibName indentifier:(NSString*)identifier{
    [self.tvList registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:identifier];
}

-(void)updateTableView{
    [self.tvList reloadData];
}

#pragma
#pragma mask - UITapGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (touch.view == self){
        return YES;
    }
    return  NO;
}

#pragma 
#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputView:heightOfRowInTableView:row:section:)]){
        return [self.delegate customizedInputView:self heightOfRowInTableView:tableView row:indexPath.row section:indexPath.section];
    }
    else{
        return 40.0f;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputView:tableView:rowDidSelected:cell:)]){
        CustomizedInputViewListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self.delegate customizedInputView:self tableView:tableView rowDidSelected:indexPath cell:cell];
    }
}

#pragma 
#pragma mark - UIPickerViewDelegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 40.0f;
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputView:stringForPickerView:forRow:forComponet:)]){
        return [self.delegate customizedInputView:self stringForPickerView:pickerView forRow:row forComponet:component];
    }
    return nil;
}

//-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
//    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputView:viewForPickerView:forRow:forComponent:)]){
//        return [self.delegate customizedInputView:self viewForPickerView:pickerView forRow:row forComponent:component];
//    }
//    return nil;
//}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customizedInputView:pickerView:valueChangedForRow:forComponent:)]){
        [self.delegate customizedInputView:self pickerView:pickerView valueChangedForRow:row forComponent:component];
    }
}
@end
