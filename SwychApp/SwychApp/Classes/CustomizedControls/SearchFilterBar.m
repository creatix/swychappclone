//
//  SearchFilterBar.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SearchFilterBar.h"
#import "UIButton+Extra.h"

@interface SearchFilterBar()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSort;
@property (weak, nonatomic) IBOutlet UIButton *btnType;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation SearchFilterBar

@synthesize buttonImageSort = _buttonImageSort;
@synthesize buttonImageType = _buttonImageType;
@synthesize buttonImageFilter = _buttonImageFilter;
@synthesize placeHolderSearchBar = _placeHolderSearchBar;


-(void)setButtonImageSort:(UIImage *)buttonImageSort{
    _buttonImageSort = buttonImageSort;
    [self.btnSort setButtonImage:buttonImageSort];
}
-(UIImage*)buttonImageSort{
    return _buttonImageSort;
}

-(void)setButtonImageType:(UIImage *)buttonImageType{
    _buttonImageType = buttonImageType;
    [self.btnType setButtonImage:buttonImageType];
}
-(UIImage*)buttonImageType{
    return _buttonImageType;
}

-(void)setButtonImageFilter:(UIImage *)buttonImageFilter{
    _buttonImageFilter = buttonImageFilter;
    [self.btnFilter setButtonImage:buttonImageFilter];
}
-(UIImage*)buttonImageFilter{
    return _buttonImageFilter;
}

-(void)setPlaceHolderSearchBar:(NSString *)placeHolderSearchBar{
    _placeHolderSearchBar = placeHolderSearchBar;
    self.searchBar.placeholder = placeHolderSearchBar;
}
-(NSString*)placeHolderSearchBar{
    return _placeHolderSearchBar;
}


-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"SearchFilterBar" class:[self class] owner:self options:nil];
    
    view.backgroundColor = [UIColor clearColor];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self setup];
    
    [self addSubview:view];
}

-(void)setup{
    self.searchBar.delegate = self;
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    self.searchBar.placeholder = NSLocalizedString(@"Placeholder_Search", nil);
    
}

- (IBAction)buttonTapped:(id)sender {
    if (self.delegate == nil){
        return;
    }
    if (sender == self.btnSort){
        if ([self.delegate respondsToSelector:@selector(searchFilterBarSortButtonTapped:)]){
            [self.delegate searchFilterBarSortButtonTapped:self];
        }
    }
    else if (sender == self.btnType){
        if ([self.delegate respondsToSelector:@selector(searchFilterBarViewTypeButtonTapped:)]){
            [self.delegate searchFilterBarViewTypeButtonTapped:self];
        }
    }
    else if (sender == self.btnFilter){
        if ([self.delegate respondsToSelector:@selector(searchFilterBarFilterButtonTapped:)]){
            [self.delegate searchFilterBarFilterButtonTapped:self];
        }
    }
}

#pragma 
#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(searchFilterBar:startSearchText:)]){
        [self.delegate searchFilterBar:self startSearchText:searchText];
    }
}


@end
