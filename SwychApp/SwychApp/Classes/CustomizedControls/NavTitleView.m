//
//  NavTitleView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "NavTitleView.h"
#import "UILabelExt.h"

#define SwychLogoTopOrg     3.0

@interface NavTitleView()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSwychBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSwychLogoTop;



@property (weak, nonatomic) IBOutlet UIImageView *ivSwych;
@property (weak, nonatomic) IBOutlet UILabelExt *labEarnPoints;

@end

@implementation NavTitleView

@synthesize titleText = _titleText;

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.labEarnPoints.verticalAlignment = VerticalAlignment_Middle;
}

-(void)setTitleText:(NSString *)titleText{
    _titleText = titleText;
    if (titleText == nil){
        CGFloat h = self.constratintSwychLogoHeight.constant;
        CGFloat offset = (self.bounds.size.height - h) / 2.0;
        self.constraintSwychLogoTop.constant = offset;
        self.labEarnPoints.hidden = YES;
        self.constraintLabHeight.constant = 0.0f;
    }
    else{
        self.labEarnPoints.hidden = NO;
        self.labEarnPoints.text = titleText;
    }
}
-(NSString*)titleText{
    return _titleText;
}

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

@end
