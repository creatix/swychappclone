//
//  ButtonWithTextAtBottom.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonWithTextAtBottomDelegate;

IB_DESIGNABLE
@interface ButtonWithTextAtBottom : UIView

@property (nonatomic,strong) IBInspectable NSString *text;
@property (nonatomic,strong) IBInspectable UIImage *buttonImage;

@property (nonatomic,strong) IBInspectable UIColor *textColor;
@property (nonatomic,strong)  UIFont *textFont;

@property (nonatomic,assign) IBInspectable BOOL showCloseButton;
@property (nonatomic,strong) UIImage *pictureImageViewImage;

@property (nonatomic,weak)  IBOutlet id<ButtonWithTextAtBottomDelegate> itemDelegate;
@property (nonatomic,assign) IBInspectable CGFloat normalImageMargin;
-(void)SetColorOfImg:(UIColor*)newcolor;

@end

@protocol ButtonWithTextAtBottomDelegate <NSObject>

-(void)buttonWithTextAtBottonItemTapped:(ButtonWithTextAtBottom*)buttonItem;
-(void)buttonWithTextAtBottomCloseButtonTapped:(ButtonWithTextAtBottom*)buttonItem;
@end