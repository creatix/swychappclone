//
//  TransactionDetailCommentView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-07.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "TransactionDetailCommentView.h"
#import "TransactionDetailCommentSwychView.h"
#import "TransactionDetailViewController.h"
#import "ContactListTableViewCell.h"
#import "TransactionPaymentCell.h"
#import "ContactBookManager.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "ModelGiftAttachments.h"
#import "NSString+Extra.h"
#define DefaultTitleTextColor       UICOLORFROMRGB(76,77,77,1.0f)

#define DefaultTitleTextFont        [UIFont fontWithName:@"Helvetica Neue" size:14.0f]

#define DefaultButtonBackgroundColorNormal      UICOLORFROMRGB(255,255,255,1.0f)
#define DefaultButtonBackgroundcolorSelected    UICOLORFROMRGB(192, 31, 109,1.0f)

#define DefaultButtonTextColorNormal        UICOLORFROMRGB(76,77,77,1.0f)
#define DefaultButtonTextColorSelected      UICOLORFROMRGB(255,255,255,1.0f)

#define DefaultButtonBorderColorNormal      UICOLORFROMRGB(215,215,215,1.0f)
#define DefaultbuttonBorderColorSelected    UICOLORFROMRGB(192, 31, 109,1.0f)
#define NIBIDSENT 0
#define NIBIDSAVE 2
#define NIBIDOTHER 1
@implementation TransactionDetailCommentView
@synthesize transaction = _transaction;
@synthesize viewIndex;
-(void)setTransaction:(ModelTransaction *)transaction{
    _transaction = transaction;
    payments = [NSMutableArray arrayWithArray:_transaction.payments];
    payments2 = [NSMutableArray arrayWithArray:_transaction.payments2];
    [self TrimUnreasonableData];
    self.vAttachmentView.avatarImage = [ContactBookManager getContactHeadImgByPhoneNumber:_transaction.sender.phoneNumber];
    self.vAttachmentView.senderName = _transaction.sender.firstName;
    self.vAttachmentView.message = _transaction.sentMessage.message;
    if (self.transaction.sentMessage == nil||[NSString isNullOrEmpty:self.transaction.sentMessage.imgUrl]){
        self.vAttachmentView.videoButtonViewHeight = 0.0f;
    }
    else{
        self.vAttachmentView.imageUrl = self.transaction.sentMessage.imgUrl;
    }
    int iIndex = [TransactionDetailViewController getiIndex];
    if(iIndex == RedeemedAsSwychBalace)
    [self ReOrgnizePaymentList];
    
}
-(void)TrimUnreasonableData{
    if(!payments) return;
    for(int i=0;i<payments.count;i++){
        ModelPaymentInfo* item = payments[i];
        if(item.paymentType == PaymentOption_RewardPoints)
           [payments removeObject:item];
    }
}
-(double)GetTotal{
    double totalAmount = 0.0;
    if(payments == nil) return totalAmount;
    for(int i=0;i<payments.count;i++){
        ModelPaymentInfo* item = payments[i];
        totalAmount = totalAmount+item.amount;
    }
    return totalAmount;
}
-(void)ReOrgnizePaymentList{
    double syCredit = [self GetTotalSwychCredit];
    double bonus = 0.0;
    if(syCredit!=(double)_transaction.amount){
        bonus = (double)_transaction.amount - syCredit;
    }
    ModelPaymentInfo* bonusItem = [ModelPaymentInfo new];
    bonusItem.paymentType = PaymentOption_SwychBalance;
    bonusItem.amount = bonus;
    if(payments2!=nil){
        [payments2 removeAllObjects];
    }
    else{
    payments2 = [[NSMutableArray alloc] init];
    }
    [payments2 addObject:bonusItem];
    if(bonus==0){
        [payments2 removeAllObjects];
        payments2 = nil;
    }
    
    ModelPaymentInfo* bonusItem1 = [ModelPaymentInfo new];
    bonusItem1.paymentType = PaymentOption_SwychBalance;
    bonusItem1.amount = syCredit;//[self GetTotal];
    [payments removeAllObjects];
    [payments addObject:bonusItem1];
}
-(double)GetTotalSwychCredit{
    double totalAmount = 0.0;
    double totalAmount2 = 0.0;
    if(payments != nil){
        for(int i=0;i<payments.count;i++){
            ModelPaymentInfo* item = payments[i];
            totalAmount = totalAmount+item.amount;
        }
    }
    if(payments2 != nil){
        for(int i=0;i<payments2.count;i++){
            ModelPaymentInfo* item = payments2[i];
            totalAmount2 = totalAmount2+item.amount;
        }
    }
    return totalAmount+totalAmount2;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}
-(void)loadView{
    int iIndex = [TransactionDetailViewController getiIndex];
    NSString* strNibName=@"";
    strNibName = @"TransactionDetailCommentSwychView";
    if(iIndex == E_giftSentOut){
        viewIndex = NIBIDSENT;
    }
    else if(iIndex == E_giftReceived|| iIndex == E_giftUserSaved){
        viewIndex = NIBIDSAVE;
    }
    else if(iIndex == Paid|| iIndex == RedeemedAsSwychBalace){
        viewIndex = NIBIDOTHER;
    }
    else {
        viewIndex = NIBIDOTHER;
    }
    UIView *view =[Utils loadViewFromNib:strNibName class:[self class] owner:self options:nil index:viewIndex];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    if(viewIndex == NIBIDSENT){
    [_tbContent registerNib:[UINib nibWithNibName:@"TransactionPaymentCell" bundle:nil] forCellReuseIdentifier:@"TransactionPaymentCellid"];
    [_tbContent registerNib:[UINib nibWithNibName:@"TransactionMessageCell" bundle:nil] forCellReuseIdentifier:@"TransactionMessageCell"];
    
    _btnRemind.backgroundColor = DefaultButtonBackgroundcolorSelected;
    _btnRemind.borderColor = DefaultButtonBackgroundcolorSelected;
    [_btnRemind setTitleColor:DefaultButtonTextColorSelected];
    _btnTakeBack.backgroundColor = DefaultButtonBackgroundcolorSelected;
    _btnTakeBack.borderColor = DefaultButtonBackgroundcolorSelected;
    [_btnTakeBack setTitleColor:DefaultButtonTextColorSelected];
    [_btnTakeBack setTitle: NSLocalizedString(@"Transaction_detail_TakeBack", nil)];
    [_btnRemind setTitle: NSLocalizedString(@"Transaction_detail_Remind", nil)];
        
    _btnTakeBack.backgroundColor = UICOLORFROMRGB(209, 210, 210, 1.0);
    [_btnTakeBack setTitleColor:UICOLORFROMRGB(76, 77, 77, 1.0)];
    }
    if(viewIndex == NIBIDOTHER){
        [_tbContent1 registerNib:[UINib nibWithNibName:@"TransactionPaymentCell" bundle:nil] forCellReuseIdentifier:@"TransactionPaymentCellid"];
    }
    if(_vAttachmentView!=nil)
        _vAttachmentView.delegate = self;

}
-(void)ReloadView{

}
- (IBAction)buttonTapped:(id)sender{
    if(sender == self.btnRemind){
        if(self.selectionViewDelegate!=nil){
            [self.selectionViewDelegate Remind];
        }
    }
    else if(sender == self.btnTakeBack){
        if(self.selectionViewDelegate!=nil){
            [self.selectionViewDelegate TakeBack];
        }
    }
    
}

#pragma mark - TransactionMessageCellDelegate
-(void)viewVideoButtonTapped:(TransactionMessageCell*)paymentOptionCell buttonTag:(NSInteger)tag{
// play the video here
}
#pragma mark - GiftAttachmentViewDelegate
-(void)giftAttachmentViewSayThanksButtonTapped:(GiftAttachmentView*)attachmentView{
    if(self.selectionViewDelegate!=nil&& [self.selectionViewDelegate respondsToSelector:@selector(SayThanks:)]){
        [self.selectionViewDelegate SayThanks:attachmentView.message];
    }
}
-(void)giftAttachmentViewWillPlayVideoMessage:(GiftAttachmentView*)attachmentView{}
-(void)giftAttachmentViewFinishedPlayVideoMessage:(GiftAttachmentView*)attachmentView{}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor = [UIColor clearColor];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(viewIndex == NIBIDSAVE) return 0;
    if(section == 0)
        return payments2.count;
    else{
    if(viewIndex == NIBIDSENT)
       return payments.count+2;
    else{
       return payments.count+1;
    }
    }
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int iIndex = [TransactionDetailViewController getiIndex];
    if(indexPath.section == 1){
        if(indexPath.row == payments.count+1){
        static NSString *cellIndentifierlast = @"TransactionMessageCell";
            TransactionMessageCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIndentifierlast];
            if (cell1 == nil){
                cell1 = [[TransactionMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifierlast];
                
            }
            [cell1.btnVideo setTitle:NSLocalizedString(@"Transaction_detail_Video_Attached", nil)];
            cell1.delegate = self;
            if(_transaction.sentMessage.imgUrl != nil && ![_transaction.sentMessage.imgUrl isEqualToString:@""])
            cell1.imageUrl = _transaction.sentMessage.imgUrl;
            else{
                cell1.btnVideo.hidden = YES;
            }
            cell1.labelMessage.text =  _transaction.sentMessage.message;
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            NSString* senderName = localUser.firstName;
            if(senderName!=nil&&![senderName isEqualToString:@""]){
            senderName = [NSString stringWithFormat:@"-%@", senderName];
            }
            else
                senderName = @"";
            cell1.lblSenderName.text = senderName;
                
            return cell1;

        }
        static NSString *cellIndentifier = @"TransactionPaymentCellid";
        TransactionPaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (cell == nil){
            cell = [[TransactionPaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
        }

        if (indexPath.row == 0){
            cell.labelMethod.text =@"";
            NSString *string1 = [NSString stringWithFormat:@"%@ $%0.2f",NSLocalizedString(@"Transaction_detail_Order_Total", nil),
                                 [self GetTotal]];
            if(iIndex == RedeemedAsCash ||iIndex ==RedeemedAsGiftcard||iIndex ==RedeemedAsMerchant){
                string1 = [NSString stringWithFormat:@"%@ $%0.2f",NSLocalizedString(@"Transaction_detail_Order_Total", nil),
                           _transaction.amount];
            }
            cell.labelAmount.text = string1;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        else {
            ModelPaymentInfo* item = payments[indexPath.row-1];
            
            cell.labelMethod.text =[Utils getPaymetOptionName:item.paymentType];
            NSString *string1 = [NSString stringWithFormat:@"-$%0.2f",
                                 item.amount];
            cell.labelAmount.text = string1;
        }
        return cell;
    }
    else{
        static NSString *cellIndentifier = @"TransactionPaymentCellid";
        TransactionPaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (cell == nil){
            cell = [[TransactionPaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
        }
        
        ModelPaymentInfo* item = payments2[indexPath.row];
        cell.labelMethod.text =[Utils getPaymetOptionName:item.paymentType];
        NSString *string1 = [NSString stringWithFormat:@"-$%0.2f",
                             item.amount];
        
        if(iIndex == RedeemedAsSwychBalace){
            NSString* stramountFormat = @"$%d";
            if(item.amount<0){
                stramountFormat = @"-$%d";
                string1 = [NSString stringWithFormat:@"$%0.2f",
                           fabs(item.amount)];
            }
           NSString* str = [NSString stringWithFormat:NSLocalizedString(@"PaymentOption_SwyChinge_balance", nil),[NSString stringWithFormat:stramountFormat,(int)fabs(item.amount)]];
            cell.labelMethod.text =str;
            
        }
        if(item.paymentType == PaymentOption_RewardPoints){
            int aNum = [item.paymentReferenceNumber intValue];
            NSString *display = [NSNumberFormatter localizedStringFromNumber:@(aNum)
                                                                 numberStyle:NSNumberFormatterDecimalStyle];
            NSString* str = [NSString stringWithFormat:NSLocalizedString(@"PaymentOption_RewardPoints_", nil),
                             [NSString localizedStringWithFormat:@"%@", display]
                             ];
            cell.labelMethod.text =str;
        }
        cell.labelAmount.text = string1;
        //
        if(item.paymentType == PaymentOption_EearnPoints){
            int aNum = [item.paymentReferenceNumber intValue];
            NSString *display = [NSNumberFormatter localizedStringFromNumber:@(aNum)
                                                                 numberStyle:NSNumberFormatterDecimalStyle];
            NSString* str = [NSString stringWithFormat:NSLocalizedString(@"PaymentOption_EarnedPoints_", nil),
                             [NSString localizedStringWithFormat:@"%@", display]
                             ];
            cell.labelMethod.text =str;
            cell.labelAmount.text = @"";
        }
        //
        
        
        UIView* backgroundView = [ [ UIView alloc ] initWithFrame:CGRectZero ];
        UIColor *clr = [UIColor colorWithRed:218/255.0 green:222/255.0 blue:222/255.0 alpha:1/1.0];//[[ApplicationConfigurations instance] getViewBackgroundColor:NSStringFromClass([self class])];
        backgroundView.backgroundColor = clr;
        cell.backgroundView = backgroundView;
        for ( UIView* view in cell.contentView.subviews )
        {
            view.backgroundColor = [ UIColor clearColor ];
        }
        return cell;

    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == (payments.count+1)){
        return 160;
    }
    return 45.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
