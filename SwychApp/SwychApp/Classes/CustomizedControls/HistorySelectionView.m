//
//  HistorySelectionView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-31.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "HistorySelectionView.h"
#import "TransactionCollectionViewCell.h"
#import "TransactionHistoryTableViewCell.h"
#import "UIButton+Extra.h"
//#import "GiftcardMallTableViewCell.h"
//#import "ModelGiftCard.h"
#import "ModelTransaction.h"
#import "ModelCustomizedActionSheetItem.h"



#define ITEM_WIDTH_COLLECTION_VIEW_WIDTH  340
#define ITEM_WIDTH_COLLECTION_VIEW_HEIGTH  340
#define ACTION_SHEET_CONTEXT_FILTER     1
#define ACTION_SHEET_CONTEXT_SORTING    2
@interface HistorySelectionView()
@property(nonatomic,strong) ModelCustomizedActionSheetItem *selectedFilterItem;
@property(nonatomic,strong) ModelCustomizedActionSheetItem *selectedSortingItem;
@property(nonatomic,strong) NSString *currentSearchCriteria;
@property(nonatomic,strong) NSMutableArray *giftcardsInUse;
@end
@implementation HistorySelectionView

@synthesize transactions = _transactions;

-(NSArray*)transactions{
    return _transactions;
}

-(void)setTransactions:(NSArray *)transactions{
    
    _transactions = transactions;
    _giftcardsInUse = [NSMutableArray arrayWithArray:transactions];
    indicatorFooter = nil;
     [self.tvGiftcards setTableFooterView:nil];
    if(indicatorFooterForCollectionview){
        [indicatorFooterForCollectionview removeFromSuperview];
        indicatorFooterForCollectionview = nil;
    }
}
-(void)RemoveIndicatorFooter{
    indicatorFooter = nil;
    [self.tvGiftcards setTableFooterView:nil];
    if(indicatorFooterForCollectionview){
        [indicatorFooterForCollectionview removeFromSuperview];
        indicatorFooterForCollectionview = nil;
    }
}
-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"HistorySelectionView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.tvGiftcards registerNib:[UINib nibWithNibName:@"TransactionHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"transactionDetailcell"];
    [self.cvGiftcards registerNib:[UINib nibWithNibName:@"TransactionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TransactionCollectionViewDetailCell"];
    
    
    self.cvGiftcards.hidden = YES;
    self.tvGiftcards.hidden = NO;
    self.cvGiftcards.delegate = self;
    self.cvGiftcards.dataSource = self;
    self.tvGiftcards.delegate = self;
    self.tvGiftcards.dataSource = self;
    _vHeader.delegate = self;
    _vHeader.btnTypeWidth.constant = 0.0f;
    
    [Utils addNotificationObserver:self selector:@selector(giftcardFavoriteNotificationHandler:) name:NOTI_Giftcard_Favorite object:nil];

}

-(void)giftcardFavoriteNotificationHandler:(NSNotification*)noti{
    if (self.transactions != nil && noti.userInfo != nil){
        ModelGiftCard *giftcard = [noti.userInfo objectForKey:@"Giftcard"];
        if (giftcard != nil){
            for(ModelTransaction *tran in self.transactions){
                if (tran.giftCard != nil && tran.giftCard.giftCardId == giftcard.giftCardId){
                    tran.giftCard.favourite = giftcard.favourite;
                }
            }
        }
    }
}


-(void)refreshData{
  //  [self.tvGiftcards reloadData];
    [self rebuildInUseGiftcardsArray];
  //  [self.cvGiftcards reloadData];
    [self.self.cvGiftcards reloadSections:[NSIndexSet indexSetWithIndex:0]];
}

-(void)switchGiftcardListView{
    if (self.tvGiftcards.hidden){
        self.cvGiftcards.hidden = YES;
        self.tvGiftcards.hidden = NO;
        [self.tvGiftcards reloadData];
    }
    else{
        self.cvGiftcards.hidden = NO;
        self.tvGiftcards.hidden = YES;
        [self.cvGiftcards reloadData];
    }
}

-(void)showSortingSheet{
    CustomizedActionSheet *cas = [CustomizedActionSheet createInstanceWithItems:[Utils getHistorySortingOptionsArray]
                                                                 withButtonText:NSLocalizedString(@"Button_Apply", nil)];
    cas.selectedItemId = self.selectedSortingItem == nil ? -1 : self.selectedSortingItem.itemId;
    cas.actionSheetDelegate = self;
    cas.context = ACTION_SHEET_CONTEXT_SORTING;
    [cas show];
}


-(void)showFilterSheet{
    CustomizedActionSheet *cas = [CustomizedActionSheet createInstanceWithItems:[Utils getHistoryFilterOptionsArray]
                                                                 withButtonText:NSLocalizedString(@"Button_Apply", nil)];
    cas.selectedItemId = self.selectedFilterItem == nil ? -1 : self.selectedFilterItem.itemId;
    cas.actionSheetDelegate = self;
    cas.context = ACTION_SHEET_CONTEXT_FILTER;
    [cas show];
}

-(void)rebuildInUseGiftcardsArray{
    NSArray *array = self.transactions;
    if ([self.currentSearchCriteria length] > 0){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *card, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [card.giftCard retailerNameLike:self.currentSearchCriteria];
        }];
        array = [array filteredArrayUsingPredicate:pred];
    }
    if (self.selectedFilterItem != nil){
        NSInteger filterId = self.selectedFilterItem.itemId;
//        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *card, NSDictionary<NSString *,id> * _Nullable bindings) {
//            if (filterId == GiftCardFilterOption_Favorite){
//                return card.giftCard.favourite == YES;
//            }
//            else{
//                return [card.giftCard hasFilterOptionById:filterId];
//            }
//        }];
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *card, NSDictionary<NSString *,id> * _Nullable bindings) {
            return card.transactionType == filterId;
        }];
        array = [array filteredArrayUsingPredicate:pred];
    }
    if (self.selectedSortingItem != nil){
        NSInteger sortingId = self.selectedSortingItem.itemId;
        switch (sortingId) {
            case GiftCardSortOption_Favorite:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelTransaction *card1 = (ModelTransaction*)a;
                    ModelTransaction *card2 = (ModelTransaction*)b;
                    return card1.giftCard.favourite < card2.giftCard.favourite;
                }];
                break;
            case GiftCardSortOption_Tranding:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelTransaction *card1 = (ModelTransaction*)a;
                    ModelTransaction *card2 = (ModelTransaction*)b;
                    return card1.giftCard.trending > card2.giftCard.trending;
                }];
                break;
            case GiftCardSortOption_Newest:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelTransaction *card1 = (ModelTransaction*)a;
                    ModelTransaction *card2 = (ModelTransaction*)b;
                    return [card1.giftCard.updated compare:card2.giftCard.updated];
                }];
                break;
            case GiftCardSortOption_PriceLowToHigh:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(ModelTransaction* a, ModelTransaction* b) {
                    NSNumber *first = [NSNumber numberWithLong:a.amount];
                    NSNumber *second = [NSNumber numberWithLong:b.amount];
                    NSComparisonResult result =  [first compare:second];
                    return  result;
                }];
                break;
            case GiftCardSortOption_PriceHighToLow:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(ModelTransaction* a, ModelTransaction* b) {
                    NSNumber *first = [NSNumber numberWithLong:a.amount];
                    NSNumber *second = [NSNumber numberWithLong:b.amount];
                    NSComparisonResult result =  [second compare:first];
                    return  result;
                }];
                break;
            default:
                break;
        }
    }
    
    self.giftcardsInUse = [NSMutableArray arrayWithArray:array];
    [self.tvGiftcards reloadData];
    [self.cvGiftcards reloadData];
}

-(void)giftcardSelected:(ModelTransaction*)card{
    if(self.selectionViewDelegate != nil && [self.selectionViewDelegate respondsToSelector:@selector(HistorySelectionView:giftcardSelected:)]){
        [self.selectionViewDelegate HistorySelectionView:self giftcardSelected:card];
    }
}
-(void)initializeRefreshControl
{
    indicatorFooter = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tvGiftcards.frame), 44)];
    [indicatorFooter setColor:[UIColor blackColor]];
    [indicatorFooter startAnimating];
    [self.tvGiftcards setTableFooterView:indicatorFooter];
}
-(void)initializeRefreshControlForCollectionView
{
    /*
    indicatorFooterForCollectionview = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.cvGiftcards.frame), 44)];
    [indicatorFooterForCollectionview setColor:[UIColor redColor]];
    [self.cvGiftcards addSubview :indicatorFooterForCollectionview];
    [self.cvGiftcards bringSubviewToFront:indicatorFooterForCollectionview];
    [indicatorFooterForCollectionview startAnimating];
     */
    if (indicatorFooterForCollectionview == nil){
        indicatorFooterForCollectionview = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        indicatorFooterForCollectionview.center = self.cvGiftcards.center;
        [self.cvGiftcards addSubview:indicatorFooterForCollectionview];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [indicatorFooterForCollectionview startAnimating];
    });
    
}
-(void)refreshTableVeiwList
{
    if(self.selectionViewDelegate != nil && [self.selectionViewDelegate respondsToSelector:@selector(HistorySelectionView:loadMore:)]&&_transactions.count>0){
        [self.selectionViewDelegate HistorySelectionView:self loadMore:[_transactions lastObject]];
        [self initializeRefreshControl];
        [self initializeRefreshControlForCollectionView];
    }
    //-(void)HistorySelectionView:(HistorySelectionView*)selectionView loadMore:(ModelTransaction*)lastObject;
    // your refresh code goes here
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(scrollView == self.cvGiftcards){
        if (scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height + 30)){
            
            [self refreshTableVeiwList];
            
        }
    }
    else if(scrollView == self.tvGiftcards){
    end = scrollView.contentOffset;
        if(end.y-start.y>150){
            
        [self refreshTableVeiwList];
        
    }
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(scrollView == self.tvGiftcards){
    start = scrollView.contentOffset;
    }
}
#pragma
/*
#pragma mark - CustomizedActionSheetDelegate
-(void)customizedActiondSheetButtonTapped:(CustomizedActionSheet *)sheet{
    ModelCustomizedActionSheetItem *item = sheet.selectedItem;
    if (item == nil){
        return;
    }
    if (sheet.context == ACTION_SHEET_CONTEXT_SORTING){
       //  [self doSort:item];
    }
    else if (sheet.context == ACTION_SHEET_CONTEXT_FILTER){
        
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelTransaction *card, NSDictionary<NSString *,id> * _Nullable bindings) {
            if(item.itemId == 0){
                return card.giftCard.favourite;
            }
            else{
            return [card.giftCard categoriesNameLike:item.name];
            }
        }];
        _giftcardsInUse = [NSMutableArray arrayWithArray:[self.transactions filteredArrayUsingPredicate:pred]];
        
        [self.tvGiftcards reloadData];
        [self.cvGiftcards reloadData];
        
    }
}
*/
#pragma mark - CustomizedActionSheetDelegate
-(void)customizedActiondSheetButtonTapped:(CustomizedActionSheet *)sheet{
    ModelCustomizedActionSheetItem *item = sheet.selectedItem;
    if (item == nil){
        return;
    }
    if (sheet.context == ACTION_SHEET_CONTEXT_SORTING){
        if (self.selectedSortingItem != nil && (self.selectedSortingItem.itemId == item.itemId)){
            self.selectedSortingItem = nil;
        }
        else{
            self.selectedSortingItem = item;
            
        }
        [self rebuildInUseGiftcardsArray];
    }
    else if (sheet.context == ACTION_SHEET_CONTEXT_FILTER){
        if (self.selectedFilterItem != nil && (self.selectedFilterItem.itemId == item.itemId)){
            self.selectedFilterItem = nil;
        }
        else{
            self.selectedFilterItem = item;
        }
        [self rebuildInUseGiftcardsArray];
    }
}

#pragma
#pragma mark - SearchFilterBarDelegate

-(void)searchFilterBarFilterButtonTapped:(SearchFilterBar*)bar{
[self showFilterSheet];
}
-(void)searchFilterBarSortButtonTapped:(SearchFilterBar*)bar{
    [self showSortingSheet];
}
-(void)searchFilterBarViewTypeButtonTapped:(SearchFilterBar*)bar{
    [self switchGiftcardListView];
}

-(void)searchFilterBar:(SearchFilterBar*)bar startSearchText:(NSString*)text{
    //[self doSearch:text];
    self.currentSearchCriteria = text;
    [self rebuildInUseGiftcardsArray];
}

#pragma
#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [_giftcardsInUse count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TransactionCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"TransactionCollectionViewDetailCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    ModelTransaction *card = [_giftcardsInUse objectAtIndex:indexPath.row];
    cell.giftcard = card;
    return cell;
}


#pragma
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   // [collectionView deselectRowAtIndexPath:indexPath animated:NO];
    ModelTransaction *card = [_giftcardsInUse objectAtIndex:indexPath.row];
    [self giftcardSelected:card];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}
#pragma
#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.frame.size.width-40, (self.frame.size.width -40)/ 2.3);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 15;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 20, 10, 20);
}

#pragma
#pragma mask - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_giftcardsInUse count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"transactionDetailcell";
    TransactionHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil){
        cell = [[TransactionHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    ModelTransaction *card = [_giftcardsInUse objectAtIndex:indexPath.row];
    cell.giftcard = card;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ModelTransaction *card = [_giftcardsInUse objectAtIndex:indexPath.row];
    [self giftcardSelected:card];
}

#pragma


@end
