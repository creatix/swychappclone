//
//  OrderCheckoutDetailView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICheckbox.h"
#import "CustomizedInputView.h"
#import "AlertViewNormal.h"


@class LocalUserInfo;

@protocol OrderCheckoutDetailViewDelegate;
@class ResponseVerifyPromotionCode;
@class ModelGiftCard;
@class ModelPaymentInfo;
@class ModelTransaction;


typedef void(^PromotionCodeVerificationCallback)(ResponseVerifyPromotionCode*);

IB_DESIGNABLE
@interface OrderCheckoutDetailView : UIView<UICheckboxDelegate,UITextFieldDelegate,CustomizedInputViewDelegate,UITableViewDataSource,AlertViewDelegate,UIPickerViewDataSource >

@property (nonatomic,weak) id<OrderCheckoutDetailViewDelegate> delegate;
@property (nonatomic,readonly) double amountDue;
@property (nonatomic,readonly) double amount;
@property (nonatomic,readonly) double totalDiscount;
@property (nonatomic,assign) BOOL allowMultipleDiscount;

@property (nonatomic,strong) NSString *promoCode;
@property (nonatomic,readonly) NSArray<ModelPaymentInfo*> *payments;

@property (nonatomic,assign) BOOL disableCredits;

-(void)setPurchaseInfo:(double)purchaseAmount giftcard:(ModelGiftCard*)card credits:(NSArray<ModelTransaction*>*)availableCredits;
-(void)update;
-(void)setDiscountAmount:(double)amount description:(NSString*)desc;
-(void)updateConstraints_After_gifted;
@end


@protocol OrderCheckoutDetailViewDelegate <NSObject>

@optional
-(void)orderCheckoutDetailView:(OrderCheckoutDetailView*)orderDetailView verifyPromotionCode:(NSString*)code giftcardId:(NSInteger)giftcardId purchaseAmount:(double)purchaseAmount callback:(void(^)(ResponseVerifyPromotionCode*))callback;
-(void)orderCheckoutDetailView:(OrderCheckoutDetailView*)orderDetailView paymentInfoChangedWithDueAmount:(double)amountDue;

@end
