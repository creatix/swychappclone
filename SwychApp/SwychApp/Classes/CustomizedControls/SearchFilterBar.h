//
//  SearchFilterBar.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchFilterBarDelegate;

IB_DESIGNABLE
@interface SearchFilterBar : UIView<UISearchBarDelegate>

@property(nonatomic,strong) UIImage *buttonImageFilter;
@property(nonatomic,strong) UIImage *buttonImageType;
@property(nonatomic,strong) UIImage *buttonImageSort;
@property(nonatomic,strong) NSString *placeHolderSearchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnTypeWidth;
@property(nonatomic,weak) id<SearchFilterBarDelegate> delegate;

@end


@protocol SearchFilterBarDelegate <NSObject>

@optional
-(void)searchFilterBarFilterButtonTapped:(SearchFilterBar*)bar;
-(void)searchFilterBarSortButtonTapped:(SearchFilterBar*)bar;
-(void)searchFilterBarViewTypeButtonTapped:(SearchFilterBar*)bar;

-(void)searchFilterBar:(SearchFilterBar*)bar startSearchText:(NSString*)text;
@end