//
//  TransactionDetailCardInfoImgView.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-07.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionDetailCardInfoImgView.h"
#import "UIImageView+WebCache.h"
@implementation TransactionDetailCardInfoImgView
@synthesize transaction = _transaction;
-(void)setTransaction:(ModelTransaction *)transaction{
    _transaction = transaction;
    if (_transaction.giftCard.giftCardImageURL != nil && [_transaction.giftCard.giftCardImageURL count] > 0){
        [self.backGroundImg sd_setImageWithURL:[NSURL URLWithString:[_transaction.giftCard.giftCardImageURL objectAtIndex:0]]];
    }
    self.lblName.text = _transaction.giftCard.retailer;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}
-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"TransactionDetailCardInfoImgView" class:[self class] owner:self options:nil];
//    UIView *view =[Utils loadViewFromNib:@"TransactionDetailCardInfoImgView" viewTag:102];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor yellowColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
    
    //self.constraintHeightLabeName.constant = 0.0f;
    //self.ivFavorite.hidden = YES;
    
    self.layer.cornerRadius = 9.0f;
    self.layer.masksToBounds = YES;
 
}

-(void)touchOnView:(id)sender{
    
}
@end
