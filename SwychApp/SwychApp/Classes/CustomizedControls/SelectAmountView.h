//
//  SelectAmountView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface SelectAmountView : UIView<UITextFieldDelegate>{
    NSInteger selectedAmountIndex;
    NSArray* buttonArray;
    NSArray* buttonConstrintsArray;
}

@property (nonatomic,strong) NSArray *amountArray;
@property (nonatomic,readonly) double selectedAmount;

@property (nonatomic,assign) NSInteger minimumAmount;
@property (nonatomic,assign) NSInteger maximumAmount;

@property (nonatomic,strong) IBInspectable UIColor *titleTextColor;
@property (nonatomic,strong) IBInspectable UIFont *titleTextFont;

@property (nonatomic,strong) IBInspectable UIColor *buttonTextColorNormal;
@property (nonatomic,strong) IBInspectable UIColor *buttonTextcolorSelected;

@property (nonatomic,strong) IBInspectable UIColor *buttonBackgroundColorNormal;
@property (nonatomic,strong) IBInspectable UIColor *buttonBackgroundColorSelected;

@property (nonatomic,strong) IBInspectable UIColor *buttonBorderColorNormal;
@property (nonatomic,strong) IBInspectable UIColor *buttonBorderColorSelected;
@property (nonatomic,weak) ModelGiftCard *giftcard;
-(void)setCurrentSelectedAmountByValue:(double)amount;
-(void)SetDefaultButton;
-(void)updateConstraints_After_gifted;
@end
