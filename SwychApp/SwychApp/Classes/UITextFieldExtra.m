//
//  UITextFieldExtra.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UITextFieldExtra.h"

#define DEFAULT_BORDER_WIDTH    1.0f

@interface UITextFieldExtra()

@property(nonatomic,strong) CALayer *bottomBorderLayer;

@end

@implementation UITextFieldExtra

@synthesize bottomBorderColor = _bottomBorderColor;

-(void)setBottomBorderColor:(UIColor *)bottomBorderColor{
    _bottomBorderColor = bottomBorderColor;
    if (self.bottomBorderLayer == nil){
        self.bottomBorderLayer = [CALayer layer];
    }
    self.bottomBorderLayer.borderColor = bottomBorderColor.CGColor;
    CGFloat borderWidth = DEFAULT_BORDER_WIDTH;
    self.bottomBorderLayer.frame = CGRectMake(0,self.frame.size.height - borderWidth,self.frame.size.width,self.frame.size.height);
    self.bottomBorderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:self.bottomBorderLayer];
    self.layer.masksToBounds = YES;
}
-(UIColor*)bottomBorderColor{
    return _bottomBorderColor;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat borderWidth = DEFAULT_BORDER_WIDTH;
    self.bottomBorderLayer.frame = CGRectMake(0,self.frame.size.height - borderWidth,self.frame.size.width,self.frame.size.height);
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    if(self.leftView){        
        return CGRectMake(bounds.origin.x + 12.0 + self.offsetX, bounds.origin.y + self.offsetY,
                          bounds.size.width - 12.0 - 2 * self.offsetX, bounds.size.height - 2 * self.offsetY);
    }
    else if(self.delegate&&self.background==nil&&self.backgroundColor==nil&&self.borderStyle==UITextBorderStyleNone){
        return bounds;
    }
    else{
        return CGRectMake(bounds.origin.x + self.offsetX, bounds.origin.y + self.offsetY,
                          bounds.size.width - 2 * self.offsetX, bounds.size.height - 2 * self.offsetY);
        
        ;
    }
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    if(self.leftView){
        return CGRectMake(bounds.origin.x + 12.0 + self.offsetX, bounds.origin.y + self.offsetY,
                          bounds.size.width - 12.0 - 2 * self.offsetX, bounds.size.height - 2 * self.offsetY);
    }
    else if(self.delegate&&self.background==nil&&self.backgroundColor==nil&&self.borderStyle==UITextBorderStyleNone){
        return bounds;
    }
    else{
        return CGRectMake(bounds.origin.x + self.offsetX, bounds.origin.y + self.offsetY,
                          bounds.size.width - 2 * self.offsetX, bounds.size.height - 2 * self.offsetY);
    }
}

@end
