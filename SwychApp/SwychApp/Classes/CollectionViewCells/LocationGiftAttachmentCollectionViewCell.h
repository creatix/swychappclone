//
//  LocationGiftAttachmentCollectionViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GiftAttachmentView;

@interface LocationGiftAttachmentCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITableView *tvLocations;
@property (weak, nonatomic) IBOutlet GiftAttachmentView *cvAttachment;
@property (weak, nonatomic) IBOutlet UIImageView *ivQuote;
@property (nonatomic,strong) NSArray *quotesFileNameArray;
@property(nonatomic,assign) BOOL showLocationView;
@property(nonatomic,assign) BOOL showQuoteView;
-(void)updateCell;
@end
