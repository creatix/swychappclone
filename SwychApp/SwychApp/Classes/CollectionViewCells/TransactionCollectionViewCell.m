//
//  TransactionCollectionViewCell.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-01.
//  Copyright © 2016 Swych Inc. All rights reserved.
//
#import "UITransactionGiftcardView.h"
#import "TransactionCollectionViewCell.h"
#import "UITransactionGiftcardView.h"
#import "ModelGiftCard.h"
#import "UIImageView+WebCache.h"
#import "ContactBookManager.h"
#import "Utils.h"
@implementation TransactionCollectionViewCell
@synthesize giftcard = _giftcard;
@synthesize giftcardImage = _giftcardImage;
@synthesize headImage = _headImage;
-(ModelTransaction *)giftcard{
    return _giftcard;
}

-(void)setGiftcard:(ModelTransaction *)giftcard{
    _giftcard = giftcard;

    if ([_giftcard.giftCard.giftCardImageURL count] > 0){
        /*NSURL* imageURL = [NSURL URLWithString:[giftcard.giftCard.giftCardImageURL objectAtIndex:0]];
        [SDWebImageDownloader.sharedDownloader downloadImageWithURL:imageURL
                                                            options:0
                                                           progress:nil
                                                          completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
         {
             if ( image && finished)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(){
//                     self.ivGiftcard.image = [Utils resizeImage:image WithWidth:self.frame.size.width withHeight:self.frame.size.width/(300.0/190.0) withKeepAspectRatio:NO];
                     self.ivGiftcard.image = image;
                     self.ivGiftcard.clipsToBounds  = YES;
                 });
                 
             }
         }];*/
        [self.ivGiftcard sd_setImageWithURL:[NSURL URLWithString:[giftcard.giftCard.giftCardImageURL objectAtIndex:0]]];
    }
    else{
        self.ivGiftcard.image = nil;
        self.ivGiftcard.clipsToBounds  = YES;
    }
//    if ([_giftcard.giftCard.he count] > 0){
//        [self.ivGiftcard sd_setImageWithURL:[NSURL URLWithString:[giftcard.giftCard.giftCardImageURL objectAtIndex:0]]];
//    }
    // self.labGiftcardName.text = giftcard.giftCard.retailer;
    NSString * htmlString = giftcard.transactionDescription; //@"<html><body> Some html string </body></html>";
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.labGiftcardName.attributedText = attrStr;
    NSDate *transdate = _giftcard.transactionDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    formatter.dateFormat = @"MM/dd/yy";
    self.labDate.text =  [formatter stringFromDate:transdate];
    /*
        UIImage* img = nil;
        self.ivHeadImg.image = nil;
        img = [ContactBookManager getContactHeadImgByPhoneNumber:giftcard.recipientPhoneNumber];
        
        if(img!=nil){
            _headImage = [Utils resizeHeaderImg:img];;
            [self.ivHeadImg setImage: _headImage];
            self.constraintHeadImg.constant = 50.0f;
        }
        else
            self.constraintHeadImg.constant = 0.0f;
     */
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(concurrentQueue, ^{
        __block UIImage *img = nil;
        dispatch_sync(concurrentQueue, ^{
          //  UIImage* img = nil;
            self.ivHeadImg.image = nil;
            img = [ContactBookManager getContactHeadImgByPhoneNumber:giftcard.recipientPhoneNumber];
        });
        dispatch_sync(dispatch_get_main_queue(), ^{
            if(img!=nil){
                _headImage = [Utils resizeHeaderImg:img];;
                [self.ivHeadImg setImage: _headImage];
                self.constraintHeadImg.constant = 40.0f;
            }
            else
                self.constraintHeadImg.constant = 0.0f;
        });
    });

}


-(UIImage *)giftcardImage{
    return _giftcardImage;
}

-(void)setGiftcardImage:(UIImage *)giftcardImage{
    UIImage *img = [Utils resizeImage:giftcardImage WithWidth:self.frame.size.width withHeight:self.frame.size.width withKeepAspectRatio:YES];
    _giftcardImage = img;
    
    self.ivGiftcard.image = _giftcardImage;
}
-(void)setHeadImage:(UIImage *)headImage{
    _headImage = headImage;
    self.ivHeadImg.image = _headImage;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    self.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
    self.ivHeadImg.layer.cornerRadius = 5.0f;
    [self.ivHeadImg.layer setMasksToBounds:YES];
    
    self.layer.cornerRadius = 9.0f;
    self.layer.masksToBounds = YES;
    [self.ivHeadImg setContentHuggingPriority:UILayoutPriorityRequired
                               forAxis:UILayoutConstraintAxisHorizontal];
}

-(void)touchOnView:(id)sender{
    if (self.giftcardViewDelegate != nil && [self.giftcardViewDelegate respondsToSelector:@selector(giftcardViewTapped:)]){
        [self.giftcardViewDelegate giftcardViewTapped:self];
    }
}
@end
