//
//  GiftcardMallCollectionViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIGiftcardView;

@interface GiftcardMallCollectionViewCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UIGiftcardView *giftcardView;

@end
