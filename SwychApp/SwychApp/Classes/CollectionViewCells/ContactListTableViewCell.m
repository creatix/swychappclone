//
//  ContactListTableViewCell.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-21.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ContactListTableViewCell.h"
#import "AContactItem.h"
@implementation ContactListTableViewCell

- (void)awakeFromNib {
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.txtContent.delegate = self;
    
    // Configure the view for the selected state
}
#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(self.icontactType == PHONE_CELL){
    [self.contact changeContactPhoneNumber:self.ContentOfContact forThis:textField.text];
    }
    else if (self.icontactType == EMAIL_CELL){
    [self.contact changeContactEmail:self.ContentOfContact forThis:textField.text];
    }
    [textField resignFirstResponder];
    
    return;
}
@end
