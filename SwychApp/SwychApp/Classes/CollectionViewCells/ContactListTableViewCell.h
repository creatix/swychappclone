//
//  ContactListTableViewCell.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-21.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelContact.h"
@interface ContactListTableViewCell : UITableViewCell<UITextFieldDelegate>
@property(nonatomic,weak) IBOutlet UILabel *labTitle;
@property(nonatomic,weak) IBOutlet UITextField *txtContent;
@property(nonatomic,weak) IBOutlet UIImageView *ivArrow;
@property(strong,nonatomic) ModelContact *contact;
@property(strong,nonatomic) NSString *ContentOfContact;
@property(nonatomic) int icontactType;
@end
