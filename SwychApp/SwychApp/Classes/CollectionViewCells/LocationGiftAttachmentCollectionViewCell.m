//
//  LocationGiftAttachmentCollectionViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LocationGiftAttachmentCollectionViewCell.h"
#import "GiftAttachmentView.h"


@interface LocationGiftAttachmentCollectionViewCell()

@end

@implementation LocationGiftAttachmentCollectionViewCell

@synthesize showLocationView = _showLocationView;

-(void)setShowLocationView:(BOOL)showLocationView{
    _showLocationView = showLocationView;
    if(!showLocationView&&_showQuoteView){
        self.tvLocations.hidden = YES;
        self.cvAttachment.hidden = YES;
        self.ivQuote.hidden = NO;
        [self bringSubviewToFront:self.ivQuote];
        return;
    }
    if (showLocationView){
        if(!_showQuoteView){
        self.tvLocations.hidden = NO;
        self.cvAttachment.hidden = YES;
            self.ivQuote.hidden = YES;
        [self bringSubviewToFront:self.tvLocations];
        }
        else{
        self.tvLocations.hidden = YES;
        self.cvAttachment.hidden = YES;
        self.ivQuote.hidden = NO;
        [self bringSubviewToFront:self.ivQuote];
        }
    }
    else{
        self.tvLocations.hidden = YES;
        self.cvAttachment.hidden = NO;
        self.ivQuote.hidden = YES;
        [self bringSubviewToFront:self.cvAttachment];
    }
    self.tvLocations.userInteractionEnabled = YES;
    self.cvAttachment.userInteractionEnabled = YES;
}

-(BOOL)showLocationView{
    return _showLocationView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"LocationGiftAttachmentCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        self.userInteractionEnabled = YES;
        self.quotesFileNameArray = [NSArray arrayWithObjects:@"quote_treat.png",
                                    @"quote_awesome.png",
                                    @"quote_deserve.png",
                                    @"quote_happy.png",
                                    @"quote_monday.png",
                                    @"quote_tgif.png",
                                    @"quote_thanks.png",
                                    @"quote_when.png",
                                    nil];
        int quoteIndex = 0 + arc4random() % (self.quotesFileNameArray.count - 0);
        if (quoteIndex >= self.quotesFileNameArray.count){
            quoteIndex =(int)( self.quotesFileNameArray.count - 1);
        }
        self.ivQuote.image = [UIImage imageNamed:[self.quotesFileNameArray objectAtIndex:quoteIndex]];
        self.ivQuote.hidden = YES;
    }
    
    return self;
}


-(void)updateCell{
    self.tvLocations.hidden = !self.showLocationView;
    self.cvAttachment.hidden = self.showLocationView;
}
@end
