//
//  TransactionCollectionViewCell.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-01.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGiftcardView.h"
#import "UIRoundImageView.h"
@class UITransactionGiftcardView;
@interface TransactionCollectionViewCell : UICollectionViewCell
@property (nonatomic,weak)  UIImage *giftcardImage;
@property (nonatomic,strong)  UIImage *headImage;
@property (nonatomic,weak)  NSString *giftcardName;
@property (nonatomic,strong) ModelTransaction *giftcard;
@property (nonatomic,weak)  id<UITransactionGiftcardViewDelegate> giftcardViewDelegate;
@property (weak, nonatomic) IBOutlet UIImageView *ivGiftcard;
@property (weak, nonatomic) IBOutlet UILabel *labGiftcardName;
@property (weak, nonatomic) IBOutlet UIRoundImageView *ivHeadImg;
@property (weak, nonatomic) IBOutlet UILabel *labDate;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeadImg;
//@property(weak,nonatomic) IBOutlet UITransactionGiftcardView *giftcardView;
@end
