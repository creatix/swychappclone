//
//  GiftcardMallCollectionViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftcardMallCollectionViewCell.h"
#import "UIGiftcardView.h"



@implementation GiftcardMallCollectionViewCell

-(void) awakeFromNib{
    [super awakeFromNib];
    
}

-(void)setBounds:(CGRect)bounds{
    [super setBounds:bounds];
    
    self.contentView.frame = bounds;
    self.giftcardView.frame = bounds;
    [self.giftcardView updateViews];
}
@end
