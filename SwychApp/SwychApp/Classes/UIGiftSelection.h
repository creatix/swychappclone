//
//  UIGiftSelection.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UIGiftSelectionDelegate;

IB_DESIGNABLE
@interface UIGiftSelection : UIView{
    BOOL _giftForFriendSelected;
}

@property(assign,nonatomic) IBInspectable BOOL giftForFriendSelected;
@property(weak,nonatomic) id<UIGiftSelectionDelegate> selectionDelegate;
@property (nonatomic,assign) BOOL hideForMyselfSelection;

-(void)setTitleForFriend:(NSString*)titleFriend titleForMySelf:(NSString*)titleMySelf;
@end


@protocol UIGiftSelectionDelegate <NSObject>

-(void)giftSelectionChanged:(UIGiftSelection*)selectionView forFriend:(BOOL)forFriend;

@end