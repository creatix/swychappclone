//
//  GiftcardSelectionView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGiftcardView.h"
#import "CustomizedActionSheet.h"

@protocol GiftcardSelectionViewDelegate;

IB_DESIGNABLE
@interface GiftcardSelectionView : UIView<UISearchBarDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGiftcardViewDelegate,UITableViewDataSource,UITableViewDelegate,CustomizedActionSheetDelegate>{
}

@property (strong,nonatomic) NSArray *giftcards;

@property (weak,nonatomic) id<GiftcardSelectionViewDelegate> selectionViewDelegate;

@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UIView *vHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnHeaderSort;
@property (weak, nonatomic) IBOutlet UIButton *btnHeaderViewType;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btnHeaderFilter;
@property (weak, nonatomic) IBOutlet UICollectionView *cvGiftcards;
@property (weak, nonatomic) IBOutlet UITableView *tvGiftcards;
@property (nonatomic)  BOOL ifHiddenPrice;
@property (nonatomic) double amount;
-(void)refreshData;
-(void)setListViewAsCurrentView:(BOOL)listViewAsCurrent;
@end

@protocol GiftcardSelectionViewDelegate <NSObject>

@optional
-(void)giftcardSelectionView:(GiftcardSelectionView*)selectionView giftcardSelected:(ModelGiftCard*)giftcard;

@end
