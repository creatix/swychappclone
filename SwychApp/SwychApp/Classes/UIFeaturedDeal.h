//
//  UIFeaturedDeal.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIRoundCornerImageView;
@class TTTAttributedLabel;
@protocol UIFeaturedDealItemDelegate;

IB_DESIGNABLE
@interface UIFeaturedDeal : UIView
@property (weak, nonatomic) IBInspectable IBOutlet UIRoundCornerImageView *ivDeal;
@property (weak, nonatomic) IBInspectable IBOutlet TTTAttributedLabel *labDealName;
@property (weak, nonatomic) IBInspectable IBOutlet TTTAttributedLabel *labDealDescription;
@property (weak,nonatomic) id<UIFeaturedDealItemDelegate> featuredDealDelegate;
@end

@protocol UIFeaturedDealItemDelegate <NSObject>

-(void)featuredDealItemTapped:(UIFeaturedDeal*)sender;

@end