//
//  KeyboardHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "KeyboardHandler.h"
#import "SwychBaseViewController.h"

#define ToolbarHeight   40
#define KeyboardHeight      216

NSString *Noti_TouchOnView = @"Noti_TouchOnView";

@interface KeyboardHandler()
-(void)moveWindowUp:(NSNotification*)noti;
-(void)moveWindowDown:(NSNotification*)noti;
-(void)createAccessoryView;
-(void)addNotifications;
-(void)buttonTapped:(id)sender;
@end
@implementation KeyboardHandler

-(id)initWithWindow:(UIWindow *)window{
    if((self = [super init])){
        _window = window;
        [self createAccessoryView];
        [self addNotifications];
    }
    return self;
}

-(void)moveWindowUp:(NSNotification*)noti{
    if (_textView == nil && _textField == nil){
        return;
    }
    NSNumber *duration = nil;
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    if (noti != nil){
        NSValue *val = [[noti userInfo] objectForKey: UIKeyboardAnimationCurveUserInfoKey];
        [val getValue: &curve];
        duration = [[noti userInfo] objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    }
    else {
        duration = [NSNumber numberWithDouble:0.5];
    }
    [UIView beginAnimations: nil context: nil];
    
    
    CGFloat keyboardUpEdgeY = _keyboardCenter.y - _keyboardHeight / 2;
    CGPoint ptText;
    UIView *view;
    
    if (_textField != nil){
        if (_textField != nil && _textField.inputAccessoryView != nil){
            keyboardUpEdgeY -= _textField.inputAccessoryView.frame.size.height;
            if (_textField.autocorrectionType != UITextAutocorrectionTypeNo)
            {
                keyboardUpEdgeY -=_textField.inputAccessoryView.frame.size.height;
            }
        }
        
        ptText = _textField.frame.origin;
        
        view = _textField.superview;
    }
    else if (_textView != nil){
        if (_textView != nil && _textView.inputAccessoryView != nil){
            keyboardUpEdgeY -= _textView.inputAccessoryView.frame.size.height;
            if (_textView.autocorrectionType != UITextAutocorrectionTypeNo)
            {
                keyboardUpEdgeY -=_textView.inputAccessoryView.frame.size.height;
            }
        }
        
        ptText = _textView.frame.origin;
        
        view = _textView.superview;
    }
    
    CGPoint ptTextNew = [_window convertPoint:ptText fromView:view];
    if (_textField != nil){
        if (ptTextNew.y + _textField.bounds.size.height> keyboardUpEdgeY)
        {
            _offsetY = ptTextNew.y + _textField.bounds.size.height - keyboardUpEdgeY;
            
            //move window up
            CGRect rc = CGRectMake(0, _offsetY * (-1), _window.bounds.size.width, _window.bounds.size.height);
            _window.frame = rc;
        }
    }
    else{
        if (ptTextNew.y + _textView.bounds.size.height> keyboardUpEdgeY)
        {
            _offsetY = ptTextNew.y + _textView.bounds.size.height - keyboardUpEdgeY;
            
            //move window up
            CGRect rc = CGRectMake(0, _offsetY * (-1), _window.bounds.size.width, _window.bounds.size.height);
            _window.frame = rc;
        }
    }
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView commitAnimations];

}
-(void)moveWindowDown:(NSNotification*)noti{
    NSNumber *duration = nil;
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    if (noti != nil){
        NSValue *val = [[noti userInfo] objectForKey: UIKeyboardAnimationCurveUserInfoKey];
        [val getValue: &curve];
        duration = [[noti userInfo] objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    }
    else {
        duration = [NSNumber numberWithDouble:0.5];
    }
    [UIView beginAnimations: nil context: nil];
    
    if (_window != nil && _offsetY > 0){
        CGRect rc = CGRectMake(0, 0, _window.bounds.size.width, _window.bounds.size.height);
        _window.frame = rc;
        _offsetY = 0;
    }
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView commitAnimations];
    
}

-(void)createAccessoryView{
    _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,0, ToolbarHeight)];
    
    UIBarButtonItem *separater = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    _doneButtonItem  = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Button_Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(buttonTapped:)];
    
    NSArray *buttons = [NSArray arrayWithObjects:separater,_doneButtonItem, nil];
    _toolbar.items = buttons;
}

-(void)buttonTapped:(id)sender{
    if (_textView == nil && _textField == nil){
        return;
    }
    if (sender == _doneButtonItem){
        id inputField = _textField != nil ? _textField : _textView;
        SwychBaseViewController *ctrl = [self getTextFieldViewController:inputField];
        if (ctrl != nil && [ctrl respondsToSelector:@selector(getNextInputTextField:)]){
            id next = [ctrl getNextInputTextField:inputField];
            [inputField resignFirstResponder];
            if (next){
                [next becomeFirstResponder];
            }
        }
        else{
            [inputField resignFirstResponder];
        }
        if (ctrl != nil && [ctrl respondsToSelector:@selector(KeyboardDoneClicked)]){
            [ctrl KeyboardDoneClicked];
        }
    }
    
}

-(void)addNotifications{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(touchOnView:) name:Noti_TouchOnView object:nil];
    
    [center addObserver:self selector:@selector(textEditBegin:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [center addObserver:self selector:@selector(textEditEnd:) name:UITextFieldTextDidEndEditingNotification object:nil];
    
    [center addObserver:self selector:@selector(textViewEditDidBegin:) name:UITextViewTextDidBeginEditingNotification object:nil];
    [center addObserver:self selector:@selector(textViewEditDidEnd:) name:UITextViewTextDidEndEditingNotification object:nil];
    
    [center addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
}
-(void)removeNotifications{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:Noti_TouchOnView object:nil];
    
    [center removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
    [center removeObserver:self name:UITextFieldTextDidEndEditingNotification object:nil];
    
    [center removeObserver:self name:UITextViewTextDidBeginEditingNotification object:nil];
    [center removeObserver:self name:UITextViewTextDidEndEditingNotification object:nil];
    
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)touchOnView:(id)sender{
    if (_textField != nil){
        [_textField resignFirstResponder];
    }
    if (_textView != nil){
        [_textView resignFirstResponder];
    }
}


-(SwychBaseViewController*)getTextFieldViewController:(id)textField{
    id nextResponder = [textField nextResponder];
    if ([nextResponder isKindOfClass:[SwychBaseViewController class]]){
        return  (SwychBaseViewController*)nextResponder;
    }
    else if ([nextResponder isKindOfClass:[UIView class]]){
        return [self getTextFieldViewController:nextResponder];
    }
    else{
        return nil;
    }
}

#pragma mark KeyboardNotification handlers

-(void) keyboardWillShow: (NSNotification*) notification
{
    if (_window != nil){
        NSValue *val = [[notification userInfo] objectForKey:  UIKeyboardFrameBeginUserInfoKey];
        CGRect frame ;
        [val getValue: &frame];
        
        _keyboardHeight =KeyboardHeight;
        _keyboardCenter = CGPointMake(_window.frame.size.width / 2.0, _window.frame.size.height - _keyboardHeight / 2.0);
        
        [self moveWindowUp:notification];
        
        _keyboardShowing = YES;
    }
}

-(void) keyboardDidShow: (NSNotification*) notification{
}

-(void) keyboardWillHide: (NSNotification*) notification
{
    [self moveWindowDown:notification];
    _keyboardShowing = false;
}

-(void)textViewEditDidBegin:(NSNotification*)notification{
    UITextView *textView = [notification object];
    _textView = textView;
    
    [_doneButtonItem setTitle:NSLocalizedString(@"Button_Done", nil)];
    
    _textView.inputAccessoryView = _toolbar;
    if (_keyboardShowing){
        [self moveWindowUp:nil];
    }
}

-(void)textViewEditDidEnd:(NSNotification*)notification{
    _textView = nil;
}

-(void) textEditBegin:(NSNotification*) notification
{
    UITextField *field = [notification object];
    _textField = field;
    
    SwychBaseViewController *ctrl = [self getTextFieldViewController:_textField];
    if (ctrl != nil && [ctrl respondsToSelector:@selector(getNextInputTextField:)]){
        UITextField *next = [ctrl getNextInputTextField:_textField];
        if (next){
            [_doneButtonItem setTitle:NSLocalizedString(@"Button_Next", nil)];
        }
        else{
            [_doneButtonItem setTitle:NSLocalizedString(@"Button_Done", nil)];
        }
    }

    _textField.inputAccessoryView = _toolbar;
    if (_keyboardShowing){
        [self moveWindowUp:nil];
    }
}

-(void) textEditEnd:(NSNotification*) notification
{
    _textField = nil;
}

@end
