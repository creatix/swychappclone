//
//  CreditPointsManager.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CreditPointsManager.h"
#import "ModelCreditPointsForPurchase.h"
#import "LocalUserInfo.h"
#import "ModelGiftCard.h"
#import "ModelTransaction.h"

@implementation CreditPointsManager


+(CreditPointsManager*)instance{
    static CreditPointsManager *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}

+(double)getValueByPoints:(NSInteger)points{
    return (double)points * 0.01;
}

+(NSInteger)getPointsByValue:(double)value{
    return (NSInteger)(value * 100);
}


-(ModelCreditPointsForPurchase*)getCreditPointsForPurchase:(double)purchaseAmount credits:(NSArray<ModelTransaction*>*)credits promotionDiscount:(double)discount{
    ModelCreditPointsForPurchase *ret = [[ModelCreditPointsForPurchase alloc] init];
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (user == nil || (user.swychBalance < 0.01 && user.swychPointValue < 0.01)){
        ret.creditUpTo = 0.0;
        ret.pointsUpTo = 0;
        
        return  ret;
    }
    double remainingBalance = purchaseAmount - discount;
    ret.creditUpTo = [self getMaximumCredit:credits purchaseAmount:remainingBalance];
    if (user.swychPointValue > 0.0 && remainingBalance > 0.0){
        if (user.swychPointValue < remainingBalance){
            ret.pointsUpTo = user.swychPoint;
        }
        else{
            
            ret.pointsUpTo = (int)(remainingBalance * 100);
        }
    }
    else{
        ret.pointsUpTo = 0;
    }
    
    
    
    return ret;
}

-(double)getMaximumCredit:(NSArray<ModelTransaction*>*)credits purchaseAmount:(double)purchaseAmount{
    double max = 0;
    NSArray *sortedCredits = [credits sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        ModelTransaction *card1 = (ModelTransaction*)obj1;
        ModelTransaction *card2 = (ModelTransaction*)obj2;
        
        return card1.amount > card2.amount;
    }];
    double maxTemp = 0;
    for(NSInteger idx1 = 0; idx1 < [sortedCredits count]; idx1 ++){
        if (maxTemp >= max){
            max = maxTemp;
        }
        ModelTransaction *card1 = [sortedCredits objectAtIndex:idx1];
        if (card1.amount > purchaseAmount){
            break;
        }
        maxTemp = card1.amount;
        for (NSInteger idx2 = idx1 + 1; idx2 < [sortedCredits count]; idx2++){
            ModelTransaction *card2 = [sortedCredits objectAtIndex:idx2];
            if (maxTemp + card2.amount > purchaseAmount){
                break;
            }
            maxTemp += card2.amount;
        }
    }
    if (maxTemp > 0 && max < 0.01){
        max = maxTemp;
    }
    return max;
}
@end
