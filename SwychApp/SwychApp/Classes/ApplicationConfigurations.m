//
//  ApplicationConfigurations.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ApplicationConfigurations.h"
#import <UIKit/UIKit.h>
#import "ApplicationConfigurationsConstants.h"
#import "MenuItem.h"
#import "TabbarItem.h"
#import "LocalUserInfo.h"
#import "ObjectConfigInfo.h"
#import "HelpMenuItem.h"
#import "MenuItemWithPosition.h"
#import "ModelPaymentMethodSettings.h"
#import "ModelSystemConfiguration.h"
#import "ModelPaymentProcessor.h"
#import "ModelApplePaySettings.h"
#import "AnalyticSettings.h"
#import "AnalyticAdjustSettings.h"
#import "AnalyticsAdjust.h"
#import "AnalyticsShareASale.h"
#import "AnalyticShareASaleSettings.h"




@interface ApplicationConfigurations(){
    LocalUserInfo *_localUser;
}
-(id)getValueForKey:(NSString*) key viewController:(NSString*)viewControllerNameame;

@end


@implementation ApplicationConfigurations

+(ApplicationConfigurations*)instance{
    static ApplicationConfigurations *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}

-(NSString *)getSubClassName:(NSString*)parentName{
    assert(parentName);
    if (_dictResources == nil ){
        return parentName;
    }
    NSDictionary *dictClassMappings = [_dictResources objectForKey:KEY_CLASS_MAPPINGS];
    if (dictClassMappings == nil){
        return parentName;
    }
    NSDictionary *dictClassXIB = [dictClassMappings objectForKey:parentName];
    if (dictClassXIB == nil){
        return parentName;
    }
    else{
        NSString *subClassName = [dictClassXIB objectForKey:KEY_CLASS_MAPPINGS_CLASS];
        return subClassName == nil ? parentName : subClassName;
    }
}


-(NSString *)getSubClassNibName:(NSString*)parentName{
    assert(parentName);
    if (_dictResources == nil ){
        return nil;
    }
    NSDictionary *dictClassMappings = [_dictResources objectForKey:KEY_CLASS_MAPPINGS];
    if (dictClassMappings == nil){
        return nil;
    }
    NSDictionary *dictClassXIB = [dictClassMappings objectForKey:parentName];
    if (dictClassXIB == nil){
        return nil;
    }
    else{
        NSString *subClassNibName = [dictClassXIB objectForKey:KEY_CLASS_MAPPINGS_XIB];
        return subClassNibName;
    }
    return nil;
}

-(id)init{
    if (self = [super init]){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ApplicationConfigurations" ofType:@"plist"];
        if (path != nil){
            _dictResources = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
            _dictGeneral = [_dictResources objectForKey:KEY_GeneralSettings];
            _dictIndividual = [_dictResources objectForKey:KEY_IndividualViewControllers];
        }
    }
    return self;
}
+(UIColor*)getColorFromString:(NSString*)value{
    if (value == nil){
        return nil;
    }
    NSArray *array = [value componentsSeparatedByString:@","];
    if ([array count] == 3 || [array count] == 4){
        return [UIColor colorWithRed:[(NSString*)[array objectAtIndex:0] floatValue]/255.0
                               green:[(NSString*)[array objectAtIndex:1] floatValue]/255.0
                                blue:[(NSString*)[array objectAtIndex:2] floatValue]/255.0
                               alpha:[array count] == 3 ? 1.0 : [(NSString*)[array objectAtIndex:3] floatValue]];
    }
    else{
        return  nil;
    }
}

+(UIColor*)getColorFromDictionary:(NSDictionary*)dict{
    NSNumber *red = (NSNumber*)[dict objectForKey:KEY_COLOR_Red];
    NSNumber *green = (NSNumber*)[dict objectForKey:KEY_COLOR_Green];
    NSNumber *blue = (NSNumber*)[dict objectForKey:KEY_COLOR_Blue];
    NSNumber *alpha = (NSNumber*)[dict objectForKey:KEY_COLOR_Alpha];
    if(red == nil || green == nil || blue == nil){
        return nil;
    }
    UIColor *clr = [UIColor colorWithRed:[red intValue]/255.0 green:[green intValue] / 255.0 blue:[blue intValue] / 255.0 alpha:alpha == nil ? 1.0 : [alpha floatValue]];
    return clr;
}


-(id)getValueForKey:(NSString*) key viewController:(NSString*)viewControllerName{
    id value = nil;
    if (_dictIndividual != nil){
        if ([key compare:viewControllerName] == NSOrderedSame){
            value = [_dictIndividual objectForKey:key];
        }
        else{
            id dict = [_dictIndividual objectForKey:viewControllerName];
            value = dict == nil ? nil : [dict objectForKey:key];
        }
    }
    if (value == nil){
        if (_dictGeneral != nil){
            if ([key compare:viewControllerName] == NSOrderedSame){
                value = [_dictGeneral objectForKey:key];
            }
            else{
                id dict = [_dictIndividual objectForKey:viewControllerName];
                value = dict == nil ? nil : [dict objectForKey:key];
            }
        }
    }
    if (value == nil){
        if (_dictResources != nil){
            if ([key compare:viewControllerName] == NSOrderedSame){
                value = [_dictResources objectForKey:key];
            }
            else{
                id dict = [_dictResources objectForKey:viewControllerName];
                value = dict == nil ? nil : [dict objectForKey:key];
            }
        }
    }
    return value;
}

-(CGFloat)getViewWidth:(NSString*)viewControllerName{
    NSString *val =(NSString*) [self getValueForKey:KEY_Width viewController:viewControllerName];
    return val == nil ? 0.0 : [val floatValue];
}

-(CGFloat)getViewHeight:(NSString*)viewControllerName{
    NSString *val = (NSString*)[self getValueForKey:KEY_Height viewController:viewControllerName];
    return val == nil ? 0.0 : [val floatValue];
}

-(NSString*)getServerURL{
    NSDictionary *dict =[_dictResources objectForKey:KEY_SERVER_URL];
    if (dict == nil){
        return nil;
    }
    NSString *url = nil;
#ifdef BUILD_PROD
    url = [dict objectForKey:KEY_PROD];
#elif BUILD_QA
    url = [dict objectForKey:KEY_QA];
#elif BUILD_UAT
    url = [dict objectForKey:KEY_UAT];
#else
    url = [dict objectForKey:KEY_DEV];
#endif
    return url;
}

-(ModelPaymentMethodSettings*)getPaymentMethodSettings{
    ModelPaymentMethodSettings *settings = [[ModelPaymentMethodSettings alloc] init];
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    
    
#ifdef BUILD_PROD
    settings.applePayMerchantIdentifier = lui.sysConfig.applePaySettings.merchantIdentifierLive;
    settings.applePayStripePublishableKey = lui.sysConfig.applePaySettings.stripePublishableKeyLive;
#elif BUILD_QA
    settings.applePayMerchantIdentifier = lui.sysConfig.applePaySettings.merchantIdentifierLive;
    settings.applePayStripePublishableKey = lui.sysConfig.applePaySettings.stripePublishableKeyLive;
#elif BUILD_UAT
    settings.applePayMerchantIdentifier = lui.sysConfig.applePaySettings.merchantIdentifierLive;
    settings.applePayStripePublishableKey = lui.sysConfig.applePaySettings.stripePublishableKeyLive;
#else
    settings.applePayMerchantIdentifier = lui.sysConfig.applePaySettings.merchantIdentifierSandbox;
    settings.applePayStripePublishableKey = lui.sysConfig.applePaySettings.stripePublishableKeySandbox;
#endif

    NSDictionary *dictMethods = [_dictResources objectForKey:KEY_PaymentMethods];
    if (dictMethods == nil){
        return settings;
    }

    //Paypal Settings
    NSDictionary *dict1 = [dictMethods objectForKey:KEY_Payment_PayPal];
    settings.payPalMerchantProductionKey = [dict1 objectForKey:KEY_Payment_PayPal_MerchantKey_PROD];
    settings.payPalMerchantTestKey = [dict1 objectForKey:KEY_Payment_PayPal_MerchantKey_TEST];
    NSDictionary *dict2 = nil;
#ifdef BUILD_PROD
    dict2 = [dict1 objectForKey:KEY_PROD];
#elif BUILD_QA
    dict2 = [dict1 objectForKey:KEY_QA];
#elif BUILD_UAT
    dict2 = [dict1 objectForKey:KEY_UAT];
#else
    dict2 = [dict1 objectForKey:KEY_DEV];
#endif

    if (dict2 != nil){
        settings.paypalKeyIsProdKey = [[dict2 objectForKey:KEY_Payment_IsProdKey] boolValue];
    }
    else{
        settings.paypalKeyIsProdKey = NO;
    }
    
    //DirectCreditCard
    dict1 = [dictMethods objectForKey:KEY_Payment_DirectCreditCard];
    NSString *url = nil;
#ifdef BUILD_PROD
    url = [dict1 objectForKey:KEY_PROD];
#elif BUILD_QA
    url = [dict1 objectForKey:KEY_QA];
#elif BUILD_UAT
    url = [dict1 objectForKey:KEY_UAT];
#else
    url = [dict1 objectForKey:KEY_DEV];
#endif
    if (url != nil){
        NSString *serverURL = [self getServerURL];
        if ([[serverURL substringFromIndex:serverURL.length - 1] isEqualToString:@"/"]){
            settings.directCreditCardURL = [NSString stringWithFormat:@"%@%@",serverURL,url];
        }
        else{
            settings.directCreditCardURL = [NSString stringWithFormat:@"%@/%@",serverURL,url];
        }
    }
    
    //AmazonPay
    dict1 = [dictMethods objectForKey:KEY_Payment_AmazonPay];
    if (dict1 != nil){
        NSDictionary *dict2 = nil;
#ifdef BUILD_PROD
        dict2 = [dict1 objectForKey:KEY_PROD];
#elif BUILD_QA
        dict2 = [dict1 objectForKey:KEY_QA];
#elif BUILD_UAT
        dict2 = [dict1 objectForKey:KEY_UAT];
#else
        dict2 = [dict1 objectForKey:KEY_DEV];
#endif
        settings.amazonAWSAccessKeyId = [dict2 objectForKey:KEY_Payment_AmazonPay_AWSAccessKeyId];
        settings.amazonIsLiveEnv = [[dict2 objectForKey:KEY_Payment_IsProdKey] boolValue];
    }
    
    //AmazonPay
    dict1 = [dictMethods objectForKey:KEY_Payment_BrainTree];
    if (dict1 != nil){
        NSDictionary *dict2 = nil;
#ifdef BUILD_PROD
        dict2 = [dict1 objectForKey:KEY_PROD];
#elif BUILD_QA
        dict2 = [dict1 objectForKey:KEY_QA];
#elif BUILD_UAT
        dict2 = [dict1 objectForKey:KEY_UAT];
#else
        dict2 = [dict1 objectForKey:KEY_DEV];
#endif
        settings.brainTreeClientId = [dict2 objectForKey:KEY_Payment_BrainTree_ClientKeyId];
    }
    
    //BrainTree
    return settings;
}

-(NSString*)getConnectionClientID{
    return _dictResources == nil ? nil : [_dictResources objectForKey:KEY_ClientID];
}

-(NSString*)getConnectionApiKey{
    return _dictResources == nil ? nil : [_dictResources objectForKey:KEY_ApiKey];
}


-(UIColor*)getTextColor:(NSString*)viewControllerName{
    NSString *clrString = (NSString*)[self getValueForKey:KEY_Text_Color viewController:viewControllerName];
    return [ApplicationConfigurations getColorFromString:clrString];
}

-(UIColor*)getPlaceHolderColor:(NSString*)viewControllerName{
    NSString *clrString = (NSString*)[self getValueForKey:KEY_PlaceHolder_Color viewController:viewControllerName];
    return [ApplicationConfigurations getColorFromString:clrString];
}

-(UIColor*)getViewBackgroundColor:(NSString*)viewControllerName{
    NSString *clrString = (NSString*)[self getValueForKey:KEY_BackgroundColor viewController:viewControllerName];
    return [ApplicationConfigurations getColorFromString:clrString];
}

-(ObjectConfigInfo*)getButtonConfiguration:(NSString*)viewControllerName{
    id val = [self getValueForKey:KEY_BUTTON viewController:viewControllerName];
    if (val != nil && [val isKindOfClass:[NSDictionary class]]){
        NSDictionary *dictButton = (NSDictionary*)val;
        ObjectConfigInfo *config = [[ObjectConfigInfo alloc] init];
        //Background color
        if ([dictButton objectForKey:KEY_BackgroundColor] != nil){
            config.backgroundColor = [ApplicationConfigurations getColorFromString:[dictButton objectForKey:KEY_BackgroundColor]];
        }
        else{
            config.backgroundColor = nil;
        }
        //TextColor
        if ([dictButton objectForKey:KEY_Text_Color] != nil){
            config.textColor = [ApplicationConfigurations getColorFromString:[dictButton objectForKey:KEY_Text_Color]];
        }
        else{
            config.textColor = nil;
        }
        //Height
        if ([dictButton objectForKey:KEY_Height] != nil){
            config.fixedHeight = [[dictButton objectForKey:KEY_Height] floatValue];
        }
        else{
            config.fixedHeight = 0.0;
        }
        //Font
        NSString *fontName = [dictButton objectForKey:KEY_FONT_NAME];
        NSString *fontSize = [dictButton objectForKey:KEY_FONT_SIZE];
        if (fontName != nil && fontSize != nil){
            config.font = [UIFont fontWithName:fontName size:[fontSize floatValue]];
        }
        else{
            config.font = nil;
        }
        return config;
    }
    else{
        return nil;
    }
}


-(ObjectConfigInfo*)getConfiguration:(NSString*)viewControllerName object:(UIView*)obj{
    ObjectConfigInfo *config = [[ObjectConfigInfo alloc] init];
    
    if (_dictGeneral != nil){
        NSDictionary *dict =  [self getSettingsForObjec:obj fromDictionary:_dictGeneral];
        [config setProperties:dict];
    }
    
    if (_dictIndividual != nil && [_dictIndividual objectForKey:viewControllerName] != nil){
        if ([_dictIndividual objectForKey:viewControllerName] != nil){
            NSDictionary *dict = (NSDictionary*)[_dictIndividual objectForKey:viewControllerName];
            NSDictionary *dictAll = [self getSettingsForObjec:obj fromDictionary:dict];
            [config setProperties:dictAll];
            
             NSDictionary *dictControls = [dict objectForKey:KEY_CONTROLS];
            if (dictControls != nil){
                NSString *objID = obj.restorationIdentifier;
                if (objID != nil ){
                    NSDictionary *dictObj = [dictControls objectForKey:objID];
                    [config setProperties:dictObj];
                }
            }
        }
    }
    
    if (config.hasPropertySettings){
        return config;
    }
    else{
        return nil;
    }

}

-(NSDictionary*)getSettingsForObjec:(UIView*)obj fromDictionary:(NSDictionary*)dictFrom{
    NSDictionary *dict = nil;
    if ([obj isKindOfClass:[UILabel class]]){
        dict = [dictFrom objectForKey:KEY_LABEL];
    }
    else if ([obj isKindOfClass:[UITextField class]]){
        dict = [dictFrom objectForKey:KEY_TEXTFIELD];
    }
    else if ([obj isKindOfClass:[UITextView class]]){
        dict = [dictFrom objectForKey:KEY_TEXTVIEW];
    }
    else if ([obj isKindOfClass:[UIButton class]]){
        dict = [dictFrom objectForKey:KEY_BUTTON];
    }
    else{
        dict = nil;
    }
    return dict;
}

-(UIImage*)getAvatarImage{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    UIImage *img = [[LocalStorageManager instance] getUserAvatarImage];
    if (img == nil){
        return [UIImage imageNamed:@"default_avatar.png"];
    }
    else{
        return img;
    }
}

-(NSString*)getUserFullName{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    return [localUser getUserFullName];
}


-(LocalUserInfo*)getLocalStoredUserInfo{
    if(_localUser == nil){
        _localUser = [[LocalUserInfo alloc] init];
    }
    return _localUser;
}

-(AnalyticSettings*)getAnalyticSettings:(NSString*)name{
    NSDictionary *dict =[_dictResources objectForKey:KEY_ANALYTICS];
    if (dict == nil){
        return nil;
    }
    if ([name compare:NSStringFromClass([AnalyticsAdjust class])] == NSOrderedSame){
        AnalyticAdjustSettings *settings = nil;
        NSDictionary *dictAdjust =[dict objectForKey:name];
        if (dictAdjust == nil){
            return nil;
        }
        NSDictionary *dict2 = nil;
#ifdef BUILD_PROD
        dict2 = [dictAdjust objectForKey:KEY_PROD];
#elif BUILD_QA
        dict2 = [dictAdjust objectForKey:KEY_QA];
#elif BUILD_UAT
        dict2 = [dictAdjust objectForKey:KEY_UAT];
#else
        dict2 = [dictAdjust objectForKey:KEY_DEV];
#endif
        if (dict2 != nil){
            settings = [[AnalyticAdjustSettings alloc] init];
            settings.appToken = [dict2 objectForKey:KEY_ANALYTICS_Adjust_AppToken];
            settings.isTestEvn = [[dict2 objectForKey:KEY_AnaLYTICS_Adjust_TextEnv] boolValue];
        }
        else{
            return nil;
        }
        return settings;
    }
    else if ([name compare:NSStringFromClass([AnalyticsShareASale class])] == NSOrderedSame){
        AnalyticShareASaleSettings *settings = nil;
        NSDictionary *dictShareASale =[dict objectForKey:name];
        if (dictShareASale == nil){
            return nil;
        }
        NSDictionary *dict2 = nil;
#ifdef BUILD_PROD
        dict2 = [dictShareASale objectForKey:KEY_PROD];
#elif BUILD_QA
        dict2 = [dictShareASale objectForKey:KEY_QA];
#elif BUILD_UAT
        dict2 = [dictShareASale objectForKey:KEY_UAT];
#else
        dict2 = [dictShareASale objectForKey:KEY_DEV];
#endif
        if (dict2 != nil){
            settings = [[AnalyticShareASaleSettings alloc] init];
            settings.merchantID = [dict2 objectForKey:KEY_ANALYTICS_ShareASale_MerchantID];
            settings.appID = [dict2 objectForKey:KEY_ANALYTICS_ShareASale_AppID];
            settings.appKey = [dict2 objectForKey:KEY_ANALYTICS_ShareASale_AppKey];
            settings.isTestEvn = [[dict2 objectForKey:KEY_ANALYTICS_ShareASale_TextEnv] boolValue];
        }
        else{
            return nil;
        }
        return settings;
    }
    return nil;
}

#pragma
#pragma mark Sliding Menu
-(CGFloat)getSlidingMenuViewWidth{
    return [self getViewWidth:KEY_ViewController_SlidingMenu];
}

-(NSArray*)getSlidingMenuItems{
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    NSString *slidingMenuItemsKey = nil;
    if (localUser.userStatus == UserStatus_PendingOTPVerified){
        slidingMenuItemsKey =KEY_VC_SlidingMenu_Items_PendingUser;
    }
    else{
        slidingMenuItemsKey = KEY_VC_SlidingMenu_Items;
    }
    id a = [self getValueForKey:slidingMenuItemsKey viewController:KEY_ViewController_SlidingMenu];
    if (a != nil && [a isKindOfClass:[NSArray class]]){
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in (NSArray*)a){
            MenuItem *item = [[MenuItem alloc] init];
            item.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            item.iconImageName = [dict objectForKey:KEY_MENU_ITME_Icon];
            item.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            item.sortOrder = [[dict objectForKey:KEY_MENU_ITEM_Order] intValue];
            item.debugOnly = [[dict objectForKey:KEY_MENU_ITEM_DegugOnly] boolValue];
            if(item.tag == 1 || item.tag == 5) continue; // hide payment method and archive
            LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
            if(!localUser.sysConfig.referFriendOnMenuEnabled){
                if(item .tag ==  3) continue;
            }
#if DEBUG
            [array addObject:item];
#else
            if (!item.debugOnly){
                [array addObject:item];
            }
#endif
        }
        NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
            return ((MenuItem*)a).sortOrder > ((MenuItem*)b).sortOrder;
        }];
        return sortedArray;
    }
    return nil;
}

#pragma
#pragma mark - Tabbar
-(UIColor*)getTabbarBackgroundColor{
    NSString *clrString = (NSString*)[self getValueForKey:KEY_BackgroundColor viewController:KEY_TabbarSettings];
    return [ApplicationConfigurations getColorFromString:clrString];
}

-(NSArray*)getTabbarItemInfo{
    id a = [self getValueForKey:KEY_TabbarItems viewController:KEY_TabbarSettings];
    if (a != nil && [a isKindOfClass:[NSArray class]]){
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in (NSArray*)a){
            TabbarItem *item = [[TabbarItem alloc] init];
            item.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            item.iconImageName = [dict objectForKey:KEY_MENU_ITME_Icon];
            item.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            item.sortOrder = [[dict objectForKey:KEY_MENU_ITEM_Order] intValue];
            
            [array addObject:item];
        }
        NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
            return ((TabbarItem*)a).sortOrder > ((TabbarItem*)b).sortOrder;
        }];
        return sortedArray;
    }
    return nil;
    
}

#pragma
#pragma mark - Help Menu

-(NSString*)getHelpURL{
    return [self getValueForKey:@"HelpURL" viewController:@"HelpViewController"];
}

- (NSArray*) getHelpMenuItems {
    id a = [self getValueForKey:@"MenuItems" viewController:@"HelpViewController"];
    if(a != nil && [a isKindOfClass:[NSArray class]]) {
        NSMutableArray* array = [NSMutableArray new];
        for(NSDictionary* dict in (NSArray*)a) {
            HelpMenuItem* menuItem = [HelpMenuItem new];
            menuItem.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            menuItem.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            
            [array addObject:menuItem];
        }
        return array;
    }
    return nil;
}

#pragma
#pragma mark - Payment Menu
- (NSArray*) getPaymentMenuItems {
    id a = [self getValueForKey:@"MenuItems" viewController:@"PaymentViewController"];
    if(a != nil && [a isKindOfClass:[NSArray class]]) {
        NSMutableArray* array = [NSMutableArray new];
        for(NSDictionary* dict in (NSArray*)a) {
            MenuItemWithPosition* menuItem = [MenuItemWithPosition new];
            menuItem.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            menuItem.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            menuItem.sortOrder =[[dict objectForKey:KEY_MENU_ITEM_Order] intValue];
            menuItem.iconImageName = [dict objectForKey:KEY_MENU_ITME_Icon];
            menuItem.iconPosition = (IconPosition)[[dict objectForKey:KEY_MENU_ITEM_WITH_POSITION] intValue];
            
            [array addObject:menuItem];
        }
        return array;
    }
    return nil;
}

#pragma
#pragma mark - Settings Menu

- (NSArray*) getConnectedAccountsMenuItems {
    id a = [self getValueForKey:@"ConnectedAccountsMenuItems" viewController:@"SettingsViewController"];
    if(a != nil && [a isKindOfClass:[NSArray class]]) {
        NSMutableArray* array = [NSMutableArray new];
        for(NSDictionary* dict in (NSArray*)a) {
            HelpMenuItem* menuItem = [HelpMenuItem new];
            menuItem.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            menuItem.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            menuItem.iconImageName = [dict objectForKey:KEY_MENU_ITME_Icon];
            
            [array addObject:menuItem];
        }
        return array;
    }
    return nil;
}

- (NSArray*) getTouchIdMenuItems {
    id a = [self getValueForKey:@"TouchIdMenuItems" viewController:@"SettingsViewController"];
    if(a != nil && [a isKindOfClass:[NSArray class]]) {
        NSMutableArray* array = [NSMutableArray new];
        for(NSDictionary* dict in (NSArray*)a) {
            HelpMenuItem* menuItem = [HelpMenuItem new];
            menuItem.text = [dict objectForKey:KEY_MENU_ITEM_Text];
            menuItem.tag = [[dict objectForKey:KEY_MENU_ITME_Tag] intValue];
            
            [array addObject:menuItem];
        }
        return array;
    }
    return nil;
}

@end

