//
//  ContactDetailView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizedInputView.h"
#import "CustomizedInputView.h"
@class ModelContact;
@protocol ContactDetailViewDelegate;

@interface ContactDetailView : UIView<UITableViewDelegate,UITableViewDataSource,CustomizedInputViewDelegate,UIGestureRecognizerDelegate>

@property(strong,nonatomic) ModelContact *contact;
@property(weak,nonatomic) IBOutlet UITableView *tvList;
@property(weak,nonatomic) IBOutlet UILabel *lblBirthday;
@property(weak,nonatomic) IBOutlet UILabel *lblBirthdayTitle;
@property(weak,nonatomic) IBOutlet UIImageView *imgBirthdayBg;
@property(weak,nonatomic) IBOutlet id<ContactDetailViewDelegate> detailViewDelegate;
@property(strong,nonatomic)  NSString *BirthdayText;
@end


@protocol ContactDetailViewDelegate <NSObject>

@optional
-(void)contactDetailView:(ContactDetailView*)detailView contact:(ModelContact*)contact selected:(NSString*)selectedItem isPhoneNumber:(BOOL)isPhone;

@end