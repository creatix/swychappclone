//
//  UIRoundCornerImageView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIRoundCornerImageView.h"


@interface UIRoundCornerImageView()

@property(nonatomic,strong) UITapGestureRecognizer *tap;
@end

@implementation UIRoundCornerImageView
@synthesize cornerRadius = _cornerRadius;
@synthesize imageViewDelegate = _imageViewDelegate;

-(void)setImageViewDelegate:(id<UIRoundCornerImageViewDelegate>)imageViewDelegate{
    _imageViewDelegate = imageViewDelegate;
    if (self.tap == nil){
        self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
        self.tap.numberOfTapsRequired = 1;
        self.tap.numberOfTouchesRequired = 1;
        [self.tap setCancelsTouchesInView:NO];
        [self addGestureRecognizer:self.tap];
        self.userInteractionEnabled = YES;
    }
}
-(id<UIRoundCornerImageViewDelegate>)imageViewDelegate{
    return _imageViewDelegate;
}

-(CGFloat)cornerRadius{
    return _cornerRadius;
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = _cornerRadius;
    self.layer.masksToBounds = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)touchOnView:(id)sender{
    if (self.imageViewDelegate != nil && [self.imageViewDelegate respondsToSelector:@selector(roundCornerImageViewTapped:)]){
        [self.imageViewDelegate roundCornerImageViewTapped:self];
    }
}

@end
