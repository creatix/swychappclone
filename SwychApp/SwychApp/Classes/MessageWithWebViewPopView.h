//
//  ChangePasswordPopupView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"
#import "ModelGiftCard.h"
@protocol MessageWithWebViewPopViewDelegate;
@interface MessageWithWebViewPopView : PopupConfirmationView
@property (strong, nonatomic)  NSString *fileName;
@property (strong, nonatomic)  NSString *Html;
@property (weak, nonatomic)  ModelGiftCard *giftcard;
@property (assign,nonatomic) id<MessageWithWebViewPopViewDelegate> uDelegate;
-(void)AddUrl:(NSString*) fileName;
-(void)AddUrl:(NSString*) fileName title:(NSString*)title;
-(void)AddUrl:(NSString*) fileName title:(NSString*)title type:(BOOL)type;
@end
@protocol MessageWithWebViewPopViewDelegate <NSObject>

-(void)NextStep:(BOOL)next obj:(id)obj tag:(int)itag;

@end
