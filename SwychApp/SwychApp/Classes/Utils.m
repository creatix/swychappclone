//
//  Utils.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "Utils.h"
#import "UITextField+Extra.h"
#import "AppDelegate.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"
#import "UIDevice+IdentifierAddition.h"
#import "ModelTransaction.h"
#import "ModelGiftCard.h"
#import "ModelCustomizedActionSheetItem.h"
#import "ModelFilterOption.h"

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#define DEVICE_TYPE_ANDROID     @"1"
#define DEVICE_TYPE_IOS         @"2"
#define DEVICE_TYPE_BLACKBERRY  @"3"
#define DEVICE_TYPE_DESKTOP     @"4"
#define DEVICE_TYPE_OTHER       @"5"
#define IMAGE_WIDTH_SAMLL     80
#define IMAGE_HEIGHT_SAMLL    80
@implementation Utils
+(void)setTextAndPlaceHolderColor:(UIView *)view withTextColor:(UIColor *)clrText withPlaceholderColor:(UIColor*)clrPlaceholder{
    if ([view isKindOfClass:[UITextField class]]){
        if(clrText != nil){
            ((UITextField*)view).textColor = clrText;
        }
        if (clrPlaceholder != nil && [view respondsToSelector:@selector(setPlaceHolderColor:)]){
            [(UITextField*)view setPlaceHolderColor:clrPlaceholder];
        }
    }
    else if ([view isKindOfClass:[UILabel class]]){
        if (clrText != nil){
            ((UILabel*)view).textColor = clrText;
        }
    }
    else if ([view isKindOfClass:[UITextView class]]){
        if (clrText != nil){
            ((UITextView *)view).textColor = clrText;
        }
    }
    else{
        for(UIView *v in view.subviews){
            [Utils setTextAndPlaceHolderColor:v withTextColor:clrText withPlaceholderColor:clrPlaceholder];
        }
    }
}

+(NSString*)getDeviceInfo{
    return [NSString stringWithFormat:@"%@:%@",[[UIDevice currentDevice] systemName],[[UIDevice currentDevice] systemVersion]];
}

+(NSString*)getDeviceType{
    return DEVICE_TYPE_IOS;
}


+(NSString*)getAppVersion{
    NSString *version =  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *versionShort = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    if ([version length] > 0){
        return [NSString stringWithFormat:@"%@ (%@)",versionShort,version];
    }
    else{
        return versionShort;
    }
}

+(NSString*)getCurrentLanguage{
    return @"en";
}


+(NSString*)getDeviceUniqueId{
#if TARGET_IPHONE_SIMULATOR
    NSString *str = [[[UIDevice currentDevice] uniqueDeviceIdentifier] uppercaseString];
    while([str length] <32){
        str = [str stringByAppendingString:@"0"];
    }
    NSMutableString *mstr = [str mutableCopy];
    [mstr insertString:@"-" atIndex:20];
    [mstr insertString:@"-" atIndex:16];
    [mstr insertString:@"-" atIndex:12];
    [mstr insertString:@"-" atIndex:8];
    return mstr;
#else
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#endif
}

+(UIViewController*)loadViewControllerFromStoryboard:(NSString*)storyBoard controllerId:(NSString*)controllerId{
    UIStoryboard *sboard = [UIStoryboard storyboardWithName:storyBoard bundle:nil];
    UIViewController *ctrl = [sboard instantiateViewControllerWithIdentifier:controllerId];
    
    return ctrl;
}
+(UIView*)loadViewFromNib:(NSString*)nibName class:(Class)aClass owner:(id)owner options:(NSDictionary *)options index:(int)index{
    NSBundle *bunle = [NSBundle bundleForClass:aClass];
    return [bunle loadNibNamed:nibName owner:owner options:options][index];
}
+(UIView*)loadViewFromNib:(NSString*)nibName class:(Class)aClass owner:(id)owner options:(NSDictionary *)options{
    NSBundle *bunle = [NSBundle bundleForClass:aClass];
    return [bunle loadNibNamed:nibName owner:owner options:options][0];
}

+(UIView*)loadViewFromNib:(NSString *)nibName viewTag:(NSInteger)tag{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %li",tag];
    NSArray *arrayFiltered = [array filteredArrayUsingPredicate:predicate];
    return [arrayFiltered count] > 0 ? [arrayFiltered firstObject] : nil;
}

+(PopupViewBase *)loadPopupViewFromNibWithTag:(NSInteger)tag{
    UIView *v = [Utils loadViewFromNib:@"PopupViews" viewTag:tag];
    return v == nil ? nil : (PopupViewBase*)v;
}

+(void)showPopupView:(PopupViewBase*)popup{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app showPopupView:popup];
}

+(void)dismissPopupView{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app dismissPopupView];
}

+(void)showAlert:(NSString*)message  delegate:(id<AlertViewDelegate>)del{
    [Utils showAlert:message title:nil delegate:del];
}

+(void)showAlert:(NSString*)message  delegate:(id<AlertViewDelegate>)del withContext:(NSInteger)context withContextObject:(NSObject*)obj{
    [Utils showAlert:message title:nil delegate:del withContext:context withContextObject:obj];
}

+(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id<AlertViewDelegate>)del{
    [Utils showAlert:message title:title delegate:del firstButton:nil secondButton:nil];
}

+(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id<AlertViewDelegate>)del withContext:(NSInteger)context withContextObject:(NSObject*)obj {
    [Utils showAlert:message title:title delegate:del firstButton:nil secondButton:nil withContext:context withContextObject:obj];
}

+(void)showAlert:(NSString*)message title:(NSString*)title  delegate:(id<AlertViewDelegate>)del firstButton:(NSString*)first secondButton:(NSString*)second {
    AlertViewNormal *alertV = (AlertViewNormal*)[Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_NormalAlert];
    alertV.alertViewDelegate = del;
    
    if (first == nil && second == nil){
        [alertV setTitle:title message:message buttonText:NSLocalizedString(@"Button_OK", @"OK"),nil];
    }
    else if (first != nil && second == nil){
        [alertV setTitle:title message:message buttonText:first,nil];
    }
    else{
        [alertV setTitle:title message:message buttonText:first,second,nil];
    }
    [Utils showPopupView:alertV];
}

+(void)showAlert:(NSString*)message title:(NSString*)title  delegate:(id<AlertViewDelegate>)del firstButton:(NSString*)first secondButton:(NSString*)second withContext:(NSInteger)context withContextObject:(NSObject*)obj{
    [Utils showAlert:message title:title delegate:del firstButton:first secondButton:second withContext:context withContextObject:obj checkboxText:nil];
}

+(void)showAlert:(NSString*)message title:(NSString*)title  delegate:(id<AlertViewDelegate>)del firstButton:(NSString*)first secondButton:(NSString*)second withContext:(NSInteger)context withContextObject:(NSObject*)obj checkboxText:(NSString*)checkboxText{
    AlertViewNormal *alertV = (AlertViewNormal*)[Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_NormalAlert];
    alertV.alertViewDelegate = del;
    alertV.context = context;
    alertV.contextObject = obj;
    alertV.checkboxText = checkboxText;
    
    if (first == nil && second == nil){
        [alertV setTitle:title message:message buttonText:NSLocalizedString(@"Button_OK", @"OK"),nil];
    }
    else if (first != nil && second == nil){
        [alertV setTitle:title message:message buttonText:first,nil];
    }
    else{
        [alertV setTitle:title message:message buttonText:first,second,nil];
    }
    [Utils showPopupView:alertV];
}

+(UIImage*)createUIImageFromColor:(UIColor *)color withSize:(CGSize)size withAlpha:(CGFloat)alpha{
    UIImage *img = nil;
    CGRect rc = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rc.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat red,green,blue,alphaOrg;
    
    if ([color getRed:&red green:&green blue:&blue alpha:&alphaOrg]){
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:red green:green blue:blue alpha:alpha].CGColor);
        CGContextFillRect(context, rc);
        img = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return img;
}
+(NSString *)getTimeString:(NSDate*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss'Z'";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *str = [dateFormatter stringFromDate:date];
    return str;
}

+(NSString *)getTimeString:(NSDate*)date withFormat:(NSString *)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    NSString *str = [dateFormatter stringFromDate:date];
    return str;
}

+(NSDate *)getTimeFromString:(NSString *)dateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'Z'"];
    NSDate *date = [formatter dateFromString:dateString];
    return date;
}

+(NSString*)getPaymetOptionName:(PaymentOption)option{
    NSString *name = nil;
    switch(option){
        case PaymentOption_ApplePay:
            name =  NSLocalizedString(@"PaymentOption_ApplePay", nil);
            break;
        case PaymentOption_PayPal:
            name = NSLocalizedString(@"PaymentOption_PayPal", nil);
            break;
        case PaymentOption_SwychBalance:
            name = NSLocalizedString(@"PaymentOption_SwychBalance", nil);
            break;
        case PaymentOption_GoogleWallet:
            name = NSLocalizedString(@"PaymentOption_GoogleWallet", nil);
            break;
        case PaymentOption_VisaCheckout:
            name = NSLocalizedString(@"PaymentOption_VisaCheckout", nil);
            break;
        case PaymentOption_AmazonCheckout:
            name = NSLocalizedString(@"PaymentOption_AmazonCheckout", nil);
            break;
        case PaymentOption_DirectCreditCard:
            name = NSLocalizedString(@"PaymentOption_DirectCreditCard", nil);
            break;
        case PaymentOption_Bitcoin:
            name = NSLocalizedString(@"PaymentOption_Bitcoin", nil);
            break;
        case PaymentOption_ScannedGiftcard:
            name = NSLocalizedString(@"PaymentOption_ScannedGiftcard", nil);
            break;
        case PaymentOption_kering:
            name = NSLocalizedString(@"PaymentOption_kering", nil);
            break;
        case PaymentOption_RewardPoints:
            name = NSLocalizedString(@"PaymentOption_RewardPoints", nil);
            break;
        case PaymentOption_PromotionCode:
            name = NSLocalizedString(@"PaymentOption_PromotionCode", nil);
            break;
        default:
            break;
            
    }
    return name;
}
+(NSString*)getPaymetStatusName:(TransactionStatus)Status{
    NSString* strName=@"";
    switch(Status){
        case E_giftSentOut:
            strName =  NSLocalizedString(@"CardTransactionStatus_Sent", nil);
            break;
        case E_giftReceived:
            strName = NSLocalizedString(@"CardTransactionStatus_Received", nil);
            break;
        case E_giftUserSaved:
            strName =  NSLocalizedString(@"CardTransactionStatus_Saved", nil);
            break;
        case RedeemedAsCash:
            strName = NSLocalizedString(@"CardTransactionStatus_redeemed_Cash", nil);
            break;
        case RedeemedAsGiftcard:
            strName =  NSLocalizedString(@"CardTransactionStatus_redeemed_Card", nil);
            break;
        case RedeemedAsMerchant:
            strName = NSLocalizedString(@"CardTransactionStatus_redeemed_Merchant", nil);
            break;
        case RedeemedAsSwychBalace:
            strName =  NSLocalizedString(@"CardTransactionStatus_redeemed_Swytch", nil);
            break;
        case Paid:
            strName =  NSLocalizedString(@"CardTransactionStatus_redeemed_Paid", nil);
            break;
        case Delivered:
            strName =  NSLocalizedString(@"CardTransactionStatus_redeemed_Delivered", nil);
            break;
        default:
            break;
    }
    return  strName;

}
+(NSString *)getFormattedPhoneNumber:(NSString*)phoneNumber withFormat:(NSString*)format{
    NSString *newPhone = [[phoneNumber componentsSeparatedByCharactersInSet:
                                                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                                componentsJoinedByString:@""];
    if ([newPhone length] != 10 && [newPhone length] != 11){
        return nil;
    }
    if ([newPhone length] == 11){
        newPhone = [newPhone substringFromIndex:1];
    }

    if ([format compare:@"-"] == NSOrderedSame){
        return [NSString stringWithFormat:@"1(%@)%@-%@",[newPhone substringWithRange:NSMakeRange(0, 3)],
                [newPhone substringWithRange:NSMakeRange(3, 3)],
                [newPhone substringWithRange:NSMakeRange(6, 4)]];
    }
    else{
        return [NSString stringWithFormat:@"%@.%@.%@",[newPhone substringWithRange:NSMakeRange(0, 3)],
                [newPhone substringWithRange:NSMakeRange(3, 3)],
                [newPhone substringWithRange:NSMakeRange(6, 4)]];
    }

}
+(UIImage*)resizeHeaderImg:(UIImage*)sourceImage{
    if(!sourceImage) return nil;
    UIImage *img = [Utils resizeImage:sourceImage WithWidth:IMAGE_WIDTH_SAMLL withHeight:IMAGE_HEIGHT_SAMLL withKeepAspectRatio:YES];
    return img;
}
+(UIImage*)resizeImage:(UIImage*)image WithWidth:(CGFloat)width withHeight:(CGFloat)height withKeepAspectRatio:(BOOL)keepRatio{
    CGSize newSize = CGSizeMake(width,height);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, [image scale]);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(NSString*)phoneNumberWithoutAreaCode:(NSString *)phone{
    if (phone!= nil && phone.length == 11){
        if ([[phone substringToIndex:0] isEqualToString:@"1"]){
            return [phone substringFromIndex:1];
        }
    }
    return phone;
}

+(NSString*)phoneNumberWithAreaCode:(NSString*)phone{
    if (phone!= nil && phone.length == 10){
        return [NSString stringWithFormat:@"1%@",phone];
    }
    return phone;
}


+(void)addNotificationObserver:(id)observer selector:(SEL)aSelector name:(NSString *)notiName object:(id)anObject{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:observer selector:aSelector name:notiName object:anObject];
}

+(void)removeNotificationObserver:(id)observer name:(NSString *)notiName object:(id)anObject{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:observer name:notiName object:anObject];
}

+(void)postLocalNotification:(NSString*)notiName object:(id)anObject userInfo:(NSDictionary*)userInfo{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:notiName object:anObject userInfo:userInfo];
}

+(NSArray<ModelTransaction*>*)getTransactionsFromGiftcards:(NSArray<ModelGiftCard*>*)giftcards{
    if (giftcards == nil || giftcards.count == 0){
        return nil;
    }
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(ModelGiftCard *card in giftcards){
        ModelTransaction *tran = [[ModelTransaction alloc] init];
        tran.parentTransactionId = card.parentTransactionId;
        tran.transactionId = card.transactionId;
        tran.giftCard = card;
        
        [array addObject:tran];
    }
    return array;
}

+(NSString*)getPhoneMCC{
#if TARGET_IPHONE_SIMULATOR
    return @"310";
#else
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    
    return mcc;
#endif
}
+(NSArray*)getHistorySortingOptionsArray{
    NSArray *array = [NSArray arrayWithObjects:[[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Favorite withName:NSLocalizedString(@"Sort_Favorites",nil)
                                                                                         withIcon:[UIImage imageNamed:@"icon_favorite_on.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Tranding withName:NSLocalizedString(@"Sort_Trending",nil)
                                                                withIcon:[UIImage imageNamed:@"icon_sort_trending.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Newest withName:NSLocalizedString(@"Sort_Newest",nil)
                                                                withIcon:[UIImage imageNamed:@"icon_sort_newest.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_PriceLowToHigh withName:NSLocalizedString(@"Sort_PriceLowtoHigh",nil)
                                                                withIcon:[UIImage imageNamed:@"icon_sort_price_low_to_high.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_PriceHighToLow withName:NSLocalizedString(@"Sort_PriceHighToLow",nil)
                                                                withIcon:[UIImage imageNamed:@"icon_sort_price_high_to_low.png"]],
                      nil];
    return array;
}
+(NSArray*)getSortingOptionsArray{
    NSArray *array = [NSArray arrayWithObjects:
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Favorite withName:NSLocalizedString(@"Sort_Favorites",nil) withIcon:[UIImage imageNamed:@"icon_favorite_on.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Tranding withName:NSLocalizedString(@"Sort_Trending",nil) withIcon:[UIImage imageNamed:@"icon_sort_trending.png"]],
                      [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardSortOption_Newest withName:NSLocalizedString(@"Sort_Newest",nil) withIcon:[UIImage imageNamed:@"icon_sort_newest.png"]],
                      nil];
    return array;
}
+(NSArray*)getHistoryFilterOptionsArray{
NSMutableArray * array = nil;
    array = [NSMutableArray arrayWithObjects:
             [[ModelCustomizedActionSheetItem alloc] initWithId: E_giftSentOut withName:NSLocalizedString(@"Filter_Sent",nil)
                                                       withIcon:[UIImage imageNamed:@"icon_sent.png"]],
             [[ModelCustomizedActionSheetItem alloc] initWithId: E_giftReceived withName:NSLocalizedString(@"Filter_Received",nil)
                                                       withIcon:[UIImage imageNamed:@"icon_received.png"]],
             [[ModelCustomizedActionSheetItem alloc] initWithId: Paid withName:NSLocalizedString(@"Filter_Purchased",nil)
                                                       withIcon:[UIImage imageNamed:@"icon_purchased.png"]],
             nil];
return array;
}
+(NSArray*)getFilterOptionsArray{
    NSMutableArray * array = nil;
    
    if ([CacheData instance].filterOptions.count > 0){
        array = [[NSMutableArray alloc] initWithCapacity:[CacheData instance].filterOptions.count];
        [array addObject:[[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Favorite withName:NSLocalizedString(@"Filter_Favorites",nil) withIcon:[UIImage imageNamed:@"icon_favorite_on.png"]]];
        for(ModelFilterOption *opt in [CacheData instance].filterOptions){
            ModelCustomizedActionSheetItem *item = [[ModelCustomizedActionSheetItem alloc] initWithId: opt.optionId withName:opt.name withIcon:[UIImage imageNamed:[Utils imageFileNameForFilterOption:opt.optionId]]];
            [array addObject:item];
        }
    }
    else{
         array = [NSMutableArray arrayWithObjects:[[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Favorite withName:NSLocalizedString(@"Filter_Favorites",nil)
                                                                                             withIcon:[UIImage imageNamed:@"icon_favorite_on.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Restaurants withName:NSLocalizedString(@"Filter_Restaurants",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_restaurants.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Entertainment withName:NSLocalizedString(@"Filter_Entertainment",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_entertainment.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Online withName:NSLocalizedString(@"Filter_Online",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_online.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Clothing withName:NSLocalizedString(@"Filter_Clothing",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_shopping.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Electronics withName:NSLocalizedString(@"Filter_Electronics",nil)
                                                                    withIcon:[UIImage imageNamed:@""]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Sports withName:NSLocalizedString(@"Filter_Sports",nil)
                                                                    withIcon:[UIImage imageNamed:@""]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Outdoors withName:NSLocalizedString(@"Filter_Outdoors",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_sportsoutdoors.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Health withName:NSLocalizedString(@"Filter_Health",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_healthyBeauty.png"]],
                          [[ModelCustomizedActionSheetItem alloc] initWithId: GiftCardFilterOption_Beauty withName:NSLocalizedString(@"Filter_Beauty",nil)
                                                                    withIcon:[UIImage imageNamed:@"icon_healthyBeauty.png"]],
                          nil];
    
    }
    return array;
}

+(NSString*)imageFileNameForFilterOption:(NSInteger)filterOptionId{
    NSString *fileName = nil;
    switch (filterOptionId) {
        case 1:
            fileName = @"icon_entertainment.png";
            break;
        case 2:
            fileName = @"icon_online.png";
            break;
        case 3:
            fileName= @"icon_shopping.png";
            break;
        case 4:
            fileName = @"icon_restaurants.png";
            break;
        case 5:
            fileName = @"icon_entertainment1.png";
            break;
        case 6:
            fileName = @"icon_sportsoutdoors.png";
            break;
        case 7:
            fileName = @"icon_healthyBeauty.png";
            break;
        default:
            break;
    }
    return fileName;
}

+(void)downloadImageByURL:(NSString*)url callback:(void(^)(UIImage*,NSError*))callback{
    [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *imgURL = [NSURL URLWithString:url];
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imgURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError) {
            UIImage *img = [[UIImage alloc] initWithData:data];
            callback(img,nil);
        }else{
            NSLog(@"%@",connectionError);
            callback(nil,connectionError);
        }
    }];
}
+(BOOL)CheckIfAmazon:(ModelGiftCard *)giftcard{
return [[giftcard.retailer lowercaseString] containsString:@"amazon"];
}
+(BOOL)CheckIfTarget:(ModelGiftCard *)giftcard{
    return [[giftcard.retailer lowercaseString] containsString:@"target"];
}
+(UIImage*)ChangeImageColor:(UIImage*)img color:(UIColor*)newcolor{
    UIColor *color = newcolor;
    UIImage *image = img;
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
    CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));
    
    UIImage* coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return coloredImg;
}

+(NSString*)getOTPSentTypeWithPaymentMethod:(PaymentMethod)method{
    NSString *sentType = nil;
    switch (method) {
        case PaymentMethod_PayPal:
            sentType = OTPSENTTYPE_PAYPAL_EMAIL_VALIDATION;
            break;
        case PaymentMethod_ApplePay:
            sentType = OTPSENTTYPE_APPLEPAY_EMAIL_VALIDATION;
            break;
        case PaymentMethod_AmazonPay:
            sentType = OTPSENTTYPE_AMAZON_EMAIL_VALIDATION;
            break;
        case PaymentMethod_DirectCreditCard:
            sentType = OTPSENTTYPE_DIRECTCREDITCARD_EMAIL_VALIDATION;
            break;
        case PaymentMethod_GoogleWallet:
            sentType = OTPSENTTYPE_ANDROIDPAY_EMAIL_VALIDATION;
            break;
        case PaymentMethod_VisaCheckout:
            sentType = OTPSENTTYPE_VISACHECKOUT_EMAIL_VALIDATION;
            break;
        case PaymentMethod_BitCoin:
            sentType = OTPSENTTYPE_BITCOIN_EMAIL_VALIDATION;
            break;
        case PaymentMethod_Kering:
            sentType = OTPSENTTYPE_KERING_EMAIL_VALIDATION;
            break;
        case PaymentMethod_None:
            sentType = nil;
        default:
            sentType = nil;
            break;
    }
    
    return sentType;
}

+(void)openURLWithExternalBrowser:(NSString*)url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];}
@end
