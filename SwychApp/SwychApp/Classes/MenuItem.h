//
//  MenuItem.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject
@property (nonatomic,strong) NSString* text;
@property (nonatomic,strong) NSString* iconImageName;
@property (nonatomic,assign) NSInteger tag;
@property (nonatomic,assign) NSInteger sortOrder;
@property (nonatomic,readonly) BOOL isSeparator;
@property (nonatomic,assign) BOOL debugOnly;
@end
