//
//  DataFomatter.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataFomatter : NSObject

+(DataFomatter*)instance;
-(NSString*)formattedPhoneNumber:(NSString*)phoneNumber countryCode:(NSString*)countryCode;
@end
