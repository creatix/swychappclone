//
//  ChangePasswordPopupView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"
@protocol ForgotPasswordPopupViewDelegate;
@interface ForgotPasswordPopupView : PopupConfirmationView
@property (strong, nonatomic)  NSString *phoneNumber;
@property (strong, nonatomic)  NSString *otp;
@property (assign,nonatomic) id<ForgotPasswordPopupViewDelegate> uDelegate;
@end
@protocol ForgotPasswordPopupViewDelegate <NSObject>

-(void)AfterUpdateNewPassword:(NSString*)password phone:(NSString*)phonenumber;

@end