//
//  Camera.h
//  TestCamera
//
//  Created by Donald Hancock on 2/28/16.
//  Copyright © 2016 com.threeOtwo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

extern NSString * const CAMERA_UTILITY_ERROR_DOMAIN;

@protocol BarcodeScannerProtocol <NSObject>

- (void) barcodeDetected:(NSString*)value
              withBounds:(CGRect)bounds;

@end

@protocol CameraProtocol <NSObject>


@end

@class AVCaptureSession;

typedef enum {
    kCameraUtilityError_AccessDenied = 0,
    kCameraUtilityError_CanNotOpenDevice = 1,
    kCameraUtilityError_CanConfirgureDevice = 2,
    kCameraUtilityError_ImageCaptureFailed = 3,
} Camera_Utility_Errors;


@interface Camera : NSObject

+ (void) loadCameras:(void(^)(NSArray<Camera*>*, NSError*)) cameraCallback;

- (void) open:(void(^)(AVCaptureSession*, NSError*)) openCallback;

- (void) captureImage:(void(^)(NSData*, NSError*)) captureImageCallback;

- (void) start;

- (void) stop;

- (void) turnTorchOnOff;

@property (nonatomic, assign) BOOL enableBarcodeScanning;

@property (nonatomic, readonly) BOOL isBackCamera;

@property (nonatomic, assign) CGFloat   zoom;

@property (nonatomic, assign) CGPoint focusPoint;

@property (nonatomic, weak) NSObject<BarcodeScannerProtocol>* barcodeProtocol;

@property (nonatomic, weak) NSObject<CameraProtocol>* cameraProtocol;

@end
