//
//  KeyboardHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KeyboardHandler : NSObject{
    UIWindow *_window;
    UITextField *_textField;
    UITextView *_textView;
    
    CGFloat _offsetY;
    CGPoint _keyboardCenter;
    CGFloat _keyboardHeight;
    BOOL _keyboardShowing;
    
    UIBarButtonItem *_doneButtonItem;
    UIBarButtonItem *_previousButtonItem;
    UIBarButtonItem *_nextButtonItem;
    UIToolbar *_toolbar;
    
}

-(id) initWithWindow: (UIWindow*) window;
-(void)removeNotifications;
@end
