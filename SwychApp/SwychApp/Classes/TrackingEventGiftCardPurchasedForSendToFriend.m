//
//  TrackingEventGiftCardPurchasedForSendToFriend.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventGiftCardPurchasedForSendToFriend.h"

@implementation TrackingEventGiftCardPurchasedForSendToFriend
-(id)init{
    self = [super init];
    if (self){
        self.eventName = ANA_EVENT_GiftCardPurchasedSendToFriend;
        self.eventCode = @"at15ns";
    }
    return self;
}

@end
