//
//  LocalUserInfo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelUser.h"

@class ModelSystemConfiguration;

@interface LocalUserInfo : ModelUser
@property(nonatomic,assign) LoginOption previousLoginOption;
@property(nonatomic,assign) BOOL touchIDEnabled;
@property(nonatomic,strong) UIImage *avatarImage;
@property(nonatomic,strong) NSString *avatarImageURL;
@property(nonatomic,strong) NSString *sessionToken;
@property(nonatomic,assign) BOOL notPromptWhenSwychGift;
@property(nonatomic,strong) ModelSystemConfiguration *sysConfig;

@property(nonatomic,strong) NSString *googleToken;
@property(nonatomic,strong) NSString *googleUserId;

@property(nonatomic,assign) NSInteger selectedPaymentMethod;

-(id)initWithUser:(ModelUser*)user;

-(void)copyFromUser:(ModelUser*)user;
-(NSString*)getUserFullName;

@end
