//
//  ApplicationConfigurations.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

@class LocalUserInfo;
@class ObjectConfigInfo;
@class ModelPaymentMethodSettings;
@class AnalyticSettings;


@interface ApplicationConfigurations : NSObject{
    NSMutableDictionary *_dictResources;
    NSMutableDictionary *_dictGeneral;
    NSMutableDictionary *_dictIndividual;
}

+(ApplicationConfigurations*)instance;

+(UIColor*)getColorFromDictionary:(NSDictionary*)dict;
+(UIColor*)getColorFromString:(NSString*)value;

-(ModelPaymentMethodSettings*)getPaymentMethodSettings;

-(NSString *)getSubClassName:(NSString*)parentName;
-(NSString *)getSubClassNibName:(NSString*)parentName;

-(NSString*)getServerURL;
-(NSString*)getConnectionClientID;
-(NSString*)getConnectionApiKey;

-(CGFloat)getViewWidth:(NSString*)viewControllerName;
-(CGFloat)getViewHeight:(NSString*)viewControllerName;

-(UIColor*)getTextColor:(NSString*)viewControllerName;
-(UIColor*)getPlaceHolderColor:(NSString*)viewControllerName;
-(UIColor*)getViewBackgroundColor:(NSString*)viewControllerName;

-(ObjectConfigInfo*)getButtonConfiguration:(NSString*)viewControllerName;
-(ObjectConfigInfo*)getConfiguration:(NSString*)viewControllerName object:(UIView*)obj;

-(UIImage*)getAvatarImage;
-(NSString*)getUserFullName;

-(LocalUserInfo*)getLocalStoredUserInfo;

-(AnalyticSettings*)getAnalyticSettings:(NSString*)name;

#pragma 
#pragma mark - Tabbar
-(NSArray*)getTabbarItemInfo;
-(UIColor*)getTabbarBackgroundColor;

#pragma
#pragma mark - Sliding Menu
-(CGFloat)getSlidingMenuViewWidth;
-(NSArray*)getSlidingMenuItems;

#pragma
#pragma mark - Help Menu
-(NSString*)getHelpURL;
- (NSArray*) getHelpMenuItems;

#pragma
#pragma mark - Payment Menu
- (NSArray*) getPaymentMenuItems;

#pragma
#pragma mark - Settings Menu

- (NSArray*) getConnectedAccountsMenuItems;

- (NSArray*) getTouchIdMenuItems;

@end
