//
//  SocialNetworkIntegration.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "SocialNetworkIntegration.h"
#import "ModelUser.h"


#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#define Error_Domain    @"swychapp.error.socialnetwork"

@protocol SocialNetworkIntegrationDeleage;

@interface SocialNetworkIntegration()

@property(nonatomic,weak) id<SocialNetworkIntegrationDeleage> delegate;
@property(nonatomic,weak) UIViewController *googleUIDelegateViewController;
@end

@implementation SocialNetworkIntegration

+(SocialNetworkIntegration*)instance{
    static SocialNetworkIntegration *g_instanceSocial = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instanceSocial = [[self alloc] init];
    });
    return g_instanceSocial;
}
-(void)doFacebookLoginOnViewController:(UIViewController*)controller{
    [self doFacebookLoginOnViewController:controller delegate:nil];
}

-(void)doFacebookLoginOnViewController:(UIViewController*)controller delegate:(id<SocialNetworkIntegrationDeleage>)delegate{
    id<SocialNetworkIntegrationDeleage> del = delegate;
    if (del == nil){
        del = (id<SocialNetworkIntegrationDeleage>)controller;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile"]
     fromViewController:controller
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         NSString *fbToken = nil;
         NSString *fbUserId = nil;
         BOOL success = NO;
         NSError *err = nil;
         if (error) {
             NSLog(@"Process error");
             success = NO;
             err = [NSError errorWithDomain:Error_Domain code:SocialNetworkError_Unknown
                                   userInfo:[NSDictionary dictionaryWithObject:[error localizedDescription] forKey:NSLocalizedDescriptionKey]];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             success = NO;
             err = [NSError errorWithDomain:Error_Domain code:SocialNetworkError_Unknown
                                   userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Error_SocialNetwork_Login_Cancelled", nil) forKey:NSLocalizedDescriptionKey]];
         } else {
             NSLog(@"FB Logged in...");
             success = YES;
             fbToken = result.token.tokenString;
             fbUserId = result.token.userID;
             err = nil;
         }
         if ([del respondsToSelector:@selector(facebookLogin:facebookToken:facebookUserId:error:)]){
             [del facebookLogin:success facebookToken:fbToken facebookUserId:fbUserId error:err];
         }

     }];
}

-(void)doGoogleLoginOnViewController:(UIViewController*)controller{
    [self doGoogleLoginOnViewController:controller delegate:nil];
}
-(void)doGoogleLoginOnViewController:(UIViewController*)controller delegate:(id<SocialNetworkIntegrationDeleage>)delegate{
    self.googleUIDelegateViewController = controller;
    self.delegate = delegate;
    id del1 = [[GIDSignIn sharedInstance] delegate];
    id del2 = [GIDSignIn sharedInstance].uiDelegate;
    
    [[GIDSignIn sharedInstance] signIn];
}



#pragma
#pragma mark - GoogleSignInDelegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    if (error == nil){
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        
        ModelUser *u = [[ModelUser alloc] init];
        u.firstName = user.profile.givenName;
        u.lastName = user.profile.familyName;
        u.email = user.profile.email;
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(googleSignIn:googleToken:googleUserId:userInfo:error:)]){
            [self.delegate googleSignIn:YES googleToken:idToken googleUserId:userId userInfo:u error:nil];
        }
    }
    else{
        
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(googleSignIn:googleToken:googleUserId:userInfo:error:)]){
        [self.delegate googleSignIn:NO googleToken:nil googleUserId:nil userInfo:nil error:error];
    }
}


#pragma
#pragma mark - Google SignIn
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //[myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    UIViewController *ctrl = self.googleUIDelegateViewController;
    if (ctrl == nil){
        ctrl = (UIViewController *) [APP getCurrentViewController];
    }
    if (ctrl != nil){
        [ctrl presentViewController:viewController animated:YES completion:nil];
    }
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    UIViewController *ctrl = self.googleUIDelegateViewController;
    if (ctrl == nil){
        ctrl = (UIViewController *)[APP getCurrentViewController];
    }
    if (ctrl != nil){
        [ctrl dismissViewControllerAnimated:YES completion:nil];
    }
}

@end

