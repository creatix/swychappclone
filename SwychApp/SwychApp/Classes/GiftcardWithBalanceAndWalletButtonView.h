//
//  GiftcardWithBalanceAndWalletButtonView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PassKit/PassKit.h"
#import "ManullyUpdateBalance.h"
@class ModelGiftCard;
@protocol GiftcardWithBalanceAndWalletButtonViewDelegate;

IB_DESIGNABLE
@interface GiftcardWithBalanceAndWalletButtonView : UIView<AlertViewDelegate,ManullyUpdateBalanceDelegate>

@property(nonatomic,strong) ModelGiftCard *giftCard;
@property(nonatomic,strong) PKAddPassesViewController* pkCtl;
@property(nonatomic,weak) id<GiftcardWithBalanceAndWalletButtonViewDelegate> delegate;

-(void)updateAddToWalletButton;
-(void)updateSubviews;
@end


@protocol GiftcardWithBalanceAndWalletButtonViewDelegate <NSObject>

@optional
-(void)giftcardWithBalanceAndWalletButtonViewStartUpdateBalance:(GiftcardWithBalanceAndWalletButtonView*)giftcardView;
-(void)giftcardWithBalanceAndWalletButtonView:(GiftcardWithBalanceAndWalletButtonView*)giftcardView finishBalanceUpdate:(double)newBalance;

-(void)giftcardWithBalanceAndWalletButtonViewStartAddPassToWallet:(GiftcardWithBalanceAndWalletButtonView*)giftcardView;
-(void)giftcardWithBalanceAndWalletButtonViewFinishedAddPassToWallet:(GiftcardWithBalanceAndWalletButtonView*)giftcardView;
@end
