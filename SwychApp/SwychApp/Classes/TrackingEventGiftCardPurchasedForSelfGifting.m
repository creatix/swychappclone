//
//  TrackingEventGiftCardPurchasedForSelfGifting.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventGiftCardPurchasedForSelfGifting.h"

@implementation TrackingEventGiftCardPurchasedForSelfGifting
-(id)init{
    self = [super init];
    if (self){
        self.eventCode = @"q3i03r";
        self.eventName = ANA_EVENT_GiftCardPurchasedSelfGifting;
    }
    return self;
}

@end
