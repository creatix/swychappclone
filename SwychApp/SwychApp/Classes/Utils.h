//
//  Utils.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertViewNormal.h"
#import "PaymentConstants.h"

#define MAINCOLOR_GRAY [UIColor grayColor];
#define MAINCOLOR_PINK UICOLORFROMRGB(192, 31, 109, 1.0);
@class PopupViewBase;
@class LocalUserInfo;
@class ModelTransaction;
@class ModelGiftCard;

@interface Utils : NSObject

+(void)setTextAndPlaceHolderColor:(UIView *)view withTextColor:(UIColor *)clrText withPlaceholderColor:(UIColor*)clrPlaceholder;
+(NSString*)getDeviceType;
+(NSString*)getDeviceInfo;
+(NSString*)getAppVersion;
+(NSString*)getCurrentLanguage;
+(NSString*)getDeviceUniqueId;

+(UIViewController*)loadViewControllerFromStoryboard:(NSString*)storyBoard controllerId:(NSString*)controllerId;
+(UIView*)loadViewFromNib:(NSString *)nibName viewTag:(NSInteger)tag;
+(UIView*)loadViewFromNib:(NSString*)nibName class:(Class)aClass owner:(id)owner options:(NSDictionary *)options;
+(PopupViewBase*)loadPopupViewFromNibWithTag:(NSInteger)tag;

+(void)showPopupView:(PopupViewBase*)popup;
+(void)dismissPopupView;

+(void)showAlert:(NSString*)message delegate:(id<AlertViewDelegate>)del;
+(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id<AlertViewDelegate>)del;
+(void)showAlert:(NSString*)message title:(NSString*)title delegate:(id<AlertViewDelegate>)del firstButton:(NSString*)first secondButton:(NSString*)second;
+(void)showAlert:(NSString*)message title:(NSString*)title  delegate:(id<AlertViewDelegate>)del firstButton:(NSString*)first secondButton:(NSString*)second withContext:(NSInteger)context withContextObject:(NSObject*)obj checkboxText:(NSString*)checkboxText;

+(UIImage*)createUIImageFromColor:(UIColor*)color withSize:(CGSize)size withAlpha:(CGFloat)alpha;

+(NSString *)getTimeString:(NSDate*)date;
+(NSString *)getTimeString:(NSDate*)date withFormat:(NSString *)format;
+(NSDate *)getTimeFromString:(NSString *)dateString;

+(NSString*)getPaymetOptionName:(PaymentOption)option;

+(NSString*)getPaymetStatusName:(TransactionStatus)Status;
+(UIView*)loadViewFromNib:(NSString*)nibName class:(Class)aClass owner:(id)owner options:(NSDictionary *)options index:(int)index;

+(NSString *)getFormattedPhoneNumber:(NSString*)phoneNumber withFormat:(NSString*)format;
+(UIImage*)resizeHeaderImg:(UIImage*)sourceImage;

+(NSString*)phoneNumberWithoutAreaCode:(NSString*)phone;
+(NSString*)phoneNumberWithAreaCode:(NSString*)phone;

+(void)addNotificationObserver:(id)observer selector:(SEL)aSelector name:(NSString *)notiName object:(id)anObject;
+(void)removeNotificationObserver:(id)observer name:(NSString *)notiName object:(id)anObject;
+(void)postLocalNotification:(NSString*)notiName object:(id)anObject userInfo:(NSDictionary*)userInfo;

+(NSArray<ModelTransaction*>*)getTransactionsFromGiftcards:(NSArray<ModelGiftCard*>*)giftcards;

+(NSString*)getPhoneMCC;

+(NSArray*)getFilterOptionsArray;
+(NSArray*)getHistorySortingOptionsArray;
+(NSArray*)getSortingOptionsArray;
+(UIImage*)resizeImage:(UIImage*)image WithWidth:(CGFloat)width withHeight:(CGFloat)height withKeepAspectRatio:(BOOL)keepRatio;
+(NSArray*)getHistoryFilterOptionsArray;
+(void)downloadImageByURL:(NSString*)url callback:(void(^)(UIImage*,NSError*))callback;
+(BOOL)CheckIfAmazon:(ModelGiftCard *)giftcard;
+(BOOL)CheckIfTarget:(ModelGiftCard *)giftcard;
+(UIImage*)ChangeImageColor:(UIImage*)img color:(UIColor*)newcolor;

+(NSString*)getOTPSentTypeWithPaymentMethod:(PaymentMethod)method;

+(void)openURLWithExternalBrowser:(NSString*)url;
@end
