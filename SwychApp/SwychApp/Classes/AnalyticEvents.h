//
//  AnalyticEvents.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#ifndef AnalyticEvents_h
#define AnalyticEvents_h

#define ANA_EVENT_AppLaunched           @"AppLaunched"

#define ANA_EVENT_AppInstalled                      @"AppInstalled"
#define ANA_EVENT_OTPCompleted                      @"OTPCompleted"
#define ANA_EVENT_RegistrationCompleted             @"RegistrationCompleted"
#define ANA_EVENT_GiftCardPurchasedSelfGifting      @"GiftCardPurchasedSelfGifting"
#define ANA_EVENT_GiftCardPurchasedSendToFriend     @"GiftCardPurchasedSendToFriend"
#define ANA_EVENT_CardUploaded                      @"CardUploaded"
#define ANA_EVENT_AffiliateDesignated               @"AffiliateDesignated"

#define ANA_EVENT_SLIDING_MENU_Displayed                        @"SlidingMenuDisplayed"
#define ANA_EVENT_SLIDING_MENU_ItemClicked                      @"SlidingMenuItemClicked"
#define ANA_ATT_SLIDING_MENU_ItemClicked_ItemName               @"MenuItemName"



#define ANA_EVENT_VIEW_Displayed                @"ViewDisplayed"
#define ANA_ATT_VIEW_Displayed_ViewName         @"ViewName"


#define ANA_ATT_USER_Login_UserName             @"UserName"
#endif /* AnalyticEvents_h */
