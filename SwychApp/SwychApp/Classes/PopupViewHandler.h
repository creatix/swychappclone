//
//  PopupViewHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PopupViewBase;
@class AppDelegate;

@interface PopupViewHandler : NSObject{
    UIWindow *_window;
    AppDelegate *_app;
    UIView *_view;
    UIView *_maskView;
    NSMutableArray *_popupViews;
}

-(void)pushView:(PopupViewBase*)view;
-(void)popView;
-(void)popView:(PopupViewBase*)view;

-(void)relayoutSubviews:(CGRect )rc;
-(UIView*)topView;
@end
