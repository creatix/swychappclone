//
//  RegistrationData.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelUser.h"


@interface RegistrationData : ModelUser

@property (strong,nonatomic) NSString* facebookId;
@property (strong,nonatomic) NSString* facebookToken;
@property (strong,nonatomic) NSString* googlePlusId;
@property (strong,nonatomic) NSString* googlePlusToken;
@property (strong,nonatomic) NSString* twitterId;
@property (strong,nonatomic) NSString* twitterToken;

@property (assign,nonatomic) RegistrationType registrationType;
@property (assign,nonatomic) BOOL verifyTOPDirectly;

-(id)initWithUser:(ModelUser*)user;
@end
