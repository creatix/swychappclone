//
//  ObjectConfigInfo.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ObjectConfigInfo.h"
#import "ApplicationConfigurations.h"
#import "ApplicationConfigurationsConstants.h"

@implementation ObjectConfigInfo

-(id)initWithDictionary:(NSDictionary*)dict{
    if (self = [super init]){
        self.backgroundColor = nil;
        self.textColor = nil;
        self.fixedHeight = 0.0;
        self.font = nil;
        
        [self setProperties:dict];
    }
    return self;
}

-(void)setProperties:(NSDictionary*)dict{
    if (dict == nil){
        return;
    }
    //Background color
    if ([dict objectForKey:KEY_BackgroundColor] != nil){
        self.backgroundColor = [ApplicationConfigurations getColorFromString:[dict objectForKey:KEY_BackgroundColor]];
        _hasPropertySettings = YES;
    }
    
    //TextColor
    if ([dict objectForKey:KEY_Text_Color] != nil){
        self.textColor = [ApplicationConfigurations getColorFromString:[dict objectForKey:KEY_Text_Color]];
        _hasPropertySettings = YES;
    }
    
    //Height
    if ([dict objectForKey:KEY_Height] != nil){
        self.fixedHeight = [[dict objectForKey:KEY_Height] floatValue];
        _hasPropertySettings = YES;
    }
    
    //PlaceHolder Color
    if ([dict objectForKey:KEY_PlaceHolder_Color]){
        self.placeHolderColor = [ApplicationConfigurations getColorFromString:[dict objectForKey:KEY_PlaceHolder_Color]];
        _hasPropertySettings = YES;
    }
    //Font
    NSString *fontName = [dict objectForKey:KEY_FONT_NAME];
    NSString *fontSize = [dict objectForKey:KEY_FONT_SIZE];
    if (fontName != nil && fontSize != nil){
        self.font = [UIFont fontWithName:fontName size:[fontSize floatValue]];
        _hasPropertySettings = YES;
    }
    
}

-(BOOL)hasPropertySettings{
    return _hasPropertySettings;
}
@end
