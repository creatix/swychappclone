//
//  UIRoundCornerButton.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIRoundCornerButton.h"

@implementation UIRoundCornerButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@dynamic cornerRadius,borderWidth,borderColor;

-(CGFloat)cornerRadius{
    return _cornerRadius;
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = _cornerRadius;
}

-(CGFloat)borderWidth{
    return _borderWidth;
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth;
}

-(UIColor*)borderColor{
    return _borderColor;
}

-(void)setBorderColor:(UIColor *)borderColor{
    _borderColor = borderColor;
    self.layer.borderColor = _borderColor.CGColor;
}

-(void)setButtonTextColor:(UIColor *)textColor backgroundColor:(UIColor *)bkColor cornerRadius:(CGFloat)radius{
    self.backgroundColor = bkColor;
    self.cornerRadius = radius;
    [self setTitleColor:textColor forState:UIControlStateNormal];
    [self setTitleColor:textColor forState:UIControlStateHighlighted];
    [self setTitleColor:textColor forState:UIControlStateSelected];

}
@end
