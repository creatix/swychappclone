//
//  CustomizedActionSheet.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CustomizedActionSheet.h"
#import "Utils.h"
#import "CustomizedActionSheetTableViewCell.h"
#import "ModelCustomizedActionSheetItem.h"


#define ROW_HEIGHT  50.0f

@interface CustomizedActionSheet()
@property (weak, nonatomic) IBOutlet UITableView *tvList;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
@property (weak, nonatomic) IBOutlet UIView *vSheetView;
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
- (IBAction)buttonTapped:(id)sender;

@end

@implementation CustomizedActionSheet

@synthesize actionSheetTableViewDataSource = _actionSheetTableViewDataSource;
@synthesize actionSheetTableViewDelegate = _actionSheetTableViewDelegate;

-(ModelCustomizedActionSheetItem*)selectedItem{
    if (selectedItemIndex >= 0 && selectedItemIndex < [self.items count]){
        return [self.items objectAtIndex:selectedItemIndex];
    }
    else{
        return nil;
    }
}
-(void)setActionSheetTableViewDataSource:(id<UITableViewDataSource>)actionSheetTableViewDataSource{
    _actionSheetTableViewDataSource = actionSheetTableViewDataSource;
    self.tvList.dataSource = _actionSheetTableViewDataSource;
}

-(id<UITableViewDataSource>)actionSheetTableViewDataSource{
    return _actionSheetTableViewDataSource;
}


-(void)setActionSheetTableViewDelegate:(id<UITableViewDelegate>)actionSheetTableViewDelegate{
    _actionSheetTableViewDelegate = actionSheetTableViewDelegate;
    self.tvList.delegate = _actionSheetTableViewDelegate;
}

-(id<UITableViewDelegate>)actionSheetTableViewDelegate{
    return _actionSheetTableViewDelegate;
}

-(void)awakeFromNib{
    [self setup];
    
    [self.tvList registerNib:[UINib nibWithNibName:@"CustomizedActionSheetTableViewCell" bundle:nil] forCellReuseIdentifier:@"CustomizedActionSheetTableViewCell"];
}

-(void)setup{    
    self.tvList.backgroundColor = [UIColor clearColor];
    self.ivBackground.image = [Utils createUIImageFromColor:[UIColor darkGrayColor] withSize:CGSizeMake(10, 10) withAlpha:0.6f];

    UIImage *img = [[UIImage imageNamed:@"customized_actionsheet_bk.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    self.tvList.backgroundView = [[UIImageView alloc] initWithImage:img];
    self.vSheetView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.btnAction setBackgroundImage:img forState:UIControlStateNormal];
    [self.btnAction setBackgroundImage:img forState:UIControlStateHighlighted];
    
    self.tvList.delegate = self;
    self.tvList.dataSource = self;
    
    selectedItemIndex = -1;
    _selectedItemId = -1;
    
    self.minimumHeigtOfSheetView = 100.0f;
    self.maximumHeigtOfSheetView = 500.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate = self;
    [self addGestureRecognizer:tap];

}

-(void)touchOnView:(id)sender{
    [self dismiss];
}

- (IBAction)buttonTapped:(id)sender {
    if (self.actionSheetDelegate != nil && [self.actionSheetDelegate respondsToSelector:@selector(customizedActiondSheetButtonTapped:)]){
        [self.actionSheetDelegate customizedActiondSheetButtonTapped:self];
    }
    [self dismiss];
}

-(void)show{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    
    self.frame = CGRectMake(0, win.frame.size.height, win.frame.size.width, win.frame.size.height);
    [win addSubview:self];
    [self adjustHeightOfSheetView];
    [win bringSubviewToFront:self];
    [self.tvList reloadData];
    
    
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.frame = CGRectMake(0, 0, win.frame.size.width, win.frame.size.height);
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDidStopSelector:@selector(removeView)];
    [UIView commitAnimations];
}

-(void)dismiss{
    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
    if (win == nil){
        return;
    }
    NSNumber *duration = [NSNumber numberWithDouble:0.5];
    UIViewAnimationCurve curve = UIViewAnimationCurveEaseOut;
    [UIView beginAnimations: nil context: nil];
    
    self.frame = CGRectMake(0, win.frame.size.height, self.frame.size.width, self.frame.size.height);
    
    [UIView setAnimationCurve: curve];
    [UIView setAnimationDuration: [duration doubleValue]];
    [UIView setAnimationDidStopSelector:@selector(removeView)];
    [UIView commitAnimations];
}

-(void)adjustHeightOfSheetView{
    CGFloat extra = self.vSheetView.frame.size.height - self.tvList.frame.origin.y - self.tvList.frame.size.height;
    CGFloat h = extra + [self getRowHeight] * [self.items count];
    if (h < self.minimumHeigtOfSheetView){
        h = self.minimumHeigtOfSheetView;
    }
    else if (h > self.maximumHeigtOfSheetView){
        h = self.maximumHeigtOfSheetView;
    }
    self.constraintHeightSheetView.constant = h;
}

-(void)removeView{
    [self removeFromSuperview];
}

+(CustomizedActionSheet*)createInstanceWithItems:(NSArray*)itemsArray withButtonText:(NSString *)buttonText{
    UIView *v = [Utils loadViewFromNib:@"CustomizedActionSheet" viewTag:1];
    if ([v isKindOfClass:[CustomizedActionSheet class]]){
        CustomizedActionSheet* cas = (CustomizedActionSheet*)v;
        cas.items = itemsArray;
        [cas.btnAction setTitle:buttonText];
        return cas;
    }
    return nil;
}

-(CGFloat)getRowHeight{
    return ROW_HEIGHT;
}

#pragma 
#pragma mask - UITapGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (touch.view == self.ivBackground){
        return YES;
    }
    return  NO;
}

#pragma 
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.items count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CustomizedActionSheetTableViewCell";
    CustomizedActionSheetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CustomizedActionSheetTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ModelCustomizedActionSheetItem *item = [self.items objectAtIndex:indexPath.row];
    
    if (item.itemId == self.selectedItemId){
        item.selected = YES;
        selectedItemIndex = indexPath.row;
    }
    else{
        item.selected = NO;
    }
    cell.item = item;
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getRowHeight];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedItemIndex != -1){
        NSIndexPath *idx = [NSIndexPath indexPathForRow:selectedItemIndex inSection:0];
        ModelCustomizedActionSheetItem *item1 = [self.items objectAtIndex:idx.row];
        item1.selected = NO;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:idx] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    ModelCustomizedActionSheetItem *item1 = [self.items objectAtIndex:indexPath.row];
    item1.selected = YES;
    selectedItemIndex = indexPath.row;
    
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self buttonTapped:self.btnAction];
}

@end

