//
//  GiftcardSelectionView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftcardSelectionView.h"
#import "GiftcardMallCollectionViewCell.h"
#import "UIButton+Extra.h"
#import "GiftcardMallTableViewCell.h"
#import "ModelGiftCard.h"
#import "ModelCustomizedActionSheetItem.h"
#import "NSObject+Extra.h"

#define CELL_HEIGHT     60.0f;


#define ACTION_SHEET_CONTEXT_FILTER     1
#define ACTION_SHEET_CONTEXT_SORTING    2


@interface GiftcardSelectionView()

@property(nonatomic,strong) ModelCustomizedActionSheetItem *selectedFilterItem;
@property(nonatomic,strong) ModelCustomizedActionSheetItem *selectedSortingItem;
@property(nonatomic,strong) NSString *currentSearchCriteria;
@property(nonatomic,strong) NSMutableArray *giftcardsInUse;

-(void)setupInfos;
-(IBAction)buttonTapped:(id)sender;
-(void)switchGiftcardListView;
-(void)showSortingSheet;
-(void)showFilterSheet;
-(void)doSearch:(NSString*)criteria;
-(void)giftcardSelected:(ModelGiftCard*)card;
@end

@implementation GiftcardSelectionView

@synthesize giftcards = _giftcards;

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

-(NSArray*)giftcards{
    return _giftcards;
}

-(void)setGiftcards:(NSArray *)giftcards{
    _giftcards = giftcards;
    [self rebuildInUseGiftcardsArray];
}


-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"GiftcardSelectionView" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.tvGiftcards registerNib:[UINib nibWithNibName:@"GiftcardMallTableViewCell" bundle:nil] forCellReuseIdentifier:@"giftcardMallTableviewDetailCell"];
    [self.cvGiftcards registerNib:[UINib nibWithNibName:@"GiftcardMallCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"giftcardCollectionViewCell"];
    
    
    
    [self setupInfos];
    [Utils addNotificationObserver:self selector:@selector(giftcardFavoriteNotificationHandler:) name:NOTI_Giftcard_Favorite object:nil];
}

-(void)giftcardFavoriteNotificationHandler:(NSNotification*)noti{
    if (self.giftcards != nil && noti.userInfo != nil){
        ModelGiftCard *giftcard = [noti.userInfo objectForKey:@"Giftcard"];
        BOOL updateTableView = NO;
        if (giftcard != nil){
            for(ModelGiftCard *card in self.giftcards){
                if (card.giftCardId == giftcard.giftCardId){
                    card.favourite = giftcard.favourite;
                    updateTableView = YES;
                }
            }
        }
        if (updateTableView){
            [self.tvGiftcards reloadData];
        }
    }
}

-(void)setupInfos{
    self.cvGiftcards.backgroundColor = [UIColor clearColor];
    
    self.tvGiftcards.hidden = NO;
    self.cvGiftcards.hidden = YES;
    [self.btnHeaderViewType setButtonImage:[UIImage imageNamed:@"icon_thumbnail.png"]];
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [[UIImage alloc] init];
    
    
    self.searchBar.placeholder = NSLocalizedString(@"Placeholder_Search", nil);
    self.searchBar.delegate = self;
}



-(void)setListViewAsCurrentView:(BOOL)listViewAsCurrent{
    if (listViewAsCurrent){
        self.tvGiftcards.hidden = NO;
        self.cvGiftcards.hidden = YES;
    }
    else{
        self.tvGiftcards.hidden = YES;
        self.cvGiftcards.hidden = NO;
    }
}

-(void)refreshData{
    [self.tvGiftcards reloadData];
    [self.cvGiftcards reloadData];
}

-(void)switchGiftcardListView{
    if (self.tvGiftcards.hidden){
        self.cvGiftcards.hidden = YES;
        self.tvGiftcards.hidden = NO;
        [self.tvGiftcards reloadData];
        [self.btnHeaderViewType setButtonImage:[UIImage imageNamed:@"icon_thumbnail.png"]];
    }
    else{
        self.cvGiftcards.hidden = NO;
        self.tvGiftcards.hidden = YES;
        [self.cvGiftcards reloadData];
        [self.btnHeaderViewType setButtonImage:[UIImage imageNamed:@"icon_list.png"]];
    }
}

-(IBAction)buttonTapped:(id)sender{
    if(sender == self.btnHeaderFilter){
        [self showFilterSheet];
    }
    else if (sender == self.btnHeaderSort){
        [self showSortingSheet];
    }
    else if (sender == self.btnHeaderViewType){
        [self switchGiftcardListView];
    }
}

-(void)showSortingSheet{
    CustomizedActionSheet *cas = [CustomizedActionSheet createInstanceWithItems:[Utils getSortingOptionsArray]
                                                                 withButtonText:NSLocalizedString(@"Button_Apply", nil)];
    cas.selectedItemId = self.selectedSortingItem == nil ? -1 : self.selectedSortingItem.itemId;
    cas.actionSheetDelegate = self;
    cas.context = ACTION_SHEET_CONTEXT_SORTING;
    [cas show];
}


-(void)showFilterSheet{
    CustomizedActionSheet *cas = [CustomizedActionSheet createInstanceWithItems:[Utils getFilterOptionsArray]
                                                                 withButtonText:NSLocalizedString(@"Button_Apply", nil)];
    cas.selectedItemId = self.selectedFilterItem == nil ? -1 : self.selectedFilterItem.itemId;
    cas.actionSheetDelegate = self;
    cas.context = ACTION_SHEET_CONTEXT_FILTER;
    [cas show];
}

-(void)rebuildInUseGiftcardsArray{
    NSArray *array = self.giftcards;
    if ([self.currentSearchCriteria length] > 0){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *card, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [card retailerNameLike:self.currentSearchCriteria];
        }];
        array = [array filteredArrayUsingPredicate:pred];
    }
    if (self.selectedFilterItem != nil){
        NSInteger filterId = self.selectedFilterItem.itemId;
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelGiftCard *card, NSDictionary<NSString *,id> * _Nullable bindings) {
            if (filterId == GiftCardFilterOption_Favorite){
                return card.favourite == YES;
            }
            else{
                return [card hasFilterOptionById:filterId];
            }
        }];
        array = [array filteredArrayUsingPredicate:pred];
    }
    if (self.selectedSortingItem != nil){
        NSInteger sortingId = self.selectedSortingItem.itemId;
        switch (sortingId) {
            case GiftCardSortOption_Favorite:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelGiftCard *card1 = (ModelGiftCard*)a;
                    ModelGiftCard *card2 = (ModelGiftCard*)b;
                    return card1.favourite < card2.favourite;
                }];
                break;
            case GiftCardSortOption_Tranding:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelGiftCard *card1 = (ModelGiftCard*)a;
                    ModelGiftCard *card2 = (ModelGiftCard*)b;
                    return card1.trending > card2.trending;
                }];
                break;
            case GiftCardSortOption_Newest:
                array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    ModelGiftCard *card1 = (ModelGiftCard*)a;
                    ModelGiftCard *card2 = (ModelGiftCard*)b;
                    return [card1.updated compare:card2.updated];
                }];
                break;
            default:
                break;
        }
    }
    
    self.giftcardsInUse = [NSMutableArray arrayWithArray:array];
    [self.tvGiftcards reloadData];
    [self.cvGiftcards reloadData];
}


-(void)giftcardSelected:(ModelGiftCard*)card{
    if(self.selectionViewDelegate != nil && [self.selectionViewDelegate respondsToSelector:@selector(giftcardSelectionView:giftcardSelected:)]){
        [self.selectionViewDelegate giftcardSelectionView:self giftcardSelected:card];
    }
}

#pragma
#pragma mark - CustomizedActionSheetDelegate
-(void)customizedActiondSheetButtonTapped:(CustomizedActionSheet *)sheet{
    ModelCustomizedActionSheetItem *item = sheet.selectedItem;
    if (item == nil){
        return;
    }
    if (sheet.context == ACTION_SHEET_CONTEXT_SORTING){
        if (self.selectedSortingItem != nil && (self.selectedSortingItem.itemId == item.itemId)){
            self.selectedSortingItem = nil;
        }
        else{
            self.selectedSortingItem = item;
            
        }
        [self rebuildInUseGiftcardsArray];
    }
    else if (sheet.context == ACTION_SHEET_CONTEXT_FILTER){
        if (self.selectedFilterItem != nil && (self.selectedFilterItem.itemId == item.itemId)){
            self.selectedFilterItem = nil;
        }
        else{
            self.selectedFilterItem = item;
        }
        [self rebuildInUseGiftcardsArray];
    }
}

#pragma
#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    self.currentSearchCriteria = searchText;
    [self rebuildInUseGiftcardsArray];
}


#pragma
#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return  [self.giftcardsInUse count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GiftcardMallCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"giftcardCollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    ModelGiftCard *card = [self.giftcardsInUse objectAtIndex:indexPath.row];
    cell.giftcardView.giftcard = card;
    cell.giftcardView.giftcardViewDelegate = self;
    return cell;
}


#pragma
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
}
#pragma
#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat w = (collectionView.bounds.size.width - 4 * 10.0)/ 2.0;
    return CGSizeMake(w, w / GIFTCARD_ASPEC_RATIO);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 15;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

#pragma
#pragma mask - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.giftcardsInUse count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"giftcardMallTableviewDetailCell";
    GiftcardMallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil){
        cell = [[GiftcardMallTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.amount = _amount;
    ModelGiftCard *card = [self.giftcardsInUse objectAtIndex:indexPath.row];
    cell.giftcard = card;
    if(self.ifHiddenPrice){
        [cell hideAmountLabel];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ModelGiftCard *card = [self.giftcardsInUse objectAtIndex:indexPath.row];
    [self giftcardSelected:card];
}

#pragma
#pragma mark - UIGiftcardViewDelegate
-(void)giftcardViewTapped:(UIGiftcardView*)giftcardView{
    [self giftcardSelected:giftcardView.giftcard];
}
@end
