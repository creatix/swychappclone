//
//  GiftForFriendView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelContact;
@class ContactDetailView;

@protocol GiftForFriendViewDelegate;


@interface GiftForFriendView : UIView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>{
    NSArray *contactArray;
    NSInteger _selectedContactIndex;
    NSString *_filterCriteria;
}

@property (weak, nonatomic) IBOutlet UICollectionView *cvForFriend;
@property (weak, nonatomic) IBOutlet UIView *vHeader;
@property (weak, nonatomic) IBOutlet UILabel *labHeaderTo;
@property (weak, nonatomic) IBOutlet UITextField *txtHeaderNameEmailMobile;
@property (weak,nonatomic) IBOutlet ContactDetailView *vContactDetail;

@property (weak,nonatomic) IBOutlet id<GiftForFriendViewDelegate> giftForFriendDelegate;

-(void)refreshContactData;

@end


@protocol GiftForFriendViewDelegate <NSObject>

@optional
-(void)giftForFriendView:(GiftForFriendView*)forFriendView itemSelected:(ModelContact*)contact;
@end