//
//  ModelContact.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Contacts/Contacts.h>
#define INIT_TYPE_ABR 1245
#define INIT_TYPE_CNC 1236
@interface ModelContact : NSObject

@property(strong,nonatomic) NSString *firstName;
@property(strong,nonatomic) NSString *lastName;
@property(strong,nonatomic) NSString *phoneNumber;
@property(strong,nonatomic) UIImage *profileImage;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSDate *birthday;

@property(strong,nonatomic) NSMutableArray *phoneNumbers;
@property(strong,nonatomic) NSMutableArray *phoneNumbersAndTitle;
@property(strong,nonatomic) NSMutableArray *emails;
@property(strong,nonatomic) NSMutableArray *emailsAndTitle;

@property (nonatomic,readonly) NSString*fullName;
@property (nonatomic,strong) id MyRecordRef;
@property (nonatomic,strong) CNMutableContact* MyCnContact;
@property(nonatomic) int TypeOfInit;

-(id)initWithABRecordRef:(ABRecordRef)recordRef;
-(id)initWithCNContact:(CNMutableContact*)cnContact;

-(BOOL)hasPhoneNumberStartWith:(NSString*)startWith;
-(BOOL)hasEmailAddresStartWith:(NSString*)startWith;
-(BOOL)hasNameContains:(NSString*)partialName;
-(BOOL)hasNameStartWith:(NSString*)name;

-(BOOL)changeContactPhoneNumber:(NSString *) phoneSought
                         forThis:(NSString *) newPhoneNumber;
-(BOOL)changeContactEmail:(NSString *) emailSought
                  forThis:(NSString *) newEmail;
-(BOOL)changeBirthday:(NSDate *) newBirthday;
-(BOOL)hasPhoneNumberContainsWith:(NSString*)startWith;
-(BOOL)hasPhoneNumber:(NSString*)phoneNumber;
@end
