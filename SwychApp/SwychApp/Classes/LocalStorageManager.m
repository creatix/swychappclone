//
//  LocalStorageManager.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LocalStorageManager.h"
#import "LocalUserInfo.h"
#import "NSObject+Extra.h"
#import "ModelBotTransaction.h"
#define SWYCH_FOLDER        @"swych/"
#define IMAGE_FOLDER        @"swych/images/"
#define AVATAR_IMAGE_FILE   @"swych/images/avatar.png"

@implementation LocalStorageManager

+(LocalStorageManager*)instance{
    static LocalStorageManager *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}

-(NSString*)getStoredSwychId{
    LocalUserInfo *data = [self getLocalStoredUserInfo];
    return  data == nil ? nil : data.swychId;
}


-(void)saveLocalUserInfo:(LocalUserInfo*)userInfo{
    NSData *data = [userInfo getJSONDataWithDateTimeFormatter:nil];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:KEY_USER_LOCAL_DATA];
    [userDefaults synchronize];
}

-(LocalUserInfo*)getLocalStoredUserInfo{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:KEY_USER_LOCAL_DATA];
    if (data == nil){
        return nil;
    }
    LocalUserInfo *userInfo = [[LocalUserInfo alloc] init];
    NSError *error = nil;
    [userInfo parseJSONData:data error:&error dateTimeFormatter:nil];
    return  userInfo;
}

-(void)clearStoredUserInfo{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:KEY_USER_LOCAL_DATA];
    [userDefaults synchronize];
}

-(void)saveResourceVersion:(NSInteger)resVersion{
    //
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithInteger:resVersion] forKey:KEY_LOCAL_RES_VERSION];
    [userDefaults synchronize];
}
-(NSInteger)getCurrentResourceVersion{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *data = [userDefaults objectForKey:KEY_LOCAL_RES_VERSION];
    return data == nil ? 0 : [data integerValue];
}

-(void)saveUserAvatarImage:(UIImage*)image{
    NSData *data = UIImagePNGRepresentation(image);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *pathImage = [[paths objectAtIndex:0] stringByAppendingPathComponent:IMAGE_FOLDER];
    NSString *pathSwych = [[paths objectAtIndex:0] stringByAppendingPathComponent:SWYCH_FOLDER];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:pathSwych])	//Does directory already exist?
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:pathSwych
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
        }
    }
    if (![[NSFileManager defaultManager] fileExistsAtPath:pathImage])	//Does directory already exist?
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:pathImage
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
        }
    }
    
    
    NSString *pathAvatar = [documentsDirectory stringByAppendingPathComponent:AVATAR_IMAGE_FILE];
    BOOL ret = [[NSFileManager defaultManager] createFileAtPath:pathAvatar
                                            contents:data
                                          attributes:nil];
    if(!ret){
        NSLog(@"Failed to create avatar image.");
    }
}
-(UIImage*)getUserAvatarImage{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:AVATAR_IMAGE_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        //File exists
        NSData *data = [[NSData alloc] initWithContentsOfFile:path];
        if (data)
        {
            UIImage *img= [[UIImage alloc] initWithData:data];
            return img;
        }
        else{
            return nil;
        }
    }
    else
    {
        return nil;
    }
}

//
-(void)setAppInstalledFlag{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithInteger:1] forKey:KEY_APP_INSTALLED];
    [userDefaults synchronize];
}
-(BOOL)getAppInstalledFlag{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *data = [userDefaults objectForKey:KEY_APP_INSTALLED];
    return data == nil ? NO : [data boolValue];
}
-(void)saveBotTransaction:(ModelBotTransaction*)botTransaction{
    if(botTransaction == nil){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:KEY_BOT_TRANSACTION_DATA];
        [userDefaults synchronize];
        return;
    }
    NSData *data = [botTransaction getJSONDataWithDateTimeFormatter:nil];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:KEY_BOT_TRANSACTION_DATA];
    [userDefaults synchronize];
}

-(ModelBotTransaction*)getBotTransaction{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:KEY_BOT_TRANSACTION_DATA];
    if (data == nil){
        return nil;
    }
    ModelBotTransaction *botTransaction = [[ModelBotTransaction alloc] init];
    NSError *error = nil;
    [botTransaction parseJSONData:data error:&error dateTimeFormatter:nil];
    return  botTransaction;
}
@end
