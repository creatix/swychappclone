//
//  PaymentOptionPayWithCardTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentOptionTableViewCell.h"

@interface PaymentOptionPayWithCardTableViewCell : PaymentOptionTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UILabel *labPayWithCard;

@end
