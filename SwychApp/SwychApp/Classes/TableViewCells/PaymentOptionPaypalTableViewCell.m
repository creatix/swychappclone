//
//  PaymentOptionPaypalTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentOptionPaypalTableViewCell.h"


#define CELL_SEPARATER_COLOR    UICOLORFROMRGB(194,194,194,1.0)

@implementation PaymentOptionPaypalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.labSeparator.backgroundColor = CELL_SEPARATER_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)buttonTapped:(id)sender{
    
}
@end
