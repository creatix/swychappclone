//
//  TransactionHistoryTableViewCell.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-04.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ModelTransaction;
@interface TransactionHistoryTableViewCell : UITableViewCell
@property(nonatomic,strong) ModelTransaction *giftcard;
@end
