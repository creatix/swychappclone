//
//  ChooseRecipientTableViewCell.m
//  SwychApp
//
//  Created by kndev3 on 10/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ChooseRecipientTableViewCell.h"
@interface ChooseRecipientTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *labNmae;
@property (weak, nonatomic) IBOutlet UILabel *labEmail;
@end
@implementation ChooseRecipientTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}
-(void)setContactItem:(AContactItem *)item{
    NSString* name = item.parentModal.fullName;
    NSString* firsName = item.parentModal.firstName;
    int len = (int)[firsName length];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:name];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0,len)];

    self.labNmae.attributedText = string;
    self.labEmail.text = item.Content_Item;
}
-(void)setupUI{
    
    self.labEmail.text = self.emailOrPhone;
    self.contentView.userInteractionEnabled = NO;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)Update{

}
@end
