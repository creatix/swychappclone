//
//  PaymentOptionPayWithCardTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentOptionPayWithCardTableViewCell.h"


#define TEXT_COLOR_PAY_WITH_CARD    UICOLORFROMRGB(28,102,174,1.0)
#define CELL_SEPARATER_COLOR    UICOLORFROMRGB(194,194,194,1.0)

#define CELL_FONT_NAME      @"HelveticaNeue-Medium"

#define CELL_TEXT_FONT_SIZE 14

@implementation PaymentOptionPayWithCardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.labPayWithCard.textColor = TEXT_COLOR_PAY_WITH_CARD;
    self.labPayWithCard.font = [UIFont fontWithName:CELL_FONT_NAME size:CELL_TEXT_FONT_SIZE];
    self.labPayWithCard.textAlignment = NSTextAlignmentLeft;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
