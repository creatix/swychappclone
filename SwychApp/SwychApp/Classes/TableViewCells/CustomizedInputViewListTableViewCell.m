//
//  CustomizedInputViewListTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CustomizedInputViewListTableViewCell.h"

@implementation CustomizedInputViewListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
