//
//  PaymentOptionTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentOptionTableViewCell.h"


#define CELL_BACKGROUND_COLOR   UICOLORFROMRGB(240,240,240,1.0)

@implementation PaymentOptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = CELL_BACKGROUND_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonTapped:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(paymentOptionCellButtonTapped:buttonTag:)]){
        [self.delegate paymentOptionCellButtonTapped:self buttonTag:((UIButton*)sender).tag];
    }
}

@end
