//
//  PaymentOptionApplePayTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentOptionApplePayTableViewCell.h"


#define CELL_BACKGROUND_COLOR   UICOLORFROMRGB(240,240,240,1.0)
#define CELL_SEPARATER_COLOR    UICOLORFROMRGB(194,194,194,1.0)

@implementation PaymentOptionApplePayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = CELL_BACKGROUND_COLOR;
    self.labSeparator.backgroundColor = CELL_SEPARATER_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
