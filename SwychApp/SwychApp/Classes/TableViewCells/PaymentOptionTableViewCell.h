//
//  PaymentOptionTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentOptionCellDelegate ;

@interface PaymentOptionTableViewCell : UITableViewCell

@property(nonatomic,weak) id<PaymentOptionCellDelegate> delegate;

- (IBAction)buttonTapped:(id)sender;
@end


@protocol PaymentOptionCellDelegate <NSObject>

@optional
-(void)paymentOptionCellButtonTapped:(PaymentOptionTableViewCell*)paymentOptionCell buttonTag:(NSInteger)tag;

@end
