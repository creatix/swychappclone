//
//  TransactionMessageCell.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TransactionMessageCellDelegate ;
@interface TransactionMessageCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UITextView* labelMessage;
@property (nonatomic, weak) IBOutlet UIButton* btnVideo;
@property (nonatomic, weak) IBOutlet UILabel* lblSenderName;
@property(nonatomic,weak) id<TransactionMessageCellDelegate>  delegate;
@property (nonatomic, strong)  NSString* imageUrl;
@property (nonatomic, strong)  NSString* url_Video;
- (IBAction)buttonTapped:(id)sender;
@end
@protocol TransactionMessageCellDelegate <NSObject>

@optional
-(void)viewVideoButtonTapped:(TransactionMessageCell*)paymentOptionCell buttonTag:(NSInteger)tag;

@end