//
//  TransactionHistoryTableViewCell.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-04.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionHistoryTableViewCell.h"
#import "ModelGiftCard.h"
#import "UIImageView+WebCache.h"
#import "ModelTransaction.h"
@interface TransactionHistoryTableViewCell()

@property(weak,nonatomic) IBOutlet UIImageView *ivCard;
@property(weak,nonatomic) IBOutlet  UILabel *labCardName;
@property(weak,nonatomic) IBOutlet  UILabel *labDate;
@property(weak,nonatomic) IBOutlet  UILabel *labAmount;
@end
@implementation TransactionHistoryTableViewCell

@synthesize giftcard = _giftcard;


-(void)setGiftcard:(ModelTransaction *)giftcard{
    _giftcard = giftcard;
    if ([_giftcard.giftCard.giftCardImageURL count] > 0){
        [self.ivCard sd_setImageWithURL:[NSURL URLWithString:[_giftcard.giftCard.giftCardImageURL objectAtIndex:0]]];
    }
    else{
        self.ivCard.image = GIFTCARD_DEFAULT_IMAGE;
    }
    
    self.labCardName.text = _giftcard.giftCard.retailer;
    NSDate *transdate = _giftcard.transactionDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    formatter.dateFormat = @"MMM dd, yyyy";
    self.labDate.text =  [formatter stringFromDate:transdate];
    NSString *amount = [NSString stringWithFormat:@"$%0.02f",
                         _giftcard.amount];
    self.labAmount.text = amount;
}

-(ModelTransaction*)giftcard{
    return _giftcard;
}

- (void)awakeFromNib {
    // Initialization code
    self.ivCard.layer.cornerRadius = 6.0f;
      [self.ivCard.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
