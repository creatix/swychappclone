//
//  TouchIdTableViewCell.h
//  SwychApp
//
//  Created by Donald Hancock on 3/23/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchIdTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* label;
@property (nonatomic, weak) IBOutlet UISwitch* enabledSwitch;

@end
