//
//  TransactionPaymentCell.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionPaymentCell.h"

@implementation TransactionPaymentCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor blueColor];
    _labelMethod.minimumScaleFactor=0.5;
    _labelMethod.adjustsFontSizeToFitWidth = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
