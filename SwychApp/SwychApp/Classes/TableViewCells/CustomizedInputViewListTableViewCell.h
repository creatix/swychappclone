//
//  CustomizedInputViewListTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CustomizedInputViewListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ivLeft;
@property (weak, nonatomic) IBOutlet UIImageView *ivRight;
@property (weak, nonatomic) IBOutlet UILabel *labLeft;
@property (weak, nonatomic) IBOutlet UILabel *labRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftImageViewAspect;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRightImageViewAspect;
@property ( nonatomic)  int HigherThanentered;
@end
