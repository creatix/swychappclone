//
//  GiftcardMallTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftcardMallTableViewCell.h"
#import "ModelGiftCard.h"
#import "UIImageView+WebCache.h"
#import "UIRoundCornerImageView.h"

@interface GiftcardMallTableViewCell()

@property(weak,nonatomic) IBOutlet UIRoundCornerImageView *ivCard;
@property(weak,nonatomic) IBOutlet  UIImageView *ivFavorite;
@property(weak,nonatomic) IBOutlet  UILabel *labCardName;
@property(weak,nonatomic) IBOutlet  UIImageView *ivArrow;
@property(weak,nonatomic) IBOutlet  NSLayoutConstraint *labAmountWidth;
@property(weak,nonatomic) IBOutlet  UILabel *labAmount;
@end

@implementation GiftcardMallTableViewCell

@synthesize giftcard = _giftcard;


-(void)setGiftcard:(ModelGiftCard *)giftcard{
    _giftcard = giftcard;
    if ([_giftcard.giftCardImageURL count] > 0){
        [self.ivCard sd_setImageWithURL:[NSURL URLWithString:[_giftcard.giftCardImageURL objectAtIndex:0]]];
    }
    else{
        self.ivCard.image = GIFTCARD_DEFAULT_IMAGE;
    }
    self.labCardName.text = _giftcard.retailer;
    if(_amount>0){
        _labAmountWidth.constant = 60;
        _labAmount.text = [NSString stringWithFormat:@"$%0.2f",_amount*_giftcard.conversionRate];//_amount*_giftcard.conversionRate
    }
    else{
       _labAmountWidth.constant = 0;
    }
    if(giftcard.favourite){
        self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_on.png"];
    }
    else{
        self.ivFavorite.image = [UIImage imageNamed:@"icon_favorite_off.png"];
    }
}
-(void)hideAmountLabel{
_labAmount.text = @"";
}
-(ModelGiftCard*)giftcard{
    return _giftcard;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
