//
//  PaymentOptionPaypalTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentOptionTableViewCell.h"

@interface PaymentOptionPaypalTableViewCell : PaymentOptionTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivPaypalLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *labSeparator;


- (IBAction)buttonTapped:(id)sender;

@end
