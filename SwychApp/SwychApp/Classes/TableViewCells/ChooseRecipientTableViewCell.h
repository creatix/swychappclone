//
//  ChooseRecipientTableViewCell.h
//  SwychApp
//
//  Created by kndev3 on 10/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AContactItem.h"
@interface ChooseRecipientTableViewCell : UITableViewCell
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *emailOrPhone;
@property(nonatomic,strong) AContactItem *contactItem;
-(void)Update;
@end
