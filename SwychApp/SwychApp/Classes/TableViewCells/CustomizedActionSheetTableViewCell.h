//
//  CustomizedActionSheetTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ModelCustomizedActionSheetItem;

@interface CustomizedActionSheetTableViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UIImageView *ivIcon;
@property(nonatomic,weak) IBOutlet UILabel *labName;
@property(nonatomic,weak) IBOutlet UIImageView *ivCheckmark;
@property (nonatomic,strong) NSObject *object;

@property (nonatomic,strong) ModelCustomizedActionSheetItem *item;

@end
