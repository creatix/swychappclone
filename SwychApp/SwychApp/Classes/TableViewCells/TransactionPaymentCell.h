//
//  TransactionPaymentCell.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionPaymentCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel* labelMethod;
@property (nonatomic, weak) IBOutlet UILabel* labelAmount;
@end
