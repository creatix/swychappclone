//
//  LocationTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LocationTableViewCell.h"
#import "ModelLocation.h"
#import "LocationManager.h"

@interface LocationTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *vAddressInfo;
@property (weak, nonatomic) IBOutlet UILabel *labMerchantName;
@property (weak, nonatomic) IBOutlet UILabel *labMerchantAddress;
@property (weak, nonatomic) IBOutlet UILabel *labMerchantHours;
@property (weak, nonatomic) IBOutlet UIView *vDistance;
@property (weak, nonatomic) IBOutlet UILabel *labDistance;
@property (weak, nonatomic) IBOutlet UIImageView *ivLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnDirection;
@property (weak, nonatomic) IBOutlet UIView *vCellView;

- (IBAction)buttonTapped:(id)sender;

@end

@implementation LocationTableViewCell

@synthesize location = _location;

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self setupUI];
}
-(void)setLocation:(ModelLocation *)location{
    _location = location;
    [self setupUI];
}

-(ModelLocation*)location{
    return _location;
}

-(void)setupUI{
    self.labMerchantName.text = self.location.merchantName;
    self.labMerchantAddress.text = self.location.address;
    self.labMerchantHours.text = self.location.hoursString;
    self.labDistance.text = self.location.distanceString;
    self.contentView.userInteractionEnabled = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonTapped:(id)sender {
    if (self.location != nil){
        [[LocationManager instance] showRouteWithLogitude:self.location.latitude
                                                longitude:self.location.longitude
                                           destnationName:self.location.merchantName];
    }
}

@end
