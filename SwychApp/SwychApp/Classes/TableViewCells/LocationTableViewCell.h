//
//  LocationTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelLocation;

@interface LocationTableViewCell : UITableViewCell

@property(nonatomic,strong) ModelLocation *location;
@end
