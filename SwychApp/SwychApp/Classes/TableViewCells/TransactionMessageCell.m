//
//  TransactionMessageCell.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-04-08.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TransactionMessageCell.h"
#import "DisplayPicturePopupView.h"
//#import < MediaPlayer/MediaPlayer.h >
@implementation TransactionMessageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)buttonTapped:(id)sender{
    if (sender == self.btnVideo){
        DisplayPicturePopupView *v = (DisplayPicturePopupView*)[Utils loadViewFromNib:@"DisplayPicturePopupView" viewTag:1];
        v.imageURL = self.imageUrl;
        
        [v show];
        return;
    }
}
@end
