//
//  GiftcardMallTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelGiftCard;

@interface GiftcardMallTableViewCell : UITableViewCell

@property(nonatomic,strong) ModelGiftCard *giftcard;
@property (nonatomic) double amount;
-(void)hideAmountLabel;
@end
