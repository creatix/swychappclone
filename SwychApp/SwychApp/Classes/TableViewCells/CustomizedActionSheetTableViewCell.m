//
//  CustomizedActionSheetTableViewCell.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CustomizedActionSheetTableViewCell.h"
#import "ModelCustomizedActionSheetItem.h"


@implementation CustomizedActionSheetTableViewCell

@synthesize item = _item;

-(ModelCustomizedActionSheetItem*)item{
    return _item;
}

-(void)setItem:(ModelCustomizedActionSheetItem *)item{
    _item = item;
    self.ivIcon.image = item.iconImage;
    self.labName.text = item.name;
    self.ivCheckmark.image = item.selected ? [UIImage imageNamed:@"icon_checkmark.png"] : nil;
}


- (void)awakeFromNib {
    // Initialization code
    self.labName.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
