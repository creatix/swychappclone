//
//  PaymentOptionApplePayTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentOptionTableViewCell.h"

@interface PaymentOptionApplePayTableViewCell : PaymentOptionTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivApplePayLogo;
@property (weak, nonatomic) IBOutlet UIImageView *ivRightArrow;
@property (weak, nonatomic) IBOutlet UILabel *labSeparator;

@end
