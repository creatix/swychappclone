//
//  UIGiftSelection.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIGiftSelection.h"
#import "TTTAttributedLabel.h"


@interface UIGiftSelection()
@property (weak, nonatomic)  IBOutlet TTTAttributedLabel *labForFriend;
@property (weak, nonatomic)  IBOutlet TTTAttributedLabel *labForMySelf;
@property (weak, nonatomic)  IBOutlet UILabel *labLineFriend;
@property (weak, nonatomic)  IBOutlet UILabel *labLineMySelf;
@property (weak, nonatomic)  IBOutlet UILabel *labSeparator;
@property (weak, nonatomic) IBOutlet UIButton *btnFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnMySelf;
- (IBAction)buttonTapped:(id)sender;

@end

@implementation UIGiftSelection

@synthesize hideForMyselfSelection = _hideForMyselfSelection;

-(void)setHideForMyselfSelection:(BOOL)hideForMyselfSelection{
    _hideForMyselfSelection = hideForMyselfSelection;
    [self hideForMySelf:hideForMyselfSelection];
}
-(BOOL)hideForMyselfSelection{
    return _hideForMyselfSelection;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    if (self.hideForMyselfSelection){
        [self hideForMySelf:YES];
    }
}
-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UIGiftSelection" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    view.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    self.labLineFriend.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    self.labLineMySelf.backgroundColor = UICOLORFROMRGB(192, 31, 109, 1.0);
    self.labForFriend.textAlignment = NSTextAlignmentCenter;
    self.labForMySelf.textAlignment = NSTextAlignmentCenter;
    
    if (self.hideForMyselfSelection){
        [self hideForMySelf:YES];
    }
}

-(void)hideForMySelf:(BOOL)hide{
    self.btnMySelf.hidden = hide;
    self.labForMySelf.hidden = hide;
    self.labLineMySelf.hidden = hide;
}

-(void)setGiftForFriendSelected:(BOOL)giftForFriendSelected{
    _giftForFriendSelected = giftForFriendSelected;
    if (_giftForFriendSelected){
        [self.labForFriend setTextColor: UICOLORFROMRGB(76, 77, 77, 1.0)];
        self.labLineFriend.hidden = NO;
        [self.labForMySelf setTextColor: UICOLORFROMRGB(149, 149, 149, 1.0)];
        self.labLineMySelf.hidden = YES;
        
    }
    else{
        self.labForFriend.textColor = UICOLORFROMRGB(149, 149, 149, 1.0);
        self.labLineFriend.hidden = YES;
        self.labForMySelf.textColor = UICOLORFROMRGB(76, 77, 77, 1.0);
        self.labLineMySelf.hidden = NO;
    }
    if (self.selectionDelegate != nil &&[self.selectionDelegate respondsToSelector:@selector(giftSelectionChanged:forFriend:)]){
        [self.selectionDelegate giftSelectionChanged:self forFriend:_giftForFriendSelected];
    }
}

-(BOOL)giftForFriendSelected{
    return _giftForFriendSelected;
}

-(void)setTitleForFriend:(NSString*)titleFriend titleForMySelf:(NSString*)titleMySelf{
    self.labForFriend.text = titleFriend;
    self.labForMySelf.text = titleMySelf;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)buttonTapped:(id)sender {
    if(sender == self.btnFriend){
        self.giftForFriendSelected = YES;
    }
    else{
        self.giftForFriendSelected = NO;
    }
}
@end
