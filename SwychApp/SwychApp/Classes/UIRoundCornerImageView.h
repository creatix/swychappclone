//
//  UIRoundCornerImageView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@protocol UIRoundCornerImageViewDelegate;

@interface UIRoundCornerImageView : UIImageView{
    CGFloat _cornerRadius;
}

@property (nonatomic,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic,weak) id<UIRoundCornerImageViewDelegate> imageViewDelegate;
@end


@protocol UIRoundCornerImageViewDelegate <NSObject>

@optional
-(void)roundCornerImageViewTapped:(UIRoundCornerImageView*)imageView;

@end