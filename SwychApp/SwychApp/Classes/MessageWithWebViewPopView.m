//
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "MessageWithWebViewPopView.h"
#import "UIRoundCornerButton.h"
#import "SwychBaseViewController.h"
#import "NSString+Extra.h"


#import "RequestForgotPassword.h"
#import "ResponseForgotPassword.h"

#define Context_ChangePasswordSuccessfully  0x01

@interface MessageWithWebViewPopView()
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UIView *vInfo;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIWebView *htmlView;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSave;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnOK;


- (IBAction)buttonTapped:(id)sender;

@end


@implementation MessageWithWebViewPopView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.vInfo.layer.cornerRadius = 8.0f;
    
    self.labTitle.text = @"";//NSLocalizedString(@"ForgotPasswordViewTitle", nil);
    [self.btnSave setTitle:NSLocalizedString(@"Button_Done", nil)];
    self.btnCancel.hidden = YES;
    self.btnOK.hidden = YES;
}
-(void)AddUrl:(NSString*) fileName{
    _fileName = fileName;
    if(_fileName == nil || [_fileName isEqualToString:@""]){
       [self.htmlView loadHTMLString:@"" baseURL:nil];
    }
    else if ([[_fileName lowercaseString] startWith:@"http://"] || [[_fileName lowercaseString] startWith:@"https://"]){
        NSURL *url = [NSURL URLWithString:_fileName];
        NSURLRequest *req = [NSURLRequest requestWithURL:url];
        [self.htmlView loadRequest:req];
    }
    else{
        self.htmlView.backgroundColor=[UIColor clearColor];
        NSString *file_path = [[NSBundle mainBundle] pathForResource:_fileName ofType:@"html"];
        NSURLRequest * req = nil;
        @try {
            req = [NSURLRequest requestWithURL: [NSURL fileURLWithPath: file_path]];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if(req != nil){
            [self.htmlView loadRequest: req];
        }
        else{
            @try {
                [self.htmlView loadHTMLString:_fileName baseURL:nil];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        }
    }


}
-(void)AddUrl:(NSString*) fileName title:(NSString*)title{
    [self AddUrl:fileName];
    _labTitle.text = title;
    
}
-(void)AddUrl:(NSString*) fileName title:(NSString*)title type:(BOOL)type{
    if(type){
        self.btnCancel.hidden = NO;
        self.btnOK.hidden = NO;
        self.btnSave.hidden = YES;
    }
    [self AddUrl:fileName title:title];
}
- (IBAction)buttonTapped:(id)sender {

        [self dismiss];
    if(sender == _btnOK){
        if(self.uDelegate){
            [self.uDelegate NextStep:YES obj:_giftcard tag:(int)self.tag];
        }
    }


}

#pragma 
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if(alertView.context == Context_ChangePasswordSuccessfully){
        [alertView dismiss];
        [self dismiss];
    }
    return YES;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL* url = [request URL];
    if (navigationType == UIWebViewNavigationTypeLinkClicked && ([[url scheme] caseInsensitiveCompare:@"http"] == NSOrderedSame || [[url scheme] caseInsensitiveCompare:@"https"] == NSOrderedSame)){
        [[UIApplication sharedApplication] openURL:url];
        return NO;
        
    }
    else{
        return YES;
    }
    
    return YES;
}

#pragma 


@end
