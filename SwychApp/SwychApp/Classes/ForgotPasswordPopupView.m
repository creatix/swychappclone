//
//  ChangePasswordPopupView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ForgotPasswordPopupView.h"
#import "UIRoundCornerButton.h"
#import "SwychBaseViewController.h"
#import "NSString+Extra.h"


#import "RequestForgotPassword.h"
#import "ResponseForgotPassword.h"

#define Context_ChangePasswordSuccessfully  0x01

@interface ForgotPasswordPopupView()
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UIView *vInfo;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labOldPassword;
@property (weak, nonatomic) IBOutlet UILabel *labNewPassword;
@property (weak, nonatomic) IBOutlet UILabel *labConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIRoundCornerButton *btnSave;


- (IBAction)buttonTapped:(id)sender;

@end


@implementation ForgotPasswordPopupView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.vInfo.layer.cornerRadius = 8.0f;
    
    self.labTitle.text = NSLocalizedString(@"ForgotPasswordViewTitle", nil);
    self.labOldPassword.text = NSLocalizedString(@"LabelOldPassword", nil);
    self.txtOldPassword.placeholder = NSLocalizedString(@"PlaceHolder_OldPassword", nil);
    self.labNewPassword.text = NSLocalizedString(@"LabelNewPassword", nil);
    self.txtNewPassword.placeholder= NSLocalizedString(@"PlaceHolder_NewPassword", nil);
    self.labConfirmPassword.text= NSLocalizedString(@"LabelConfirmPassword", nil);
    self.txtConfirmPassword.placeholder = NSLocalizedString(@"PlaceHolder_ConfirmPassword", nil);
    
    [self.btnCancel setTitle:NSLocalizedString(@"Button_Cancel", nil)];
    [self.btnSave setTitle:NSLocalizedString(@"Button_Apply", nil)];
    self.txtOldPassword.hidden = YES;
    self.labOldPassword.hidden = YES;
}
- (IBAction)buttonTapped:(id)sender {
    if(sender == self.btnCancel){
        [self dismiss];
    }
    else if (sender == self.btnSave){
        if ([NSString isNullOrEmpty:self.txtNewPassword.text] || [NSString isNullOrEmpty:self.txtConfirmPassword.text]){
            [[APP getCurrentViewController] showAlert:NSLocalizedString(@"Err_EmptyNewPassword", nil)];
            return;
        }
        if (![self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text]){
            [[APP getCurrentViewController] showAlert:NSLocalizedString(@"Err_NewPasswordNotMatch", nil)];
            return;
        }
        [self sendForgotPasswordRequest:self.txtOldPassword.text newPassowrd:self.txtNewPassword.text];
    }
}

#pragma 
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if(alertView.context == Context_ChangePasswordSuccessfully){
        [alertView dismiss];
        if(self.uDelegate)
         [self.uDelegate AfterUpdateNewPassword:self.txtNewPassword.text phone:self.phoneNumber];

        [self dismiss];
    }
    return YES;
}



#pragma 
#pragma mark - Server Communication
-(void)sendForgotPasswordRequest:(NSString*)oldPassword newPassowrd:(NSString*)newPassword{
    SwychBaseViewController *vc = [APP getCurrentViewController];
    [vc showLoadingView:nil];
    RequestForgotPassword *req = [[RequestForgotPassword alloc] init];
    req.phoneNumber = self.phoneNumber;
    req.newpassword = newPassword;
    req.otp = self.otp;
    SEND_REQUEST(req, handleForgotPasswordResponse, handleForgotPasswordResponse)
}
-(void)handleForgotPasswordResponse:(ResponseBase*)resposne{
    SwychBaseViewController *vc = [APP getCurrentViewController];
    [vc dismissLoadingView];
    
    if (![vc handleServerResponse:resposne]){
        [vc showAlert:NSLocalizedString(@"MSG_ForgotPasswordSuccessfully", nil)
                title:nil
             delegate:self
          firstButton:nil secondButton:nil context:Context_ChangePasswordSuccessfully contextObject:nil];
    }
}

@end
