//
//  CacheData.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ModelFilterOption;

@interface CacheData : NSObject


+(CacheData*)instance;

@property(nonatomic,strong) NSArray<ModelFilterOption*> *filterOptions;

@end
