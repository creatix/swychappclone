//
//  Constants.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define DATE_TIME_FORMATTER         @"yyyy-MM-dd hh:mm:ss a"
#define DATE_TIME_FORMATTER_WITHOUTAM         @"yyyy-MM-dd hh:mm:ss"
#define BotNewTransactionNotification @"com.swych.bot.Notification"
#define BotNewLinkNotification @"com.swych.botnewlink.Notification"
typedef NS_ENUM(NSInteger,NotificationType){
    NotificationType_AskSenderStartPurchase = 1,
    NotificationType_ReceivedNewGift,
    NotificationType_Reminder,
    NotificationType_SayThanks,
    NotificationType_RemindRecipientClaimCard,
    NotificationType_RemindSenderSentOutCardReturnedAndRedeemedForSender,
    NotificationType_BotLinkConfirm,
    NotificationType_BotGiftPurchase
};

typedef NS_ENUM(NSInteger,TransactionType){
    TransactionType_GiftSentOut         = 1,
    TransactionType_GiftReceived        = 2,
    TransactionType_SavedAsSwychBalance = 4,
    TransactionType_CashOut             = 8,
    TransactionType_Redeemed            = 16,
    TransactionType_RedeemedAsMerchantBalance   = 32
};

typedef NS_ENUM(NSInteger,PaymentStatus){
    PaymentStatus_PreAutherized = 1,
    PaymentStatus_Pending   = 2,
    PaymentStatus_Settled   = 3,
    PaymentStatus_Failed    = 4,
    PaymentStatus_Cancelled = 5
};

typedef NS_ENUM(NSInteger, LoginOption){
    LoginOption_PhoneNumber = 1,
    LoginOption_Facebook,
    LoginOption_Google,
    LoginOption_Email,
    LoginOption_TouchId,
    LoginOption_PIN
} ;

typedef NS_ENUM(NSInteger, PaymentOption){
    PaymentOption_Unknow            = 0,
    PaymentOption_SwychBalance      = 1,
    PaymentOption_ApplePay          = 2,
    PaymentOption_GoogleWallet      = 3,
    PaymentOption_PayPal            = 4,
    PaymentOption_VisaCheckout      = 5,
    PaymentOption_AmazonCheckout    = 6,
    PaymentOption_DirectCreditCard  = 7,
    PaymentOption_Bitcoin           = 8,
    PaymentOption_ScannedGiftcard   = 9,
    PaymentOption_kering            = 10,
    PaymentOption_RewardPoints      = 11,
    PaymentOption_PromotionCode     = 12,
    PaymentOption_EearnPoints       = 14
};

typedef enum {
    E_giftSentOut=1,
    E_giftReceived=2,
    E_giftUserSaved=4,
    RedeemedAsCash=8,
    RedeemedAsGiftcard=16,
    RedeemedAsMerchant=32,
    RedeemedAsSwychBalace=64,
    Paid=128,
    Delivered=129
} TransactionStatus;

typedef NS_ENUM(NSInteger,GiftCardSortOption){
    GiftCardSortOption_Favorite = 0,
    GiftCardSortOption_Tranding,
    GiftCardSortOption_Newest,
    GiftCardSortOption_PriceLowToHigh,
    GiftCardSortOption_PriceHighToLow
};

typedef NS_ENUM(NSInteger,GiftCardFilterOption){
    GiftCardFilterOption_Favorite = 0,
    GiftCardFilterOption_Entertainment,
    GiftCardFilterOption_Online,
    GiftCardFilterOption_Clothing,
    GiftCardFilterOption_Restaurants,
    GiftCardFilterOption_Electronics,
    GiftCardFilterOption_Sports,
    GiftCardFilterOption_Outdoors,
    GiftCardFilterOption_Health,
    GiftCardFilterOption_Beauty
};


typedef NS_ENUM(NSInteger,RegistrationType){
    RegistrationType_Phone      = 1,
    RegistrationType_Email      = 2,
    RegistrationType_Facebook   = 3,
    RegistrationType_Google     = 4
};

typedef NS_ENUM(NSInteger,UserStatus){
    UserStatus_Pending = 1,
    UserStatus_Actived = 2,
    UserStatus_Disabled = 3,
    UserStatus_PendingOTPVerified = 4
};

typedef NS_ENUM(NSInteger,LoginViewType){
    LoginViewType_Phone = 1,
    LoginViewType_Facebook = 2,
    LoginViewType_Google = 3
};

typedef NS_ENUM(NSInteger,PurchaseType){
    PurchaseType_ForFriend = 1,
    PurchaseType_SelfGifting = 2,
    PurchaseType_SwychGiftCard = 3
};
// NSError defination
#define ERR_DOMAIN_PAYMENT  @"Error.com.goswych.swychapp.payment"
#define ERR_DOMAIN_CAMERA   @"Error.com.goswych.swychapp.camera"
#define ERR_DOMAIN_PASS     @"Error.com.goswych.swychapp.pass"

#define ERR_CODE_PAYMENT_ApplePay_Not_Configured    0x01001

#define ERR_CODE_CAMERA                             0x02001

#define ERR_CODE_PASS                               0x03001

// Local notification defination
static NSString *NOTI_Giftcard_Archived         = @"NOTI_Giftcard_Archived";
static NSString *NOTI_Giftcard_Unarchived       = @"NOTI_Giftcard_Unarchived";
static NSString *NOTI_ReceivedNewGift           = @"NOTI_Giftcard_Received";
static NSString *NOTI_NeedPurchaseGift          = @"NOTI_Giftcard_NeedPurchase";
static NSString *NOTI_Giftcard_Favorite         = @"NOTI_Giftcard_Favorite";

static NSString *NOTI_AvatarImageAvailable      = @"NOTI_AvatarImageAvailable";
static NSString *NOTI_UserStatusActivated       = @"NOTI_UserStatusActivated";
static NSString *NOTI_BackToHome                = @"NOTI_Back_To_Home";

static NSString *NOTI_AppWillEnterForeground    = @"NOTI_AppWillEnterForeground";

#define OTPSENTTYPE_UNKNOWN                         @"0"
#define OTPSENTTYPE_REGISTRATION                    @"1"
#define OTPSENTTYPE_EMAILVALIDATION                 @"2"
#define OTPSENTTYPE_FORGOTPASSOWRD                  @"3"
#define OTPSENTTYPE_AMAZON_EMAIL_VALIDATION         @"4"
#define OTPSENTTYPE_PAYPAL_EMAIL_VALIDATION         @"5"
#define OTPSENTTYPE_ANDROIDPAY_EMAIL_VALIDATION     @"6"
#define OTPSENTTYPE_APPLEPAY_EMAIL_VALIDATION       @"7"
#define OTPSENTTYPE_DIRECTCREDITCARD_EMAIL_VALIDATION   @"8"
#define OTPSENTTYPE_VISACHECKOUT_EMAIL_VALIDATION   @"9"
#define OTPSENTTYPE_BITCOIN_EMAIL_VALIDATION        @"10"
#define OTPSENTTYPE_KERING_EMAIL_VALIDATION         @"11"



#define FORMAT_MONEY            @"$%.02f"
#define FORMAT_MONEY_MINUS      @"-$%.02f"
#define FORMAT_MONEY_INTEGER_ONLY   @"$%.0f"

#define ERR_DOMAIN                  @"SwychAppErrorDomain"
#define ERR_CODE_INVALID_JSON_DATA  1001
#define ERR_CODE_JSON_DATA_PARSING  1002
#define ERR_CODE_EMPTY_JSON_DATA    1003

/* Local storage    */
#define KEY_USER_LOCAL_DATA         @"UserLocalData"
#define KEY_LOCAL_RES_VERSION       @"ResourceVersion"
#define KEY_APP_INSTALLED           @"AppInstalled"
#define KEY_BOT_TRANSACTION_DATA         @"BotTransactionData"
/*  Popup view tags */
#define POPUP_VIEW_TAG_NormalAlert          1
#define POPUP_VIEW_TAG_ProgressView         2
#define POPUP_VIEW_TAG_OTP_Verification     10


/*  UI constants    */
#define ROUND_CORNER_BUTTON_RADIUS      10.0f
#define GIFTCARD_ASPEC_RATIO            (300.0/190.0)

#endif /* Constants_h */
