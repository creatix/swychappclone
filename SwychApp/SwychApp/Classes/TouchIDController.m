//
//  TouchIDController.m
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-27.
//  Copyright © 2016 Swych Inc. All rights reserved.
//


#import "TouchIDController.h"

static TouchIDController *g_instance_TouchIDController;

@implementation TouchIDController

+(TouchIDController*)instance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance_TouchIDController = [[TouchIDController alloc] init];
    });
    return g_instance_TouchIDController;
}

-(BOOL)touchIDEnabled{
    NSError *err = nil;
    LAContext *context = [[LAContext alloc] init];
    return [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&err];
}

-(void)authicate:(NSString*)localizedReason withEnterPasswordEnabled:(BOOL)enablePasswordFallback withCompletion:(void(^)(TouchIDControllerResponse))completionBlock{
    LAContext *context = [[LAContext alloc] init];
    NSError *err = nil;
    if (!enablePasswordFallback){
        context.localizedFallbackTitle = @"";
    }
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&err]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:localizedReason
                          reply:^(BOOL success, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (success){
                                      if (completionBlock) {
                                          completionBlock(TouchIDControllerResponseSuccess);
                                      }
                                  }
                                  else{
                                      TouchIDControllerResponse response;
                                      switch(error.code){
                                          case LAErrorAuthenticationFailed:
                                              response = TouchIDControllerResponseAuthenticationFailed;
                                              break;
                                          case LAErrorUserCancel:
                                              response = TouchIDControllerResponseUserCancelled;
                                              break;
                                          case LAErrorSystemCancel:
                                              response = TouchIDControllerResponseSystemCancelled;
                                              break;
                                          case LAErrorUserFallback:
                                              response = TouchIDControllerResponseFallback;
                                              break;
                                          case LAErrorPasscodeNotSet:
                                              response = TouchIDControllerResponsePasscodeNotSet;
                                              break;
                                          case LAErrorTouchIDNotAvailable:
                                              response = TouchIDControllerResponseTouchIDNotAvailable;
                                              break;
                                          case LAErrorTouchIDNotEnrolled:
                                              response = TouchIDControllerResponseTouchIDNotEnrolled;
                                              break;
                                          default:
                                              response = TouchIDControllerResponseUndefined;
                                      }
                                      if (completionBlock) {
                                          completionBlock(response);
                                      }
                                  }
                              });
                          }];
    } else {
        // Could not evaluate policy; look at authError and present an appropriate message to user
        if (completionBlock){
            completionBlock(TouchIDControllerResponseTouchIDNotAvailable);
        }
    }
}

-(NSDictionary*)getTouchIDSavedInfo:(NSString*)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *val = [defaults objectForKey:key];
    return val;
    
}

-(void)saveTouchIDLoginInfo:(NSDictionary*)info withKey:(NSString*)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:info forKey:key];
    [defaults synchronize];
}
@end
