//
//  SlidingMenuTableViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UILabelExt;

@interface SlidingMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UILabelExt *labName;
@property (weak, nonatomic) IBOutlet UIImageView *ivIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constIconWidth;
@end
