//
//  HamburgerMenuView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "HamburgerMenuView.h"
#import "UILabelExt.h"
#import "UIImage+Extra.h"
#import "SlidingMenuTableViewCell.h"
#import "MenuItem.h"
#import "LocalUserInfo.h"
#import "RegistrationProfileViewController.h"

#import "PaymentViewController.h"
#import "HelpViewController.h"
#import "ReferFriendViewController.h"
#import "PromotionsViewController.h"
#import "ArchivedCardsViewController.h"
#import "SettingsViewController.h"
#import "AboutViewController.h"

#import "RequestLogOut.h"
#import "ResponseLogOut.h"

#define TABLEVIEW_CELL_HEIGHT       40

#define CELL_TAG_Unknown            0
#define CELL_TAG_Payment            1
#define CELL_TAG_Help               2
#define CELL_TAG_ReferAFriend       3
#define CELL_TAG_Promotions         4
#define CELL_TAG_ArchivedCards      5
#define CELL_TAG_Settings           6
#define CELL_TAG_About              7

#define CELL_TAG_DeleteLocalData    99
#define CELL_TAG_Logout             100


#define TABLE_CELL_Indentifier      @"SlidingMenuTableViewCell"

@interface HamburgerMenuView()
@property (weak, nonatomic) IBOutlet UIImageView *vBackground;
@property (weak, nonatomic) IBOutlet UIImageView *ivRight;

@property (weak, nonatomic) IBOutlet UIView *vMenu;
@property (weak, nonatomic) IBOutlet UIImageView *ivMenuBackground;
@property (weak, nonatomic) IBOutlet UITableView *tvMenu;

@property (weak, nonatomic) IBOutlet UIView *vUserInfo;
@property (weak, nonatomic) IBOutlet UIImageView *vUserInfoBackground;
@property (weak, nonatomic) IBOutlet UIRoundCornerImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabelExt *labUserName;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constratinMenuWidth;


@property (nonatomic,strong) NSArray *menuItems;
@end

@implementation HamburgerMenuView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [Utils addNotificationObserver:self selector:@selector(avatarImageAvailableNotificationHandler:) name:NOTI_AvatarImageAvailable object:nil];
    [Utils addNotificationObserver:self selector:@selector(userStatusActivatedNotificationHandler:) name:NOTI_UserStatusActivated object:nil];
    
    self.menuItems = [[ApplicationConfigurations instance] getSlidingMenuItems];
    
    [self.tvMenu registerNib:[UINib nibWithNibName:@"HamburgerMenuTableViewCell" bundle:nil] forCellReuseIdentifier:TABLE_CELL_Indentifier];
    self.backgroundColor = [UIColor clearColor];
    
    self.labUserName.verticalAlignment = VerticalAlignment_Middle;
    
    [self setupUserInfo];
    [self.tvMenu reloadData];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self.ivRight addGestureRecognizer:tap];

}
-(void)avatarImageAvailableNotificationHandler:(NSNotification*)noti{

}
-(void)touchOnView:(id)sender{
    [APP showHideHamburgerMenu];
}
-(void)roundCornerImageViewTapped:(UIRoundCornerImageView*)imageView{
    [APP showHideHamburgerMenu];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (localUser.userStatus == UserStatus_PendingOTPVerified){
        RegistrationProfileViewController *vc = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
        vc.user = localUser;
        [APP pushViewController:vc animated:YES];
    }
    else{
        SettingsViewController *vc = (SettingsViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"SettingsViewController"];
        [APP pushViewController:vc animated:YES];
    }
}
-(void)setupUserInfo{
    self.labUserName.text = [[ApplicationConfigurations instance] getUserFullName];
    UIImage *img = [[ApplicationConfigurations instance] getAvatarImage];
    if (img != nil){
        UIImage *scaledImage = [img scaletoSizeKeepAspect:CGSizeMake(img.size.width * self.ivAvatar.frame.size.height / img.size.height, self.ivAvatar.frame.size.height)];
        self.ivAvatar.contentMode = UIViewContentModeLeft;
        self.ivAvatar.image = scaledImage;
        self.ivAvatar.cornerRadius = self.ivAvatar.bounds.size.height / 2.0;
        _ivAvatar.imageViewDelegate = self;
    }
}    

-(void)f:(NSNotification *)noti{
    [self setupUserInfo];
}

-(void)userStatusActivatedNotificationHandler:(NSNotification*)noti{
    self.menuItems = [[ApplicationConfigurations instance] getSlidingMenuItems];
    [self.tvMenu reloadData];
}

-(void)updateInfo{
    [self setupUserInfo];
}

-(BOOL)ignoreToSetInfo:(id)object{
    return YES;
}

#pragma
#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.menuItems count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuItem *item = [self.menuItems objectAtIndex:indexPath.row];
    if ([item.text compare:@"-"] ==NSOrderedSame){
        return TABLEVIEW_CELL_HEIGHT / 2.0;
    }
    return TABLEVIEW_CELL_HEIGHT;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SlidingMenuTableViewCell"];
    MenuItem *mi = [self.menuItems objectAtIndex:indexPath.row];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if ([cell isKindOfClass:[SlidingMenuTableViewCell class]]){
        SlidingMenuTableViewCell *c = (SlidingMenuTableViewCell*)cell;\
        UIColor *clr = [[ApplicationConfigurations instance] getTextColor:NSStringFromClass([self class])];
        if (clr == nil){
            c.labName.textColor = [UIColor whiteColor];
        }
        else{
            c.labName.textColor = clr;
        }
        if (localUser.userStatus == UserStatus_PendingOTPVerified && mi.tag != CELL_TAG_Settings && mi.tag != CELL_TAG_Help && mi.tag != CELL_TAG_About && mi.tag != CELL_TAG_ReferAFriend){
            c.labName.textColor = [UIColor grayColor];
        }
        c.labName.textAlignment = NSTextAlignmentLeft;
        c.labName.verticalAlignment = VerticalAlignment_Middle;
        [self setMenuTableCellImage:c withRowIndex:indexPath.row];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MenuItem *item = [self.menuItems objectAtIndex:indexPath.row];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    if (item.isSeparator){
        return;
    }
    if (localUser.userStatus == UserStatus_PendingOTPVerified && item.tag != CELL_TAG_Settings && item.tag != CELL_TAG_Help && item.tag != CELL_TAG_About && item.tag != CELL_TAG_ReferAFriend){
        return;
    }
    [self menuItemClicked:item];
}


#pragma
#pragma mark -
-(void)setMenuTableCellImage:(SlidingMenuTableViewCell*)cell withRowIndex:(NSInteger)row{
    MenuItem *item = [self.menuItems objectAtIndex:row];
    
    cell.labName.text = item.text;
    if ([cell.labName.text compare:@"-"] == NSOrderedSame){
        cell.ivBackground.hidden = NO;
        cell.labName.hidden = YES;
        cell.ivIcon.hidden = YES;
        if (item.iconImageName != nil){
            cell.ivBackground.image = [UIImage imageNamed:item.iconImageName];
        }
        else{
            cell.ivBackground.image = nil;
        }
    }
    else{
        cell.ivBackground.hidden = YES;
        cell.labName.hidden = NO;
        cell.ivIcon.hidden = NO;
        if (item.iconImageName != nil){
            cell.ivIcon.image = [UIImage imageNamed:item.iconImageName];
        }
        if (cell.ivIcon.image == nil){
            cell.constIconWidth.constant = 0.0f;
        }
    }
}

-(void)menuItemClicked:(MenuItem*)item{
    NSInteger tag = item.tag;
    NSLog(@"Menu item with tag %li tapped", (long)tag);
    [APP showHideHamburgerMenu];
    
    if (tag == CELL_TAG_Logout){
        [[APP getCurrentViewController] showAlert:NSLocalizedString(@"PromptLogOut", nil)
                  title:nil delegate:self
            firstButton:NSLocalizedString(@"Button_NO", nil)
           secondButton:NSLocalizedString(@"Button_YES", nil)];
    }
    else if (tag == CELL_TAG_DeleteLocalData){
        [[LocalStorageManager instance] clearStoredUserInfo];
        [[APP getCurrentViewController] showAlert:@"Local data has been deleted."];
    }
    else if(tag == CELL_TAG_Help) {
        NSString *url = [[ApplicationConfigurations instance] getHelpURL];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    else if(tag == CELL_TAG_Promotions) {
        PromotionsViewController *vc = (PromotionsViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"PromotionsViewController"];
        [APP pushViewController:vc animated:YES];
    }
    else if(tag == CELL_TAG_Payment) {
        PaymentViewController *vc = (PaymentViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"PaymentViewController"];
        [APP pushViewController:vc animated:YES];
    }
    else if(tag == CELL_TAG_ReferAFriend) {
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if(localUser.userStatus == UserStatus_PendingOTPVerified){
            RegistrationProfileViewController *vc = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
            vc.user = localUser;
            [APP pushViewController:vc animated:YES];
        }
        else{
            ReferFriendViewController *vc = (ReferFriendViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"ReferFriendViewController"];
            [APP pushViewController:vc animated:YES];
        }
    }
    else if(tag == CELL_TAG_About) {
        AboutViewController *vc = (AboutViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"AboutViewController"];
        [APP pushViewController:vc animated:YES];
    }
    else if(tag == CELL_TAG_Settings) {
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if (localUser.userStatus == UserStatus_PendingOTPVerified){
            RegistrationProfileViewController *vc = (RegistrationProfileViewController*)[Utils loadViewControllerFromStoryboard:@"Registration" controllerId:@"ProfileViewController"];
            vc.user = localUser;
            [APP pushViewController:vc animated:YES];
        }
        else{
            SettingsViewController *vc = (SettingsViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"SettingsViewController"];
            [APP pushViewController:vc animated:YES];
        }
    }
    else if(tag == CELL_TAG_ArchivedCards) {
        ArchivedCardsViewController *vc = (ArchivedCardsViewController*)[Utils loadViewControllerFromStoryboard:@"Settings" controllerId:@"ArchivedCardsViewController"];
        [APP pushViewController:vc animated:YES];
    }
    
}
#pragma
#pragma mark - AlertViewDelegate
-(BOOL)alertView:(AlertViewNormal*)alertView buttonTappedWithIndex:(NSInteger)index{
    if (index == 2){//Yes button to log out current session
        LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
        if(localUser.touchIDEnabled){
            [[LocalStorageManager instance] saveLocalUserInfo:localUser];
            AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [app switchToLoginView:NO preview:@""];
            return YES;
        }
        [self sendLogOutRequest];
    }
    return YES;
}

#pragma
#pragma mark - Server Communication
-(void)sendLogOutRequest{
    [[APP getCurrentViewController] showLoadingView:nil];
    
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestLogOut *req = [[RequestLogOut alloc] init];
    req.token = localUser.sessionToken;
    
    SEND_REQUEST(req, handleLogOutResponse, handleLogOutResponse);
}

-(void)handleLogOutResponse:(ResponseBase*)resposne{
    [[APP getCurrentViewController] dismissLoadingView];
    LocalUserInfo *localUser = [[LocalStorageManager instance] getLocalStoredUserInfo];
    localUser.sessionToken = nil;
    
    [APP setCallContactSyncResetNeeded:YES];
    [[LocalStorageManager instance] saveLocalUserInfo:localUser];
    [APP switchToLoginView:NO preview:@""];
}

@end
