//
//  ContactBookManager.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ContactBookManager.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Contacts/Contacts.h>
#import "ModelContact.h"
#import "AContactItem.h"
@implementation ContactBookManager
+(ContactBookManager*)instance{
    static ContactBookManager *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[self alloc] init];
    });
    return g_instance;
}


-(NSArray*)getAllContacts{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    //iOSVersion = 1.0; //force to use ABAddressBook;
    if (iOSVersion < 9.0){
        ABAddressBookRef addressBook = ABAddressBookCreate();
        __block BOOL accessGranted = NO;
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

        if (accessGranted) {
            NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
            for (id record in allContacts){
                ABRecordRef thisContact = (__bridge ABRecordRef)record;
                ModelContact *c = [[ModelContact alloc] initWithABRecordRef:thisContact];
                if (c != nil){
                    [array addObject:c];
                }
            }
        }
    }
    else{
        CNContactStore *store = [[CNContactStore alloc] init];
        __block BOOL accessGranted = NO;
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        if (accessGranted) {
            
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey,CNContactBirthdayKey,CNContactEmailAddressesKey];
            
                NSError *error;
            
            NSArray* allContainers = [store containersMatchingPredicate:nil error:&error];
            for (CNContainer* container in allContainers)
            {
                NSString *containerId = container.identifier;
                
                NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
                NSError *error;
                NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
                if (error) {
                    NSLog(@"error fetching contacts %@", error);
                } else {
                    for (CNContact *contact in cnContacts) {
                        CNMutableContact * contactMu = [contact mutableCopy];
                        ModelContact *c = [[ModelContact alloc] initWithCNContact:contactMu];
                        if (c != nil){
                            [array addObject:c];
                        }
                    }
                }
            }
        }
    }
    
    
    return array;
}

+(UIImage*)getContactHeadImgByPhoneNumber:(NSString*)phone{
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    for(int i = 0;i<array.count;i++){
        ModelContact * item = (ModelContact*)array[i];
        if([item hasPhoneNumber:phone]){
            return item.profileImage;
        }
    }
    return nil;
}

+(NSString*)getContactFirstNameByPhoneNumber:(NSString*)phone{
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    for(int i = 0;i<array.count;i++){
        ModelContact * item = (ModelContact*)array[i];
        if([item hasPhoneNumber:phone]){
            return item.firstName;
        }
        
    }
    return nil;
}
+(NSArray*)getContactItemList:(ModelContact*)user{
    AContactItem* item;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(int i = 0; i<user.phoneNumbers.count;i++){
        item = [AContactItem new];
        item.contentType = PHONE_CELL;
        item.parentModal = user;
        item.Content_Item = user.phoneNumbers[i];
        [array addObject:item];
    }
    for(int i = 0; i<user.emails.count;i++){
        item = [AContactItem new];
        item.contentType = EMAIL_CELL;
        item.parentModal = user;
        item.Content_Item = user.emails[i];
        [array addObject:item];
    }
    return array;
}
+(NSArray*)getContactItemListByModelList:(NSArray*)users{
    AContactItem* item;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if(!users) return nil;
    for(int j = 0;j<users.count;j++){
        ModelContact* model = users[j];
        for(int i = 0; i<model.phoneNumbers.count;i++){
            item = [AContactItem new];
            item.contentType = PHONE_CELL;
            item.parentModal = model;
            item.Content_Item = model.phoneNumbers[i];
            [array addObject:item];
        }
        for(int i = 0; i<model.emails.count;i++){
            item = [AContactItem new];
            item.contentType = EMAIL_CELL;
            item.parentModal = model;
            item.Content_Item = model.emails[i];
            [array addObject:item];
        }
    }
    return array;
}
@end
