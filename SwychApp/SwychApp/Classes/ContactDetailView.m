//
//  ContactDetailView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ContactDetailView.h"
#import "ModelContact.h"
#import "AContactItem.h"
#import "ContactListTableViewCell.h"
@implementation ContactDetailView

@synthesize contact = _contact;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code6
}
*/
-(void)awakeFromNib{
    [super awakeFromNib];
    [self.tvList registerNib:[UINib nibWithNibName:@"ContactListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ContactListTableViewCell"];
    self.lblBirthday.hidden = YES;
    self.lblBirthdayTitle.hidden = YES;
    self.imgBirthdayBg.hidden = YES;
    self.tvList.hidden = YES;
    self.lblBirthday.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnBirthdayLabel:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    tap.delegate = self;
    [self.lblBirthday addGestureRecognizer:tap];
//    self.tvList.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    [self.tvList setSeparatorColor:[UIColor lightGrayColor]];
}

-(ModelContact*)contact{
    return _contact;
}

-(void)setContact:(ModelContact *)contact{
    self.lblBirthday.hidden = NO;
    self.lblBirthdayTitle.hidden = NO;
    self.imgBirthdayBg.hidden = NO;
    self.tvList.hidden = NO;
    if(!contact){
        self.lblBirthday.hidden = YES;
        self.lblBirthdayTitle.hidden = YES;
        self.imgBirthdayBg.hidden = YES;
        self.tvList.hidden = YES;
    }
    _contact = contact;
    [self.tvList reloadData];
    
    
    NSDate *birthday = _contact.birthday;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    formatter.dateFormat = @"MM/dd/yyyy";
    self.BirthdayText =  [formatter stringFromDate:birthday];
    self.lblBirthday.text = [formatter stringFromDate:birthday];

    if (contact.emailsAndTitle.count + contact.phoneNumbersAndTitle.count == 1){
        NSString *selectItem = nil;
        BOOL isPhoneNumber = NO;
        if(contact.phoneNumbersAndTitle.count == 1){
            isPhoneNumber = YES;
            AContactItem *item =contact.phoneNumbersAndTitle[0];
            selectItem = item.Content_Item;
        }
        else{
            isPhoneNumber = NO;
            AContactItem *item = contact.emailsAndTitle[0];
            selectItem = item.Content_Item;
        }
        if (self.detailViewDelegate != nil && [self.detailViewDelegate respondsToSelector:@selector(contactDetailView:contact:selected:isPhoneNumber:)]){
            [self.detailViewDelegate contactDetailView:self contact:contact selected:selectItem isPhoneNumber:isPhoneNumber];
        }
    }
    

}
-(void)touchOnBirthdayLabel:(id)sender{
    CustomizedInputView *input = [CustomizedInputView  createInstanceWithViewType:CustomizedInputView_Date objectWithInput:self.lblBirthday];
    input.delegate = self;
    [input show];
}
#pragma 
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.contact == nil){
        return 0;
    }
    else{
        NSInteger num =  1 + ([self.contact.phoneNumbersAndTitle count] > 0 ? 1 : 0) + ([self.contact.emails count] > 0 ? 1 : 0);
        return num;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            if ([self.contact.phoneNumbersAndTitle count] > 0){
                return [self.contact.phoneNumbersAndTitle count];
            }
            else{
                return [self.contact.emails count];
            }
            break;
        case 2:
            return [self.contact.emails count];
            break;
        default:
            break;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"ContactListTableViewCell";
    ContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil){
        cell = [[ContactListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.contact = self.contact;
    cell.txtContent.delegate = cell;
    
    if (indexPath.section == 0){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if (indexPath.section == 1){
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        if ([self.contact.phoneNumbersAndTitle count] > 0){
            AContactItem* item = [self.contact.phoneNumbersAndTitle objectAtIndex:indexPath.row];
            cell.txtContent.text = item.Content_Item;
            cell.labTitle.text = item.Title_Item;
            cell.txtContent.keyboardType = UIKeyboardTypeNumberPad;
            cell.ContentOfContact = item.Content_Item;
            cell.icontactType = PHONE_CELL;
        }
        else{
            AContactItem* item = [self.contact.emailsAndTitle objectAtIndex:indexPath.row];
            cell.txtContent.text = item.Content_Item;//[self.contact.emails objectAtIndex:indexPath.row];
            cell.labTitle.text = item.Title_Item;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            cell.ContentOfContact = item.Content_Item;
            cell.icontactType = EMAIL_CELL;
        }
    }
    else {
        AContactItem* item = [self.contact.emailsAndTitle objectAtIndex:indexPath.row];
        cell.txtContent.text = item.Content_Item;//[self.contact.emails objectAtIndex:indexPath.row];
        cell.labTitle.text = item.Title_Item;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.ContentOfContact = item.Content_Item;
        cell.icontactType = EMAIL_CELL;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){
        return;
    }
    NSString *item = nil;
    BOOL isPhoneNumber = NO;
    ContactListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    isPhoneNumber = (cell.icontactType == PHONE_CELL);
    item = cell.txtContent.text;
    
    if (self.detailViewDelegate != nil && [self.detailViewDelegate respondsToSelector:@selector(contactDetailView:contact:selected:isPhoneNumber:)]){
        [self.detailViewDelegate contactDetailView:self contact:self.contact selected:item isPhoneNumber:isPhoneNumber];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.0f;
}


#pragma
#pragma mark - CustomizedInputViewDelegate
-(void)customizedInputViewValueChanged:(CustomizedInputView*)inputView{
    NSDate *dateSelected = [inputView getDate];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    self.lblBirthday.text = [formatter stringFromDate:dateSelected];

}
-(void)customizedInputviewDismissed:(CustomizedInputView*)inputView doneButtonTapped:(BOOL)done{
    if(done){
        NSDate *dateSelected = [inputView getDate];
        [_contact changeBirthday:dateSelected];
    }
    else{
        self.lblBirthday.text = self.BirthdayText;
    }
}
@end
