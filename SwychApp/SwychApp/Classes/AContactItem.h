//
//  ContactItem.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-03-21.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelContact.h"
#define EMAIL_CELL 100
#define PHONE_CELL 101
@interface AContactItem : NSObject
@property(strong,nonatomic) NSString *Title_Item;
@property(strong,nonatomic) NSString *Content_Item;
@property(strong,nonatomic) ModelContact *parentModal;
@property(nonatomic) int contentType;
@end
