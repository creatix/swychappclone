//
//  LocalUserInfo.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "LocalUserInfo.h"

@implementation LocalUserInfo

@synthesize avatarImageURL = _avatarImageURL;

-(void)setAvatarImageURL:(NSString *)avatarImageURL{
    _avatarImageURL = avatarImageURL;
    
}
-(NSString*)avatarImageURL{
    return _avatarImageURL;
}

-(id)initWithUser:(ModelUser*)user{
    if (self = [super init]){
        [self copyFromUser:user];
    }
    return self;
}

-(void)copyFromUser:(ModelUser*)user{
    self.firstName = user.firstName;
    self.lastName = user.lastName;
    self.email = user.email;
    self.swychId = user.swychId;
    self.phoneNumber = user.phoneNumber;
    self.createdTime = user.createdTime;
    self.password = user.password;
    self.userStatus = user.userStatus;
    self.swychBalance = user.swychBalance;
    self.swychPoint = user.swychPoint;
    self.swychPointValue = user.swychPointValue;
    self.encryptedPassword = user.encryptedPassword;
    self.regType = user.regType;
    self.avatarImageURL = user.avatar;
    self.referalCode = user.referalCode;
    self.referalLink = user.referalLink;
    self.referalRecipientCredit = user.referalRecipientCredit;
    self.referalSenderCredit = user.referalSenderCredit;
    self.botLinkingCode = user.botLinkingCode;
}

-(NSString*)getUserFullName{
    if ([self.firstName length] == 0 && [self.lastName length] == 0){
        return NSLocalizedString(@"Default_User_FullName", nil);
    }
    NSString *name = [NSString stringWithFormat:@"%@ %@",[self.firstName length]  == 0 ? @"" : self.firstName,
                                                          [self.lastName length] == 0 ? @"" : self.lastName];
    NSMutableString *str = [NSMutableString stringWithString:name];
    CFStringTrimWhitespace((__bridge CFMutableStringRef)str);
    
    return str;
}

-(BOOL)isJSONIgnoredProperty:(NSString *)propName{
    if ([propName caseInsensitiveCompare:@"avatarImage"] == NSOrderedSame){
        return  YES;
    }
    return [super isJSONIgnoredProperty:propName];
}

@end
