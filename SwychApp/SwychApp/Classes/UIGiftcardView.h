//
//  UIGiftcardView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,GiftcardViewType){
    GiftcardView_Normal = 1,
    GiftcardView_Empty_Discount,
    GiftcardView_Empty_StoreCard,
    GiftcardView_Empty_More,
    GiftcardView_GiftcardImageOnly,
    GiftcardView_NormalWithDiscount,
    GiftcardView_Normal_bigger_bottom
};

@class ModelGiftCard;
@protocol UIGiftcardViewDelegate;
@protocol UIGiftcardViewDelegate;
IB_DESIGNABLE
@interface UIGiftcardView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic,assign) IBInspectable BOOL isFavorite;
@property (nonatomic,weak) IBInspectable UIImage *giftcardImage;
@property (nonatomic,weak) IBInspectable NSString *giftcardName;
@property (nonatomic,strong) ModelGiftCard *giftcard;
@property (nonatomic,weak) IBInspectable id<UIGiftcardViewDelegate> giftcardViewDelegate;
@property (nonatomic,assign) IBInspectable BOOL frontOnly;
@property (nonatomic,assign) IBInspectable BOOL frontViewAsCurrent;
@property (nonatomic,assign) IBInspectable BOOL ifShowBackViewWithLogo;
@property (nonatomic,assign) GiftcardViewType viewType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrainFontGiftcardImageBottom;
@property (weak, nonatomic) IBOutlet UILabel *labFrontViewAmount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabFrontViewAmountWidth;

-(void)updateViews;
- (IBAction)buttonTapped:(id)sender;
@end

@protocol UIGiftcardViewDelegate <NSObject>

@optional
-(void)giftcardViewTapped:(UIGiftcardView*)giftcardView;
- (void)buttonTapped:(int)btnIndex;

@end