//
//  ModelPaymentMethodSettings.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelPaymentMethodSettings : NSObject

@property(nonatomic,strong) NSString *applePayStripePublishableKey;
@property(nonatomic,strong) NSString *applePayMerchantIdentifier;
@property(nonatomic,strong) NSString *payPalMerchantProductionKey;
@property(nonatomic,strong) NSString *payPalMerchantTestKey;
@property(nonatomic,assign) BOOL paypalKeyIsProdKey;
@property(nonatomic,strong) NSString *directCreditCardURL;
@property(nonatomic,strong) NSString *amazonAWSAccessKeyId;
@property(nonatomic,assign) BOOL amazonIsLiveEnv;
@property(nonatomic,strong) NSString *brainTreeClientId;

@end
