//
//  TouchIDController.h
//  SwychApp
//
//  Created by knupro-idev1 on 2016-06-27.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>


typedef NS_ENUM(NSInteger,TouchIDControllerResponse){
    TouchIDControllerResponseUndefined,
    TouchIDControllerResponseSuccess,
    TouchIDControllerResponseAuthenticationFailed,
    TouchIDControllerResponseUserCancelled,
    TouchIDControllerResponseSystemCancelled,
    TouchIDControllerResponseFallback,
    TouchIDControllerResponsePasscodeNotSet,
    TouchIDControllerResponseTouchIDNotAvailable,
    TouchIDControllerResponseTouchIDNotEnrolled
};

@interface TouchIDController : NSObject {
    
}

+(TouchIDController*)instance;

-(BOOL)touchIDEnabled;
-(void)authicate:(NSString*)localizedReason withEnterPasswordEnabled:(BOOL)enablePasswordFallback withCompletion:(void(^)(TouchIDControllerResponse))completionBlack;
-(NSDictionary*)getTouchIDSavedInfo:(NSString*)key;
-(void)saveTouchIDLoginInfo:(NSDictionary*)info withKey:(NSString*)key;
@end

