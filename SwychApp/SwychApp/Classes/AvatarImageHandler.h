//
//  AvatarImageHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RSKImageCropper/RSKImageCropper.h>

@protocol AvatarImageHandlerDelegate;
@interface AvatarImageHandler : NSObject<RSKImageCropViewControllerDelegate>

+(AvatarImageHandler*)instance;
-(void)handleAvatarImage:(UIImage*)source viewController:(UIViewController*)viewController delegate:(id<AvatarImageHandlerDelegate>)del;
@end


@protocol AvatarImageHandlerDelegate <NSObject>

@optional
-(void)avatarImageHandler:(AvatarImageHandler*)handler didSelectedImage:(UIImage*)selectedImage;
-(void)avatarImageHdnalerDidCancelled:(AvatarImageHandler*)handler;

@end