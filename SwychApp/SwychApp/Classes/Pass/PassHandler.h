//
//  PassHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PassKit/PassKit.h>

@protocol PassHandlerDelegate;

@interface PassHandler : NSObject

@property(nonatomic,weak) id<PassHandlerDelegate> delegate;

+(PassHandler*)instance;
-(BOOL)RemovePass:(NSString*)serialNumber;

-(BOOL)passExists:(NSString*)serialNumber;
-(void)GeneratePassFromServer:(NSData*)data delegate:(id<PassHandlerDelegate>)del;
-(BOOL)isPassLibAvailable;

@end


@protocol PassHandlerDelegate <NSObject>

@optional
-(void)beforePresentAddPassController;
-(void)afterPresentAddPassController;
-(void)addPassViewWillDismissWithCancelButtonTapped:(BOOL)cancelButtonTapped addPass:(BOOL)success error:(NSError*)error ;

@end