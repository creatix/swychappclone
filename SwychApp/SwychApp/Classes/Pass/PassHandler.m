//
//  PassHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PassHandler.h"
#import "Utils.h"
#import "AppDelegate.h"

@interface PassHandler()

@property(nonatomic,strong) PKPass *currentPass;

@end
@implementation PassHandler

+(PassHandler*)instance{
    static PassHandler *g_PassHandlerInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_PassHandlerInstance = [[self alloc] init];
    });
    return g_PassHandlerInstance;
}

-(BOOL)isPassLibAvailable{
    return YES;
}

-(BOOL)RemovePass:(NSString*)serialNumber{
    PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
    if(passLib.passes.count == 0){
        return YES;
    }
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(PKPass *pass, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [pass.serialNumber isEqualToString:serialNumber];
    }];
    NSArray *array = [passLib.passes filteredArrayUsingPredicate:pred];
    if(array.count > 0){
        [passLib removePass:[array objectAtIndex:0]];
        if ([passLib containsPass:array[0]]){
            return NO;
        }
        else{
            return YES;
        }
    }
    return  YES;
}

-(BOOL)passExists:(NSString*)serialNumber{
    PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
    if (passLib.passes.count == 0){
        return NO;
    }
    NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(PKPass *pass, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [pass.serialNumber isEqualToString:serialNumber];
    }];
    NSArray *array = [passLib.passes filteredArrayUsingPredicate:pred];
    return array.count > 0;
}
-(void)GeneratePassFromServer:(NSData*)data delegate:(id<PassHandlerDelegate>)del{
    NSError * passerror;
    self.delegate = del;
    
    PKPass * pass = [[PKPass alloc] initWithData:data error:&passerror];
    
    if(!pass) {
        NSString *errorMessage = NSLocalizedString(@"InvalidPassData", nil);
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(addPassViewWillDismissWithCancelButtonTapped:addPass:error:)]){
            NSError *err = [NSError errorWithDomain:ERR_DOMAIN_PASS code:ERR_CODE_PASS userInfo:@{NSLocalizedDescriptionKey : errorMessage}];
            [self.delegate addPassViewWillDismissWithCancelButtonTapped:NO addPass:NO error:err];
        }
        else{
            [Utils showAlert:errorMessage delegate:nil];
        }
        return;
    }
    
    //init a pass library
    PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
    //check if pass library contains this pass already
    if([passLib containsPass:pass]) {
        //pass already exists in library, show an error message
        NSString *errorMessage = NSLocalizedString(@"Error_PassExists", nil);
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(addPassViewWillDismissWithCancelButtonTapped:addPass:error:)]){
            NSError *err = [NSError errorWithDomain:ERR_DOMAIN_PASS code:ERR_CODE_PASS userInfo:@{NSLocalizedDescriptionKey : errorMessage}];
            [self.delegate addPassViewWillDismissWithCancelButtonTapped:NO addPass:NO error:err];
        }
        else{
            [Utils showAlert:errorMessage delegate:nil];
        }
        
    } else {
        if(![PKAddPassesViewController canAddPasses]){
        [Utils showAlert:@"This device can not add pass." delegate:nil];
            return;
        }
        //present view controller to add the pass to the library
        PKAddPassesViewController *vc = [[PKAddPassesViewController alloc] initWithPass:pass];
        vc.delegate = (id<PKAddPassesViewControllerDelegate>)self;
        
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        UIViewController *rootViewController = (UIViewController*)[app getCurrentViewController];
        self.currentPass = pass;
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(beforePresentAddPassController)] ){
            [self.delegate beforePresentAddPassController];
        }
        [rootViewController presentViewController:vc animated:YES completion:^{
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(afterPresentAddPassController)] ){
                [self.delegate afterPresentAddPassController];
            }
        }];
    }
}


#pragma 
#pragma mark - PKAddPassesViewControllerDelegate
-(void)addPassesViewControllerDidFinish:(PKAddPassesViewController *)controller{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UIViewController *rootViewController = (UIViewController*)[app getCurrentViewController];
    [rootViewController dismissViewControllerAnimated:YES completion:^{
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(addPassViewWillDismissWithCancelButtonTapped:addPass:error:)]){
            PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
            if (self.currentPass != nil && [passLib containsPass:self.currentPass]){
                [self.delegate addPassViewWillDismissWithCancelButtonTapped:NO addPass:YES error:nil];
                self.currentPass = nil;
            }
            else{
                [self.delegate addPassViewWillDismissWithCancelButtonTapped:YES addPass:NO error:nil];
            }
            self.delegate = nil;
        }
    }];
    
}
@end
