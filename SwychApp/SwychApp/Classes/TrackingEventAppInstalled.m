//
//  TrackingEventAppInstalled.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventAppInstalled.h"

@implementation TrackingEventAppInstalled

-(id)init{
    self = [super init];
    if (self){
        self.eventCode = @"3spxd3";
        self.eventName = ANA_EVENT_AppInstalled;
    }
    return self;
}
@end
