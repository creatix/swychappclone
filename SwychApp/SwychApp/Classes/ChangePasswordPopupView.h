//
//  ChangePasswordPopupView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupConfirmationView.h"

@interface ChangePasswordPopupView : PopupConfirmationView

@end
