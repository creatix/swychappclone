//
//  TrackingEventOTPCompleted.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventOTPCompleted.h"

@implementation TrackingEventOTPCompleted
-(id)init{
    self = [super init];
    if (self){
        self.eventName = ANA_EVENT_OTPCompleted;
        self.eventCode = @"q8kzaw";
    }
    return self;
}

@end
