//
//  TrackingEventCardUploaded.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventCardUploaded.h"

@implementation TrackingEventCardUploaded
-(id)init{
    self = [super init];
    if (self){
        self.eventCode = @"a6qwtv";
        self.eventName = ANA_EVENT_CardUploaded;
    }
    return self;
}

@end
