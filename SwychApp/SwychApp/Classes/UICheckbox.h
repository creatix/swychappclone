//
//  UICheckbox.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UICheckboxDelegate;
IB_DESIGNABLE
@interface UICheckbox : UIView

@property (nonatomic,assign) IBInspectable BOOL checked;
@property (nonatomic,weak) IBInspectable UIImage *checkedImage;
@property (nonatomic,weak) IBInspectable UIImage *unCheckedImage;
@property (nonatomic,weak) IBInspectable NSString *text;
@property (nonatomic,assign) IBInspectable BOOL showImageOnly;

@property (weak,nonatomic) id<UICheckboxDelegate> checkboxDelegate;
@end

@protocol UICheckboxDelegate <NSObject>

-(void)checkbox:(UICheckbox*)sender stateChanged:(BOOL)state;
-(BOOL)checkbox:(UICheckbox*)sender stateWillChange:(BOOL)state;
@end