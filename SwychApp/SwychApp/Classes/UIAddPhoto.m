//
//  UIAddPhoto.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIAddPhoto.h"
#import "TTTAttributedLabel.h"

@interface UIAddPhoto(){
    IBOutlet UIImageView *_ivPlus;
    IBOutlet UIImageView *_ivSelected;
    IBOutlet TTTAttributedLabel *_labText;
}

@end

@implementation UIAddPhoto

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(void)setAddPhotoText:(NSString *)text{
    _labText.text = text;
}

-(UIImage*)selectedImage{
    return _ivSelected.image;
}
-(void)setSelectedImage:(UIImage *)selectedImage{
    _ivSelected.image = selectedImage;
    _ivPlus.hidden = selectedImage == nil ? NO : YES;
    _labText.hidden = selectedImage == nil ? NO : YES;
    _ivSelected.hidden = selectedImage == nil ? YES : NO;
}

-(UIImage*)plusImage{
    return _ivPlus.image;
}
-(void)setPlusImage:(UIImage *)plusImage{
    _ivPlus.image = plusImage;
}

-(NSString *)text{
    return _labText.text;
}

-(void)setText:(NSString *)text{
    _labText.text = text;
}

-(void)loadView{
    UIView *view =[Utils loadViewFromNib:@"UIAddPhoto" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:view];
    
    NSLayoutConstraint *constraintPlus = [NSLayoutConstraint
                                               constraintWithItem:_ivPlus
                                               attribute:NSLayoutAttributeHeight
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:_ivPlus.superview
                                               attribute:NSLayoutAttributeHeight
                                               multiplier:0.5
                                               constant:0];
    [_ivPlus.superview addConstraint:constraintPlus];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
}

-(void)touchOnView:(id)sender{
    if (self.addPhotoDelegate != nil && [self.addPhotoDelegate respondsToSelector:@selector(addPhotoTapped:)]){
        [self.addPhotoDelegate addPhotoTapped:self];
    }
}

@end
