//
//  PopupViewBase.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PopupViewBase.h"
#import "AppDelegate.h"
#import "PopupViewHandler.h"

@implementation PopupViewBase

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        self.layer.cornerRadius = 10.0f;
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    return self;
}
-(void)adjustControls{
    
}

+(void)dismissCurrentPopup{
    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [app.popupViewHandler popView];
    
}
-(void)dismiss{
    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [app.popupViewHandler popView:self];
}

-(BOOL)allowTouchToDismiss{
    return NO;
}
@end
