//
//  MenuItem.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

@synthesize text,tag,sortOrder,iconImageName;
@dynamic isSeparator;

-(BOOL)isSeparator{
    return [self.text compare:@"-"] == NSOrderedSame;
}
@end
