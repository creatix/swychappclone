//
//  ModelCustomizedActionSheetItem.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelCustomizedActionSheetItem.h"

@implementation ModelCustomizedActionSheetItem

-(id)initWithId:(NSInteger)Id withName:(NSString*)name withIcon:(UIImage*)image{
    if (self = [super init]){
        self.name = name;
        self.iconImage = image;
        self.itemId = Id;
    }
    return self;
}
@end
