//
//  ModelGiftcardOrderInfo.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ModelGiftCard;
@class ModelPaymentInfo;

@interface ModelGiftcardOrderInfo : NSObject

@property (assign,nonatomic) BOOL giftForFriend;
@property (nonatomic,strong) ModelGiftCard *giftcard;
@property (nonatomic,assign) CGFloat amount;
@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) UIImage *imagePhoto;
@property (nonatomic,strong) NSDate *dateDelivery;
@property (nonatomic,strong) NSString *recipientPhoneNumberOrEmail;
@property (nonatomic,assign) BOOL recipientIdenIsPhoneNumber;

@property (nonatomic,strong) NSArray<ModelPaymentInfo*> * payments;
@property (nonatomic,assign) double amountDue;

@property (nonatomic,assign) BOOL forSwych;
@property (nonatomic,strong) ModelGiftCard *swychToGiftCard;
@property (nonatomic,assign) double swychToAmount;
@property (nonatomic,assign) double swychFromAmount;


@end
