//
//  ModelCustomizedActionSheetItem.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelCustomizedActionSheetItem : NSObject

@property (nonatomic,assign) NSInteger itemId;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) UIImage *iconImage;
@property (nonatomic,assign) BOOL selected;

-(id)initWithId:(NSInteger)Id withName:(NSString*)name withIcon:(UIImage*)image;
@end
