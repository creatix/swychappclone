//
//  ModelCreditPointsForPurchase.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ModelGiftCard;

@interface ModelCreditPointsForPurchase : NSObject

@property (nonatomic,assign) double creditUpTo;
@property (nonatomic,assign) NSInteger pointsUpTo;

@property (nonatomic,strong) NSArray<ModelGiftCard*> *creditGiftcardList;

@property (nonatomic,readonly) BOOL empty;


@end
