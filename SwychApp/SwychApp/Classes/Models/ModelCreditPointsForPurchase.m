//
//  ModelCreditPointsForPurchase.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelCreditPointsForPurchase.h"

@implementation ModelCreditPointsForPurchase

-(BOOL)empty{
    return self.creditUpTo == 0.0 && self.pointsUpTo == 0;
}

@end
