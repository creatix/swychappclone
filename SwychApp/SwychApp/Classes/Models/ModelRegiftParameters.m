//
//  ModelRegiftParameters.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelRegiftParameters.h"
#import "ModelTransaction.h"
#import "ModelGiftCard.h"

@implementation ModelRegiftParameters

-(ModelGiftCard*)originalGiftcard{
    return self.originalTransaction != nil ? self.originalTransaction.giftCard : nil;
}

-(double)amount{
    return self.originalTransaction != nil ? self.originalTransaction.amount : 0.0;
}
@end
