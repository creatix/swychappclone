//
//  ModelRegiftParameters.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ModelGiftCard;
@class ModelTransaction;

@interface ModelRegiftParameters : NSObject

@property(nonatomic,strong) ModelTransaction *originalTransaction;
@property(nonatomic,readonly) ModelGiftCard *originalGiftcard;
@property(nonatomic,readonly) double amount;
@property(nonatomic,strong) NSString *selectedContactItem;
@property(nonatomic,assign) BOOL selectedContactItemIsPhoneNumber;

@end
