//
//  ModelAppleNotification.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAppleNotification.h"
#import "NSObject+Extra.h"


@implementation ModelAPS

-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"content-available"]){
        return @"contentAvailable";
    }
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"contentAvailable"]){
        return @"content-available";
    }
    return [super getJSONKeyNameInProperties:propertyName];
}

@end

@implementation ModelNotiGift


@end

@implementation ModelAppleNotification

@end
