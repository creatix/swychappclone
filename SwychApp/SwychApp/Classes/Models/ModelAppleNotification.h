//
//  ModelAppleNotification.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "CommunicationModelBase.h"

@interface ModelAPS : NSObject

@property(nonatomic,strong) NSString *alert;
@property(nonatomic,assign) NSInteger badge;
@property(nonatomic,strong) NSString *sound;
@property(nonatomic,assign) NSInteger contentAvailable;
@end

@interface ModelNotiGift : NSObject
@property(nonatomic,assign) NSInteger gid;
@property(nonatomic,assign) NSInteger tid;
@property(nonatomic,assign) NSInteger amount;
@property(nonatomic,strong) NSString *gname;
@end

@interface ModelAppleNotification : NSObject

@property(nonatomic,strong) ModelAPS *aps;
@property(nonatomic,strong) ModelNotiGift *gift;
@property(nonatomic,assign) NSInteger send;
@property(nonatomic,strong) NSString *name;

@end
