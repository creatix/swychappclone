//
//  UICheckbox.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UICheckbox.h"
#import "TTTAttributedLabel.h"

@interface UICheckbox(){
    BOOL _state;
    UIImage *_checkedImage;
    UIImage *_uncheckedImage;
}
@property(nonatomic,weak) IBOutlet UIImageView *imageView;
@property(nonatomic,weak) IBOutlet TTTAttributedLabel *labText;
@end
@implementation UICheckbox
@dynamic checked;
@synthesize showImageOnly = _showImageOnly;

-(BOOL)showImageOnly{
    return _showImageOnly;
}

-(void)setShowImageOnly:(BOOL)showImageOnly{
    _showImageOnly = showImageOnly;
    if (_showImageOnly){
        self.labText.hidden = YES;
    }
    else{
        self.labText.hidden = NO;
    }
}

-(BOOL)checked{
    return _state;
}

-(void)setChecked:(BOOL)checked{
    _state = checked;
    if (checked){
        self.imageView.image = (self.checkedImage != nil ? self.checkedImage : [UIImage imageNamed:@"icon_checkbox_checked.png"]);
    }
    else{
        self.imageView.image = (self.unCheckedImage != nil ? self.unCheckedImage : [UIImage imageNamed:@"icon_checkbox_unchecked.png"]);
    }
    if (self.checkboxDelegate != nil && [self.checkboxDelegate respondsToSelector:@selector(checkbox:stateChanged:)]){
        [self.checkboxDelegate checkbox:self stateChanged:checked];
    }
}

-(UIImage *)checkedImage{
    return  _checkedImage;
}

-(void)setCheckedImage:(UIImage *)checkedImage{
    _checkedImage = checkedImage;
}


-(UIImage*)unCheckedImage{
    return _uncheckedImage;
}

-(void)setUnCheckedImage:(UIImage *)unCheckedImage{
    _uncheckedImage = unCheckedImage;
}

-(NSString*)text{
    return self.labText.text;
}

-(void)setText:(NSString *)text{
    self.labText.text = text;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self loadView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        [self loadView];
    }
    return self;
}

-(void)loadView{
    self.checkedImage = [UIImage imageNamed:@"icon_checkbox_checked.png"];
    self.unCheckedImage =[UIImage imageNamed:@"icon_checkbox_unchecked.png"];
    
    UIView *view =[Utils loadViewFromNib:@"UICheckbox" class:[self class] owner:self options:nil];
    
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.checked = NO;
    
    [self addSubview:view];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchOnView:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tap setCancelsTouchesInView:NO];
    [self addGestureRecognizer:tap];
    
    self.backgroundColor = [UIColor clearColor];
    self.imageView.backgroundColor = [UIColor clearColor];
    self.labText.backgroundColor = [UIColor clearColor];
}

-(void)touchOnView:(id)sender{
    BOOL contineChange = YES;
    if(self.checkboxDelegate != nil && [self.checkboxDelegate respondsToSelector:@selector(checkbox:stateWillChange:)]){
        contineChange = [self.checkboxDelegate checkbox:self stateWillChange:!self.checked];
    }
    if (contineChange && self.checkboxDelegate != nil && [self.checkboxDelegate respondsToSelector:@selector(checkbox:stateChanged:)]){
        self.checked = !self.checked;
        [self.checkboxDelegate checkbox:self stateChanged:self.checked];
    }
    else{
        self.checked = !self.checked;
    }
}
@end
