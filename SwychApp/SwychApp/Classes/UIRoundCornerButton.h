//
//  UIRoundCornerButton.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIRoundCornerButton : UIButton {
    CGFloat _cornerRadius;
    CGFloat _borderWidth;
    UIColor *_borderColor;

}

@property (nonatomic,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic,assign) IBInspectable CGFloat borderWidth;
@property (nonatomic,strong) IBInspectable UIColor *borderColor;

-(void)setButtonTextColor:(UIColor*)textColor backgroundColor:(UIColor*)bkColor cornerRadius:(CGFloat)radius;
@end


