//
//  SocialNetworkIntegration.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Google/SignIn.h>


typedef NS_ENUM(NSInteger,SocialNetworkIntegrationError){
    SocialNetworkError_Success = 0,
    SocialNetworkError_Unknown,
    SocialNetworkError_UserCancelled,
    SocialNetworkError_IncorrectUsernamePassword
};

@protocol SocialNetworkIntegrationDeleage;
@class ModelUser;

@interface SocialNetworkIntegration : NSObject<GIDSignInDelegate,GIDSignInUIDelegate>

+(SocialNetworkIntegration*)instance;

-(void)doFacebookLoginOnViewController:(UIViewController*)controller delegate:(id<SocialNetworkIntegrationDeleage>)delegate;
-(void)doFacebookLoginOnViewController:(UIViewController*)controller;

-(void)doGoogleLoginOnViewController:(UIViewController*)controller delegate:(id<SocialNetworkIntegrationDeleage>)delegate;
-(void)doGoogleLoginOnViewController:(UIViewController*)controller;


@end


@protocol SocialNetworkIntegrationDeleage <NSObject>

@optional
-(void)facebookLogin:(BOOL)success facebookToken:(NSString*)token facebookUserId:(NSString*)userId error:(NSError*)error;
-(void)googleSignIn:(BOOL)success googleToken:(NSString *)token googleUserId:(NSString*)userId userInfo:(ModelUser*)user error:(NSError*)error;

@end

