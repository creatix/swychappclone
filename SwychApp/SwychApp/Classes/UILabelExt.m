//
//  UILabelExt.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UILabelExt.h"

@implementation UILabelExt

@synthesize delegate,underlineText;
@synthesize verticalAlignment = _verticalAlignment;

-(CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    CGRect rect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    CGRect result;
    switch (_verticalAlignment)
    {
        case VerticalAlignment_Top:
            result = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
            break;
        case VerticalAlignment_Middle:
            result = CGRectMake(rect.origin.x, rect.origin.y + (bounds.size.height - rect.size.height) / 2, rect.size.width, rect.size.height);
            break;
        case VerticalAlignment_Bottom:
            result = CGRectMake(rect.origin.x, rect.origin.y + (bounds.size.height - rect.size.height), rect.size.width, rect.size.height);
            break;
        default:
            result = bounds;
            break;
    }
    return result;
}

-(void)drawTextInRect:(CGRect)rect
{
    CGRect r = [self textRectForBounds:rect limitedToNumberOfLines:self.numberOfLines];
    [super drawTextInRect:r];
}

#pragma mark - UIView
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (delegate != nil && [delegate respondsToSelector:@selector(labelExtTouched:)]){
        [delegate labelExtTouched:self];
    }
}

- (void)drawRect:(CGRect)rect {
    if (underlineText){
        CGFloat offsetY = 1;
        CGFloat offsetX = 0;
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        const CGFloat* colors = CGColorGetComponents(self.textColor.CGColor);
        
        CGContextSetRGBStrokeColor(ctx, colors[0], colors[1], colors[2], 1.0); // RGBA
        
        CGContextSetLineWidth(ctx, 1.0f);
        NSDictionary *attributes = @{NSFontAttributeName:self.font};
        CGSize tmpSize = [self.text sizeWithAttributes:attributes];
        //CGSize tmpSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(200, 9999)];
        if (self.textAlignment == NSTextAlignmentLeft){
            offsetX = 0;
        }
        else if (self.textAlignment == NSTextAlignmentRight){
            offsetX = rect.size.width - tmpSize.width;
        }
        else{
            offsetX = (rect.size.width - tmpSize.width) / 2.0;
        }
        CGContextMoveToPoint(ctx, offsetX, self.bounds.size.height - offsetY);
        CGContextAddLineToPoint(ctx, offsetX + tmpSize.width, self.bounds.size.height - offsetY);
        
        CGContextStrokePath(ctx);
    }
    [super drawRect:rect];
}

#pragma mark - Property
-(UILabelVerticalAlignment)verticalAlignment{
    return _verticalAlignment;
}

-(void)setVerticalAlignment:(UILabelVerticalAlignment)value{
    _verticalAlignment = value;
    [self setNeedsDisplay];
}

@end
