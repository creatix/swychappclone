//
//  GiftForFriendView.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "GiftForFriendView.h"
#import "ContactBookManager.h"
#import "ContactCollectionViewCell.h"
#import "ModelContact.h"
#import "NSString+Extra.h"
#import "ContactDetailView.h"

@interface GiftForFriendView()
-(NSArray*)filterContact;
@end

@implementation GiftForFriendView

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        self.labHeaderTo.text = NSLocalizedString(@"HeaderTo", nil);
        self.txtHeaderNameEmailMobile.placeholder = NSLocalizedString(@"HeaderNamePlaceHolder", nil);
        _selectedContactIndex = -1;
   }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)refreshContactData{
    contactArray = [self filterContact];
    [self.cvForFriend reloadData];
    
}

-(NSArray *)filterContact{
    NSArray *array = [[ContactBookManager instance] getAllContacts];
    if (_filterCriteria == nil || [_filterCriteria length] == 0){
        return array;
    }
    if ([self filterByEmail]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasEmailAddresStartWith:_filterCriteria];
        }];
        return [array filteredArrayUsingPredicate:pred];
    }
    else if([self filterByPhoneNumber]){
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasPhoneNumberStartWith:_filterCriteria];
        }];
        return [array filteredArrayUsingPredicate:pred];
    }
    else{
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(ModelContact *contact, NSDictionary<NSString *,id> * _Nullable bindings) {
            return [contact hasNameStartWith:_filterCriteria];
        }];
        return [array filteredArrayUsingPredicate:pred];
    }
    return nil;
}

-(BOOL)filterByPhoneNumber{
    NSString *str = [[_filterCriteria componentsSeparatedByCharactersInSet:
                     [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                        componentsJoinedByString:@""];
    return [str length] > 0;
}

-(BOOL)filterByEmail{
    return [_filterCriteria rangeOfString:@"@"].location != NSNotFound;
}
#pragma
#pragma mark UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([[str trim] length] > 0 || ([_filterCriteria length] > 0)){
        _filterCriteria = [[str lowercaseString] trim];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self performSelectorOnMainThread:@selector(refreshContactData) withObject:nil waitUntilDone:NO];
        });
    }
    self.vContactDetail.contact=nil;
    _selectedContactIndex = -1;
    return YES;
}

#pragma
#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return  [contactArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ContactCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"contactCellForFriend" forIndexPath:indexPath];
    ModelContact *contact = [contactArray objectAtIndex:indexPath.row];
    cell.contact = contact;
    cell.selectedContact = _selectedContactIndex == indexPath.row;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:
(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
        return [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    }
    return nil;
}


#pragma
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_selectedContactIndex >= 0 && _selectedContactIndex != indexPath.row){
        ContactCollectionViewCell *cell = (ContactCollectionViewCell*)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:_selectedContactIndex inSection:indexPath.section]];
        cell.selectedContact = NO;
        _selectedContactIndex = -1;
    }
    ContactCollectionViewCell *cell = (ContactCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    cell.selectedContact = YES;
    _selectedContactIndex = indexPath.row;
    if (self.giftForFriendDelegate != nil && [self.giftForFriendDelegate respondsToSelector:@selector(giftForFriendView:itemSelected:)]){
        [self.giftForFriendDelegate giftForFriendView:self itemSelected:cell.contact];
    }
    self.vContactDetail.contact = cell.contact;
    self.txtHeaderNameEmailMobile.text = cell.contact.fullName;
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    ContactCollectionViewCell *cell = (ContactCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    cell.selectedContact = NO;
    _selectedContactIndex = -1;
}
#pragma
#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(85, 150);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 0, 0, 0);
}
@end
