//
//  AnalyticAdjustSettings.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AnalyticSettings.h"

@interface AnalyticAdjustSettings : AnalyticSettings

@property(nonatomic,strong) NSString *appToken;
@property(nonatomic,assign) BOOL isTestEvn;
@end
