//
//  ApplePayPayment+AutherizeNet.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ApplePayPayment+AutherizeNet.h"
#import "ModelPayment+Internal.h"
#import "ModelPaymentMethodSettings.h"
#import "ModelPaymentProcessorData.h"


#import <PassKit/PKPaymentRequest.h>
#import <PassKit/PKPaymentToken.h>
#import <PassKit/PKPayment.h>
#import <PassKit/PKConstants.h>
#import <PassKit/PKPaymentAuthorizationViewController.h>

#import "NSObject+Extra.h"
#import "ModelApplePayData.h"

#import "Base64/MF_Base64Additions.h"

#import "ModelAddress.h"
#import "AuthNet.h"


#define TransactionDataDescriptor   @"FID=COMMON.APPLE.INAPP.PAYMENT"
#define APILoginID  @"29XNx9w36"


@interface ApplePayPayment_AutherizeNet(private)<AuthNetDelegate>

@end


@implementation ApplePayPayment_AutherizeNet

#pragma mark - PKPaymentAuthorizationViewControllerDelegate -


- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion {
    if(payment){
        NSObject<PaymentDelegate>* del = [ModelPayment getDelegate];
        if (del != nil && [del respondsToSelector:@selector(paymentCompleted:withPaymentMethod:withCallback:)]){
            NSString *pd = [[NSString alloc] initWithData:payment.token.paymentData encoding:NSUTF8StringEncoding];
            NSString *base64PaymentData = [pd base64String];
            
            [del paymentCompleted:[base64PaymentData dataUsingEncoding:NSUTF8StringEncoding]
                withPaymentMethod:PaymentMethod_ApplePay
                     withCallback:^(BOOL success) {
                         if(success) {
                             completion(PKPaymentAuthorizationStatusSuccess);
                         }
                         else {
                             completion(PKPaymentAuthorizationStatusFailure);
                         }
                     }];
        }

    }
    else{
        completion(PKPaymentAuthorizationStatusFailure);
    }
}


-(CreateTransactionRequest *)createTransactionReqObjectWithApiLoginID:(NSString *) apiLoginID
                                                  fingerPrintHashData:(NSString *) fpHashData
                                                       sequenceNumber:(NSInteger) sequenceNumber
                                                      transactionType:(AUTHNET_ACTION) transactionType
                                                      opaqueDataValue:(NSString*) opaqueDataValue
                                                       dataDescriptor:(NSString *) dataDescriptor
                                                        invoiceNumber:(NSString *) invoiceNumber
                                                          totalAmount:(NSDecimalNumber*) totalAmount
                                                          fpTimeStamp:(NSTimeInterval) fpTimeStamp
                                                       billingAddress:(ModelAddress*)billingAddress
{
    // create the transaction.
    CreateTransactionRequest *transactionRequestObj = [CreateTransactionRequest createTransactionRequest];
    TransactionRequestType *transactionRequestType = [TransactionRequestType transactionRequest];
    
    transactionRequestObj.transactionRequest = transactionRequestType;
    transactionRequestObj.transactionType = transactionType;
    
    // Set the fingerprint.
    // Note: Finger print generation requires transaction key.
    // Finger print generation must happen on the server.
    
    FingerPrintObjectType *fpObject = [FingerPrintObjectType fingerPrintObjectType];
    fpObject.hashValue = fpHashData;
    fpObject.sequenceNumber= sequenceNumber;
    fpObject.timeStamp = fpTimeStamp;
    
    transactionRequestObj.anetApiRequest.merchantAuthentication.transactionKey = fpHashData;// .fingerPrint = fpObject;
    transactionRequestObj.anetApiRequest.merchantAuthentication.name = apiLoginID;
    
    // Set the Opaque data
    OpaqueDataType *opaqueData = [OpaqueDataType opaqueDataType];
    opaqueData.dataValue= opaqueDataValue;
    opaqueData.dataDescriptor = dataDescriptor;
    
    PaymentType *paymentType = [PaymentType paymentType];
    paymentType.creditCard= nil;
    paymentType.bankAccount= nil;
    paymentType.trackData= nil;
    paymentType.swiperData= nil;
    paymentType.opData = opaqueData;
    
    transactionRequestObj.transactionRequest.billTo.firstName = billingAddress.firstName;
    transactionRequestObj.transactionRequest.billTo.lastName = billingAddress.lastName;
    transactionRequestObj.transactionRequest.billTo.address = billingAddress.street1;
    transactionRequestObj.transactionRequest.billTo.city = billingAddress.city;
    transactionRequestObj.transactionRequest.billTo.zip = billingAddress.zip;
    transactionRequestObj.transactionRequest.billTo.state = billingAddress.state;
    transactionRequestObj.transactionRequest.billTo.country = billingAddress.country;
    transactionRequestObj.transactionRequest.billTo.phoneNumber = billingAddress.phone;
    
    
    transactionRequestType.amount = [NSString stringWithFormat:@"%@",totalAmount];
    transactionRequestType.payment = paymentType;
    transactionRequestType.retail.marketType = @"0"; //0
    transactionRequestType.retail.deviceType = @"7";
    
    OrderType *orderType = [OrderType order];
    orderType.invoiceNumber = invoiceNumber;
    NSLog(@"Invoice Number Before Sending the Request %@", orderType.invoiceNumber);
    
    return transactionRequestObj;
}


- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}


#pragma mark -
#pragma mark - AuthNetDelegate Methods

- (void)paymentSucceeded:(CreateTransactionResponse*)response {
    NSLog(@"Payment Success ********************** ");
    
    TransactionResponse *transResponse = response.transactionResponse;
    
    NSLog(@"%@",response.responseReasonText);
    
    if ([transResponse.responseCode isEqualToString:@"4"])
    {
    }
    else
    {
    }
}

- (void)paymentCanceled {
    
    NSLog(@"Payment Canceled ********************** ");
    
}

-(void) requestFailed:(AuthNetResponse *)response {
    
    NSLog(@"Request Failed ********************** ");
    
}

- (void) connectionFailed:(AuthNetResponse *)response {
    NSLog(@"%@", response.responseReasonText);
    NSLog(@"Connection Failed");
    
}


-(void)logoutSucceeded:(LogoutResponse *)response{
    
    NSLog(@"Logout Success ********************** ");
    
}
@end
