//
//  StripeResponse.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class StripeCard;
@interface StripeResponse : NSObject

@property(nonatomic,strong) NSString *respId;
@property(nonatomic,strong) NSString *object;
@property(nonatomic,strong) StripeCard *card;
@property(nonatomic,strong) NSString *client_ip;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *livemode;
@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *used;

@property(nonatomic,readonly)NSData *tokenData;

-(id)initWithData:(NSData*)data;
@end
