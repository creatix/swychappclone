//
//  PayPalPayment.h
//  SwychApp
//
//  Created by Donald Hancock on 2/18/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPayment.h"

@interface PayPalPaymentMethod : ModelPayment

@end
