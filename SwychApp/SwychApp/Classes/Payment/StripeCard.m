//
//  StripeCard.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "StripeCard.h"
#import "NSObject+Extra.h"


@implementation StripeCard


-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"id"]){
        return @"cardId";
    }
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"cardId"]){
        return @"id";
    }
    return [super getJSONKeyNameInProperties:propertyName];
}

@end
