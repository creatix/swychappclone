//
//  ApplePayment+BrainTree.m
//  SwychApp
//
//  Created by Jason Hancock on 11/23/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ApplePayPayment+BrainTree.h"
#import <PassKit/PKPaymentRequest.h>
#import <PassKit/PKPaymentToken.h>
#import <PassKit/PKPayment.h>
#import <PassKit/PKConstants.h>
#import <PassKit/PKPaymentAuthorizationViewController.h>

#import "ModelPayment+Internal.h"
#import "ModelPaymentMethodSettings.h"
#import "BraintreeApplePay.h"
#import "ModelPaymentMethodSettings.h"
#import "ModelPaymentProcessorData.h"

@implementation ApplePayPayment_BrainTree
    
+ (void) initialize {
}
    
#pragma mark - PKPaymentAuthorizationViewControllerDelegate -
    
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion {
    
    ModelPaymentProcessorData* paymentProcessorData = (ModelPaymentProcessorData*)self.contextObject;
    BTAPIClient *braintreeClient = [[BTAPIClient alloc] initWithAuthorization:paymentProcessorData.accessToken];
    
    // Example: Tokenize the Apple Pay payment
    BTApplePayClient *applePayClient = [[BTApplePayClient alloc]
                                        initWithAPIClient:braintreeClient];
    
    [applePayClient tokenizeApplePayPayment:payment
                                 completion:^(BTApplePayCardNonce *tokenizedApplePayPayment, NSError *error) {
                                     
                                     NSObject<PaymentDelegate>* del = [ModelPayment getDelegate];
                                     if (error) {
                                         if (del != nil && [del respondsToSelector:@selector(paymentError:withPaymentMethod:)]){
                                             [del paymentError:error withPaymentMethod:PaymentMethod_ApplePay];
                                         }
                                         completion(PKPaymentAuthorizationStatusFailure);
                                     }
                                     else {
                                         //NSData* data = [tokenizedApplePayPayment.nonce dataUsingEncoding:NSUTF8StringEncoding];
                                         NSData* data = [[[ModelApplePayData alloc] initWithToken:tokenizedApplePayPayment.nonce
                                                                                  applePayPayment:payment] paymentData]
                                         if (del != nil && [del respondsToSelector:@selector(paymentCompleted:withPaymentMethod:withCallback:)]){
                                             [del paymentCompleted:data
                                                 withPaymentMethod:PaymentMethod_ApplePay
                                                      withCallback:^(BOOL success) {
                                                          if(success) {
                                                              completion(PKPaymentAuthorizationStatusSuccess);
                                                          }
                                                          else {
                                                              completion(PKPaymentAuthorizationStatusFailure);
                                                          }
                                                      }];
                                         }
                                     }
                                 }];
    
}
    

@end
