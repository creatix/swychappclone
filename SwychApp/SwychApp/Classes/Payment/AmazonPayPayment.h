//
//  AmazonPayPayment.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPayment.h"

#define AMAZON_ERR_DOMAIN                           @"Swych_Amazon"

#define AMAZON_ERR_INVALID_SETTING                  0x0001

#define AMAZON_ERR_AUTH_USER_CANCELLED              0x0101
#define AMAZON_ERR_AUTH_OTHER                       0x0102

#define AMAZON_ERR_PAYMENT_UNKNOWN_ERROR                0x0103
#define AMAZON_ERR_PAYMENT_FAILED_TO_CREATE_ORDER       0x0104
#define AMAZON_ERR_PAYMENT_FAILED_TO_GET_ORDER_DETAIL   0x0105
#define AMAZON_ERR_PAYMENT_FAILED_TO_GET_AUTH_ID        0x0106
#define AMAZON_ERR_PAYMENT_EMPTY_PROCESS_PAYMENT_REQ    0x0107
#define AMAZON_ERR_PAYMENT_USER_CANCELLED               0x0108
#define AMAZON_ERR_PAYMENT_INVALID_ORDER_STATUS         0x0109
#define AMAZON_ERR_PAYMENT_CANNOT_PROCESS               0x0110

typedef void(^amazonProcessPaymentCompletion)(BOOL);


@interface AmazonPayPayment : ModelPayment

@property(nonatomic,strong) NSString *accessToken;
@property(nonatomic,strong) NSString *amazonOrderReferenceId;


-(void)startPaymentWithDelegate:(NSObject<PaymentDelegate>*) delegate;
-(void)processPaymentWithAuthorization:(amazonProcessPaymentCompletion)completeHandler;

@end
