//
//  ApplePayPayment+Stripe.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ApplePayPayment+Stripe.h"
#import <Stripe/Stripe.h>
#import "ModelPayment+Internal.h"
#import "ModelPaymentMethodSettings.h"
#import "ModelApplePayData.h"

@implementation ApplePayPayment_Stripe

+ (void) initialize {
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    [Stripe setDefaultPublishableKey:settings.applePayStripePublishableKey];
}


#pragma mark - PKPaymentAuthorizationViewControllerDelegate -

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion {
    
    [[STPAPIClient sharedClient] createTokenWithPayment:payment
                                             completion:^(STPToken *token, NSError *error) {
                                                 NSObject<PaymentDelegate>* del = [ModelPayment getDelegate];
                                                 if (error) {
                                                     if (del != nil && [del respondsToSelector:@selector(paymentError:withPaymentMethod:)]){
                                                         [del paymentError:error withPaymentMethod:PaymentMethod_ApplePay];
                                                     }
                                                     completion(PKPaymentAuthorizationStatusFailure);
                                                 }
                                                 else {
                                                     NSData* data = [[[ModelApplePayData alloc] initWithToken:token.tokenId
                                                                                             applePayPayment:payment] paymentData];// [token.tokenId dataUsingEncoding:NSUTF8StringEncoding];
                                                     if (del != nil && [del respondsToSelector:@selector(paymentCompleted:withPaymentMethod:withCallback:)]){
                                                         [del paymentCompleted:data
                                                             withPaymentMethod:PaymentMethod_ApplePay
                                                                  withCallback:^(BOOL success) {
                                                                      if(success) {
                                                                          completion(PKPaymentAuthorizationStatusSuccess);
                                                                      }
                                                                      else {
                                                                          completion(PKPaymentAuthorizationStatusFailure);
                                                                      }
                                                                  }];
                                                     }
                                                 }
                                             }];
    
}

@end
