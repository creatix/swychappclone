//
//  ModelPayment+Internal.h
//  SwychApp
//
//  Created by Donald Hancock on 2/19/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPayment.h"

NSString* countryCodeString(CurrencyType countryCode);

NSString* currencyCodeString(CurrencyType countryCode);

@interface NSArray (select)

- (NSArray*) select:(NSObject*(^)(Purchase*)) function;

@end

@interface NSNumber (NSDecimal)

- (NSDecimalNumber*) toDecimalNumber;

@end

@interface ModelPayment (Internal)

+ (NSObject<PaymentDelegate>*) getDelegate;

+ (BOOL) isAvailable;

+(BOOL)canMakePayment;

+ (void) initialize;

- (UIViewController*) createPaymentViewController;

@end
