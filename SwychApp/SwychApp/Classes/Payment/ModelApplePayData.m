//
//  ModelApplePayData.m
//  SwychApp
//
//  Created by Kai Xing on 2016-11-14.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelApplePayData.h"
#import "NSObject+Extra.h"
#import "ModelAddress.h"
#import <PassKit/PKPayment.h>
#import <AddressBook/ABAddressBook.h>
#import <PassKit/PKContact.h>

@implementation ModelApplePayData

-(id)initWithToken:(NSString*)token applePayPayment:(PKPayment*)payment{
    self = [super init];
    if(self){
        self.token = token;
        if (payment != nil && payment.billingContact != nil){
            self.billingAddress = [[ModelAddress alloc] init];
            self.billingAddress.firstName = payment.billingContact.name.givenName;
            self.billingAddress.lastName = payment.billingContact.name.familyName;
            self.billingAddress.street1 = payment.billingContact.postalAddress.street;
            self.billingAddress.city = payment.billingContact.postalAddress.city;
            self.billingAddress.state = payment.billingContact.postalAddress.state;
            self.billingAddress.country = payment.billingContact.postalAddress.country;
            self.billingAddress.zip = payment.billingContact.postalAddress.postalCode;
            self.billingAddress.email = payment.billingContact.emailAddress;
            self.billingAddress.phone = payment.billingContact.phoneNumber.stringValue;
        }
    }
    return self;
}
-(NSData*)paymentData{
    return [self getJSONDataWithDateTimeFormatter:nil];
}
@end
