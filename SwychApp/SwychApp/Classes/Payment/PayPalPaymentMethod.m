//
//  PayPalPayment.m
//  SwychApp
//
//  Created by Donald Hancock on 2/18/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PayPalPaymentMethod.h"
#import "PayPalMobile.h"
#import "ModelPayment+Internal.h"
#import "PaymentConstants.h"
#import "ModelPaymentMethodSettings.h"



@implementation PayPalPaymentMethod

+ (BOOL) isAvailable {
    return YES;
}

+ (void) initialize {
    DEBUGLOG(@"%@", [PayPalMobile libraryVersion]);
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    if (settings.payPalMerchantTestKey == nil && settings.payPalMerchantProductionKey == nil){
        @throw [NSException exceptionWithName:@"Invalid settings" reason:@"Empty Paypal settings" userInfo:nil];
    }
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : settings.payPalMerchantProductionKey,
                                                           PayPalEnvironmentSandbox : settings.payPalMerchantTestKey}];
    
    [PayPalMobile preconnectWithEnvironment:settings.paypalKeyIsProdKey ? PayPalEnvironmentProduction : PayPalEnvironmentSandbox];
}

- (NSString*) name {
    return NSLocalizedString(@"PaymentOption_PayPal", nil);
}

-(UIViewController*)showShareProfileViewController{
    PayPalConfiguration *payPalConfig = [[PayPalConfiguration alloc] init];
    payPalConfig.acceptCreditCards = NO;// YES;
    payPalConfig.merchantName = self.merchantName;
    payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"http://www.goswych.com/privacy"];
    payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"http://www.goswych.com/terms"];
    payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    DEBUGLOG(@"PayPal language: %@",payPalConfig.languageOrLocale);
    payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;

    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:payPalConfig delegate:self];
    return profileSharingPaymentViewController;
}


- (UIViewController*) createPaymentViewController {
    
    return [self createPaymentViewControllerInternal];
}

-(UIViewController*)createPaymentViewControllerInternal{
    NSArray* items = [self.purchases select:^NSObject *(Purchase* obj) {
        return [PayPalItem itemWithName:obj.lineItem
                           withQuantity:1
                              withPrice:[obj.amount toDecimalNumber]
                           withCurrency:currencyCodeString(self.currencyType)
                                withSku:nil];
    }];
    
    PayPalConfiguration *payPalConfig = [[PayPalConfiguration alloc] init];
    payPalConfig.acceptCreditCards = NO;// YES;
    payPalConfig.merchantName = self.merchantName;
    payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"http://www.goswych.com/privacy"];
    payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"http://www.goswych.com/terms"];
    payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    DEBUGLOG(@"PayPal language: %@",payPalConfig.languageOrLocale);
    payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:[self.shippingCost toDecimalNumber]
                                                                                    withTax:[self.tax toDecimalNumber]];
    
    NSDecimalNumber *total = subtotal;
    
    if(self.tax != nil) {
        total = [total decimalNumberByAdding:[self.tax toDecimalNumber]];
    }
    
    if(self.shippingCost != nil) {
        total = [total decimalNumberByAdding:[self.shippingCost toDecimalNumber]];
    }
    
    PayPalPayment *paypalPayment = [[PayPalPayment alloc] init];
    paypalPayment.amount = total;
    paypalPayment.currencyCode = currencyCodeString(self.currencyType);
    paypalPayment.shortDescription =((PayPalItem*)[items objectAtIndex:0]).name;
    paypalPayment.items = items;  // if not including multiple items, then leave payment.items as nil
    paypalPayment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    paypalPayment.intent = PayPalPaymentIntentAuthorize;
    
    if (!paypalPayment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    return [[PayPalPaymentViewController alloc] initWithPayment:paypalPayment
                                                  configuration:payPalConfig
                                                       delegate:(NSObject<PayPalPaymentDelegate>*)[self class]];
}
#pragma mark - PayPalPaymentDelegate -

+ (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    NSObject<PaymentDelegate>* del = [ModelPayment getDelegate];
    NSData* data = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
                                                   options:0
                                                     error:nil];
    if (del != nil && [del respondsToSelector:@selector(paymentCompleted:withPaymentMethod:withCallback:)]){
        [del paymentCompleted:data
            withPaymentMethod:PaymentMethod_PayPal
             withCallback:^(BOOL success) {
                 [paymentViewController dismissViewControllerAnimated:YES completion:nil];
             }];
    }
}

+ (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
}



- (void)userDidCancelPayPalProfileSharingViewController:(nonnull PayPalProfileSharingViewController *)profileSharingViewController{
    
}

- (void)payPalProfileSharingViewController:(nonnull PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(nonnull NSDictionary *)profileSharingAuthorization{
    
}

@end
