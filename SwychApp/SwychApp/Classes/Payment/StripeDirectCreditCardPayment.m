//
//  StripeDirectCreditCardPayment.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "StripeDirectCreditCardPayment.h"
#import "StripeDirectCreditCardViewController.h"
#import "ModelPaymentMethodSettings.h"
#import "ModelPayment+Internal.h"
#import "NSString+Extra.h"

#import "StripeResponse.h"
#import "StripeCard.h"

@implementation StripeDirectCreditCardPayment

+ (BOOL) isAvailable {
    return YES;
}

+ (void) initialize {
}

- (NSString*) name {
    return NSLocalizedString(@"PaymentOption_DirectCreditCard", nil);
}

- (UIViewController*) createPaymentViewController {
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    if(settings.directCreditCardURL == nil){
        return nil;
    }
    StripeDirectCreditCardViewController *ctrl = (StripeDirectCreditCardViewController*)[Utils loadViewControllerFromStoryboard:@"WebView" controllerId:@"StripeDirectCreditCardViewController"];
    Purchase *item = [self.purchases objectAtIndex:0];
    ctrl.purchaseAmount = [item.amount doubleValue];
    ctrl.purchaseDesc = item.lineItem;
    ctrl.paymentURL = settings.directCreditCardURL;
    ctrl.productImageUrl = item.imageURL;
    ctrl.delPayment = self;
    
    //setup delegate here
    return ctrl;
}



#pragma mark - StripeDirectCreditCardPaymentDelegate

-(void)stripeDirectCreditCardPaymentCancelled:(StripeDirectCreditCardViewController*)viewController{
    
}
-(void)stripeDirectCreditCardPaymentSuccess:(StripeDirectCreditCardViewController*)viewController token:(NSString*)token{
    NSObject<PaymentDelegate>* del = [ModelPayment getDelegate];

    
    NSData* data = [[token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
    StripeResponse *stripeResp = [[StripeResponse alloc] initWithData:data];
    
    if (del != nil && [del respondsToSelector:@selector(paymentCompleted:withPaymentMethod:withCallback:)]){
        [del paymentCompleted:stripeResp.tokenData
            withPaymentMethod:PaymentMethod_DirectCreditCard
                 withCallback:^(BOOL success) {
                     [viewController dismissViewControllerAnimated:YES completion:nil];
                 }];
    }
}

@end
