//
//  ModelApplePayData.h
//  SwychApp
//
//  Created by Kai Xing on 2016-11-14.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ModelAddress;
@class PKPayment;
@interface ModelApplePayData : NSObject

@property(nonatomic,readonly) NSData *paymentData;
@property(nonatomic,strong) NSString *token;
@property(nonatomic,strong) ModelAddress *billingAddress;

-(id)initWithToken:(NSString*)token applePayPayment:(PKPayment*)payment;
@end
