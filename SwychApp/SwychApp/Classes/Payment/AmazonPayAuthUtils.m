//
//  AmazonPayAuthUtils.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AmazonPayAuthUtils.h"



@implementation AmazonPayAuthUtils
/*

+(NSError*)createError:(NSInteger)errorCode message:(NSString*)errorMessage{
    NSError *err = [NSError errorWithDomain:AMAZON_ERR_DOMAIN
                                       code:errorCode
                                   userInfo:errorMessage == nil ? nil : [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
    return err;
}

+ (PWALoginHandler)buildLoginHandlerWithHandler:(void (^)(AMZNAuthorizeResult *, NSError *))supplementaryHandler{
    return ^(AMZNAuthorizeResult *result, BOOL userDidCancel, NSError *error) {
        NSError *err = nil;
        if (error) {
            DEBUGLOG(@"Login with Amazon failed");
            DEBUGLOG(@"LWA error: %@", error.description);
            if (userDidCancel) {
                err = [AmazonPayAuthUtils createError:AMAZON_ERR_AUTH_USER_CANCELLED message:error.localizedDescription];
            } else {
                err = [AmazonPayAuthUtils createError:AMAZON_ERR_AUTH_OTHER message:error.localizedDescription];
            }
        } else {
            DEBUGLOG(@"Login with Amazon succeeded");
            DEBUGLOG(@"Amazon user name: %@", result.user.name);
            DEBUGLOG(@"Amazon user email: %@", result.user.email);
            err = nil;
        }
        supplementaryHandler(result, err);
    };
}

+ (PWACreateOrderHandler)buildCreateOrderHandlerWithHandler:(void (^)(NSString *, NSError *))supplementaryHandler{
    return ^(NSString *amazonOrderReferenceId, NSError *error) {
        NSError *err = nil;
        if (error) {
            DEBUGLOG(@"Create order reference failed");
            DEBUGLOG(@"Error during create order: %@", error.description);
            err = [AmazonPayAuthUtils createError:AMAZON_ERR_PAYMENT_FAILED_TO_CREATE_ORDER message:error.localizedDescription];
        } else {
            DEBUGLOG(@"Create order reference succeeded");
            DEBUGLOG(@"Amazon order reference Id: %@", amazonOrderReferenceId);
            err = nil;
        }
        
        supplementaryHandler(amazonOrderReferenceId, err);
    };
}

+ (void)createOrderReferenceWithCompletionHandler:(PWACreateOrderHandler)completionHandler{
    // Construct the request object with the mandatory parameter useAmazonAddressBook;
    // Set to true if the shipping address required, otherwise set to false
    PWACreateOrderReferenceRequest *createOrderReferenceRequest = [[PWACreateOrderReferenceRequest alloc] initWithUseAmazonAddressBook:YES];
    
    // Optional. set this only if you want to override the region for this order.
    //createOrderReferenceRequest.region = overrideRegion;
    
    // Optional. Set this only if you want to override the ledger currency for this order.
    //createOrderReferenceRequest.ledgerCurrencyCode = overrideLedgerCurrencyCode;
    
    // Optional. Set this only if you want to override the locale for this order.
    //createOrderReferenceRequest.locale = overrideLocale;
    
    NSLog(@"Making Create Order Reference PWA API call");
    [PayWithAmazon createOrderReference:createOrderReferenceRequest
                      completionHandler:completionHandler];
}


+ (void)getOrderReferenceDetailsForOrderId:(NSString *)amazonOrderReferenceID completionHandler:(void (^)(NSData *, NSError *))completionHandler{
    AMZNAuthorizeRequest *request = [[AMZNAuthorizeRequest alloc] init];
    
    request.scopes = [[NSArray alloc] initWithObjects:[PWAPaymentScope initiate], nil];
    // The SDK will check for locally cached authorized grant. Even if authorized grant is not found, SignIn flow will not occur
    request.interactiveStrategy = AMZNInteractiveStrategyNever;
    
    NSLog(@"Making Authorize LWA API call for Get Order Details");
    [[AMZNAuthorizationManager sharedManager] authorize:request
                                            withHandler:^(AMZNAuthorizeResult * _Nullable result, BOOL userDidCancel, NSError * _Nullable error) {
                                                if (error) {
                                                    NSLog(@"LWA Authorize error: %@", error.description);
                                                    //[PWADemoAlertMessageUtils setAlertMessage:@"Error authorizing with Amazon"];
                                                    return;
                                                }
                                                
                                                NSLog(@"Getting order reference details from merchant backend");
                                                
                                                // TODO: add code to call backend with result.token (accessToken) and amazonOrderReferenceId which calls MWS GetOrderReferenceDetails API. See integration guide for more details on calling GetOrderReferenceDetails.
                                                // The XML response from GetOrderReferenceDetails should be converted to JSON string by your backend.
                                                // The completionHandler will handle the JSON string containing the GetOrderReferenceDetails response.
                                                
                                            }];
}





*/

@end
