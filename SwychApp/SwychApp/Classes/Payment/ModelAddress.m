//
//  ModelAddress.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelAddress.h"

@implementation ModelAddress

@synthesize firstName, lastName, street1, street2, city, state, zip, country, email, phone;

- (id) init {
    self = [super init];
    if (self) {
        self.firstName = nil;
        self.lastName = nil;
        self.street1 = nil;
        self.street2 = nil;
        self.city = nil;
        self.state = nil;
        self.zip = nil;
        self.country = nil;
        self.email = nil;
        self.phone = nil;
    }
    return self;
}

-(id)initWithPKPaymentABRecord:(ABRecordRef *) PKPaymentABRecord{
    self = [super init];
    if (self){
        [self updateAddressFromPKPaymentABRecord:PKPaymentABRecord];
    }
    return self;
}

-(void) updateAddressFromPKPaymentABRecord:(ABRecordRef *) PKPaymentABRecord
{
    if (PKPaymentABRecord != nil)
    {
        
        self.firstName = (__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord,kABPersonFirstNameProperty);
        self.lastName = (__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonLastNameProperty);
        
        ABMultiValueRef emails = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonEmailProperty));
        
        if(ABMultiValueGetCount(emails) > 0 )
        {
            for(CFIndex i = 0; i< ABMultiValueGetCount(emails); i++ )
            {
                CFStringRef label = ABMultiValueCopyLabelAtIndex(emails, i);
                CFStringRef value = ABMultiValueCopyValueAtIndex(emails, i);
                self.email = (__bridge NSString*)value;
                if (label) CFRelease(label);
                if (value) CFRelease(value);
            }
        }
        
        
        ABMultiValueRef phoneNumbers = (__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(PKPaymentABRecord, kABPersonPhoneProperty));
        
        if(ABMultiValueGetCount(phoneNumbers) > 0 )
        {
            for(CFIndex i = 0; i< ABMultiValueGetCount(phoneNumbers); i++ )
            {
                CFStringRef label = ABMultiValueCopyLabelAtIndex(phoneNumbers, i);
                CFStringRef value = ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                self.phone = (__bridge NSString*)value;
                if (label) CFRelease(label);
                if (value) CFRelease(value);
            }
        }
        
        ABMultiValueRef streetAddress = ABRecordCopyValue(PKPaymentABRecord, kABPersonAddressProperty);
        if (ABMultiValueGetCount(streetAddress) > 0)
        {
            NSDictionary *theDict = (__bridge NSDictionary*)ABMultiValueCopyValueAtIndex(streetAddress, 0);
            
            self.street1 = [theDict objectForKey:(NSString *)kABPersonAddressStreetKey];
            self.city = [theDict objectForKey:(NSString *)kABPersonAddressCityKey];
            self.state = [theDict objectForKey:(NSString *)kABPersonAddressStateKey];
            self.zip = [theDict objectForKey:(NSString *)kABPersonAddressZIPKey];
            self.country = [theDict objectForKey:(NSString *)kABPersonAddressCountryKey];
        }
        CFRelease(streetAddress);
    }
    else
    {
        NSLog(@"Invalid Params");
    }
}
@end
