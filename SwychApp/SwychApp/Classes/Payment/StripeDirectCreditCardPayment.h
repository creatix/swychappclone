//
//  StripeDirectCreditCardPayment.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPayment.h"

@protocol StripeDirectCreditCardPaymentDelegate;


@interface StripeDirectCreditCardPayment : ModelPayment<StripeDirectCreditCardPaymentDelegate>

@end
