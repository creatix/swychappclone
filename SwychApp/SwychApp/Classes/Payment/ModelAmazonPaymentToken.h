//
//  ModelAmazonPaymentToken.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelAmazonPaymentToken : NSObject

@property(nonatomic,strong) NSString *amazonOrderReferenceId;
@property(nonatomic,strong) NSString *amazonAuthorizationId;
@property(nonatomic,strong) NSString *amazonAccessToken;

@end
