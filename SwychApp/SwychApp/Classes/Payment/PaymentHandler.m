//
//  PaymentHandler.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "PaymentHandler.h"
#import "ModelPayment+Internal.h"
#import "ApplePayPayment.h"
#import "PayPalPaymentMethod.h"
#import "StripeDirectCreditCardPayment.h"
#import "ApplePayPayment+Stripe.h"
#import "ApplePayPayment+AutherizeNet.h"
#import "ApplePayPayment+BrainTree.h"
#import "AmazonPayPayment.h"
#import "LocalUserInfo.h"
#import "ModelSystemConfiguration.h"
#import "ModelPaymentProcessor.h"

#define PROCESSOR_UNKNOWN               0
#define PROCESSOR_STRIPE                1
#define PROCESSOR_AUTHORIZE_NET         2
#define PROCESSOR_BRAINTREE             5

@interface PaymentHandler()

@property (nonatomic,weak) UIViewController *parentViewController;
@property (nonatomic,weak) id<PaymentHandlerDelegate> delegate;

@property (nonatomic,assign) PaymentProcessor paymentProcessor_ApplePay;
@property (nonatomic,assign) PaymentProcessor paymentProcessor_PayPal;
@property (nonatomic,assign) PaymentProcessor paymentProcessor_DirectCreditCard;
@property (nonatomic,assign) PaymentProcessor paymentProcessor_AmazonPay;

@property (nonatomic,strong) ModelPayment *paymentApplePay;
@property (nonatomic,strong) ModelPayment *paymentPayPal;
@property (nonatomic,strong) ModelPayment *paymentDirectCC;
@property (nonatomic,strong) ModelPayment *paymentAmazonPay;

@end

@implementation PaymentHandler

+(PaymentHandler*)instance{
    static PaymentHandler *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
        
        g_instance = [[self alloc] init];
        switch (lui.sysConfig.paymentProcessor.paymentProcessorApplePay) {
            case PaymentProcessor_AutherizeNet:
                g_instance.paymentProcessor_ApplePay = PaymentProcessor_AutherizeNet;
                break;
            case PaymentProcessor_Braintree:
                g_instance.paymentProcessor_ApplePay = PaymentProcessor_Braintree;
                break;
            default:
                g_instance.paymentProcessor_ApplePay = PaymentProcessor_Stripe;
                break;
        }
        
        if(g_instance.paymentProcessor_ApplePay == PaymentProcessor_Stripe){
            g_instance.paymentApplePay = [[ApplePayPayment_Stripe alloc] init];
        }
        else if (g_instance.paymentProcessor_ApplePay == PaymentProcessor_AutherizeNet){
            g_instance.paymentApplePay = [[ApplePayPayment_AutherizeNet alloc] init];
        }
        else if (g_instance.paymentProcessor_ApplePay == PaymentProcessor_Braintree){
            g_instance.paymentApplePay = [[ApplePayPayment_BrainTree alloc] init];
        }
        else{
            g_instance.paymentApplePay = nil;
        }
        
        g_instance.paymentProcessor_PayPal = PaymentProcessor_Paypal;
        g_instance.paymentProcessor_DirectCreditCard = PaymentProcessor_Stripe;
        g_instance.paymentProcessor_AmazonPay = PaymentProcessor_AmazonPay;

        g_instance.paymentPayPal = [[PayPalPaymentMethod alloc] init];
        
        if(g_instance.paymentProcessor_DirectCreditCard == PaymentProcessor_Stripe){
            g_instance.paymentDirectCC = [[StripeDirectCreditCardPayment alloc] init];
        }
        else if (g_instance.paymentProcessor_DirectCreditCard == PaymentProcessor_AutherizeNet){
            g_instance.paymentDirectCC = nil;
        }
        else{
            g_instance.paymentDirectCC = nil;
        }
        
        g_instance.paymentAmazonPay = [[AmazonPayPayment alloc] init];
        
        Class cls[] = { [ApplePayPayment class],
            [PayPalPaymentMethod class],
            [StripeDirectCreditCardPayment class],
            [AmazonPayPayment class]};
        
        for(int classCounter = 0; classCounter < sizeof(cls) / sizeof(Class); classCounter++) {
            Class c = cls[classCounter];
            if([c isAvailable]) {
                [c initialize];
            }
        }
    });
    return g_instance;
}

-(void)resetPaymentProcessorIfNessary{
    LocalUserInfo *lui = [[LocalStorageManager instance] getLocalStoredUserInfo];
    BOOL resetProcessorApplePay = NO;
    if(self.paymentProcessor_ApplePay == PaymentProcessor_Braintree && lui.sysConfig.paymentProcessor.paymentProcessorApplePay != PaymentProcessor_Braintree){
        resetProcessorApplePay = YES;
    }
    else if (self.paymentProcessor_ApplePay == PaymentProcessor_AutherizeNet && lui.sysConfig.paymentProcessor.paymentProcessorApplePay != PaymentProcessor_AutherizeNet){
        resetProcessorApplePay = YES;
    }
    else if (self.paymentProcessor_ApplePay == PaymentProcessor_Stripe && lui.sysConfig.paymentProcessor.paymentProcessorApplePay != PaymentProcessor_Stripe && lui.sysConfig.paymentProcessor.paymentProcessorApplePay != PaymentProcessor_Unknow){
        resetProcessorApplePay = YES;
    }
    if (!resetProcessorApplePay){
        return;
    }
    switch (lui.sysConfig.paymentProcessor.paymentProcessorApplePay) {
        case PaymentProcessor_AutherizeNet:
            self.paymentProcessor_ApplePay = PaymentProcessor_AutherizeNet;
            break;
        case PaymentProcessor_Braintree:
            self.paymentProcessor_ApplePay = PaymentProcessor_Braintree;
            break;
        default:
            self.paymentProcessor_ApplePay = PaymentProcessor_Stripe;
            break;
    }
    
    if(self.paymentProcessor_ApplePay == PaymentProcessor_Stripe){
        self.paymentApplePay = [[ApplePayPayment_Stripe alloc] init];
    }
    else if (self.paymentProcessor_ApplePay == PaymentProcessor_AutherizeNet){
        self.paymentApplePay = [[ApplePayPayment_AutherizeNet alloc] init];
    }
    else if (self.paymentProcessor_ApplePay == PaymentProcessor_Braintree){
        self.paymentApplePay = [[ApplePayPayment_BrainTree alloc] init];
    }
    else{
        self.paymentApplePay = nil;
    }

}

-(PaymentProcessor)getPaymentProssor:(PaymentMethod)method{
    PaymentProcessor processor = PaymentProcessor_Unknow;
    switch (method) {
        case PaymentMethod_ApplePay:
            processor = self.paymentProcessor_ApplePay;
            break;
        case PaymentMethod_PayPal:
            processor = self.paymentProcessor_PayPal;
            break;
        case PaymentMethod_DirectCreditCard:
            processor = self.paymentProcessor_DirectCreditCard;
            break;
        case PaymentMethod_AmazonPay:
            processor = self.paymentProcessor_AmazonPay;
            break;
        default:
            break;
    }
    return processor;
}

-(BOOL)isPaymentMethodAvailable:(PaymentMethod)method{
    BOOL available = NO;
    switch (method) {
        case PaymentMethod_ApplePay:
            available = [ApplePayPayment isAvailable];
            break;
        case PaymentMethod_PayPal:
            available = [PayPalPaymentMethod isAvailable];
            break;
        case PaymentMethod_DirectCreditCard:
            available = [StripeDirectCreditCardPayment isAvailable];
            break;
        case PaymentMethod_AmazonPay:
            available = [AmazonPayPayment isAvailable];
            break;
        default:
            break;
    }
    return available;
}

-(BOOL)isPaymentMethodCanMakePayment:(PaymentMethod)method{
    BOOL canMakePayment = NO;
    switch (method) {
        case PaymentMethod_ApplePay:
            canMakePayment = [ApplePayPayment canMakePayment];
            break;
        case PaymentMethod_PayPal:
            canMakePayment = [PayPalPaymentMethod canMakePayment];
            break;
        case PaymentMethod_DirectCreditCard:
            canMakePayment = [StripeDirectCreditCardPayment canMakePayment];
            break;
        case PaymentMethod_AmazonPay:
            canMakePayment = [AmazonPayPayment canMakePayment];
            break;
        default:
            break;
    }
    return canMakePayment;
}

-(ModelPayment*)applePay{
    return nil;
}
-(ModelPayment*)paypal{
    return nil;
}
-(ModelPayment*)visaCheckout{
    return nil;
}


- (NSArray<ModelPayment*>*) availablePaymentOptions {
    NSMutableArray* availablePayments = [NSMutableArray array];
    
    Class cls[] = { [ApplePayPayment class], [PayPalPaymentMethod class],[StripeDirectCreditCardPayment class],[AmazonPayPayment class] };
    
    for(int classCounter = 0; classCounter < sizeof(cls) / sizeof(Class); classCounter++) {
        Class c = cls[classCounter];
        if([c isAvailable]) {
            [availablePayments addObject:[c new]];
        }
    }
    
    return availablePayments;
}

-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController{
    [self makePayment:method amount:amount description:description originalPurchaseAmount:0 withDelegate:delegate viewController:viewController productImage:nil];
}
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController{
    [self makePayment:method amount:amount description:description originalPurchaseAmount:orgAmount withDelegate:delegate viewController:viewController productImage:nil];
}

-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL{
    [self makePayment:method
               amount:amount
          description:description
        originalPurchaseAmount:0
         withDelegate:delegate
       viewController:viewController
         productImage:imageURL
        contextObject:nil];
}
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL{
    [self makePayment:method
               amount:amount
          description:description
        originalPurchaseAmount:orgAmount
         withDelegate:delegate
       viewController:viewController
         productImage:imageURL
     contextObject:nil];
}
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL contextObject:(NSObject*)contextObject{
    [self makePayment:method
               amount:amount
          description:description
        originalPurchaseAmount:0
         withDelegate:delegate
       viewController:viewController
         productImage:imageURL
        contextObject:contextObject];
}

-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL contextObject:(NSObject*)contextObject{
    [self resetPaymentProcessorIfNessary];
    
    self.parentViewController = viewController;
    self.delegate = delegate;
    
    ModelPayment *payment = nil;
    switch (method) {
        case PaymentMethod_ApplePay:
            payment = [PaymentHandler instance].paymentApplePay;
            break;
        case PaymentMethod_PayPal:
            payment = [PaymentHandler instance].paymentPayPal;
            break;
        case PaymentMethod_DirectCreditCard:
            payment = [PaymentHandler instance].paymentDirectCC;
            break;
        case PaymentMethod_AmazonPay:
            payment = [PaymentHandler instance].paymentAmazonPay;
            break;
        default:
            break;
    }
    if (payment == nil){
        NSError *error = [NSError errorWithDomain:@"SwychApp"
                                             code:-1
                                         userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"UnsupportedPaymentMethod", nil) forKey:NSLocalizedDescriptionKey]];
        if (delegate != nil && [delegate respondsToSelector:@selector(paymentCompleted:paymentToken:error:method:)]){
            [delegate paymentCompleted:NO paymentToken:nil error:error method:method];
            return;
        }
    }
    Purchase* purchase = [[Purchase alloc] init:[NSNumber numberWithDouble:amount]
                                 withDescription:description
                                   withImageURL:imageURL];
    purchase.orgAmount = [NSNumber numberWithDouble:orgAmount];
    [payment clearPurchases];
    
    [payment addPurchase:purchase];
    payment.contextObject = contextObject;
    
    if ([payment isKindOfClass:[AmazonPayPayment class]]){
        [(AmazonPayPayment*)payment startPaymentWithDelegate:self];
    }
    else{
        [payment presentPaymentScreen:viewController withDelegate:(NSObject<PaymentDelegate>*)self];
    }
}

#pragma
#pragma mark - Payment handling
- (void) paymentCompleted:(NSData*)data withPaymentMethod:(PaymentMethod)method
             withCallback:(void(^)(BOOL))callback {
    NSString *paymentToken =[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    DEBUGLOG(@"Payment completed with token: %@",paymentToken == nil ? @"(null)" : paymentToken);
    
    if (callback){
        callback(YES);
    }
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(paymentCompleted:paymentToken:error:method:)]){
        [self.delegate paymentCompleted:YES paymentToken:paymentToken error:nil method:method];
    }
    self.parentViewController = nil;
    self.delegate = nil;
}

- (void) paymentError:(NSError*)error withPaymentMethod:(PaymentMethod)method {
    DEBUGLOG(@"Payment failed with error: %@",[error localizedDescription]);
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(paymentCompleted:paymentToken:error:method:)]){
        [self.delegate paymentCompleted:NO paymentToken:nil error:error method:method];
    }
    self.parentViewController = nil;
    self.delegate = nil;
}

- (void) paymentFinished {
    if (self.parentViewController != nil){
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
    self.parentViewController = nil;
    self.delegate = nil;
    
}

@end
