//
//  ModelPayment.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentConstants.h"


typedef enum : NSUInteger {
    kCurrencyType_USDollars
} CurrencyType;

@interface Purchase : NSObject

- (instancetype) init:(NSNumber*)amount
      withDescription:(NSString*)lineItem;
- (instancetype) init:(NSNumber*)amount
      withDescription:(NSString*)lineItem
         withImageURL:(NSString*)url;
@property (nonatomic, readonly) NSNumber* amount;

@property (nonatomic, readonly) NSString* lineItem;

@property (nonatomic, readonly) NSString* imageURL;

@property (nonatomic, strong) NSNumber *orgAmount;

@end

@protocol PaymentDelegate <NSObject>

- (void) paymentCompleted:(NSData*)data withPaymentMethod:(PaymentMethod)method withCallback:(void(^)(BOOL))callback;

- (void) paymentError:(NSError*)error withPaymentMethod:(PaymentMethod)method;

- (void) paymentFinished;

@end

@interface ModelPayment : NSObject

@property (nonatomic,strong) NSObject *contextObject;

@property (nonatomic, readonly) NSString* name;

@property (nonatomic, assign) CurrencyType currencyType;

@property (nonatomic, readonly) NSArray<Purchase*>* purchases;

@property (nonatomic, copy) NSNumber* tax;

@property (nonatomic, copy) NSNumber* shippingCost;

@property (nonatomic, readonly) NSDecimalNumber *totalAmount;

@property (nonatomic, readonly) NSString *merchantName;

@property (nonatomic, readonly) NSDecimalNumber *totalOrgAmount;


- (void) presentPaymentScreen:(UIViewController*)presentingController
                 withDelegate:(NSObject<PaymentDelegate>*) delegate;

- (void) pushPaymentScreen:(UIViewController*)presentingController
              withDelegate:(NSObject<PaymentDelegate>*) delegate;

-(void)startPaymentWithDelegate:(NSObject<PaymentDelegate>*) delegate;

- (void) addPurchase:(Purchase*) purchase;

- (void) removePurchase:(Purchase*) purchase;

-(void) clearPurchases;

-(NSObject<PaymentDelegate>*)getPaymentDelegate;

@end
