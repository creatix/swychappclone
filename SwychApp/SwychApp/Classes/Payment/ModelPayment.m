//
//  ModelPayment.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ModelPayment.h"
#import "ModelPayment+Internal.h"
#import "SwychBaseViewController.h"


NSString* countryCodeString(CurrencyType countryCode) {
    switch (countryCode) {
        case kCurrencyType_USDollars:   return @"US";
        default:                        return nil;
    }
}

NSString* currencyCodeString(CurrencyType countryCode) {
    switch (countryCode) {
        case kCurrencyType_USDollars:   return @"USD";
        default:                        return nil;
    }
}

@implementation NSArray (select)

- (NSArray*) select:(NSObject*(^)(Purchase*)) function {
    NSMutableArray* results = [NSMutableArray arrayWithCapacity:self.count];
    
    for(Purchase* obj in self) {
        [results addObject:function(obj)];
    }
    
    return results;
}

@end

@implementation NSNumber (NSDecimal)

- (NSDecimalNumber*) toDecimalNumber {
    return [NSDecimalNumber decimalNumberWithMantissa:(long long)(self.doubleValue * 100)
                                             exponent:-2
                                           isNegative:NO];
}

@end

@implementation Purchase

- (instancetype) init:(NSNumber*)amount
      withDescription:(NSString*)lineItem {
    if((self = [super init]) != nil) {
        _amount = [amount copy];
        _lineItem = [lineItem copy];
    }
    return self;
}

- (instancetype) init:(NSNumber*)amount
      withDescription:(NSString*)lineItem
         withImageURL:(NSString*)url{
    if((self = [super init]) != nil) {
        _amount = [amount copy];
        _lineItem = [lineItem copy];
        _imageURL = [url copy];
    }
    return self;
}
@end

static NSObject<PaymentDelegate>* _sDelegate;

@interface ModelPayment () {
    NSMutableArray<Purchase*>*  _purchases;
}

@end

@implementation ModelPayment

+ (BOOL) isAvailable {
    return NO;
}

+(BOOL)canMakePayment{
    return YES;
}

+ (void) initialize {
    
}

+ (NSObject<PaymentDelegate>*) getDelegate {
    return _sDelegate;
}

- (instancetype) init {
    if((self = [super init]) != nil) {
        _purchases = [NSMutableArray array];
        _currencyType = kCurrencyType_USDollars;
    }
    return self;
}

-(NSDecimalNumber *)totalAmount{
    double total = 0;
    for(Purchase *p in self.purchases){
        total += [p.amount doubleValue];
    }
    return [NSDecimalNumber numberWithDouble:total];
}

- (NSString*) name {
    @throw [NSException exceptionWithName:@"Not Implemented" reason:nil userInfo:nil];
}


-(NSDecimalNumber*)totalOrgAmount{
    double total = 0;
    for(Purchase *p in self.purchases){
        total += [p.orgAmount doubleValue];
    }
    return [NSDecimalNumber numberWithDouble:total];
}

-(NSString*)merchantName{
    return @"Swych Inc.";
}

- (UIViewController*) createPaymentViewController {
    @throw [NSException exceptionWithName:@"Not Implemented" reason:nil userInfo:nil];
}

- (void) addPurchase:(Purchase*) purchase {
    [_purchases addObject:purchase];
}

- (void) removePurchase:(Purchase*) purchase {
    [_purchases removeObject:purchase];
}

-(void) clearPurchases{
    if(_purchases != nil){
        [_purchases removeAllObjects];
    }
}

- (NSArray<Purchase*>*) purchases {
    return _purchases;
}

-(NSObject<PaymentDelegate>*)getPaymentDelegate{
    return _sDelegate;
}

-(void)startPaymentWithDelegate:(NSObject<PaymentDelegate>*) delegate{
    _sDelegate = delegate;
}

- (void) presentPaymentScreen:(UIViewController*)presentingController
                 withDelegate:(NSObject<PaymentDelegate>*) delegate {
    _sDelegate = delegate;

    presentingController.modalPresentationStyle = UIModalPresentationCustom;
    presentingController.transitioningDelegate = (NSObject<UIViewControllerTransitioningDelegate>*)[ModelPayment class];

    UIViewController* paymentViewController = [self createPaymentViewController];
    if (paymentViewController == nil){
        NSError *error = [NSError errorWithDomain:ERR_DOMAIN_PAYMENT code:ERR_CODE_PAYMENT_ApplePay_Not_Configured userInfo:nil];
        if (delegate != nil && [delegate respondsToSelector:@selector(paymentError:withPaymentMethod:)]){
            [delegate paymentError:error withPaymentMethod:PaymentMethod_ApplePay];
        }
    }
    else{
        if (paymentViewController.navigationController == nil && [paymentViewController isKindOfClass:[SwychBaseViewController class]]){
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:paymentViewController];
            [presentingController presentViewController:navigationController animated:YES completion:nil]; // This is for modalviewcontroller
        }
        else{
            [presentingController presentViewController:paymentViewController
                                       animated:YES
                                     completion:nil];
        }
    }
}

- (void) pushPaymentScreen:(UIViewController*)presentingController
                 withDelegate:(NSObject<PaymentDelegate>*) delegate {
    _sDelegate = delegate;
    
    presentingController.modalPresentationStyle = UIModalPresentationCustom;
    presentingController.transitioningDelegate = (NSObject<UIViewControllerTransitioningDelegate>*)[ModelPayment class];
    
    UIViewController* paymentViewController = [self createPaymentViewController];
    if (paymentViewController == nil){
        NSError *error = [NSError errorWithDomain:ERR_DOMAIN_PAYMENT code:ERR_CODE_PAYMENT_ApplePay_Not_Configured userInfo:nil];
        if (delegate != nil && [delegate respondsToSelector:@selector(paymentError:withPaymentMethod:)]){
            [delegate paymentError:error withPaymentMethod:PaymentMethod_ApplePay];
        }
    }
    else{
        [presentingController.navigationController pushViewController:paymentViewController animated:YES];
    }
}
#pragma mark - UIViewControllerTransitioningDelegate -

+ (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    dismissed.presentingViewController.transitioningDelegate = nil;
    _sDelegate = nil;
    return nil;
}

@end
