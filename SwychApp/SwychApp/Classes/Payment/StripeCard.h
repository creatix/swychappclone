//
//  StripeCard.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StripeCard : NSObject

@property(nonatomic,strong) NSString *cardId;
@property(nonatomic,strong) NSString *object;
@property(nonatomic,strong) NSString *address_city;
@property(nonatomic,strong) NSString *address_country;
@property(nonatomic,strong) NSString *address_line1;
@property(nonatomic,strong) NSString *address_line1_check;
@property(nonatomic,strong) NSString *address_line2;
@property(nonatomic,strong) NSString *address_state;
@property(nonatomic,strong) NSString *address_zip;
@property(nonatomic,strong) NSString *address_zip_check;
@property(nonatomic,strong) NSString *brand;
@property(nonatomic,strong) NSString *country;
@property(nonatomic,strong) NSString *cvc_check;
@property(nonatomic,strong) NSString *dynamic_last4;
@property(nonatomic,strong) NSString *funding;
@property(nonatomic,strong) NSString *last4;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *tokenization_method;
@property(nonatomic,strong) NSString *exp_year;
@property(nonatomic,strong) NSString *exp_month;
@end
