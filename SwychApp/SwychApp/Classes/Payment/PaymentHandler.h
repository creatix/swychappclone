//
//  PaymentHandler.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentConstants.h"

@class ModelPayment;
@protocol PaymentHandlerDelegate;

@interface PaymentHandler : NSObject

+(PaymentHandler*)instance;


-(NSArray<ModelPayment*>*) availablePaymentOptions;
-(BOOL)isPaymentMethodAvailable:(PaymentMethod)method;
-(BOOL)isPaymentMethodCanMakePayment:(PaymentMethod)method;

-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController;
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController;
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL;
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL;
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL contextObject:(NSObject*)contextObject;
-(void)makePayment:(PaymentMethod)method amount:(double)amount description:(NSString*)description originalPurchaseAmount:(double)orgAmount withDelegate:(NSObject<PaymentHandlerDelegate>*)delegate viewController:(UIViewController*)viewController productImage:(NSString*)imageURL contextObject:(NSObject*)contextObject;

-(PaymentProcessor)getPaymentProssor:(PaymentMethod)method;

@end


@protocol PaymentHandlerDelegate <NSObject>

@optional
-(void)paymentCompleted:(BOOL)success paymentToken:(NSString*)token error:(NSError*)error method:(PaymentMethod)method;
@end
