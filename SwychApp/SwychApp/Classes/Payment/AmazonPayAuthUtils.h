//
//  AmazonPayAuthUtils.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LoginWithAmazon/LoginWithAmazon.h>
#import <PayWithAmazon/PayWithAmazon.h>

typedef void (^PWALoginHandler)(AMZNAuthorizeResult *, BOOL, NSError *);
typedef void (^PWALogoutHandler)(NSError *error);

typedef void (^PWACreateOrderHandler)(NSString *, NSError *);
typedef void (^PWAGetDetailsHandler)(NSData *, NSError *);
typedef void (^PWAChangeOrderDetailsHandler)(PWAChangeOrderDetailsResponse *, NSError *);
typedef void (^PWAProcessPaymentHandler)(PWAProcessPaymentResponse *, NSError *);
typedef void (^PWAProcessPaymentDetailsHandler)(NSDictionary *, NSError *);


@interface AmazonPayAuthUtils : NSObject

+(NSError*)createError:(NSInteger)errorCode message:(NSString*)errorMessage;

+ (PWALoginHandler)buildLoginHandlerWithHandler:(void (^)(AMZNAuthorizeResult *, NSError *))supplementaryHandler;
+ (PWACreateOrderHandler)buildCreateOrderHandlerWithHandler:(void (^)(NSString *, NSError *))supplementaryHandler;
+ (void)createOrderReferenceWithCompletionHandler:(PWACreateOrderHandler)completionHandler;
+ (void)getOrderReferenceDetailsForOrderId:(NSString *)amazonOrderReferenceID completionHandler:(void (^)(NSData *, NSError *))completionHandler;

@end
