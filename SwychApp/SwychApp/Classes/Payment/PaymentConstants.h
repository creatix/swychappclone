//
//  PaymentConstants.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#ifndef PaymentConstants_h
#define PaymentConstants_h

typedef NS_ENUM(NSInteger,PaymentProcessor){
    PaymentProcessor_Unknow         = 0,
    PaymentProcessor_Stripe         = 1,
    PaymentProcessor_AutherizeNet   = 2,
    PaymentProcessor_Paypal         = 3,
    PaymentProcessor_AmazonPay      = 4,
    PaymentProcessor_Braintree      = 5
};

typedef NS_ENUM(NSInteger,PaymentMethod){
    PaymentMethod_None = 0,
    PaymentMethod_ApplePay = 1,
    PaymentMethod_PayPal,
    PaymentMethod_DirectCreditCard,
    PaymentMethod_AmazonPay,
    PaymentMethod_GoogleWallet,
    PaymentMethod_VisaCheckout,
    PaymentMethod_BitCoin,
    PaymentMethod_Kering
};


#define PAYMENT_DESCRIPTION     @"SWYCH"
#define PAYMENT_DESCRIPTION_APPLEPAY    PAYMENT_DESCRIPTION
#define PAYMENT_DESCRIPTION_PAYPAL      PAYMENT_DESCRIPTION
#define PAYMENT_DESCRIPTION_CREDITCARD  PAYMENT_DESCRIPTION

#endif /* PaymentConstants_h */
