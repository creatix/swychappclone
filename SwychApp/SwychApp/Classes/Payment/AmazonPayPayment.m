//
//  AmazonPayPayment.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "AmazonPayPayment.h"
#import "AmazonPayAuthUtils.h"
#import "ModelPaymentMethodSettings.h"
#import "SwychBaseViewController.h"
#import "NSString+Extra.h"
#import "NSObject+Extra.h"
#import "ModelAmazonPaymentToken.h"
#import "ViewOTPVerification.h"
#import "ModelUser.h"
#import "LocalUserInfo.h"


#import "RequestAmazonGetOrderReferenceDetail.h"
#import "ResponseAmazonGetOrderReferenceDetail.h"
#import "RequestResendOTP.h"
#import "ResponseResendOTP.h"
#import "RequestVerifyOTP.h"
#import "ResponseVerifyOTP.h"

#import "AmazonOrderConfirmViewController.h"
#import "ModelAmazonConstraints.h"
#import "ModelAmazonConstraint.h"


@import PayWithAmazon;
@import LoginWithAmazon;


#define Context_GetAmazonOrderDetailBeforeAuthorization     0x0001
#define Context_GetAmazonOrderDetailAfterAuthorization      0x0002
#define Context_GetAmazonOrderDetailAfterAuthorization_WithError      0x0003


@interface AmazonPayPayment()
@property(nonatomic,strong) PWAProcessPaymentRequest *currentProcessPaymentRequest;

@end

@implementation AmazonPayPayment

+ (void) initialize {
    [PayWithAmazon setDefaultRegion:NA];
    [PayWithAmazon setDefaultLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [PayWithAmazon setDefaultLedgerCurrency:@"USD"];
}

+ (BOOL) isAvailable {
    return YES;
}

-(NSError*)createError:(NSInteger)errorCode message:(NSString*)errorMessage{
    NSError *err = [NSError errorWithDomain:AMAZON_ERR_DOMAIN
                                       code:errorCode
                                   userInfo:errorMessage == nil ? nil : [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
    return err;
}

-(NSError*)createError:(NSInteger)errorCode error:(NSError*)error{
    NSError *err = [NSError errorWithDomain:AMAZON_ERR_DOMAIN
                                       code:errorCode
                                   userInfo:error == nil ? nil : [NSDictionary dictionaryWithObject:[error localizedDescription] forKey:NSLocalizedDescriptionKey]];
    return err;
}

- (NSString*) name {
    return NSLocalizedString(@"PaymentOption_AmazonPay", nil);
}

- (void)createAmazonOrderReferenceObject
{
    // Construct the request object with the mandatory parameter useAmazonAddressBook;
    // Set to true if the shipping address required, otherwise set to false
    PWACreateOrderReferenceRequest *createOrderReferenceRequest = [[PWACreateOrderReferenceRequest alloc] initWithUseAmazonAddressBook:NO];
    
    // Optional. set this only if you want to override the region for this order.
    //createOrderReferenceRequest.region = overrideRegion;
    
    // Optional. Set this only if you want to override the ledger currency for this order.
    //createOrderReferenceRequest.ledgerCurrencyCode = overrideLedgerCurrencyCode;
    
    // Optional. Set this only if you want to override the locale for this order.
    //createOrderReferenceRequest.locale = overrideLocale;
    
    NSLog(@"Making Create Order Reference PWA API call");
    [PayWithAmazon createOrderReference:createOrderReferenceRequest
                      completionHandler:^(NSString * _Nullable amazonOrderReferenceId, NSError * _Nullable error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                          if (error){
                              NSError *err = [self createError:AMAZON_ERR_PAYMENT_FAILED_TO_CREATE_ORDER error:error];
                              [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
                          }
                          else{
                              self.amazonOrderReferenceId = amazonOrderReferenceId;
                              [self getOrderReferenceDetailsWithId:amazonOrderReferenceId];
                          }
                    });
                      }];
}


- (void)getOrderReferenceDetailsWithId:(NSString *)amazonOrderReferenceId
{
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    if (settings.amazonAWSAccessKeyId == nil){
        //TODO: call delegate, pass settings error AMAZON_ERR_INVALID_SETTING
    }
    
    PWAPrice *price = [PWAPrice priceWithAmount:self.totalAmount currencyCode:@"USD"];
    
    PWAProcessPaymentRequest *requestProcessPayment = [[PWAProcessPaymentRequest alloc] initWithAmazonOrderReferenceID:amazonOrderReferenceId
                                                                                              orderTotal:price
                                                                                          awsAccessKeyID:settings.amazonAWSAccessKeyId];
    // Set optional parameters in request object
    // Options: @"Authorize" @"ConfirmOrder" @"AuthorizeAndCapture"
    // See integration guide on details about each option
    requestProcessPayment.paymentAction =@"Authorize";// @"AuthorizeAndCapture";
    
    // Construct and set optional seller order attributes
    PWASellerOrderAttributes *orderAttributes = [[PWASellerOrderAttributes alloc] init];
    orderAttributes.sellerOrderID = @"1";
    orderAttributes.sellerStoreName = self.merchantName;
    //orderAttributes.customInformation = nil;
    //orderAttributes.sellerNote = nil;
    requestProcessPayment.sellerOrderAttributes = orderAttributes;
    
    // Construct and set optional authorize attributes
    PWAAuthorizeAttributes *authorizeAttributes = [[PWAAuthorizeAttributes alloc] init];
    // You can authorize a lesser amount than the order total.
    authorizeAttributes.authorizationAmount = price;
    //authorizeAttributes.sellerSoftDescriptor = nil;
    requestProcessPayment.authorizeAttributes = authorizeAttributes;

    NSString *requestString = [PayWithAmazonUtil generateStringToSign:requestProcessPayment];
    
    RequestAmazonGetOrderReferenceDetail *req = [[RequestAmazonGetOrderReferenceDetail alloc] init];
    req.amazonToken = self.accessToken;
    req.amazonReferenceId = amazonOrderReferenceId;
    req.amazonQueryString = requestString;
    req.contextObject = requestProcessPayment;
    req.context = Context_GetAmazonOrderDetailBeforeAuthorization;
    //Local impl
//    requestProcessPayment.signature = [requestString HMACSHA256String:@"3t3uAZ/hFW6Eu0wMtpOhdMz9Ny1R/jAZ1ww5aOZ9"];
//    [PayWithAmazon processPayment:requestProcessPayment
//                completionHandler:^(PWAProcessPaymentResponse *response, NSError *error){
//                    if(error){
//                        DEBUGLOG(@"AmazonPay - ProcessPayment error: %@",error.localizedDescription);
//                    }
//                    else{
//                        DEBUGLOG(@"AmazonPay - ProcessPayment success.");
//                    }
//                }];
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent showLoadingView:nil];
    SEND_REQUEST(req, handGetAmazonOrderDetailResponse, handGetAmazonOrderDetailResponse);
}


-(void)startPaymentWithDelegate:(NSObject<PaymentDelegate>*) delegate{
    [super startPaymentWithDelegate:delegate];
    
    [[AMZNAuthorizationManager sharedManager] signOut:^(NSError * _Nullable error) {
        
    
    
    AMZNAuthorizeRequest *request = [[AMZNAuthorizeRequest alloc] init];
    request.scopes = [NSArray arrayWithObjects:[AMZNProfileScope profile],
                      [PWAPaymentScope initiate],
                      [PWAPaymentScope billingAddress],
                      [PWAPaymentScope shippingAddress],
                      [PWAPaymentScope instrument],
                      nil];
    
    // Make an Authorize call to the Login with Amazon SDK
    NSLog(@"Making Authorize LWA API call");
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    [[AMZNAuthorizationManager sharedManager] setSandboxMode:!settings.amazonIsLiveEnv];
    BOOL sandbox = [[AMZNAuthorizationManager sharedManager] isSandboxMode];
    DEBUGLOG(@"Amazon payment in sandbox mode: %@.", sandbox ? @"YES" : @"NO");
    [[AMZNAuthorizationManager sharedManager] authorize:request
                                            withHandler:^(AMZNAuthorizeResult * _Nullable result, BOOL userDidCancel,NSError * _Nullable error){
                                                if(error){
                                                    //TODO: Call delegate, pass error
                                                    NSError *err = [self createError:userDidCancel ? AMAZON_ERR_AUTH_USER_CANCELLED : AMAZON_ERR_AUTH_OTHER error:error];
                                                    [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
                                                }
                                                else{
                                                    self.accessToken = result.token;
                                                    [self createAmazonOrderReferenceObject];
                                                }
                                            }];
        
        }];
}

-(void)processPaymentWithAuthorization:(amazonProcessPaymentCompletion)completeHandler{
    if (self.currentProcessPaymentRequest != nil){
        [PayWithAmazon processPayment:self.currentProcessPaymentRequest
                    completionHandler:^(PWAProcessPaymentResponse *response, NSError *error){
                        if(error){
                            DEBUGLOG(@"AmazonPay - ProcessPayment error: %@",error.localizedDescription);
                            NSError *err = [self createError:AMAZON_ERR_PAYMENT_FAILED_TO_GET_ORDER_DETAIL error:error];
                            //[[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
                            
                            RequestAmazonGetOrderReferenceDetail *req = [[RequestAmazonGetOrderReferenceDetail alloc] init];
                            req.amazonToken = self.accessToken;
                            req.amazonReferenceId = response.amazonOrderReferenceId;
                            req.amazonQueryString = @"QAZWSX";
                            req.contextObject = error;
                            req.context = Context_GetAmazonOrderDetailAfterAuthorization_WithError;
                            
                            SwychBaseViewController *ctrl = [APP getCurrentViewController];
                            [ctrl showLoadingView:nil];
                            SEND_REQUEST(req, handGetAmazonOrderDetailResponse, handGetAmazonOrderDetailResponse);
                            
                        }
                        else{
                            DEBUGLOG(@"AmazonPay - ProcessPayment success.");
                            RequestAmazonGetOrderReferenceDetail *req = [[RequestAmazonGetOrderReferenceDetail alloc] init];
                            req.amazonToken = self.accessToken;
                            req.amazonReferenceId = response.amazonOrderReferenceId;
                            req.amazonQueryString = @"QAZWSX";
                            req.contextObject = completeHandler;
                            req.context = Context_GetAmazonOrderDetailAfterAuthorization;
                            
                            SwychBaseViewController *ctrl = [APP getCurrentViewController];
                            [ctrl showLoadingView:nil];
                            SEND_REQUEST(req, handGetAmazonOrderDetailResponse, handGetAmazonOrderDetailResponse);
                        }
                    }];
        }
    else{
        NSError *err = [self createError:AMAZON_ERR_PAYMENT_EMPTY_PROCESS_PAYMENT_REQ message:@"Empty PWAProcessPaymentRequest"];
        [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
    }
}

-(void)ValidateOPT:(ResponseAmazonGetOrderReferenceDetail*)response{
    PopupViewBase *v =  [Utils loadPopupViewFromNibWithTag:POPUP_VIEW_TAG_OTP_Verification];
    if (v && [v isKindOfClass:[ViewOTPVerification class]]){
        ViewOTPVerification *vVerification = (ViewOTPVerification*)v;
        vVerification.verificationDelegate =(id<OTPVerificationDelegate>) self;
        vVerification.bTapDissMiss = NO;
        [vVerification setMobilePhoneNumber:response.email];
        vVerification.textCode = response.errorMessage == nil ? NSLocalizedString(@"JustEmailedCodeTo", nil) : response.errorMessage;
        vVerification.contextObject = response.email;
        
        [Utils showPopupView:(PopupViewBase*)v];
    }
}

#pragma makr = OTPVerificationDelegate
-(void)OTPVerificationResendOTP:(ViewOTPVerification*)verificationView{
    
    [self sendResendOTPRequest:verificationView];
}
-(void)OTPVerificationSubmitOTP:(ViewOTPVerification *)verificationView value:(NSString*)OTP{
    [self sendOTPVerificationRequest:verificationView.user verificationView:verificationView];
}
-(void)sendResendOTPRequest:(ViewOTPVerification*)v{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent showLoadingView:nil];
    
    LocalUserInfo *user = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestResendOTP *req = [[RequestResendOTP alloc] init];
    req.swychId = user.swychId;
    req.email = (NSString*)v.contextObject;
    req.otpSentType = OTPSENTTYPE_AMAZON_EMAIL_VALIDATION;
    
    SEND_REQUEST(req, handleResendOTPResponse, handleResendOTPResponse);
}


-(void)handleResendOTPResponse:(ResponseBase*)response{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent dismissLoadingView];
    
    [Utils showAlert:response.errorMessage delegate:nil];
    
}
-(void)sendOTPVerificationRequest:(ModelUser*)user verificationView:(ViewOTPVerification*)v{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent showLoadingView:nil];
    
    LocalUserInfo *lu = [[LocalStorageManager instance] getLocalStoredUserInfo];
    RequestVerifyOTP *req = [[RequestVerifyOTP alloc] init];
    req.contextObject = v;
    req.swychId = lu.swychId;
    req.otp = [v getOTPCode];
    req.context = v.context;
    req.email = (NSString*)v.contextObject;
    req.otpSentType = OTPSENTTYPE_AMAZON_EMAIL_VALIDATION;
    SEND_REQUEST(req, handleOTPVerificationResponse, handleOTPVerificationResponse);
}

-(void)handleOTPVerificationResponse:(ResponseBase*)response{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent dismissLoadingView];
    
    
    if (response.success){
        
        ViewOTPVerification *v = (ViewOTPVerification*)response.contextObject;
        ResponseVerifyOTP *resp = (ResponseVerifyOTP*)response;
        [v dismiss];
        [self getOrderReferenceDetailsWithId:self.amazonOrderReferenceId];
        
    }
    else{
        [Utils showAlert:response.errorMessage delegate:nil];
    }
}


#pragma
#pragma mark - server conmunication handling

-(void)handGetAmazonOrderDetailResponse:(ResponseBase*)response{
    SwychBaseViewController *ctrlParent = [APP getCurrentViewController];
    [ctrlParent dismissLoadingView];
    
    SwychBaseViewController *ctrl = [[SwychBaseViewController alloc] init];
    if (![response isKindOfClass:[ResponseAmazonGetOrderReferenceDetail class]]){
        return;
    }
    ResponseAmazonGetOrderReferenceDetail *resp = (ResponseAmazonGetOrderReferenceDetail*)response;
    if (resp.errorCode == ERR_Payment_BuyerEmailVerificationNeeded){
        [self ValidateOPT:resp];
        return;
    }

    if ([ctrl handleServerResponse:response]){
        NSError *err = [self createError:AMAZON_ERR_PAYMENT_FAILED_TO_GET_ORDER_DETAIL message:@"Failed to get order detail from server."];
        [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
        return;
    }
    if(resp.context == Context_GetAmazonOrderDetailBeforeAuthorization){
        PWAProcessPaymentRequest *requestProcessPayment = (PWAProcessPaymentRequest*)resp.contextObject;
        requestProcessPayment.signature = resp.signature;
        self.currentProcessPaymentRequest = requestProcessPayment;
        //show confirm order view
        AmazonOrderConfirmViewController *ctrl =(AmazonOrderConfirmViewController*) [Utils loadViewControllerFromStoryboard:@"AmazonPayment" controllerId:@"AmazonOrderConfirmViewController"];
        ctrl.amazonPaymentHandler = self;
        ctrl.paymentInstrument = resp.paymentInstrument;
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ctrl];
        SwychBaseViewController *currCtrl = [APP getCurrentViewController];
        [currCtrl presentViewController:navigationController animated:YES completion:nil];
    }
    else if (resp.context == Context_GetAmazonOrderDetailAfterAuthorization_WithError){
        NSError *err = (NSError*)resp.contextObject;
        
        if (err == nil){
            err = [self createError:AMAZON_ERR_PAYMENT_CANNOT_PROCESS message:@"Cannot continue capture the payment."];
        }
        else{
            if ([err localizedFailureReason] != nil){
                err = [self createError:AMAZON_ERR_PAYMENT_CANNOT_PROCESS message:[err localizedFailureReason]];
            }
        }
        if (resp.orderReferenceDetail != nil && resp.orderReferenceDetail.getOrderReferenceDetailsResult != nil &&
            resp.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails != nil &&
            resp.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails.constraints != nil &&
            resp.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails.constraints.constraint != nil){
            NSArray<ModelAmazonConstraint*> *array =resp.orderReferenceDetail.getOrderReferenceDetailsResult.orderReferenceDetails.constraints.constraint;
            ModelAmazonConstraint *constraint = [array firstObject];
            err = [self createError:AMAZON_ERR_PAYMENT_CANNOT_PROCESS message:constraint.constraintDesc];
            
        }
        [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
    }
    else if (resp.context == Context_GetAmazonOrderDetailAfterAuthorization){
        //Get authorizationId here, then send to serer to capture
        NSString *authId = resp.amazonAuthorizationId;
        amazonProcessPaymentCompletion handler = (amazonProcessPaymentCompletion)resp.contextObject;
        NSString *state = resp.amazonOrderState;
        if (authId == nil){
            if(handler){
                handler(NO);
            }
            NSError *err = nil;
            if(state != nil && [state caseInsensitiveCompare:@"draft"] == NSOrderedSame){
                err = [self createError:AMAZON_ERR_PAYMENT_USER_CANCELLED message:@"User did not accept the payment."];
            }
            else{
                err = [self createError:AMAZON_ERR_PAYMENT_FAILED_TO_GET_AUTH_ID message:@"Cannot get authorizatonId from server."];
            }
            [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
        }
        else{
            if(handler){
                handler(YES);
            }

            if (state != nil && [state caseInsensitiveCompare:@"open"] == NSOrderedSame){
                ModelAmazonPaymentToken *token = [[ModelAmazonPaymentToken alloc] init];
                token.amazonOrderReferenceId = self.amazonOrderReferenceId;
                token.amazonAuthorizationId = authId;
                token.amazonAccessToken = self.accessToken;
                
                [[self getPaymentDelegate] paymentCompleted:[token getJSONDataWithDateTimeFormatter:nil] withPaymentMethod:PaymentMethod_AmazonPay withCallback:nil];
            }
            else{
                NSError *err = [self createError:AMAZON_ERR_PAYMENT_INVALID_ORDER_STATUS message:@"Cannot continue capture the payment due to invalid order state."];
                [[self getPaymentDelegate] paymentError:err withPaymentMethod:PaymentMethod_AmazonPay];
            }
        }
    }
}

@end
