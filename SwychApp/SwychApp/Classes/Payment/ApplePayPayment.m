//
//  ApplePayPayment.m
//  SwychApp
//
//  Created by Donald Hancock on 2/18/16.
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "ApplePayPayment.h"
#import <PassKit/PassKit.h>
#import "ModelPayment+Internal.h"
#import "ModelPaymentMethodSettings.h"
#import "PaymentHandler.h"

@implementation ApplePayPayment

+ (BOOL) isAvailable {
    return [PKPaymentAuthorizationViewController canMakePayments];
}


+(BOOL)canMakePayment{
    return [PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:@[PKPaymentNetworkAmex, PKPaymentNetworkDiscover, PKPaymentNetworkMasterCard, PKPaymentNetworkVisa]];
}

- (NSString*) name {
    return NSLocalizedString(@"PaymentOption_ApplePay", nil);
}


- (UIViewController*) createPaymentViewController {
    ModelPaymentMethodSettings *settings = [[ApplicationConfigurations instance] getPaymentMethodSettings];
    PKPaymentRequest* request = [[PKPaymentRequest alloc] init];
    request.paymentSummaryItems = [self.purchases select:^NSObject* (Purchase* obj) {
        return [PKPaymentSummaryItem summaryItemWithLabel:obj.lineItem
                                                   amount:[obj.amount toDecimalNumber]];
    }];
    
    request.currencyCode = currencyCodeString(self.currencyType);
    request.countryCode = countryCodeString(self.currencyType);
    request.merchantIdentifier = settings.applePayMerchantIdentifier;
    request.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkDiscover, PKPaymentNetworkMasterCard, PKPaymentNetworkVisa];
    PaymentProcessor processor = [[PaymentHandler instance] getPaymentProssor:PaymentMethod_ApplePay];
    if (processor == PaymentProcessor_Stripe){
        request.merchantCapabilities = PKMerchantCapability3DS;// | PKMerchantCapabilityEMV;
    }
    else{
        request.merchantCapabilities = PKMerchantCapability3DS;
    }
    request.requiredBillingAddressFields = PKAddressFieldEmail | PKAddressFieldPostalAddress;
    
    PKPaymentAuthorizationViewController* vc = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
    if (vc != nil){
        vc.delegate = (NSObject<PKPaymentAuthorizationViewControllerDelegate>*)self;
    }
    return vc;
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

@end
