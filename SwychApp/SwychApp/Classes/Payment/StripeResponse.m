//
//  StripeResponse.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "StripeResponse.h"
#import "NSObject+Extra.h"


@implementation StripeResponse

-(id)initWithData:(NSData*)data{
    self = [super init];
    if (self){
        //NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSError *error = nil;
        [self parseJSONData:data error:&error dateTimeFormatter:nil];
    }
    return self;
}

-(NSData*)tokenData{
    if (self.respId == nil){
        return nil;
    }
    else{
        return [self.respId dataUsingEncoding:NSUTF8StringEncoding];
    }
}

-(NSString*)getPropertyNameInJSONKeys:(NSString*)jsonKeyName{
    if ([jsonKeyName isEqualToString:@"id"]){
        return @"respId";
    }
    return [super getPropertyNameInJSONKeys:jsonKeyName];
}
-(NSString*)getJSONKeyNameInProperties:(NSString*)propertyName{
    if ([propertyName isEqualToString:@"respId"]){
        return @"id";
    }
    return [super getJSONKeyNameInProperties:propertyName];
}

@end
