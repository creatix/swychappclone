//
//  UIRoundImageView.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "UIRoundCornerImageView.h"

@interface UIRoundImageView : UIRoundCornerImageView

@end
