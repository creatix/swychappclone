//
//  RegistrationData.m
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "RegistrationData.h"

@implementation RegistrationData

-(id)initWithUser:(ModelUser*)user{
    if(self = [super init]){
        self.swychId = user.swychId;
        self.firstName = user.firstName;
        self.lastName = user.lastName;
        self.phoneNumber = user.phoneNumber;
        self.password = user.password;
        self.encryptedPassword = user.encryptedPassword;
        self.userStatus = user.userStatus;
        self.createdTime = user.createdTime;
        self.email = user.email;
    }
    return self;
}
@end
