//
//  TrackingEventGiftCardPurchasedForSendToFriend.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import "TrackingEventBase.h"

@interface TrackingEventGiftCardPurchasedForSendToFriend : TrackingEventBase

@end
