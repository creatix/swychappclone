//
//  ContactCollectionViewCell.h
//  SwychApp
//
//  Copyright © 2016 Swych Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModelContact;

@interface ContactCollectionViewCell : UICollectionViewCell{
    BOOL _selectedContact;
}

@property (nonatomic,strong) ModelContact *contact;
@property (nonatomic,assign) BOOL selectedContact;
@end
